FROM registry.access.redhat.com/ubi8/nodejs-14 as production

WORKDIR /tmp
COPY package.json /tmp/
RUN npm config set registry http://registry.npmjs.org/ && npm install --only=prod

USER 0

WORKDIR /usr/src/app
COPY . /usr/src/app/
RUN cp -a /tmp/node_modules /usr/src/app/

USER 1001

ENV NODE_ENV=production
ENV PORT=9000

CMD ["npm","run", "start"]
EXPOSE $PORT