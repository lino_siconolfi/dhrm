#!/bin/bash

#Copy the config files for safe-keeping
cp /srv/jhrm-ecd/config/production.js /tmp/production.js.bak
cp /srv/jhrm-ecd/config/local.js /tmp/local.js.bak

#Remove the old files
rm -rf /srv/jhrm-ecd/*