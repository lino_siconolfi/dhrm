define({ "api": [
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.Location",
            "description": ""
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "src/api/rapidhra/router.js",
    "group": "/Users/tom/Intelesense/jhrm/jhrm-bitbucket/src/api/rapidhra/router.js",
    "groupTitle": "/Users/tom/Intelesense/jhrm/jhrm-bitbucket/src/api/rapidhra/router.js",
    "name": ""
  },
  {
    "type": "get",
    "url": "/api/metadata/",
    "title": "sensor array profile",
    "description": "<p>List all Sensor Array Profiles</p>",
    "group": "Array_Profiles",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "Response",
            "description": "<p>JSON Array Object</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.sensor_array_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.fixed_mobile",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.site_mgrs",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.doehrs_location",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.mgrs_location",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.sampling_points_notes",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.sensorType",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n    \"sensor_array_id\": \"M-8RY8OF76KG\",\n    \"fixed_mobile\": \"Fixed\",\n    \"site_mgrs\": \"38SMC1661906536\",\n    \"doehrs_location\": \"Base Camp Taji\",\n    \"location_description\": \"All within 30 meters of the back gate\",\n    \"mgrs_location\": null,\n    \"sampling_points_notes\": \"Incident\"\n    \"sensor type: JCAD\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/metadata/router.js",
    "groupTitle": "Array_Profiles",
    "name": "GetApiMetadata"
  },
  {
    "type": "get",
    "url": "/api/rapidhra/hra",
    "title": "List HRAs",
    "description": "<p>List all DRI HRAs</p>",
    "group": "DRI_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "hra[]",
            "description": "<p>Array of DRI HRAs</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.hra_objective",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.location",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.sap_file_name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.created_by",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "hra.created_on",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.approved_by",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.last_edited_by",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.last_edited_on",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "hra.uploaded_to_doehrs",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "hra.qa_approved",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.status",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.sensor_type",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.media_type",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.population_at_risk",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "hra.individuals_at_risk",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.chemical_name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.cas_number",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "hra.acute_peak_pepc",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.acute_peak_pepc_units",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "hra.acute_average_pepc",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.acute_average_pepc_units",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "hra.acute_peak_meg",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.acute_peak_meg_name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.acute_peak_meg_units",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.acute_peak_risk",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.acute_peak_confidence",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "hra.acute_average_meg",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.acute_average_meg_name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.acute_average_meg_units",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.acute_average_risk",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.acute_average_confidence",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.pathway_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.pathway_name",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[{\n    \"id\": \"R-6JHHO9BP8R\",\n    \"hra_objective\": \"Complete an HRA according to the exposure pathway\",\n    \"location\": \"Base Camp AL AWAD\",\n    \"sap_file_name\": \"\",\n    \"created_by\": \"John Doe\",\n    \"created_on\": \"2020-07-10T13:18:45.000Z\",\n    \"approved_by\": null,\n    \"last_edited_by\": null,\n    \"last_edited_on\": null,\n    \"uploaded_to_doehrs\": 0,\n    \"qa_approved\": 0,\n    \"status\": \"COMPLETE\",\n    \"sensor_type\": null,\n    \"media_type\": null,\n    \"population_at_risk\": \"Entire Base\",\n    \"individuals_at_risk\": 120,\n    \"chemical_name\": \"Carbon Monoxide\",\n    \"cas_number\": \"630-08-0\",\n    \"acute_peak_pepc\": 20,\n    \"acute_peak_pepc_units\": \"mg/m3\",\n    \"acute_average_pepc\": 0,\n    \"acute_average_pepc_units\": \"mg/m3\",\n    \"acute_peak_meg\": 95,\n    \"acute_peak_meg_name\": \"10minNEG\",\n    \"acute_peak_meg_units\": \"mg/m3\",\n    \"acute_peak_risk\": \"Low\",\n    \"acute_peak_confidence\": \"low\",\n    \"acute_average_meg\": 95,\n    \"acute_average_meg_name\": \"1hourNEG\",\n    \"acute_average_meg_units\": \"mg/m3\",\n    \"acute_average_risk\": \"Low\",\n    \"acute_average_confidence\": \"low\",\n    \"pathway_id\": null,\n    \"pathway_name\": null\n}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/rapidhra/router.js",
    "groupTitle": "DRI_HRA",
    "name": "GetApiRapidhraHra"
  },
  {
    "type": "get",
    "url": "/api/rapidhra/hra/:hraId",
    "title": "Get single DRI HRA",
    "description": "<p>Look up a DRI HRA by ID</p>",
    "group": "DRI_HRA",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>URL Parameter. The JHRM ID of the HRA</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "hra",
            "description": "<p>DRI HRA</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.hra_objective",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.location",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.sap_file_name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.created_by",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "hra.created_on",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.approved_by",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.last_edited_by",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.last_edited_on",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "hra.uploaded_to_doehrs",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "hra.qa_approved",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.status",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.sensor_type",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.media_type",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.population_at_risk",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "hra.individuals_at_risk",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.chemical_name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.cas_number",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "hra.acute_peak_pepc",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.acute_peak_pepc_units",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "hra.acute_average_pepc",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.acute_average_pepc_units",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "hra.acute_peak_meg",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.acute_peak_meg_name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.acute_peak_meg_units",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.acute_peak_risk",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.acute_peak_confidence",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "hra.acute_average_meg",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.acute_average_meg_name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.acute_average_meg_units",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.acute_average_risk",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.acute_average_confidence",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.pathway_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hra.pathway_name",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n    \"id\": \"R-6JHHO9BP8R\",\n    \"hra_objective\": \"Complete an HRA according to the exposure pathway\",\n    \"location\": \"Base Camp AL AWAD\",\n    \"sap_file_name\": \"\",\n    \"created_by\": \"John Doe\",\n    \"created_on\": \"2020-07-10T13:18:45.000Z\",\n    \"approved_by\": null,\n    \"last_edited_by\": null,\n    \"last_edited_on\": null,\n    \"uploaded_to_doehrs\": 0,\n    \"qa_approved\": 0,\n    \"status\": \"COMPLETE\",\n    \"sensor_type\": null,\n    \"media_type\": null,\n    \"population_at_risk\": \"Entire Base\",\n    \"individuals_at_risk\": 120,\n    \"chemical_name\": \"Carbon Monoxide\",\n    \"cas_number\": \"630-08-0\",\n    \"acute_peak_pepc\": 20,\n    \"acute_peak_pepc_units\": \"mg/m3\",\n    \"acute_average_pepc\": 0,\n    \"acute_average_pepc_units\": \"mg/m3\",\n    \"acute_peak_meg\": 95,\n    \"acute_peak_meg_name\": \"10minNEG\",\n    \"acute_peak_meg_units\": \"mg/m3\",\n    \"acute_peak_risk\": \"Low\",\n    \"acute_peak_confidence\": \"low\",\n    \"acute_average_meg\": 95,\n    \"acute_average_meg_name\": \"1hourNEG\",\n    \"acute_average_meg_units\": \"mg/m3\",\n    \"acute_average_risk\": \"Low\",\n    \"acute_average_confidence\": \"low\",\n    \"pathway_id\": null,\n    \"pathway_name\": null\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "HRA Not Found",
          "content": "HTTP/1.1 404 HRA Not Found",
          "type": "json"
        },
        {
          "title": "Access Denied",
          "content": "HTTP/1.1 403 Access Denied",
          "type": "json"
        },
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/rapidhra/router.js",
    "groupTitle": "DRI_HRA",
    "name": "GetApiRapidhraHraHraid"
  },
  {
    "type": "get",
    "url": "/api/rapidhra/hra/:hraId/assessment",
    "title": "Get DRI HRA Assessments",
    "description": "<p>Get all assessments for the given HRA.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>URL Parameter. The JHRM ID of the HRA</p>"
          }
        ]
      }
    },
    "group": "DRI_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "assessment[]",
            "description": "<p>Array of assessments</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "assessment.id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "assessment.chem_rapid_hra_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "assessment.type",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "assessment.chemical_name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "assessment.cas_number",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "assessment.meg",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "assessment.meg_name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "assessment.meg_units",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "assessment.meg_version",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "assessment.severity",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "assessment.prob_degree_of_exp",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "assessment.prob_degree_of_exp_score",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "assessment.prob_rep_of_data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "assessment.prob_rep_of_data_score",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "assessment.prob_duration_of_exp",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "assessment.prob_duration_of_exp_score",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "assessment.prob_rate_of_exp",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "assessment.prob_rate_of_exp_score",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "assessment.probability",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "assessment.probability_score",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "assessment.risk_level",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "assessment.confidence",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "assessment.custom_exposure_duration",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "assessment.custom_meg",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[{\n    id : 22\n    chem_rapid_hra_id : \"R-P9QFS798RK\"\n    type : \"ACUTE_PEAK\"\n    chemical_name : \"Nitric oxide\"\n    cas_number : \"10102-43-9\"\n    meg : 3.68\n    meg_name : \"8hourNEG\"\n    meg_units : \"mg/m3\"\n    meg_version : \"2013 Revision\"\n    severity : \"Negligible\"\n    prob_degree_of_exp : \"\"\n    prob_degree_of_exp_score : 0\n    prob_rep_of_data : \"Adequate\"\n    prob_rep_of_data_score : 2\n    prob_duration_of_exp : \"\"\n    prob_duration_of_exp_score : 0\n    prob_rate_of_exp : \"Moderate\"\n    prob_rate_of_exp_score : 2\n    probability : \"Unlikely\"\n    probability_score : 4\n    risk_level : \"Low\"\n    confidence : \"low\"\n    custom_exposure_duration : 10\n    custom_meg : 0\n}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "No associated megs found.",
          "content": "HTTP/1.1 404 No associated megs found.",
          "type": "json"
        },
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/rapidhra/router.js",
    "groupTitle": "DRI_HRA",
    "name": "GetApiRapidhraHraHraidAssessment"
  },
  {
    "type": "get",
    "url": "/api/rapidhra/hra/:hraId/megs",
    "title": "Get DRI HRA Megs",
    "description": "<p>Get all related megs for a DRI HRA. If no chemical is selected yet for the HRA, this will return an error.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>URL Parameter. The JHRM ID of the HRA</p>"
          }
        ]
      }
    },
    "group": "DRI_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "meg[]",
            "description": "<p>Array of MEGs</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "meg.chemical_name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "meg.cas_number",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "DimensionedValue",
            "optional": false,
            "field": "meg.value",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "meg.basis",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "meg.severity",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "meg.timeframe",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "meg.minutes",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "meg.parm",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "meg.is_cwa",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "meg.molecular_weight",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "meg.media",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "meg.version",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[{\n    chemical_name : \"Nitric oxide\",\n    cas_number : \"10102-43-9\",\n    value : \n    {\n        value : 3.68,\n        isValid : true,\n        originalValue : 3.68,\n        units : \"mg/m3\",\n        precision : 3,\n        history : [\"Current value: 3.68 mg/m3\"]\n    },\n    basis : \"CEGL*\",\n    severity : \"Negligible\",\n    timeframe : \"8hour\",\n    minutes : 480,\n    parm : \"8hourNEG\",\n    is_cwa : false,\n    molecular_weight : 30.0061,\n    media : \"Air\",\n    version : \"2013 Revision\"\n}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "No associated megs found.",
          "content": "HTTP/1.1 404 No associated megs found.",
          "type": "json"
        },
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/rapidhra/router.js",
    "groupTitle": "DRI_HRA",
    "name": "GetApiRapidhraHraHraidMegs"
  },
  {
    "type": "get",
    "url": "/api/rapidhra/hra/:hraId/outcomes",
    "title": "Get potential health outcomes",
    "description": "<p>Get potential health outcomes from the RD230 reference. Search by CAS number and MEG.</p>",
    "group": "DRI_HRA",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>URL Parameter. The JHRM ID of the HRA</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "response",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "response.potential_health_outcomes",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n    \"potential_health_outcomes\" : null\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "HRA not found",
          "content": "HTTP/1.1 404 HRA not found",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/rapidhra/router.js",
    "groupTitle": "DRI_HRA",
    "name": "GetApiRapidhraHraHraidOutcomes"
  },
  {
    "type": "get",
    "url": "/api/rapidhra/hra/:hraId/pathway",
    "title": "Get exposure pathway",
    "description": "<p>Get the exposure pathway associated with the current HRA.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>URL Parameter. The JHRM ID of the HRA</p>"
          }
        ]
      }
    },
    "group": "DRI_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "pathway",
            "description": "<p>Exposure Pathway</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.location",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.threat_source",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.health_hazard",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.exposure_point_area",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.exposure_medium",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.exposure_route",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.exposure_duration_of_concern",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.exposed_population",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[{\n    id : \"value\",\n    location : \"value\",\n    name : \"value\",\n    threat_source : \"value\",\n    health_hazard : \"value\",\n    exposure_point_area : \"value\",\n    exposure_medium : \"value\",\n    exposure_route : \"value\",\n    exposure_duration_of_concern : \"value\",\n    exposed_population : \"value\"\n}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "HRA Not Found",
          "content": "HTTP/1.1 404 HRA Not Found",
          "type": "json"
        },
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/rapidhra/router.js",
    "groupTitle": "DRI_HRA",
    "name": "GetApiRapidhraHraHraidPathway"
  },
  {
    "type": "get",
    "url": "/api/rapidhra/hra/:hraId/results",
    "title": "Get Data Log",
    "description": "<p>Get the raw datalog values that comprise the sample(s)</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>URL Parameter. The JHRM ID of the HRA</p>"
          }
        ]
      }
    },
    "group": "DRI_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result[]",
            "description": "<p>Array of results</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.chem_rapid_hra_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.unique_sample_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.unique_result_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.chemical_name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.cas_number",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.concentration",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.units",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.analytical_method",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.concentration_below_threshold",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.selected",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[{\n    id : 54,\n    chem_rapid_hra_id : \"R-P9QFS798RK\",\n    unique_sample_id : \"123456\",\n    unique_result_id : \"CEW7ADOL6K\",\n    chemical_name : \"NO\",\n    cas_number : \"10102-43-9\",\n    concentration : 0.304025,\n    units : \"mg/m3\",\n    analytical_method : \"\",\n    concentration_below_threshold : 0,\n    selected : 1,\n }]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "HRA not found",
          "content": "HTTP/1.1 404 HRA not found",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/rapidhra/router.js",
    "groupTitle": "DRI_HRA",
    "name": "GetApiRapidhraHraHraidResults"
  },
  {
    "type": "get",
    "url": "/api/rapidhra/hra/:hraId/samples",
    "title": "Get a list of MultiRae samples for Rapid HRA",
    "description": "<p>List all samples</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>URL Parameter. The JHRM ID of the HRA</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "selected",
            "description": "<p>URL Parameter. Limit results to selected samples</p>"
          }
        ]
      }
    },
    "group": "DRI_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "sample[]",
            "description": "<p>Array of samples</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sample.sample_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sample.sampling_point",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sample.chemical_name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sample.cas_number",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sample.sensor_type",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "sample.start_date_time",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "sample.end_date_time",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "sample.duration",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sample.exceeds_meg",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "sample.latitude",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "sample.longitude",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sample.media",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "sample.selected",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "DimensionedValue",
            "optional": false,
            "field": "sample.peak_concentration",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "DimensionedValue",
            "optional": false,
            "field": "sample.average_concentration",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[{\n    id : \"123456\",\n    sampling_point : \"Office Area\",\n    chemical_name : \"NO\",\n    cas_number : \"10102-43-9\",\n    sensor_type : \"MULTIRAE\",\n    start_date_time : \"2017-08-01T10:53:00.000Z\",\n    end_date_time : \"2017-08-01T11:34:00.000Z\",\n    duration : 41,\n    exceeds_meg : \"Yes\",\n    latitude : 33.5396754,\n    longitude : 44.2019173,\n    media : \"AIR\",\n    selected : 1,\n    peak_concentration : \n    {\n        value : 3.68174847,\n        isValid : true,\n        originalValue : 3.68174847,\n        units : \"mg/m3\",\n        precision : 1,\n        history : [ \"Current value: 3 ppm\", \"Converted from ppm to mg/m3 with equation: 0.0409 x ppm (3) x 30.0061\", \"Current value: 4 mg/m3\" ]\n    }\n    average_concentration : \n    {\n        value : 2.45449898,\n        isValid : true,\n        originalValue : 2.45449898,\n        units : \"mg/m3\",\n        precision : 1,\n        history : [ \"Current value: 2 ppm\", \"Converted from ppm to mg/m3 with equation: 0.0409 x ppm (2) x 30.0061\", \"Current value: 2 mg/m3\" ]\n     }\n }]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "HRA not found",
          "content": "HTTP/1.1 404 HRA not found",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/rapidhra/router.js",
    "groupTitle": "DRI_HRA",
    "name": "GetApiRapidhraHraHraidSamples"
  },
  {
    "type": "post",
    "url": "/api/hra/:hraId/samples",
    "title": "Associate Samples",
    "description": "<p>Associate samples with this DRI HRA</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>URL Parameter. The JHRM ID of the HRA that is being created</p>"
          },
          {
            "group": "Parameter",
            "type": "Number[]",
            "optional": false,
            "field": "Array",
            "description": "<p>of sample IDs to associate.</p>"
          }
        ]
      }
    },
    "group": "DRI_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "response",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "response.status",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\":\"success\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Access Denied",
          "content": "HTTP/1.1 403 Not allowed to edit complete HRA.",
          "type": "json"
        },
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 No samples found",
          "type": "json"
        },
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Unable to reconcile sample IDs",
          "type": "json"
        },
        {
          "title": "HRA not found",
          "content": "HTTP/1.1 404 HRA not found",
          "type": "json"
        },
        {
          "title": "Bad Request",
          "content": "HTTP/1.1 401 At least one sample must be selected",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/rapidhra/router.js",
    "groupTitle": "DRI_HRA",
    "name": "PostApiHraHraidSamples"
  },
  {
    "type": "post",
    "url": "/api/rapidhra/hra",
    "title": "Save DRI HRA",
    "description": "<p>Create or save a DRI HRA</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>URL Parameter. The JHRM ID of the HRA</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "hra",
            "description": "<p>HRA Properties object</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hra.id",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hra.hra_objective",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hra.location",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hra.population_at_risk",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hra.individuals_at_risk",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hra.sap_file_name",
            "description": ""
          }
        ]
      }
    },
    "group": "DRI_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "response",
            "description": "<p>Response JSON Object</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.Status",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.id",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 201 CREATED\n{\n    Status: \"Create Record Success\"\n    id: \"E-FFGTUSD1ZS\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "HRA Not Found",
          "content": "HTTP/1.1 404 HRA Not Found",
          "type": "json"
        },
        {
          "title": "Access Denied",
          "content": "HTTP/1.1 403 Not allowed to edit complete HRA.",
          "type": "json"
        },
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/rapidhra/router.js",
    "groupTitle": "DRI_HRA",
    "name": "PostApiRapidhraHra"
  },
  {
    "type": "post",
    "url": "/api/rapidhra/hra/:hraId/assessment",
    "title": "",
    "description": "<p>Post one or more assessments, unique on chemical and type</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>URL Parameter. The JHRM ID of the HRA</p>"
          },
          {
            "group": "Parameter",
            "type": "Object[]",
            "optional": false,
            "field": "assessment[]",
            "description": "<p>Array of assessments</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "assessment.chem_rapid_hra_id",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "assessment.type",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "assessment.chemical_name",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "assessment.cas_number",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "assessment.meg",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "assessment.meg_name",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "assessment.meg_units",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "assessment.meg_version",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "assessment.severity",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "assessment.prob_degree_of_exp",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "assessment.prob_degree_of_exp_score",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "assessment.prob_rep_of_data",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "assessment.prob_rep_of_data_score",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "assessment.prob_duration_of_exp",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "assessment.prob_duration_of_exp_score",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "assessment.prob_rate_of_exp",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "assessment.prob_rate_of_exp_score",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "assessment.probability",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "assessment.probability_score",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "assessment.risk_level",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "assessment.confidence",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "assessment.custom_exposure_duration",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "assessment.custom_meg",
            "description": ""
          }
        ]
      }
    },
    "group": "DRI_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "response",
            "description": "<p>Response JSON Object</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.Status",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 201 CREATED\n{\n    Status: \"Create Record Success\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "HRA Not Found",
          "content": "HTTP/1.1 404 HRA Not Found",
          "type": "json"
        },
        {
          "title": "Access Denied",
          "content": "HTTP/1.1 403 Not allowed to edit complete HRA.",
          "type": "json"
        },
        {
          "title": "Bad Request",
          "content": "HTTP/1.1 400 Assessment missing required field.",
          "type": "json"
        },
        {
          "title": "Bad Request",
          "content": "HTTP/1.1 400 At least one assessment is required.",
          "type": "json"
        },
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/rapidhra/router.js",
    "groupTitle": "DRI_HRA",
    "name": "PostApiRapidhraHraHraidAssessment"
  },
  {
    "type": "post",
    "url": "/api/rapidhra/hra/:hraId/pathway",
    "title": "Create exposure pathway",
    "description": "<p>Create a new exposure pathway and associate with the current HRA.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>URL Parameter. The JHRM ID of the HRA</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "pathway",
            "description": "<p>Exposure Pathway</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pathway.location",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pathway.name",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pathway.threat_source",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pathway.health_hazard",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pathway.exposure_point_area",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pathway.exposure_medium",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pathway.exposure_route",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pathway.exposure_duration_of_concern",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pathway.exposed_population",
            "description": ""
          }
        ]
      }
    },
    "group": "DRI_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "response",
            "description": "<p>Response JSON Object</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.Status",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.id",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 201 CREATED\n{\n    Status: \"Create Record Success\"\n    id: \"P-FFGTUSD1ZS\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "HRA Not Found",
          "content": "HTTP/1.1 404 HRA Not Found",
          "type": "json"
        },
        {
          "title": "Access Denied",
          "content": "HTTP/1.1 403 Not allowed to edit complete HRA.",
          "type": "json"
        },
        {
          "title": "Invalid Request",
          "content": "HTTP/1.1 401 The parameter \"name\" is required.",
          "type": "json"
        },
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/rapidhra/router.js",
    "groupTitle": "DRI_HRA",
    "name": "PostApiRapidhraHraHraidPathway"
  },
  {
    "type": "post",
    "url": "/api/rapidhra/hra/:hraId/summary.",
    "title": "Set Summary Report Input",
    "description": "<p>Associate samples with this DRI HRA</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>URL Parameter. The JHRM ID of the HRA that is being created</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "report",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "report.potential_health_outcomes",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "report.additional_information",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "report.recommended_mitigation",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "report.communicated_to_name",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "report.communicated_to_position",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "report.communicated_to_contact",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "report.communicated_date",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "report.communicated_by",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "report.communicated_how",
            "description": ""
          }
        ]
      }
    },
    "group": "DRI_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "response",
            "description": "<p>Response JSON Object</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.Status",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 201 CREATED\n{\n    Status: \"Create Record Success\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "HRA Not Found",
          "content": "HTTP/1.1 404 HRA Not Found",
          "type": "json"
        },
        {
          "title": "Access Denied",
          "content": "HTTP/1.1 403 Not allowed to edit complete HRA.",
          "type": "json"
        },
        {
          "title": "Bad Request",
          "content": "HTTP/1.1 400 Assessment missing required field.",
          "type": "json"
        },
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/rapidhra/router.js",
    "groupTitle": "DRI_HRA",
    "name": "PostApiRapidhraHraHraidSummary"
  },
  {
    "type": "post",
    "url": "/api/rapidhra/hra/:hraId/summary.",
    "title": "Set Summary Report Input",
    "description": "<p>Associate samples with this DRI HRA</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>URL Parameter. The JHRM ID of the HRA that is being created</p>"
          }
        ]
      }
    },
    "group": "DRI_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "report",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.chem_rapid_hra_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.potential_health_outcomes",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.additional_information",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.recommended_mitigation",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.communicated_to_name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.communicated_to_position",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.communicated_to_contact",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.communicated_date",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.communicated_by",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.communicated_how",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n    chem_rapid_hra_id : \"R-2018BE4\"\n    potential_health_outcomes : \"value\"\n    additional_information : \"value\"\n    recommended_mitigation : \"value\"\n    communicated_to_name : \"value\"\n    communicated_to_position : \"value\"\n    communicated_to_contact : \"value\"\n    communicated_date : \"value\"\n    communicated_by : \"value\"\n    communicated_how : \"value\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "HRA Not Found",
          "content": "HTTP/1.1 404 HRA Not Found",
          "type": "json"
        },
        {
          "title": "Access Denied",
          "content": "HTTP/1.1 403 Not allowed to edit complete HRA.",
          "type": "json"
        },
        {
          "title": "Bad Request",
          "content": "HTTP/1.1 400 Assessment missing required field.",
          "type": "json"
        },
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/rapidhra/router.js",
    "groupTitle": "DRI_HRA",
    "name": "PostApiRapidhraHraHraidSummary"
  },
  {
    "type": "post",
    "url": "/api/metadata/sensorarray/:arrayId/:sensorSn/deletesensor",
    "title": "Add sensors to an Array profile",
    "description": "<p>Delete a new sensor from the Sensor Array Profile.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "alpha-number",
            "optional": false,
            "field": "sensor_metadata_id",
            "description": "<p>URL Parameter. The sensor array ID of the sensor array profile</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "serial_number",
            "description": "<p>URL Parameter. The serial number of the sensor array</p>"
          }
        ]
      }
    },
    "group": "Delete_MetaData_Sensor_Row",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "Response",
            "description": "<p>JSON Array Object</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.sensor_metadata_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.serial_number",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 201 CREATED\n{\n    Status: \"Delete Record Success\"\n    id: \"M-123456\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Sensor Not Found",
          "content": "HTTP/1.1 404 Sensor Not Found",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/metadata/router.js",
    "groupTitle": "Delete_MetaData_Sensor_Row",
    "name": "PostApiMetadataSensorarrayArrayidSensorsnDeletesensor"
  },
  {
    "type": "get",
    "url": "/api/enhancedhra/locations",
    "title": "List Locations",
    "description": "<p>List DOEHRS-IH locations.</p>",
    "group": "EHRA_DOEHRS_Data",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "location[]",
            "description": "<p>Array of locations</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "location.location_name",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[{\n    \"ilocation_name\":14681,\n}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/enhancedhra/router.js",
    "groupTitle": "EHRA_DOEHRS_Data",
    "name": "GetApiEnhancedhraLocations"
  },
  {
    "type": "get",
    "url": "/api/enhancedhra/pathway",
    "title": "List Exposure Pathways",
    "description": "<p>List exposure pathways optionally filtered by location and environmental media.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "location",
            "description": "<p>Location (Optional)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "env_media",
            "description": "<p>Environmental Media (Optional)</p>"
          }
        ]
      }
    },
    "group": "EHRA_DOEHRS_Data",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "pathway[]",
            "description": "<p>Array of exposure pathways</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "pathway.id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.population_at_risk_descr",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.source",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.environmental_media",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.health_threat",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.exposure_route",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.affected_personnel",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.priority",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[{\n    \"id\":14681,\n    \"name\":\"PM2.5 AMBIENT AIR\",\n    \"source\":\"Ambient Air\",\n    \"environmental_media\":\"Air\",\n    \"health_threat\":\"Airborne Contaminants/Particulate Matter\",\n    \"exposure_route\":\"Inhalation\",\n    \"affected_personnel\":1,\n    \"population_at_risk_descr\":\"Entire Camp\",\n    \"priority\":\"Low\"\n}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Invalid parameter",
          "content": "HTTP/1.1 422 Invalid parameter",
          "type": "json"
        },
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/enhancedhra/router.js",
    "groupTitle": "EHRA_DOEHRS_Data",
    "name": "GetApiEnhancedhraPathway"
  },
  {
    "type": "get",
    "url": "/api/rapidhra/pathway",
    "title": "List MetaData Pathways",
    "description": "<p>List exposure pathways optionally filtered by location and environmental media.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "location",
            "description": "<p>Location (Optional)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "env_media",
            "description": "<p>Environmental Media (Optional)</p>"
          }
        ]
      }
    },
    "group": "EHRA_DOEHRS_Data",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "pathway[]",
            "description": "<p>Array of exposure pathways</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "pathway.id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.population_at_risk_descr",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.source",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.environmental_media",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.health_threat",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.exposure_route",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.affected_personnel",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.priority",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[{\n    \"id\":14681,\n    \"name\":\"PM2.5 AMBIENT AIR\",\n    \"source\":\"Ambient Air\",\n    \"environmental_media\":\"Air\",\n    \"health_threat\":\"Airborne Contaminants/Particulate Matter\",\n    \"exposure_route\":\"Inhalation\",\n    \"affected_personnel\":1,\n    \"population_at_risk_descr\":\"Entire Camp\",\n    \"priority\":\"Low\"\n}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Invalid parameter",
          "content": "HTTP/1.1 422 Invalid parameter",
          "type": "json"
        },
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/rapidhra/router.js",
    "groupTitle": "EHRA_DOEHRS_Data",
    "name": "GetApiRapidhraPathway"
  },
  {
    "type": "get",
    "url": "/api/enhancedhra/hra",
    "title": "List HRAs",
    "description": "<p>List all HRAs</p>",
    "group": "Enhanced_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "hra[]",
            "description": "<p>Array of HRAs</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.hra_objective",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.location",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "hra.latitude",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "hra.longitude",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.sap_file_name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.id_doehrs_pathway_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.exposure_pathway_name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.chemical_name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.exposure_route",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.source",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.population_at_risk",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.media",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.created_by",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "date",
            "optional": false,
            "field": "hra.created_on",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.last_edited_by",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "date",
            "optional": false,
            "field": "hra.lasted_edited_on",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.approved_by",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.priority",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.status",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.recommended_mitigation",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.potential_health_outcomes_acute",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.potential_health_outcomes_chronic",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.cas_number",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.acute_peak_risk",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.acute_peak_confidence",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.acute_average_risk",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.acute_average_confidence",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.chronic_average_risk",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.chronic_average_confidence",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[{\n    \"id\":\"E-FFGTUSD1ZS\",\n    \"hra_objective\":\"Sample HRA\",\n    \"location\":\"Base Camp TAJI\",\n    \"latitude\":69.2634,\n    \"longitude\":34.9462,\n    \"sap_file_name\":null,\n    \"id_doehrs_pathway_id\":14681,\n    \"exposure_pathway_name\":\"PM2.5 AMBIENT AIR\",\n    \"chemical_name\":\"PARTICULATE <2.5M (PM-2.5)\",\n    \"exposure_route\":\"Inhalation\",\n    \"source\":\"Ambient Air\",\n    \"population_at_risk\":\"Entire Camp\",\n    \"media\":\"Air\",\n    \"created_by\":\"Ally McBeal\",\n    \"created_on\":\"2020-04-13T18:53:30.000Z\",\n    \"last_edited_by\":null,\n    \"lasted_edited_on\":null,\n    \"approved_by\":null,\n    \"priority\":null,\n    \"status\":\"INCOMPLETE\",\n    \"recommended_mitigation\":null,\n    \"potential_health_outcomes_acute\":null,\n    \"potential_health_outcomes_chronic\":null,\n    \"cas_number\":\"PM2.5\",\n    \"acute_peak_risk\":\"Moderate\",\n    \"acute_peak_confidence\":\"medium\",\n    \"acute_average_risk\":\"Moderate\",\n    \"acute_average_confidence\":\"medium\",\n    \"chronic_average_risk\":\"Low\",\n    \"chronic_average_confidence\":\"medium\"\n}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/enhancedhra/router.js",
    "groupTitle": "Enhanced_HRA",
    "name": "GetApiEnhancedhraHra"
  },
  {
    "type": "get",
    "url": "/api/enhancedhra/hra/:hraId",
    "title": "Get HRA",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>URL Parameter. The JHRM ID of the HRA</p>"
          }
        ]
      }
    },
    "group": "Enhanced_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "hra",
            "description": "<p>HRA Object</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.hra_objective",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.location",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "hra.latitude",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "hra.longitude",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.sap_file_name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.id_doehrs_pathway_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.exposure_pathway_name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.chemical_name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.exposure_route",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.source",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.population_at_risk",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.media",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.created_by",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "date",
            "optional": false,
            "field": "hra.created_on",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.last_edited_by",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "date",
            "optional": false,
            "field": "hra.lasted_edited_on",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.approved_by",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.priority",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.status",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.recommended_mitigation",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.potential_health_outcomes_acute",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.potential_health_outcomes_chronic",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.cas_number",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.acute_peak_risk",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.acute_peak_confidence",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.acute_average_risk",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.acute_average_confidence",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.chronic_average_risk",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "hra.chronic_average_confidence",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n    \"id\":\"E-FFGTUSD1ZS\",\n    \"hra_objective\":\"Sample HRA\",\n    \"location\":\"Base Camp TAJI\",\n    \"latitude\":69.2634,\n    \"longitude\":34.9462,\n    \"sap_file_name\":null,\n    \"id_doehrs_pathway_id\":14681,\n    \"exposure_pathway_name\":\"PM2.5 AMBIENT AIR\",\n    \"chemical_name\":\"PARTICULATE <2.5M (PM-2.5)\",\n    \"exposure_route\":\"Inhalation\",\n    \"source\":\"Ambient Air\",\n    \"population_at_risk\":\"Entire Camp\",\n    \"media\":\"Air\",\n    \"created_by\":\"Ally McBeal\",\n    \"created_on\":\"2020-04-13T18:53:30.000Z\",\n    \"last_edited_by\":null,\n    \"lasted_edited_on\":null,\n    \"approved_by\":null,\n    \"priority\":null,\n    \"status\":\"INCOMPLETE\",\n    \"recommended_mitigation\":null,\n    \"potential_health_outcomes_acute\":null,\n    \"potential_health_outcomes_chronic\":null,\n    \"cas_number\":\"PM2.5\",\n    \"acute_peak_risk\":\"Moderate\",\n    \"acute_peak_confidence\":\"medium\",\n    \"acute_average_risk\":\"Moderate\",\n    \"acute_average_confidence\":\"medium\",\n    \"chronic_average_risk\":\"Low\",\n    \"chronic_average_confidence\":\"medium\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "HRA Not Found",
          "content": "HTTP/1.1 404 HRA Not Found",
          "type": "json"
        },
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/enhancedhra/router.js",
    "groupTitle": "Enhanced_HRA",
    "name": "GetApiEnhancedhraHraHraid"
  },
  {
    "type": "get",
    "url": "/api/enhancedhra/hra/:hraId/audit",
    "title": "Audit Log",
    "description": "<p>Generate audit log for HRA</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>URL Parameter. The JHRM ID of the HRA</p>"
          }
        ]
      }
    },
    "group": "Enhanced_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "response",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "response.status",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\":\"success\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "HRA not found",
          "content": "HTTP/1.1 404 HRA not found",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/enhancedhra/router.js",
    "groupTitle": "Enhanced_HRA",
    "name": "GetApiEnhancedhraHraHraidAudit"
  },
  {
    "type": "get",
    "url": "/api/enhancedhra/hra/:hraId/megs",
    "title": "Create new HRA",
    "description": "<p>Get all remated megs for an HRA. If no chemical is selected yet for the HRA, this will return an error.</p>",
    "group": "Enhanced_HRA",
    "version": "0.0.0",
    "filename": "src/api/enhancedhra/router.js",
    "groupTitle": "Enhanced_HRA",
    "name": "GetApiEnhancedhraHraHraidMegs"
  },
  {
    "type": "get",
    "url": "/api/enhancedhra/hra/:hraId/pathway",
    "title": "Get Pathway",
    "description": "<p>Get exposure pathway for the HRA if one exists.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>The JHRM ID of the HRA</p>"
          }
        ]
      }
    },
    "group": "Enhanced_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "pathway",
            "description": "<p>Exposure pathway</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "pathway.id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.population_at_risk_descr",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.source",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.environmental_media",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.health_threat",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.exposure_route",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.affected_personnel",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.priority",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[{\n    \"id\":14681,\n    \"name\":\"PM2.5 AMBIENT AIR\",\n    \"source\":\"Ambient Air\",\n    \"environmental_media\":\"Air\",\n    \"health_threat\":\"Airborne Contaminants/Particulate Matter\",\n    \"exposure_route\":\"Inhalation\",\n    \"affected_personnel\":1,\n    \"population_at_risk_descr\":\"Entire Camp\",\n    \"priority\":\"Low\"\n}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "HRA not found",
          "content": "HTTP/1.1 404 HRA not found",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/enhancedhra/router.js",
    "groupTitle": "Enhanced_HRA",
    "name": "GetApiEnhancedhraHraHraidPathway"
  },
  {
    "type": "get",
    "url": "/api/enhancedhra/hra/:hraId/results",
    "title": "List Associated Results",
    "description": "<p>List sample results for an HRA based on the samples selected.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>The JHRM ID of the HRA</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "chemical",
            "description": "<p>Optional. Filter the results to a single chemical</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "selected",
            "description": "<p>Optional. Only return selected results</p>"
          }
        ]
      }
    },
    "group": "Enhanced_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "Result[]",
            "description": "<p>Array of sample results</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "result.id_doehrs_sample_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "result.id_doehrs_result_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.chemical_name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "result.cas_number",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.concentration",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.units",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.analytical_method",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.found_meg",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.exceeds_1_year_neg",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.selected",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": " HTTP/1.1 200 OK\n [{\n   \"id\":30,\n   \"id_doehrs_sample_id\":\"0000KQ1T\",\n   \"id_doehrs_result_id\":1172,\n   \"chemical_name\":\"Antimony\",\n   \"concentration\":0.069584,\n   \"units\":\"ug/m3\",\n   \"cas_number\":\"7440-36-0\",\n   \"analytical_method\":\"EPA 200.8\",\n   \"found_meg\":true,\n   \"exceeds_1_year_neg\":\"No\",\n   \"selected\":0\n}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "HRA not found",
          "content": "HTTP/1.1 404 HRA not found",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/enhancedhra/router.js",
    "groupTitle": "Enhanced_HRA",
    "name": "GetApiEnhancedhraHraHraidResults"
  },
  {
    "type": "get",
    "url": "/api/enhancedhra/hra/:hraId/risk",
    "title": "Get Risk Tables",
    "description": "<p>Get the data for the peak and average tables. Only tables that pass prescreen are returned.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>URL Parameter. The JHRM ID of the HRA</p>"
          }
        ]
      }
    },
    "group": "Enhanced_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "table",
            "description": "<p>Map of peak and average calculations</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "row.acute_peak",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "row.acute_peak.severity",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "row.acute_peak.degreeOfExposure",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "row.acute_peak.durationOfExposure",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "row.acute_peak.representativenessOfData",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "row.acute_peak.rateOfExposure",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "row.acute_peak.meg",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "row.acute_average",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "row.acute_average.severity",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "row.acute_average.degreeOfExposure",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "row.acute_average.durationOfExposure",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "row.acute_average.representativenessOfData",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "row.acute_average.rateOfExposure",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "row.acute_average.meg",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "row.chronic_average",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "row.chronic_average.severity",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "row.chronic_average.degreeOfExposure",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "row.chronic_average.durationOfExposure",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "row.chronic_average.representativenessOfData",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "row.chronic_average.rateOfExposure",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "row.chronic_average.meg",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n\"acute_peak\":{\n    \"meg\":{\n        \"chemical_name\":\"Particulate matter < 2.5 µm\",\n        \"cas_number\":\"PM2.5\",\n        \"value\":{\n            \"value\":0.065,\n            \"isValid\":true,\n            \"originalValue\":0.065,\n            \"units\":\"mg/m3\",\n            \"precision\":5\n        },\n        \"basis\":\"RD-230 Sec. 4-5\",\n        \"severity\":\"Marginal\",\n        \"timeframe\":\"1year\",\n        \"minutes\":525600,\n        \"parm\":\"1yearMARG\",\n        \"isCWA\":false\n    },\n    \"severity\":{\n        \"value\":\"Marginal\"\n    },\n    \"degreeOfExposure\":{\n        \"value\":\"44% Range\",\n        \"score\":2\n    },\n    \"durationOfExposure\":{\n        \"value\":\"1day / 24hour\",\n        \"score\":3\n    }\n},\n\"acute_average\":{\n    \"meg\":{\n        \"chemical_name\":\"Particulate matter < 2.5 µm\",\n        \"cas_number\":\"PM2.5\",\n        \"value\":{\n            \"value\":0.065,\n            \"isValid\":true,\n            \"originalValue\":0.065,\n            \"units\":\"mg/m3\",\n            \"precision\":5\n        },\n        \"basis\":\"RD-230 Sec. 4-5\",\n        \"severity\":\"Marginal\",\n        \"timeframe\":\"1year\",\n        \"minutes\":525600,\n        \"parm\":\"1yearMARG\",\n        \"isCWA\":false\n    },\n    \"severity\":{\n        \"value\":\"Marginal\"\n    },\n    \"degreeOfExposure\":{\n        \"value\":\"15% Range\",\n        \"score\":1\n    },\n    \"durationOfExposure\":{\n        \"value\":\"1day / 24hour\",\n        \"score\":3\n    }\n},\n\"chronic_average\":{\n    \"durationOfExposure\":{\n        \"value\":\"9day / 1year\",\n        \"score\":1\n    },\n    \"severity\":{\n        \"value\":\"Negligible\",\n        \"score\":1\n    },\n    \"degreeOfExposure\":{\n        \"value\":\"> 0.015 mg/m3\",\n        \"score\":2\n    },\n    \"meg\":{\n        \"chemical_name\":\"Particulate matter < 2.5 µm\",\n        \"cas_number\":\"PM2.5\",\n        \"value\":{\n            \"value\":0.015,\n            \"isValid\":true,\n            \"originalValue\":0.015,\n            \"units\":\"mg/m3\",\n            \"precision\":5\n        },\n        \"basis\":\"RD-230 Sec. 4-5\",\n        \"severity\":\"Negligible\",\n        \"timeframe\":\"1year\",\n        \"minutes\":525600,\n        \"parm\":\"1yearNEG\",\n        \"isCWA\":false\n    }\n}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "HRA not found",
          "content": "HTTP/1.1 404 HRA not found",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/enhancedhra/router.js",
    "groupTitle": "Enhanced_HRA",
    "name": "GetApiEnhancedhraHraHraidRisk"
  },
  {
    "type": "get",
    "url": "/api/enhancedhra/hra/:hraId/samples",
    "title": "List Samples",
    "description": "<p>Get samples for an HRA based on the exposure pathway and optionally filter to show only selected samples</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>The JHRM ID of the HRA</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "selected",
            "description": "<p>Optional. Only return selected samples</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "start",
            "description": "<p>Optional. YYYY-MM-DD Filter the results by date.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "end",
            "description": "<p>Optional. YYY-MM-DD Filter the results by date</p>"
          }
        ]
      }
    },
    "group": "Enhanced_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "Sample[]",
            "description": "<p>Array of Samples</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "sample.id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sample.id_doehrs_sample_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "sample.chem_enhanced_hra_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sample.sampling_point",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "sample.start_date_time",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sample.exposure_notes",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "sample.sample_time",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "sample.approved_by_qa",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "sample.media",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "sample.selected",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "sample.analytical_method",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[{\n    \"id\":46,\n    \"id_doehrs_sample_id\":\"0000KQ1T\",\n    \"chem_enhanced_hra_id\":\"E-FFGTUSD1ZS\",\n    \"sampling_point\":\"Office Area\",\n    \"start_date_time\":\"2018-01-25T13:30:00.000Z\",\n    \"exposure_notes\":\"Continues air sampling\",\n    \"sample_time\":1440,\n    \"approved_by_qa\":1,\n    \"media\":\"AIR\",\n    \"selected\":0,\n    \"analytical_method\":\"EPA 200.8,GRAV\"\n}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "HRA not found",
          "content": "HTTP/1.1 404 HRA not found",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/enhancedhra/router.js",
    "groupTitle": "Enhanced_HRA",
    "name": "GetApiEnhancedhraHraHraidSamples"
  },
  {
    "type": "post",
    "url": "/api/enhancedhra/hra",
    "title": "Save HRA",
    "description": "<p>Save an HRA and return the ID. If an ID is not provided, a new HRA will be created. Otherwise the existing HRA will be updated.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "hra",
            "description": "<p>Health Risk Assessment JSON object</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hra.hra_objective",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hra.location",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": false,
            "field": "hra.latitude",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hra.sap_file_name",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "hra.id_doehrs_pathway_id",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hra.exposure_pathway_name",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hra.chemical_name",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hra.created_by",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hra.priority",
            "description": ""
          }
        ]
      }
    },
    "group": "Enhanced_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "response",
            "description": "<p>Response JSON Object</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.Status",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.id",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n    Status: \"Create Record Success\"\n    id: \"E-FFGTUSD1ZS\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Pathway Not Found",
          "content": "HTTP/1.1 404 Pathway Not Found",
          "type": "json"
        },
        {
          "title": "Access Denied",
          "content": "HTTP/1.1 403 Access Denied",
          "type": "json"
        },
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/enhancedhra/router.js",
    "groupTitle": "Enhanced_HRA",
    "name": "PostApiEnhancedhraHra"
  },
  {
    "type": "post",
    "url": "/api/enhancedhra/hra/:hraId/duplicate",
    "title": "Duplicat HRA",
    "description": "<p>Duplicate and Enhanced HRA, copying all user inputs, and set status to INCOMPLETE</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>The JHRM ID of the HRA</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "created_by",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "priority",
            "description": ""
          }
        ]
      }
    },
    "group": "Enhanced_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "Data",
            "description": "<p>{} JSON Data</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Status",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": \"Success\"\n    \"hra_id\": \"H-s554he5s\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "HRA not found",
          "content": "HTTP/1.1 404 HRA not found",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/enhancedhra/router.js",
    "groupTitle": "Enhanced_HRA",
    "name": "PostApiEnhancedhraHraHraidDuplicate"
  },
  {
    "type": "post",
    "url": "/api/enhancedhra/hra/:hraId/outcomes",
    "title": "Get potential health outcomes",
    "description": "<p>Get potential health outcomes from the RD230 reference. Search by CAS number and MEG.</p>",
    "group": "Enhanced_HRA",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>URL Parameter. The JHRM ID of the HRA</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "response",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "response.potential_health_outcomes_acute",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "response.potential_health_outcomes_chronic",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n    \"potential_health_outcomes_acute\":null,\n    \"potential_health_outcomes_chronic\":null\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "HRA not found",
          "content": "HTTP/1.1 404 HRA not found",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/enhancedhra/router.js",
    "groupTitle": "Enhanced_HRA",
    "name": "PostApiEnhancedhraHraHraidOutcomes"
  },
  {
    "type": "post",
    "url": "/api/enhancedhra/hra/:hraId/prescreen",
    "title": "Get PEPC",
    "description": "<p>Get the acute PEPC for the HRA</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>URL Parameter. The JHRM ID of the HRA</p>"
          }
        ]
      }
    },
    "group": "Enhanced_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "row[]",
            "description": "<p>Array of table rows</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "row.analyte_name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "DimensionedValue",
            "optional": false,
            "field": "row.peak_value",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "DimensionedValue",
            "optional": false,
            "field": "row.avg_value",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "DimensionedValue",
            "optional": false,
            "field": "row.chronic_meg",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "row.chronic_meg_exceeded",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "row.acute_meg",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "DimensionedValue",
            "optional": false,
            "field": "row.acute_meg_value",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "row.acute_meg_exceeed",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "row.chronic_meg_detection_frequency",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[\n{\n    \"analyte_name\":\"PARTICULATE <2.5M (PM-2.5)\",\n    \"peak_value\":{\n        \"value\":0.14613,\n        \"isValid\":true,\n        \"originalValue\":0.14613,\n        \"units\":\"mg/m3\",\n        \"precision\":7\n    },\n    \"avg_value\":{\n        \"value\":0.0918555,\n        \"isValid\":true,\n        \"originalValue\":0.0918555,\n        \"units\":\"mg/m3\",\n        \"precision\":9\n    },\n    \"chronic_meg_value\":{\n        \"value\":0.015,\n        \"isValid\":true,\n        \"originalValue\":0.015,\n        \"units\":\"mg/m3\",\n        \"precision\":5\n    },\n    \"chronic_meg\":\"1yearNEG\",\n    \"chronic_meg_detection_frequency\":100,\n    \"chronic_meg_exceeded\":\"Yes\",\n    \"acute_meg\":\"24hourNEG\",\n    \"acute_meg_value\":{\n        \"value\":0.065,\n        \"isValid\":true,\n        \"originalValue\":0.065,\n        \"units\":\"mg/m3\",\n        \"precision\":5\n    },\n    \"acute_meg_exceeded\":\"Yes\"\n}\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "HRA not found",
          "content": "HTTP/1.1 404 HRA not found",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/enhancedhra/router.js",
    "groupTitle": "Enhanced_HRA",
    "name": "PostApiEnhancedhraHraHraidPrescreen"
  },
  {
    "type": "post",
    "url": "/api/enhancedhra/hra/:hraId/report",
    "title": "Save report input",
    "description": "<p>Save user-provided values from summary report input</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>URL Parameter. The JHRM ID of the HRA</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "report",
            "description": "<p>JSON Body Parameter</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "report.title",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "report.potential_health_outcomes",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "report.recommended_mitigation",
            "description": ""
          }
        ]
      }
    },
    "group": "Enhanced_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "response",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "response.status",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\":\"success\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "HRA not found",
          "content": "HTTP/1.1 404 HRA not found",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/enhancedhra/router.js",
    "groupTitle": "Enhanced_HRA",
    "name": "PostApiEnhancedhraHraHraidReport"
  },
  {
    "type": "post",
    "url": "/api/enhancedhra/hra/:hraId/report",
    "title": "Generate PDF Report",
    "description": "<p>Generate PDF report for HRA</p>",
    "group": "Enhanced_HRA",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>URL Parameter. The JHRM ID of the HRA</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "PDF",
            "optional": false,
            "field": "Report",
            "description": ""
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "HRA not found",
          "content": "HTTP/1.1 404 HRA not found",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/enhancedhra/router.js",
    "groupTitle": "Enhanced_HRA",
    "name": "PostApiEnhancedhraHraHraidReport"
  },
  {
    "type": "post",
    "url": "/api/enhancedhra/hra/:hraId/results",
    "title": "Associate Sample Results",
    "description": "<p>Add sample results to an existing HRA</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>URL Parameter. The JHRM ID of the HRA that is being edited</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "request",
            "description": "<p>JSON Body Parameter</p>"
          },
          {
            "group": "Parameter",
            "type": "number[]",
            "optional": false,
            "field": "object.resultIds",
            "description": "<p>The IDs of the results to add</p>"
          }
        ]
      }
    },
    "group": "Enhanced_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "response",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "response.status",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\":\"success\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "HRA not found",
          "content": "HTTP/1.1 404 HRA not found",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/enhancedhra/router.js",
    "groupTitle": "Enhanced_HRA",
    "name": "PostApiEnhancedhraHraHraidResults"
  },
  {
    "type": "post",
    "url": "/api/enhancedhra/hra/:hraId/risk",
    "title": "Save Risk Tables",
    "description": "<p>Save user-provided values from peak and average tables and return a summary</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>URL Parameter. The JHRM ID of the HRA</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "table",
            "description": "<p>Map of peak and average calculations</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "row.acute_peak",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "row.acute_peak.severity",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "row.acute_peak.degreeOfExposure",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "row.acute_peak.durationOfExposure",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "row.acute_peak.representativenessOfData",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "row.acute_peak.rateOfExposure",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "row.acute_peak.meg",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "row.acute_average",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "row.acute_average.severity",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "row.acute_average.degreeOfExposure",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "row.acute_average.durationOfExposure",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "row.acute_average.representativenessOfData",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "row.acute_average.rateOfExposure",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "row.acute_average.meg",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "row.chronic_average",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "row.chronic_average.severity",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "row.chronic_average.degreeOfExposure",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "row.chronic_average.durationOfExposure",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "row.chronic_average.representativenessOfData",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "row.chronic_average.rateOfExposure",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "row.chronic_average.meg",
            "description": ""
          }
        ]
      }
    },
    "group": "Enhanced_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "response",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "response.status",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\":\"success\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "HRA not found",
          "content": "HTTP/1.1 404 HRA not found",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/enhancedhra/router.js",
    "groupTitle": "Enhanced_HRA",
    "name": "PostApiEnhancedhraHraHraidRisk"
  },
  {
    "type": "post",
    "url": "/api/enhancedhra/hra/:hraId/samples",
    "title": "Associate Samples",
    "description": "<p>Add samples to an existing HRA</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "hraId",
            "description": "<p>URL Parameter. The JHRM ID of the HRA that is being edited</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "request",
            "description": "<p>JSON Body Parameter</p>"
          },
          {
            "group": "Parameter",
            "type": "number[]",
            "optional": false,
            "field": "object.sampleIds",
            "description": "<p>The IDs of the results to add</p>"
          }
        ]
      }
    },
    "group": "Enhanced_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "response",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "response.status",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\":\"success\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "HRA not found",
          "content": "HTTP/1.1 404 HRA not found",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/enhancedhra/router.js",
    "groupTitle": "Enhanced_HRA",
    "name": "PostApiEnhancedhraHraHraidSamples"
  },
  {
    "type": "get",
    "url": "/api/metadata/sensorarray/:arrayId/:sensorSn/deletesensor",
    "title": "Get sensors from an Array profile",
    "description": "<p>Get a new sensor from the Sensor Array Profile.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "alpha-number",
            "optional": false,
            "field": "sensor_metadata_id",
            "description": "<p>URL Parameter. The sensor array ID of the sensor array profile</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "serial_number",
            "description": "<p>URL Parameter. The serial number of the sensor array</p>"
          }
        ]
      }
    },
    "group": "Get_Selected_MetaData_Sensor_Row",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "Response",
            "description": "<p>JSON Array Object</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.sensor_metadata_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.sensor_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.sensor_type",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.serial_number",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.software_version",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.hardware_version",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.mfr_date",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.sensor_mgrs_location",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.sensor_location_description",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.sensor_employment_type",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n    \"sensor_metadata_id\": \"M-8RY8OF76KG\",\n    \"Sensor_id\" : \"JCAD13\",\n    \"Sensor_type\" : \"JCAD\",\n    \"Serial_number\" : \"JCAD1o1133\",\n    \"Software_version\" : \"12.2\",\n    \"Hardware_version\" : \"3\",\n    \"MFR_Date\" : \"1/1/2020\",\n    \"Sensor_mgrs_location\" : \"38SMC1661906536\",\n    \"Sensor_location_description\" : \"All by the back entrance\",\n    \"Sensor_employment_type\" : \"Datalog\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Sensor Not Found",
          "content": "HTTP/1.1 404 Sensor Not Found",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/metadata/router.js",
    "groupTitle": "Get_Selected_MetaData_Sensor_Row",
    "name": "GetApiMetadataSensorarrayArrayidSensorsnDeletesensor"
  },
  {
    "type": "get",
    "url": "/api/ifa/downloadfile/:ifa_incident_id",
    "title": "",
    "description": "<p>Get the file from the database and display it in the browser</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "ifa_incident_id",
            "optional": false,
            "field": "ifa",
            "description": "<p>Id URL Parameter. The JHRM ID of the IFA</p>"
          }
        ]
      }
    },
    "group": "IFA",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "Object Display in Browser",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Bad Request",
          "content": "HTTP/1.1 400 Bad Request.",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/ifa/router.js",
    "groupTitle": "IFA",
    "name": "GetApiIfaDownloadfileIfa_incident_id"
  },
  {
    "type": "get",
    "url": "/api/ifa/ifafileattachments/:ifa_incident_id",
    "title": "",
    "description": "<p>Get a list of files for the ifa incident from the database</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": ":ifa_incident_id",
            "optional": false,
            "field": "ifa",
            "description": "<p>Id URL Parameter. The JHRM ID of the IFA</p>"
          }
        ]
      }
    },
    "group": "IFA",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "\t\tHTTP/1.1 200 OK\n\t\t[{\n            \"ifa_incident_id\": 1,\n            \"file_name\": \"name_of_file.jpg\",\n            \"attachment_type\": \"MEDSITREP\",\n            \"file_type\": \"image/jpeg\"\n\t\t}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Bad Request",
          "content": "HTTP/1.1 400 Assessment missing required field.",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/ifa/router.js",
    "groupTitle": "IFA",
    "name": "GetApiIfaIfafileattachmentsIfa_incident_id"
  },
  {
    "type": "get",
    "url": "/api/ifa/reports",
    "title": "List IFA Reports",
    "description": "<p>List all IFA Reports</p>",
    "group": "IFA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "report[]",
            "description": "<p>Array of IFA Reports</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "report.id",
            "description": "<p>Unique JHRM ID of IFA</p>"
          },
          {
            "group": "Success 200",
            "type": "IfaPreparer[]",
            "optional": false,
            "field": "report.preparers",
            "description": "<p>Array of IFA preparers</p>"
          },
          {
            "group": "Success 200",
            "type": "IFA_STATUS",
            "optional": false,
            "field": "report.status",
            "description": "<p>One of ()</p>"
          },
          {
            "group": "Success 200",
            "type": "IFA_SUBMISSION_STATUS",
            "optional": false,
            "field": "report.submission_status",
            "description": "<p>Deprecated One of ()</p>"
          },
          {
            "group": "Success 200",
            "type": "IFA_QA_STATUS",
            "optional": false,
            "field": "report.qa_status",
            "description": "<p>One of ()</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "report.report_start_datetime",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "report.report_start_datetime.date",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "report.report_completion_datetime",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "report.report_completion_datetime.date:",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.short_name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "report.start_datetime",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "report.start_datetime.date",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "report.end_datetime",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "report.end_datetime.date",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.ccir_sigact_num",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "IFA_INCIDENT_SCENARIO",
            "optional": false,
            "field": "report.scenario",
            "description": "<p>One of ()</p>"
          },
          {
            "group": "Success 200",
            "type": "IFA_INCIDENT_TYPE[]",
            "optional": false,
            "field": "report.incident_type",
            "description": "<p>One of ()</p>"
          },
          {
            "group": "Success 200",
            "type": "IFA_INCIDENT_CAUSE",
            "optional": false,
            "field": "report.cause",
            "description": "<p>One of (&quot;UNKNOWN&quot;)</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.cause_descr",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.summary",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Location",
            "optional": false,
            "field": "report.location",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "report.location.lon",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "report.location.lat",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.location.mgrs",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "report.map_image_available",
            "description": "<p>false</p>"
          },
          {
            "group": "Success 200",
            "type": "IFA_TRINARY",
            "optional": false,
            "field": "report.pers_assoc_to_incident",
            "description": "<p>One of &quot;YES&quot;,&quot;NO,&quot;UNKNOWN&quot;</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.pers_assoc_to_incident_descr",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "IFA_TRINARY",
            "optional": false,
            "field": "report.signs_symptoms_reported",
            "description": "<p>One of &quot;YES&quot;,&quot;NO,&quot;UNKNOWN&quot;</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.signs_symptoms_reported_descr",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.units_involved",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "report.were_you_at_incident_location",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "\t\tHTTP/1.1 200 OK\n\t\t[{\n           \"preparers\": [\n               {\n                   \"PortableType\": \"IfaPreparer\",\n                   \"id\": 1,\n                   \"ifa_incident_id\": 1,\n                   \"type\": \"INITIAL\",\n                   \"name\": \"Kyrstin Stinson\",\n                   \"email\": \"kstinson0@princeton.edu\",\n                   \"phone_num\": \"716-657-7582\",\n                   \"unit\": \"in hac habitasse\"\n               }\n           ],\n           \"status\": \"INCOMPLETE\",\n           \"submission_status\": \"SUBMITTED\",\n           \"qa_status\": \"READY_FOR_QA\",\n           \"report_start_datetime\": {\n               \"PortableType\": \"Datetime\",\n               \"date\": \"2019-09-28T14:02:44.000Z\"\n           },\n           \"id\": 1,\n           \"report_completion_datetime\": {\n               \"PortableType\": \"Datetime\",\n               \"date\": \"2020-09-02T09:29:36.400Z\"\n           },\n           \"name\": \"quam a odio in\",\n           \"short_name\": \"nisi eu orci\",\n           \"start_datetime\": {\n               \"PortableType\": \"Datetime\",\n               \"date\": \"2020-02-18T19:15:42.000Z\"\n           },\n           \"end_datetime\": {\n               \"PortableType\": \"Datetime\",\n               \"date\": \"2019-10-05T15:27:34.000Z\"\n           },\n           \"ccir_sigact_num\": null,\n           \"scenario\": \"TRAINING\",\n           \"incident_type\": [\n               \"CONCUSSIVE\"\n           ],\n           \"cause\": \"UNKNOWN\",\n           \"cause_descr\": null,\n           \"summary\": \"Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.\",\n           \"location\": {\n               \"PortableType\": \"Location\",\n               \"lon\": 13.0299,\n               \"lat\": 59.2669,\n               \"mgrs\": \"33VUF8770171432\"\n           },\n           \"map_image_available\": false,\n           \"pers_assoc_to_incident\": \"UNKNOWN\",\n           \"pers_assoc_to_incident_descr\": null,\n           \"signs_symptoms_reported\": \"NO\",\n           \"signs_symptoms_reported_descr\": null,\n           \"units_involved\": \"metus\",\n           \"were_you_at_incident_location\": 0,\n           \"PortableType\": \"IfaReport\"\n\t\t}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Error Error",
          "content": "Undefined location cannot read property",
          "type": "none"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/ifa/router.js",
    "groupTitle": "IFA",
    "name": "GetApiIfaReports"
  },
  {
    "type": "get",
    "url": "/api/ifa/reports",
    "title": "Get IFA Report",
    "description": "<p>Get a single IFA report by ID</p>",
    "group": "IFA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "report[]",
            "description": "<p>IFA Report</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "report.id",
            "description": "<p>Unique JHRM ID of IFA</p>"
          },
          {
            "group": "Success 200",
            "type": "IfaPreparer[]",
            "optional": false,
            "field": "report.preparers",
            "description": "<p>Array of IFA preparers</p>"
          },
          {
            "group": "Success 200",
            "type": "IFA_STATUS",
            "optional": false,
            "field": "report.status",
            "description": "<p>One of ()</p>"
          },
          {
            "group": "Success 200",
            "type": "IFA_SUBMISSION_STATUS",
            "optional": false,
            "field": "report.submission_status",
            "description": "<p>Deprecated One of ()</p>"
          },
          {
            "group": "Success 200",
            "type": "IFA_QA_STATUS",
            "optional": false,
            "field": "report.qa_status",
            "description": "<p>One of ()</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "report.report_start_datetime",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "report.report_start_datetime.date",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "report.report_completion_datetime",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "report.report_completion_datetime.date:",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.short_name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "report.start_datetime",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "report.start_datetime.date",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "report.end_datetime",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "report.end_datetime.date",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.ccir_sigact_num",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "IFA_INCIDENT_SCENARIO",
            "optional": false,
            "field": "report.scenario",
            "description": "<p>One of ()</p>"
          },
          {
            "group": "Success 200",
            "type": "IFA_INCIDENT_TYPE[]",
            "optional": false,
            "field": "report.incident_type",
            "description": "<p>One of ()</p>"
          },
          {
            "group": "Success 200",
            "type": "IFA_INCIDENT_CAUSE",
            "optional": false,
            "field": "report.cause",
            "description": "<p>One of (&quot;UNKNOWN&quot;)</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.cause_descr",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.summary",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Location",
            "optional": false,
            "field": "report.location",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "report.location.lon",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "report.location.lat",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.location.mgrs",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "report.map_image_available",
            "description": "<p>false</p>"
          },
          {
            "group": "Success 200",
            "type": "IFA_TRINARY",
            "optional": false,
            "field": "report.pers_assoc_to_incident",
            "description": "<p>One of &quot;YES&quot;,&quot;NO,&quot;UNKNOWN&quot;</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.pers_assoc_to_incident_descr",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "IFA_TRINARY",
            "optional": false,
            "field": "report.signs_symptoms_reported",
            "description": "<p>One of &quot;YES&quot;,&quot;NO,&quot;UNKNOWN&quot;</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.signs_symptoms_reported_descr",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report.units_involved",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "report.were_you_at_incident_location",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "\t\tHTTP/1.1 200 OK\n\t\t{\n           \"preparers\": [\n               {\n                   \"PortableType\": \"IfaPreparer\",\n                   \"id\": 1,\n                   \"ifa_incident_id\": 1,\n                   \"type\": \"INITIAL\",\n                   \"name\": \"Kyrstin Stinson\",\n                   \"email\": \"kstinson0@princeton.edu\",\n                   \"phone_num\": \"716-657-7582\",\n                   \"unit\": \"in hac habitasse\"\n               }\n           ],\n           \"status\": \"INCOMPLETE\",\n           \"submission_status\": \"SUBMITTED\",\n           \"qa_status\": \"READY_FOR_QA\",\n           \"report_start_datetime\": {\n               \"PortableType\": \"Datetime\",\n               \"date\": \"2019-09-28T14:02:44.000Z\"\n           },\n           \"id\": 1,\n           \"report_completion_datetime\": {\n               \"PortableType\": \"Datetime\",\n               \"date\": \"2020-09-02T09:29:36.400Z\"\n           },\n           \"name\": \"quam a odio in\",\n           \"short_name\": \"nisi eu orci\",\n           \"start_datetime\": {\n               \"PortableType\": \"Datetime\",\n               \"date\": \"2020-02-18T19:15:42.000Z\"\n           },\n           \"end_datetime\": {\n               \"PortableType\": \"Datetime\",\n               \"date\": \"2019-10-05T15:27:34.000Z\"\n           },\n           \"ccir_sigact_num\": null,\n           \"scenario\": \"TRAINING\",\n           \"incident_type\": [\n               \"CONCUSSIVE\"\n           ],\n           \"cause\": \"UNKNOWN\",\n           \"cause_descr\": null,\n           \"summary\": \"Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.\",\n           \"location\": {\n               \"PortableType\": \"Location\",\n               \"lon\": 13.0299,\n               \"lat\": 59.2669,\n               \"mgrs\": \"33VUF8770171432\"\n           },\n           \"map_image_available\": false,\n           \"pers_assoc_to_incident\": \"UNKNOWN\",\n           \"pers_assoc_to_incident_descr\": null,\n           \"signs_symptoms_reported\": \"NO\",\n           \"signs_symptoms_reported_descr\": null,\n           \"units_involved\": \"metus\",\n           \"were_you_at_incident_location\": 0,\n           \"PortableType\": \"IfaReport\"\n\t\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Error Error",
          "content": "Undefined location cannot read property",
          "type": "none"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/ifa/router.js",
    "groupTitle": "IFA",
    "name": "GetApiIfaReports"
  },
  {
    "type": "get",
    "url": "/api/ifa/reports",
    "title": "Fetch IFA associated Personnel",
    "description": "<p>Get associated personnel for an IFA report</p>",
    "group": "IFA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "report[]",
            "description": "<p>IFA Report</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "report.id",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[{\n}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Error Error",
          "content": "Undefined location cannot read property",
          "type": "none"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/ifa/router.js",
    "groupTitle": "IFA",
    "name": "GetApiIfaReports"
  },
  {
    "type": "post",
    "url": "/api/ifa/report",
    "title": "Create IFA",
    "description": "<p>Save a new IFA report</p>",
    "group": "IFA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "report[]",
            "description": "<p>Array of IFA Reports</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "report.id",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[{\n}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Error Error",
          "content": "Undefined location cannot read property",
          "type": "none"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/ifa/router.js",
    "groupTitle": "IFA",
    "name": "PostApiIfaReport"
  },
  {
    "type": "post",
    "url": "/api/ifa/report/:id",
    "title": "Save IFA Report",
    "description": "<p>Save an IFA that already exists in the system.</p>",
    "group": "IFA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "report[]",
            "description": "<p>IFA Report</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "report.id",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[{\n}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Error Error",
          "content": "Undefined location cannot read property",
          "type": "none"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/ifa/router.js",
    "groupTitle": "IFA",
    "name": "PostApiIfaReportId"
  },
  {
    "type": "post",
    "url": "/api/ifa/reports",
    "title": "Save IFA associated Personnel",
    "description": "<p>Save associated personnel for an IFA report</p>",
    "group": "IFA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "report[]",
            "description": "<p>IFA Report</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "report.id",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[{\n}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Error Error",
          "content": "Undefined location cannot read property",
          "type": "none"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/ifa/router.js",
    "groupTitle": "IFA",
    "name": "PostApiIfaReports"
  },
  {
    "type": "post",
    "url": "/api/ifa/uploadfile",
    "title": "",
    "description": "<p>Upload files (*.pdf, *.jpg, *.xlsx, *.docx) to the database</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "ifa_incident_id",
            "optional": false,
            "field": "ifa",
            "description": "<p>Id URL Parameter. The JHRM ID of the IFA</p>"
          }
        ]
      }
    },
    "group": "IFA",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 201 CREATED\n{\n    status: 'Successfully Uploaded File'\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Bad Request",
          "content": "HTTP/1.1 400 Assessment missing required field.",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/ifa/router.js",
    "groupTitle": "IFA",
    "name": "PostApiIfaUploadfile"
  },
  {
    "type": "get",
    "url": "/report/:hraId/localdrihraslist",
    "title": "Get the list of DRI HRAs IDs from the ifa_chemical_detector_sensor_data",
    "description": "<p>List all samples</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": ":hraId",
            "description": "<p>URL Parameter. The JHRM ID of the HRA</p>"
          }
        ]
      }
    },
    "group": "IFA_DRI_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "localreferencedrihras[]",
            "description": "<p>Array of samples</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "localreferencedrihras.ifa_associated_personnel_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "localreferencedrihras.chem_rapid_hra_id",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n  [{\n       \"ifa_associated_personnel_id\": 1,\n       \"chem_rapid_hra_id\": \"R-69QC7LSH5I\"\n   },\n   {\n       \"ifa_associated_personnel_id\": 1,\n       \"chem_rapid_hra_id\": \"R-ZGNHWT5SJI\"\n  }]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "HRA not found",
          "content": "HTTP/1.1 404 HRA not found",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/ifa/router.js",
    "groupTitle": "IFA_DRI_HRA",
    "name": "GetReportHraidLocaldrihraslist"
  },
  {
    "type": "get",
    "url": "/report/:id/drihrasids",
    "title": "Get the list of HRA DRIs from the location specified by the IFA incident information location",
    "description": "<p>List all samples</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": ":ifaId",
            "description": "<p>URL Parameter. The JHRM ID of the IFA</p>"
          }
        ]
      }
    },
    "group": "IFA_DRI_HRA",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "driHRAsIds[]",
            "description": "<p>Array of samples</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "driHRAsIds.chem_rapid_hra_id",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n  [{\n      \"chem_rapid_hra_id\": \"R-69QC7LSH5I\"\n  },\n  {\n      \"chem_rapid_hra_id\": \"R-ZGNHWT5SJI\"\n  }]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "HRA not found",
          "content": "HTTP/1.1 404 HRA not found",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/ifa/router.js",
    "groupTitle": "IFA_DRI_HRA",
    "name": "GetReportIdDrihrasids"
  },
  {
    "type": "get",
    "url": "/api/metadata/sensorarray/:arrayId/pathway",
    "title": "Get exposure pathway",
    "description": "<p>Get the exposure pathway associated with the current sensor array.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "AlphaNumeric",
            "optional": false,
            "field": "Sensor",
            "description": "<p>Array ID URL Parameter. The Sensor Array ID of the Array Profile</p>"
          }
        ]
      }
    },
    "group": "MetaData_Sensor_Array_Profile",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "pathway",
            "description": "<p>Exposure Pathway</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.location",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.threat_source",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.health_hazard",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.exposure_point_area",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.exposure_medium",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.exposure_route",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.exposure_duration_of_concern",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.exposed_population",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[{\n    id : \"value\",\n    location : \"value\",\n    name : \"value\",\n    threat_source : \"value\",\n    health_hazard : \"value\",\n    exposure_point_area : \"value\",\n    exposure_medium : \"value\",\n    exposure_route : \"value\",\n    exposure_duration_of_concern : \"value\",\n    exposed_population : \"value\"\n}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Sensor Array Not Found",
          "content": "HTTP/1.1 404 Sensor Array Not Found",
          "type": "json"
        },
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/metadata/router.js",
    "groupTitle": "MetaData_Sensor_Array_Profile",
    "name": "GetApiMetadataSensorarrayArrayidPathway"
  },
  {
    "type": "post",
    "url": "/api/metadata/sensorarray/:arrayId/pathway",
    "title": "Create exposure pathway for sensor array profile",
    "description": "<p>Create a new exposure pathway and associate with the current Sensor Array Profile.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "sensor_array_id",
            "description": "<p>URL Parameter. The sensor array ID of the sensor array profile</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "pathway",
            "description": "<p>Exposure Pathway</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pathway.location",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pathway.name",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pathway.threat_source",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pathway.health_hazard",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pathway.exposure_point_area",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pathway.exposure_medium",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pathway.exposure_route",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pathway.exposure_duration_of_concern",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pathway.exposed_population",
            "description": ""
          }
        ]
      }
    },
    "group": "MetaData_Sensor_Array_Profile",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "response",
            "description": "<p>Response JSON Object</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.Status",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.id",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 201 CREATED\n{\n    Status: \"Create Record Success\"\n    id: \"SAP-FFGTUSD1ZS\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Sensor Array Not Found",
          "content": "HTTP/1.1 404 Sensor Array Not Found",
          "type": "json"
        },
        {
          "title": "Access Denied",
          "content": "HTTP/1.1 403 Not allowed to edit complete process.",
          "type": "json"
        },
        {
          "title": "Invalid Request",
          "content": "HTTP/1.1 401 The parameter \"name\" is required.",
          "type": "json"
        },
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/metadata/router.js",
    "groupTitle": "MetaData_Sensor_Array_Profile",
    "name": "PostApiMetadataSensorarrayArrayidPathway"
  },
  {
    "type": "post",
    "url": "/api/metadata/sensorarray/:arrayId/sensordetails",
    "title": "Add sensors to an Array profile",
    "description": "<p>Add a new sensor to the current Sensor Array Profile.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "sensor_array_id",
            "description": "<p>URL Parameter. The sensor array ID of the sensor array profile</p>"
          }
        ]
      }
    },
    "group": "MetaData_Sensor_Details",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "Response",
            "description": "<p>JSON Array Object</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.sensor_metadata_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.sensor_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.sensor_type",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.serial_number",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.software_version",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.hardware_version",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.mfr_date",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.sensor_mgrs_location",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.sensor_location_description",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.sensor_employment_type",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 201 CREATED\n{\n    Status: \"Successfully added to the sensor array!\"\n    id: \"SAP-FFGTUSD1ZS\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Sensor Not Found",
          "content": "HTTP/1.1 404 Sensor Not Found",
          "type": "json"
        },
        {
          "title": "Access Denied",
          "content": "HTTP/1.1 403 Not allowed to edit complete process.",
          "type": "json"
        },
        {
          "title": "Invalid Request",
          "content": "HTTP/1.1 401 The parameter \"name\" is required.",
          "type": "json"
        },
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/metadata/router.js",
    "groupTitle": "MetaData_Sensor_Details",
    "name": "PostApiMetadataSensorarrayArrayidSensordetails"
  },
  {
    "type": "get",
    "url": "/api/rapidhra/meta/:arrayId/pathway",
    "title": "MetaData Pathways",
    "description": "<p>Return exposure pathways by arrayId.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Meta",
            "description": "<p>Array Id Pathway</p>"
          }
        ]
      }
    },
    "group": "Meta_Array_Pathway_Data",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "pathway",
            "description": "<p>JSON Meta Array Pathway</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.sensor_array_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.location",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.threa_source",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.health_hazard",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.exposure_point_area",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.exposure_medium",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.exposure_route",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.exposure_duration_of_concern",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pathway.exposed_population",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n    \"id\":SEP-5OH096JR7M,\n    \"sensor_array_id : M-111112\"\n    \"location : Base Camp TAJI\"\n    \"name\":\"PM2.5 AMBIENT AIR\",\n    \"threat_source\":\"Burn Pit\",\n    \"health_hazard\":\"Polycyclic Aromatic Hydrocarbons (PAHs)\",\n    \"exposure_point_area\":\"\",\n    \"exposure_medium\":\"\",\n    \"exposure_route\":\"Inhalation\",\n    \"exposure_duration_of_concern\":\"24 Hours\",\n    \"exposed_population\":\"99\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Invalid parameter",
          "content": "HTTP/1.1 422 Invalid parameter",
          "type": "json"
        },
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/rapidhra/router.js",
    "groupTitle": "Meta_Array_Pathway_Data",
    "name": "GetApiRapidhraMetaArrayidPathway"
  },
  {
    "type": "get",
    "url": "/api/metadata/sensorarray/:arrayID/sensordetails",
    "title": "sensor details",
    "description": "<p>List all Sensors in an Array</p>",
    "group": "Sensor_Details",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "Response",
            "description": "<p>JSON Array Object</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.sensor_metadata_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.sensor_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.sensor_type",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.serial_number",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.software_version",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.hardware_version",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.mfr_date",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.sensor_mgrs_location",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.sensor_location_description",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.sensor_employment_type",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "   HTTP/1.1 200 OK\n   {\n       \"sensor_metadata_id\": \"M-8RY8OF76KG\",\n       \"Sensor_id\" : \"JCAD13\",\n       \"Sensor_type\" : \"JCAD\",\n       \"Serial_number\" : \"JCAD1o1133\",\n       \"Software_version\" : \"12.2\",\n       \"Hardware_version\" : \"3\",\n       \"MFR_Date\" : \"1/1/2020\",\n       \"Sensor_mgrs_location\" : \"38SMC1661906536\",\n       \"Sensor_location_description\" : \"All by the back entrance\",\n       \"Sensor_employment_type\" : \"null\"\n   }\n/* @apiErrorExample Internal Server Error\n   HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/metadata/router.js",
    "groupTitle": "Sensor_Details",
    "name": "GetApiMetadataSensorarrayArrayidSensordetails"
  },
  {
    "type": "get",
    "url": "/api/metadata/",
    "title": "get user",
    "group": "metadata",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "response",
            "description": "<p>Response JSON Object</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.Status",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.id",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n    \"id\": 1,\n    \"full_name\": \"John Smith\",\n    \"email\": \"jsmith@domenix.com\",\n    \"commercial_phone_number\": \"555-555-1234\",\n    \"dsn_phone_number\": \"555-323-2461\",\n    \"unit\": \"Team Bravo\",\n    \"last_updated\": \"7/22/2020\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/metadata/router.js",
    "groupTitle": "metadata",
    "name": "GetApiMetadata"
  },
  {
    "type": "get",
    "url": "/api/metadata/",
    "title": "post user",
    "group": "metadata",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "response",
            "description": "<p>Response JSON Object</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.Status",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "response.id",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n    \"id\": 1,\n    \"full_name\": \"Joe Smith\",\n    \"email\": \"jsmith@domenix.com\",\n    \"commercial_phone_number\": \"555-555-1234\",\n    \"dsn_phone_number\": \"555-323-2461\",\n    \"unit\": \"Team Bravo\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Internal Server Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/api/metadata/router.js",
    "groupTitle": "metadata",
    "name": "GetApiMetadata"
  },
  {
    "type": "get",
    "url": "/api/rd230/",
    "title": "Get All rd230",
    "description": "<p>Get all rd230 records in the system</p>",
    "group": "rd230",
    "version": "0.0.0",
    "filename": "src/api/rd230_meg/router.js",
    "groupTitle": "rd230",
    "name": "GetApiRd230"
  },
  {
    "type": "get",
    "url": "/api/tg230/",
    "title": "Get All tg230",
    "description": "<p>Get all tg230 records in the system</p>",
    "group": "tg230",
    "version": "0.0.0",
    "filename": "src/api/tg230_meg/router.js",
    "groupTitle": "tg230",
    "name": "GetApiTg230"
  }
] });
