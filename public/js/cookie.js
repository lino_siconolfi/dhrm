'use strict';

/**
 * Class that interacts with retrieval and storage of a cookie and its string value.
 */
export class Cookie
{
	/**
	 * Constructs a cookie.  If a cookie data for the key already exists on the client, it is populated
	 * with the value of the cookie.
	 * 
	 * @param {*} pKey 
	 */
	constructor(key)
	{
		if (Cookie.cookieMap === undefined)
		{
			Cookie.cookieMap = new Object();
			let cookies = document.cookie.split("; ");

			cookies.forEach(element => {
				let KeyVal = element.split("=");
				Cookie.cookieMap[ KeyVal[0] ] = decodeURIComponent(KeyVal[1]);
			});
		}

		this._key = key;
	}

	/**
	 * Sets the value of the cookie and stores it on the client.
	 * 
	 * @param {*} value 
	 */
	setValue(value)
	{
		document.cookie = this._key + "=" + encodeURIComponent(value) + "; path=/";
		Cookie.cookieMap[ this._key ] = value;
	}

	/**
	 * Returns the value if it is been set, or undefined if it is not. 
	 */
	getValue()
	{
		return Cookie.cookieMap[ this._key ];
	}

	/**
	 * Deletes the cookie from the user's browser.
	 */
	delete()
	{
		document.cookie = this._key + "=x; path=/; " + "max-age=-1;";
		Cookie.cookieMap[ this._key ] = undefined;
	}
};