import { Location } from "./shared/location.js";
import { validDodid } from "./shared/validDodid.js";
import { JhrmRadioGroup } from "./radio_group.js"

/**
 * This class allows the creation of custom form validators. 
 * 
 * Define validators on the form elements directly using data attributes:
 * 
 * <input type="text" data-validate-required data-validate-minlen="5">
 * 
 * @property {boolean} valid Indicates the current status of the form
 * @property {string[]} errors Aray of all error messages. 
 */
export class FormValidator
{
    /**
     * Constructor
     * 
     * @param {DomElement} container The container element to search for form 
     *     elements to be validated. 
     * @param {Object} options Options to define how the validator works. 
     * @param {String} options.inputSelector The string value to be passed to 
     *     container.querySelector() to select the form input elements. By 
     *     default this will match all input and textarea elements. To match 
     *     other inputs or custom inputs, you can pass a different value. Only 
     *     inputs that match this selector will be validated.  
     * @param {String} options.errorClass The class to add to elements which do 
     *     not pass validation. This allows them to be styled in a different way
     *     to highlight them to the user. If this is set to an empty string, 
     *     no classes will be added. 
     * @param {String} options.defaultLabel The validator will attempt to find
     *     the form label which matches an input element in ordr to make a more
     *     friendly and useful error message. But if no label is found, this 
     *     value will be used instead. 
     */
    constructor(container, options)
    {
        this._container = container;
        this._options = Object.assign({
            inputSelector : 'input, textarea',
            errorClass : 'validation-error',
            defaultLabel : 'Field'
        }, options);
        this.valid = undefined;
        this.errors = [];
    }

    /**
     * Perform validation. 
     * 
     * @returns {Boolean} Indicating whether validation passed. 
     */
    validate()
    {
        let inputs = [...this._container.querySelectorAll(this._options.inputSelector)];
        if(inputs.length===0)
        {
            return this._success();
        }
        let messages = this._run(inputs);
        return messages.length ? this._fail(messages) : this._success();
    }

    /**
     * Internal 
     * 
     * @param {DomElement[]} inputs 
     */
    _run(inputs)
    {
        // Loop through each input
        let result = inputs.map(el=>{

            // If there is a class defined, remove it before running validators
            if(this._options.errorClass)
            {
                el.classList.remove(this._options.errorClass);
            }

            // Special handling for JhrmRadioGroup whcih does not implement a
            // standard form element interface. We take the value and assign it
            // to the container for validation. 
            let radio = el.querySelector("input[type=radio]"); 
            if(el.tagName == 'DIV' && radio)
            {
                // Verify that this is a JhrmRadioGroup
                let group = JhrmRadioGroup._byName[radio.name];
                if(group)
                {
                    // Assume the value is an empty string unless changed below
                    el.value = '';

                    // We can't use group.Value without this check because it 
                    // can return the first option even if the UI shows no 
                    // selection. 
                    let selected = Array
                        .from(el.querySelectorAll(`input[name=${radio.name}]`))
                        .find(el=>el.checked);
                    if(selected)
                    {
                        el.value = group.Value;
                    }
                }
            }

            // This catches the case where if no data elements are defined, 
            // the property can be undefined. 
            return (el.dataset ? Object.keys(el.dataset) : [])

            // Do not bother if there is not a validation element. 
            .filter(key=>key.indexOf('validate')===0)

            // Loop through each defined validator. 
            .map(key=>{

                // We only continue if we find a matching validator. 
                let validator = FormValidator.validators.find(item=>item.tag==key);
                if(validator)
                {
                    // Run the validator with the validator parameter, element 
                    // value, and element as parameters. Only continue if the
                    // validation fails. 
                    if(!validator.run(el.dataset[validator.tag], el.value, el))
                    {
                        // Only add the error class if one is defined. 
                        if(this._options.errorClass)
                        {
                            el.classList.add(this._options.errorClass);
                        }

                        // See if we can find a label for this form element. 
                        let label = el.id ? document.querySelector(`label[for=${el.id}]`) : el.parentNode.querySelector('label');

                        // Define replacement text for the error message. 
                        let replacements = {
                            label : label ? label.childNodes[0].nodeValue.trim() : this._options.defaultLabel,
                            param : el.dataset[validator.tag]
                        }

                        // Get error message. 
                        return validator.message.replace(/{(.*?)}/g,  (_, key) => {
                            return replacements && replacements[key] || _;
                        });
                    }
                }
            }).filter(result=>!!result);
        })

        // Flatten array - this will contain all the validation errors, or an 
        // empty array if there were none. 
        return [].concat(...result);
    }

    /**
     * Internal  
     * 
     * Set the fail state. 
     * 
     * @param {String[]} messages 
     */
    _fail(messages)
    {
        this.errors = messages;
        this.valid = false;
        return this.valid;
    }

    /**
     * Internal 
     * 
     * Set the success state. 
     */
    _success()
    {
        this.errors = [];
        this.valid = true;
        return this.valid;
    }

    /**
     * Define the validators. Each consists of: 
     * 
     *     tag: a tag in camelCase (Why? See: https://developer.mozilla.org/en-US/docs/Web/API/HTMLOrForeignElement/dataset )
     *     message: The error message with the optional {label} and {param} replacement tags. 
     *     run: the function to run, which should return boolean. 
     */
    static validators = [
        {
            tag : 'validateRequired',
            message : "{label} is required.",

            /**
             * Makes sure the value is not an empty string. However, will 
             * only run if the element is visible. 
             * 
             * @param {String} param 
             * @param {*} val 
             * @param {DomElement} el 
             */
            run: function(param, val, el)
            {
                return el.offsetParent === null ? true : val.length > 0;
            }
        },
        {
            tag : 'validateTime',
            message : "{label} must be a valid 24-hour time in the format HH:MM.",

            /**
             * Makes sure the value is not an empty string. However, will 
             * only run if the element is visible. 
             * 
             * @param {String} param 
             * @param {*} val 
             * @param {DomElement} el 
             */
            run: function(param, val, el)
            {
                return !val || val.match(/^([01]?[0-9]|2[0-3])\:[0-5][0-9]$/) !== null;
            }
        },
        {
            tag : 'validateMgrs',
            message : "{label} must be a valid MGRS string.",

            /**
             * If the value is set, makes sure it is a valid MGRS string. 
             * 
             * @param {String} param 
             * @param {*} val 
             * @param {DomElement} el 
             */
            run: function(param, val)
            {
                if(!val)
                {
                    return true;
                }
                try
                {
                    let location = new Location(val);
                    return true;
                }
                catch(error)
                {
                    return false;
                }
            }
        },
        {
            tag : 'validateMinlen',
            message : "{label} must be at least {param} characters.",

            /**
             * Makes sure the value is longer than val characters. 
             * 
             * @param {String} param 
             * @param {*} val 
             * @param {DomElement} el 
             */
            run: function(param, val)
            {
                return val.length >= param;
            }
        },
        {
            tag : 'validateMaxlen',
            message : "{label} must not exceed {param} characters.",

            /**
             * Makes sure the value is shorter than val characters. 
             * 
             * @param {String} param 
             * @param {*} val 
             * @param {DomElement} el 
             */
            run: function(param, val)
            {
                return val.length <= param;
            }
        },
        {
            tag : 'validateNumeric',
            message : "{label} must be numeric.",

            /**
             * Makes sure the value can be cast to a number. 
             * 
             * @param {String} param 
             * @param {*} val 
             * @param {DomElement} el 
             */
            run: function(param, val)
            {
                return !val || !isNaN(+val);
            }
        },
        {
            tag : 'validateDodid',
            message : "{label} must be a valid DOD ID.",

            /**
             * Makes sure the value can be cast to a number. 
             * 
             * @param {String} param 
             * @param {*} val 
             * @param {DomElement} el 
             */
            run: function(param, val)
            {
                return !val || validDodid(val)
            }
        }
    ];
}