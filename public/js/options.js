'use strict'

import * as CONSTANTS from "./shared/Constants.js"

/**
 * Generate a set of radio or checkbox options based on a constant defined in 
 * Constants.js
 * 
 * @element jhrm-constant-options
 */
export class ConstantOptions extends HTMLElement
{
    /**
     * Constructor 
     * 
     * @attribute {ConstantOptions.TYPE} type
     * @attribute {String} value
     * @attribute {String} constant
     */
	constructor()
	{
        super();
        this.name = this.id + 'Options';
        this.constant = CONSTANTS[this.getAttribute('constant')];
        this._value = [];
        this._disabled = [];
        this.hash='';
        this.type = ( this.getAttribute('type') == ConstantOptions.TYPE.RADIO )
            ? ConstantOptions.TYPE.RADIO 
            : ConstantOptions.TYPE.CHECKBOX;
        Object.seal(this)

        // Allow initial value to be set by attribute. 
        let attribute_value = this.getAttribute('value');
        if(attribute_value)
        {
            this.value = attribute_value;
        }

        // Allow disabled items to be set by attribute. 
        let attribute_disabled = this.getAttribute('disabled');
        if(attribute_disabled)
        {
            this.disabled = attribute_disabled.split(',');
        }

    }

    connectedCallback()
    {
        this.render();
    }

    /**
     * Set the value. 
     * 
     * @param {CONSTANT[]} val array of valid members of defined constant. 
     */
    set value(val)
    {
        // Value can only be validated in the context of a constant. 
        if(!this.constant)
        {
            throw new Error('Cannot set value when constant is undefined.')
        }

        // Can't set the value to null. 
        if(!val)
        {
            val=[];
        }

        // Treat all values as arrays internally. 
        if(!Array.isArray(val))
        {
            val = [val];
        }
        val.forEach(item=>{
            if(!this.constant.isValid(item))
            {
                throw new Error("Not a valid value.")
            }
        })

        // Only bother to continue of the value has changed. 
        let hash = Object.keys(this.constant).map(item=>val.includes(item)?'T':'F').join('');
        if(this.hash != hash)
        {
            this._value = val;
            this.hash = hash;
            this.dispatchEvent(new CustomEvent('change', {detail : this}));
            this.render();
        }
    }
    set Value(val)
    {
        this.value = val;
    }

    /**
     * return {array}
     */
    get value()
    {
        return this._value;
    }
    get Value()
    {
        return this.value;
    }

    /**
     * Set items to mark as disabled. 
     * @param {array} val Array of valid members of the defined constant
     */
    set disabled(val)
    {
        // Value can only be validated in the context of a constant. 
        if(!this.constant)
        {
            throw new Error('Cannot set value when constant is undefined.')
        }

        // Treat all values as arrays internally. 
        if(!Array.isArray(val))
        {
            val = [val];
        }
        val.forEach(item=>{
            if(!this.constant.isValid(item))
            {
                throw new Error("Not a valid value.")
            }
        })
        this._disabled = val;
        this.render();
    }

    /**
     * return {array}
     */
    get disabled()
    {
        return this._disabled;
    }

    /**
     * Redraw and bind event handlers. 
     */
    render()
    {
        // Value can only be validated in the context of a constant. 
        if(!this.constant)
        {
            throw new Error('Cannot render when constant is undefined.')
        }

        // Recreate HTML each time It's a small redraw. 
        this.innerHTML = Object
            .keys(this.constant)
            .map(option => `
                <label>
                    <input 
                        type="${this.type}" 
                        ${this.value.includes(option) ? "checked" : ""} 
                        ${this.disabled.includes(option) ? "disabled" : ""} 
                        value="${option}" 
                        name="${this.name}"
                    > 
                    ${this.constant.toString(option)}
                </label>
            `).join('');

        // If any of the inputs are clicked, set the value of the component 
        // based on which are checked. 
        this.querySelectorAll('input').forEach(el=>el.onclick = (event) => {
            this.value = [].slice.call(this.querySelectorAll('input:checked')).map(el=>el.value);
        });
    }
}

ConstantOptions.TYPE = {
    RADIO : 'radio',
    CHECKBOX : 'checkbox'
}
customElements.define('jhrm-constant-options', ConstantOptions);

/**
 * Define a block that can be toggled based on the value of a ConstantOption 
 * instance 
 * 
 * The block will be set to visible when the ConstantOption instance has a value
 * that includes "show"
 * 
 * @attribute {ConstantOption} source
 * @attriburte {CONSTANT} show
 */
export class ConstantOptionsTogglable extends HTMLElement
{

	constructor()
	{
        super();
        this._show = (this.getAttribute('show') || '').split(',');
        this._source = this.getAttribute('source');
        this._built = false;
        this._el = undefined;
    }

    connectedCallback()
    {
        if(!this._built)
        {
            this._built = true;

            if(!this._el)
            {
                if(this._source)
                {
                    this._el = document.getElementById(this._source);
                }
            }
        
            if(this._el && this._el instanceof ConstantOptions)
            {
                let handler = (event) => {
                    this.style.display = this._show.reduce((acc, cur)=>this._el.value.includes(cur) || acc, false) ? 'block' : 'none';
                }
                this._el.addEventListener('change', handler);
                handler();
            }
        }
    }

    set Source(val)
    {
        if(val instanceof ConstantOptions)
        {
            this._el = val;
        }
        else 
        {
            throw new Error("Invalid source.");
        }
    }

    
}
customElements.define('jhrm-constant-options-togglable', ConstantOptionsTogglable);

/**
 * Define a block that can be toggled based on the value of a ConstantOption 
 * instance 
 * 
 * The block will be set to visible when the ConstantOption instance has a value
 * that includes "show"
 * 
 * @attribute {ConstantOption} source
 * @attriburte {CONSTANT} show
 */
export class CheckboxTogglable extends HTMLElement
{

	constructor()
	{
        super();
        this._show = (this.getAttribute('show') || '');
        this._source = this.getAttribute('source');
        this._built = false;
        this._el = undefined;
        this._display = this.style.display || 'block';
    }

    connectedCallback()
    {
        let handler = (event) => {
            this.style.display = this._show == this._el.checked ? this._display : 'none';
        }

        if(!this._built)
        {
            this._built = true;

            if(!this._el)
            {
                if(this._source)
                {
                    this._el = document.getElementById(this._source);
                }
            }
        
            if(this._el && this._el.tagName=='INPUT')
            {
                
                this._el.addEventListener('change', handler);
            }
        }

        if(this._el && this._el.tagName=='INPUT')
        {
            handler();
        }
    }

    set Source(val)
    {
        if(val.tagName=='INPUT')
        {
            this._el = val;
        }
        else 
        {
            throw new Error("Invalid source.");
        }
    }

    
}
customElements.define('jhrm-checkbox-togglable', CheckboxTogglable);