import { selectAndStripId } from "../utility.js"
import { Sortable } from "../sortable.js"
import { Request } from "../request.js"

/**
 * { Integer } DoehrsIhId
 * { string } SamplingPoint
 * { Datetime } SampleDate
 * { Integer } SampleTimeMin
 * { string } SampleMedia
 * { string } SampleAnalyticalMethod
 * { string } SampleExposureNotes
 */
 export class Sensor
 {
     /**
      * Sets Sensor fields to their default values on the passed object.
      */
     static setDefaults(obj)
     {
         obj.DoehrsIhId = undefined;
         obj.SamplingPoint = "";
         obj.SamplingDate = "";
         obj.SampleTimeMin = "";
         obj.SampleMedia = "";
         obj.SampleAnalyticalMethod = "";
         obj.SampleExposureNotes = "";
     }
 
     /**
      * Creates a Sensor object from the corresponding fields on the backend.
      * 
      * @param { object } obj
      */
     static fromBackendFields(obj)
     {
         let ret = new Sensor();
 
             ret.DoehrsIhId = obj.id_doehrs_sample_id;
             ret.SamplingPoint = obj.sampling_point;
             ret.SamplingDate = obj.start_date_time
             ret.SampleTimeMin = obj.sample_time;
             ret.SampleMedia = obj.media;
             ret.SampleAnalyticalMethod = obj.analytical_method;
             ret.SampleExposureNotes = obj.exposure_notes;
        
         return ret;
     }
 
     constructor(obj)
     {
         Sensor.setDefaults(this);
         Object.seal(this);
 
         if (obj)
             Sensor.copyFields(obj, this);
     }
 
     /**
      * Gets an object populated with properties named according to backend conventions.
      * 
      * @returns { object }
      */
     toBackendFields()
     {
         return {
             id_doehrs_sample_id : this.DoehrsIhId,
             sampling_point : this.SamplingPoint,
             start_date_time : this.SamplingDate,
             sample_time : this.SampleTimeMin,
             media : this.SampleMedia,
             analytical_method : this.SampleAnalyticalMethod,
             exposure_notes : this.SampleExposureNotes
         };
     }
 }


export class ShowSamplesRow extends HTMLTableRowElement
{
	constructor()
	{
		super();

		this.innerHTML = `
			<td style="text-align: center;" id="doehrs_ih_id"></td>
			<td style="text-align: center;" id="sampling_point"></td>
			<td style="text-align: center;" id="sample_date"></td>
            <td style="text-align: center;" id="sample_time_min"></td>
            <td style="text-align: center;" id="sample_media"></td>
            <td style="text-align: center;" id="sample_analytical_method"></td>
            <td style="text-align: center;" id="sample_exposure_notes"></td>
		`;

		this._doehrs_ih_id = selectAndStripId(this, "doehrs_ih_id");
		this._sampling_point = selectAndStripId(this, "sampling_point");
		this._sample_date = selectAndStripId(this, "sample_date");
        this._sample_time_min = selectAndStripId(this, "sample_time_min");
		this._sample_media = selectAndStripId(this, "sample_media");
		this._sample_analytical_method = selectAndStripId(this, "sample_analytical_method");
        this._sample_exposure_notes = selectAndStripId(this, "sample_exposure_notes");

		this._sensor = new Sensor();

 	}

	/**
	 * @param { Sensor }
	 */
	set Sensor(val)
	{
		this._sensor = val;

        this._doehrs_ih_id.innerHTML = val.DoehrsIhId;
		this._sampling_point.innerHTML = val.SamplingPoint;
		this._sample_date.innerHTML = val.SamplingDate;
        this._sample_time_min.innerHTML = val.SampleTimeMin;
		this._sample_media.innerHTML = val.SampleMedia;
		this._sample_analytical_method.innerHTML = val.SampleAnalyticalMethod;
        this._sample_exposure_notes.innerHTML = val.SampleExposureNotes;
	}

	/**
	 * @param { Sensor }
	 */
	get Sensor()
	{
		return this._sensor;
	}

}

customElements.define('jhrm-eh-sample-show-samples-row', ShowSamplesRow,  { extends: 'tr' });

export class ShowSamplesTable extends HTMLElement
{
	constructor()
	{
		super();

        this.innerHTML = `
			<div style="display: grid; grid-template-columns: auto 2fr 2fr 1fr; min-width: 40em; grid-gap: 0.75em;">
                <table style="grid-row: 2; grid-column: 1/5; overflow: auto;" class="tablescroll"  id="main_table">
					<thead>
						<tr>
							<th>DOEHRS-IH ID</th>
							<th>Sampling Point</th>
							<th>Date</th>
                            <th>Sample Time (minutes)</th>
                            <th>Media</th>
                            <th>Analytical Method</th>
                            <th>Exposure Notes</th>
						</tr>
                    </thead>
					<tbody id="table_body">
					</tbody>
				</table>
			</div>
		`;
        this._main_table = selectAndStripId(this, "main_table");
		this._table_body = selectAndStripId(this, "table_body");

    	this._doehrs_ih_id = selectAndStripId(this, "doehrs_ih_id");
		this._sampling_point = selectAndStripId(this, "sampling_point");
		this._sample_date = selectAndStripId(this, "sample_date");
        this._sample_time_min = selectAndStripId(this, "sample_time_min");
		this._sample_media = selectAndStripId(this, "sample_media");
		this._sample_analytical_method = selectAndStripId(this, "sample_analytical_method");
        this._sample_exposure_notes = selectAndStripId(this, "sample_exposure_notes");

		Sortable(this._main_table);
	}

    /**
	 * @param { Sensor[] }
	 */
    set Sensors(ss)
    {
        this._table_body.innerHTML = "";

        for( let i = 0; i < ss.length; ++i)
		{
			let next_row = new ShowSamplesRow();
			next_row.Sensor = ss[i];

			this._table_body.append(next_row);
		}
    }
}

customElements.define('jhrm-eh-sample-show-samples-table', ShowSamplesTable);