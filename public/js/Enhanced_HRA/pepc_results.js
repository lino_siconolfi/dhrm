import { FormatDimensionedValue } from "../format_dimensioned_value.js"
import { Request } from "../request.js";

export class EnhancedPepcResultExposureRow extends HTMLTableRowElement
{
	constructor(record)
	{
		super();

		this._Built = false;
		
		this._ElmPepcCalcName = document.createElement('td');
		this._ElmPepcCalcPeakVal = document.createElement('td');
		this._ElmPepcCalcAvgVal = document.createElement('td');
		this._ElmPepcCalcChronicMeg = document.createElement('td');
		this._ElmPepcCalcChronicValue = document.createElement('td');
		this._ElmPepcCalcChronicExceeded = document.createElement('td');
		this._ElmPepcCalcAcuteMeg = document.createElement('td');
		this._ElmPepcCalcAcuteValue = document.createElement('td');
		this._ElmPepcCalcAcuteExceeded = document.createElement('td');
		this._Record = record;
	}

	connectedCallback()
	{
		if ( !this._Built )
		{
			let TdSelected = document.createElement('td');
			TdSelected.append(this._ElmSelected);

			this.append(
				this._ElmPepcCalcName, 
				this._ElmPepcCalcPeakVal,
				this._ElmPepcCalcAvgVal, 
				this._ElmPepcCalcChronicMeg,
				this._ElmPepcCalcChronicValue,
				this._ElmPepcCalcChronicExceeded,
				this._ElmPepcCalcAcuteMeg,
				this._ElmPepcCalcAcuteValue,
				this._ElmPepcCalcAcuteExceeded
			);

			this._Built = true;
		}
	}

	get Record()
	{
		return this._Record;
	}

	set PepcCalcName(val)
	{
		this._ElmPepcCalcName.innerText = val;
	}

	get PepcCalcName()
	{
		return this._ElmPepcCalcName.textContent;
	}

	set PepcCalcPeakVal(val)
	{
		this._ElmPepcCalcPeakVal.innerText = val;
	}

	get PepcCalcPeakVal()
	{
		return this._ElmPepcCalcPeakVal.textContent;
	}

	set PepcCalcAvgVal(val)
	{
		this._ElmPepcCalcAvgVal.innerText = val;
	}

	get PepcCalcAvgVal()
	{
		return this._ElmPepcCalcAvgVal.textContent;
	}

	set PepcCalcOneYrNeg(val)
	{
		this._ElmPepcCalcOneYrNeg.innerText = val;
	}

	get PepcCalcOneYrNeg()
	{
		return this._ElmPepcCalcOneYrNeg.textContent;
	}

	set PepcCalcChronicMeg(val)
	{
		this._ElmPepcCalcChronicMeg.innerText = val;
	}

	get PepcCalcChronicMeg()
	{
		return this._ElmPepcCalcChronicMeg.textContent;
	}

	set PepcCalcChronicValue(val)
	{
		this._ElmPepcCalcChronicValue.innerText = val;
	}

	get PepcCalcChronicValue()
	{
		return this._ElmPepcCalcChronicValue.textContent;
	}

	set PepcCalcChronicExceeded(val)
	{
		if (val == "Yes")
		{
			this._ElmPepcCalcChronicExceeded.style.backgroundColor="yellow";
		}
		this._ElmPepcCalcChronicExceeded.innerText = val;
	}

	get PepcCalcChronicExceeded()
	{
		return this._ElmPepcCalcChronicExceeded.textContent;
	}
	
	set PepcCalcAcuteMeg(val)
	{
		this._ElmPepcCalcAcuteMeg.innerText = val;
	}

	get PepcCalcAcuteMeg()
	{
		return this._ElmPepcCalcAcuteMeg.textContent;
	}

	set PepcCalcAcuteValue(val)
	{
		this._ElmPepcCalcAcuteValue.innerText = val;
	}

	get PepcCalcAcuteValue()
	{
		return this._ElmPepcCalcAcuteValue.textContent;
	}

	set PepcCalcAcuteExceeded(val)
	{
		if (val == "Yes")
		{
			this._ElmPepcCalcAcuteExceeded.style.backgroundColor="yellow";
		}
		this._ElmPepcCalcAcuteExceeded.innerText = val;
	}

	get PepcCalcAcuteExceeded()
	{
		return this._ElmPepcCalcAcuteExceeded.textContent;
	}

	static fromObjData(record)
	{
		let nextRow = new EnhancedPepcResultExposureRow(record);
		nextRow.PepcCalcName = record.analyte_name;
		nextRow.PepcCalcPeakVal = FormatDimensionedValue(record.peak_value);
		nextRow.PepcCalcAvgVal = FormatDimensionedValue(record.avg_value);
		nextRow.PepcCalcChronicMeg = record.chronic_meg;
		nextRow.PepcCalcChronicValue = FormatDimensionedValue(record.chronic_meg_value);
		nextRow.PepcCalcChronicExceeded = record.chronic_meg_exceeded;
		nextRow.PepcCalcAcuteMeg = record.acute_meg;
		nextRow.PepcCalcAcuteValue = FormatDimensionedValue(record.acute_meg_value);
		nextRow.PepcCalcAcuteExceeded = record.acute_meg_exceeded;

		return nextRow;
	}
}

customElements.define('jhrm-ehra-pepc-result-exposure-row', EnhancedPepcResultExposureRow, { extends: 'tr' });

export class EnhancedHraPepcCalcsTable extends HTMLElement
{
	constructor()
	{
		super();

		this._Built = false;

		let createTH = (text) =>
		{
			let elm = document.createElement('th');
			elm.innerHTML = text;

			return elm;
		}

		this._ElmHeader = document.createElement('thead');
		this._ElmBody = document.createElement('tbody');
		this._Caption = document.createElement('caption');

		this._ElmHeader.append(
			createTH('Chemical Name'),
			createTH('Peak Value'),
			createTH('Avg Value'),
			createTH('Chronic MEG'),
			createTH('Chronic MEG Value'),
			createTH('Chronic MEG Exceeded'),
			createTH('Acute MEG'),
			createTH('Acute MEG Value'),
			createTH('Acute MEG Exceeded')
		);
		
		this._ElmTable = document.createElement('table');
		this._ElmTable.append(this._Caption, this._ElmHeader, this._ElmBody);

		}
		connectedCallback()
		{
			if (!this._Built)
			{
				this.append(this._ElmTable);
				this._Built = true;
			}
		};

		async acutePEPC(hraId)
		{
			// make sure empty first
			while (this._ElmBody.lastElementChild) 
			{
				this._ElmBody.removeChild(this._ElmBody.lastElementChild);
			}
			let results = await Request.get('/api/enhancedhra/hra/' + hraId + '/prescreen');
			results.forEach(
				record =>
				{
					let nextRow = EnhancedPepcResultExposureRow.fromObjData(record);
					this.appendExposureRow(nextRow);
				}
			);	
		}

	appendExposureRow(row)
	{
		if (row instanceof EnhancedPepcResultExposureRow)
		{
			row.GroupName = this.id;
			this._ElmBody.append(row);
		}
	}

	set Caption(val)
	{
		this._Caption.innerText = val;
	}

	get ChronicExceeded()
	{
		let tBody = this._ElmBody;

		for (let i = 0; i < tBody.children.length; ++i)
		{
			let element = tBody.children.item(i);
			let data = element.Record;

			if (data.chronic_meg_exceeded == 'Yes')
				return data;
		}

		return false;
	}

	get AcuteExceeded()
	{
		let tBody = this._ElmBody;

		for (let i = 0; i < tBody.children.length; ++i)
		{
			let element = tBody.children.item(i);
			let data = element.Record;

			if (data.acute_meg_exceeded == 'Yes')
				return data;
		}

		return false;
	}
}

customElements.define('jhrm-ehra-pepc-calcs-table', EnhancedHraPepcCalcsTable);