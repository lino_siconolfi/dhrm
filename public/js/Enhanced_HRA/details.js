import { JhrmTooltip } from "../tooltip.js"
import { MaterialIcon } from "../material_icon.js"
import { Request } from "../request.js"

let createLabel = function(text, row, col, tipContents)
{
	let ret = document.createElement('div');
	ret.style.gridRow = row.toString();
	ret.style.gridColumn = col.toString();
	ret.style.whiteSpace = "nowrap";
	ret.append(text);

	if (undefined != tipContents)
	{
		let HelpIcon = MaterialIcon.Create("help_outline");
		let Tooltip = new JhrmTooltip();

		if (tipContents instanceof HTMLElement)
			Tooltip.append(tipContents);
		else
			Tooltip.innerHTML = tipContents;

		Tooltip.Target = HelpIcon;
		ret.append( HelpIcon, Tooltip );
	}

	return ret;
};

let createRadio = function(name, value, col, text)
{
	let input = document.createElement('input');
	input.type = "radio";
	input.name = name;
	input.value = value;
	let elmText = (undefined == text) ? value : text;

	let ret = document.createElement('div');
	ret.style.gridColumn = col.toString();
	ret.style.whiteSpace = "nowrap";
	ret.append(input, document.createTextNode(" " + elmText) );

	ret.RadioElement = input;

	return ret;
};

let createRadioGrid = function(row, col)
{
	let elm = document.createElement('div');

	elm.style.display = "grid";
	elm.style.gridRow = row.toString();
	elm.style.gridColumn = col.toString();
	elm.style.gridTemplateColumns = "1fr 1fr 1fr 1fr";
	elm.style.gridColumnGap = "0.75em";

	return elm;
};

let gridOr = function (row, col)
{
	let elm = document.createElement('div');
	elm.style.gridRow = row.toString();
	elm.style.gridColumn = col.toString();
	elm.style.textAlign = "center";
	elm.innerText = "OR";

	return elm;
};

let createTextInput = function(row, col)
{
	let ret = document.createElement('input');
	ret.style.width = "100%";
	ret.style.gridRow = row.toString();
	ret.style.gridColumn = col.toString();

	return ret;
}

export class EnhancedHraDetailsForm extends HTMLElement
{
	constructor()
	{
		super();

		this._inputObjective = createTextInput(1, "2/7");
		this._inputLatitude = createTextInput(2, 4);
		this._inputLongitude = createTextInput(2, 6);
		this._inputSapFile = createTextInput(3, 2);

		this._selectLocation = document.createElement('select');
		this._selectLocation.style.gridRow = "2";
		this._selectLocation.style.gridColumn = "2";
		this._selectLocation.style.width = "100%";

		this._buttonUpload = document.createElement('button');
		this._buttonUpload.style.className = "small-button";
		this._buttonUpload.style.gridRow = "3";
		this._buttonUpload.style.gridColumn = "3";
		this._buttonUpload.style.marginLeft = "auto";
		this._buttonUpload.style.marginRight = "auto";
		this._buttonUpload.innerText = "Upload";

		this._buttonPickSap = document.createElement('button');
		this._buttonPickSap.style.className = "small-button";
		this._buttonPickSap.style.gridRow = "3";
		this._buttonPickSap.style.gridColumn = "5/7";
		this._buttonPickSap.style.marginLeft = "auto";
		this._buttonPickSap.style.marginRight = "auto";
		this._buttonPickSap.innerText = "Pick from completed SAPs";

		this._inputLatitude.style.minWidth = "6em";
		this._inputLongitude.style.minWidth = "6em";

		this._Latitude = 0;
		this._Longitude = 0;

		/////////////////////////////

		let MO = EnhancedHraDetailsForm.MediaOption;

		this._MediaRadioElements = [
			createRadio("medium", MO.Air, 1, MO.Air),
			createRadio("medium",  MO.Water, 2, MO.Water),
			createRadio("medium", MO.Soil, 3, MO.Soil)
		];

		this._RadioMediaGrid = createRadioGrid(4, 2);
		this._RadioMediaGrid.style.minWidth = "10em";
		this._RadioMediaGrid.append( ...this._MediaRadioElements );
		this._MediaRadioElements[0] = this._MediaRadioElements[0].RadioElement;
		this._MediaRadioElements[1] = this._MediaRadioElements[1].RadioElement;
		this._MediaRadioElements[2] = this._MediaRadioElements[2].RadioElement;

		this._MediaRadioElements[0].checked = true;
		this._MediaRadioElements[1].disabled = true;
		this._MediaRadioElements[2].disabled = true;

		/////////////////////////////

		let SBO = EnhancedHraDetailsForm.SelectByOption;

		this._SelectByRadioElements = [
			createRadio("selection_by", SBO.ExposurePathway, "1/3", SBO.ExposurePathway),
			createRadio("selection_by", SBO.Samples, 4, SBO.Samples)
		];

		this._RadioSelectionByGrid = createRadioGrid(5, 2);
		this._RadioSelectionByGrid.style.minWidth = "20em";
		this._RadioSelectionByGrid.append(
			this._SelectByRadioElements[0],
			gridOr(1, 3),
			this._SelectByRadioElements[1]
		);
		this._SelectByRadioElements[0] = this._SelectByRadioElements[0].RadioElement;
		this._SelectByRadioElements[1] = this._SelectByRadioElements[1].RadioElement;

		this._SelectByRadioElements[0].disabled = false;
		this._SelectByRadioElements[1].disabled = true;

		this._SelectByRadioElements[0].checked = true;

		/////////////////////////////

		let SSO = EnhancedHraDetailsForm.SampleSelectionOption;

		this._SampleSelectionRadioElements = [
			createRadio("sample_selection", SSO.TabularForm, "1/3", SSO.TabularForm),
			createRadio("sample_selection", SSO.GisMap, 4, SSO.GisMap)
		];

		this._SampleSelectionRadioGrid = createRadioGrid(7, 2);
		this._SampleSelectionRadioGrid.style.minWidth = "10em";
		this._SampleSelectionRadioGrid.append(
			this._SampleSelectionRadioElements[0],
			gridOr(1, 3),
			this._SampleSelectionRadioElements[1]
		);
		this._SampleSelectionRadioElements[0] = this._SampleSelectionRadioElements[0].RadioElement;
		this._SampleSelectionRadioElements[1] = this._SampleSelectionRadioElements[1].RadioElement;

		this._SampleSelectionRadioElements[0].disabled = false;
		this._SampleSelectionRadioElements[1].disabled = true;

		this._SampleSelectionRadioElements[0].checked = true;

		this._Built = false;
	}

	connectedCallback()
	{
		if ( !this._Built )
		{
			this.style.display = "grid";
			this.style.gridTemplateColumns = "auto fit-content(25rem) 1fr auto 1fr auto";
			this.style.gridGap = "1rem 0.5rem";

			let ObjectiveTipText = document.createElement('span');
			ObjectiveTipText.style.textAlign = "center";
			ObjectiveTipText.innerHTML = "128 character limit<br>ie: Assess the parameter around the barracks.";

			let LabelObjective = createLabel(
				"* HRA Objective: ",
				1, 1,
				ObjectiveTipText
			);

			let LatitudeLabel = createLabel("Latitude: ", 2, 3);
			LatitudeLabel.style.textAlign = "right";
			LatitudeLabel.style.marginLeft = "1rem";

			let LongitudeLabel = createLabel("Longitude: ", 2, 5);
			LongitudeLabel.style.textAlign = "right";
			LongitudeLabel.style.marginLeft = "1rem";

			let LocationTooltipText = document.createElement('div');
			LocationTooltipText.style.display = "inline-block";
			LocationTooltipText.style.whiteSpace = "normal";
			LocationTooltipText.innerHTML =
				"Select the location for the samples you wish to use";

			this.append(
				LabelObjective, this._inputObjective,
				createLabel(
					"* Location: ", 2, 1,
					LocationTooltipText
				),
				this._selectLocation,
				LatitudeLabel, this._inputLatitude, LongitudeLabel, this._inputLongitude,
				createLabel(
					"SAP File: ", 3, 1,
					"Upload the sampling and analysis plan (SAP) or select a " +
					"completed SAP from the list."
				),
				this._inputSapFile, this._buttonUpload,
				gridOr(3, 4), this._buttonPickSap,
				createLabel(
					"* Media: ", 4, 1,
					"Select the media for the exposure pathway type of concern."),
				this._RadioMediaGrid,
				createLabel("Selection By: ", 5, 1, "Future Funcionality"),
				this._RadioSelectionByGrid,
				createLabel("", 6, 1,),
				createLabel("Sample Selection: ", 7, 1, "Future Funcionality"),
				this._SampleSelectionRadioGrid
			);

			this._inputLatitude.value = this._Latitude.toString();
			this._inputLongitude.value = this._Longitude.toString();

			Request.get('/api/enhancedhra/locations').then(
				function(response)
				{
					this._selectLocation.innerHTML = "";

					let allElm = document.createElement('option');
					allElm.innerText = "(all)";

					this._selectLocation.append(allElm);

					for ( let record of response)
					{
						let option = document.createElement('option');
						option.innerText = record.location_name;

						this._selectLocation.append(option);
					}
				}.bind(this),
				function(error)
				{
					// Known empty function
				}
			);

			this._Built = true;
		}
	}

	get Objective()
	{
		return this._inputObjective.value;
	}

	set Objective(val)
	{
		this._inputObjective.value = val;
	}

	get Latitude()
	{
		return this._Latitude;
	}

	set Latitude(val)
	{
		this._Latitude = val;
		this._inputLatitude.value = val.toString();
	}

	get Longitude()
	{
		return this._Longitude;
	}

	set Longitude(val)
	{
		this._Longitude = val;
		this._inputLongitude.value = val.toString();
	}

	get SapFile()
	{
		return this._inputSapFile.value;
	}

	set SapFile(val)
	{
		this._inputSapFile.value = val;
	}

	get Media()
	{
		let elements = this._MediaRadioElements;

		for(let i = 0; i < elements.length; i++)
		{
			if( elements[i].checked )
				return elements[i].value;
		}
	}

	set Media(val)
	{
		let elements = this._MediaRadioElements;

		for(let i = 0; i < elements.length; i++)
		{
			if( elements[i].value = val )
			{
				elements[i].checked = true;
				return;
			}
		}
	}

	get SelectionBy()
	{
		let elements = this._SelectByRadioElements;

		for(let i = 0; i < elements.length; i++)
		{
			if( elements[i].checked )
				return elements[i].value;
		}
	}

	set SelectionBy(val)
	{
		let elements = this._SelectByRadioElements;

		for(let i = 0; i < elements.length; i++)
		{
			if( elements[i].value = val )
			{
				elements[i].checked = true;
				return;
			}
		}
	}

	get SampleSelection()
	{
		let elements = this._SampleSelectionRadioElements;

		for(let i = 0; i < elements.length; i++)
		{
			if( elements[i].checked )
				return elements[i].value;
		}
	}

	set SampleSelection(val)
	{
		let elements = this._SampleSelectionRadioElements;

		for(let i = 0; i < elements.length; i++)
		{
			if( elements[i].value = val )
			{
				elements[i].checked = true;
				return;
			}
		}
	}

	get Location()
	{
		try
		{
			return this._selectLocation.options[this._selectLocation.selectedIndex].text;
		}
		catch(err)
		{
			return undefined;
		}
	}

	set Location(val)
	{
		this._selectLocation.value = val;
	}

	// Code to be updated in the future to handle media, selection by, and sample selection radio button selections
	// to update Exposure Pathway table
	// --lino
	/* set onSearch(func)
	{
		this._buttonSearch.onclick = func;
	} */

	set onLocationChange(func)
	{
		this._selectLocation.onchange = func;
	}
}

EnhancedHraDetailsForm.MediaOption =
{
	Air : "Air",
	Water : "Water",
	Soil : "Soil"
};

EnhancedHraDetailsForm.SelectByOption =
{
	ExposurePathway : "Exposure Pathway",
	Samples : "Samples"
};

EnhancedHraDetailsForm.SampleSelectionOption =
{
	TabularForm : "Tabular Form",
	GisMap : "GIS Map"
};

customElements.define('jhrm-ehra-details-form', EnhancedHraDetailsForm);
