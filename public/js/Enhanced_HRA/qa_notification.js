import {EnhancedHraSummaryTable} from './summary.js'
import { Request } from "../request.js"

export class QANotification extends HTMLElement {
    constructor() {
        super();
        
        this._cookie = document.cookie;
    }

    async connectedCallback() {
        let ehra = new EnhancedHraSummaryTable;
        let user = await Request.get('./api/user');
        let pending = await ehra.getRowsPendingQA();
        if(pending.length > 0 && user.role.hra_qa) {
            this.style.width = "2em";
            this.style.height = "2em";
            this.style.borderRadius = "50%";
            this.style.backgroundColor= "red";
            this.style.color = "white";
            this.style.fontSize = "1.4em";
            this.style.padding = "10px"
            this.style.textAlign = "center";
            this.style.fontWeight = "600";
            this.style.display = "inline-block";
            this.style.marginLeft = "15px";
            this.innerText = pending.length;
        } else {
            this.style.dispay = "none";
        }
    }
}

customElements.define("qa-notification", QANotification);