import { Request } from "../request.js";
import { Sortable } from "../sortable.js";

export class EnhancedHraExposurePathwayRow extends HTMLTableRowElement
{

	constructor()
	{
		super();

		this._Built = false;

		this._ElmSelected = document.createElement('input');
		this._ElmSelected.type = 'radio';

		this._ElmPathwayId = document.createElement('td');
		this._ElmPathwayName = document.createElement('td');
		this._ElmSource = document.createElement('td');
		this._ElmEnvironmentalMedia = document.createElement('td');
		this._ElmHealthThreat = document.createElement('td');
		this._ElmRouteOfExposure = document.createElement('td');
		this._ElmPopulationAtRiskDescription = document.createElement('td');
		this._ElmPathwayPriority = document.createElement('td');

		this._PopulationAtRisk = undefined;
		this._PathwayId = undefined;
		this._Location = undefined;
	}

	connectedCallback()
	{
		if ( !this._Built )
		{
			let TdSelected = document.createElement('td');
			TdSelected.append(this._ElmSelected);

			this.append(
				TdSelected, this._ElmPathwayId, this._ElmPathwayName,
				this._ElmSource, this._ElmEnvironmentalMedia,
				this._ElmHealthThreat, this._ElmRouteOfExposure,
				this._ElmPopulationAtRiskDescription, this._ElmPathwayPriority
			);

			this._Built = true;
		}
	}

	set GroupName(val)
	{
		this._ElmSelected.name = val;
	}

	set Selected(val)
	{
		this._ElmSelected.checked = val;
	}

	get Selected()
	{
		return this._ElmSelected.checked && this.Visible;
	}

	set PathwayId(val)
	{
		this._PathwayId = val;

		let strVal = val.toString();
		this._ElmPathwayId.innerText = strVal;
	}

	get PathwayId()
	{
		return this._PathwayId;
	}

	set PathwayName(val)
	{
		this._ElmPathwayName.innerText = val;
	}

	get PathwayName()
	{
		return this._ElmPathwayName.textContent;
	}

	set Source(val)
	{
		this._ElmSource.innerText = val;
	}

	get Source()
	{
		return this._ElmSource.textContent;
	}

	set EnvironmentalMedia(val)
	{
		this._ElmEnvironmentalMedia.innerText = val;
	}

	get EnvironmentalMedia()
	{
		return this._ElmEnvironmentalMedia.textContent;
	}

	set HealthThreat(val)
	{
		this._ElmHealthThreat.innerText = val;
	}

	get HealthThreat()
	{
		return this._ElmHealthThreat.textContent;
	}


	set RouteOfExposure(val)
	{
		this._ElmRouteOfExposure.innerText = val;
	}

	get RouteOfExposure()
	{
		return this._ElmRouteOfExposure.textContent;
	}

	set RouteOfExposure(val)
	{
		this._ElmRouteOfExposure.innerText = val;
	}

	get RouteOfExposure()
	{
		return this._ElmRouteOfExposure.textContent;
	}

	set PopulationAtRiskDescription(val)
	{
		this._PopulationAtRiskDescription = val;
		this._ElmPopulationAtRiskDescription.innerText = val.toString();
	}

	get PopulationAtRiskDescription()
	{
		return this._PopulationAtRiskDescription;
	}

	set PathwayPriority(val)
	{
		this._ElmPathwayPriority.innerText = val;
	}

	get PathwayPriority()
	{
		return this._ElmPathwayPriority.textContent;
	}

	set Location(val)
	{
		this._Location = val;
	}

	get Location()
	{
		return this._Location;
	}

	set Visible(val)
	{
		this.style.display = val ? "table-row" : "none";
	}

	get Visible()
	{
		let style = window.getComputedStyle(this);
		return (style.display != "none");
	}
}

customElements.define('jhrm-ehra-exposure-pathway-row', EnhancedHraExposurePathwayRow, { extends: 'tr' });

export class EnhancedHraExposurePathwayTable extends HTMLElement
{
	constructor()
	{
		super();

		this._Built = false;
		this._onupdate = undefined;

		let createTH = (text) =>
		{
			let elm = document.createElement('th');
			elm.innerHTML = text;

			return elm;
		}

		this._ElmHeader = document.createElement('thead');
		this._ElmBody = document.createElement('tbody');

		this._ElmHeader.append(
			createTH('Sel.'),
			createTH('Exposure Pathway ID'),
			createTH('Name of Exposure Pathway'),
			createTH('Threat Source'),
			createTH('Environmental Media'),
			createTH('Health Hazards'),
			createTH('Route of Exposures'),
			createTH('Population at Risk Description'),
			createTH('Priority of Exposure Pathway')
		);
		
		this._ElmTable = document.createElement('table');
		this._ElmTable.append(this._ElmHeader, this._ElmBody);
		this._ElmTableWrapper = document.createElement('div');
		this._ElmTableWrapper.append(this._ElmTable);

		this._AllRows = [];

		Sortable(this._ElmTable);
	}

	connectedCallback()
	{
		if (!this._Built)
		{
			this._ElmTableWrapper.className="tablewrapper";
			this.append(this._ElmTableWrapper);

			Request.get('/api/enhancedhra/pathway').then(
				function(response)
				{
					for (let index in response)
					{
						let record = response[index];
						let nextRow = new EnhancedHraExposurePathwayRow();

						nextRow.PathwayId = record.id;
						nextRow.PathwayName = record.name;
						nextRow.Source = record.source;
						nextRow.EnvironmentalMedia = record.environmental_media;
						nextRow.HealthThreat = record.health_threat;
						nextRow.RouteOfExposure = record.exposure_route;
						nextRow.PopulationAtRiskDescription = record.population_at_risk_descr;
						nextRow.PathwayPriority = record.priority;
						nextRow.Location = record.location;
						nextRow.GroupName = this.id;

						this._ElmBody.append(nextRow);
						this._AllRows.push(nextRow);

						if (undefined != this._onupdate)
							this._onupdate();
					}
				}.bind(this)
			);

			this._Built = true;
		}
	}

	appendExposureRow(row)
	{
		if (row instanceof EnhancedHraExposurePathwayRow)
		{
			row.GroupName = this.id;
			this._ElmBody.append(row);
		}
	}

	filter(location, media)
	{
		let tBody = this._ElmBody;

		let filterByLocation = (location != undefined);
		let filterByMedia = (media != undefined);

		if ( filterByMedia )
			media = media.toLowerCase();
		
		if ( filterByLocation )
			location = location.toLowerCase();

		for (let i = 0; i < this._AllRows.length; ++i)
		{
			let element = this._AllRows[i];

			element.Visible = 
				(!filterByLocation || element.Location .toLowerCase() === location) &&
				(!filterByMedia || element.EnvironmentalMedia.toLowerCase() === media);
		}

		return undefined;
	}

	/**
	 * 
	 * @param { number } id
	 * 	The id of the pathway that should be selected.
	 * 
	 * @param { boolean } selectInvisible
	 * 	If true, the pathway will be selected even if filtering makes it invisible.
	 * 	this defaults to false.
	 * 
	 * @returns
	 * 	True if the pathway was found and selected, false otherwise.
	 */
	selectPathwayId(id, selectInvisible)
	{

		let tBody = this._ElmBody;

		for (let i = 0; i < tBody.children.length; ++i)
		{
			let element = tBody.children.item(i);

			if (element.PathwayId == id)
			{
				if (selectInvisible || element.Visible)
				{
					element.Selected = true;
					return true;
				}
				else
				{
					return false;
				}
			}
		}

		return false;
	}

	get SelectedRow()
	{
		let tBody = this._ElmBody;

		for (let i = 0; i < this._AllRows.length; ++i)
		{
			let element = this._AllRows[i];

			if (element.Selected)
				return element;
		}

		return undefined;
	}

	get SelectedID()
	{
		let row = this.SelectedRow;
		return (undefined != row) ? row.PathwayId : undefined;
	}

	set onupdate(func)
	{
		this._onupdate = func;
	}

	get Locations()
	{
		let tBody = this._ElmBody;
		let ret = new Set();

		for (let i = 0; i < this._AllRows.length; ++i)
		{
			let element = this._AllRows[i];
			ret.add(element.Location);
		}

		let retArray = Array.from(ret);
		retArray.sort();

		return retArray;
	}
}

customElements.define('jhrm-ehra-exposure-pathway-table', EnhancedHraExposurePathwayTable);


export class EnhancedHraExposurePathwayDisplay extends HTMLElement
{
	constructor()
	{
		super();

		this._Built = false;
		this._onupdate = undefined;

		let createTH = (text) =>
		{
			let elm = document.createElement('th');
			elm.innerHTML = text;

			return elm;
		}

		this._ElmHeader = document.createElement('thead');
		this._ElmBody = document.createElement('tbody');
		this._DisplayRow = document.createElement('tr');

		this._ElmHeader.append(
			createTH('Exposure Pathway ID'),
			createTH('Name of Exposure Pathway'),
			createTH('Source'),
			createTH('Environmental Media'),
			createTH('Health Threat'),
			createTH('Route of Exposure'),
			createTH('Population at Risk Description'),
			createTH('Priority of Exposure Pathway')
		);
		
		this._ElmPathwayId = document.createElement('td');
		this._ElmPathwayName = document.createElement('td');
		this._ElmSource = document.createElement('td');
		this._ElmEnvironmentalMedia = document.createElement('td');
		this._ElmHealthThreat = document.createElement('td');
		this._ElmRouteOfExposure = document.createElement('td');
		this._ElmPopulationAtRiskDescription = document.createElement('td');
		this._ElmPathwayPriority = document.createElement('td');

		this._DisplayRow.append(
			this._ElmPathwayId, this._ElmPathwayName,
			this._ElmSource, this._ElmEnvironmentalMedia,
			this._ElmHealthThreat, this._ElmRouteOfExposure,
			this._ElmPopulationAtRiskDescription, this._ElmPathwayPriority
		);

		this._ElmTable = document.createElement('table');
		this._ElmTable.append(this._ElmHeader, this._ElmBody);
		this._ElmTable.append(this._DisplayRow);
	}

	async populateFromHra(hraId)
	{
		let results = await Request.get('/api/enhancedhra/hra/' + hraId + '/pathway');
		let record = results[0];

		this.PathwayId = record.id;
		this.PathwayName = record.name;
		this.Source = record.source;
		this.EnvironmentalMedia = record.environmental_media;
		this.HealthThreat = record.health_threat;
		this.RouteOfExposure = record.exposure_route;
		this.PopulationAtRiskDescription = record.population_at_risk_descr;
		this.PathwayPriority = record.priority;
		this.Location = record.location;
	}
	
	connectedCallback()
	{
		if (!this._Built)
		{
			this.append(this._ElmTable);
			this._Built = true;
		}
	}

	set PathwayId(val)
	{
		this._ElmPathwayId.innerText = val;
	}

	get PathwayId()
	{
		return this._ElmPathwayId.textContent;
	}

	set PathwayName(val)
	{
		this._ElmPathwayName.innerText = val;
	}

	get PathwayName()
	{
		return this._ElmPathwayName.textContent;
	}

	set Source(val)
	{
		this._ElmSource.innerText = val;
	}

	get Source()
	{
		return this._ElmSource.textContent;
	}

	set EnvironmentalMedia(val)
	{
		this._ElmEnvironmentalMedia.innerText = val;
	}

	get EnvironmentalMedia()
	{
		return this._ElmEnvironmentalMedia.textContent;
	}

	set HealthThreat(val)
	{
		this._ElmHealthThreat.innerText = val;
	}

	get HealthThreat()
	{
		return this._ElmHealthThreat.textContent;
	}


	set RouteOfExposure(val)
	{
		this._ElmRouteOfExposure.innerText = val;
	}

	get RouteOfExposure()
	{
		return this._ElmRouteOfExposure.textContent;
	}

	set RouteOfExposure(val)
	{
		this._ElmRouteOfExposure.innerText = val;
	}

	get RouteOfExposure()
	{
		return this._ElmRouteOfExposure.textContent;
	}

	set PopulationAtRiskDescription(val)
	{
		this._PopulationAtRiskDescription = val;

		this._ElmPopulationAtRiskDescription.innerText =
			(undefined !== val) ? val.toString() : "";
	}

	get PopulationAtRiskDescription()
	{
		return this._PopulationAtRiskDescription;
	}

	set PathwayPriority(val)
	{
		this._ElmPathwayPriority.innerText = val;
	}

	get PathwayPriority()
	{
		return this._ElmPathwayPriority.textContent;
	}

	set Location(val)
	{
		this._Location = val;
	}

	get Location()
	{
		return this._Location;
	}

	set Visible(val)
	{
		this.style.display = val ? "table-row" : "none";
	}

	get Visible()
	{
		let style = window.getComputedStyle(this);
		return (style.display != "none");
	}
}

customElements.define('jhrm-ehra-exposure-pathway-display', EnhancedHraExposurePathwayDisplay);
