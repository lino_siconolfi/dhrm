import { Request } from "../request.js";
import { Sortable } from "../sortable.js";
import { validateAnalyticalMethodLimits } from "../analytical_method.js";
import { Datetime } from "../shared/datetime.js"


export class EnhancedHraSampleSelectRow extends HTMLTableRowElement
{
	constructor(_ShowSelection)
	{
		super();

		this._Built = false;
		this._ShowSelection = _ShowSelection;

		this._ElmSelected = document.createElement('input');
		this._ElmSelected.type = 'checkbox';

		this._ElmSampleId = document.createElement('td');
		this._ElmSamplingPoint = document.createElement('td');
		this._ElmStartDateTime = document.createElement('td');
		this._ElmExposureNotes = document.createElement('td');
		this._ElmSampleTime = document.createElement('td');
		this._ElmAnalyticalMethod = document.createElement('td');

		this._StartDate = undefined;

		this.validator = new validateAnalyticalMethodLimits();
	}

	connectedCallback()
	{
		if ( !this._Built )
		{
			let TdSelected = document.createElement('td');
			TdSelected.append(this._ElmSelected);

			if(this._ShowSelection)
			{
				this.append(
					TdSelected
				);
			}

			this.append(
				this._ElmSampleId, this._ElmSamplingPoint, this._ElmStartDateTime,
				this._ElmExposureNotes, this._ElmSampleTime, this._ElmAnalyticalMethod
			);

			this.style.backgroundColor = undefined; //this._calcColor();

			this._Built = true;
		}
	}

	_calcColor()
	{
		let response = undefined;
		let aVal = this.AnalyticalMethod;

		response = this.validator.analyticalMethodChecker(aVal,this.SampleTime)

		if (response === true)
		{
			return ( this._ElmSampleTime ) ? "Yellow" : null;
		}
		
		return null;
	}

	set Selected(val)
	{
		this._ElmSelected.checked = val;
	}

	get Selected()
	{
		return this._ElmSelected.checked;
	}

	set SampleID(val)
	{
		this._ElmSampleId.innerText = val;
	}

	get SampleID()
	{
		return this._ElmSampleId.textContent;
	}

	set SamplingPoint(val)
	{
		this._ElmSamplingPoint.innerText = val;
	}

	get SamplingPoint()
	{
		return this._ElmSamplingPoint.textContent;
	}

	set StartTime(val)
	{
		if ( !(val instanceof Datetime) )
			throw new Error("Start Time needs to be a Datetime.");

		this._StartDate = val;

		this._ElmStartDateTime.innerText = val;
	}

	get StartTime()
	{
		return this._StartDate;
	}

	set ExposureNotes(val)
	{
		this._ElmExposureNotes.innerText = val;
	}

	get ExposureNotes()
	{
		return this._ElmExposureNotes.textContent;
	}

	set SampleTime(val)
	{

		this._ElmSampleTime.innerText = val.toString();
		this.style.backgroundColor = this._calcColor();
	}

	get SampleTime()
	{
		return this._ElmSampleTime.textContent;
	}

	set AnalyticalMethod(val)
	{
		this._ElmAnalyticalMethod.innerText = val;
	}

	get AnalyticalMethod()
	{
		return this._ElmAnalyticalMethod.textContent;
	}

	static fromObjData(record, _ShowSelection)
	{
		let ret = new EnhancedHraSampleSelectRow(_ShowSelection);
		ret.SampleID = record.id_doehrs_sample_id;
		ret.SamplingPoint = record.sampling_point;
		ret.StartTime = record.start_date_time;
		ret.ExposureNotes = record.exposure_notes;
		ret.AnalyticalMethod = record.analytical_method;
		ret.SampleTime = record.sample_time;
		ret.Selected = record.selected;

		return ret;
	}
}

customElements.define('jhrm-ehra-sample-select-row', EnhancedHraSampleSelectRow, { extends: 'tr' });

export class EnhancedHraSampleSelectTable extends HTMLElement
{
	constructor()
	{
		super();

		
		this._Built = false;

		this._ElmHeaderSelected = document.createElement('input');
		this._ElmHeaderSelected.type = 'checkbox';

		this._ShowSelection = this.getAttribute('show-selection') == "true";

		let createTH = (text) =>
		{
			let elm = document.createElement('th');
			elm.innerHTML = text;

			return elm;
		}

		let createCheckBoxTH = (text) =>
		{
			
			let TdHeaderSelected = document.createElement('th');			
			TdHeaderSelected.append(text, this._ElmHeaderSelected);

			return TdHeaderSelected;
		}


		this._ElmHeader = document.createElement('thead');
		this._ElmBody = document.createElement('tbody');

		if(this._ShowSelection)
		{
			this._ElmHeader.append(createCheckBoxTH('Sel. '));
		}

		this._ElmHeader.append(
			createTH('DOEHRS Sample ID'),
			createTH('Sampling Point'),
			createTH('Start Date/Time (UTC)'),
			createTH('Exposure Notes'),
			createTH('Sample Time (Min)'),
			createTH('Analytical Method')
		);
		
		this._ElmTable = document.createElement('table');
		this._ElmTable.append(this._ElmHeader, this._ElmBody);
		this._ElmTableWrapper = document.createElement('div');
		this._ElmTableWrapper.append(this._ElmTable);
		Sortable(this._ElmTable);
	}

	connectedCallback()
	{
		if (!this._Built)
		{
			this._ElmTableWrapper.className="tablewrapper";
			this.append(this._ElmTableWrapper);
			this._Built = true;
		}

		this._ElmHeaderSelected.onclick = function()
			{
				this.filterSelectCase(this._ElmHeaderSelected.checked);
				
			}.bind(this);
	}

	appendSampleRow(row)
	{
		if (row instanceof EnhancedHraSampleSelectRow)
			this._ElmBody.append(row);
	}

	async populateFromHra(hraId, selectedOnly)
	{
		// make sure empty first
		this._ElmBody.innerHTML = "";

		let params = selectedOnly ? {selected : "true"} : null;
		let results = await Request.get('/api/enhancedhra/hra/' + hraId + '/samples', params);	
		results.forEach(record => {
			let nextRow = EnhancedHraSampleSelectRow.fromObjData(record, this._ShowSelection);
			this.appendSampleRow(nextRow);
		});

		document.getElementById("hSampleSelection").innerHTML = "Sample Selection (Total Samples: " + results.length + ")"
	}

	filterSelectCase(selectAll)
	{
		let tBody = this._ElmBody;

		if(selectAll === true)
		{
			for (let i = 0; i < tBody.children.length; ++i)
			{	
				tBody.children.item(i).Selected = true;	
			}
		}
		else
		{
			for (let i = 0; i < tBody.children.length; ++i)
			{	
				tBody.children.item(i).Selected = false;	
			}
		}
	}

	get SelectedIDs()
	{
		let tBody = this._ElmBody;
		let ret = [];

		for (let i = 0; i < tBody.children.length; ++i)
		{
			let element = tBody.children.item(i);

			if (element.Selected)
				ret.push(element.SampleID);
		}

		return ret;
	}
}

customElements.define('jhrm-ehra-sample-select-table', EnhancedHraSampleSelectTable);
