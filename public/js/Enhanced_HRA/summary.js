import { RiskAssessment } from "../risk_assessment.js"
import { MaterialIcon } from "../material_icon.js"
import { Request } from "../request.js"
import { Login } from "../login.js"
import {EHRA_STATUS} from "/js/shared/Constants.js"
import { Sortable } from "../sortable.js";
import { Filterable } from "../filterable.js";
import { Datetime } from "../shared/datetime.js"

export class EnhancedHraSummaryRow extends HTMLTableRowElement
{
	constructor()
	{
		super();

		this._Built = false;
		this._IsSubRow = false;
		this._ElmId = document.createElement('td');
		this._ElmLocation = document.createElement('td');
		this._ElmExposurePathway = document.createElement('td');
		this._ElmChemicalOfConcern = document.createElement('td');
		this._ElmCasrn = document.createElement('td');
		this._ElmAcuteRiskLevel = document.createElement('td');
		this._ElmChronicRiskLevel = document.createElement('td');
		this._ElmCreatedDate = document.createElement('td');
		this._ElmCreatedBy = document.createElement('td');
		this._ElmStatus = document.createElement('td');
		this._ElmUploadedToDoehrs = document.createElement('td');
		this._ElmQA = document.createElement('td');
		this._ElmDuplicate = document.createElement('td');
		this._CreatedDate = undefined;
		this._UploadedToDoehrs = false;
		this._Id = undefined;

		this.Levels = RiskAssessment.RiskLevel;
		this._SubRows = [];
	}

	connectedCallback()
	{
		if ( !this._Built )
		{
			let CopyIcon = MaterialIcon.Create("file_copy");
			CopyIcon.style.cursor = "pointer ";
			CopyIcon.onclick = function()
			{
				Request.post(
					'./api/enhancedhra/hra/' + this.Id +'/duplicate',
					{
						created_by : Login.FirstName + " " + Login.LastName
					}
				).then(
					function(res)
					{
						let QueryParams = new URLSearchParams();
						QueryParams.set("ehra", res.hra_id);

						window.location = "./new_ehra.html?" + QueryParams.toString();
					}.bind(this),
					function (err)
					{
						window.alert("Duplicate failed: " + err.message);
					}.bind(this)
				)
			}.bind(this);

			this._ElmDuplicate.append(CopyIcon);

			this.append(
				this._ElmId, this._ElmLocation,
				this._ElmExposurePathway, this._ElmChemicalOfConcern,
				this._ElmCasrn,
				this._ElmAcuteRiskLevel, this._ElmChronicRiskLevel,
				this._ElmCreatedDate, this._ElmCreatedBy,
				this._ElmStatus, this._ElmUploadedToDoehrs, 
				this._ElmQA, this._ElmDuplicate
			);
			this._Built = true;
		}


		// This is intentionally outside of the _Built guard! 
		// Whenever this row is moved, sorted, or replaced, it should re-render
		// its sub rows and re-stripe itself. Striping cannot be dne with CSS 
		// in tables with rowspans. Which is lame. 
		if(!this.SubRow)
		{
			if([].slice.call(this.parentNode.childNodes).filter(row=>!row.SubRow).indexOf(this) % 2 == 0)
			{
				this.style.backgroundColor = "#ffffff";
			}
			else 
			{
				this.style.backgroundColor = "#e3e5e8";
			}

			this._SubRows.forEach(row=>{
				row.style.backgroundColor = this.style.backgroundColor;
				this.parentNode.insertBefore(row, this.nextSibling);
				this.updateRowspans();
			});
		}
	}

	/**
	 * Set a row as a subrow of this one. It will always be displayed after this
	 * row in the table. 
	 * 
	 * @param {EnhancedHraSummaryRow} row 
	 */
	addSubRow(row)
	{
		if (row instanceof EnhancedHraSummaryRow)
		{
			row.makeSubRow();
			this._SubRows.push(row);
			row.style.backgroundColor = this.style.backgroundColor;
			this.parentNode.insertBefore(row, this.nextSibling);
			this.updateRowspans();
		}
	}

	/**
	 * This method updates the rowspans for this row to take into account any 
	 * subrows. 
	 * 
	 */
	updateRowspans()
	{
		this._ElmId.rowSpan = 
		this._ElmLocation.rowSpan = 
		this._ElmExposurePathway.rowSpan = 
		this._ElmCreatedDate.rowSpan = 
		this._ElmCreatedBy.rowSpan = 
		this._ElmStatus.rowSpan = 
		this._ElmUploadedToDoehrs.rowSpan = 
		this._ElmQA.rowSpan = 
		this._ElmDuplicate.rowSpan = 
		this.Rows;
	}

	/**
	 * This method causes a row to hide all the duplicate table cells which will 
	 * be filled in by rowspan expanded cells from the parent. 
	 * 
	 * Subrows are non-sortable. 
	 */
	makeSubRow()
	{
		this.SubRow = true;
		this.classList.add('nosort');
		this._ElmId.style.display =
		this._ElmLocation.style.display =
		this._ElmExposurePathway.style.display =
		this._ElmCreatedDate.style.display =
		this._ElmCreatedBy.style.display =
		this._ElmStatus.style.display =
		this._ElmUploadedToDoehrs.style.display =
		this._ElmQA.style.display =
		this._ElmDuplicate.style.display="none";
		this._ElmChemicalOfConcern.style.borderTop =
		this._ElmCasrn.style.borderTop =
		this._ElmAcuteRiskLevel.style.borderTop =
		this._ElmChronicRiskLevel.style.borderTop = "1px dashed #999"
		
	}

	/**
	 * Return the total number of rows that this row encompases, including 
	 * itself and any subrows. 
	 */
	get Rows()
	{
		return this._SubRows.length + 1;
	}

	get SubRow()
	{
		return this._IsSubRow;
	}

	get SubRows()
	{
		return this._SubRows;
	}

	set SubRow(val)
	{
		this._IsSubRow = val;
	}

	set Id(val)
	{	
		this._Id = val.ID;
		this._IDStatus = val.Status

		if (this._IDStatus === EHRA_STATUS.COMPLETE)
		{
			let searchParams = new URLSearchParams();
			
			let anchorElement = document.createElement('a');
			anchorElement.href = "./ehra_print_report.html?ehra=" + this._Id;
			anchorElement.target = "_blank"
			anchorElement.innerText = this._Id;

			this._ElmId.innerHTML = "";
			this._ElmId.append(anchorElement);

		}
		else 
		{
			let searchParams = new URLSearchParams();
			searchParams.append("ehra", this._Id);
			searchParams.append("page", "DetailsAndPathwaySelectionPage");

			let queryString = "?" + searchParams.toString();

			let anchorElement = document.createElement('a');
			anchorElement.href = "./new_ehra.html" + queryString;
			anchorElement.innerText = this._Id;

			this._ElmId.innerHTML = "";
			this._ElmId.append(anchorElement);
		}
	}

	get Id()
	{
		return this._Id;
	}

	set Location(val)
	{
		this._ElmLocation.innerText = val;
	}

	get Location()
	{
		return this._ElmLocation.textContent;
	}

	set ExposurePathway(val)
	{
		this._ElmExposurePathway.innerText = val;
	}

	get ExposurePathway()
	{
		return this._ElmExposurePathway.textContent;
	}

	set ChemicalOfConcern(val)
	{
		this._ElmChemicalOfConcern.innerText = val;
	}

	get ChemicalOfConcern()
	{
		return this._ElmChemicalOfConcern.textContent;
	}

	set CASRN(val)
	{
		this._ElmCasrn.innerText = val;
	}

	get CASRN()
	{
		return this._ElmCasrn.textContent;
	}

	set AcuteRiskLevel(val)
	{
		if (val.acuteRisk == this.Levels.ExtremelyHigh)
		{	
			this._ElmAcuteRiskLevel.style.backgroundColor="Black";
			this._ElmAcuteRiskLevel.style.color = "White"
		}
		else if (val.acuteRisk == this.Levels.High)
		{
			this._ElmAcuteRiskLevel.style.backgroundColor="Red";
		}
		else if (val.acuteRisk == this.Levels.Moderate)
		{
			this._ElmAcuteRiskLevel.style.backgroundColor="Yellow";
		}
		else if (val.acuteRisk == this.Levels.Low)
		{
			this._ElmAcuteRiskLevel.style.backgroundColor="Green";
		}
		
		this._ElmAcuteRiskLevel.innerText = val.acuteRisk + '\n' + val.acuteConfidence;
	}

	get AcuteRiskLevel()
	{
		return this._ElmAcuteRiskLevel.textContent;
	}

	set ChronicRiskLevel(val)
	{
		if (val.averageRisk == this.Levels.ExtremelyHigh)
		{
			this._ElmChronicRiskLevel.style.backgroundColor = "Black";
			this._ElmChronicRiskLevel.style.color = "White";
		}
		else if (val.averageRisk == this.Levels.High)
		{
			this._ElmChronicRiskLevel.style.backgroundColor = "Red";
		}
		else if (val.averageRisk == this.Levels.Moderate)
		{
			this._ElmChronicRiskLevel.style.backgroundColor = "Yellow";
		}
		else if (val.averageRisk == this.Levels.Low)
		{
			this._ElmChronicRiskLevel.style.backgroundColor = "Green";
		}
		
		this._ElmChronicRiskLevel.innerText = val.averageRisk + '\n' + val.averageConfidence;
	}

	get ChronicRiskLevel()
	{
		return this._ElmChronicRiskLevel.textContent;
	}

	set CreatedDate(val)
	{
		if ( !(val instanceof Datetime) )
			throw new Error("Start Time needs to be a Date.");

		this._CreatedDate = val;

		this._ElmCreatedDate.innerText = val;
	}

	get CreatedDate()
	{
		return this._CreatedDate;
	}

	set CreatedBy(val)
	{
		this._ElmCreatedBy.innerText = val;
	}

	get CreatedBy()
	{
		return this._ElmCreatedBy.textContent;
	}

	set Status(val)
	{
		
		if (val.status === EHRA_STATUS.COMPLETE)
		{
			let searchParams = new URLSearchParams();
			searchParams.append("hra", val.id);
			let anchorElement = document.createElement('a');
			Request.get('./api/user')
			.then(user => {
				if(user.role.hra_qa && val.approved_by == null) {
					anchorElement.href = "./hra_qa.html?id=" + this.Id;
					anchorElement.innerText = "Start QA";
				} 
				else {
					anchorElement.href = "./sshra_audit.html?" + searchParams.toString();
					anchorElement.innerText = val.status;
				}

				this._ElmStatus.innerHTML = "";
				this._ElmStatus.append(anchorElement);
			})	
		}
		else 
		{
			this._ElmStatus.innerText = val.status;
		}
	}

	get Status()
	{
		return this._IDStatus;
	}

	set UploadedToDoehrs(val)
	{		
		this._UploadedToDoehrs = val;
		this._ElmUploadedToDoehrs.innerText = (val) ? "Yes" : "No";	
	}

	get UploadedToDoehrs()
	{
		return this._UploadedToDoehrs;
	}

	set Qa(val)
	{
		this._ElmQA.innerText = "No"

		if (val)
		{
			let anchorElement = document.createElement('a');
			anchorElement.innerText = "Yes"
			anchorElement.href = "./hra_qa.html?id=" + this.Id + "&qa_approved=" + anchorElement.innerText;
			// placeholder link for read-only qa review page

			this._ElmQA.innerText = "";
			this._ElmQA.append(anchorElement);
		}	
	}

	get Qa()
	{
		return this._ElmQA.textContent;
	}

}

customElements.define('jhrm-ehra-summary-row', EnhancedHraSummaryRow, { extends: 'tr' });

export class EnhancedHraSummaryTable extends HTMLElement
{
	constructor()
	{
		super();

		this._Built = false;

		let createTH = (text) =>
		{
			let elm = document.createElement('th');
			elm.innerHTML = text;

			return elm;
		}

		this._ElmHeader = document.createElement('thead');
		this._ElmBody = document.createElement('tbody');

		this._ElmHeader.append(
			createTH('HRA ID'),
			createTH('Location'),
			createTH('Exposure Pathway'),
			createTH('Chemical of Concern'),
			createTH('CASRN'),
			createTH('Acute Risk Level (Confidence Level)'),
			createTH('Chronic Risk Level (Confidence Level)'),
			createTH('Created Date'),
			createTH('Created By'),
			createTH('Status'),
			createTH('Uploaded To DOEHRS'),
			createTH('QA Approved'),
			createTH('') // Duplication column
		);
		
		this._ElmTable = document.createElement('table');
		this._ElmTable.append(this._ElmHeader, this._ElmBody);
		this._ElmTableWrapper = document.createElement('div');
		this._ElmTableWrapper.append(this._ElmTable);

		Request.get('/api/enhancedhra/hra').then(response=>{
			response.forEach(record =>
			{
				let nextRow = new EnhancedHraSummaryRow();
				nextRow.Id = {ID: record.id, Status : record.status};
				nextRow.Location = record.location;
				nextRow.ExposurePathway = record.exposure_pathway_name;
				nextRow.ChemicalOfConcern = record.chemical_name;
				nextRow.CASRN = record.cas_number;	
				if (record.status === 'INCOMPLETE' || record.status === null)
				{
					nextRow.AcuteRiskLevel = {
						acuteRisk : '',
						acuteConfidence : ''
					};
					nextRow.ChronicRiskLevel = {
						averageRisk : '',
						averageConfidence : ''
					};
				}
				else
				{
					nextRow.AcuteRiskLevel = record.acute_peak_risk ? {
						acuteRisk : record.acute_peak_risk,
						acuteConfidence : '(' + record.acute_peak_confidence + ')'
					} : {
						acuteRisk : 'None',
						acuteConfidence : ''
					};
					nextRow.ChronicRiskLevel = record.chronic_average_risk ? {
						averageRisk : record.chronic_average_risk,
						averageConfidence : '(' + record.chronic_average_confidence + ')'
					} : {
						averageRisk : 'None',
						averageConfidence : ''
					};
				}
				nextRow.CreatedDate = record.created_on;
				nextRow.CreatedBy = record.created_by;
				nextRow.Status = record;
				nextRow.UploadedToDoehrs = (record.uploaded_to_doehrs == 1 ? true : false);
				nextRow.Qa = (record.approved_by != null);

				let existing = this.getRowById(nextRow.Id)
				if(existing)
				{
					existing.addSubRow(nextRow);
				}
				else 
				{
					this.appendSummaryRow(nextRow);
				}
				
			});
		}).catch(err=>{
			// Do nothing for now. 
		});

		Sortable(this._ElmTable);
		new Filterable(this._ElmTable);
	}

	connectedCallback()
	{
		if (!this._Built)
		{
			this._ElmTableWrapper.className = "tablewrapper";
			this.append(this._ElmTableWrapper);
			this._Built = true;
		}
	}

	appendSummaryRow(row)
	{
		if (row instanceof EnhancedHraSummaryRow)
			this._ElmBody.append(row);
	}

	getRowById(id)
	{
		let tBody = this._ElmBody;

		for (let i = 0; i < tBody.children.length; ++i)
		{
			let element = tBody.children.item(i);

			if(element.Id == id)
			{
				return element;
			}
		}

		return false;
	}

	// return array of HRA ids that are ready for QA
	async getRowsPendingQA()
	{
		let idsPendingQA = [];
		await Request.get('/api/enhancedhra/hra').then(
			response=>{
				response.forEach(record =>
				{
					if (record.status === 'COMPLETE' && record.approved_by === null && !idsPendingQA.includes(record.id))
					{
						idsPendingQA.push(record.id);
					}
				});
			}
		).catch(err=>{
			// Do nothing for now. 
		});
		return idsPendingQA;
	}
}

customElements.define('jhrm-ehra-summary-table', EnhancedHraSummaryTable);