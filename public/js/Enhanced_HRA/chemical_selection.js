import { Request } from "../request.js";
import { MaterialIcon } from "../material_icon.js"
import { JhrmTooltip } from "../tooltip.js"
import { Sortable } from "../sortable.js"
import { JhrmAlign } from "../align.js"

export class EnhancedHraChemicalSelectRow extends HTMLTableRowElement
{
	constructor()
	{
		super();

		this._Built = false;

		this._ElmSelected = document.createElement('input');
		this._ElmSelected.type = 'checkbox';

		this._TdSelected = document.createElement('td');
		this._TdSelected.append(this._ElmSelected);

		this._ElmSampleId = document.createElement('td');
		this._ElmChemicalName = document.createElement('td');
		this._ElmCasNumber = document.createElement('td');
		this._ElmConcentration = document.createElement('td');
		this._ElmUnits = document.createElement('td');
		this._ElmAnalyticalMethod = document.createElement('td');
		this._ElmExceedsPreScreeningMeg = document.createElement('td');

		this._ExceedsPreScreenMeg = false;
		this._HasMegSamples = true;

		this._onSelectChanged = function(rowElement) {
			// Known empty function
		};
	}

	connectedCallback()
	{
		if ( !this._Built )
		{
			this.append(
				this._TdSelected, this._ElmSampleId, this._ElmChemicalName,
				this._ElmCasNumber, this._ElmConcentration,
				this._ElmUnits, this._ElmAnalyticalMethod,
				this._ElmExceedsPreScreeningMeg
			);

			this._ElmSelected.onchange = function()
			{
				this._onSelectChanged(this);
			}.bind(this);

			this.style.backgroundColor = this._calcColor();

			this._Built = true;
		}
	}

	_calcColor()
	{
		if (this._HasMegSamples)
		{
			return ( this._ExceedsPreScreenMeg ) ? "LightCoral" : null;
		}
		else
		{
			return "Yellow";
		}
	}

	set SelectEnabled(val)
	{
		this._ElmSelected.disabled = !val;
	}

	get SelectEnabled()
	{
		return !this._ElmSelected.disabled;
	}

	set Selected(val)
	{
		this._ElmSelected.checked = val;
	}

	get Selected()
	{
		return this._ElmSelected.checked && this._HasMegSamples;
	}

	set SampleId(val)
	{
		this._ElmSampleId.innerText = val;
	}

	get SampleId()
	{
		return this._ElmSampleId.textContent;
	}

	set ChemicalName(val)
	{
		this._ElmChemicalName.innerText = val;
	}

	get ChemicalName()
	{
		return this._ElmChemicalName.textContent;
	}

	set CasNumber(val)
	{
		this._ElmCasNumber.innerText = val;
	}

	get CasNumber()
	{
		return this._ElmCasNumber.textContent;
	}

	set Concentration(val)
	{
		this._ElmConcentration.innerText = val;
	}

	get Concentration()
	{
		return this._ElmConcentration.textContent;
	}

	set Units(val)
	{
		this._ElmUnits.innerText = val;
	}

	get Units()
	{
		return this._ElmUnits.textContent;
	}

	set AnalyticalMethod(val)
	{
		this._ElmAnalyticalMethod.innerText = val;
	}

	get AnalyticalMethod()
	{
		return this._ElmAnalyticalMethod.textContent;
	}

	set ExceedsPreScreeningMeg(val)
	{
		this._ExceedsPreScreenMeg = val;
		this._ElmExceedsPreScreeningMeg.innerText = ( val ) ? "Yes" : "No";

		this.style.backgroundColor = this._calcColor();
	}

	get ExceedsPreScreeningMeg()
	{
		return this._ExceedsPreScreenMeg;
	}

	set onSelectChange(func)
	{
		this._onSelectChanged = func;
	}

	set ID(val)
	{
		this._Id = val;
	}

	get ID()
	{
		return this._Id;
	}

	set Visible(val)
	{
		this.style.display = val ? "table-row" : "none";
	}

	get Visible()
	{
		let style = window.getComputedStyle(this);
		return (style.display != "none");

	}

	set HasMegSamples(val)
	{
		if (val == this._HasMegSamples)
			return;

		let WasSelected = this.Selected;

		this._HasMegSamples = val;
		this._TdSelected.innerHTML = "";

		if ( val )
		{
			this._TdSelected.append(this._ElmSelected);
		}
		else
		{
			let ErrorIcon = MaterialIcon.Create("error");
			ErrorIcon.style.fontSize = "125%";
			ErrorIcon.style.color = "Red";

			let Tooltip = new JhrmTooltip();
			Tooltip.innerText = "No Prescreen MEG";
			Tooltip.Target = ErrorIcon;

			this._TdSelected.append(ErrorIcon, Tooltip);

		}

		if (this._built)
		{
			this.style.backgroundColor = this._calcColor();

			if ( WasSelected != this.Selected )
				this._onSelectChanged(this);
		}
	}

	get HasMegSamples()
	{
		return this._HasMegSamples;
	}

	static fromObjData(record)
	{
		let ret = new EnhancedHraChemicalSelectRow();
		ret.SampleId = record.id_doehrs_sample_id;
		ret.ChemicalName = record.chemical_name;
		ret.CasNumber = record.cas_number;
		ret.Concentration = record.concentration;
		ret.Units = record.units;
		ret.AnalyticalMethod = record.analytical_method;
		ret.ExceedsPreScreeningMeg = ( record.exceeds_1_year_neg.toLowerCase() == "yes" );
		ret.ID = record.id;
		ret.Selected = record.selected;
		ret.HasMegSamples = record.found_meg;
		return ret;
	}
}

customElements.define('jhrm-ehra-chemical-select-row', EnhancedHraChemicalSelectRow, { extends: 'tr' });

export class EnhancedHraChemicalSelectTable extends HTMLElement
{
	constructor()
	{
		super();

		this._Built = false;

		let createTH = (text) =>
		{
			let elm = document.createElement('th');
			elm.innerHTML = text;

			return elm;
		}

		this._ChemSelect = document.createElement('select');
		this._ElmHeader = document.createElement('thead');
		this._ElmBody = document.createElement('tbody');

		this._LayoutExceedsCheck = document.createElement('div');
		this._LayoutExceedsCheck.style.display = "inline-grid";
		this._LayoutExceedsCheck.style.gridTemplateColumns = "min-content min-content";
		this._LayoutExceedsCheck.style.marginLeft = "1.5em";
		this._LayoutExceedsCheck.innerHTML = `
			<jhrm-align middle style="grid-column: 1;"><input id="exceed_check" type="checkbox"></input></jhrm-align>
			<jhrm-align middle style="grid-column: 2; white-space: nowrap;">Select All Chemicals Exceeding Pre-Screen MEG</jhrm-align>
		`;

		this._ElmExceedCheck = this._LayoutExceedsCheck.querySelector('#exceed_check');
		this._ElmExceedCheck.id = null;

		this._ElmHeader.append(
			createTH('Sel.'),
			createTH('DOEHRS Sample ID'),
			createTH('Chemical Name'),
			createTH('CAS Number'),
			createTH('Concentration'),
			createTH('Units'),
			createTH('Analytical Method'),
			createTH('Exceeds the Pre-Screening MEG')
		);

		this._ElmTable = document.createElement('table');
		this._ElmTableWrapper = document.createElement('div');
		this._ElmTable.append(this._ElmHeader, this._ElmBody);
		this._ElmTableWrapper.append(this._ElmTable);
		this._SampleIDs = new Set();

		this._BlockChemSelectChangeHandler = false;

		Sortable(this._ElmTable);
	}

	connectedCallback()
	{
		if (!this._Built)
		{
			this.style.display = "grid";
			this.style.gridTemplateColumns = "auto 1fr";
			this.style.gridTemplateRows = "auto 1fr";
			this.style.rowGap = "0.5rem";

			this._ChemSelect.style.gridRow = "1";
			this._ChemSelect.style.gridColumn = "1";

			this._LayoutExceedsCheck.style.gridRow = "1";
			this._LayoutExceedsCheck.style.gridColumn = "2";

			this._ElmTableWrapper.style.gridRow = "2";
			this._ElmTableWrapper.style.gridColumn = "1/3";
			this._ElmTableWrapper.className = "tablewrapper";

			this.append(this._ChemSelect, this._LayoutExceedsCheck, this._ElmTableWrapper);
			this._Built = true;

			this._ChemSelect.onchange = function ()
			{
				if (!this._BlockChemSelectChangeHandler)
				{
					let optionText = this._ChemSelect.options[this._ChemSelect.selectedIndex].text;
					this.filter(optionText == '(all)' ? undefined : optionText);

					this._RaiseChangeEvent();
				}
			}.bind(this);

			this._ElmExceedCheck.onchange = function ()
			{
				this._BlockChemSelectChangeHandler = true;

				let ExceedChecked = this._ElmExceedCheck.checked;
				let tBody = this._ElmBody;

				if (ExceedChecked)
				{
					this._ChemSelect.value = '(all)';
					this._ChemSelect.disabled = true;

					let ExceedChemicals = new Set();

					for (let i = 0; i < tBody.children.length; ++i)
					{
						let element = tBody.children.item(i);

						if (element.ExceedsPreScreeningMeg)
							ExceedChemicals.add(element.ChemicalName);
					}

					for (let i = 0; i < tBody.children.length; ++i)
					{
						let element = tBody.children.item(i);
						let hasChemical = ExceedChemicals.has(element.ChemicalName);

						element.Selected = hasChemical;
						element.Visible = hasChemical;
					}
				}
				else
				{
					for (let i = 0; i < tBody.children.length; ++i)
					{
						let element = tBody.children.item(i);
						element.Visible = true;
					}

					this._ChemSelect.disabled = false;
				}

				this._RaiseChangeEvent();
				this._BlockChemSelectChangeHandler = false;
			}.bind(this);
		}
	}

	_RaiseChangeEvent()
	{
		this.dispatchEvent( new Event("change") );
	}

	async populateFromHra(hraId)
	{
		this._ElmBody.innerHTML = "";

		let results = await Request.get('/api/enhancedhra/hra/'+hraId+'/results');
		let chemicals = new Set();

		let row_select_change_handler = function(row)
		{
			if ( this._BlockChemSelectChangeHandler )
				return;

			if ( row.Selected )
			{
				if ( this._ChemSelect.disabled )
					return;

				this._ChemSelect.value = row.ChemicalName;
				this._ChemSelect.disabled  = true;
				this.filter(row.ChemicalName);
			}
			else
			{
				if ( !this._ElmExceedCheck.checked && this.SelectedIDs.length == 0 )
					this._ChemSelect.disabled  = false;
			}

			this._RaiseChangeEvent();
		}.bind(this);

		results.forEach (
			record =>
			{
				let nextRow = EnhancedHraChemicalSelectRow.fromObjData(record);
				this._ElmBody.append(nextRow);

				nextRow.onSelectChange = row_select_change_handler;
				chemicals.add(nextRow.ChemicalName);
			}
		);

		this._ChemSelect.innerHTML = "";

		let allElm = document.createElement('option');
		allElm.innerText = "(all)";
		this._ChemSelect.append(allElm);

		for(let option of chemicals)
		{
			let elm = document.createElement('option');
			elm.innerText = option;

			this._ChemSelect.append(elm);
		}
	}

	getChemicals()
	{
		let tBody = this._ElmBody;
		let ret = new Set();

		for (let i = 0; i < tBody.children.length; ++i)
		{
			let element = tBody.children.item(i);
			ret.add(element.ChemicalName);
		}

		let retArray = Array.from(ret);
		retArray.sort();

		return retArray;
	}

	filter(chemical)
	{
		let tBody = this._ElmBody;
		let ret = [];

		if (undefined == chemical)
		{
			for (let i = 0; i < tBody.children.length; ++i)
			{
				let element = tBody.children.item(i);
				element.Visible = true;
			}
		}
		else
		{
			let val;
			for (let i = 0; i < tBody.children.length; ++i)
			{
				let element = tBody.children.item(i);
				element.Visible = (element.ChemicalName == chemical);
				if (element.Visible)
				{
					tBody.children.item(i).Selected = true;
				}
			}
		}
	}

	get SelectedIDs()
	{
		let tBody = this._ElmBody;
		let ret = [];

		for (let i = 0; i < tBody.children.length; ++i)
		{
			let element = tBody.children.item(i);

			if (element.Selected && element.Visible)
				ret.push(element.ID);
		}

		return ret;
	}

	get SelectedIDExceedsMEG()
	{
		let tBody = this._ElmBody;
		let ret = [];

		for (let i = 0; i < tBody.children.length; ++i)
		{
			let element = tBody.children.item(i);

			if (element.Selected && element.Visible)
				ret.push(element.ExceedsPreScreeningMeg);
		}

		return ret;
	}

	get HasChemicalsExceedingMeg()
	{
		return !![...this._ElmBody.children].find(row=>row.ExceedsPreScreeningMeg)
	}
}

customElements.define('jhrm-ehra-chemical-select-table', EnhancedHraChemicalSelectTable);
