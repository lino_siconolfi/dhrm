
/**
 * Object that is used to store properties of a JhrmSelect option.
 *
 * Internal Documentation
 */
export class JhrmSelectHandle
{
	constructor(index, value, option, select)
	{
		this._index = index;
		this._value = value;
		this._option = option;
		this._select = select;
		Object.seal(this);
	}

	get Style()
	{
		return this._option.style;
	}

	set Label(val)
	{
		this._option.innerHTML = val;
	}

	get Label()
	{
		return this._option.innerHTML;
	}

	set Value(val)
	{
		if (val == this._value)
			return;

		this._value = val;

		if (this._select.selectedIndex == this._index)
			this._select._RaiseChangeEvent();
	}

	get Value()
	{
		return this._value;
	}

	get Index()
	{
		return this._index;
	}

	get Disabled()
	{
		return this._option.disabled;
	}

	set Disabled(val)
	{
		this._option.disabled = val;
	}
}

export class JhrmSelect extends HTMLElement
{
	constructor()
	{
		super();

		this.style.display = "inline-grid";

		this._Handles = [];
		this._onChange = undefined;
		this._elmSelect = document.createElement('select');
		this._elmSelect.style.cssText = "grid-row: 1; grid-column: 1;";
		this.append(this._elmSelect);

		this._elmSelect.onchange = this._RaiseChangeEvent.bind(this);
	}

	_RaiseChangeEvent(original_event)
	{
		let currSel = this.Value;

		if (this._onChange)
			this._onChange(currSel);

		var event = new CustomEvent(
			"change",
			{
				detail: currSel
			}
		);

		this.dispatchEvent(event);

		if ( original_event )
			original_event.stopPropagation();
	}

	connectedCallback()
	{
    // Known empty function
	}

	clear()
	{
		this._elmSelect.innerHTML = '';
		this._Handles = [];
	}

	/**
	 * Adds an option to the select element.
	 *
	 * @param {*} text
	 *   The text to show for the option.
	 *
	 * @param {*} value
	 *   The value to associate for the option.
	 */
	addOption(text, value, disabled)
	{
		let send_change = ( this._Handles.length == 0);

		let elm_option = document.createElement('option');
		elm_option.text = text;

		if ( disabled == JhrmSelect.DISABLED )
			elm_option.disabled = true;

		let handle = new JhrmSelectHandle(this._Handles.length, value, elm_option, this);
		this._Handles.push(handle);

		this._elmSelect.add(elm_option, this._Handles.length - 1);

		if ( send_change )
			this._RaiseChangeEvent();

		return handle;
	}

	/**
	 * Adds options by using the properties in the passed object to add options
	 * to the select.  The property names will be added as the text for the option
	 * and the values for the property will be the associated value.
	 *
	 * @param { object } obj
	 *
	 * @param { Object } options
	 *
	 * @param { bool } options.AutoSpace
	 *   If true, camel case names attempt to auto space each word for property names
	 *   for the name listed in the select box.
	 */
	addOptions(obj, options)
	{
		let names = Object.getOwnPropertyNames(obj);
		let ret = [];

		for(let i = 0; i < names.length; ++i)
		{
			let selName = names[i];

			if (options && options.AutoSpace)
				selName = selName.replace(/([a-z0-9])([A-Z])/g, '$1 $2');

			ret.push( this.addOption(selName, obj[names[i]]) );
		}

		return ret;
	}

	/**
	 * Gets the value of the current selection.
	 */
	get Value()
	{
		let idx = this._elmSelect.selectedIndex;
		return ( 0 <= idx ) ? this._Handles[idx].Value : null;
	}

	/**
	 * Sets the currently selected value, or throws an exception if none of the
	 * options represent that value. This does NOT look at the text of the options.
	 */
	set Value(val)
	{
		if (val == this.Value)
			return;

		for(let i = 0; i < this._Handles.length; ++i)
		{
			if (this._Handles[i].Value == val)
			{
				this._elmSelect.selectedIndex = i;
				this._RaiseChangeEvent();

				return;
			}
		}

		throw new Error("Value not found.");
	}

	/**
	 * Gets the text of the current selection.
	 */
	get Text()
	{
		let idx = this._elmSelect.selectedIndex;
		return ( 0 <= idx ) ? this._Handles[idx].Label : null;
	}

	/**
	 * Sets the selection using the text of the
	 */
	set Text(txt)
	{
		if (txt == this.Text)
			return;

		for(let i = 0; i < this._Handles.length; ++i)
		{
			if (this._Handles[i].Label == txt)
			{
				this._elmSelect.selectedIndex = i;
				this._RaiseChangeEvent();

				return;
			}
		}

		throw new Error("Value not found.");
	}

	/**
	 * Gets the index of the selected item.
	 */
	get Index()
	{
		return this._elmSelect.selectedIndex;
	}

	/**
	 * Sets the selection by index and raises a change event.  If idx is out of
	 * range or already selected, nothing will happen.
	 */
	set Index(idx)
	{
		if ( idx != this._elmSelect.selectedIndex && idx >= 0 && idx < this._Handles.length )
		{
			this._elmSelect.selectedIndex = idx;
			this._RaiseChangeEvent();
		}
	}

	/**
	 * Sets the change handler.  Changes in the selection will cause the handler to
	 * be called with the new value as its single argument.
	 */
	set onchange(func)
	{
		this._onChange = func;
	}

	/**
	 * Returns the number of selectable objects.
	 */
	get length()
	{
		return this._Handles.length;
	}
}

JhrmSelect.DISABLED = 'DISABLED';

customElements.define('jhrm-select', JhrmSelect);
