import { JhrmSwapSpace } from "./swap_space.js"
import { JhrmAlign } from "./align.js"

let confidence_table = `
	<table>
	<caption>Articulating the Level of Confidence (or Uncertainty) in the Risk Estimate</caption>
		<thead>
			<tr>
				<th>Confidence</th>
				<th>Criteria</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>High</td>
				<td>
					Field sampling data quality is very good – substantial samples over time/space.<br>
					Field activity patterns are well known.<br>
					True exposures are reasonably approximated.<br>
					No critical missing information.<br>
					The predicted health outcomes are highly plausible (strong toxicological weight of evidence/human data) are already demonstrated.
				</td>
			</tr>
			<tr>
				<td>Medium</td>
				<td>
					Field data quality is relatively good.<br>
					Estimates of field exposure are likely to be greater than true exposures due to incomplete data coverage relative to actual exposure<br>
					Detailed information is lacking regarding true personnel activity patterns in the field.<br>
					Predicted health outcomes are plausible; there is toxicological data but limited weight of evidence /human data is lacking.
				</td>
			</tr>
			<tr>
				<td>Low</td>
				<td>
					Important data gaps and/or inconsistencies exist.<br>
					Exposure conditions are not well defined.<br>
					Field personnel activity patterns are no well-defined.<br>
					Predicted health outcomes are not plausible because it is not consistent with real-world events/experience.
				</td>
			</tr>
		</tbody>
	</table>
`;

export class JhrmHelp extends HTMLElement
{
	constructor()
	{
		super();

		let border_radius = "0.3rem";

		let styleButton = function(elm)
		{
			elm.style.cursor = "pointer";
			elm.style.padding = "0.15em 1em";
			elm.style.marginLeft = "2em";
			elm.style.backgroundColor = "#1b365d";
			elm.style.borderRadius = border_radius;
			elm.style.color = "white";
			elm.style.textDecoration = "none";
			elm.style.textAlign = "center";
			elm.style.minWidth = "6em";
		};

		this._elmSwap = new JhrmSwapSpace();
		this._elmSwap.SizeToCurrentPage = true;

		this._elmHelpLayout = document.createElement('div');
		this._elmHelpLayout.style.display = "grid";
		this._elmHelpLayout.style.backgroundColor = "rgba(10,10,10,0.7)";
		this._elmHelpLayout.style.padding = "0.5em";

		this._elmTitleText = new JhrmAlign();
		this._elmTitleText.Alignment = JhrmAlign.Left;
		this._elmTitleText.Alignment = JhrmAlign.Middle;
		this._elmTitleText.style.whiteSpace = "nowrap";
		this._elmTitleText.style.color = "white";
		this._elmTitleText.style.height = "100%";
		this._elmTitleText.style.width = "100%";
		this._elmTitleText.style.fontWeight = "bolder";
		this._elmTitleText.style.fontSize = "1.25em";
		this._elmTitleText.style.backgroundColor = "rgba(0,0,0,0.0);"
		this._elmTitleText.style.gridRow = "1";
		this._elmTitleText.style.gridColumn = "1";

		this._elmCloseButton = document.createElement('div');
		styleButton(this._elmCloseButton);
		this._elmCloseButton.innerText = "Close";

		this._elmHelpTab = document.createElement('div');
		this._elmHelpTab.style.display = "block";
		this._elmHelpTab.style.cursor = "pointer";
		this._elmHelpTab.style.color = "white";
		this._elmHelpTab.style.fontWeight = "bolder";
		this._elmHelpTab.style.backgroundColor = "#4d4d4d";
		this._elmHelpTab.style.padding = "0.2em 1.5em";
		this._elmHelpTab.style.whiteSpace = "nowrap";
		this._elmHelpTab.style.fontSizeAdjust = "0.9";
		this._elmHelpTab.innerHTML = "Help &#x25bc;";

		let open_tab_container = new JhrmAlign();
		open_tab_container.style.width = "100%";
		open_tab_container.style.height = "100%";
		open_tab_container.Alignment = JhrmAlign.Right;
		open_tab_container.Alignment = JhrmAlign.Top;
		open_tab_container.append(this._elmHelpTab);

		this._elmHelpText = document.createElement('div');
		this._elmHelpText.style.display = "inline-block";
		this._elmHelpText.style.marginTop = "0.5em";
		this._elmHelpText.style.padding = "0.5em";
		this._elmHelpText.style.maxHeight = "66vh";
		this._elmHelpText.style.maxWidth = "50vw";
		this._elmHelpText.style.overflow = "auto";

		let help_text_container = document.createElement('div');
		help_text_container.style.gridRow = "2";
		help_text_container.style.gridColumn = "1/3";
		help_text_container.style.backgroundColor = "white";
		help_text_container.style.border = "0.2em solid white";
		help_text_container.style.borderRadius = border_radius;
		help_text_container.style.padding = "0.25em";
		help_text_container.style.marginTop = "0.5em";
		help_text_container.style.height = "auto";
		help_text_container.append(this._elmHelpText);


		let close_button_container = new JhrmAlign();
		close_button_container.style.gridRow = "1";
		close_button_container.style.gridColumn = "2";
		close_button_container.Alignment = JhrmAlign.Right;
		close_button_container.Alignment = JhrmAlign.Middle;
		close_button_container.append(this._elmCloseButton);

		this._elmHelpLayout.append(this._elmTitleText, close_button_container, help_text_container);
		this._elmSwap.appendPage(open_tab_container);
		this._elmSwap.appendPage(this._elmHelpLayout);

		this._built = false;
	}

	connectedCallback()
	{
		if (!this._built)
		{
			this.style.display = "block";

			if ( this.hasAttribute('heading') )
				this._elmTitleText.innerText = this.getAttribute('heading');

			let Children = [];
			for (let i = 0; i < this.childNodes.length; ++i)
				Children.push(this.childNodes[i]);

			this._elmHelpText.innerHTML = "";
			this._elmHelpText.append(...Children);

			this._elmHelpTab.onclick = function()
			{
				this._elmSwap.next();
			}.bind(this);

			this._elmCloseButton.onclick = function()
			{
				this._elmSwap.previous();
			}.bind(this);

			this.append(this._elmSwap);

			this._built = true;
		}
	}

	set Heading(text)
	{
		this._elmTitleText.innerText = text;
	}

	set Content(val)
	{
		if (val instanceof HTMLElement)
		{
			this._elmHelpText.innerHTML = "";
			this._elmHelpText.append(val);
		}
		else if ((typeof val) == 'string')
		{
			this._elmHelpText.innerHTML = val;
		}
		else
		{
			throw new Error("Help content must be an HTMLElement or a string");
		}
	}

	set Visible(val)
	{
		this.style.display = val ? "block" : "none";
	}
}

customElements.define('jhrm-help', JhrmHelp);

/**
 * Chemical Exposure Health Risk Assessments from Direct Reading Instruments
 */
export class JhrmCeHrafromDri extends HTMLElement
{
	constructor()
	{
		super();

		this.Title = "Chemical Exposure Health Risk Assessments from Direct Reading Instruments Tool Tips";

		this._built = false;
	}
	
	connectedCallback()
	{
		if (!this._built)
		{
			this.innerHTML = "";
			this.innerHTML = 
				`
					<p>Peak and Average Table Guidance and Tool Tips</p>

					<p>Tool Tips – use these tips when hovering over the topic.</p>
					
					<p>Severity: Severity category for the population exposure (Ref. TG230 Exhibit 3-1 & 3-4).
					Rank Hazard Probability: Hazard probability is ranked by jointly considering four hazard probability factors: degree of exposure, representativeness of the field data, duration
					of exposure, and rate of exposure (Ref. TG230, Exhibit 3-3).</p>
					
					<p>Degree of Exposure: Measure of how much greater the population exposure estimate is compared to the MEG. The higher an exposure is above the MEG, the higher
					probability that the severity level health outcome will occur (Ref. TG230, Exhibit 3-3).
					Representativeness of Field Data: Measure of the how representative the field-collected data is of the true exposure concentration (Ref. TG230, Exhibit 3-3). Select
					whether the field data adequately, underestimates, or overestimates population exposure (see guidance below).</p>
					
					<p>Duration of Exposure: A ratio of the populations’ exposure duration is relative to the exposure duration used to develop the MEG (Ref. TG230, Exhibit 3-3)</p>
					
					<p>Rate of Exposure: Measure of how different the population’s rate of exposure (e.g., inhalation rate, water consumption rate, soil contact rate) is relative to the assumed rate
					used to develop the MEG (Ref. TG230, Exhibit 3-3). Select the appropriate rate of exposure (see guidance below).</p>
					
					<p>Overall Risk Level Guidance: Select the confidence level for the overall risk assessment (Ref. TG230, Table 3-6) (see guidance below).</p>
					
					<p>Representativeness of Field Data<br>
					Score this factor from the options below by using exposure assessment principles and professional judgment to determine if the field-collected data adequately represents
					the population exposure or if the field data likely under- or over-estimates exposure. Considerations include the sampling design, analytical method detection uncertainties,
					sample size, and data coverage across the exposure duration. A score of 2 is typically expected, especially if there is little-to-no information on any bias in the field data.</p>

					<table>
						<thead>
							<tr>
								<th>Option</th>
								<th>Factor Score</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Field data overestimates the population exposure</td>
								<td>1</td>
							</tr>
							<tr>
								<td>Field data adequately estimates population expsosure</td>
								<td>2</td>
							</tr>
							<tr>
								<td>Field data underestimates the population exposure</td>
								<td>3</td>
							</tr>
						</tbody>
					</table>

					<p>Rate of Exposure<br>
					Score this factor from the options below by making a professional judgment of the actual field conditions associated with population exposure at the site. A score of 2 is typically
					expected, especially if little-to-no information about exposure rate is available.</p>

					<table>
						<thead>
							<tr>
								<th>Level of activity related to inhalation rate</th>
								<th>Factor</th>
							</tr>
							<tr>
								<td>Light exertion. Standing in foxhole. Guard duty. Desk work. Vehicle driving.</td>
								<td>1</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Typical exertion. Equipment maintenance. March with load-bearing equipment no rucksack. (Use this)</td>
								<td>2</td>
							</tr>
							<tr>
								<td>Heavy exertion. Forced load carriage march with 20-kg load. Repetitive lifting and carrying heavy</td>
								<td>3</td>
							</tr>
						</tbody>
					</table>

					<p>Overall Risk Level Guidance<br>
					Data quality, including both sampling data, information that inform exposure parameters, and available health effects data will have a direct impact on the confidence in the risk
					assessment. Indicators of confidence are given in the table below.</p>

					${confidence_table}
				</div>
			`;

			this._built = true;
		}
	}
}

customElements.define('jhrm-ce-hra-from-dri-help', JhrmCeHrafromDri);

export class PeakAvgTableRef extends HTMLElement
{
	constructor()
	{
		super();

		this._built = false;
	}

	connectedCallback()
	{
		if (!this._built)
		{
			this.innerHTML = "";
			this.innerHTML = `
				<h2>Rate of Exposure</h2>
				<p>Score this factor from the options below by making a professional judgment of the actual field conditions
				associated with population exposure at the site. A score of 2 is typically expected, especially if little-to-no
				information about exposure rate is available.</p>

				
				<h2>Overall Risk Level Guidance</h2>
				<p>Data quality, including both sampling data, information that inform exposure parameters, and available
				health effects data will have a direct impact on the confidence in the risk assessment. Indicators of
				confidence are given in the table below.</p>

				${confidence_table}
			`;

			this._built = true;
		}
	}
}

customElements.define('peak-avg-table-ref-help', PeakAvgTableRef);

export class TacticalRiskDefinitionsTable extends HTMLElement
{
	constructor()
	{
		super();

		//  style="margin:2em 0; width:48%; float:left; margin-right: 1.5%;"

		this.innerHTML = 
			`
				<table style="border-collapse: collapse;">
					<caption style="font-weight: bold; text-align: left; font-size: 1.2em; margin-bottom: 0.5em;">Tactical (Acute) Risk Definitions Table</caption>
					<tr>
						<th style="background-color: #4d4d4d; color: white;">Risk Level</th>
						<th style="background-color: #4d4d4d; color: white;">Consequences to Military Operations and Force Readiness</th>
					</tr>
					<tr>
						<td style="background-color: black; color: white;">Extremely High</td>
						<td style="text-align: left;">Loss of ability to accomplish the mission if hazards occur during mission. Notable
							in-theater medical countermeasures and resources anticipated. For example, protection, treatment, and
							exposure documentation. 
						</td>
					</tr>
					<tr>
						<td style="background-color: red; color: white;">High</td>
						<td style="text-align: left;">                
							Significant degradation of mission capabilities in terms of the required mission standard, inability to
							accomplish all parts of the mission, or inability to complete the mission to standard if hazards occur
							during the mission. Some in-theater medical countermeasures and resources anticipated. For example,
							protection, treatment, and exposure documentation.
						</td>
					</tr>
					<tr>
						<td style="background-color: yellow;">Moderate</td>
						<td style="text-align: left;">
							Expected degraded mission capabilities in terms of the required mission standard and will result in reduced
							mission capability if hazards occur during the mission. Limited in-theater medical countermeasures and
							resources anticipated. For example, protection, treatment, and exposure documentation.
						</td>
					</tr>
					<tr>
						<td style="background-color:green;">Low</td>
						<td style="text-align: left;">                
							Expected losses have little or no impact on accomplishing the mission. Little to no in-theater medical
							resources anticipated for protection and treatment. However, a summary of any negative or low level sampling
							results should be documented and archived particularly if some personnel express concerns.
						</td>
					</tr>
				</table>
			`;

		let style_border = (element) =>
		{
			element.style.border = "2px solid black";
			element.style.padding = "0.5em";
		};

		let td = [].slice.call(this.querySelectorAll("td"));
		td.forEach(style_border);

		let th = [].slice.call(this.querySelectorAll("th"));
		th.forEach(style_border);
	}
}

customElements.define('tactical-risk-definitions-table', TacticalRiskDefinitionsTable);

export class LifecycleRiskDefinitionsTable extends HTMLElement
{
	constructor()
	{
		super();

		// style="margin: 2em 0; width: 48%; float: right; margin-left: 1.5%;"

		this.innerHTML = 
			`
				<table style="border-collapse: collapse;">
					<caption style="font-weight: bold; text-align:left; font-size: 1.2em; margin-bottom: 0.5em;">Lifecycle (Chronic) Risk Definitions Table</caption>
					<tr>
						<th style="background-color: #4d4d4d; color: white;">Risk Level</th>
						<th style="background-color: #4d4d4d; color: white;">Consequences to Military Operations and Force Readiness</th>
					</tr>
					<tr>
						<td style="background-color: black; color: white;">Extremely High</td>
						<td style="text-align: left;">
							Significant future medical surveillance activities and medical provider resources anticipated.
							Documentation of environmental data in designated DoD archive and designate a registry to actively track the exposed
							personnel. Conduct specific active surveillance and/or medical follow-up procedures for life- cycle of identified group.
						</td>
					</tr>
					<tr >
						<td style="background-color: red; color: white;">High</td>
						<td style="text-align: left;">
							Notable future medical surveillance activities and related resources anticipated. Documentation of
							environmental data in designated DoD archive. Specific identification and documentation of the exposed personnel. Possible
							passive medical surveillance related activities.
						</td>
					</tr>
					<tr >
						<td style="background-color: yellow;">Moderate</td>
						<td style="text-align: left;">Limited future medical surveillance activities and related resources anticipated. Documentation of
							environmental data in designated DoD archive. Consider documenting exposed groups or personnel of surveillance interest.
						</td>
					</tr>
					<tr >
						<td style="background-color:green;">Low</td>
						<td style="text-align: left;">
							No specific medical action required. Documentation of environmental data in designated DoD archive.
						</td>
					</tr>
				</table>
			`;

		let style_border = (element) =>
		{
			element.style.border = "2px solid black";
			element.style.padding = "0.5em";
		};

		let td = [].slice.call(this.querySelectorAll("td"));
		td.forEach(style_border);

		let th = [].slice.call(this.querySelectorAll("th"));
		th.forEach(style_border);
	}
}

customElements.define('lifecycle-risk-definitions-table', LifecycleRiskDefinitionsTable);

export class ConfidenceTable extends HTMLElement
{
	constructor()
	{
		super();

		// style="margin: 2em 0; width: 48%; float: right; margin-left: 1.5%;"

		this.innerHTML = confidence_table;
		let style_border = (element) =>
		{
			element.style.border = "2px solid black";
			element.style.padding = "0.5em";
		};

		let td = [].slice.call(this.querySelectorAll("td"));
		td.forEach(style_border);

		let th = [].slice.call(this.querySelectorAll("th"));
		th.forEach(style_border);

		this.querySelector('table').style.borderCollapse = 'collapse';

	}
}

customElements.define('confidence-table', ConfidenceTable);