import { JhrmRadioGroup } from "../radio_group.js"
import { selectAndStripId } from "../utility.js"
import { Sortable } from "../sortable.js"
import { Request } from "../request.js"
import { PushTextEdit } from "../form_components.js"
import { JhrmHelpIcon } from "../help_icon.js"

/**
 * { Integer } Id
 * { string } DodId
 * { string } FirstName
 * { string } LastName
 * { string } Grade
 * { string } MosAfsc
 * { string } Unit
 * { IssuanceSensor[] } IssuedSensors
 */
export class Person
{
	/**
	 * Sets Sensor fields to their default values on the passed object.
	 */
	static setDefaults(obj)
	{
		obj.Id = undefined;
		obj.DodId = "";
		obj.FirstName = "";
		obj.LastName = "";
		obj.Grade = "";
		obj.MosAfsc = "";
		obj.Unit = "";
		obj.IssuedSensors = [];
	}

	/**
	 * Copies Sensor fields present from src into dest.  Any missing fields in src are ignored
	 * and left at their original values in dest.
	 * 
	 * @param { object } src
	 * @param { object } dest
	 */
	static copyFields(src, dest)
	{
		if ( src.Id )
			dest.Id = src.Id;

		if ( src.DodId )
			dest.DodId = src.DodId;

		if ( src.FirstName )
			dest.FirstName = src.FirstName;
			
		if ( src.LastName )
			dest.LastName = src.LastName;
			
		if ( src.Grade )
			dest.Grade = src.Grade;
			
		if ( src.MosAfsc )
			dest.MosAfsc = src.MosAfsc;
			
		if ( src.Unit )
			dest.Unit = src.Unit;

		if ( src.IssuedSensors )
			dest.IssuedSensors = src.IssuedSensors;
	}

	/**
	 * Creates a Person object from the corresponding fields on the backend.
	 * 
	 * @param { object } obj
	 * @param { boolean } show_issued
	 */
	static fromBackendFields(obj)
	{
		let ret = new Person();

		ret.Id = obj.id;
		ret.DodId = obj.dodid;
		ret.FirstName = obj.first_name;
		ret.LastName = obj.last_name;
		ret.MosAfsc = obj.mos_code;
		ret.Grade = obj.grade;
		ret.Unit = obj.unit;		

		return ret;
	}

	constructor(obj)
	{
		Person.setDefaults(this);
		Object.seal(this);

		if (obj)
			Person.copyFields(obj, this);
	}

	/**
	 * Gets an object populated with properties named according to backend conventions.
	 * 
	 * @returns { object }
	 */
	toBackendFields()
	{
		return {
			id : this.Id,
			dodid : this.DodId,
			first_name : this.FirstName,
			last_name : this.LastName,
			mos_code : this.MosAfsc,
			grade : this.Grade,
			unit :this.Unit
		};
	}

	static async getAllPeople(refresh)
	{
		if ( refresh || !Person._all_persons )
		{
			let people = await Request.get('/api/issuance/personnel');

			Person._all_people = people.map(
				(person) => { return Person.fromBackendFields(person); }
			);
		}
		
		return [...Person._all_people];
	}
}

Person._all_people = undefined;


export class PersonnelTableRow extends HTMLTableRowElement
{
	constructor()
	{
		super();
		
		this.innerHTML = `
			<td id="check_area">
				
			</td>
			<td id="dod_id"></td>
			<td id="first_name"></td>
			<td id="last_name"></td>
			<td id="grade"></td>
			<td id="unit"></td>
		`;

		this._check_area = selectAndStripId(this, "check_area");
		this._id = undefined;
		this._dod_id = selectAndStripId(this, "dod_id");
		this._first_name = selectAndStripId(this, "first_name");
		this._last_name = selectAndStripId(this, "last_name");
		this._grade = selectAndStripId(this, "grade");
		this._unit = selectAndStripId(this, "unit");

		this._radio_group = undefined;
	}

	/**
	 * Used by a containing table to set the radio group 
	 */
	set _RadioGroup(val)
	{
		if ( !(val instanceof JhrmRadioGroup))
			throw new Error("Expected argument of type JhrmRadioGroup.");
		
		this._radio_group = val;

		this._check_area.innerHTML = "";
		this._check_area.append(
			this._radio_group.genElement(this)
		);
	}

	/**
	 * @returns { boolean }
	 */
	get Selected()
	{
		return (undefined != this._radio_group) && (this._radio_group.Value == this);
	}

	/**
	 * @returns { Integer }
	 */
	get Id()
	{
		return parseInt(this._id);
	}

	/**
	 * @param { Integer }
	 */
	set Id(val)
	{
		if ( !Number.isInteger(val) )
			throw new Error("Integer value expected.");

		this._id = val;
	}

	/**
	 * @returns { string }
	 */
	get DodId()
	{
		return this._dod_id.innerText;
	}

	/**
	 * @param { string }
	 */
	set DodId(val)
	{
		this._dod_id.innerText = val;
	}

	/**
	 * @param { string }
	 */
	set FirstName(val)
	{
		this._first_name.innerText = val;
	}

	/**
	 * @returns { string }
	 */
	get FirstName()
	{
		return this._first_name.innerText;
	}

	/**
	 * @param { string }
	 */
	set LastName(val)
	{
		this._last_name.innerText = val;
	}

	/**
	 * @returns { string }
	 */
	get LastName()
	{
		return this._last_name.innerText;
	}

	/**
	 * @param { string }
	 */
	set Grade(val)
	{
		this._grade.innerText = val;
	}

	/**
	 * @returns { string }
	 */
	get Grade()
	{
		return this._grade.innerText;
	}

	/**
	 * @param { string }
	 */
	set Unit(val)
	{
		this._unit.innerText = val;
	}

	/**
	 * @returns { string }
	 */
	get Unit()
	{
		return this._unit.innerText;
	}
}

customElements.define('jhrm-personnel-table-row', PersonnelTableRow, { extends : 'tr' });

export class PersonnelTable extends HTMLElement
{
	constructor()
	{
		super();

		this.innerHTML = `
			<div class="tablewrapper">
				<table id="table" style="width: 100%; border: 0; padding: 0; margin: 0;">
					<thead>
						<tr>
							<th></th>
							<th>DOD ID</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Grade</th>
							<th>Unit</th>
						</tr>
					</thead>
					<tbody id="table_body">
					</tbody>
				</table>
			</div>
		`;

		this._table_body = selectAndStripId(this, "table_body");
		this._table = selectAndStripId(this, "table");

		this._selected_radio_group = new JhrmRadioGroup();

		Sortable(this._table);
	}

	/**
	 * Gets an array of all of the Person objects in the table.  The returned
	 * objects can be used to set fields displayed in the table.
	 */
	get Persons()
	{
		return Array.from(this._table_body.children);
	}

	/**
	 * Creates a new person row in the table.
	 * 
	 * @param { Person } person 
	 * 
	 * @returns { Object } An object that has the properties of Person and
	 * can be used to set fields in the table row.
	 */
	addPerson(person)
	{
		if (!person)
			person = new Person();
		
		let row = new PersonnelTableRow();
		row._RadioGroup = this._selected_radio_group;

		Person.copyFields(person, row);
		this._table_body.append(row);

		return row;
	}

	get Selected()
	{
		return this._selected_radio_group.Value;
	}

	set Selected(val)
	{
		this.Persons.forEach(person => {
			if(person.DodId == val.DodId)
			{
				this._selected_radio_group.Value = person;
			}
		});
		
	}
}

customElements.define('jhrm-personnel-table', PersonnelTable);

class PersonnelSearchSelectRow extends HTMLTableRowElement
{
	constructor()
	{
		super();

		this.innerHTML = `
			<td><input type="checkbox" id="check"></input></td>
			<td style="text-align: center;" id="first_name"></td>
			<td style="text-align: center;" id="last_name"></td>
			<td style="text-align: center;" id="dodid"></td>
		`;

		this._checkbox = selectAndStripId(this, "check");
		this._first_name = selectAndStripId(this, "first_name");
		this._last_name = selectAndStripId(this, "last_name");
		this._dodid = selectAndStripId(this, "dodid");

		this._person = new Person();

		this.Visible = true;
	}

	/**
	 * @param { Person }
	 */
	set Person(val)
	{
		this._person = val;

		this._first_name.innerHTML = val.FirstName;
		this._last_name.innerHTML = val.LastName;
		this._dodid.innerHTML = val.DodId;
	}

	/**
	 * @param { Person }
	 */
	get Person()
	{
		return this._person;
	}

	get Selected()
	{
		return this._checkbox.checked;
	}

	set Selected(val)
	{
		this._checkbox.checked = val;
	}

	set Visible(val)
	{
		this.style.display = (val) ? "table-row" : "none";
	}

	get Visible()
	{
		return (this.style.display == "table-row");
	}
}

customElements.define('jhrm-personnel-search-select-row', PersonnelSearchSelectRow,  { extends: 'tr' });

export class PersonnelSearchSelect extends HTMLElement
{
	constructor()
	{
		super();

		this.innerHTML = `
			<div style="display: grid; grid-template-columns: auto 2fr 2fr 1fr; min-width: 40em; grid-gap: 0.75em;">
				<div style="grid-row: 1; grid-column: 1;">Search By: </div>
				<jhrm-push-text-edit style="grid-row: 1; grid-column: 2;" id="search_first_name" placeholder="First Name"></jhrm-push-text-edit>
				<jhrm-push-text-edit style="grid-row: 1; grid-column: 3;" id="search_last_name" placeholder="Last Name"></jhrm-push-text-edit>
				<jhrm-push-text-edit style="grid-row: 1; grid-column: 4;" id="search_dodid" placeholder="DOD ID"></jhrm-push-text-edit>
				<table style="grid-row: 2; grid-column: 1/5" id="main_table">
					<thead>
						<tr>
							<th></th>
							<th>First Name</th>
							<th>Last name</th>
							<th>DOD ID</th>
						</tr>
					</thead>
					<tbody id="table_body">
					</tbody>
				</table>
			</div>
		`;

		this._search_first_name = selectAndStripId(this, "search_first_name");
		this._search_last_name = selectAndStripId(this, "search_last_name");
		this._search_dodid = selectAndStripId(this, "search_dodid");

		this._main_table = selectAndStripId(this, "main_table");
		this._table_body = selectAndStripId(this, "table_body");

		this._search_first_name.onchange = (next_val) => { this._update_filter(); return true; };
		this._search_last_name.onchange = (next_val) => { this._update_filter(); return true; };
		this._search_dodid.onchange = (next_val) => { this._update_filter(); return true; };

		Sortable(this._main_table);
	}

	_update_filter()
	{
		let first_name = this._search_first_name.Text.trim().toLowerCase();
		first_name = ( first_name.length > 0) ? first_name : undefined;

		let last_name = this._search_last_name.Text.trim().toLowerCase();
		last_name = ( last_name.length > 0) ? last_name : undefined;

		let dodid = this._search_dodid.Text.trim().toLowerCase();
		dodid = ( dodid.length > 0) ? dodid : undefined;

		let rows = Array.from(this._table_body.children);

		for( let i = 0; i < rows.length; ++i)
		{
			let lower_first_name = rows[i].Person.FirstName.toLowerCase();
			let lower_last_name = rows[i].Person.LastName.toLowerCase();
			let lower_id = rows[i].Person.DodId.toLowerCase();


			rows[i].Visible = ( !first_name || lower_first_name.includes(first_name) ) &&
			                  ( !last_name || lower_last_name.includes(last_name) ) &&
			                  ( !dodid || lower_id.includes(dodid) );
		}
	}

	/**
	 * @param { Person[] }
	 */
	set Persons(ps)
	{
		this._table_body.innerHTML = "";

		for( let i = 0; i < ps.length; ++i)
		{
			let next_row = new PersonnelSearchSelectRow();
			next_row.Person = ps[i];

			this._table_body.append(next_row);
		}
	}

	get SelectedPersons()
	{
		let rows = Array.from(this._table_body.children);
		let ret = [];

		for( let i = 0; i < rows.length; ++i)
		{
			if (rows[i].Selected && rows[i].Visible)
				ret.push(rows[i].Person);
		}

		return ret;
	}

	/**
	 * Marks the rows with DOD Ids from the passed in array as selected.
	 * 
	 * @param { Array } Array of DOD Ids
	 */
	set SelectedPersons(ps)
	{
		let rows = Array.from(this._table_body.children);

		for(let i = 0; i < rows.length; ++i)
		{
			rows[i].Selected = ps.includes(rows[i].Person.DodId)
		}
	}
}

customElements.define('jhrm-personnel-search-select', PersonnelSearchSelect);