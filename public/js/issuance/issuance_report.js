import { Person } from "./personnel.js"
import { JhrmAlign } from "../align.js"
import { IssuanceSensor } from "./sensor.js"
import { selectAndStripId } from "../utility.js"
import { Request } from "../request.js"
import { JhrmSelect } from "../select.js"
import { Sortable } from "../sortable.js"
import { MaterialIcon } from "../material_icon.js"
import { ISS_DEVICE_TYPE_ID, ISS_WEAR_LOCATION } from "/js/shared/Constants.js";

export class SensorIssuanceReportRow extends HTMLTableRowElement
{
	constructor()
	{
		super();

		this.innerHTML = `
			<td id="name_id"></td>
			<td id="grade"></td>
			<td id="sensor_type"></td>
			<td id="sensor_sn"></td>
			<td id="issue_datetime"></td>
			<td id="wearer_location"></td>
			<td id="return_datetime"></td>
			<td id="return_status"></td>
		`;

		this._person = undefined;
		this._sensor = undefined;
		this._issue_date = undefined;
		this._return_date = undefined;

		this._name_id = selectAndStripId(this, "name_id");
		this._grade = selectAndStripId(this, "grade");
		this._sensor_type = selectAndStripId(this, "sensor_type");
		this._sensor_sn = selectAndStripId(this, "sensor_sn");
		this._issue_datetime = selectAndStripId(this, "issue_datetime");
		this._wearer_location = selectAndStripId(this, "wearer_location");
		this._return_datetime = selectAndStripId(this, "return_datetime");
		this._return_status = selectAndStripId(this, "return_status");
	}

	/**
	 * 
	 * @param { Date } date 
	 * @returns { string }
	 */
	_toDateString(date)
	{
		return     date.getUTCDate().toString().padStart(2, '0') + "/"
		        + (date.getUTCMonth() + 1).toString().padStart(2, '0') + "/"
		        +  date.getUTCFullYear() + " "
		        +  date.getUTCHours().toString().padStart(2, '0')
		        +  date.getUTCMinutes().toString().padStart(2, '0')
		        +  "Z";
	}

	/**
	 * @returns { Person }
	 */
	get Person()
	{
		return this._person;
	}

	/**
	 * @param { Person }
	 */
	set Person(person)
	{
		this._person = person;

		this._name_id.innerHTML = `${person.DodId} / ${person.LastName}, ${person.FirstName}`;
		this._grade.innerHTML = person.Grade;
	}

	/**
	 * @returns { IssuanceSensor }
	 */
	get Sensor()
	{
		return this._sensor;
	}

	/**
	 * @param { IssuanceSensor }
	 */
	set Sensor( sensor )
	{
		this._sensor = sensor;

		this._sensor_type.innerHTML =  ISS_WEAR_LOCATION.toString(sensor.WearLocation);
		this._sensor_sn.innerHTML = sensor.SerialNumber;
	}

	/**
	 * @returns { Date }
	 */
	get IssueDatetime()
	{
		return this._issue_date;
	}

	/**
	 * @param { Date }
	 */
	set IssueDatetime(date)
	{
		this._issue_date = date;
		this._issue_datetime.innerHTML = (date) ? this._toDateString(date) : "";
	}

	/**
	 * @returns { Date }
	 */
	get ReturnDatetime()
	{
		return this._return_date;
	}

	/**
	 * @param { Date }
	 */
	set ReturnDatetime(date)
	{
		this._return_date = date;
		this._return_datetime.innerHTML = (date) ? this._toDateString(date) : "";
	}

	/**
	 * @returns { string }
	 */
	get ReturnedSensorStatus()
	{
		return this._return_status.innerHTML;
	}

	/**
	 * @param { string }
	 */
	set ReturnedSensorStatus(status)
	{
		this._return_status.innerHTML = status;
	}

	/**
	 * @returns { string }
	 */
	get WearerLocation()
	{
		return this._wearer_location.innerHTML;
	}

	/**
	 * @param { string }
	 */
	set WearerLocation(location)
	{
		this._wearer_location.innerHTML = location;
	}
}

customElements.define('jhrm-sensor-issuance-report-row', SensorIssuanceReportRow, { extends: 'tr' });

export class SensorIssuanceReportTable extends HTMLElement
{
	constructor()
	{
		super();

		this.classList.add("tablescroll");

		this.innerHTML = `
			<div class="tablewrapper">
				<table id="table" style="width: 100%;">
					<thead>
						<tr>
							<th>DODID / Individual</th>
							<th>Grade</th>
							<th>Sensor Type</th>
							<th>Sensor SN</th>
							<th>Issued Data/Time</th>
							<th>Wearer Location</th>
							<th>Returned Date/Time</th>
							<th>Returned Sensor Status</th>
						</tr>
					</thead>
					<tbody id="table_body">
					</tbody>
				</table>
			</div>
		`;

		this._table_body = selectAndStripId(this, "table_body");
		this._table = selectAndStripId(this, "table");

		this._only_show_issued = false;

		Sortable(this._table);
	}

	_update_row_display(row)
	{
		let issued = row.IssueDatetime != undefined;
		let returned = row.ReturnDatetime != undefined;
		let show = ( !this._only_show_issued ) || ( issued != returned);
		row.style.display = show ? "table-row" : "none";
	}

	set Unit(unit)
	{
		setTimeout(
			async () =>
			{
				this._unit = unit;
				let issuances = await Request.get('/api/issuance/report/blastgauges', { unit : this._unit });

				this._table_body.innerHTML = "";

				for ( let i = 0; i < issuances.length; ++i )
				{
					let row = new SensorIssuanceReportRow();
					row.Person = Person.fromBackendFields(issuances[i]);
					row.Sensor = IssuanceSensor.fromBackendFields(issuances[i]);
					
					if (issuances[i].issued_date)
						row.IssueDatetime =  new Date(issuances[i].issued_date);
					
					if (issuances[i].wear_location)
						row.WearerLocation =  issuances[i].wear_location;
					
					if (issuances[i].returned_date)
						row.ReturnDatetime =  new Date(issuances[i].returned_date);
					
					if (issuances[i].returned_sensor_status)
						row.ReturnedSensorStatus =  issuances[i].returned_sensor_status;

					this._table_body.append(row);
					this._update_row_display(row);
				}
			}
		);
	}

	get Unit()
	{
		return this._unit;
	}

	set OnlyDisplayIssued(val)
	{
		this._only_show_issued = val;
		let rows = this._table_body.childNodes;

		for (let i = 0; i < rows.length; i++)
			this._update_row_display(rows[i]);
	}

	get OnlyDisplayIssued()
	{
		return this._only_show_issued;
	}
}

customElements.define('jhrm-sensor-issuance-report-table', SensorIssuanceReportTable);

export class SensorIssuanceReport extends HTMLElement
{
	constructor()
	{
		super();

		let now = new Date();
		let date_str = 
			   now.getDate().toString().padStart(2, '0') + "/"
			+ (now.getMonth() + 1).toString().padStart(2, '0') + "/"
			+  now.getFullYear();

		this.innerHTML = `
			<div style="display: grid; grid-template-columns: 1fr auto 1fr;">
				<div style="grid-row: 1; grid-column: 1; padding: 0.5em;">
					Date: ${date_str}<br>
					Unit: <jhrm-select id="unit_select"></jhrm-select><br>
					<label>Only show Issued: <input id="only_show_issued" type="checkbox"></input></label>
				</div>
				<jhrm-align middle center style="grid-row: 1; grid-column: 2; font-weight: bold; font-size: 120%;">EH Sensor Issuance Report</jhrm-align>
				<jhrm-align middle right style="grid-row: 1; grid-column: 3; font-size: 200%;">
					<material-icon icon="cloud_download"></material-icon>
					<material-icon icon="cloud_upload"></material-icon>
					<material-icon icon="print"></material-icon>
				</jhrm-align>
			</div>
			<jhrm-sensor-issuance-report-table id="report_table" style="grid-row: 2; grid-column: 1/4;">
			</jhrm-sensor-issuance-report-table>
		`;
		
		this._report_table = selectAndStripId(this, "report_table");
		this._unit_select = selectAndStripId(this, "unit_select");
		this._only_show_issued = selectAndStripId(this, "only_show_issued");

		this._initialized = false;
		this._unit = undefined;

		this._only_show_issued.onchange = () =>
		{
			this._report_table.OnlyDisplayIssued = this._only_show_issued.checked;
		};

		setTimeout(
			async () =>
			{
				let units = await Request.get('/api/issuance/personnel/distinctunit');
				
				for (let i = 0; i < units.length; i++)
					this._unit_select.addOption(units[i].unit, units[i].unit);

				if ( this._unit )
				{
					this._unit_select.Value = this._unit;
					this._unit = undefined;
				}
				
				this._report_table.Unit = this._unit_select.Value;

				this._unit_select.onchange = () =>
				{
					this._report_table.Unit = this._unit_select.Value;
				}

				this._initialized = true;
			}
		);
	}

	/**
	 * @param { string }
	 */
	set Unit(unit)
	{
		if ( this._initialized )
			this._unit_select.Value = unit;
		else
			this._unit = unit;
	}

	get Unit()
	{
		return ( this._initialized ) ? this._unit_select.Value : this._unit;
	}
}

customElements.define('jhrm-sensor-issuance-report', SensorIssuanceReport);