'use strict';

import { JhrmSelect } from "../select.js"
import { selectAndStripId } from "../utility.js"
import { Request } from "../request.js"
import { ISS_DEVICE_TYPE_ID, ISS_WEAR_LOCATION } from "/js/shared/Constants.js";


/**
 * { string } Id
 * { ISS_DEVICE_TYPE_ID } Type
 * { ISS_WEAR_LOCATION } WearLocation
 * { string } LocationOnWearer
 * { string } Manufacturer
 * { string } SerialNumber
 * { string } LotNumber
 * { string } ModelNumber
 * { string } SWVersion
 * { string } HWVersion
 * { string } Status
 * { Date } ManufactureDate
 */
export class IssuanceSensor
{
	/**
	 * Sets Sensor fields to their default values on the passed object.
	 */
	static setDefaults(obj)
	{
		obj.Id = undefined;
		obj.Type = ISS_DEVICE_TYPE_ID.NONE;
		obj.WearLocation = ISS_WEAR_LOCATION.OTHER,
		obj.LocationOnWearer = "";
		obj.Manufacturer = "";
		obj.SerialNumber = "";
		obj.LotNumber = "";
		obj.ModelNumber = "";
		obj.SWVersion = "";
		obj.HWVersion = "";
		obj.Status = "";
		obj.ManufactureDate = undefined;
	}

	/**
	 * Copies Sensor fields present from src into dest.  Any missing fields in src are ignored
	 * and left at their original values in dest.
	 * 
	 * @param { object } src
	 * @param { object } dest
	 */
	static copyFields(src, dest)
	{
		if ( src.Id )
			dest.Id = src.Id;

		if ( src.Type )
			dest.Type = src.Type;
			
		if ( src.WearLocation )
			dest.WearLocation = src.WearLocation;
			
		if ( src.LocationOnWearer )
			dest.WearLocation = src.LocationOnWearer;

		if ( src.Manufacturer )
			dest.Manufacturer = src.Manufacturer;
			
		if ( src.SerialNumber )
			dest.SerialNumber = src.SerialNumber;
			
		if ( src.LotNumber )
			dest.LotNumber = src.LotNumber;
			
		if ( src.ModelNumber )
			dest.ModelNumber = src.ModelNumber;
			
		if ( src.SWVersion )
			dest.SWVersion = src.SWVersion;
			
		if ( src.HWVersion )
			dest.HWVersion = src.HWVersion;
			
		if ( src.Status )
			dest.Status = src.Status;
			
		if ( src.ManufactureDate )
			dest.ManufactureDate = src.ManufactureDate;
	}

	/**
	 * Creates an IssuanceSensor object from the corresponding fields on the backend.
	 * 
	 * @param { object } obj
	 */
	static fromBackendFields(obj)
	{
		let ret = new IssuanceSensor();

		ret.Id = obj.id;
		ret.Type = obj.iss_device_type_id;
		ret.WearLocation = obj.device_type;
		ret.SerialNumber = obj.serial_number;
		ret.Manufacturer = obj.manufacturer;
		ret.ModelNumber = obj.model_number;
		ret.LotNumber = obj.lot_number;
		ret.Status = obj.status;

		return ret;
	}

	constructor(obj)
	{
		IssuanceSensor.setDefaults(this);
		Object.seal(this);

		if (obj)
			copyFields(obj, this);
	}

	/**
	 * Gets all issuance sensors that have been registered from the backend;
	 * 
	 * @returns { IssuanceSensor[] }
	 */
	static async getInventory(refresh)
	{
		if ( refresh || !IssuanceSensor._Inventory )
		{
			let items = await Request.get('/api/issuance/inventory');

			IssuanceSensor._Inventory = items.map(
				(item) => { return IssuanceSensor.fromBackendFields(item); }
			);
		}
		
		return [...IssuanceSensor._Inventory];
	}
}

IssuanceSensor._Inventory = undefined;

export class SensorSerialSelect extends HTMLElement
{
	constructor()
	{
		super();

		this.innerHTML = `
			<jhrm-select id="select" style="width: 100%;"></jhrm-select>
		`;

		this._select = selectAndStripId(this, "select");

		setTimeout( async () => { this.Sensors = await IssuanceSensor.getInventory(); } );
	}

	/**
	 * @returns { IssuanceSensor }
	 */
	get Value()
	{
		return this._select.Value
	}

	/**
	 * @param { String } val 
	 */
	set Value(val)
	{
		this._select.Text = val;
	}

	/**
	 * @param { IssuanceSensor[] }
	 */
	set Sensors( sensors )
	{
		let sorted = [...sensors].sort(
			(a, b) => 
			{
				return a.SerialNumber.localeCompare(b.SerialNumber)
			}
		);

		this._select.clear();

		this.CHOOSE_SENSOR = "Choose Sensor";
		this.DEFAULT_VALUE = "default";
		
		this._NonSelectableOption = this._select.addOption(this.CHOOSE_SENSOR, this.DEFAULT_VALUE, JhrmSelect.DISABLED);
		this._select.Value = this.DEFAULT_VALUE;

		let issueWearLocation = ISS_WEAR_LOCATION.fromString(this.parentElement.WearLocation); 

		if (issueWearLocation == ISS_WEAR_LOCATION.HEAD) {
			sorted.forEach(
				element => {
					if ( element.WearLocation == ISS_WEAR_LOCATION.HEAD && element.Status == "SERVICEABLE"){
						this._select.addOption(element.SerialNumber, element);
					} 
				}
			);
		}

		else if (issueWearLocation == ISS_WEAR_LOCATION.CHEST) {
			sorted.forEach(
				element => {
					if ( element.WearLocation == ISS_WEAR_LOCATION.CHEST && element.Status == "SERVICEABLE"){
						this._select.addOption(element.SerialNumber, element); 
					} 
				}
			);
		}

		else if (issueWearLocation == ISS_WEAR_LOCATION.SHOULDER) {
			sorted.forEach(
				element => {
					if ( element.WearLocation == ISS_WEAR_LOCATION.SHOULDER && element.Status == "SERVICEABLE"){
						this._select.addOption(element.SerialNumber, element); 
					} 
				}
			);
		}

		// if other 
		else {
			// make "choose sensor" selectable
			this._select.clear();
			this._select.addOption(this.CHOOSE_SENSOR, this.DEFAULT_VALUE, false);
			this._select.Value = this.DEFAULT_VALUE;

			// check the selected values of all sibling issuance slots
			let issues = this.parentElement.parentElement.children;
			let selectedSensors = [];
			for (var index in issues){
				if (issues[index]._serial_select == this)
					selectedSensors.push(issues[index].Value);
			}
			
			sorted.forEach(
				element => {
					if (element.Status == "SERVICEABLE" && !selectedSensors.includes(element.SerialNumber))
					{
						this._select.addOption(element.SerialNumber, element);
					}
				}
			);
		}	
	}
	
	// reset the serial selection 
	async resetSerialSelect() 
	{
		this.Sensors = await IssuanceSensor.getInventory(true); 
	}
}

customElements.define('jhrm-sensor-serial-select', SensorSerialSelect);