'use strict'

import { JhrmSwapSpace } from "../swap_space.js";
import { JhrmDialog } from "../dialog.js";
import { FormValidator } from "../validate.js";
import { JhrmRadioGroup } from "../radio_group.js";
import { IssuanceSensor } from "./sensor.js";
import { Request } from "../request.js"
import {
    ISS_WEAR_LOCATION,
    ISS_DEVICE_TYPE_ID
} from "../shared/Constants.js";

export class JhrmIssuanceSensorDialog extends HTMLElement {

	constructor()
	{
		super();
		this.innerHTML = `
            <jhrm-dialog id="sensor_dialog">
                <h3>
                    Add / Edit <jhrm-select id="fields_select"></jhrm-select>
                </h3>
                <form id="sensor_form">
                    <div style="display: block; clear: both; margin-bottom: 0.75em;">
                        <div style="font-size: smaller; margin-top: 0.75em;">* required</div>
                    </div>
                    <jhrm-swap-space id="field_swap_space">
                        <div id="blast_gauge_fields">
                            <div class="dialog_field dialog_required">
                                <label for="sensor_dialog_serial_number">Serial Number</label>
                                <input data-validate-required
                                    data-validate-minlen="6"
                                    data-validate-maxlen="6"
                                    class="dialog_input"
                                    id="sensor_dialog_serial_number"
                                    type="text">
                            </div>
                            <div class="dialog_field">
                                <label for="sensor_dialog_manufacturer">Manufacturer</label>
                                <input class="dialog_input" list="sensor_dialog_manufacturer_options" id="sensor_dialog_manufacturer">
                                <datalist id="sensor_dialog_manufacturer_options"></datalist>
                            </div>
                            <div class="dialog_field">
                                <label for="sensor_dialog_model_number">Model Number</label>
                                <input class="dialog_input" list="sensor_dialog_model_number_options" id="sensor_dialog_model_number">
                                <datalist id="sensor_dialog_model_number_options"></datalist>
                            </div>
                            <div class="dialog_field dialog_required">
                                <label for="sensor_dialog_radio_area">Wear Location</label>
                                <div class="dialog_input" data-validate-required id="sensor_dialog_radio_area"></div>
                            </div>
                        </div>
                        <div id="chemical_passive_sampler_fields" disabled>
                        </div>
                        <div id="joint_personal_dosimeter_fields" disabled>
                        </div>
                    </jhrm-swap-space>
                </form>
            </jhrm-dialog>
		`;

        // This is being done here at a stand-in for programatically
        // setting these dropdowns based on values in the database
        // in the future.
        this.querySelector('#sensor_dialog_manufacturer_options').innerHTML = `
            <option>Blackbox Biometrics, Inc</option>
        `;
        this.querySelector('#sensor_dialog_model_number_options').innerHTML = `
            <option>Gen 7.0</option>
        `;

        this.Dialog = this.querySelector("#sensor_dialog");
        this.Form = this.querySelector("form");
        this.SwapSpace = this.querySelector("#field_swap_space");
        this.BlastGauge =
        {
            Id : undefined,
            SerialNumber : this.querySelector("#sensor_dialog_serial_number"),
            Manufacturer : this.querySelector("#sensor_dialog_manufacturer"),
            ModelNumber : this.querySelector("#sensor_dialog_model_number"),
            WearLocation : new JhrmRadioGroup()
        }
        this.FieldSets =
        {
            BlastGauge : this.querySelector("#blast_gauge_fields"),
            ChemicalPassiveSampler : this.querySelector("#chemical_passive_sampler_fields"),
            JpdI : this.querySelector("#joint_personal_dosimeter_fields")
        }
        this.FieldSelect = this.querySelector("#fields_select");
        this.FieldSelect.addOption("Blast Gauge", this.FieldSets.BlastGauge);
        this.FieldSelect.addOption("Chemical Passive Sampler", this.FieldSets.ChemicalPassiveSampler)._option.disabled = true;
        this.FieldSelect.addOption("JPD-I", this.FieldSets.JpdI)._option.disabled = true;
        this.FieldSelect.addOption("Other", this.FieldSets.Other)._option.disabled = true;
        this.FieldSelect.onchange = () =>
        {
            this.SwapSpace.VisibleElement = this.FieldSelect.Value;
        };
        this.RadioArea = this.querySelector("#sensor_dialog_radio_area");
        let ABGWearLocation = this.BlastGauge.WearLocation;
        this.RadioArea.append( ...ABGWearLocation.genElementAndLabel(ISS_WEAR_LOCATION.HEAD, "Head") );
        this.RadioArea.append( ...ABGWearLocation.genElementAndLabel(ISS_WEAR_LOCATION.SHOULDER, "Shoulder") );
        this.RadioArea.append( ...ABGWearLocation.genElementAndLabel(ISS_WEAR_LOCATION.CHEST, "Chest") );

        this.getSensor = () => {
            let ret = new IssuanceSensor();
            let sensor_type = this.SwapSpace.VisibleElement;

            if ( sensor_type == this.FieldSets.BlastGauge )
            {
                ret.Type = ISS_DEVICE_TYPE_ID.BLAST_GAUGE;
                ret.SerialNumber = this.BlastGauge.SerialNumber.value;
                ret.Manufacturer = this.BlastGauge.Manufacturer.value;
                ret.ModelNumber = this.BlastGauge.ModelNumber.value;
                ret.WearLocation = this.BlastGauge.WearLocation.Value;
                ret.Id = this.BlastGauge.Id;
            }
            else
            {
                window.alert("Adding sensor of this type not yet supported.");
            }

            return ret;
        };

        this.Dialog.OnAccept = async () =>
        {
            let sensor = new IssuanceSensor();
            let sensor_type = this.SwapSpace.VisibleElement;

            if ( sensor_type != this.FieldSets.BlastGauge )
            {
                window.alert("Adding sensor of this type not yet supported.");
                return false;
            }

            sensor.Type = ISS_DEVICE_TYPE_ID.BLAST_GAUGE;
            sensor.SerialNumber = this.BlastGauge.SerialNumber.value;
            sensor.Manufacturer = this.BlastGauge.Manufacturer.value;
            sensor.ModelNumber = this.BlastGauge.ModelNumber.value;
            sensor.WearLocation = this.BlastGauge.WearLocation.Value;
            sensor.Id = this.BlastGauge.Id;


            let validator = new FormValidator(sensor_form, {
                inputSelector : '.dialog_input'
            });
            if(!validator.validate())
            {
                alert(validator.errors.join("\n"));
                return false;
            }

            try
            {
                if (sensor.Id)
                {
                    await Request.post(
                        '/api/issuance/inventory/editsensor',
                        {
                            iss_device_type_id : ISS_DEVICE_TYPE_ID.BLAST_GAUGE,
                            serial_number : sensor.SerialNumber,
                            manufacturer : sensor.Manufacturer,
                            model_number : sensor.ModelNumber,
                            device_type : sensor.WearLocation,
                            id : sensor.Id
                        }
                    );
                }
                else
                {
                    await Request.post(
                        '/api/issuance/inventory/blastgauge',
                        {
                            iss_device_type_id : ISS_DEVICE_TYPE_ID.BLAST_GAUGE,
                            serial_number : sensor.SerialNumber,
                            manufacturer : sensor.Manufacturer,
                            model_number : sensor.ModelNumber,
                            device_type : sensor.WearLocation
                        }
                    );
                }
                this.OnSave();
            }
            catch(e)
            {
                console.log(e)
                alert("A Sensor with that Type and Serial Number already exists");
                return false;
            }

            return true;
        };
        this.Dialog.Buttons = JhrmDialog.ButtonSet.SaveCancel;
        this.OnSave = ()=>{
          // Known empty function
        };
	}

	connectedCallback()
	{
		// Known empty function
	}

    /**
     * Edit an existing sensor, or a scanned sensor
     */
    Edit(sensor)
    {
        this.Form.reset();

        this.BlastGauge.Id = sensor.Id || undefined;
        this.BlastGauge.SerialNumber.value = sensor.SerialNumber || '';
        this.BlastGauge.Manufacturer.value = sensor.Manufacturer || '';
        this.BlastGauge.ModelNumber.value = sensor.ModelNumber || '';
        this.BlastGauge.WearLocation.Value = sensor.WearLocation;

        this.BlastGauge.SerialNumber.disabled = true;
        this.FieldSelect.querySelector('select').disabled = true;

        this.Dialog.show();
    }

    /**
     * Manually add a sensor.
     */
    Add()
    {
        this.Form.reset();

        this.BlastGauge.SerialNumber.disabled = false;
        this.FieldSelect.querySelector('select').disabled = false;

        this.Dialog.show();
    }



  };

  customElements.define('jhrm-issuance-sensor-dialog', JhrmIssuanceSensorDialog);
