

import { JhrmSelect } from "../select.js"
import { IssuanceSensor } from "./sensor.js"
import { JhrmDialog } from "../dialog.js"
import { selectAndStripId, removeElement } from "../utility.js"
import { JhrmSwapSpace } from "../swap_space.js"
import { SensorSerialSelect } from "./sensor.js"
import { ISS_DEVICE_TYPE_ID, ISS_ASSOCIATION, ISS_WEAR_LOCATION } from "/js/shared/Constants.js";

/**
 * 
 */
export class BlastGaugeIssuanceEditor extends HTMLElement
{
	constructor()
	{
		super();

		this.style.display = "grid";
		this.style.width = "100%";
		this.style.gridGap = "0.45em";
		this.style.gridTemplateColumns = "1fr auto auto";
		this._verification = ISS_ASSOCIATION.SELECTION;

		this.innerHTML = `
		<div style="grid-row: 1; grid-column: 1;">
				*<span id="body_location_head_text"></span> Gauge Serial #
		</div>
			<div style="grid-row: 1; grid-column: 3;"><span id="location_required_indicator">*</span>Location on Wearer
				<jhrm-help-icon> Enter the location or choose from the dropdown list where the sensor is worn (e.g., back of helmet, opposite shooting shoulder, center of chest). </jhrm-help-icon> 
			</div>
		<jhrm-sensor-serial-select id="serial_select" style="grid-row: 2; grid-column: 1;"></jhrm-sensor-serial-select>
			<jhrm-swap-space id="location_swap" style="grid-row: 2; grid-column: 3;">
				<input id="location_on_wearer" style="min-width: 15em;">
				<jhrm-select id="location_shoulder_select"></jhrm-select>
			</jhrm-swap-space>
			<jhrm-align id="remove_button_container" right style="grid-row: 3; grid-column: 1/4;">
				<button id="remove_button">Remove</button>
			</jhrm-align>
		`;

		this._body_location_head_text = selectAndStripId(this, "body_location_head_text");
		this._serial_select = selectAndStripId(this, "serial_select");
		this._location_on_wearer = selectAndStripId(this, "location_on_wearer");
		this._location_swap = selectAndStripId(this, "location_swap");
		this._location_shoulder_select = selectAndStripId(this, "location_shoulder_select");
		this._location_required_indicator = selectAndStripId(this, "location_required_indicator");

		this._remove_button_container = selectAndStripId(this, "remove_button_container");
		this._remove_button = selectAndStripId(this, "remove_button");

		this._location_shoulder_select.addOption("Select Location", "", JhrmSelect.DISABLED);
		this._location_shoulder_select.addOption("Left Shoulder", "Left Shoulder");
		this._location_shoulder_select.addOption("Right Shoulder", "Right Shoulder");
		this._location_shoulder_select.addOption("Unknown", "Unknown");

		this._serial_select.addEventListener(
			'change',
			() =>
			{
				// when value is changed, refresh all the selection dropdowns
				let issues = this.parentElement.children;
				this._serial_select.resetSerialSelect();
			}
		);

		this._wear_location = ISS_WEAR_LOCATION.OTHER;

		this._remove_button.onclick = () =>
		{
			let dlg = new JhrmDialog();
			dlg.Buttons = JhrmDialog.ButtonSet.OkCancel;
			dlg.innerHTML = "Remove gauge?";

			dlg.OnAccept = () =>
			{
				removeElement(this);
				return true;
			}

			dlg.show();
		}

		this.EnableRemove = false;
		this._location_shoulder_select.Value = "";
	}

	set EnableRemove(val)
	{ 
		this._remove_button.style.display = (val) ? "initial" : "none";
	}

	set onSerialChanged(func)
	{
		this._serial_select.onchange = function()
		{
			func(this._serial_select.Text);
		}.bind(this);
	}

	/**
	 * @returns { String }
	 */
	get Verification()
	{
		return this._verification;
	}

	/**
	 * @param { String }
	 */
	set Verification(val)
	{
		this._verification = val;
	}

	/**
	 * @param { string }	
	 */
	set LocationOnWearer(val)
	{
		if ( this._location_swap.VisibleElement == this._location_on_wearer )
		{
			this._location_on_wearer.value = val;
		}
		else
		{
			try 
			{
				this._location_shoulder_select.Value = val;
			}
			catch (error)
			{

			}
		}
	}

	/**
	 * @returns { string }
	 */
	get LocationOnWearer()
	{
		return (this._location_swap.VisibleElement == this._location_on_wearer) ?
			this._location_on_wearer.value : this._location_shoulder_select.Value;
	}

	/**
	 * @param { string }
	 */
	set SerialNumber(val)
	{
		this._serial_select.Value = val;
	}

	/**
	 * @returns { IssuanceSensor } 
	 * 
	 * @TODO This should return a string
	 */
	get SerialNumber()
	{
		return this._serial_select.Value;
	}

	/**
	 * @returns { ISS_WEAR_LOCATION }
	 */
	set WearLocation(val)
	{
		this._wear_location = val;
		this._body_location_head_text.innerHTML = (val == ISS_WEAR_LOCATION.OTHER ) ? 
			"Additional" : ISS_WEAR_LOCATION.toString(val);
		
		if ( val == ISS_WEAR_LOCATION.SHOULDER )
		{
				this._location_swap.VisibleElement = this._location_shoulder_select;
				this._location_required_indicator.style.visibility = "inherit";
		}
		else
		{
			this._location_swap.VisibleElement = this._location_on_wearer;
			this._location_required_indicator.style.visibility = "hidden";
		}
	}

	/**
	 * @returns { ISS_WEAR_LOCATION }
	 */
	get WearLocation()
	{
		return this._wear_location
	}
}

customElements.define('jhrm-blast-gauge-issuance-editor', BlastGaugeIssuanceEditor);

export class IssuanceEditor extends HTMLElement
{
	constructor()
	{
		super();

		this.innerHTML = `
			<div style="display: grid;">
				<div>*Select Sensor Type
					<jhrm-help-icon> Select the type of sensor you want to issue.
				</jhrm-help-icon></div>
				<jhrm-select id="type_select" style="width: fit-content;"></jhrm-select>
				<div>*Choose Serial Number from dropdown or* Scan</div>
				<jhrm-swap-space id="issues_swap_space">
					<div id="none_issues_area">
					</div>
					<div id="blast_gauge_issues_area" style="display: grid; grid-template-rows: 1fr auto;">
						<div id="blast_gauge_issues" style="grid-row: 1; gird-column: 1; overflow: auto; max-height: 30em;">

						</div>
						<button id="btn_blast_gauge_additional" style="grid-row: 2; gird-column: 1;">+ Additional Gauge</button>
					</div>
				</jhrm-swap-space>
			</div>
		`;

		this._issues_swap_space = selectAndStripId(this, "issues_swap_space");
		this._none_issues_area = selectAndStripId(this, "none_issues_area");
		this._blast_gauge_issues_area = selectAndStripId(this, "blast_gauge_issues_area");
		this._blast_gauge_issues = selectAndStripId(this, "blast_gauge_issues");
		this._btn_blast_gauge_additional = selectAndStripId(this, "btn_blast_gauge_additional");

		this._type_select = selectAndStripId(this, "type_select");
		this._type_select.addOption("(none)", ISS_DEVICE_TYPE_ID.NONE);
		this._type_select.addOption("BLAST GAUGE", ISS_DEVICE_TYPE_ID.BLAST_GAUGE);
		this._type_select.addEventListener(
			'change',
			() =>
			{
				let val = this._type_select.Value;

				if ( ISS_DEVICE_TYPE_ID.NONE == val)
					this._issues_swap_space.VisibleElement = this._none_issues_area
				else if ( ISS_DEVICE_TYPE_ID.BLAST_GAUGE = val)
					this._issues_swap_space.VisibleElement = this._blast_gauge_issues_area;
			}
		);

		this.Add(ISS_DEVICE_TYPE_ID.BLAST_GAUGE, ISS_WEAR_LOCATION.HEAD)
		this.Add(ISS_DEVICE_TYPE_ID.BLAST_GAUGE, ISS_WEAR_LOCATION.SHOULDER)
		this.Add(ISS_DEVICE_TYPE_ID.BLAST_GAUGE, ISS_WEAR_LOCATION.CHEST)

		// @TODO 
		// We will need to deal with the fact that additional gauges can have 
		// other wear locations - for example, there could be a second shoulder 
		// gauge. The easiest way to do this would be to make WearLocation a 
		// select input in BlastGaugeIssuanceEditor
		this._btn_blast_gauge_additional.onclick = () =>
		{
			let editor = this.Add(ISS_DEVICE_TYPE_ID.BLAST_GAUGE, ISS_WEAR_LOCATION.OTHER);
			editor.EnableRemove = true;
		};
	}

	set onTypeChanged(func)
	{
		this._type_select.onchange = function()
		{
			func(this.Type);
		}.bind(this);
	}

	get Issues()
	{
		return {
			BlastGauge : Array.from(this._blast_gauge_issues.children)
		}
	}

	get Type()
	{
		return this._type_select.Value;
	}

	set Type(type)
	{
		this._type_select.Value = type;
	}

	/**
	 * Add an editor element 
	 * 
	 * @param {ISS_DEVICE_TYPE_ID} sensor_type 
	 * @param {IssuanceSensor.WEAR_LOCATION} wear_location 
	 */
	Add(sensor_type, wear_location)
	{
		switch(sensor_type)
		{
			case ISS_DEVICE_TYPE_ID.BLAST_GAUGE:
				let editor = new BlastGaugeIssuanceEditor();
				editor.WearLocation = wear_location;
				this._blast_gauge_issues.append(editor);			
				return editor;
		}
	}

	/**
	 * Reset editors to default. 
	 */
	clear()
	{
		// loop through first 4 sensor issues
		for (var i = 0; i < 3; i++) 
		{
			this.Issues.BlastGauge[i]._serial_select.resetSerialSelect();
			this.Issues.BlastGauge[i].LocationOnWearer = "";
		}
		
		// remove additional sensors added
		while (this._blast_gauge_issues.childElementCount > 3)
		{
			this._blast_gauge_issues.removeChild(this._blast_gauge_issues.lastElementChild);
		}
	}	
}

customElements.define('jhrm-issuance-editor', IssuanceEditor);