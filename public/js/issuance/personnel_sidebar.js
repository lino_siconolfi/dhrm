import { selectAndStripId, removeElement } from "../utility.js"
import { Sortable } from "../sortable.js"
import { JhrmTooltip } from "../tooltip.js"
import { MaterialIcon } from "../material_icon.js"
import { JhrmSelect } from "../select.js"
import { Request } from "../request.js"
import { JhrmHelpIcon } from "../help_icon.js"

export class PersonnelSidebarUnit extends HTMLElement
{
	constructor()
	{
		super();

		this.innerHTML = `
			<div>
				<span style=" font-size: 110%; font-weight: bold; margin-bottom: 0.5em;">* Select Unit
					<jhrm-help-icon> Select the unit to view individuals. </jhrm-help-icon>
				</span>
			</div>

			<div style="margin: 0.5em 0;">
					<jhrm-select id="ResultingSelectUnit"> </jhrm-select><br>
					<div id="personnelissuedsensor" style="display: none;" >
						<label>Personnel Issued Sensors:</label>
						<input id="only_show_issued" type="checkbox"/>
					</div>
				<br>
		`;
		this.USER_SPECIFIC_SELECTION = "User Specific Selection";
		this.CHOOSE_UNIT = "Choose Unit";
		this.DEFAULT_VALUE = "default";

		this._ResultingSelectUnit = selectAndStripId(this, "ResultingSelectUnit");
		this._NonSelectableOption = this._ResultingSelectUnit.addOption(this.CHOOSE_UNIT, this.DEFAULT_VALUE, JhrmSelect.DISABLED);
		this._ResultingSelectUnit.Value = this.DEFAULT_VALUE;
		this._personnelissuedsensor = selectAndStripId(this, "personnelissuedsensor");
		this._only_show_issued = selectAndStripId(this, "only_show_issued");


		if (window.location.href.includes('issuance_return_sensor.html'))
		{
			this._personnelissuedsensor.style.display = "block";
		}

		Request.get('/api/issuance/personnel/distinctunit').then(
			function(response)
			{
				for (let i=0; i< response.length; i++)
				{
					this._ResultingSelectUnit.addOption(response[i].unit, response[i].unit);
				}
			}.bind(this),
			function(error)
			{
				// Known empty function
			}
		);
	}

	get ResultingSelectedUnit()
	{
		try
		{
			return this._ResultingSelectUnit.Value;
		}
		catch
		{
			return undefined;
		}
	}

	set ResultingSelectedUnit(val)
	{
		this._ResultingSelectUnit.Value = val;
	}

	set onUnitChange(func)
	{
		this._ResultingSelectUnit.onchange = func;
	}

	/**
	 * @param {Boolean} val
	 */
	set Custom(val)
	{
		if(val)
		{
			this._NonSelectableOption.Label = this.USER_SPECIFIC_SELECTION;
			this._ResultingSelectUnit.Value = this.DEFAULT_VALUE;
		}
		else
		{
			this._NonSelectableOption.Label = this.CHOOSE_UNIT;
		}
	}
}



customElements.define('jhrm-personnel-select-unit', PersonnelSidebarUnit);


export class PersonnelSidebarTableRow extends HTMLTableRowElement
{
	constructor()
	{
		super();

		this.innerHTML = `
			<td id="selected_dodid"></td>
			<td id="selected_individual"></td>
		`;

		this._selected_individual = selectAndStripId(this, "selected_individual");
		this._selected_dodid = selectAndStripId(this, "selected_dodid");
		this._selected_person = undefined;

		this.onclick = (evt) => {
			this.clearTableHighlight();
			this.classList.toggle("personnel-sidebar-selected");
			this.classList.contains("personnel-sidebar-selected");
			if(typeof this._on_personnel_select === 'function')
			{
				this._on_personnel_select(evt);
			}
		}
	}

	//function so you only click and highlight one user at a time
	clearTableHighlight()
	{
		let _tRows = this.parentElement.children;

		let tRowsArrays = [..._tRows];

		tRowsArrays.forEach(
			tRow =>
			{
				if ( tRow != this )
				{
					tRow.classList.remove('personnel-sidebar-selected');
				}
			}
		);
	}

	/**
	 * @returns { string }
	 */
	get SelectedPersonnel()
	{
		return this._selected_individual.innerText;
	}

	/**
	 * @param { string }
	 */
	set SelectedPersonnel(val)
	{
		this._selected_individual.innerText = val;
	}

	/**
	 * @returns { string }
	 */
	get SelectedDodid()
	{
		return this._selected_dodid.innerText;
	}

	/**
	 * @param { string }
	 */
	set SelectedDodid(val)
	{
		this._selected_dodid.innerText = val;
	}

	 /**
	 * @returns { Person }
	 */
	  get SelectedPerson()
	  {
		  return this._selected_person;
	  }

	  /**
	   * @param { Person }
	   */
	  set SelectedPerson(val)
	  {
		  this._selected_person = val;
	  }

	/**
	 * @param { Person } record
	 */
	static fromObjData(record)
	{
		let ret = new PersonnelSidebarTableRow();
		ret.SelectedPersonnel = record.FirstName + " " + record.LastName;
		ret.SelectedDodid = record.DodId;
		ret.SelectedPerson = record;

		return ret;
	}

	set onPersonnelSelect(func)
	{
		this._on_personnel_select = func;
	}
}

customElements.define('jhrm-personnel-sidebar-table-row', PersonnelSidebarTableRow, { extends : 'tr' });

export class PersonnelSidebarTable extends HTMLElement
{
	constructor()
	{
		super();
		this._active = false;

		this._css = `
		<style id="jhrm-personnel-row-styles">
			.personnel-sidebar tr:hover { background-color:rgb(68, 255, 11); }
			.personnel-sidebar tr { background: #ffffff; cursor: pointer; }
			.personnel-sidebar tr.personnel-sidebar-selected { background: yellow; cursor: pointer; }
		</style> `;

		if( !document.getElementById('tbody') )
			document.head.innerHTML += this._css;

		this.classList.add("personnel-sidebar");

		this.innerHTML = `
			<table id="table" class="tablescroll" style="overflow: auto; width: 100%; font-weight: bold; border: 0; padding: 0; margin: 0; max-height: 100%;">
				<thead>
					<tr>
						<th>DoDid Number</th>
						<th>Name</th>
					</tr>
				</thead>
				<tbody id="table_body">
						<td> DoDid Number </td>
						<td> Name </td>
				</tbody>
			</table>
			<jhrm-help-icon> Select the individual to issue/return sensor(s) </jhrm-help-icon>
		`;

		this._table_body = selectAndStripId(this, "table_body");
		this._table = selectAndStripId(this, "table");

		Sortable(this._table);

		this._Built = false;
	}

	connectedCallback()
	{
		if ( !this._Built )
		{
			this.style.display = "grid";
			this.style.gridTemplateColumns = "auto fit-content(25rem) 1fr auto 1fr auto";
			this.style.gridGap = "1rem 0.5rem";

			this._Built = true;
		}
	}

	appendRow(row)
	{
		if ( row instanceof PersonnelSidebarTableRow )
		{
			row.onPersonnelSelect = function()
			{
				if (this._on_person_selected)
					this._on_person_selected(row);

			}.bind(this);

			this._table_body.append(row);
		}
	}

	clearTable()
	{
		this._table_body.innerHTML = "";
	}

	async populateFromArray(personnels, unit)
	{
		this.clearTable();

		personnels.forEach (
			(record) =>
			{
				if ( !unit || record.Unit == unit)
				{
					let nextRow = PersonnelSidebarTableRow.fromObjData(record);
					this.appendRow(nextRow);
				}
			}
		);
	 }

	get Unit()
	{
		try
		{
			return this._ResultingSelectUnit.options[this._ResultingSelectUnit.selectedIndex].text;
		}
		catch(err)
		{
			return undefined;
		}
	}

	set Unit(val)
	{
		this._ResultingSelectUnit.Value = val;
	}

	/**
	 * @Returns { Person } person
	 */
	 get SelectedPerson()
	 {
		 let selected = this.querySelector('.personnel-sidebar-selected');

		 if ( selected )
		 {
			 return selected.SelectedPerson;
		 }
	 }

	/**
	 * @Returns {String} dodid
	 */
	get Selected()
	{
		let selected = this.querySelector('.personnel-sidebar-selected');

		if ( selected )
		{
			return selected.SelectedDodid;
		}
	}

	/**
	 * @param {String} val dodid
	 */
	set Selected(val)
	{
		let row = Array.from(this.querySelectorAll('tr'))
			.find(item=>item.SelectedDodid==val);
		if(row)
		{
			// I dont love this. I think that if we have a table that displays
			// objects like Person, it should be easy to get the object back
			// or say select(Person). But this gaurentees that the same logic
			// happens whether the user clicks on a row or it happens here.
			row.click();
		}
	}

	get Persons()
	{
		return Array.from(this._table_body.children);
	}

	set onPersonSelected(func)
	{
		this._on_person_selected = func;
	}
}

customElements.define('jhrm-personnel-sidebar-table', PersonnelSidebarTable);
