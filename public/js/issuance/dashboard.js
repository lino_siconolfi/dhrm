import { JhrmAlign } from "../align.js"
import { IssuanceSensor } from "./sensor.js"
import { selectAndStripId } from "../utility.js"
import { Request } from "../request.js"
import { ISS_WEAR_LOCATION, ISS_DEVICE_TYPE_ID, ISS_DEVICE_STATUS } from "/js/shared/Constants.js"

export class BlastGaugeDashboard extends HTMLElement
{
	constructor()
	{
		super();

		this.style.display = "grid";
		this.style.width = "fit-content";
		this.style.gridTemplateRows = "repeat(3, min-content) 1fr";

		this.innerHTML = `
			<jhrm-align center style="grid-row: 1; grid-column: 1;">
				<img id="sensor_image" style="display: block; width: 10em; height: 10em; border: 2px solid black;">
			</jhrm-align>
			<div style="grid-row: 2; grid-column: 1; text-align: center; font-weight: bold; padding-top: 0.5em;">Blast Gauges</div>
			<jhrm-align center top style="grid-row: 3; grid-column: 1;">
				<table style="padding: 1em;">
					<tr>
						<th></th>
						<th>Available</th>
						<th>Issued</th>
					</tr>
					<tr>
						<th>Head</th>
						<td id="head_available">0</td>
						<td id="head_issued">0</td>
					</tr>
					<tr>
						<th>Shoulder</th>
						<td id="shoulder_available">0</td>
						<td id="shoulder_issued">0</td>
					</tr>
					<tr>
						<th>Chest</th>
						<td id="chest_available">0</td>
						<td id="chest_issued">0</td>
					</tr>
				</table>
			</jhrm-align>
			<jhrm-align center bottom style="grid-row: 4; grid-column: 1;">
				<button id="view_issuance_report">View Issuance Report</button>
			</jhrm-align>
		`;

		this._head_available = selectAndStripId(this, 'head_available');
		this._head_issued = selectAndStripId(this, 'head_issued');

		this._shoulder_available = selectAndStripId(this, 'shoulder_available');
		this._shoulder_issued = selectAndStripId(this, 'shoulder_issued');
		
		this._chest_available = selectAndStripId(this, 'chest_available');
		this._chest_issued = selectAndStripId(this, 'chest_issued');
		
		this._view_issuance_report = selectAndStripId(this, 'view_issuance_report');
		
		this._sensor_image = selectAndStripId(this, 'sensor_image');
		this._sensor_image.src = "./img/BlastGauge.jpg";

		this._view_issuance_report.onclick = () =>
		{
			document.location = "./issuance_report.html?unit=ODA%20Team";
		};

		setTimeout(
			async () =>
			{
				await this.populate();
			}
		);
	}

	/**
	 * Populates the fields for the table.  If refresh is true, this will force
	 * a request back to the server.  Otherwise, cached data will be used if
	 * available.
	 */
	async populate(refresh)
	{
		let head_issued = 0;
		let head_available = 0;
		let chest_issued = 0;
		let chest_available = 0;
		let shoulder_issued = 0;
		let shoulder_available = 0;

		let sensors = await IssuanceSensor.getInventory();
		sensors = sensors.filter( element => element.Type == ISS_DEVICE_TYPE_ID.BLAST_GAUGE );

		for ( let i=0; i< sensors.length; i++ )
		{
			if ( sensors[i].Status == ISS_DEVICE_STATUS.ISSUED )
			{
				if ( sensors[i].WearLocation == ISS_WEAR_LOCATION.HEAD )
					++head_issued;
				else if ( sensors[i].WearLocation == ISS_WEAR_LOCATION.CHEST )
					++chest_issued;
				else if ( sensors[i].WearLocation == ISS_WEAR_LOCATION.SHOULDER )
					++shoulder_issued;
			}
			else if ( sensors[i].Status == ISS_DEVICE_STATUS.SERVICEABLE )
			{
				if ( sensors[i].WearLocation == ISS_WEAR_LOCATION.HEAD )
					++head_available;
				else if ( sensors[i].WearLocation == ISS_WEAR_LOCATION.CHEST )
					++chest_available;
				else if ( sensors[i].WearLocation == ISS_WEAR_LOCATION.SHOULDER )
					++shoulder_available;
			}
		}

		this._head_issued.innerHTML = head_issued;
		this._head_available.innerHTML = head_available;
		this._chest_issued.innerHTML = chest_issued;
		this._chest_available.innerHTML = chest_available;
		this._shoulder_issued.innerHTML = shoulder_issued;
		this._shoulder_available.innerHTML = shoulder_available;
	}
}

customElements.define('jhrm-blast-gauge-dashboard', BlastGaugeDashboard);

export class PcsDashboard extends HTMLElement
{
	constructor()
	{
		super();

		this.style.display = "grid";
		this.style.width = "fit-content";
		this.style.gridTemplateRows = "repeat(3, min-content) 1fr";

		this.innerHTML = `
			<jhrm-align center style="grid-row: 1; grid-column: 1;">
				<img id="sensor_image" style="display: block; width: 13.3em; height: 10em; border: 2px solid black;">
			</jhrm-align>
			<div style="grid-row: 2; grid-column: 1; text-align: center; font-weight: bold; padding-top: 0.5em;">Passive Chemical Sampler</div>
			<jhrm-align center top style="grid-row: 3; grid-column: 1;">
				<table style="padding: 1em;">
					<tr>
						<th>Available</th>
						<th>Issued</th>
					</tr>
					<tr>
						<td id="available">0</td>
						<td id="issued">0</td>
					</tr>
				</table>
			</jhrm-align>
			<jhrm-align center bottom style="grid-row: 4; grid-column: 1;">
				<div style="white-space: nowrap;">
					<button disabled=true style="cursor: default; background-color:#999; color:#ccc" id="view_issuance_report">View Issuance Report</button>
					<button disabled=true style="cursor: default; background-color:#999; color:#ccc" id="print_coc">Print COC</button>
				</div>
			</jhrm-align>
		`;

		this._available = selectAndStripId(this, 'available');
		this._issued = selectAndStripId(this, 'issued');
		
		this._sensor_image = selectAndStripId(this, 'sensor_image');
		this._sensor_image.src = "./img/PSC.jpg";
		
		this._view_issuance_report = selectAndStripId(this, 'view_issuance_report');
		this._print_coc = selectAndStripId(this, 'print_coc');

		this._view_issuance_report.onclick = () =>
		{
			document.location = "./issuance_report.html?unit=ODA%20Team";
		};

		setTimeout(
			async () =>
			{
				await this.populate();
			}
		)
	}

	/**
	 * Populates the fields for the table.  If refresh is true, this will force
	 * a request back to the server.  Otherwise, cached data will be used if
	 * available.
	 */
	async populate(refresh)
	{
		let sensors = await IssuanceSensor.getInventory();
	}
}

customElements.define('jhrm-pcs-dashboard', PcsDashboard);

export class PsmDashboard extends HTMLElement
{
	constructor()
	{
		super();

		this.style.display = "grid";
		this.style.width = "fit-content";
		this.style.gridTemplateRows = "repeat(3, min-content) 1fr";

		this.innerHTML = `
			<jhrm-align center style="grid-row: 1; grid-column: 1;">
				<img id="sensor_image" style="display: block; width: 11.4em; height: 10em; border: 2px solid black;">
			</jhrm-align>
			<div style="grid-row: 2; grid-column: 1; text-align: center; font-weight: bold; padding-top: 0.5em;">Physiological Status Monitor</div>
			<jhrm-align center top style="grid-row: 3; grid-column: 1;">
				<table style="padding: 1em;">
					<tr>
						<th>Available</th>
						<th>Issued</th>
					</tr>
					<tr>
						<td id="available">0</td>
						<td id="issued">0</td>
					</tr>
				</table>
			</jhrm-align>
			<jhrm-align center bottom style="grid-row: 4; grid-column: 1;">
				<button disabled=true style="cursor: default; background-color:#999; color:#ccc" id="view_issuance_report">View Issuance Report</button>
			</jhrm-align>
		`;

		this._available = selectAndStripId(this, 'available');
		this._issued = selectAndStripId(this, 'issued');
		
		this._sensor_image = selectAndStripId(this, 'sensor_image');
		this._sensor_image.src = "./img/PSM.jpg";
		
		this._view_issuance_report = selectAndStripId(this, 'view_issuance_report');

		this._view_issuance_report.onclick = () =>
		{
			document.location = "./issuance_report.html?unit=ODA%20Team";
		};

		setTimeout(
			async () =>
			{
				await this.populate();
			}
		)
	}

	/**
	 * Populates the fields for the table.  If refresh is true, this will force
	 * a request back to the server.  Otherwise, cached data will be used if
	 * available.
	 */
	async populate(refresh)
	{
		let sensors = await IssuanceSensor.getInventory();
	}
}

customElements.define('jhrm-psm-dashboard', PsmDashboard);


export class JpdiDashboard extends HTMLElement
{
	constructor()
	{
		super();

		this.style.display = "grid";
		this.style.width = "fit-content";
		this.style.gridTemplateRows = "repeat(3, min-content) 1fr";

		this.innerHTML = `
			<jhrm-align center style="grid-row: 1; grid-column: 1;">
				<img id="sensor_image" style="display: block; width: 7.5em; height: 10em; border: 2px solid black;">
			</jhrm-align>
			<div style="grid-row: 2; grid-column: 1; text-align: center; font-weight: bold; padding-top: 0.5em;">JPD-I</div>
			<jhrm-align center top style="grid-row: 3; grid-column: 1;">
				<table style="padding: 1em;">
					<tr>
						<th>Available</th>
						<th>Issued</th>
					</tr>
					<tr>
						<td id="available">0</td>
						<td id="issued">0</td>
					</tr>
				</table>
			</jhrm-align>
			<jhrm-align center bottom style="grid-row: 4; grid-column: 1;">
				<button disabled=true style="cursor: default; background-color:#999; color:#ccc" id="view_issuance_report">View Issuance Report</button>
			</jhrm-align>
		`;

		this._available = selectAndStripId(this, 'available');
		this._issued = selectAndStripId(this, 'issued');
		
		this._sensor_image = selectAndStripId(this, 'sensor_image');
		this._sensor_image.src = "./img/jpdi.jpg";
		
		this._view_issuance_report = selectAndStripId(this, 'view_issuance_report');

		this._view_issuance_report.onclick = () =>
		{
			document.location = "./issuance_report.html?unit=ODA%20Team";
		};

		setTimeout(
			async () =>
			{
				await this.populate();
			}
		)
	}

	/**
	 * Populates the fields for the table.  If refresh is true, this will force
	 * a request back to the server.  Otherwise, cached data will be used if
	 * available.
	 */
	async populate(refresh)
	{
		let sensors = await IssuanceSensor.getInventory();
	}
}

customElements.define('jhrm-jpdi-dashboard', JpdiDashboard);