import { JhrmTabs } from "../tabs.js"

/**
 * Set up tabs for the JHRM issuance tool. 
 * 
 * USAGE
 * 
 * <jhrm-issuance-tabs></jhrm-issuance-tabs>
 * 
 * Selected tab is set from current page URL.
 * 
 */
export class JhrmIssuanceTabs extends HTMLElement
{
    /**
     * Define default tab styles. 
     */
	constructor()
	{
        super();
        this.innerHTML = `
            <jhrm-tabs>
                <jhrm-swap-space>
                    <div data-href="/issuance_tool.html" data-tab="Sensor Dashboard"></div>
                    <div data-href="/issuance_issue_sensor.html" data-tab="Sensor Issue"></div>
                    <div data-href="/issuance_return_sensor.html" data-tab="Sensor Return"></div>
                    <div data-href="/issuance_roster.html" data-tab="Personnel Roster"></div>
                    <div data-href="/issuance_inventory.html" data-tab="Sensor Inventory"></div>
                </jhrm-swap-space>
            </jhrm-tabs>
        `;
	}

    /**
     * Render tabs
     */
	connectedCallback()
	{
        this.querySelector('jhrm-tabs').render();
        this.querySelector('jhrm-swap-space').style.display = 'none';
        document.head.innerHTML += `
            <style>
                .jhrm-tabs {
                    clear:both
                }
            </style>
        `
    }

}

customElements.define('jhrm-issuance-tabs', JhrmIssuanceTabs);