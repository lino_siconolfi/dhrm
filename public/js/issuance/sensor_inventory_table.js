import { selectAndStripId, removeElement } from "../utility.js"
import { ISS_DEVICE_TYPE_ID, ISS_WEAR_LOCATION } from "/js/shared/Constants.js"
import { Sortable } from "../sortable.js"
import { Request } from "../request.js";
import { IssuanceSensor } from "./sensor.js";
import { JhrmRadioGroup } from "../radio_group.js";
import { JhrmHelpIcon } from "../help_icon.js"

export class SensorInventoryTableRow extends HTMLTableRowElement
{
	constructor()
	{
		super();
		this._id = undefined;

		this.innerHTML = `
			<td id="check_area">

			</td>
			<td id="sensor_type_wear"></td>
			<td id="serial_number"></td>
			<td id="manufacturer"></td>
			<td id="model_number"></td>
			<td id="lot_number"></td>
			<td id="status"></td>
		`;

		this._check_area = selectAndStripId(this, "check_area");
		this._sensor_type_wear = selectAndStripId(this, "sensor_type_wear");
		this._serial_number = selectAndStripId(this, "serial_number");
		this._manufacturer = selectAndStripId(this, "manufacturer");
		this._model_number = selectAndStripId(this, "model_number");
		this._lot_number = selectAndStripId(this, "lot_number");
		this._status = selectAndStripId(this, "status");

		this._radio_group = undefined;
		this._type = ISS_DEVICE_TYPE_ID.NONE;
		this._wear_location = ISS_WEAR_LOCATION.OTHER;
	}

		/**
	 * Used by a containing table to set the radio group 
	 */
	set _RadioGroup(val)
	{
		if ( !(val instanceof JhrmRadioGroup))
			throw new Error("Expected argument of type JhrmRadioGroup.");
		
		this._radio_group = val;

		this._check_area.innerHTML = "";
		this._check_area.append(
			this._radio_group.genElement(this)
		);
	}

	/**
	 * @returns { boolean }
	 */
	get Selected()
	{
		return (undefined != this._radio_group) && (this._radio_group.Value == this);
	}

	/**
	 * @returns { string }
	 */
	get Id()
	{
		return this._id;
	}

	/**
	 * @param { string }
	 */
	set Id(val)
	{
		this._id = val;
	}

	/**
	 * @returns { ISS_DEVICE_TYPE_ID }
	 */
	get Type()
	{
		return this._type;
	}

	/**
	 * @param { ISS_DEVICE_TYPE_ID }
	 */
	set Type(val)
	{
		this._type = val;
		this._sensor_type_wear.innerText = `
			${ISS_DEVICE_TYPE_ID.toString(val)} -
			${ISS_WEAR_LOCATION.toString(this._wear_location)}
		`;
	}

	/**
	 * @returns { ISS_WEAR_LOCATION }
	 */
	get WearLocation()
	{
		return this._wear_location;
	}

	/**
	 * @param { ISS_WEAR_LOCATION }
	 */
	set WearLocation(wl)
	{
		this._wear_location = wl;
		this._sensor_type_wear.innerText = `
			${ISS_DEVICE_TYPE_ID.toString(this._type)} -
			${ISS_WEAR_LOCATION.toString(wl)}
		`;
	}

	/**
	 * @returns { string }
	 */
	get SerialNumber()
	{
		return this._serial_number.innerText;
	}

	/**
	 * @param { string }
	 */
	set SerialNumber(val)
	{
		this._serial_number.innerText = val;
	}

	/**
	 * @returns { string }
	 */
	get Manufacturer()
	{
		return this._manufacturer.innerText;
	}

	/**
	 * @param { string }
	 */
	set Manufacturer(val)
	{
		this._manufacturer.innerText = val;
	}

	/**
	 * @returns { string }
	 */
	get ModelNumber()
	{
		return this._model_number.innerText;
	}

	/**
	 * @param { string }
	 */
	set ModelNumber(val)
	{
		this._model_number.innerText = val;
	}

	/**
	 * @returns { string }
	 */
	get LotNumber()
	{
		return this._lot_number.innerText;
	}

	/**
	 * @param { string }
	 */
	set LotNumber(val)
	{
		this._lot_number.innerText = val;
	}

	/**
	 * @returns { string }
	 */
	get Status()
	{
		return this._status.innerText;
	}

	/**
	 * @param { string }
	 */
	set Status(val)
	{
		this._status.innerText = val;
	}
}

customElements.define('jhrm-sensor-inventory-table-row', SensorInventoryTableRow, { extends : 'tr' });

/**
 * Sensor objects  passed to and returned from this table will have the following properties:
 * 
 * Selected { boolean }
 * SensorType { string }
 * SerialNumber { string }
 * Manufacturer { string }
 * ModelNumber { string }
 * LotNumber { string }
 * Status { string }
 */
export class SensorInventoryTable extends HTMLElement
{
	constructor()
	{
		super();

		this.innerHTML = `
			<div class="tablewrapper">
				<table id="table" style="width: 100%; border: 0; padding: 0; margin: 0;">
					<thead>
						<tr>
							<th></th>
							<th>Sensor Type</th>
							<th>Serial Number</th>
							<th>Manufacturer</th>
							<th>Model Number</th>
							<th>Lot Number</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody id="table_body">
					</tbody>
				</table>
			</div>
		`;

		this._table_body = selectAndStripId(this, "table_body");
		this._table = selectAndStripId(this, "table");
		this._selected_radio_group = new JhrmRadioGroup();

		this.SensorInventory();

		Sortable(this._table);
	}

	async SensorInventory()
	{
		this.Sensors = await IssuanceSensor.getInventory();
	}

	/**
	 * Gets an array of all of the sensor objects in the table.  The returned
	 * objects can be used to set fields displayed in the table.
	 */
	get Sensors()
	{
		return Array.from(this._table_body.children);
	}

	/**
	 * Sets the sensors that are displayed in the table.  Existing items will
	 * be removed.
	 * 
	 * @param { object[] } sensors
	 *  An array of objects with sensor parameters for display in the table.
	 */
	set Sensors(sensors)
	{
		this._table_body.innerHTML = "";
		sensors.forEach( this.addSensor.bind(this) );
	}

	/**
	 * Adds a sensor to the table.  The created object is returned, and can be used
	 * to modify the field data shown in the table.
	 * 
	 * @param { object } sensor 
	 */
	addSensor(sensor)
	{
		let row = new SensorInventoryTableRow();
		row._RadioGroup = this._selected_radio_group;
		IssuanceSensor.copyFields(sensor, row);

		this._table_body.append(row);

		return row;
	}

	/**
	 * Finds a sensor in the table with the passed serial number.  The returned object can be used
	 * to set fields displayed in the table.
	 * 
	 * @param { string } serial 
	 */
	find(serial)
	{
		return this.Sensors.find( (elm) => { return elm.SerialNumber == serial; } );
	}

	/**
	 * Removes a sensor with the passed serial number from the table if it exists.  The properites
	 * of the removed sensor will be returned if it exists.
	 * 
	 * @param { string } serial 
	 */
	removeSensor(serial)
	{
		let element = this.find(serial);

		if ( element )
		{
			let ret = {};
			this._copy_sensor_props(element, ret);

			removeElement(element);
			return ret;
		}

		let ret = undefined;
	}

	/**
	 * Gets an array of all selected sensors in the table.
	 */
	get Selected()
	{
		return this._selected_radio_group.Value;
	}

	/**
	 * Sets a single selected row. 
	 * 
	 * @param {String} val Sensor SN
	 */
	set Selected(val)
	{
		this.Sensors.forEach(sensor => {
			if(sensor.SerialNumber == val)
			{
				this._selected_radio_group.Value = sensor;
			}
		});
	}
}

customElements.define('jhrm-sensor-inventory-table', SensorInventoryTable);