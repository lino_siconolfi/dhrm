import { selectAndStripId, removeElement,randomChars } from "../utility.js"
import { ISS_DEVICE_TYPE_ID, ISS_ASSOCIATION} from "/js/shared/Constants.js"
import { Sortable } from "../sortable.js"
import { JhrmDatepicker } from "/js/datepicker.js"
import { JhrmHelpIcon } from "../help_icon.js"

export class SensorReturnTableRow extends HTMLTableRowElement
{
	constructor()
	{
		super();
		this._RadioButtonSelection = undefined;
		this._SubRowComments = undefined;
		this._SubRows = [];
        this._IsSubRow = false;
		this._ShowSubrows = false;
		this._device_id = undefined;
		this._personnel_id = undefined;
		this._wear_location = undefined;
		this._device_type_id = undefined;

		let name = randomChars(8);

		this.innerHTML = `
			<td class="span-all-rows">
				<input type="checkbox" id="selected"></input>
			</td>
			<td id="sensor_type"></td>
			<td id="serial_number"></td>
			<td id="serviceable"><input type="radio" value="serviceable" name="${name}"></td>
			<td id="nonserviceable"><input type="radio" value="nonserviceable" name="${name}"></td>
			<td id="lost"><input type="radio" value="lost" name="${name}"></td>
			<td id="datamismatch"><input type="radio" value="datamismatch" name="${name}"></td>
			<td id="verification">${ISS_ASSOCIATION.MANUAL}</td>
		`;

		this.onclick = () => this.toggleSubrows();

		this._selected = selectAndStripId(this, "selected");
		this._sensor_type = selectAndStripId(this, "sensor_type");
		this._serial_number = selectAndStripId(this, "serial_number");
		this._serviceable = selectAndStripId(this, "serviceable");
		this._nonserviceable = selectAndStripId(this, "nonserviceable");
		this._lost = selectAndStripId(this, "lost");
		this._datamismatch = selectAndStripId(this, "datamismatch");
		this._verification = selectAndStripId(this, "verification");

	}

	connectedCallback()
	{
		if ( !this._Built )
		{
			this._Built = true;
		}

		// This is intentionally outside of the _Built guard!
		this.updateDisplay();
    }

	/**
	 * Set a row as a subrow of this one. It will always be displayed after this
	 * row in the table.
	 *
	 * @param {JhrmIssueTableRow} row
	 */
	addSubRow(row)
	{
        // Allow JhrmIssueTableSubRow
		if (row instanceof JhrmIssueTableSubRow)
		{
			this._SubRows.push(row);
			this.updateDisplay();
		}
    }

    toggleSubrows(val)
    {
        if(val !== undefined)
        {
            this._ShowSubrows = val;
        }
        else
        {
			let val = this.rowIndex;
			if ( this.RadioStatus == "nonserviceable" || this.RadioStatus == "lost" || this.RadioStatus == "datamismatch")
			{
				this._ShowSubrows = true;
			}
			else
			{
				this._ShowSubrows = false;
			}
        }
        this.updateDisplay();
    }

	/**
	 * This method updates the rowspans for this row to take into account any
	 * subrows.
	 */
	updateDisplay()
	{
        if(!this._Built)
        {
            return;
        }

        // Set rowspans
        Array.from(this.querySelectorAll(".span-all-rows"))
            .forEach(col=>col.rowSpan = this._ShowSubrows ? this.Rows : 1)

        // Put all subrows after current row. This makes sure that if the row is
        // sorted, the subrows go with it.
        this._SubRows.forEach(row=>{
            row.style.display = this._ShowSubrows ? 'table-row' : 'none';
            this.parentNode.insertBefore(row, this.nextSibling);
        });
	}

	/**
	 * Return the total number of rows that this row encompases, including
	 * itself and any subrows.
	 */
	get Rows()
	{
		return this._SubRows.length + 1;
	}

    /**
     * Return the array of sub rows
     */
	get SubRows()
	{
		return this._SubRows;
    }

	/**
	 * @returns { boolean }
	 */
    get SubRow()
	{
		return this._IsSubRow;
	}

	/**
	 * @returns { boolean }
	 */
	get Selected()
	{
		return this._selected.checked;
	}

	/**
	 * @param { boolean }
	 */
	set Selected(selected)
	{
		this._selected.checked = selected;
	}

	/**
	 * @returns { boolean }
	 */
	get Serviceable()
	{
		return this._serviceable.querySelector("input").checked;
	}

	/**
	 * @param { boolean }
	 */
	set Serviceable(selected)
	{
		this._serviceable.querySelector("input").checked = selected;
	}

	/**
	 * @returns { String }
	 */
	get Verification()
	{
		return this._verification.innerText;
	}

	/**
	 * @param { String }
	 */
	set Verification(val)
	{
		this._verification.innerText = val;
	}

	/**
	 * @returns { int }
	 */
	get DeviceId()
	{
		return this._device_id;
	}

	/**
	 * @param { int }
	 */
	set DeviceId(val)
	{
		this._device_id = val;
	}

	/**
	 * @returns { string }
	 */
	get WearLocation()
	{
		return this._wear_location;
	}

	/**
	 * @param { string }
	 */
	set WearLocation(val)
	{
		this._wear_location = val;
	}

	/**
	 * @returns { int }
	 */
	get PersonnelId()
	{
		return this._personnel_id;
	}

	/**
	 * @param { int }
	 */
	set PersonnelId(val)
	{
		this._personnel_id = val;
	}

	/**
	 * @returns { int }
	 */
	get DeviceTypeId()
	{
		return this._device_type_id;;
	}

	/**
	 * @param { int }
	 */
	set DeviceTypeId(val)
	{
		this._device_type_id = val;
	}

	/**
	 * @returns { string }
	 */
	get SensorType()
	{
		return this._sensor_type.innerText;
	}

	/**
	 * @param { string }
	 */
	set SensorType(val)
	{
		this._sensor_type.innerText = val;
	}

	/**
	 * @returns { string }
	 */
	get SensorSerial()
	{
		return this._serial_number.innerText;
	}

	/**
	 * @param { string }
	 */
	set SensorSerial(val)
	{
		this._serial_number.innerText = val;
	}

	get RadioStatus()
	{
		let RadioGroup = this.querySelectorAll('input[type="radio"]')

		RadioGroup.forEach(record => {
			if (true == record.checked)
			{
				this._RadioButtonSelection = record.defaultValue;
			}
		});

		return this._RadioButtonSelection;
	}

	blastgaugetypelocation(iss_device_type_id,wear_location)
	{
		if (iss_device_type_id == ISS_DEVICE_TYPE_ID.BLAST_GAUGE)
		{
			return "BLAST GAUGE - " + wear_location;
		}
		else if (iss_device_type_id == ISS_DEVICE_TYPE_ID.JPDI)
		{
			return "JPD-I"
		}
		else
		{
			return wear_location;
		}
	}

	static fromObjData(record)
	{
		let ret = new SensorReturnTableRow();

		ret.SensorType = ret.blastgaugetypelocation(record.iss_device_type_id, record.wear_location);
		ret.SensorSerial  = record.serial_number;
		ret.DeviceId  = record.id;
		ret.WearLocation = record.wear_location;
		ret.PersonnelId = record.iss_personnel_id;
		ret.DeviceTypeId = record.iss_device_type_id;

		return ret;
	}
}

customElements.define('jhrm-sensor-return-table-row', SensorReturnTableRow, { extends : 'tr' });

export class JhrmIssueTableSubRow extends HTMLTableRowElement
{
	constructor(id)
	{
        super();
        this._Built = false;
        this._IsSubRow = true;
        this._template = document.createElement('template');
		this.classList.add('nosort');
		this._template.innerHTML = `
			<td colspan="2">* Date Reported:
				<material-icon icon="help_outline" id="DateReportedHelp${id}"></material-icon>
				<jhrm-tooltip target="DateReportedHelp${id}"> Enter the date the issue was reported.</jhrm-tooltip>
				<jhrm-datepicker class="datereported"></jhrm-datepicker>
			</td>
			<td colspan="2">* Date Discovered:
				<material-icon icon="help_outline" id="DateDiscoveredHelp${id}"></material-icon>
				<jhrm-tooltip target="DateDiscoveredHelp${id}"> Enter the date the issue was discovered.</jhrm-tooltip>
				<jhrm-datepicker class="datediscovered"></jhrm-datepicker>
			</td>
			<td colspan="3">* Comments:
				<material-icon icon="help_outline" id="CommentsHelp${id}"></material-icon>
				<jhrm-tooltip target="CommentsHelp${id}"> Enter comments to describe the issue with the sensor return.</jhrm-tooltip>
				<input class="comments" type="text" size="40"/>
			</td>
		`
    }

    connectedCallback()
	{
		if ( !this._Built )
		{
            this.append(this._template.content);
			this._Built = true;

			this._datereported =  this.querySelector('.datereported');
			this._datediscovered =  this.querySelector('.datediscovered');
			this._comments =  this.querySelector('.comments');
		}
    }

	/**
	 * @param { boolean }
	 */
    get SubRow()
	{
		return this._IsSubRow;
	}

	/**
	 * @returns { string }
	 */
	get DateReported()
	{
		return this._datereported.value;
	}

	/**
	 * @returns { string }
	 */
	get DateDiscovered()
	{
		return this._datediscovered.value;
	}

	/**
	 * @returns { string }
	 */
	get Comments()
	{
		return this._comments;
	}
}

customElements.define('jhrm-sensor-return-table-subrow', JhrmIssueTableSubRow, { extends : 'tr' });

/**
 * Sensor objects  passed to and returned from this table will have the following properties:
 *
 * Select { boolean }
 * Sensor Type { string }
 * SerialNumber { string }
 * Serviceable { string }
 * Non-Serviceable { string }
 * Lost { string }
 * Data Mismatch { string }
 * Scan
 */
export class SensorReturnTable extends HTMLElement
{
	constructor()
	{
		super();

		this.innerHTML = `
			<table id="table" style="width: 100%; border: 0; padding: 0; margin: 0;">
				<thead>
					<tr>
						<th>Select<input type="checkbox" id="chkallselect"></th>
						<th>Sensor Type</th>
						<th>Sensor Serial Number</th>
						<th>Serviceable <input type="checkbox" id="chkallservicable"></th>
						<th>Non-Serviceable
							<jhrm-help-icon>
								Select if the sensor is damaged or inoperable.
							</jhrm-help-icon>
						</th>
						<th>Lost</th>
						<th>
							Data Mismatch
							<jhrm-help-icon>
								Select if the sensor serial number does not match the sensor issued to the individual.
							</jhrm-help-icon>
						</th>
						<th>Verification</th>
					</tr>
				</thead>
				<tbody id="table_body">
				</tbody>
			</table>
		`;

		this._table_body = selectAndStripId(this, "table_body");
		this._table = selectAndStripId(this, "table");
		this._chkallservicable = selectAndStripId(this, "chkallservicable");
		this._chkallselect = selectAndStripId(this,"chkallselect");

		this.addPromptRow();
	}

	connectedCallback()
	{
		this._chkallservicable.onclick = function()
			{
				this.ServiceableSelectCase(this._chkallservicable.checked);

			}.bind(this);

		this._chkallselect.onclick = function()
			{
				this.SelectCase(this._chkallselect.checked);

			}.bind(this);
	}

	SelectCase(selectAll)
	{
		let checked = (selectAll === true) ? true : false;
		this.Rows.forEach(row=>{
			row.Selected = checked
		});

	}

	ServiceableSelectCase(selectAll)
	{
		let checked = (selectAll === true) ? true : false;
		this.Rows.forEach(row=>{
			row.Serviceable = checked
		});
	}

	addRow(row)
	{
		if (row instanceof SensorReturnTableRow)
			this._table_body.append(row);
	}

	populateIssuedSensors(sensors)
	{
		sensors.forEach(sensor => {
			let nextRow = SensorReturnTableRow.fromObjData(sensor);
			nextRow.addSubRow(new JhrmIssueTableSubRow(sensor.id));
			this.addRow(nextRow);
		});
	}

	addPromptRow()
	{
		this._table_body.innerHTML = `
			<tr>
				<td colspan="8">
					<strong style="font-size: 18px;">* Please select a person from the left search bar to start the return sensor process.</strong>
				</td>
			</tr>
		`;
	}

	clear()
	{
		this._table_body.innerHTML = "";
	}

	/**
	 * Return all sensor rows (not including subrows)
	 */
	get Rows()
	{
		return Array
			.from(this._table_body.children)
			.filter(item=>!item.SubRow);
	}

	get SelectedRows()
	{
		return Array
			.from(this._table_body.children)
			.filter(item=>item.Selected);
	}
}

customElements.define('jhrm-sensor-return-table', SensorReturnTable);
