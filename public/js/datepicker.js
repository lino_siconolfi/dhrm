import { Datetime } from "./shared/datetime.js";
import { FormValidator } from "./validate.js";
const css = `
	<style id="jhrm-datepicker-styles">
	.jhrm-datepicker-wrapper{
		display:inline-block;
		position:relative;
		min-width:10em;
	}
	.jhrm-datepicker-wrapper input[type=date]{
		width:100%;
	
	}
	.jhrm-datepicker-wrapper input[type=text]{
		position:absolute;
		top:0;
		left:0;
		height:100%;
		width:100%;
		pointer-events:none;
	}
	.jhrm-datepicker-wrapper input[type="date"]::-webkit-calendar-picker-indicator {
		position: absolute;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		width: auto;
		height: auto;
		color: transparent;
		background: transparent;
	}
	.jhrm-datepicker-wrapper div {
		position:absolute;
		right:0.2em;
		top:50%;
		margin-top:-0.5em;
		height:50%;
		font-size:1.0em;
		line-height:1.0em;
		display:inline-block;
	}
	.jhrm-datepicker-wrapper div * 
	{
		padding:0;margin:0;font-size:1em;
	}
	
	.jhrm-datepicker-wrapper .jhrm-datepicker-calendar {
		display:none;
	}
	.jhrm-datepicker-empty div
	{
		pointer-events:none;
	}
	.jhrm-datepicker-empty .jhrm-datepicker-calendar 
	{
		display:inline-block;
	}
	.jhrm-datepicker-empty .jhrm-datepicker-cancel 
	{
		display:none;
	}
	.jhrm-datepicker-icons
	{ 
		color: #999
	}
	.jhrm-datepicker-wrapper:hover .jhrm-datepicker-icons
	{ 
		color: #999
	}
	jhrm-timepicker
		{
			width:max-content;
			display:inline-block;
			position:relative;
			
		}
		jhrm-timepicker input
		{
			min-width:8em;
		}
		jhrm-timepicker span
		{
			position:absolute;
			top:50%;
			font-size:0.8rem;
			margin-top:-0.4rem;
			right:3px;
			pointer-events:none;
		}
	</style>
`;
/**
 * This is intended to be a drop-in replacement for <input type="date"> but 
 * allowing for the display format of the date use the standard JHRM format. 
 * 
 * This is necessary because the built-in date picker uses the browser locale
 * to determine the display format and it is not customizable. 
 * 
 * <jhrm-datepicker value=""></jhrm-datepicker>
 * 
 * input.value, input.valueAsDate and input valueAsNumber are all supported. 
 * 
 * Events are supported via event bubbling. 
 * 
 */
export class JhrmDatepicker extends HTMLElement
{
	/**
	 * Define default tab styles. 
	 */
	constructor()
	{
		super();

		this._built = false;
	}

	/**
	 * Add styles to header and attempt to find the linked swap space. 
	 */
	connectedCallback()
	{
		if(!document.getElementById('jhrm-datepicker-styles'))
		{
			document.head.innerHTML += css;
		}
		if(!this._built)
		{
			this.innerHTML = `
				<input type="date">
				<input type="text" placeholder="Select Date">
				<div class="jhrm-datepicker-icons">
					<material-icon class="jhrm-datepicker-calendar" icon="calendar_today"></material-icon>
					<material-icon class="jhrm-datepicker-cancel" icon="cancel"></material-icon>
				</div>
			`;
			this._dateInput = this.querySelector("input[type=date]");
			this._textInput = this.querySelector("input[type=text]");
			if(this._value)
			{
				this._dateInput.value = this._value;
			}
			else 
			{
				this._value = "";
			}
			this._cancelIcon = this.querySelector(".jhrm-datepicker-cancel");
			this._dateInput.onchange = evt => {
				this._value = evt.target.value;
				this.render();
			};
			this._cancelIcon.onclick = evt => {
				this.clear();
			};
			this.classList.add('jhrm-datepicker-wrapper');
			this.render();
			this._built = true;
		}
	}	

	/**
	 * Update the display so that it reflects the current value of the date
	 * input. 
	 */
	render()
	{
		if(this._dateInput.value)
		{
			let t = this._dateInput.value.split('-');

			// Note that we use this method to prevent timezone issues that 
			// could arise, and to use the same date formatter used everywhere
			// else. It's annoying, but it prevents issues that could arise 
			// if using valueAsDate
			let date = new Datetime(new Date(Date.UTC(t[0], t[1]-1, t[2])), Datetime.FORMATS.DATE);
			this._textInput.value = date;
			this.classList.remove('jhrm-datepicker-empty');
		}
		else 
		{
			this.classList.add('jhrm-datepicker-empty');
			this._textInput.value = '';
		}
	}

	/**
	 * 
	 */
	clear()
	{
		this._dateInput.value = '';
		let event = new Event('change', {bubbles: true});
		this._dateInput.dispatchEvent(event);
		this.render();
	}

	/**
	 * Pass through to set the value of the date input. See documentation 
	 * for <input type="date"> for details. 
	 * 
	 * @param {string} val
	 */
	set value(val)
	{
		this._value = val || '';
		if(this._built)
		{
			this._dateInput.value = this._value;
			this.render();
		}
	}

	/**
	 * Return the current value of the date input. 
	 */
	get value()
	{
		return this._value;
	}

	/**
	 * Return the current value of the date input as a Date object. 
	 */
	get valueAsDate()
	{
		return this._dateInput.valueAsDate;
	}

	/**
	 * Return the current value of the date input as a number. 
	 */
	get valueAsNumber()
	{
		return this._dateInput.valueAsNumber;
	}

}

customElements.define('jhrm-datepicker', JhrmDatepicker);

/**
 * This is intended to be a drop-in replacement for <input type="time"> but 
 * allowing for the display format of the date use the standard JHRM format. 
 * 
 * This is necessary because the built-in time picker uses the browser locale
 * to determine the display format and it is not customizable. 
 * 
 * <jhrm-timepicker value=""></jhrm-timepicker>
 * 
 * input.value is supported. 
 * 
 * input.valueAsDate and input valueAsNumber are not supported.
 * 
 * Events are supported via event bubbling. 
 * 
 */
export class JhrmTimepicker extends HTMLElement
{
	/**
	 * Define default tab styles. 
	 */
	constructor()
	{
		super();
		this._built = false;
		if(this.hasAttribute("value"))
		{
			this._value = this.getAttribute("value");
		}
		else 
		{
			this._value = "";
		}
	}

	connectedCallback()
	{
		if(!document.getElementById('jhrm-datepicker-styles'))
		{
			document.head.innerHTML += css;
		}
		if(!this._built)
		{
			this.innerHTML = `
				<input value="${this._value}" data-validate-time placeholder="HH:MM"><span>Zulu / UTC</span>
			`;
			this._textInput = this.querySelector("input");
			this._textInput.onkeyup = (e) => {
				this.validator.validate();
				this._value = this._textInput.value;
			};
			this.validator = new FormValidator(this);
			this.validator.validate();
			this._built = true;
		}
	}	

	/**
	 * Pass through to set the value of the date input. See documentation 
	 * for <input type="date"> for details. 
	 * 
	 * @param {string} val
	 */
	set value(val)
	{
		this._value = val || '';
		if(this._built)
		{
			this._textInput.value = this._value;
			this.validator.validate();
		}
	}

	/**
	 * Return the current value of the date input. 
	 */
	get value()
	{
		return this._value
	}

}

customElements.define('jhrm-timepicker', JhrmTimepicker);