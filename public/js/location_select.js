import { MaterialIcon } from "./material_icon.js"
import { JhrmTooltip } from "./tooltip.js"

export class LocationOption extends HTMLOptionElement
{
	constructor()
	{
		super()

		this._Name = undefined;
		this._Latitude = 0.0;
		this._Longitude = 0.0;
	}

	get Name()
	{
		return this._Name;
	}

	set Name(val)
	{
		this._Name = val;
		this.value = val;
	}

	get Latitude()
	{
		return this._Latitude;
	}

	set Latitude(val)
	{
		this._Latitude = val;
	}

	get Longitude()
	{
		return this._Longitude;
	}

	set Longitude(val)
	{
		this._Longitude = val;
	}
}

customElements.define('jhrm-location-option', LocationOption, { extends: 'option' });

export class JhrmLocationSelect extends HTMLElement
{
	constructor()
	{
		super();

		this._selectLocation = document.createElement('select');
		this._inputLongitude = document.createElement('input');
		this._inputLatitude = document.createElement('input');
		this._labelLong = document.createElement('div');
		this._labelLat = document.createElement('div');

		this._noneOption = new LocationOption();
		this._noneOption.innerText = '(none)';

		this._options = [ ];
	}

	get _SelectedOption()
	{
		try
		{
			return this._selectLocation.options[this._selectLocation.selectedIndex];
		}
		catch
		{
			return undefined;
		}
	}

	_onSelectChange()
	{
		let option = this._SelectedOption;
		let display = (option && option.Name) ? null : "hidden";

		this._inputLongitude.style.visibility = display;
		this._inputLatitude.style.visibility = display;
		this._labelLong.style.visibility = display;
		this._labelLat.style.visibility = display;

		if ( option && display != null )
		{
			this._inputLongitude.value = option.Longitude;
			this._inputLatitude.value = option.Latitude;
		}
	}

	connectedCallback()
	{
		if ( !this._Built )
		{	
			let layout = document.createElement('div');
			layout.width = "100%";
			layout.style.display = "grid";
			layout.style.gridTemplateColumns = "auto 6em 1fr 6em 1fr";

			this._selectLocation.style.gridRow = "1";
			this._selectLocation.style.gridColumn = "1";

			this._labelLat.style.textAlign = "right";
			this._labelLat.innerText = "Latitude: ";
			this._labelLat.style.gridRow = 1;
			this._labelLat.style.gridColumn = 2;
			this._labelLat.style.marginRight = "0.5em";

			this._inputLatitude.style.width = "100%";
			this._inputLatitude.style.padding = 0;
			this._inputLatitude.style.gridRow = 1;
			this._inputLatitude.style.gridColumn = 3;

			this._labelLong.style.textAlign = "right";
			this._labelLong.innerText = "Longitude: ";
			this._labelLong.style.gridRow = 1;
			this._labelLong.style.gridColumn = 4;
			this._labelLong.style.marginRight = "0.5em";

			this._inputLongitude.style.width = "100%";
			this._inputLongitude.style.padding = 0;
			this._inputLongitude.style.gridRow = 1;
			this._inputLongitude.style.gridColumn = 5;

			let icon = MaterialIcon.Create("help_outline");
			icon.style.fontSize = "0.9em";
			icon.style.marginLeft = "0.2em";

			let tooltip = new JhrmTooltip();
			tooltip.Target = icon;
			tooltip.append(
				'Select the location for the samples you wish to use.'
			);

			let elmSelect = document.createElement('span');
			elmSelect.append(this._selectLocation, icon, tooltip);

			layout.append(
				elmSelect,
				this._labelLat,
				this._inputLatitude,
				this._labelLong,
				this._inputLongitude,
			);

			this._inputLatitude.addEventListener(
				"focusout",
				function(evt)
				{
					let option = _SelectedOption();
					if ( option )
						option.Latitude = parseFloat(this._inputLatitude.value);
				}.bind(this)
			);

			this._inputLongitude.addEventListener(
				"focusout",
				function(evt)
				{
					let option = _SelectedOption();
					if ( option )
						option.Longitude = parseFloat(this._inputLongitude.value);
				}.bind(this)
			);


			this.append(layout);

			this.sortLocations();
			this._selectLocation.addEventListener( "change", this._onSelectChange.bind(this));

			this._Built = true;
		}
	}

	set Location(val)
	{
		this._selectLocation.value = val;
		this._onSelectChange();
	}

	set Latitude(val)
	{
		this._inputLatitude.value = val;
	}

	set Longitude(val)
	{
		this._inputLongitude.value = val;
	}

	get Name()
	{
		let option = this._SelectedOption;
		return ( option ) ? option.Name : undefined;
	}

	get Latitude()
	{
		let option = this._SelectedOption;
		return ( option && (option.Latitude != undefined) ) ? option.Latitude : undefined;
	}

	get Longitude()
	{
		let option = this._SelectedOption;
		return ( option && (option.Longitude != undefined) ) ? option.Longitude : undefined;
	}

	addLocation(name, lat, long)
	{
		let option = new LocationOption();

		this._selectLocation.append(option);
		this._options.push(option);

		option.Name = name;
		option.innerText = name;

		option.Latitude = lat;
		option.Longitude = long;
	}

	clearLocations()
	{
		this._selectLocation.innerHTML = "";
		this._selectLocation.append(this._noneOption);

		this._onSelectChange();
	}

	sortLocations()
	{
		this._options.sort(
			function (left, right)
			{
				return left.Name.localeCompare(right.Name);
			}
		)

		this._selectLocation.innerHTML = "";
		this._selectLocation.append(this._noneOption, ...this._options);

		this._selectLocation.value = this._noneOption.textContent;
		this._onSelectChange();
	}
}

customElements.define('jhrm-location-select', JhrmLocationSelect);