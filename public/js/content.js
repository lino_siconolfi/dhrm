export class JhrmContent extends HTMLElement
{
	constructor()
	{
		super();

		this.Initialized = false;
	}

	connectedCallback()
	{
		if (false == this.Initialized)
		{
			this.Initialized = true;
		}
	}
}

customElements.define('jhrm-content', JhrmContent);