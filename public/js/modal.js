export class JhrmModal extends HTMLElement
{
	constructor()
	{
		super();

		this._built = false;
		this._visible = false;
	}

	connectedCallback()
	{
		if ( !this._built )
		{
			let computed_style = window.getComputedStyle(this);
			let zIndex = isNaN(computed_style.zIndex) ? `999` : `${Number.parseInt(computed_style.zIndex) + 1}`;

			this.style.position = "fixed";
			this.style.top = "0px";
			this.style.left = "0px";
			this.style.margin = "0px";
			this.style.boxSizing = "border-box";
			this.style.zIndex = zIndex;

			this._onresize = function()
			{
				this.style.height = `${window.innerHeight}px`;
				this.style.width = `${window.innerWidth}px`;
			}.bind(this);

			this._built = true;
			this._apply_visible();
		}
	}

	_apply_visible()
	{
		if (!this._built)
			return;

		if (this._visible)
		{
			this._onresize();
			window.addEventListener('resize', this._onresize);
		}
		else
		{
			window.removeEventListener('resize', this._onresize);
		}

		this.hidden = !this._visible;
	}

	set Visible(val)
	{
		if ( val != this._visible )
		{
			this._visible = val;
			this._apply_visible();
		}
	}
}

customElements.define('jhrm-modal', JhrmModal);