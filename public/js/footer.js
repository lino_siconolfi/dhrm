import { Request } from "./request.js";
import { JhrmAlign } from "./align.js";

let stealChildId = function(parent, id)
{
	let childElm = parent.querySelector(`#${id}`);
	childElm.id = null;

	return childElm;
}

export class JhrmFooter extends HTMLElement
{
	constructor()
	{
		super();

		this.Initialized = false;
	}

	connectedCallback()
	{
		if (false == this.Initialized)
		{
			this.style.display = "grid";
			this.style.gridTemplateColumns = "auto auto";
			this.style.gridTemplateColumns = "auto auto";
			this.style.padding = "0.5em 1.5em";
			this.style.backgroundColor = "black";

			let TitleVersion = document.createElement('div');
			TitleVersion.innerHTML =
				`<span style="font-size: 120%; margin-bottom="1em">JHRM-ECD </span>
				<span style="font-size: 80%;"><span id="span_version"></span> / <span id="span_db_version"></span></span>`;
			TitleVersion.style.gridRow = "1";
			TitleVersion.style.gridColumn = "1";
			TitleVersion.style.color = "white";
			TitleVersion.style.marginBottom = "1em";

			this._span_version = stealChildId(TitleVersion, 'span_version');
			this._span_db_version = stealChildId(TitleVersion, 'span_db_version');

			Request.get("./api/version").then(
				function(response)
				{
					this._span_version.innerHTML = `version ${response.version}`;
					this._span_db_version.innerHTML = response.db_version;
				}.bind(this)
			);

			let secondary_color = "lightgrey";

			let Contacts = document.createElement('div');
			Contacts.innerHTML = 
				`Demonstration Manager: Adam Becker (<a style="color: ${secondary_color};" href="mailto: adam.j.becker3.civ@mail.mil">adam.j.becker3.civ@mail.mil</a>)<br>
				Operation Manager: LTC April Verlo (<a style="color: ${secondary_color};" href="mailto: april.verlo@socom.mil">april.verlo@socom.mil</a>)`;
			Contacts.style.gridRow = "2";
			Contacts.style.gridColumn = "1";
			Contacts.style.color = secondary_color;

			let Info = document.createElement('div');
			Info.innerHTML = 
				`&copy; 2021 JPEO-CBRND<br>
				<a style="text-decoration: none; color: ${secondary_color};" href="#">Privacy Policy</a><br>
				<a style="text-decoration: none; color: ${secondary_color};" href="#">Security Policy</a><br>`;
			Info.style.gridRow = "1/3";
			Info.style.gridColumn = "2";
			Info.style.textAlign = "right";
			Info.style.lineHeight = "1.5";
			Info.style.color = secondary_color;

			this.append(TitleVersion, Contacts, Info);
			
			this.Initialized = true;
		}
	}
}

customElements.define('jhrm-footer', JhrmFooter);