export class JhrmAlign extends HTMLElement
{
	constructor()
	{
		super();

		this._built = false;
		this._valign = JhrmAlign.VNone;
		this._halign = JhrmAlign.HNone;

		this._built = false;
	}

	connectedCallback()
	{
		if (!this._built)
		{
			this.style.pointerEvents = "none";

			let Children = [];
			for (let i = 0; i < this.childNodes.length; ++i)
				Children.push(this.childNodes[i]);

			this._container = document.createElement('div');
			this._container.style.pointerEvents = "auto";
			this._container.style.height = "fit-content";
			this._container.style.minHeight = "min-content";
			this._container.style.width = "fit-content";
			this._container.style.minWidth = "min-content";

			this._container.append(...Children);

			this.style.display = "grid";
			this.style.boxSizing = "border-box";
			this.style.margin = "0";

			this.innerHTML = "";

			if (this.hasAttribute("top"))
			{
				this._valign = JhrmAlign.Top;
			}
			else if (this.hasAttribute("middle"))
			{
				this._valign = JhrmAlign.Middle;
			}
			else if (this.hasAttribute("bottom"))
			{
				this._valign = JhrmAlign.Bottom;
			}

			/////////////////////////////////

			if (this.hasAttribute("left"))
			{
				this._halign = JhrmAlign.Left;
			}
			else if (this.hasAttribute("center"))
			{
				this._halign = JhrmAlign.Center;
			}
			else if (this.hasAttribute("right"))
			{
				this._halign = JhrmAlign.Right;
			}

			this.append(this._container);
			this._built = true;

			this.Alignment = this._valign;
			this.Alignment = this._halign;
		}
	}

	set Alignment(val)
	{
		if (val >= JhrmAlign.HNone && val <= JhrmAlign.Right )
		{
			this._halign = val;
		}
		else if (val >= JhrmAlign.VNone && val <= JhrmAlign.Bottom)
		{
			this._valign = val;
		}
		else if (val == JhrmAlign.None)
		{
			this._valign = JhrmAlign.VNone;
			this._halign = JhrmAlign.HNone;
		}

		if (this._built)
		{
			if (val == JhrmAlign.Left)
			{
				this.style.gridTemplateColumns = "auto 1fr";
				this._container.style.gridColumn = "1";
			}
			else if (val == JhrmAlign.Center)
			{
				this.style.gridTemplateColumns = "1fr auto 1fr";
				this._container.style.gridColumn = "2";
			}
			else if (val == JhrmAlign.Right)
			{
				this.style.gridTemplateColumns = "1fr auto";
				this._container.style.gridColumn = "2";
			}
			else if (val == JhrmAlign.HNone)
			{
				this.style.gridTemplateColumns = "auto";
				this._container.style.gridColumn = "1";
			}
			else if (val == JhrmAlign.Top)
			{
				this.style.gridTemplateRows = "auto 1fr";
				this._container.style.gridRow = "1";
			}
			else if (val == JhrmAlign.Middle)
			{
				this.style.gridTemplateRows = "1fr auto 1fr";
				this._container.style.gridRow = "2";
			}
			else if (val == JhrmAlign.Bottom)
			{
				this.style.gridTemplateRows = "1fr auto";
				this._container.style.gridRow = "2";
			}
			else if (val == JhrmAlign.VNone)
			{
				this.style.gridTemplateRows = "auto";
				this._container.style.gridRow = "1";
			}
			else if (val == JhrmAlign.None)
			{
				this.style.gridTemplateColumns = "auto";
				this.style.gridTemplateRows = "auto";

				this._container.style.gridColumn = "1";
				this._container.style.gridRow = "1";
			}
		}
	}

	append()
	{
		if (this._built)
			this._container.append(...arguments);
		else
			super.append(...arguments);
	}

	appendChild(node)
	{
		if (this._built)
			this._container.appendChild(node);
		else
			super.appendChild(node);
	}
}

JhrmAlign.None = 0;

JhrmAlign.VNone = 1;
JhrmAlign.Top = 2;
JhrmAlign.Middle = 3;
JhrmAlign.Bottom = 4;

JhrmAlign.HNone = 5;
JhrmAlign.Left = 6
JhrmAlign.Center = 7;
JhrmAlign.Right = 8;

customElements.define('jhrm-align', JhrmAlign);
