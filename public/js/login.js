import { Cookie } from "./cookie.js"
/**
 * Class of static functionality related to login and login status.
 */
export class Login
{	
	/**
	 * Signs in and then reloads the current page.
	 */
	static signin(name, password)
	{
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4)
			{
				try
				{
					var data = JSON.parse(xhr.responseText);
					if (data.success)
					{
						let unCookie = new Cookie("user_name");
						unCookie.setValue(data.username);
						let fnCookie = new Cookie("first_name");
						fnCookie.setValue(data.first_name);
						let lnCookie = new Cookie("last_name");
						lnCookie.setValue(data.last_name);
						let roleCookie = new Cookie("role");
						roleCookie.setValue(data.role.level);
						document.location = 'welcome.html';
					}
					else
					{
						window.alert("Login failed!");
					}
				}
				catch (ex)
				{
					// For now, accept the login on exceptions
					// so we can still develop
					window.alert("Login threw exception.  Setting login state for development purposes.");
					let unCookie = new Cookie("user_name");
					let fnCookie = new Cookie("first_name");
					let lnCookie = new Cookie("last_name");
					let roleCookie = new Cookie("role");
					document.location = 'welcome.html';
				}
			}
		}.bind(this);
		xhr.open('POST', '/api/login', true);
		// prepare form data
		xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
		xhr.send(JSON.stringify({
			username : name,
			password : password
		}));
	}
	/**
	 * Signs out and then reloads the current page.
	 */
	static signout()
	{
		let unCookie = new Cookie("user_name");
		unCookie.delete();
		let fnCookie = new Cookie("first_name");
		fnCookie.delete();
		let lnCookie = new Cookie("last_name");
		lnCookie.delete();
		let roleCookie = new Cookie("role");
		roleCookie.delete();
	};
	/**
	 * Returns true if the user is logged in, false otherwise.
	 */
	static get LoggedIn()
	{
		if (!Login._loggedIn)
		{
			let unCookie = new Cookie("user_name");
			Login._loginName = unCookie.getValue();
			Login._loggedIn = (typeof(Login._loginName) !== 'undefined');
			if (Login._loggedIn)
			{
				let fnCookie = new Cookie("first_name");
				Login._first_name = fnCookie.getValue();
				let lnCookie = new Cookie("last_name");
				Login._last_name = lnCookie.getValue();
				let roleCookie = new Cookie("role");
				Login._role = parseInt(roleCookie.getValue());
			}
			else
			{
				unCookie.delete();
			}
		}
		return Login._loggedIn;
	}
	/**
	 * Gets the user name if there is a user logged in, otherwise returns undefined.
	 */
	static get UserName()
	{
		if (Login._loggedIn)
			return Login._loginName;
		return undefined;
	}
	static get FirstName()
	{
		if (Login._loggedIn)
			return Login._first_name;
		return undefined;
	}
	static get LastName()
	{
		if (Login._loggedIn)
			return Login._last_name;
		return undefined;
	}
	/**
	 * Gets the role if there is a user logged in, otherwise returns undefined.
	 */
	static get Role()
	{
		if (Login._loggedIn)
			return Login._role;
		return undefined;
	}
	/**
	 * Gets the QA Reviewer role if there is a user logged in, otherwise returns undefined.
	 */
	 static get HraQa()
	 {
		 if (Login._loggedIn)
			 return Login._hra_qa;
		 return undefined;
	 }
}
Login._loggedIn = false;