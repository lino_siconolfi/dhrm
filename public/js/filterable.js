'use strict'

const filterableClass = "filterable";
const filteredClass = "filtered";

/**
 * This is a mixin that adds filterable behavior to HTML tables. This mixin 
 * does not actually provide an interface. Instead, it is up to the developer 
 * to create filters and update them via UI elements. 
 * 
 * For example: 
 * 
 * myFilter = new StringContainsFilter(myTable, myColumnIndex);
 * document.getElementById('myTextBox').onchange = (e) => {
 *     myFilter.value = myTextBox.value;
 * }
 * 
 */
export class Filterable
{
	/**
	 * Sets classes and adds the class itself to the table. 
	 * 
	 * @param {HtmlTableElement} table 
	 */
	constructor(table)
	{
		this._css = `
			<style id="filterable-styles">
				.filtered 
				{
					display:none;
				}
			</style>
		`;
		if(!document.getElementById('filterable-styles'))
		{
			document.head.innerHTML += this._css;
		}
		this.table = table;
		table.classList.add(filterableClass);
		table.Filterable = this;
		this.filters = [];
	}

	/**
	 * Return an array of TH elements representing the columns of the table. 
	 * 
	 * Read only. 
	 */
	get columns()
	{
		return [].slice.call(this.table.querySelector('thead').querySelectorAll('th'));
	}

	/**
	 * Return an array of TR elements representing the rows of the table. 
	 * 
	 * Read only. 
	 */
	get rows()
	{
		return [].slice.call(this.table.querySelector('tbody').querySelectorAll('tr'));
	}

	/**
	 * Run all defined filters for each row. 
	 * 
	 * If a row does not match, the the HTML class attribute is set to the value 
	 * of filteredClass. It is up to the developer to decide what to do with 
	 * this - one assumes set it to display:none. 
	 * 
	 * If a row has the custom properties indicating that it has subrows, the 
	 * filter is considered to apply to all subrows as a group - that is to say, 
	 * a row and all of its subrows are either displayed of filtered out together 
	 * based on whether any of the subrows match the filter. 
	 * 
	 * @TODO if performance is ever an issue, this method could be updated to 
	 * break early on a filter match. I think that we do not have any tables 
	 * with enough rows to worry about this. 
	 */
	filter()
	{
		this.rows.forEach(row=>{

			// Subrows are filtered with their parent. 
			if(row.SubRow)
			{
				return;
			}

			// Create an array that will contain the row and any subrows that 
			// should be fintered with it as one unit. 
			let rows = [row];
			if(row.Rows && row.Rows > 1)
			{
				rows = rows.concat(row.SubRows);
			}

			// Determine if the set of rows should be filtered out. 
			let show = this.filters.reduce((show, filter)=>{
				return show && ( rows.reduce((show, row)=>{
					return show || filter.match(row);
				}, false) );
			}, true);

			// Set the filtered style on all rows in the set. 
			rows.forEach(row=>row.classList.toggle(filteredClass, !show));
		});
	}
}

/**
 * Base class for table filters. A table filter defines a filter operation and 
 * applies it whenever the user input is changed. 
 */
class Filter 
{
	/**
	 * Set up a new Filter
	 * 
	 * @param {HtmlTableElement} table Also supports custom JHRM components 
	 *     which have a table as a child element. 
	 * @param {Number | Function} columnIndex If this is a number, the filter 
	 *     will use the text content of the cell at that index to perform its 
	 *     comparison. If more complex logic is necessary, such as parsing a 
	 *     date into a Date object, set this to a function that takes the row
	 *     as an argument and returns the appropriate value for the filter. 
	 */
	constructor(table, columnIndex)
	{
		if(!(table instanceof HTMLElement))
		{
			throw new Error("Filter can only be applied to Filterable table.")
		}
		table = table.tagName=='TABLE' ? table : table.querySelector('table');
		if( !table || !table.Filterable || !(table.Filterable instanceof Filterable))
		{
			throw new Error("Filter can only be applied to Filterable table.")
		}
		this.table = table;
		this.table.Filterable.filters.push(this);

		if(typeof columnIndex == 'function')
		{
			this.getComparison = columnIndex;
		}
		else 
		{
			if(!this.table.Filterable.columns[columnIndex])
			{
				throw new Error("Invalid column index index specified for Filter.")
			}
			this.getComparison = function(row){
				return row.cells[columnIndex].innerText;
			};
		}
	}

	/**
	 * Set the value of the filter and re-run all filters. 
	 */
	set value(value)
	{
		this.validate(value);
		this._value = value;
		this.table.Filterable.filter();
	}

	/**
	 * Return the current value of the filter. 
	 * @return {*}
	 */
	get value()
	{
		return this._value;
	}

	/**
	 * Determine if the row matches the filter. 
	 * 
	 * Should be overridden by child classes. 
	 * 
	 * @param {HtmlTableRowElement} row 
	 * @return {Boolean} 
	 */
	match(row)
	{
		return true;
	}

	/**
	 * Validate the assigned value. Throw if invalid. 
	 * 
	 * Should be overridden by child classes. 
	 * 
	 * @param {*} value 
	 */
	validate(value)
	{
		return;
	}

	/**
	 * Clear the filter
	 * 
	 * May be overridden if the filter requires a specific clear value. 
	 */
	clear()
	{
		this.value = undefined;
	}
}

/**
 * Match any row where the specified table column contains the search string in 
 * it's text content. 
 */
export class StringContainsFilter extends Filter
{
	constructor(table, columnIndex)
	{
		super(table, columnIndex);
		this.value = '';
	}

	/**
	 * Perform a case-insensitive match against the value of this filter 
	 * in the innerText of the table cell. 
	 * 
	 * Matches all rows on an empty filter. 
	 * 
	 * @param {HtmlTableRowElement} row 
	 */
	match(row)
	{
		return ( this.value != '' )
			? this.getComparison(row)
				.toLowerCase()
				.indexOf(this.value.toLowerCase()) !== -1 
			: true;
	}

	/**
	 * Only allow string values or undefined. 
	 * 
	 * @param {String} value 
	 */
	validate(value)
	{
		if(typeof value != "string" && typeof value != 'undefined')
		{
			throw new Error("Invalid filter value for StringContainsFilter.")
		}
	}
}

/**
 * Match any row where the specified table column contains the search string in 
 * it's text content. 
 */
 export class NumericComparisonFilter extends Filter
 {
	 constructor(table, columnIndex, comparison_type)
	 {
		 super(table, columnIndex);
		 if(!NumericComparisonFilter.COMPARISON_TYPE[comparison_type])
		{
			throw new Error("Invalid comparison type for numeric filter.");
		}
		this.comparison_type = comparison_type;
	 }
 
	 /**
	  * Perform a case-insensitive match against the value of this filter 
	  * in the innerText of the table cell. 
	  * 
	  * Matches all rows on an empty filter. 
	  * 
	  * @param {HtmlTableRowElement} row 
	  */
	 match(row)
	 {
		if(!this.value)
		{
			return true;
		}
		switch(this.comparison_type)
		{
			case NumericComparisonFilter.COMPARISON_TYPE.EQUAL:
				return parseInt(this.getComparison(row), 10) == this.value;
			case NumericComparisonFilter.COMPARISON_TYPE.EQUALORGREATER: 
				return parseInt(this.getComparison(row), 10) >= this.value;
			case NumericComparisonFilter.COMPARISON_TYPE.EQUALORLESS:
				return parseInt(this.getComparison(row), 10) <= this.value;
		}
	 }
 
	 /**
	  * Only allow string values or undefined. 
	  * 
	  * @param {String} value 
	  */
	 validate(value)
	 {
		 if(typeof value != "number" && typeof value != 'undefined')
		 {
			 throw new Error("Invalid filter value for NumericComparisonFilter.")
		 }
	 }
 }

/**
 * Match a row based on comparison to a date. 
 */
export class DateComparisonFilter extends Filter 
{
	/**
	 * @param {HtmlTableElement} table 
	 * @param {Number | Function} columnIndex 
	 * @param {COMPARISON_TYPE} comparison_type 
	 */
	constructor(table, columnIndex, comparison_type)
	{
		super(table, columnIndex);
		if(!DateComparisonFilter.COMPARISON_TYPE[comparison_type])
		{
			throw new Error("Invalid comparison type for date filter.");
		}
		this.comparison_type = comparison_type;
	}

	/**
	 * Compare value to a date. 
	 * 
	 * @param {HtmlTableRowElement} row 
	 */
	match(row)
	{
		if(!this.value)
		{
			return true;
		}
		switch(this.comparison_type)
		{
			case DateComparisonFilter.COMPARISON_TYPE.EQUAL:
				return this.getComparison(row).date == this.value;
			case DateComparisonFilter.COMPARISON_TYPE.EQUALORGREATER: 
				return this.getComparison(row).date >= this.value;
			case DateComparisonFilter.COMPARISON_TYPE.EQUALORLESS:
				return this.getComparison(row).date <= this.value;
		}
	}
}

DateComparisonFilter.COMPARISON_TYPE = {
	EQUAL : "EQUAL", 
	EQUALORGREATER : "EQUALORGREATER",
	EQUALORLESS : "EQUALORLESS"
}
NumericComparisonFilter.COMPARISON_TYPE = {
	EQUAL : "EQUAL", 
	EQUALORGREATER : "EQUALORGREATER",
	EQUALORLESS : "EQUALORLESS"
}