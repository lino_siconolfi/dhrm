'use strict';
/**
 * This is a simple class to format dates
 */

export class DateFormat
{
	/**
	 * Take a string or date object and return a string formatted according to 
	 * the JHRM standard date format. 
	 * 
	 * @param {date} val 
	 */
	static jhrm(val)
	{
		if ( !(val instanceof Date) )
			throw new Error("Start Time needs to be a Date.");

		return val.getDate().toString() + "/"
			+ (val.getMonth() + 1).toString().padStart(2, '0') + "/"
			+ val.getFullYear().toString().padStart(2, '0') + " "
			+ val.getHours().toString() + ":"
			+ val.getMinutes().toString();
	}
}
