import { JhrmSwapSpace } from "./swap_space.js"

/**
 * Create a tab bar that can navigate a JhrmSwapSpace. 
 * 
 * USAGE
 * 
 * Link to a swap space by specifying the ID in the source attribute
 * 
 * <jhrm-tabs source="MySwapSpace"></jhrm-tabs>
 * 
 * You can also contain a swap space within the tabs component
 * 
 * <jhrm-tabs source="MySwapSpace">
 *     <jhrm-swap-space> ... </jhrm-swap-space>
 * </jhrm-tabs>
 * 
 * Tabs will only be created for elements in the swap space that have a data-tab
 * attribute set and which do not have a data-hidetab attribute set. 
 * 
 * <jhrm-swap-space id="MySwapSpace">
 *     <div id="MyPage" data-tab="My Page"></div> 
 * </jhrm-swap-space>
 * 
 * The active tab is set automatically by the selected index of the swap space
 * upon initialization. 
 * 
 * CUSTOM HANDLERS
 * 
 * By default, the tabs will swap beteen the swap space screens. However, you 
 * can attach a custom handler to replace this default behavor. This should 
 * only be done if you do not want the default behavor. If you simply want to 
 * take some additional action when the tab is changed, you should prefer to 
 * listen for the "swap" event on the swapspace. 
 * 
 * myTabs.handler=(evt)=>{ // custom handler }
 * 
 * The event is a standard link click event. You can access the id and the 
 * SwapSpace index of the SwapSpace element that corresponds to the tab the 
 * user clicked (the screen that should be displayed) as follows:
 * 
 * myTabs.handler=(evt)=>{
 *     let index = evt.target.dataset.index;
 *     let id = evt.target.dataset.id;
 *     MySwapSapce.VisibleIndex = index;
 * }
 * 
 * STYLING
 * 
 * This component creates a set of default style for the tabs which is
 * appropriate for being displayed above a "jhrm-boxed-section" div. For other
 * uses, you can override the styles by adding a class to your tabs element:
 * 
 * <jhrm-tabs class="custom-tabs"></jhrm-tabs>
 * 
 * The structre of the generated HTML you can attach styles to is as follows: 
 * 
 * <ul class="jhrm-tabs>
 *     <li class="[active|inactive]">
 *         <a href="">[title]</a>
 *     </li>
 * </ul>
 * 
 * For example, to remove all borders:
 * 
 * .custom-tabs .jhrm-tabs li {
 *     border: none;
 * }
 */
export class JhrmTabs extends HTMLElement
{
    /**
     * Define default tab styles. 
     */
	constructor()
	{
        super();
        this._css = `
            <style id="jhrm-tabs-styles">
                .jhrm-tabs,
                .jhrm-tabs li {
                margin: 0;
                padding: 0;
                list-style-type: none;
                }
                .jhrm-tabs {
                padding-left:2em;
                }
                .jhrm-tabs li {
                
                display: inline-block;
                padding: 0.6em 1em;
                border: 2px solid black;
                border-radius: 8px 8px 0 0;
                background-color:#fff;
                position:relative;
      
                top:2px;
                margin-left:-2px;
                background-color:#ddd;
                }
                .jhrm-tabs li a{
                text-decoration:none;
                color:#000;
                }
                .jhrm-tabs li.active {
                border-bottom-color: #fff;
                background-color:#fff;
                font-weight: bold;
                }
            </style>
        `;
        this.source = this.getAttribute('source');
        this._built = false;
	}

    /**
     * Add styles to header and attempt to find the linked swap space. 
     */
	connectedCallback()
	{
        if(!document.getElementById('jhrm-tabs-styles'))
        {
            document.head.innerHTML += this._css;
        }

        if(!this._built)
        {
            let source = this.source ? 
                document.getElementById(this.source) :
                this.querySelector('jhrm-swap-space');
            if(source && source instanceof JhrmSwapSpace)
            {
                this._tabcontainer = document.createElement('DIV');
                this.prepend(this._tabcontainer);
                this.swap_space = source;
                this.swap_space.addEventListener('swap', event=>this.render())
                this.swap_space.AllPages.forEach(page=>{
                    if(page.dataset.selected || (page.dataset.href && page.dataset.href == window.location.pathname))
                    {
                        this.swap_space.VisibleIndex = this.swap_space.AllPages.indexOf(page);
                    }
                });
                this._built = true;
                this.render();
            }
        }
    }	

    /**
     * Default handler for tab click events. Set the VisibleElement of the 
     * JhrmSwapSpace based on the clicked tab. 
     * 
     * @param {Event} event 
     */
    handle(event)
    {
        let index = event.target.dataset.index;
        if(index)
        {
            this.swap_space.VisibleIndex = index;
        }
    }

    /**
     * Replace the default handler with a custom one. 
     */
    set handler(val)
    {
        if(typeof val == 'function')
        {
            this.handle = val;
        }
    }
    get handler()
    {
        return this.handle;
    }
    
    /**
     * Render the tabs based on the linked SwapSpace. 
     */
    render()
    {
        if(!this._built)
        {
            this.connectedCallback();
        }
        if(this._built)
        {
            this._tabcontainer.innerHTML = `<ul class="jhrm-tabs">` + this.swap_space.AllPages
                .filter( page => page.dataset.tab && !page.dataset.hidetab )
                .map( page=>`<li class="${this.swap_space.VisibleElement==page ? 'active' : 'inactive'}">
                        <a data-id="${page.id}" data-index="${this.swap_space.AllPages.indexOf(page)}" href="${page.dataset.href || ''}">${page.dataset.tab}</a>
                    </li>`)
                .join('') + `</ul>`;
            this.querySelectorAll('a').forEach( el => el.onclick = event=> {
                if(!event.target.getAttribute("href"))
                {
                    event.preventDefault(); 
                    this.handle(event);
                }
            });
        }
    }

}

customElements.define('jhrm-tabs', JhrmTabs);