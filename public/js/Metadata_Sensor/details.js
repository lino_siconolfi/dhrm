import { JhrmTooltip } from "../tooltip.js"
import { MaterialIcon } from "../material_icon.js"
import { JhrmSelect } from "../select.js"
import { Request } from "../request.js"
import { SAMPLING_REASON, SITE_ARRAY_OPTION } from "/js/shared/Constants.js"
import { Datetime } from "/js/shared/datetime.js"

let createTooltip = function(tip)
{
	let icon = MaterialIcon.Create("help_outline");
	icon.style.fontSize = "0.9em";

	let tooltip = new JhrmTooltip();
	tooltip.Target = icon;
	tooltip.append(tip);

	let ret = document.createElement('span');
	ret.append(icon, tooltip);

	return ret;
}

let createLabel = function(text, row, col)
{
	let ret = document.createElement('div');
	ret.style.gridRow = row.toString();
	ret.style.gridColumn = col.toString();
	ret.style.whiteSpace = "nowrap";

	ret.append(text);

	return ret;
};

let createRadio = function(name, value, col, text)
{
	let input = document.createElement('input');
	input.type = "radio";
	input.name = name;
	input.value = value;
	let elmText = (undefined == text) ? value : text;

	let ret = document.createElement('div');
	ret.style.gridColumn = col.toString();
	ret.style.whiteSpace = "nowrap";
	ret.append(input, document.createTextNode(" " + elmText) );

	ret.RadioElement = input;

	return ret;
};

let createRadioGrid = function(row, col)
{
	let elm = document.createElement('div');

	elm.style.display = "grid";
	elm.style.gridRow = row.toString();
	elm.style.gridColumn = col.toString();
	elm.style.gridTemplateColumns = "1fr 1fr 5fr 5fr";
	elm.style.gridColumnGap = "0.75em";

	return elm;
};

export class MetaDataArrayDetailsForm extends HTMLElement
{
	constructor()
	{
		super();

		let makeBr = function()
		{
			return document.createElement('br');
		};


		this.main_layout = document.createElement('div');
		this.main_layout.style.display = "grid";
		this.main_layout.style.gridTemplateColumns = "auto 1fr";
		this.main_layout.style.gridGap = "0.5em";

		let textIndicatesRequired = document.createElement('div');
		textIndicatesRequired.style.marginBottom = "1em";
		textIndicatesRequired.style.fontWeight = "bold";
		textIndicatesRequired.innerText = "* Indicates required";

		this._SiteArrayRadioElements = [
			createRadio("sitearray", SITE_ARRAY_OPTION.Yes, 1, SITE_ARRAY_OPTION.Yes),
			createRadio("sitearray",  SITE_ARRAY_OPTION.No, 2, SITE_ARRAY_OPTION.No),
			createRadio("sitearray", SITE_ARRAY_OPTION.Combo, 3, SITE_ARRAY_OPTION.Combo)
		];

		this._RadioSiteArrayGrid = createRadioGrid(2, 2);
		this._RadioSiteArrayGrid.style.minWidth = "1em";
		this._RadioSiteArrayGrid.append( ...this._SiteArrayRadioElements );
		this._SiteArrayRadioElements[0] = this._SiteArrayRadioElements[0].RadioElement;
		this._SiteArrayRadioElements[1] = this._SiteArrayRadioElements[1].RadioElement;
		this._SiteArrayRadioElements[2] = this._SiteArrayRadioElements[2].RadioElement;

		this._SiteArrayRadioElements[0].checked = true;

		this._SiteSamplingReasonRadioElements = [
			createRadio("samplingreason", SAMPLING_REASON.OEHSA, 1, SAMPLING_REASON.OEHSA),
			createRadio("samplingreason", SAMPLING_REASON.Incident_Other, 2, SAMPLING_REASON.Incident_Other),
			createRadio("samplingreason", SAMPLING_REASON.CBRN, 3, SAMPLING_REASON.CBRN)
		];

		this._RadioSamplingReasonGrid = createRadioGrid(8, 2);
		this._RadioSamplingReasonGrid.style.minWidth = "1em";
		this._RadioSamplingReasonGrid.append( ...this._SiteSamplingReasonRadioElements );
		this._SiteSamplingReasonRadioElements[0] = this._SiteSamplingReasonRadioElements[0].RadioElement;
		this._SiteSamplingReasonRadioElements[1] = this._SiteSamplingReasonRadioElements[1].RadioElement;
		this._SiteSamplingReasonRadioElements[2] = this._SiteSamplingReasonRadioElements[2].RadioElement;

		this._SiteSamplingReasonRadioElements[0].checked = true;

		this._inputLocation = new JhrmSelect();
		this._inputLocation.style.width = "25rem";
		this._inputLocation.addOption('none', 'none');

		Request.get('/api/enhancedhra/locations').then(
			function(response)
			{
				for ( let record of response)
				{
					this._inputLocation.addOption(record.location_name, record.location_name);
				}
			}.bind(this),
			function(error)
			{
				// Known empty function
			}
		);

		let makeSiteMGRS = function()
		{
			let elm = document.createElement('span');
			elm.style.display = "inline-block";
			elm.innerText = "*Site MGRS:";
			elm.id = "labelSiteMGRS";
			elm.style.gridRow = "5";
			elm.style.gridColumn = "1";

			return elm;
		};

		this._inputSiteMGRS = document.createElement('input');
		this._inputSiteMGRS.style.width = "25rem";
		this._inputSiteMGRS.style.marginRight = "0.5em";


		let tooltipSiteMGRSText = document.createElement('div');
		tooltipSiteMGRSText.style.textAlign = "center";
		tooltipSiteMGRSText.innerHTML =
			"Enter Site MGRS (Location of the Base, Camp, facility, etc.)<br>" +
			"Example: 38SMC1661906536";

		let tooltipSiteMGRS = createTooltip(tooltipSiteMGRSText);

		let inputSiteMGRS = document.createElement('span');
		inputSiteMGRS.id = "inputSiteMGRS";
		inputSiteMGRS.append(this._inputSiteMGRS, tooltipSiteMGRS);
		inputSiteMGRS.style.gridRow = "5";
		inputSiteMGRS.style.gridColumn = "2";


		let makeLocationDescription = function()
		{
			let elm = document.createElement('span');
			elm.style.display = "inline-block";
			elm.innerText = "* Location Description:";
			elm.id = "labelLocationDescription";
			elm.style.gridRow = "6";
			elm.style.gridColumn = "1";

			return elm;
		};

		let makeSiteOccupationDate = function()
		{
			let elm = document.createElement('span');
			elm.style.display = "inline-block";
			elm.innerText = "Site Occupation Date (if known):";
			elm.id = "labelSiteOccupationDate";
			elm.style.gridRow = "7";
			elm.style.gridColumn = "1";

			return elm;
		};

		this._inputLocationDescription = document.createElement('input');
		this._inputLocationDescription.style.width = "25rem";
		this._inputLocationDescription.style.marginRight = "0.5em";

		let inputLocationDescription = document.createElement('span');
		inputLocationDescription.id = "inputLocationDescription";
		inputLocationDescription.append(this._inputLocationDescription);
		inputLocationDescription.style.gridRow = "6";
		inputLocationDescription.style.gridColumn = "2";

		this._inputSiteOccupationDate = document.createElement('jhrm-datepicker');
		this._inputSiteOccupationDate.id = "inputSiteOccupationDate";
		this._inputSiteOccupationDate.style.width = "25rem";
		this._inputSiteOccupationDate.style.marginRight = "0.5em";

		let inputSiteOccupationDate = document.createElement('span');
		inputSiteOccupationDate.append(this._inputSiteOccupationDate);
		inputSiteOccupationDate.style.gridRow = "7";
		inputSiteOccupationDate.style.gridColumn = "2";

		//////////////////////////

		this._inputIncident_Other = document.createElement('input');
		this._inputIncident_Other.style.width = "25rem";
		this._inputIncident_Other.style.marginRight = "0.5em";
		this._inputIncident_Other.id = "inputIncident_Other";

		this.layoutIncident_Other = document.createElement('span');
		this.layoutIncident_Other.style.display = "flex";
		this.layoutIncident_Other.id = "Incident_Other";
		this.layoutIncident_Other.style.gridRow = "9";
		this.layoutIncident_Other.style.gridColumn = "2";


		this.layoutIncident_Other.append(
			this._inputIncident_Other
		);

		//////////////////////////

		this._inputAttachments = document.createElement('input');
		this._inputAttachments.id = "Attachment_Input";

		this._buttonUploadAttachments = document.createElement('button');
		this._buttonUploadAttachments.className = "small-button";
		this._buttonUploadAttachments.id = "bUpload";
		this._buttonUploadAttachments.style.margin = "0 1em 0 1em";
		this._buttonUploadAttachments.innerText = "Upload";

		let layoutAttachments = document.createElement('span');
		layoutAttachments.style.gridRow = "10";
		layoutAttachments.style.gridColumn = "2";
		layoutAttachments.append(
			this._inputAttachments,
			this._buttonUploadAttachments,
		);

		this.main_layout.append(
			createLabel(textIndicatesRequired, 1, "1/3"),
			createLabel("* Fixed Site Array: ", 2, 1),
			this._RadioSiteArrayGrid,
			createLabel("* DOEHRS Location: ", 3, 1),
			this._inputLocation,
			makeSiteMGRS(),
			inputSiteMGRS,
			makeLocationDescription(),
			inputLocationDescription,
			makeSiteOccupationDate(),
			inputSiteOccupationDate,
			createLabel("* Sampling reason: ", 8, 1),
			this._RadioSamplingReasonGrid,
			createLabel("", 9, 1),
			this.layoutIncident_Other,
			createLabel("Attachments:", 10, 1),
			layoutAttachments
		);

		this._StartDateTime = new Datetime();

		//////////////////////////////

		this._Built = false;
	}

	connectedCallback()
	{
		if ( !this._Built )
		{
			this.append(this.main_layout);
			this._Built = true;
		}
	}

	get SiteArray()
	{
		let elements = this._SiteArrayRadioElements;

		for(let i = 0; i < elements.length; i++)
		{
			if( elements[i].checked )
				return elements[i].value;
		}
	}

	set SiteArray(val)
	{
		let elements = this._SiteArrayRadioElements;


		for(let i = 0; i < elements.length; i++)
		{
			if( elements[i].value == val )
			{
				elements[i].checked = true;
				return;
			}
		}
	}

	get SiteMGRS()
	{
		return this._inputSiteMGRS.value;
	}

	set SiteMGRS(val)
	{
		this._inputSiteMGRS.value = val;
	}

	get LocationDescription()
	{
		return this._inputLocationDescription.value;
	}

	set LocationDescription(val)
	{
		this._inputLocationDescription.value = val;
	}

	set SiteOccupationDate(val)
	{

		if ( !(val instanceof Datetime) )
			throw new Error("Start Time needs to be a Datetime.");

		this._StartDateTime = val;

		this._inputSiteOccupationDate.value = this._StartDateTime.dateValue;
	}

	get SiteOccupationDate()
	{
		return this._StartDate;
	}

	get SelectedSampleReason()
	{
		let elements = this._SiteSamplingReasonRadioElements;

		for(let i = 0; i < elements.length; i++)
		{
			if( elements[i].checked )
				return elements[i].value;
		}
	}

	set SelectedSampleReason(val)
	{
		let elements = this._SiteSamplingReasonRadioElements;

		for(let i = 0; i < elements.length; i++)
		{
			if( elements[i].value == val )
			{
				elements[i].checked = true;
				return;
			}
		}
	}

	get SampleReason()
	{
		return this._inputIncident_Other.value;
	}

	set SampleReason(val)
	{
		this._inputIncident_Other.value = val;
	}

	get Attachments()
	{
		return this._inputAttachments.value;
	}

	set Attachments(val)
	{
		this._inputAttachments.value = val;
	}

	get Location()
	{
		try
		{
			return this._inputLocation.Value;
		}
		catch(err)
		{
			return undefined;
		}
	}

	set Location(val)
	{
		this._inputLocation.Value = val;
	}

	get SampleReasonSelection()
	{
		try
		{
			return this._RadioSamplingReasonGrid.Name;
		}
		catch(err)
		{
			return undefined;
		}
	}

	set SampleReasonSelection(val)
	{
		this._RadioSamplingReasonGrid.value = val;
	}

	set onSearch(func)
	{
		this._buttonSearch.onclick = func;
	}

	set onLocationChange(func)
	{
		this._inputLocation.onchange = func;
	}

	set onRadioSelectionChange(func)
	{
		this._RadioSamplingReasonGrid.onchange = func;
	}
}

customElements.define('jhrm-meta-details-form', MetaDataArrayDetailsForm);
