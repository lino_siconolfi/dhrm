import { JhrmHelpIcon } from "../help_icon.js"
import { Request } from "../request.js";
import { Datetime } from "../shared/datetime.js";

export class MetaDataHraSummaryRow extends HTMLTableRowElement
{
	constructor()
	{
		super();

		this._Built = false;

		this._ElmId = document.createElement('td');
		this._ElmDateCreated = document.createElement('td');
		this._ElmArrayType = document.createElement('td');
		this._ElmSensorType = document.createElement('td');
		this._ElmNumberOfSensors = document.createElement('td');
		this._ElmLocationMGRS = document.createElement('td');
		this._ElmLocationDescription = document.createElement('td');
		this._ElmSamplingReason = document.createElement('td');
		this._ElmSamplingDataReceived = document.createElement('td');
		
		this._Id = undefined;
		this._SamplingDataReceived = undefined;

	}

	connectedCallback()
	{
		if ( !this._Built )
		{
			this.append(
				this._ElmId,
				this._ElmDateCreated,
				this._ElmArrayType,
				this._ElmSensorType,
				this._ElmNumberOfSensors,
				this._ElmLocationMGRS,
				this._ElmLocationDescription,
				this._ElmSamplingReason,
				this._ElmSamplingDataReceived
			);

			this._Built = true;
		}
	}

	set Id(val)
	{	
		let searchParams = new URLSearchParams();
			searchParams.append("metadata", val);
			searchParams.append("page", "MetaArrayDetailsPage");

			let queryString = "?" + searchParams.toString();

			let anchorElement = document.createElement('a');
			anchorElement.href = "./new_meta.html" + queryString;
			anchorElement.innerText = val;

			this._ElmId.innerHTML = "";
			this._ElmId.append(anchorElement);
	}

	get Id()
	{
		return this._ElmId.textContent;
	}

	set DateCreated(val)
	{
		if ( !(val instanceof Datetime) )
			throw new Error("Date Created needs to be a Datetime.");

		this._ElmDateCreated.innerText = val;
	}

	get DateCreated()
	{
		return this._ElmDateCreated;		
	}

	set ArrayType(val)
	{
		this._ElmArrayType.innerText = val;
	}

	get ArrayType()
	{
		return this._ElmArrayType.textContent;
	}

	set SensorType(val)
	{
		this._ElmSensorType.innerText = val;
	}

	get SensorType()
	{
		return this._ElmSensorType.textContent;
	}

	set NumberOfSensors(val)
	{
		this._ElmNumberOfSensors.innerText = val;
	}

	get NumberOfSensors()
	{
		return this._ElmNumberOfSensors.textContent;
	}

	set SensorLocationMGRS(val)
	{
		this._ElmLocationMGRS.innerText = val;
	}

	get SensorLocationMGRS()
	{
		return this._ElmLocationMGRS.textContent;
	}

	set LocationDescription(val)
	{
		this._ElmLocationDescription.innerText = val;
	}

	get LocationDescription()
	{
		return this._ElmLocationDescription.textContent;
	}

	set SamplingReason(val)
	{
		this._ElmSamplingReason.innerText = val;
	}

	get SamplingReason()
	{
		return this._ElmSamplingReason.textContent;
	}

	set SamplingDataReceived(val)
	{
		this._SamplingDataReceived = val;
		this._ElmSamplingDataReceived.innerText = (val) ? "Yes" : "No";
	}

	get SamplingDataReceived()
	{
		return this._SamplingDataReceived;
	}

	static fromObjData(record)
	{
		
		let nextRow = new MetaDataHraSummaryRow();
		nextRow.Id = record.sensor_array_id;
		nextRow.DateCreated = record.date_created;
		nextRow.ArrayType = record.array_type;
		nextRow.SensorType = record.sensor_type;
		nextRow.NumberOfSensors = record.number_of_sensors_in_array
		nextRow.SensorLocationMGRS = record.array_mgrs;
		if (record.doehrs_location == 'none')
			{
				nextRow.LocationDescription = record.array_location_description;
			}
		else
			{
				nextRow.LocationDescription = record.doehrs_location;
			}
		nextRow.SamplingReason = record.sampling_reason;
		nextRow.SamplingDataReceived = record.sampling_data_received;

		return nextRow;
	}
}

customElements.define('jhrm-meta-summary-row', MetaDataHraSummaryRow, { extends: 'tr' });

export class MetaDataSummaryTable extends HTMLElement
{
	constructor()
	{
		super();

		this._Built = false;

		let createTH = (contents) =>
		{
			let elm = document.createElement('th');
			elm.append(contents);

			return elm;
		}

		this._ElmHeader = document.createElement('thead');
		this._ElmBody = document.createElement('tbody');

		this._ElmHeader.append(
			createTH('ID'),
			createTH('Date Created'),
			createTH('Fixed Array'),
			createTH('Sensor Type'),
			createTH('Number of Sensors'),
			createTH('Array Location (MGRS)'),
			createTH('Array Location Description'),
			createTH('Sampling Reason'),
			createTH('Sampling Data Received')
		);
		
		this._ElmTable = document.createElement('table');
		this._ElmTable.append(this._ElmHeader, this._ElmBody);

		this.SummaryMetaDataHRA();
		
	}

	connectedCallback()
	{
		if (!this._Built)
		{
			this.append(this._ElmTable);
			this._Built = true;
		}
	}

	async SummaryMetaDataHRA()
		{
			try
			{
				let results = await Request.get('/api/metadata/sensorarrayprofile');
			
			
				if (results)
				{
					
					results.forEach(
						record =>
						{
							let nextRow = MetaDataHraSummaryRow.fromObjData(record);
							this.appendSummaryRow(nextRow);
						}
					);
				}
			}
			catch(err)
			{
				return undefined;
			}
		}

	appendSummaryRow(row)
	{
		if (row instanceof MetaDataHraSummaryRow)
			this._ElmBody.append(row);
	}
}

customElements.define('jhrm-metadata-summary-table', MetaDataSummaryTable);