import { JhrmTooltip } from "../tooltip.js"
import { MaterialIcon } from "../material_icon.js"
import { Request } from "../request.js"

let createLabel = function(text, row, col, tipContents)
{
	let ret = document.createElement('div');
	ret.style.gridRow = row.toString();
	ret.style.gridColumn = col.toString();
	ret.style.whiteSpace = "nowrap";
    ret.append(text);
    
    if (undefined != tipContents)
	{
		let HelpIcon = MaterialIcon.Create("help_outline");
		let Tooltip = new JhrmTooltip();

		if (tipContents instanceof HTMLElement)
			Tooltip.append(tipContents);
		else
			Tooltip.innerHTML = tipContents;
		
		Tooltip.Target = HelpIcon;
		ret.append( HelpIcon, Tooltip );
	}

    return ret;
};

let createTextInput = function(row, col)
{
	let ret = document.createElement('input');
	ret.style.width = "100%";
	ret.style.gridRow = row.toString();
	ret.style.gridColumn = col.toString();

	return ret;
}

export class personalMetadataForm extends HTMLElement
{
    constructor()
    {
        super();

        this._inputName = createTextInput(1, 2);
        this._inputEmail = createTextInput(2, 2);
        this._inputCommPhone = createTextInput(3, 2);
        this._inputDSNPhone = createTextInput(4,2);
        this._inputUnit = createTextInput(5, 2);

        
        this._Built = false;
    }

    connectedCallback()
    {
        if ( !this._Built )
        {
                
            this.style.display = "grid";
            this.style.gridTemplateColumns = "auto fit-content(25rem) 1fr auto 1fr auto";
            this.style.gridGap = "1rem 0.5rem";

            let NameTipText = document.createElement('span');
			NameTipText.style.textAlign = "center";
            NameTipText.innerHTML = "Enter User Name";
            
            let EmailTipText = document.createElement('span');
			EmailTipText.style.textAlign = "center";
            EmailTipText.innerHTML = "Enter Email Address";
            
            let NameLabel = createLabel("* Name: ", 1, 1,NameTipText);          
            NameLabel.style.textAlign = "right";
            NameLabel.style.marginLeft = "1rem";
            
            let EmailLabel = createLabel("* Email: ", 2, 1,EmailTipText);
            EmailLabel.style.textAlign = "right";
            EmailLabel.style.marginLeft = "1rem";
            
            let CommercialPhoneLabel = createLabel(" Commercial Phone Number: ", 3, 1);
            CommercialPhoneLabel.style.textAlign = "right";
            CommercialPhoneLabel.style.marginLeft = "1rem";

            let DSNPhoneLabel = createLabel(" DSN Phone Number: ", 4, 1);
            DSNPhoneLabel.style.textAlign = "right";
            DSNPhoneLabel.style.marginLeft = "1rem";
            
            let UnitLabel = createLabel("* Unit: ", 5, 1);
            UnitLabel.style.textAlign = "right";
            UnitLabel.style.marginLeft = "1rem";
           
            
            this.append(
                NameLabel, this._inputName,
                EmailLabel, this._inputEmail,
                CommercialPhoneLabel, this._inputCommPhone,
                DSNPhoneLabel, this._inputDSNPhone,
                UnitLabel, this._inputUnit,
            );
            
            this._Built = true;
        }
    }
        
    get Name()
    {
      return this._inputName.value;
     }

    set Name(val)
    {
         this._inputName.value = val;
     }

    get Email()
    {
         return this._inputEmail.value;
    }

    set Email(val)
    {
        this._inputEmail.value = val;
     }

    get PhoneNumber()
    {
        return this._inputPhone.value;
    }

    set PhoneNumber(val)
    {
        this._inputPhone.value = val;
    }

    get Unit()
    {
        return this._inputUnit.value;
    }

    set Unit(val)
    {
         this._inputUnit.value = val;
    }

    
}

customElements.define('jhrm-personal-metadata-form', personalMetadataForm);