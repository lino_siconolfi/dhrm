import { JhrmTooltip } from "../tooltip.js"
import { MaterialIcon } from "../material_icon.js"
import { JhrmSelect } from "../select.js"
import { Datetime } from "../shared/datetime.js";
import { SENSOR_EMPLOYMENT_OPTION, SENSOR_TYPE} from "/js/shared/Constants.js"


let createTooltip = function(tip)
{
	let icon = MaterialIcon.Create("help_outline");
	icon.style.fontSize = "0.9em";

	let tooltip = new JhrmTooltip();
	tooltip.Target = icon;
	tooltip.append(tip);

	let ret = document.createElement('span');
	ret.append(icon, tooltip);

	return ret;
}

let createLabel = function(text, row, col)
{
	let ret = document.createElement('div');
	ret.style.gridRow = row.toString();
	ret.style.gridColumn = col.toString();
	ret.style.whiteSpace = "nowrap";

	ret.append(text);

	return ret;
};

let createRadio = function(name, value, col, text)
{
	let input = document.createElement('input');
	input.type = "radio";
	input.name = name;
	input.value = value;
	let elmText = (undefined == text) ? value : text;

	let ret = document.createElement('div');
	ret.style.gridColumn = col.toString();
	ret.style.whiteSpace = "nowrap";
	ret.append(input, document.createTextNode(" " + elmText) );

	ret.RadioElement = input;

	return ret;
};

let createRadioGrid = function(row, col)
{	
	let elm = document.createElement('div');

	elm.style.display = "grid";
	elm.style.gridRow = row.toString();
	elm.style.gridColumn = col.toString();
	elm.style.gridTemplateColumns = "1fr 1fr 5fr 5fr";
	elm.style.gridColumnGap = "0.75em";

	return elm;
};



export class MetaDataSensorDetailsForm extends HTMLElement

{
	constructor()
	{
		super();

		
		this.main_layout = document.createElement('div');
		this.main_layout.style.display = "grid";
		this.main_layout.style.gridTemplateColumns = "auto 1fr";
		this.main_layout.style.gridGap = "0.5em";

		let textIndicatesRequired = document.createElement('div');
		textIndicatesRequired.style.marginBottom = "1em";
		textIndicatesRequired.style.fontWeight = "bold";
		textIndicatesRequired.innerText = "* Indicates required";
	

		let makeSensorMGRS = function()
		{
			let elm = document.createElement('span');
			elm.style.display = "inline-block";
			elm.innerText = "*Sensor MGRS Location:";
			elm.id = "labelSensorMGRS";
			elm.style.gridRow = "3";
			elm.style.gridColumn = "3";

			return elm;
		};

		//Sensor Type
		this._inputSensorType = new JhrmSelect();
		this._inputSensorType.addOption('JCAD',SENSOR_TYPE.JCAD);
		this._inputSensorType.addOption('MultiRAE',SENSOR_TYPE.MultiRAE);
		this._inputSensorType.addOption('Hapsite',SENSOR_TYPE.Hapsite);
		
		let inputSensorType = document.createElement('span');
		inputSensorType.id = "inputSensorType";
		inputSensorType.append(this._inputSensorType);
		inputSensorType.style.gridRow = "2";
		inputSensorType.style.gridColumn = "2";

		//Serial Number
		this._inputSerialNumber = document.createElement('input');
		this._inputSerialNumber.style.width = "18rem";
		this._inputSerialNumber.style.marginRight = "0.5em";
		
		let inputSerialNumber = document.createElement('span');
		inputSerialNumber.id = "inputSerialNumber";
		inputSerialNumber.append(this._inputSerialNumber);
		inputSerialNumber.style.gridRow = "3";
		inputSerialNumber.style.gridColumn = "2";

		//Software Version
		this._inputSWVersion = document.createElement('input');
		this._inputSWVersion.style.width = "18rem";
		this._inputSWVersion.style.marginRight = "0.5em";
		
		let inputSWVersion = document.createElement('span');
		inputSWVersion.id = "inputSWVersion";
		inputSWVersion.append(this._inputSWVersion);
		inputSWVersion.style.gridRow = "4";
		inputSWVersion.style.gridColumn = "2";

		//Hardware Version
		this._inputHWVersion = document.createElement('input');
		this._inputHWVersion.style.width = "18rem";
		this._inputHWVersion.style.marginRight = "0.5em";
		this._inputHWVersion.type = "number";
		
		let inputHWVersion = document.createElement('span');
		inputHWVersion.id = "inputHWVersion";
		inputHWVersion.append(this._inputHWVersion);
		inputHWVersion.style.gridRow = "5";
		inputHWVersion.style.gridColumn = "2";

		//Manufacturing Date
		this._inputMFRDate = document.createElement('jhrm-datepicker');
		this._inputMFRDate.id = "enterMFRDate";
		this._inputMFRDate.style.width = "18rem";
		this._inputMFRDate.style.marginRight = "0.5em";

		let tooltipMFRDateText = document.createElement('div');
		tooltipMFRDateText.style.width = "18em";
		tooltipMFRDateText.innerHTML = "Enter the Manufacturing Date if known.";
		let tooltipMFRDate = createTooltip(tooltipMFRDateText);

		let inputMFRDate = document.createElement('span');
		inputMFRDate.id = "inputMFRDate";
		inputMFRDate.append(this._inputMFRDate, tooltipMFRDate);
		inputMFRDate.style.gridRow = "2";
		inputMFRDate.style.gridColumn = "4";
		inputMFRDate.style.whiteSpace = "nowrap";

		//Sensor MGRS Location
		this._inputSensorMGRS = document.createElement('input');
		this._inputSensorMGRS.style.width = "18rem";
		this._inputSensorMGRS.style.marginRight = "0.5em";
		this._inputSensorMGRS.minLength = "8";
		this._inputSensorMGRS.maxLength = "15";
		
		let tooltipSensorMGRSText = document.createElement('div');
		tooltipSensorMGRSText.style.textAlign = "center";
		tooltipSensorMGRSText.innerHTML = "Enter the Sensor MGRS coordinates.";
		let tooltipSensorMGRS = createTooltip(tooltipSensorMGRSText);

		let inputSensorMGRS = document.createElement('span');
		inputSensorMGRS.id = "inputSensorMGRS";
		inputSensorMGRS.append(this._inputSensorMGRS, tooltipSensorMGRS);
		inputSensorMGRS.style.gridRow = "3";
		inputSensorMGRS.style.gridColumn = "4";
		inputSensorMGRS.style.whiteSpace = "nowrap";
		
		//Sensor Location Description
		let makeLocationDescription = function()
		{
			let elm = document.createElement('span');
			elm.style.display = "inline-block";
			elm.innerText = "Sensor Location Description:";
			elm.id = "labelSensorLocationDescription";
			elm.style.gridRow = "4";
			elm.style.gridColumn = "3";

			return elm;
		};

		this._inputSensorLocationDescription = document.createElement('input');
		this._inputSensorLocationDescription.style.width = "18rem";
		this._inputSensorLocationDescription.style.marginRight = "0.5em";

		let tooltipLocationDescText = document.createElement('div');
		tooltipLocationDescText.style.textAlign = "center";
		tooltipLocationDescText.innerHTML = "Enter a more descriptive location"
		let tooltipLocationDesc = createTooltip(tooltipLocationDescText);
		
		let inputSensorLocationDescription = document.createElement('span');
		inputSensorLocationDescription.id = "inputSensorLocationDescription";
		inputSensorLocationDescription.append(this._inputSensorLocationDescription, tooltipLocationDesc);
		inputSensorLocationDescription.style.gridRow = "4";
		inputSensorLocationDescription.style.gridColumn = "4";

		let makeSensorEmployment = function()
		{
			let elm = document.createElement('span');
			elm.style.display = "inline-block";
			elm.innerText = "Sensor Employment:";
			elm.id = "labelSensorEmployment";
			elm.style.gridRow = "5";
			elm.style.gridColumn = "3";

			return elm;
		};

		//Sensor Employment radio buttons
		this._SensorEmploymentRadioElements = [
			createRadio("sensoremployment", SENSOR_EMPLOYMENT_OPTION.SingleSample, 1, SENSOR_EMPLOYMENT_OPTION.SingleSample),
			createRadio("sensoremployment", SENSOR_EMPLOYMENT_OPTION.Datalog, 2, SENSOR_EMPLOYMENT_OPTION.Datalog)
		];

		this._RadioSensorEmploymentGrid = createRadioGrid(5,4);
		this._RadioSensorEmploymentGrid.style.minWidth = "1em";
		this._RadioSensorEmploymentGrid.id = "radiosensorgrid";
		this._RadioSensorEmploymentGrid.append(...this._SensorEmploymentRadioElements );
		this._SensorEmploymentRadioElements[0] = this._SensorEmploymentRadioElements[0].RadioElement;
		this._SensorEmploymentRadioElements[1] = this._SensorEmploymentRadioElements[1].RadioElement;

		//add to array button
		this._addToArrayButton = document.createElement('button');
		this._addToArrayButton.className = "medium-button";
		this._addToArrayButton.id ="addtoarraybutton";
		this._addToArrayButton.style.maxWidth = "10em";
		this._addToArrayButton.style.gridRow = "6";
		this._addToArrayButton.style.gridColumn = "4";
		this._addToArrayButton.innerText = "Add To Array";

		//array id
		this._arrayID = document.createElement('span');
		this._arrayID.style.width = "18rem";
		this._arrayID.id = "arrayidvalue";
		this._arrayID.style.gridRow = "8";
		this._arrayID.style.gridColumn = "1";
		this._arrayID.style.color = "Blue";
		
		this.main_layout.append(	
			createLabel(textIndicatesRequired, 1, "1/3"),
			createLabel("*Sensor Type: ", 2, 1),
			inputSensorType,
			createLabel("*Serial Number: ", 3, 1),
			inputSerialNumber,
			createLabel("*SW Version: ", 4, 1),
			inputSWVersion,
			createLabel("HW Version: ", 5, 1),
			inputHWVersion,
			createLabel("MFR Date: ", 2, 3),
			inputMFRDate,
			createLabel("*Sensor MGRS Location: ", 3, 3),
			inputSensorMGRS,
			createLabel("*Sensor Location Description: ", 4, 3),
			inputSensorLocationDescription,
			this._addToArrayButton,
			makeSensorEmployment(),
			this._RadioSensorEmploymentGrid,
			this._arrayID
		);

		this._MFRDate = new Date();

		this._Built = false;
	}

	connectedCallback()
	{
		if ( !this._Built )
		{
			this.append(this.main_layout);
			this._Built = true;
		}
	}

	get SensorType()
	{
		try
		{
			return this._inputSensorType.Value;
		}
		catch (err)
		{
			return undefined;
		}
	}

	set SensorType(val)
	{
		this._inputSensorType.Value = val;
	}

	get SerialNumber()
	{
		return this._inputSerialNumber.value;
	}

	set SerialNumber(val)
	{
		this._inputSerialNumber.value = val;
	}

	get SWVersion()
	{
		return this._inputSWVersion.value;
	}

	set SWVersion(val)
	{
		this._inputSWVersion.value = val;
	}

	get HWVersion()
	{
		return this._inputHWVersion.value;
	}

	set HWVersion(val)
	{
		this._inputHWVersion.value = val;
	}

	get MFRDate()
	{
		return this._inputMFRDate.value;
	}

	set MFRDate(val)
	{
		if (val == null)
		{
			this._inputMFRDate.value = '';
		}
		else
		{
			if ( !(val instanceof Datetime) )
				throw new Error("Manufacture Date needs to be a Datetime.");
			
			this._MFRDate = val;

			this._inputMFRDate.value = val.dateValue;
		}
	}

	get SensorMGRS()
	{
		return this._inputSensorMGRS.value;
	}

	set SensorMGRS(val)
	{
		this._inputSensorMGRS.value = val;
	}

	get SensorLocationDescription()
	{
		return this._inputSensorLocationDescription.value;
	}

	set SensorLocationDescription(val)
	{
		this._inputSensorLocationDescription.value = val;
	}

	get SensorEmploymentSelection()
	{
		let elements = this._SensorEmploymentRadioElements;

		for(let i = 0; i < elements.length; i++)
		{
			if( elements[i].checked )
				return elements[i].value;
		}
	}

	set SensorEmploymentSelection(val)
	{
		let elements = this._SensorEmploymentRadioElements;
		

		for(let i = 0; i < elements.length; i++)
		{
			if( elements[i].value == val )
			{
				elements[i].checked = true;
				return;
			}
		}
	}

	set onRadioSelectionChange(func)
	{
		this._RadioSensorEmploymentGrid.onchange = func;
	}

	set onSensorTypeChange(func)
	{
		this._inputSensorType.onchange = func;
	}

	set onAddToArray(func)
	{
		this._addToArrayButton.onclick = func;
	}
}

customElements.define('jhrm-meta-sensor-form', MetaDataSensorDetailsForm);