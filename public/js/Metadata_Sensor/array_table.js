import { Request } from "../request.js"
import { Sortable } from "../sortable.js";
import { Datetime } from "../shared/datetime.js";


export class MetaDataSensorDetailsRow extends HTMLTableRowElement
{
    constructor(_ShowSelection)
    {
    super();
    
    this._Built = false;
    this._ShowSelection = _ShowSelection;

    this._ElmSelected = document.createElement('input');
    this._ElmSelected.type = 'checkbox';
    
    this._ElmSensorSN = document.createElement('td');
    this._ElmSensorSWVersion = document.createElement('td');
    this._ElmSensorHWVersion = document.createElement('td');
    this._ElmSensorMFRDate = document.createElement('td');
    this._ElmSensorMGRSLocation = document.createElement('td');
    this._ElmSensorLocationDescription = document.createElement('td');
   // this._ElmSensorEmployment = document.createElement('td');

    this._MFRDate = new Date();
        
    }

    connectedCallback()
    {
		if ( !this._Built )
		{
			let TdSelected = document.createElement('td');
			TdSelected.append(this._ElmSelected);

			if( this._ShowSelection )
			{
				this.append(
					TdSelected
				);
			}

			this.append(
				this._ElmSensorSN, this._ElmSensorSWVersion, this._ElmSensorHWVersion,
				this._ElmSensorMFRDate, this._ElmSensorMGRSLocation, this._ElmSensorLocationDescription //, this._ElmSensorEmployment
			);

			this._Built = true;
        }
    }

//all per line of each value in an specified array
//setting if selected
set Selected(val)
{
    this._ElmSelected.checked = val;
}

//getting if selected
get Selected()
{
    return this._ElmSelected.checked;
}
// set sensor serial number
set SensorSN(val)
{
	this._ElmSelected.value = val;
    this._ElmSensorSN.innerText = val;
}
//get sensor serial number
get SensorSN()
{
    return this._ElmSensorSN.textContent;
}
//set sensor software version
set SensorSWVersion(val)
{
    this._ElmSensorSWVersion.innerText = val;
}
//get sensor software version
get SensorSWVersion()
{
    return this._ElmSensorSWVersion.textContent;
}
//set sensor hardware version
set SensorHWVersion(val)
{
    this._ElmSensorHWVersion.innerText = val;
}
//get sensor hardware version
get SensorHWVersion()
{
    return this._ElmSensorHWVersion.textContent;
}
//set the sensor manufacturer date
set SensorMFRDate(val)
{
    if (val == null)
	{
		this._ElmSensorMFRDate.innerText = '';
	}
	else
	{
    	if ( !(val instanceof Datetime) )
        	throw new Error("Manufacture Date needs to be a Date.");
			
		this._MFRDate = val;

       	this._ElmSensorMFRDate.innerText = val;
	}
}
//get sensor manufacturer date
get SensorMFRDate()
{
    return this._ElmSensorMFRDate;
}
//set sensor specific mgrs location
set SensorMGRSLocation(val)
{
    this._ElmSensorMGRSLocation.innerText = val;
}
//get sensor specific mgrs location
get SensorMGRSLocation()
{
    return this._ElmSensorMGRSLocation.textContent;
}
// set sensor location description
set SensorLocationDescription(val)
{
    this._ElmSensorLocationDescription.innerText = val;
}
//get sensor location description
get SensorLocationDescription()
{
    return this._ElmSensorLocationDescription.textContent;
}
//set the sensor employment value: this is an enum and only right now for multirae
set SensorEmployment(val)
{
    this._ElmSensorEmployment = val;
}
//get sensor employment value
get SensorEmployment()
{
    return this._ElmSensorEmployment.something;
}

static fromObjData(record, _ShowSelection)
{
	let ret = new MetaDataSensorDetailsRow(_ShowSelection);
	ret.Selected = false;
	ret.SensorSN = record.serial_number;
	ret.SensorSWVersion = record.software_version;
	ret.SensorHWVersion = record.hardware_version;
	ret.SensorMFRDate = record.mfr_date;
	ret.SensorMGRSLocation = record.sensor_mgrs_location;
	ret.SensorLocationDescription = record.sensor_location_description;
	return ret;
}

}

customElements.define('jhrm-meta-sensor-row', MetaDataSensorDetailsRow, { extends: 'tr' });

export class SensorsInArrayTable extends HTMLElement
{
	constructor()
	{
        super();
        
        
        this._Built = false;
       
		this._ElmHeaderSelected = document.createElement('input');
		this._ElmHeaderSelected.type = 'checkbox';

		this._ShowSelection = this.getAttribute('show-selection') == "true";

		let createTH = (text) =>
		{
			let elm = document.createElement('th');
			elm.innerHTML = text;

			return elm;
		}

		let createCheckBoxTH = (text) =>
		{
			
			let TdHeaderSelected = document.createElement('th');			
			TdHeaderSelected.append(text, this._ElmHeaderSelected);

			return TdHeaderSelected;
		}

		this._ElmHeader = document.createElement('thead');
		this._ElmBody = document.createElement('tbody');

		if(this._ShowSelection)
		{
			this._ElmHeader.append(createCheckBoxTH('Sel. '));
		}

		this._ElmHeader.append(
			createTH('Sensor SN'),
			createTH('SW Version'),
			createTH('HW Version'),
			createTH('Manufacturing Date'),
			createTH('Sensor Location (MGRS)'),
			createTH('Sensor Location Description')
        );


        this._ElmTable = document.createElement('table');
		this._ElmTable.append(this._ElmHeader, this._ElmBody);
		this._ElmTableWrapper = document.createElement('div');
		this._ElmTableWrapper.append(this._ElmTable);
		Sortable(this._ElmTable);
    }
    
    
	connectedCallback()
	{
		if (!this._Built)
		{
			this._ElmTableWrapper.className="tablewrapper";
			this.append(this._ElmTableWrapper);
			this._Built = true;

        }

        this._ElmHeaderSelected.onclick = function()
            {
                this.filterSelectCase(this._ElmHeaderSelected.checked);
            }.bind(this);
	
	}

	appendSampleRow(row)
	{
		if (row instanceof MetaDataSensorDetailsRow)
			this._ElmBody.append(row);
	}

	async deleteSampleRow(selectedID,SensorSN)
	{
		await Request.post('/api/metadata/sensorarray/' + selectedID + '/' + SensorSN + '/deletesensor');
	}

	async populateFromArray(arrayID)
	{
		// make sure empty first
		this._ElmBody.innerHTML = "";

		let results = await Request.get('/api/metadata/sensorarray/' +arrayID + '/sensordetails');
		results.forEach(record => {
			let nextRow = MetaDataSensorDetailsRow.fromObjData(record, this._ShowSelection);
			this.appendSampleRow(nextRow);
		});

		if (results == 0)
		{
			this._ElmHeaderSelected.checked = false;
		}
	}
	
	async deleteFromArray(selectedID)
	{
		let val = selectedID;

		let tBody = this._ElmBody;
		let ret = [];

		for (let i = 0; i < tBody.children.length; ++i)
		{
			let element = tBody.children.item(i);

			if (element.Selected)
				this.deleteSampleRow(selectedID, element.SensorSN)	
		}
		
		this._ElmHeaderSelected.checked = false;
	}	

	filterSelectCase(selectAll)
	{
		let tBody = this._ElmBody;

		if(selectAll === true)
		{
			for (let i = 0; i < tBody.children.length; ++i)
			{	
				tBody.children.item(i).Selected = true;	
			}
		}
		else
		{
			for (let i = 0; i < tBody.children.length; ++i)
			{	
				tBody.children.item(i).Selected = false;	
			}
		}
	}

	get SelectedIDs()
	{
		let tBody = this._ElmBody;
		let ret = [];

		for (let i = 0; i < tBody.children.length; ++i)
		{
			let element = tBody.children.item(i);

			if (element.Selected)
				ret.push(element.SampleID);
		}

		return ret;
	}

	numberofRows()
	{
		let tBody = this._ElmBody;
		let selected = 0;

		for (let i = 0; i < tBody.children.length; ++i)
		{
			let element = tBody.children.item(i);

			if (element.Selected)
				selected++;

		}

		return selected;
	}

	async getSampleRow(selectedID,SensorSN)
	{
		let val = await Request.get('/api/metadata/sensorarray/' + selectedID + '/' + SensorSN + '/getrowsensor');

		return val;
	}

	async rowDataToEdit(selectedID)
	{
		
		let tBody = this._ElmBody;

		for (let i = 0; i < tBody.children.length; ++i)
		{
			let element = tBody.children.item(i);

			if (element.Selected)
				return await Request.get('/api/metadata/sensorarray/' + selectedID + '/' + element.SensorSN + '/getrowsensor');
		}

	}
}

customElements.define('jhrm-meta-sensors-in-array-table', SensorsInArrayTable);