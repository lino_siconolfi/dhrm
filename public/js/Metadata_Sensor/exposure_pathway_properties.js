import { JhrmHelpIcon } from "../help_icon.js"
import { Request } from "../request.js"

export class JhrmExposurePathwayProperties extends HTMLElement
{
	constructor()
	{
		super();

		let in_style = `style="border: 0; width: 100%; background-color: rgba(0,0,0,0);"`;
		let label_style = `style="text-align: left; padding-right: 3em; white-space: nowrap;"`;

		this.innerHTML = `
			<table style="width: 100%;">
				<caption style="text-align: left; margin: 0.2em;">Please input the following information in the table below:</caption>
				<thead>
					<tr>
						<th style="width: 30%; min-width: auto;">Exposure Pathway Attribute</th>
						<th>Selected Chosen Attribute Value</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td ${label_style}>
							Standardized Exposure Pathway Name
							<jhrm-help-icon>
								Enter the exposure pathway name such as Air (General Air Quality)
								or Air (On-Site: Burn Pit).
							</jhrm-help-icon>
						</td>
						<td><input id="StandardizedExposurePathwayName" ${in_style}></input></td>
					</tr>
					<tr>
						<td ${label_style}>
							Threat Source
							<jhrm-help-icon>
								Identify the threat source (e.g., burn pit, fuel farm).
							</jhrm-help-icon>
						</td>
						<td><input id="ThreatSource" ${in_style}></input></td>
					</tr>
					<tr>
						<td ${label_style}>
							Health Hazard(s)
							<jhrm-help-icon>
								Identify the health hazard(s) (e.g., Inhalation of VOCs or Particulate Matter).
							</jhrm-help-icon>
						</td>
						<td><input id="HealthHazards" ${in_style}></input></td>
					</tr>
					<tr>
						<td ${label_style}>
							Exposure Point Area
							<jhrm-help-icon>The area where the PAR exposure occurs.</jhrm-help-icon>
						</td>
						<td><input id="ExposurePointArea" ${in_style}></input></td>
					</tr>
					<tr>
						<td ${label_style}>
							Exposure Medium
							<jhrm-help-icon>
								Identify the exposure medium (Air, Water, Soil or Other).
							</jhrm-help-icon>
						</td>
						<td><input id="ExposureMedium" ${in_style}></input></td>
					</tr>
					<tr>
						<td ${label_style}>
							Exposure Route
							<jhrm-help-icon>
								Identify the exposure route (Inhalation, ingestion, etc).
							</jhrm-help-icon>
						</td>
						<td><input id="ExposureRoute" ${in_style}></input></td>
					</tr>
					<tr>
						<td ${label_style}>
							Exposure Duration of Concern
							<jhrm-help-icon>
								Enter the exposure duration of concern and any other
								relevent temporal factors.
							</jhrm-help-icon>
						</td>
						<td><input id="ExposureDurationOfConcern" ${in_style}></input></td>
					</tr>
					<tr>
						<td ${label_style}>
							Exposed Population
							<jhrm-help-icon>
								Describe the exposed population and/or number of personnel affected (All range personnel, 350 people).
							</jhrm-help-icon>
						</td>
						<td><input id="ExposedPopulation" ${in_style}></input></td>
					</tr>
				</tbody>
			</table>
			<button id="SaveButton" style="display:none;margin-top: 0.5em;">Save Pathway</button>
		`;

		this._StandardizedExposurePathwayName = this.querySelector('#StandardizedExposurePathwayName');
		this._ThreatSource = this.querySelector('#ThreatSource');
		this._HealthHazards = this.querySelector('#HealthHazards');
		this._ExposurePointArea = this.querySelector('#ExposurePointArea');
		this._ExposureMedium = this.querySelector('#ExposureMedium');
		this._ExposureRoute = this.querySelector('#ExposureRoute');
		this._ExposureDurationOfConcern = this.querySelector('#ExposureDurationOfConcern');
		this._ExposedPopulation = this.querySelector('#ExposedPopulation');
		this._SaveButton = this.querySelector('#SaveButton');

		this._NeedsSave = false;
		this._visible = true;

		this._built = false;
	}

	async connectedCallback()
	{
		if (!this._built)
		{

			this.style.display = "inline-block";

			let inputs = [
				this._StandardizedExposurePathwayName,
				this._ThreatSource, this._HealthHazards, this._ExposurePointArea,
				this._ExposureMedium, this._ExposureRoute,
				this._ExposureDurationOfConcern, this._ExposedPopulation
			];

			let setUnsaved = function()
			{
				this._NeedsSave = true;
				this._SaveButtonEnabled = true;
			}.bind(this);

			inputs.forEach(
				function(elm)
				{
					elm.addEventListener("change", setUnsaved);
				}
			);

			this._SaveButtonEnabled = false;
			this._SaveButton.onclick = this.save.bind(this);

			this._built = true;

			this.Visible = this._visible;
		}
	}

	set Visible(val)
	{
		this._visible = val;

		if (this._built)
			this.style.display = this._visible ? "inline-block" : "none";
	}

	get Visible()
	{
		return this._visible;
	}

	get StandardizedExposurePathwayName()
	{
		return this._StandardizedExposurePathwayName.value;
	}

	set StandardizedExposurePathwayName(val)
	{
		
		this._StandardizedExposurePathwayName.value = val;
	}

	get ThreatSource()
	{
		return this._ThreatSource.value;
	}

	set ThreatSource(val)
	{
		this._ThreatSource.value = val;
	}

	get HealthHazards()
	{
		return this._HealthHazards.value;
	}

	set HealthHazards(val)
	{
		this._HealthHazards.value = val;
	}

	get ExposurePointArea()
	{
		return this._ExposurePointArea.value;
	}

	set ExposurePointArea(val)
	{
		this._ExposurePointArea.value = val;
	}

	get ExposureMedium()
	{
		return this._ExposureMedium.value;
	}

	set ExposureMedium(val)
	{
		this._ExposureMedium.value = val;
	}

	get ExposureRoute()
	{
		return this._ExposureRoute.value;
	}

	set ExposureRoute(val)
	{
		this._ExposureRoute.value = val;
	}

	get ExposureDurationOfConcern()
	{
		return this._ExposureDurationOfConcern.value;
	}

	set ExposureDurationOfConcern(val)
	{
		this._ExposureDurationOfConcern.value = val;
	}

	get ExposedPopulation()
	{
		return this._ExposedPopulation.value;
	}

	set ExposedPopulation(val)
	{
		this._ExposedPopulation.value = val;
	}

	get Saved()
	{
		return !this._NeedsSave;
	}

	set _SaveButtonEnabled(val)
	{
		if (val)
		{
			this._SaveButton.style.backgroundColor = null;
			this._SaveButton.style.borderColor = null;
			this._SaveButton.style.cursor = null;
			this._SaveButton.disabled = false;
		}
		else
		{
			this._SaveButton.style.backgroundColor = "gray";
			this._SaveButton.style.borderColor = "gray";
			this._SaveButton.style.cursor = "default";
			this._SaveButton.disabled = true;
		}
	}

	async save()
	{

		this._NeedsSave = false;
		this._SaveButtonEnabled = false;
	}
}

customElements.define('jhrm-exposure-pathway-properties', JhrmExposurePathwayProperties);