import { JhrmTooltip } from "./tooltip.js"
import { Positioning } from "./positioning.js"
import { MaterialIcon } from "./material_icon.js"
import { JhrmAlign } from "./align.js"
import { selectAndStripId } from "./utility.js"
import { JhrmSelect } from "./select.js"
import { JhrmSwapSpace } from "./swap_space.js"
import { SEVERITY, EXPOSURE_DURATION } from "/js/shared/Constants.js"
import { JhrmMegTableSelect } from "./meg_table_select.js"
import { JhrmHraDurationSelector } from "./duration_selector.js"
import { MEG } from "/js/shared/MEG.js"
import { DimensionedValue } from "/js/shared/DimensionedValue.js"
import { normalizeUnits } from "/js/shared/normalizeUnits.js"
import * as calculatePepc from "/js/shared/calculatePepc.js"
import { ChemicalSample } from "./chemical_sample.js"
import { Request } from "./request.js"


export class RiskAssessment
{
	/**
	 * Styles the text color and background color of an element
	 * according to the risk level.
	 *
	 * @param { HTMLElement} elm
	 * @param { RiskAssessment.RiskLevel } lvl
	 */
	static styleForRisk(elm, lvl)
	{
		let levels = RiskAssessment.RiskLevel;

		if (lvl == levels.ExtremelyHigh)
		{
			elm.style.backgroundColor = "black";
			elm.style.color = "white";
		}
		else if (lvl == levels.High)
		{
			elm.style.backgroundColor = "red";
			elm.style.color = "white";
		}
		else if (lvl == levels.Moderate)
		{
			elm.style.backgroundColor = "yellow";
			elm.style.color = "black";
		}
		else if (lvl == levels.Low)
		{
			elm.style.backgroundColor = "green";
			elm.style.color = "black";
		}
		else if (lvl == levels.None)
		{
			elm.style.backgroundColor = "white";
			elm.style.color = "black";
		}
	};

	/**
	 * Gets a probability enumeration from a numeric score.
	 *
	 * @param { Number } score
	 *  The numeric score to convert.
	 */
	static toProbability(score)
	{
		let PropOption = RiskAssessment.Probability;

		if (score < 4)
			return PropOption.None;
		else if (score >= 4 && score <= 6)
			return PropOption.Unlikely;
		else if (score == 7)
			return PropOption.Seldom;
		else if (score == 8)
			return PropOption.Occasionally;
		else if (score == 9)
			return PropOption.Likely;
		else
			return PropOption.Frequent;
	}
}

RiskAssessment.Representativeness =
{
	Overestimated : {
		score : 1,
		value : "Overestimated"
	},
	Adequate : {
		score : 2,
		value : "Adequate"
	},
	Underestimated : {
		score : 3,
		value : "Underestimated"
	},
};

RiskAssessment.ExposureRate =
{
	Light : {
		score : 1,
		value : "Light"
	},
	Typical :
	{
		score : 2,
		value : "Typical"
	},
	Heavy : {
		score : 3,
		value : "Heavy"
	},
};

RiskAssessment.RiskLevel =
{
	ExtremelyHigh : "Extremely High",
	High : "High",
	Moderate : "Moderate",
	Low : "Low",
	None : "N/A"
};

let RiskLevelValues = {};
RiskLevelValues[RiskAssessment.RiskLevel.ExtremelyHigh] = 4;
RiskLevelValues[RiskAssessment.RiskLevel.High] = 3;
RiskLevelValues[RiskAssessment.RiskLevel.Moderate] = 2;
RiskLevelValues[RiskAssessment.RiskLevel.Low] = 1;
RiskLevelValues[RiskAssessment.RiskLevel.None] = 0;

Object.defineProperties(
	RiskAssessment.RiskLevel,
	{
		compare :
		{
			value : function(left, right)
			{
				let iLeft = RiskLevelValues[left];
				let iRight = RiskLevelValues[right];

				return (iLeft < iRight) ?
					1 : (iLeft > iRight) ?
					-1 : 0;
			}
		}
	}
);

RiskAssessment.Probability =
{
	None : "None",
	Unlikely : "Unlikely",
	Seldom : "Seldom",
	Occasionally : "Occasionally",
	Likely : "Likely",
	Frequent : "Frequent"
};

let R = RiskAssessment.RiskLevel;
let S = SEVERITY;
let P = RiskAssessment.Probability;
let Table = {};

Table[S.Catastrophic] = {};
Table[S.Catastrophic][P.None] = R.Low;
Table[S.Catastrophic][P.Unlikely] = R.Moderate;
Table[S.Catastrophic][P.Seldom] = R.High;
Table[S.Catastrophic][P.Occasionally] = R.High;
Table[S.Catastrophic][P.Likely] = R.ExtremelyHigh;
Table[S.Catastrophic][P.Frequent] = R.ExtremelyHigh;

Table[S.Critical] = {};
Table[S.Critical][P.None] = R.Low;
Table[S.Critical][P.Unlikely] = R.Low;
Table[S.Critical][P.Seldom] = R.Moderate;
Table[S.Critical][P.Occasionally] = R.High;
Table[S.Critical][P.Likely] = R.High;
Table[S.Critical][P.Frequent] = R.ExtremelyHigh;

Table[S.Marginal] = {};
Table[S.Marginal][P.None] = R.Low;
Table[S.Marginal][P.Unlikely] = R.Low;
Table[S.Marginal][P.Seldom] = R.Low;
Table[S.Marginal][P.Occasionally] = R.Moderate;
Table[S.Marginal][P.Likely] = R.Moderate;
Table[S.Marginal][P.Frequent] = R.High;

Table[S.Negligible] = {};
Table[S.Negligible][P.None] = R.Low;
Table[S.Negligible][P.Unlikely] = R.Low;
Table[S.Negligible][P.Seldom] = R.Low;
Table[S.Negligible][P.Occasionally] = R.Low;
Table[S.Negligible][P.Likely] = R.Low;
Table[S.Negligible][P.Frequent] = R.Moderate;

RiskAssessment.RiskLevelTable = Table;

let ChemGroup = {
	None : "None",
	AcutePeak : "Acute Peak",
	AcuteAverage : "Acute Average",
	ChronicAverage : "Chronic Average"
}

////////////////////////////////////////////////

var RowsTemplate = "repeat(4, 2rem) repeat(4, 1.6rem) repeat(2, 2rem)";

var center = Positioning.center;
var vCenter = Positioning.vCenter;

var boxForGrid = function(elm, row, col)
{
	let ret = document.createElement('div');
	ret.style.display = "inline-block";

	ret.style.boxSizing = "border-box";
	ret.style.padding = "0.2rem";
	ret.style.margin = "0";
	ret.style.width = "100%";
	ret.style.height = "100%";

	if (undefined != row)
		ret.style.gridRow = row.toString();


	if (undefined != col)
		ret.style.gridColumn = col.toString();

	ret.append(center(elm));

	return ret;
}

let styleForRisk = RiskAssessment.styleForRisk;



let _checkAndReturn = function(v)
{
	if (undefined == v)
		throw new Error("Data not set or not enough data set to calculate value.");

	return v;
};

//////////////////////////////////////////

/**
 * The SampleCalculator allows client code to set data such as semaple and meg sets,
 * and embeds methods for processing that data to get values typical for risk
 * assessment through its get methods.  If data required for a requested calculation has
 * not been set in the object, an exception will be thrown when the cacluation is
 * attempted.
 */
export class SampleCalculator
{
	constructor()
	{
		// All properties used in functions should default to undefined.
	}

	/**
	 * Sets the megs that are used in Degree of Exposure calculations.
	 *
	 * @param { MEG[] }
	 */
	set ChemicalMegs(megs)
	{
		for(let i = 0; i < length; ++i)
		{
			if ( !(megs[i] instanceof MEG) )
				throw new Error("Each element in passed array must be a MEG.");
		}

		this._AllMegsForChemical = megs;
	}

	get ChemicalMegs()
	{
		return _checkAndReturn(this._AllMegsForChemical);
	}

	/**
	 * Sets the megs that are used in Degree of Exposure calculations.
	 *
	 * @param { ChemicalSample[] }
	 */
	set ChemicalSamples(samples)
	{
		for(let i = 0; i < length; ++i)
		{
			if ( !(samples[i] instanceof ChemicalSample) )
				throw new Error("Each element in passed array must be a ChemicalSample.");
		}

		this._AllSamplesForChemical = samples;
	}

	/**
	 * Sets the megs that are used in Degree of Exposure calculations.
	 *
	 * @param { ChemicalSample[] }
	 */
	get ChemicalSamples()
	{
		return _checkAndReturn(this._AllSamplesForChemical);
	}

	/**
	 * @returns { EXPOSURE_DURATION }
	 */
	get SampleTime()
	{
		return this._SampleTime || this.DefaultSampleTime;
	}

	/**
	 * @param { EXPOSURE_DURATION } st
	 */
	set SampleTime( st )
	{
		this._SampleTime = st;
	}

	get DefaultSampleTime()
	{
		return this.Group==ChemGroup.ChronicAverage ? EXPOSURE_DURATION.YEAR_1 : this.ChemicalSamples[0].sample.sample_time;
	}

	/**
	 * @returns { Meg }
	 */
	get Meg()
	{
		return this._Meg || this.DefaultMeg;
	}

	/**
	 * @returns { Meg }
	 */
	set Meg( meg )
	{
		this._Meg = meg;
	}

	get DefaultMeg()
	{
		return calculatePepc.getComparisonMegForSeverity(
			this.PEPC,
			this.ChemicalMegs,
			this.SampleTime,
			this.Group==ChemGroup.ChronicAverage
		);
	}

	/**
	 * @param { string }
	 */
	set Chemical(ch)
	{
		this._Chemical = ch;
	}

	/**
	 * @returns { string }
	 */
	get Chemical()
	{
		return _checkAndReturn(this._Chemical);
	}

	/**
	 * @param { string }
	 */
	set CasNumber(str)
	{
		this._CasNumber = str;
	}

	/**
	 * @returns { string }
	 */
	get CasNumber()
	{
		return _checkAndReturn(this._CasNumber);
	}

	/**
	 * @param { ChemGroup }
	 */
	set Group(grp)
	{
		this._Group = grp;
	}

	/**
	 * @returns { ChemGroup }
	 */
	get Group()
	{
		return _checkAndReturn(this._Group);
	}

	/**
	 * @returns { DimensionedValue }
	 */
	get PEPC()
	{
		let group = this.Group;

		if ( group == ChemGroup.AcutePeak )
		{
			return calculatePepc.getPeakConcentration(this.ChemicalSamples, this.ChemicalMegs[0]);
		}
		else if ( group == ChemGroup.AcuteAverage || group == ChemGroup.ChronicAverage)
		{
			return calculatePepc.getAvgConcentration(this.ChemicalSamples, this.ChemicalMegs[0]);
		}
		else
		{
			throw new Error("Invalid or no Group set.");
		}
	}

	get DegreeOfExposure()
	{
		let compare_megs = calculatePepc.getBoundingMegsFromComparisonMeg(this.Meg, this.ChemicalMegs, this.SampleTime);
		return calculatePepc.calculateDegreeOfExposure(this.PEPC, compare_megs);
	}

	/**
	 * @param { RiskAssessment.Representativeness }
	 */
	set DataRepresentativeness(dr)
	{
		this._DataRepresentativeness = dr;
	}

	/**
	 * @returns { RiskAssessment.Representativeness }
	 */
	get DataRepresentativeness()
	{
		return _checkAndReturn(this._DataRepresentativeness);
	}

	get DurationOfExposure()
	{
		return calculatePepc.calculateDurationOfExposure(this.SampleTime, [this.Meg]);
	}

	/**
	 * @param { RiskAssessment.ExposureRate }
	 */
	set RateOfExposure(roe)
	{
		this._RateOfExposure = roe;
	}

	/**
	 * @returns { RiskAssessment.ExposureRate }
	 */
	get RateOfExposure()
	{
		return _checkAndReturn(this._RateOfExposure);
	}

	get RiskLevel()
	{
		return RiskAssessment.RiskLevelTable[this.Severity][this.Probability]
	}

	get Severity()
	{
		return this.Meg.severity;
	}

	get Probability()
	{
		return RiskAssessment.toProbability(this.Score);
	}

	get Score()
	{
		return this.DegreeOfExposure.score + this.DataRepresentativeness.score +
		       this.DurationOfExposure.score + this.RateOfExposure.score;
	}

	get PeakConcentration()
	{
		return calculatePepc.getPeakConcentration(this.ChemicalSamples, this.ChemicalMegs[0]);
	}

	get AverageConcentration()
	{
		return calculatePepc.getAvgConcentration(this.ChemicalSamples, this.ChemicalMegs[0]);
	}

	get IsAcute()
	{
		return calculatePepc.acutePrescreen(this.PeakConcentration, this.ChemicalMegs).exceeded;
	}

	get IsChronic()
	{
		return calculatePepc.chronicPrescreen(this.AverageConcentration, this.ChemicalMegs).exceeded;
	}

	/**
	 * @param { ChemicalSample[] } samples
	 * @param { MEG[] } megs
	 * @param {Object[]} assessment[] Array of assessments as returned by GET /api/enhancedhra/hra/[ID]/risk
	 * @param {Number} assessment.id
	 * @param {String} assessment.chem_rapid_hra_id
	 * @param {String} assessment.type
	 * @param {String} assessment.chemical_name
	 * @param {String} assessment.cas_number
	 * @param {Number} assessment.meg
	 * @param {String} assessment.meg_name
	 * @param {String} assessment.meg_units
	 * @param {String} assessment.meg_version
	 * @param {String} assessment.severity
	 * @param {String} assessment.prob_degree_of_exp
	 * @param {Number} assessment.prob_degree_of_exp_score
	 * @param {String} assessment.prob_rep_of_data
	 * @param {Number} assessment.prob_rep_of_data_score
	 * @param {String} assessment.prob_duration_of_exp
	 * @param {Number} assessment.prob_duration_of_exp_score
	 * @param {String} assessment.prob_rate_of_exp
	 * @param {Number} assessment.prob_rate_of_exp_score
	 * @param {String} assessment.probability
	 * @param {Number} assessment.probability_score
	 * @param {String} assessment.risk_level
	 * @param {String} assessment.confidence
	 * @param {Number} assessment.custom_exposure_duration
	 * @param {Number} assessment.custom_meg
	 *
	 * @returns An object indexed with properties of the cas number, then the groups applicable
	 *          to the chemical of the cas.  The returned object will have an AsArray method to
	 *          get the calculators as a one-dimentional array, and a filterLowerRisks function
	 *          to create a copy with the lower of acute peak/average risk removed.
	 */
	static makeCalculators(samples, megs, assessments)
	{
		let ret = {};
		assessments = assessments || [];

		let groups = ChemGroup;

		let grouped_megs = {};
		for (let i = 0; i < megs.length; ++i)
		{
			let current_meg = megs[i];

			if ( !grouped_megs[current_meg.cas_number])
				grouped_megs[current_meg.cas_number] = [];

			grouped_megs[current_meg.cas_number].push(current_meg);
		}

		let grouped_samples = {};
		for (let i = 0; i < samples.length; ++i)
		{
			let sample = samples[i];
			let cas_number = sample.cas_number;

			if ( !grouped_samples[cas_number])
				grouped_samples[cas_number] = [];

			grouped_samples[cas_number].push(sample);
		}

		for (const cas_number in grouped_samples)
		{
			ret[cas_number] = {};

			let calculator = new SampleCalculator();
			calculator.ChemicalSamples = grouped_samples[cas_number];
			calculator.ChemicalMegs = grouped_megs[cas_number];

			let is_acute = calculator.IsAcute;
			let is_chronic = calculator.IsChronic;

			let make_group_calc = (grp, assessment) =>
			{
				let ret = new SampleCalculator();
				ret.ChemicalSamples = calculator.ChemicalSamples;
				ret.ChemicalMegs = calculator.ChemicalMegs;
				ret.Group = grp;
				ret.CasNumber = cas_number;
				ret.Chemical = ret.ChemicalSamples[0].chemical_name;
				if(assessment)
				{
					ret.SampleTime = assessment.custom_exposure_duration;
					ret.DataRepresentativeness =
						Object.values(RiskAssessment.Representativeness)
							.find(item=>item.score == assessment.prob_rep_of_data_score) ||
						RiskAssessment.Representativeness.Adequate;
					ret.RateOfExposure =
						Object.values(RiskAssessment.ExposureRate)
							.find(item=>item.score == assessment.prob_rate_of_exp_score) ||
						RiskAssessment.ExposureRate.Typical;
					let meg = ret.ChemicalMegs.find(item=>item.parm == assessment.meg_name);
					if(meg)
					{
						ret.Meg = meg;
					}
				}


				return ret;
			};

			if (is_acute && is_chronic)
			{
				ret[cas_number][groups.AcutePeak] = make_group_calc(groups.AcutePeak, assessments.find(item=>item.cas_number==cas_number&&item.type=='acute_peak'));
				ret[cas_number][groups.AcuteAverage] = make_group_calc(groups.AcuteAverage, assessments.find(item=>item.cas_number==cas_number&&item.type=='acute_average'));
				ret[cas_number][groups.ChronicAverage] = make_group_calc(groups.ChronicAverage, assessments.find(item=>item.cas_number==cas_number&&item.type=='chronic_average'));
			}
			else if (is_acute)
			{
				ret[cas_number][groups.AcutePeak] = make_group_calc(groups.AcutePeak, assessments.find(item=>item.cas_number==cas_number&&item.type=='acute_peak'));
				ret[cas_number][groups.AcuteAverage] = make_group_calc(groups.AcuteAverage, assessments.find(item=>item.cas_number==cas_number&&item.type=='acute_average'));
			}
			else if (is_chronic)
			{
				ret[cas_number][groups.ChronicAverage] = make_group_calc(groups.ChronicAverage, assessments.find(item=>item.cas_number==cas_number&&item.type=='chronic_average'));
			}
			else
			{
				ret[cas_number][groups.None] = make_group_calc(groups.None);
			}
		}

		let cas_keys = Object.keys(ret);
		cas_keys.forEach(
			(key) =>
			{
				if ( Object.keys(ret[key]).length == 0 )
					delete ret[key];
			}
		);

		SampleCalculator._attachMemberFunctions(ret);

		return ret;
	}

	static _attachMemberFunctions(obj)
	{
		Object.defineProperties(
			obj,
			{
				asArray :
				{
					value : function()
					{
						let all_calcs = [];

						let cas_vals = Object.values(this);
						cas_vals.forEach(
							(current) =>
							{
								all_calcs.push(
									...Object.values(current)
								);
							}
						);

						return all_calcs;
					}.bind(obj)
				},
				filterLowerRisks :
				{
					value : function()
					{
						let ret = {};

						let cas_keys = Object.keys(this);
						let compare = RiskAssessment.RiskLevel.compare;

						cas_keys.forEach(
							(current) =>
							{
								ret[current] = Object.assign({}, this[current]);
								let item = ret[current];

								if ( item[ChemGroup.AcutePeak] && item[ChemGroup.AcuteAverage] )
								{
									if ( compare(item[ChemGroup.AcuteAverage].RiskLevel, item[ChemGroup.AcutePeak].RiskLevel) > 0 )
									{
										delete item[ChemGroup.AcuteAverage];
									}
									else
									{
										delete item[ChemGroup.AcutePeak];
									}
								}
							}
						);

						SampleCalculator._attachMemberFunctions(ret);
						return ret;
					}.bind(obj)
				}
			}
		);
	}
}

export class HraRiskAssessmentHeadings extends HTMLElement
{
	constructor()
	{
		super();

		this._built = false;
	}

	connectedCallback()
	{
		if (!this._built)
		{
			let makeElement = function(tag, contents, row, tip_contents)
			{
				let ret = document.createElement(tag);
				ret.style.display = "inline-block";
				ret.style.boxSizing = "border-box";
				ret.style.gridRow = row.toString();
				ret.style.border = "0";
				ret.innerText = contents;

				if (tip_contents != undefined)
				{
					let HelpIcon = MaterialIcon.Create("help_outline");
					HelpIcon.style.fontSize = "0.8rem";

					let Tooltip = new JhrmTooltip();
					Tooltip.style.fontSize = "0.8rem";
					Tooltip.style.whiteSpace = "normal";

					if (tip_contents instanceof HTMLElement)
						Tooltip.append(tip_contents);
					else
						Tooltip.innerHTML = tip_contents;

					Tooltip.Target = HelpIcon;
					ret.append( HelpIcon, Tooltip );
				}

				return vCenter(ret);
			};

			this.style.display = "grid";
			this.style.gridTemplateRows = RowsTemplate;
			this.style.whiteSpace = "nowrap";
			this.style.marginBottom = "auto";
			this.style.alignItems = "center";
			this.style.paddingRight = "1.5rem";

			this.append(
				makeElement('div', "  ", 1),
				makeElement('h1', "Chemical of Concern: ", 2),
				makeElement(
					'h1', "Severity: ", 3,
					"Severity category for the population exposure (Ref. TG230 Exhibit 3-1 & 3-4)."
				),
				makeElement(
					'h1', "Rank Hazard Probability: ", 4,
					"Hazard probability is ranked by jointly considering four hazard " +
					"probability factors: degree of exposure, representativeness of the field data, duration of " +
					"exposure, and rate of exposure (Ref. TG230, Exhibit 3-3)."
				),
				makeElement(
					'h2', "Degree of Exposure: ", 5,
					"Measure of how much greater the population exposure estimate is " +
					"compared to the MEG. The higher an exposure is above the MEG, the higher probability that " +
					"the severity level health outcome will occur (Ref. TG230, Exhibit 3-3)."
				),
				makeElement(
					'h2', "* Representativeness of Data: ", 6,
					"Measure of the how representative the field-collected data " +
					"is of the true exposure concentration (Ref. TG230, Exhibit 3-3). Select whether the field data " +
					"adequately, underestimates, or overestimates population exposure (see guidance below)."
				),
				makeElement(
					'h2', "Duration of Exposure: ", 7,
					"A ratio of the exposure duration is relative to the exposure " +
					"duration used to develop the MEG (Ref. TG230, Exhibit 3-3)"
				),
				makeElement(
					'h2', "* Rate of Exposure: ", 8,
					"Measure of how different the population's rate of exposure (e.g., inhalation " +
					"rate, water consumption rate, soil contact rate) is relative to the assumed rate used to develop " +
					"the MEG (Ref. TG230, Exhibit 3-3). Select the appropriate rate of exposure (see guidance " +
					"below)."
				),
				makeElement('h1', "Probability: ", 9),
				makeElement(
					'h1', "Risk Level: ", 10,
					"Select the confidence level for the overall risk assessment (Ref. " +
					"TG230, Table 3-6) (see guidance below)."
				)
			);

			this._built = true;
		}
	}
}

customElements.define('jhrm-hra-risk-assessment-headings', HraRiskAssessmentHeadings);

export class HraRiskAssessment extends HTMLElement
{
	constructor()
	{
		super();

		this._built = false;

		this.ElmHeading = document.createElement('h1');
		this.ElmHeading.style.padding = "0.5em";
		this.ElmHeading.style.margin = "0";

		this.ElmChemicalOfConcern = document.createElement('h2');
		this.ElmChemicalOfConcern.style.padding = "0";
		this.ElmChemicalOfConcern.style.margin = "0";

		this.ElmSeverity = document.createElement('div');

		this.ElmDegreeOfExposure = new Object();
		this.ElmDegreeOfExposure.Description = document.createElement('div');
		this.ElmDegreeOfExposure.Score = document.createElement('div');

		this.ElmDataRepresentativeness = new Object();
		this.ElmDataRepresentativeness.Selection = document.createElement('select');
		this.ElmDataRepresentativeness.Score = document.createElement('div');

		this.ElmExposureDuration = new Object();
		this.ElmExposureDuration.Description = document.createElement('div');
		this.ElmExposureDuration.Score = document.createElement('div');

		this.ElmExposureRate = new Object();
		this.ElmExposureRate.Selection = document.createElement('select');
		this.ElmExposureRate.Score = document.createElement('div');

		this.ElmProbabilityScore = new Object();
		this.ElmProbabilityScore.Description = document.createElement('div');
		this.ElmProbabilityScore.Score = document.createElement('div');

		this.ElmRiskLevel = document.createElement('div');

		this._DegreeOfExposureScore = 0;
		this._DataRepresentativeness = RiskAssessment.Representativeness.Adequate;
		this._ExposureDurationScore = 0;
		this._ExposureRate = RiskAssessment.ExposureRate.Typical;
		this._RiskLevel = RiskAssessment.RiskLevel.Low;
		this._Severity = SEVERITY.Negligible;
		this._Probability = RiskAssessment.Probability.None;
	}

	connectedCallback()
	{
		if (!this._built)
		{
			this.style.display = "grid";
			this.style.gridTemplateRows = RowsTemplate;
			this.style.gridTemplateColumns = "repeat(2, fit-content)";
			this.style.gridGap = "0";
			this.style.border = "var(--section-border)";
			this.style.boxSizing = "border-box";

			let borderTopRight = function(elm)
			{
				elm.style.borderTop = "var(--section-border)";
				elm.style.borderRight = "var(--section-border)";

				return elm;
			};

			let ProbScoreNum = boxForGrid(this.ElmProbabilityScore.Score, 9, 2);
			ProbScoreNum.style.borderTop = "var(--section-border)";

			let ScoreElement = document.createElement('h1');
			ScoreElement.innerText = "Score";
			ScoreElement.style.padding = "0";
			ScoreElement.style.margin = "0";

			this.ElmDataRepresentativeness.Selection.style.width = "100%";
			this.ElmDataRepresentativeness.Selection.style.height = "100%";
			this.ElmDataRepresentativeness.Selection.style.gridRow = "6";
			this.ElmDataRepresentativeness.Selection.style.gridColumn = "1";
			this.ElmDataRepresentativeness.Selection.style.border = "0";

			this.ElmExposureRate.Selection.style.width = "100%";
			this.ElmExposureRate.Selection.style.height = "100%";
			this.ElmExposureRate.Selection.style.gridRow = "8";
			this.ElmExposureRate.Selection.style.gridColumn = "1";
			this.ElmExposureRate.Selection.style.border = "0";

			let makeOptionElement = function(text)
			{
				let ret = document.createElement('option');
				ret.innerHTML = text;

				return ret;
			};

			for (let property in RiskAssessment.Representativeness)
			{
				let Obj = RiskAssessment.Representativeness[property];
				this.ElmDataRepresentativeness.Selection.add(makeOptionElement(Obj.value));
			}

			for (let property in RiskAssessment.ExposureRate)
			{
				let Obj = RiskAssessment.ExposureRate[property];
				this.ElmExposureRate.Selection.add(makeOptionElement(Obj.value));
			}

			this._RiskLevelContainer = borderTopRight(
				boxForGrid(this.ElmRiskLevel, 10, 1)
			);

			this.append(
				boxForGrid(this.ElmHeading, 1, "1/3"),
				boxForGrid(this.ElmChemicalOfConcern, 2, 1),
				boxForGrid(this.ElmSeverity, 3, 1),
				boxForGrid(ScoreElement, 4, 2),

				borderTopRight(
					boxForGrid(this.ElmDegreeOfExposure.Description, 5, 1)
				),
				boxForGrid(this.ElmDegreeOfExposure.Score, 5, 2),

				borderTopRight(this.ElmDataRepresentativeness.Selection),
				boxForGrid(this.ElmDataRepresentativeness.Score, 6, 2),

				borderTopRight(
					boxForGrid(this.ElmExposureDuration.Description, 7, 1)
				),
				boxForGrid(this.ElmExposureDuration.Score, 7, 2),

				borderTopRight(this.ElmExposureRate.Selection),
				boxForGrid(this.ElmExposureRate.Score, 8, 2),

				borderTopRight(
					boxForGrid(this.ElmProbabilityScore.Description, 9, 1)
				),
				ProbScoreNum,
				this._RiskLevelContainer
			);

			this.DegreeOfExposure = { score : 0 };
			this.DataRepresentativeness = this._DataRepresentativeness;
			this.ExposureDuration = { score : 0 };
			this.ExposureRate = this._ExposureRate;

			this.ElmSeverity.innerText = SEVERITY.toString(this._Severity);

			this.updateProbabilityScore(true)

			this._built = true;

			this.ElmDataRepresentativeness.Selection.addEventListener(
				'change',
				function (event)
				{
					let SelectText = this.ElmDataRepresentativeness.Selection.value;

					if (SelectText != this._DataRepresentativeness.value)
					{
						for (let property in RiskAssessment.Representativeness)
						{
							let Obj = RiskAssessment.Representativeness[property];

							if (Obj.value == SelectText)
							{
								this.DataRepresentativeness = Obj;
								return;
							}
						}
					}
				}.bind(this)
			);

			this.ElmExposureRate.Selection.addEventListener(
				'change',
				function (event)
				{
					let SelectText = this.ElmExposureRate.Selection.value;

					if (SelectText != this._ExposureRate.value)
					{
						for (let property in RiskAssessment.ExposureRate)
						{
							let Obj = RiskAssessment.ExposureRate[property];

							if (Obj.value == SelectText)
							{
								this.ExposureRate = Obj;
								return;
							}
						}
					}
				}.bind(this)
			);
		}
	}

	get ProbabilityScore()
	{
		let ret = this._DegreeOfExposureScore + this._ExposureDurationScore;

		if (this._DataRepresentativeness != undefined)
			ret += this._DataRepresentativeness.score;

		if (this._ExposureRate != undefined)
			ret += this._ExposureRate.score;

		return ret;
	}

	get Probability()
	{
		return this._Probability;
	}

	get Risk()
	{
		return this._RiskLevel;
	}

	updateRiskLevel(force)
	{
		let levels = RiskAssessment.RiskLevel;
		let lvl = levels.None;

		if (this._Probability != levels.None)
			lvl = RiskAssessment.RiskLevelTable[this._Severity][this._Probability];

		if (lvl != this._RiskLevel || force != undefined)
		{
			this._RiskLevel = lvl;
			this.ElmRiskLevel.innerText = lvl;

			styleForRisk(this._RiskLevelContainer, lvl);
		}
	}

	updateProbabilityScore(force)
	{
		let Score = this.ProbabilityScore;
		let PropOption = RiskAssessment.Probability;
		let next = 0

		if (Score < 4)
			next = PropOption.None;
		else if (Score >= 4 && Score <= 6)
			next = PropOption.Unlikely;
		else if (Score == 7)
			next = PropOption.Seldom;
		else if (Score == 8)
			next = PropOption.Occasionally;
		else if (Score == 9)
			next = PropOption.Likely;
		else
			next = PropOption.Frequent;

		this.ElmProbabilityScore.Score.innerText = Score.toString();

		if (next != this._Probability || force != undefined)
		{
			this._Probability = next;

			this.ElmProbabilityScore.Description.innerText = this._Probability;

			this.updateRiskLevel(force);
		}
	}

	get Heading()
	{
		return this.ElmHeading.textContent;
	}

	set Heading(val)
	{
		this.ElmHeading.innerText = val;
	}

	get ChemicalOfConcern()
	{
		return this.ElmChemicalOfConcern.textContent;
	}

	set ChemicalOfConcern(val)
	{
		this.ElmChemicalOfConcern.innerText = val;
	}

	get Severity()
	{
		return this._Severity;
	}

	set Severity(val)
	{
		if (val != this._Severity)
		{
			this._Severity = val;
			this.ElmSeverity.innerText = SEVERITY.toString(val);

			this.updateRiskLevel();
		}
	}

	get DegreeOfExposure()
	{
		return {
			value : this.ElmDegreeOfExposure.Description.textContent,
			score : this._DegreeOfExposureScore
		};
	}

	set DegreeOfExposure(args)
	{
		if ("value" in args)
			this.ElmDegreeOfExposure.Description.innerText = args.value;

		if ("score" in args)
		{
			this.ElmDegreeOfExposure.Score.innerText = args.score.toString();
			this._DegreeOfExposureScore = args.score;

			this.updateProbabilityScore();
		}
	}

	get DataRepresentativeness()
	{
		return this._DataRepresentativeness;
	}

	set DataRepresentativeness(rep)
	{
		this._DataRepresentativeness = rep;

		this.ElmDataRepresentativeness.Selection.value = rep.value;
		this.ElmDataRepresentativeness.Score.innerHTML = rep.score.toString();

		this.updateProbabilityScore();
	}

	get ExposureDuration()
	{
		return {
			value : this.ElmExposureDuration.Description.textContent,
			score : this._ExposureDurationScore
		};
	}

	set ExposureDuration(args)
	{
		if ("value" in args)
			this.ElmExposureDuration.Description.innerText = args.value;

		if ("score" in args)
		{
			this.ElmExposureDuration.Score.innerText = args.score.toString();
			this._ExposureDurationScore = args.score;

			this.updateProbabilityScore();
		}
	}

	get ExposureRate()
	{
		return this._ExposureRate;
	}

	set ExposureRate(rate)
	{
		this._ExposureRate = rate;

		this.ElmExposureRate.Selection.value = rate.value;
		this.ElmExposureRate.Score.innerHTML = rate.score.toString();

		this.updateProbabilityScore();
	}

	set Visible(val)
	{
		this.style.display = (val) ? "grid" : "none";
	}

	get Visible()
	{
		let style = window.getComputedStyle(this);
		return (style.display != "none");
	}
}

customElements.define('jhrm-hra-risk-assessment', HraRiskAssessment);

export class HraRiskSummary extends HTMLElement
{
	constructor()
	{
		super();

		this._ElmChemicalOfConcern = document.createElement('div');

		this._ElmPeakPepcValue = document.createElement('div');
		this._ElmAveragePepcValue = document.createElement('div');

		this._ElmPeakMilitaryExposureGuidline = document.createElement('div');
		this._ElmAverageMilitaryExposureGuidline = document.createElement('div');

		this._ElmPeakRiskLevel = document.createElement('div');
		this._ElmAverageRiskLevel = document.createElement('div');

		this._containerPeakRiskLevel = new JhrmAlign();
		this._containerPeakRiskLevel.setAttribute("center", '');
		this._containerPeakRiskLevel.setAttribute("middle", '');
		this._containerPeakRiskLevel.append(this._ElmPeakRiskLevel);

		this._containerAverageRiskLevel = new JhrmAlign();
		this._containerAverageRiskLevel.setAttribute("center", '');
		this._containerAverageRiskLevel.setAttribute("middle", '');
		this._containerAverageRiskLevel.append(this._ElmAverageRiskLevel);

		this._ElmPeakRiskLevelConfidence = document.createElement('div');
		this._ElmAverageRiskLevelConfidence = document.createElement('div');

		this._PeakRiskLevel = RiskAssessment.RiskLevel.Low;
		this._AverageRiskLevel = RiskAssessment.RiskLevel.Low;

		this._built = false;
	}

	connectedCallback()
	{
		if (!this._built)
		{
			this.style.display = "grid";

			let createTopHeading = function(row, col, text)
			{
				let elm = new JhrmAlign();
				elm.innerHTML = `<b>${text}</b>`;
				elm.setAttribute("center", '');
				elm.setAttribute("middle", '');

				elm.style.gridRow = `${row}`;
				elm.style.gridColumn = `${col}`;

				return elm;
			};

			let createLeftHeading = function(row, col, text)
			{
				let elm = new JhrmAlign();
				elm.innerHTML = `<div style="padding-right: 1em;"><b>${text}</b></div>`;
				elm.setAttribute("middle", '');
				elm.setAttribute("right", '');

				elm.style.gridRow = `${row}`;
				elm.style.gridColumn = `${col}`;

				return elm;
			};

			let addElement = function(row, col, elm)
			{
				elm.style.gridRow = `${row}`;
				elm.style.gridColumn = `${col}`;

				this.append(elm);
			}.bind(this);

			this.append(
				createTopHeading(1, 2, "Acute Risk Peak"),
				createTopHeading(1, 3, "Acute Risk Average"),
				createLeftHeading(2, 1, "PEPC Value"),
				createLeftHeading(3, 1, "Military Exposure Guideline"),
				createLeftHeading(4, 1, "Risk Level"),
				createLeftHeading(5, 1, "Risk Level Confidence"),
			);

			addElement(1, 1, document.createElement('div'));
			addElement(2, 2, this._ElmPeakPepcValue);
			addElement(2, 3, this._ElmAveragePepcValue);
			addElement(3, 2, this._ElmPeakMilitaryExposureGuidline);
			addElement(3, 3, this._ElmAverageMilitaryExposureGuidline);
			addElement(4, 2, this._containerPeakRiskLevel);
			addElement(4, 3, this._containerAverageRiskLevel);
			addElement(5, 2, this._ElmPeakRiskLevelConfidence);
			addElement(5, 3, this._ElmAverageRiskLevelConfidence);

			this.PeakRiskLevel = this._PeakRiskLevel;
			this.AverageRiskLevel = this._AverageRiskLevel;

			if (this.hasAttribute('grid-border'))
				this.Border = this.getAttribute('grid-border');

			this._built = true;
		}
	}

	get ChemicalOfConcern()
	{
		return this._ElmChemicalOfConcern.textContent;
	}

	set ChemicalOfConcern(val)
	{
		this._ElmChemicalOfConcern.innerText = val;
	}

	get ExposurePathway()
	{
		// this._ElmExposurePathway.textContent;
	}

	set ExposurePathway(val)
	{
		// This element is not defined. It is causing errors.
		// this._ElmExposurePathway.innerText = val;
	}

	get PeakPepcValue()
	{
		return this._ElmPeakPepcValue.textContent;
	}

	set PeakPepcValue(val)
	{
		this._ElmPeakPepcValue.innerText = val;
	}

	get AveragePepcValue()
	{
		return this._ElmAveragePepcValue.textContent;
	}

	set AveragePepcValue(val)
	{
		this._ElmAveragePepcValue.innerText = val;
	}

	get PeakMilitaryExposureGuidline()
	{
		return this._ElmPeakMilitaryExposureGuidline.textContent;
	}

	set PeakMilitaryExposureGuidline(val)
	{
		this._ElmPeakMilitaryExposureGuidline.innerText = val;
	}

	get AverageMilitaryExposureGuidline()
	{
		return this._ElmAverageMilitaryExposureGuidline.textContent;
	}

	set AverageMilitaryExposureGuidline(val)
	{
		this._ElmAverageMilitaryExposureGuidline.innerText = val;
	}

	get PeakRiskLevel()
	{
		return this._PeakRiskLevel;
	}

	set PeakRiskLevel(val)
	{
		this._PeakRiskLevel = val;
		this._ElmPeakRiskLevel.innerText = val;

		styleForRisk(this._containerPeakRiskLevel, val);
	}

	get AverageRiskLevel()
	{
		return this._AverageRiskLevel;
	}

	set AverageRiskLevel(val)
	{
		this._AverageRiskLevel = val;
		this._ElmAverageRiskLevel.innerText = val;

		styleForRisk(this._containerAverageRiskLevel, val);
	}

	get PeakRiskLevelConfidence()
	{
		return this._ElmPeakRiskLevelConfidence.textContent;
	}

	set PeakRiskLevelConfidence(val)
	{
		this._ElmPeakRiskLevelConfidence.innerText = val;
	}

	get AverageRiskLevelConfidence()
	{
		return this._ElmAverageRiskLevelConfidence.textContent;
	}

	set AverageRiskLevelConfidence(val)
	{
		this._ElmAverageRiskLevelConfidence.innerText = val;
	}

	set Border(borderDef)
	{
		this.style.borderTop = borderDef;
		this.style.borderLeft = borderDef;

		for(let i = 0; i < this.children.length; ++i)
		{
			let elm = this.children.item(i);

			elm.style.borderRight = borderDef;
			elm.style.borderBottom = borderDef;
		}
	}
}

customElements.define('jhrm-hra-risk-summary', HraRiskSummary);

export class JhrmHraRiskTable extends HTMLElement
{
	constructor()
	{
		super();

		this.style.display = "inline-grid";

		this._table = document.createElement('table');
		this._table.style.cssText = `grid-row: 1; grid-column: 1; border-collapse: collapse;`;

		this.append(this._table);

		this._HasAcuteAverage = false;
		this._HasAcutePeak = false;
		this._HasChronicAverage = false;

		this._AcuteConfidence = undefined;
		this._ChronicConfidence = undefined;

		// HTML Related
		this._AcuteRiskLabel = undefined;
		this._ChronicRiskLabel = undefined;

		this._AcuteConfidenceLabel = undefined;
		this._ChronicConfidenceLabel = undefined;

		this._AcuteConfidenceLevelTableData = undefined;
		this._ChronicConfidenceLevelTableData = undefined;

		this._RowCount = 0;
	}

	connectedCallback()
	{
		// Known empty function
	}

	/**
	 * Sets the data for the table.  This will replace any existing data.
	 *
	 * @param { Object[] } data
	 * @param { ChemGroup } data[].Column
	 * @param { string } data[].Chemical
	 * @param { float } data[].PEPC - The PEPC concentration in mg/m3.
	 * @param { Meg } data[].Meg - Meg exceeded
	 * @param { RiskAssessment.RiskLevel } data[].RiskLevel - The risk level.
	 */
	setData(data)
	{
		const chem_stat_headings = `
			<th>PEPC (mg/m<sup>3</sup>)</th>
			<th>MEG (mg/m<sup>3</sup>)</th>
			<th>Risk Level</th>
		`;

		let Cols = ChemGroup;

		let ChemMap = {};
		let hasAcutePeak = false;
		let hasAcuteAverage = false;
		let hasChronicAverage = false;

		for ( let i = 0; i < data.length; ++i )
		{
			let Chemical = data[i].Chemical;
			let Column = data[i].Column;

			if (ChemMap[Chemical] == undefined)
				ChemMap[Chemical] = {};

			ChemMap[ Chemical ][ Column ] = data[i];

			hasAcutePeak |= (Column == ChemGroup.AcutePeak);
			hasAcuteAverage |= (Column == ChemGroup.AcuteAverage);
			hasChronicAverage |= (Column == ChemGroup.ChronicAverage);
		}

		let tr_1_inner = `<th rowspan="3" style="vertical-align: bottom;">Chemical Name</th>`;
		let colspan = 0;

		if ( hasAcutePeak && hasAcuteAverage )
			colspan = 6;
		else if (hasAcutePeak || hasAcuteAverage)
			colspan = 3;

		if ( colspan > 0)
			tr_1_inner += `<th id="AcuteRiskLabel" colspan="${colspan}" style="text-align: center;">Acute Risk Assessment</th>`;

		if ( hasChronicAverage )
			tr_1_inner += `<th id="ChronicRiskLabel" colspan="3" style="text-align: center;">Chronic Risk Assessment</th>`;

		let tr1 = document.createElement('tr');
		tr1.innerHTML = tr_1_inner;

		let tr_2_inner = ``;

		if ( hasAcutePeak )
			tr_2_inner += `<th colspan="3" style="text-align: center;">Peak</th>`;

		if ( hasAcuteAverage )
			tr_2_inner += `<th colspan="3" style="text-align: center;">Average</th>`;

		if ( hasAcutePeak || hasAcuteAverage)
			tr_2_inner += `<th id="AcuteConfidenceLabel" rowspan="2" style="text-align: center;">Confidence Level</th>`;

		if ( hasChronicAverage )
		{
			tr_2_inner += `<th colspan="3" style="text-align: center;">Average</th>`;
			tr_2_inner += `<th id="ChronicConfidenceLabel" rowspan="2" style="text-align: center;">Confidence Level</th>`;
		}

		let tr2 = document.createElement('tr');
		tr2.innerHTML = tr_2_inner;

		let tr_3_inner = ``;

		if ( hasAcutePeak )
			tr_3_inner += chem_stat_headings;

		if ( hasAcuteAverage )
			tr_3_inner += chem_stat_headings;

		if ( hasChronicAverage )
			tr_3_inner += chem_stat_headings;

		let tr3 = document.createElement('tr');
		tr3.innerHTML = tr_3_inner;

		let thead = document.createElement('thead');
		thead.append(tr1, tr2, tr3);

		this._table.innerHTML = "";
		this._table.append(thead);

		const createChemTableData = function(chem_entry)
		{
			let ret = [
				document.createElement('td'),
				document.createElement('td'),
				document.createElement('td')
			];

			ret[0].innerHTML = chem_entry.PEPC.toFixed(3).toString();
			ret[1].innerHTML = `
					<span style="white-space: nowrap;">${chem_entry.Meg.parm}</span><br>
					<span style="white-space: nowrap;">${chem_entry.Meg.value.value}</span>
				`;
			ret[2].innerHTML = chem_entry.RiskLevel;

			styleForRisk(ret[2], chem_entry.RiskLevel);

			return ret;
		}

		const createEmptyTableData = function()
		{
			let ret = [
				document.createElement('td'),
				document.createElement('td'),
				document.createElement('td')
			];

			ret[0].innerHTML = "N/A";
			ret[1].innerHTML = "N/A";
			ret[2].innerHTML = "N/A";

			return ret;
		};

		let tbody = document.createElement('tbody');
		this._table.append(tbody)

		let propKeys = Object.getOwnPropertyNames(ChemMap);
		propKeys.sort();

		this._RowCount = propKeys.length;

		for ( let i = 0; i < propKeys.length; ++i )
		{
			let key = propKeys[i];
			let entry = ChemMap[key];

			let chemRow = document.createElement('tr');
			tbody.append(chemRow);

			let chemHead = document.createElement('th');
			chemHead.innerHTML = key;

			chemRow.append(chemHead);

			if ( hasAcutePeak )
			{
				if ( entry[Cols.AcutePeak] )
					chemRow.append(...createChemTableData(entry[Cols.AcutePeak]));
				else
					chemRow.append(...createEmptyTableData());
			}

			if ( hasAcuteAverage )
			{
				if ( entry[Cols.AcuteAverage] )
					chemRow.append(...createChemTableData(entry[Cols.AcuteAverage]));
				else
					chemRow.append(...createEmptyTableData());
			}

			if ( 0 == i && (hasAcutePeak || hasAcuteAverage) )
			{
				let elm = document.createElement('td');
				elm.id = "AcuteConfidenceLevelTableData";

				chemRow.append(elm);
			}

			if ( hasChronicAverage )
			{
				if ( entry[Cols.ChronicAverage] )
					chemRow.append(...createChemTableData(entry[Cols.ChronicAverage]));
				else
					chemRow.append(...createEmptyTableData());

				if ( 0 == i )
				{
					let elm = document.createElement('td');
					elm.id = "ChronicConfidenceLevelTableData";

					chemRow.append(elm);
				}
			}
		}

		this._HasAcuteAverage = hasAcuteAverage;
		this._HasAcutePeak = hasAcutePeak;
		this._HasChronicAverage = hasChronicAverage;

		let style_border = (element) =>
		{
			element.style.border = "2px solid black";
			element.style.padding = "0.5em";
		};

		let td = [].slice.call(this.querySelectorAll("td"));
		td.forEach(style_border);

		let th = [].slice.call(this.querySelectorAll("th"));
		th.forEach(style_border);

		this._AcuteRiskLabel = selectAndStripId(this, "AcuteRiskLabel");
		this._ChronicRiskLabel = selectAndStripId(this, "ChronicRiskLabel");

		this._AcuteConfidenceLabel = selectAndStripId(this, "AcuteConfidenceLabel");
		this._ChronicConfidenceLabel = selectAndStripId(this, "ChronicConfidenceLabel");

		this._AcuteConfidenceLevelTableData = selectAndStripId(this, "AcuteConfidenceLevelTableData");
		this._ChronicConfidenceLevelTableData = selectAndStripId(this, "ChronicConfidenceLevelTableData");

		this.AcuteConfidence = this._AcuteConfidence;
		this.ChronicConfidence = this._ChronicConfidence;
	}

	/**
	 *
	 * @param { SampleCalculator[] } calcs An array of SampleCalculator objects.
	 */
	setCalcs(calcs)
	{
		let tabledata = calcs.map(
			(item) =>
			{
				return {
					Column : item.Group,
					Chemical : item.Chemical,
					PEPC : item.Group != ChemGroup.None ? item.PEPC.value : null,
					Meg : item.Group != ChemGroup.None ? item.Meg : null,
					RiskLevel : item.Group != ChemGroup.None ? item.RiskLevel : null
				};
			}
		);

		this.setData(tabledata);
	}

	get HasAcuteAverage()
	{
		return this._HasAcuteAverage;
	}

	get HasAcutePeak()
	{
		return this._HasAcutePeak;
	}

	get HasChronicAverage()
	{
		return this._HasChronicAverage;
	}

	get AcuteConfidence()
	{
		return this._AcuteConfidence;
	}

	set AcuteConfidence( ac )
	{
		this._AcuteConfidence = ac;

		if ( this._AcuteConfidenceLevelTableData )
		{
			let ACLTD = this._AcuteConfidenceLevelTableData;

			ACLTD.innerHTML = ( ac ) ? ac : "";
			ACLTD.style.display = ( ac ) ? "table-cell" : "none";
			ACLTD.rowSpan = this._RowCount.toString();
		}

		if ( this._AcuteRiskLabel )
		{
			let ARL = this._AcuteRiskLabel;
			let colspan = 0;

			colspan += ( ac ) ? 1 : 0;
			colspan += ( this._HasAcuteAverage ) ? 3 : 0;
			colspan += ( this._HasAcutePeak ) ? 3 : 0;

			ARL.colSpan = colspan.toString();
		}

		if ( this._AcuteConfidenceLabel )
			this._AcuteConfidenceLabel.style.display = (ac) ? "table-cell" : "none";
	}

	get ChronicConfidence()
	{
		return this._ChronicConfidence;
	}

	set ChronicConfidence( cc )
	{
		this._ChronicConfidence = cc;

		if ( this._ChronicConfidenceLevelTableData )
		{
			let CCLTD = this._ChronicConfidenceLevelTableData;

			CCLTD.innerHTML = ( cc ) ? cc : "";
			CCLTD.style.display = ( cc ) ? "table-cell" : "none";
			CCLTD.rowSpan = this._RowCount.toString();
		}

		if ( this._ChronicRiskLabel )
		{
			let CRL = this._ChronicRiskLabel;
			let colspan = 0;

			colspan += ( cc ) ? 1 : 0;
			colspan += ( this._HasChronicAverage ) ? 3 : 0;

			CRL.colSpan = colspan.toString();
		}

		if ( this._ChronicConfidenceLabel )
			this._ChronicConfidenceLabel.style.display = (cc) ? "table-cell" : "none";
	}
}

JhrmHraRiskTable.Column = {
	AcutePeak : "Acute Peak",
	AcuteAverage : "Acute Average",
	ChronicAverage : "Chronic Average"
};

customElements.define('jhrm-hra-risk-table', JhrmHraRiskTable);

export class HraRiskSummaryDisplay extends HTMLElement
{
	constructor()
	{
		super();

		let cell_style = `white-space: nowrap; padding: 0.2em 1em;`;
		let header_style = `text-align: right; ${cell_style}`;
		let text_val_style = `text-align: left; ${cell_style}`;
		let score_style = `text-align: center; ${cell_style}`;
		let border_def = `2px solid`;
		let border_color = "#2F528F";
		let prob_level_label_style = "font-size: 1.25em; font-weight: bold; padding-right: 0.75em;";
		let prob_level_value_style = `border: ${border_def}; border-color: black; padding: 0.3em 1.5em; font-size: 1.25em; text-align: center; min-width: 7em;`;

		let rep = RiskAssessment.Representativeness.Overestimated;
		let er = RiskAssessment.ExposureRate.Light;

		this.style.display = "inline-grid";
		this.style.gridTemplateColumns = "50% max-content minmax( minmax(2em, max-content), 1fr)";
		this.style.width = "auto";
		this.style.minWidth = "min-content";

		this.innerHTML = `
			<div style="text-align: center; width: 100%; grid-row: 1; grid-column: 1/4;
			            border-top: ${border_def}; border-left: ${border_def};  border-right: ${border_def};
			            border-color: ${border_color}; padding: 0.5em; font-size: 1.3em;"
			>
				Rank Hazard Probability
			</div>
			<div style="grid-row: 2; grid-column: 1/4; display: grid; grid-template-columns: min-content minmax(10em, auto) minmax(3em, auto) 1fr;
			            border-left: ${border_def}; border-right: ${border_def}; border-color: ${border_color}; padding: 0.35em;"
			>
				<div style="grid-row: 1; grid-column: 1; ${header_style}">Degree of Exposure: </div>
				<div style="grid-row: 1; grid-column: 2; ${text_val_style}" id="_ElmDegreeOfExposureText">(none)</div>
				<div style="grid-row: 1; grid-column: 3; ${score_style}" id="_ElmDegreeOfExposureScore">0</div>

				<div style="grid-row: 2; grid-column: 1; ${header_style}">Representiveness of Data: </div>
				<div style="grid-row: 2; grid-column: 2; ${text_val_style}" id="_ElmRepresentivenessOfDataText">${rep.value}</div>
				<div style="grid-row: 2; grid-column: 3; ${score_style}" id="_ElmRepresentivenessOfDataScore">${rep.score}</div>

				<div style="grid-row: 3; grid-column: 1; ${header_style}">Duration of Exposure: </div>
				<div style="grid-row: 3; grid-column: 2; ${text_val_style}" id="_ElmDurationOfExposureText">(none)</div>
				<div style="grid-row: 3; grid-column: 3; ${score_style}" id="_ElmDurationOfExposureScore">0</div>

				<div style="grid-row: 4; grid-column: 1; ${header_style}">Rate of Exposure: </div>
				<div style="grid-row: 4; grid-column: 2; ${text_val_style}" id="_ElmRateOfExposureText">${er.value}</div>
				<div style="grid-row: 4; grid-column: 3; ${score_style}" id="_ElmRateOfExposureScore">${er.score}</div>

				<div style="grid-row: 5; grid-column: 3; border-top: ${border_def}; border-color: black; ${score_style}" id="_ElmScore"></div>
			</div>
			<div style="grid-row: 3; grid-column: 1; border-left: ${border_def}; border-color: ${border_color};"></div>
			<jhrm-align middle right style="grid-row: 3; grid-column: 1; ${prob_level_label_style}
			                                border-left: ${border_def}; border-color: ${border_color};"
			>
				Probability:
			</jhrm-align>
			<div style="grid-row: 3; grid-column: 2; ${prob_level_value_style}" id="_ElmProbability"></div>
			<div style="grid-row: 3; grid-column: 3; border-right: ${border_def}; border-color: ${border_color};"></div>

			<div style="grid-row: 4; grid-column: 1/4; border: ${border_def}; border-top: 0; border-color: ${border_color}; height: 0.75em;"></div>

			<div style="grid-row: 5; grid-column: 1/4; height: 0.75em;"></div>

			<jhrm-align middle right style="grid-row: 6; grid-column: 1; ${prob_level_label_style}">Risk Level: </jhrm-align>
			<div style="grid-row: 6; grid-column: 2; ${prob_level_value_style}" id="_ElmRiskLevel">Low</div>
	`;

		this._ElmDegreeOfExposureText = selectAndStripId(this, "_ElmDegreeOfExposureText");
		this._ElmDegreeOfExposureScore = selectAndStripId(this, "_ElmDegreeOfExposureScore");

		this._ElmRepresentivenessOfDataText = selectAndStripId(this, "_ElmRepresentivenessOfDataText");
		this._ElmRepresentivenessOfDataScore = selectAndStripId(this, "_ElmRepresentivenessOfDataScore");

		this._ElmDurationOfExposureText = selectAndStripId(this, "_ElmDurationOfExposureText");
		this._ElmDurationOfExposureScore = selectAndStripId(this, "_ElmDurationOfExposureScore");

		this._ElmRateOfExposureText = selectAndStripId(this, "_ElmRateOfExposureText");
		this._ElmRateOfExposureScore = selectAndStripId(this, "_ElmRateOfExposureScore");

		this._ElmScore = selectAndStripId(this, "_ElmScore");

		this._ElmProbability = selectAndStripId(this, "_ElmProbability");
		this._ElmRiskLevel = selectAndStripId(this, "_ElmRiskLevel");

		this._Calc = new SampleCalculator();
		this._Built = false;
	}

	connectedCallback()
	{
		if (!this._Built)
		{
			this._Built = true;
		}
	}

	_updateOutcomes()
	{
		try
		{
			let score = this._Calc.Score;
			let probablity = this._Calc.Probability;
			let risk_level = this._Calc.RiskLevel;

			this._ElmScore.innerText = score.toString();
			this._ElmProbability.innerHTML = (probablity.length > 0) ? probablity : "&#8205;";
			this._ElmRiskLevel.innerText = risk_level;

			styleForRisk(this._ElmRiskLevel, risk_level);
		}
		catch (ex)
		{

		}
	}

	_updateDurationOfExposure()
	{
		try
		{
			let doe = this._Calc.DurationOfExposure;

			this._ElmDurationOfExposureText.innerText = doe.value;
			this._ElmDurationOfExposureScore.innerText = doe.score.toString();
		}
		catch (ex)
		{

		}
	}

	_updateDegreeOfExposure()
	{
		try
		{
			let doe = this._Calc.DegreeOfExposure;

			this._ElmDegreeOfExposureText.innerText = doe.value;
			this._ElmDegreeOfExposureScore.innerText = doe.score.toString();
		}
		catch (ex)
		{

		}
	}

	/**
	 * Sets the megs that are used in Degree of Exposure calculations.
	 *
	 * @param { MEG[] }
	 */
	set ChemicalMegs(megs)
	{
		this._Calc.ChemicalMegs = megs;
		this._updateDegreeOfExposure();
		this._updateOutcomes();
	}

	get ChemicalMegs()
	{
		return this._Calc.ChemicalMegs;
	}

	/**
	 * Sets the megs that are used in Degree of Exposure calculations.
	 *
	 * @param { ChemicalSample[] }
	 */
	set ChemicalSamples(samples)
	{
		this._Calc.ChemicalSamples = samples;
	}

	/**
	 * Sets the megs that are used in Degree of Exposure calculations.
	 *
	 * @param { ChemicalSample[] }
	 */
	get ChemicalSamples()
	{
		return this._Calc.ChemicalSamples;
	}

	/**
	 * @returns { EXPOSURE_DURATION }
	 */
	get SampleTime()
	{
		return this._Calc.SampleTime;
	}

	/**
	 * @param { EXPOSURE_DURATION } st
	 */
	set SampleTime( st )
	{
		this._Calc.SampleTime = st;
		this._updateDurationOfExposure();
		this._updateDegreeOfExposure();
		this._updateOutcomes();
	}

	get DefaultSampleTime()
	{
		return this._Calc.DefaultSampleTime;
	}

	/**
	 * @returns { Meg }
	 */
	get Meg()
	{
		return this._Calc.Meg;
	}

	/**
	 * @returns { Meg }
	 */
	set Meg(meg)
	{
		this._Calc.Meg = meg;
		this._updateDegreeOfExposure();
		this._updateDurationOfExposure();
		this._updateOutcomes();
	}

	get DefaultMeg()
	{
		return this._Calc.DefaultMeg;
	}

	/**
	 * @param { string }
	 */
	set Chemical(ch)
	{
		this._Calc.Chemical = ch;
	}

	/**
	 * @returns { string }
	 */
	get Chemical()
	{
		return this._Calc.Chemical;
	}

	/**
	 * @param { string }
	 */
	set CasNumber(str)
	{
		this._Calc.CasNumber = str;
	}

	/**
	 * @returns { string }
	 */
	get CasNumber()
	{
		return this._Calc.CasNumber;
	}

	/**
	 * @param { ChemGroup }
	 */
	set Group(grp)
	{
		this._Calc.Group = grp;
		this._updateOutcomes();
	}

	/**
	 * @returns { ChemGroup }
	 */
	get Group()
	{
		return this._Calc.Group;
	}

	/**
	 * @returns { DimensionedValue }
	 */
	get PEPC()
	{
		return this._Calc.PEPC;
	}

	/**
	 * @returns { object }
	 * @returns { number } object.score - The numeric score.
	 * @returns { string } object.value - The text that is displayed with the score.
	 */
	get DegreeOfExposure()
	{
		return this._Calc.DegreeOfExposure;
	}

	/**
	 * @param { RiskAssessment.Representativeness } rep
	 */
	set DataRepresentativeness(rep)
	{
		this._Calc.DataRepresentativeness = rep;

		this._ElmRepresentivenessOfDataText.innerText = rep.value;
		this._ElmRepresentivenessOfDataScore.innerHTML = rep.score.toString();

		this._updateOutcomes();
	}

	/**
	 * @returns { RiskAssessment.Representativeness }
	 */
	get DataRepresentativeness()
	{
		return this._Calc.DataRepresentativeness;
	}

	get DurationOfExposure()
	{
		return this._Calc.DurationOfExposure;
	}

	/**
	 * @param { RiskAssessment.ExposureRate } rate
	 */
	set RateOfExposure(rate)
	{
		this._Calc.RateOfExposure = rate;

		this._ElmRateOfExposureText.innerText = rate.value;
		this._ElmRateOfExposureScore.innerHTML = rate.score.toString();

		this._updateOutcomes();
	}

	/**
	 * @returns { RiskAssessment.ExposureRate }
	 */
	get RateOfExposure()
	{
		return this._Calc.RateOfExposure;
	}

	/**
	 * @returns { RiskAssessment.RiskLevel }
	 */
	get RiskLevel()
	{
		return this._Calc.RiskLevel;
	}

	/**
	 * @returns { SEVERITY }
	 */
	get Severity()
	{
		return this._Calc.Severity;
	}

	/**
	 * @returns { RiskAssessment.Probability }
	 */
	get Probability()
	{
		return this._Calc.Probability;
	}

	get Score()
	{
		return this._Calc.Score;
	}

	get PeakConcentration()
	{
		return this._Calc.PeakConcentration;
	}

	get AverageConcentration()
	{
		return this._Calc.AverageConcentration;
	}

	get IsAcute()
	{
		return this._Calc.IsAcute;
	}

	get IsChronic()
	{
		return this._Calc.IsChronic;
	}
}

customElements.define('jhrm-hra-risk-summary-display', HraRiskSummaryDisplay);

export class JhrmRiskEditorPage extends HTMLElement
{
	constructor()
	{
		super();

		this.innerHTML = `
			<div style="display: inline-grid; margin: 0.5em;">
				<jhrm-align middle style="grid-row: 1; grid-column: 1; padding-right: 0.25em;">PEPC Value (mg/m3):</jhrm-align>
				<jhrm-align middle left style="display: inline-block; grid-row: 1; grid-column: 2; border: 1px solid black; padding: 0.5em; min-width: 8em;"><span id="PEPC">0.00</span></jhrm-align>
			</div><br>
			<jhrm-hra-risk-summary-display id="Display"></jhrm-hra-risk-summary-display>
		`;

		this._elmRiskDisplay = selectAndStripId(this, "Display");
		this._elmPepc = selectAndStripId(this, "PEPC");

		this._elmPepc.innerHTML = "0.0";

		this._SampleTimeSelect = undefined;
		this._MegSelect = undefined;

		this._Completed = false;
	}

	_updatePepc()
	{
		try
		{
			this._elmPepc.innerHTML = normalizeUnits(this.PEPC, this.Meg).print(true);
		}
		catch (err)
		{

		}
	}

	/**
	 * Sets the megs that are used in Degree of Exposure calculations.
	 *
	 * @param { MEG[] }
	 */
	set ChemicalMegs(megs)
	{
		this._elmRiskDisplay.ChemicalMegs = megs;
		this._updatePepc();
	}

	get ChemicalMegs()
	{
		return this._elmRiskDisplay.ChemicalMegs;
	}

	/**
	 * Sets the megs that are used in Degree of Exposure calculations.
	 *
	 * @param { ChemicalSample[] }
	 */
	set ChemicalSamples(samples)
	{
		this._elmRiskDisplay.ChemicalSamples = samples;
		this._updatePepc();
	}

	/**
	 * Sets the megs that are used in Degree of Exposure calculations.
	 *
	 * @param { ChemicalSample[] }
	 */
	get ChemicalSamples()
	{
		return this._elmRiskDisplay.ChemicalSamples;
	}

	/**
	 * @returns { EXPOSURE_DURATION }
	 */
	get SampleTime()
	{
		return this._elmRiskDisplay.SampleTime;
	}

	/**
	 * @param { EXPOSURE_DURATION } st
	 */
	set SampleTime( st )
	{
		this._elmRiskDisplay.SampleTime = st;
	}

	get DefaultSampleTime()
	{
		return this._elmRiskDisplay.DefaultSampleTime;
	}

	/**
	 * @returns { MEG }
	 */
	get Meg()
	{
		return this._elmRiskDisplay.Meg;
	}

	/**
	 * @param { MEG }
	 */
	set Meg( meg )
	{
		this._elmRiskDisplay.Meg = meg;
	}

	get DefaultMeg()
	{
		return this._elmRiskDisplay.DefaultMeg;
	}

	/**
	 * Gets the chemical associated with the page.
	 *
	 * @returns { string }
	 */
	get Chemical()
	{
		return this._elmRiskDisplay.Chemical;
	}

	set Chemical(chem)
	{
		this._elmRiskDisplay.Chemical = chem;
	}

	/**
	 * @param { string }
	 */
	get CasNumber()
	{
		return this._elmRiskDisplay.CasNumber;
	}

	/**
	 * @returns { string }
	 */
	set CasNumber(cas)
	{
		this._elmRiskDisplay.CasNumber = cas;
	}

	/**
	 * @param { ChemGroup }
	 */
	set Group(grp)
	{
		this._elmRiskDisplay.Group = grp;
		this._updatePepc();
	}

	/**
	 * @returns{ ChemGroup }
	 */
	get Group()
	{
		return this._elmRiskDisplay.Group;
	}

	/**
	 * @returns { DimensionedValue }
	 */
	get PEPC()
	{
		return this._elmRiskDisplay.PEPC;
	}

	get DegreeOfExposure()
	{
		return this._elmRiskDisplay.DegreeOfExposure;
	}

	/**
	 * @param { RiskAssessment.Representativeness } rep
	 */
	set DataRepresentativeness(rep)
	{
		this._elmRiskDisplay.DataRepresentativeness = rep;
	}

	get DataRepresentativeness()
	{
		return this._elmRiskDisplay.DataRepresentativeness;
	}

	get DurationOfExposure()
	{
		return this._elmRiskDisplay.DurationOfExposure;
	}

	get RateOfExposure()
	{
		return this._elmRiskDisplay.RateOfExposure;
	}

	get RiskLevel()
	{
		return this._elmRiskDisplay.RiskLevel;
	}

	get Severity()
	{
		return this._elmRiskDisplay.Severity;
	}

	get Probability()
	{
		return this._elmRiskDisplay.Probability;
	}

	get Score()
	{
		return this._elmRiskDisplay.Score;
	}

	get Completed()
	{
		return this._Completed;
	}

	set Completed(val)
	{
		this._Completed = val;
	}

	/**
	 * Gets a SampleCalculator preset to the current state of the editor.
	 */
	get Calculator()
	{
		let ret = new SampleCalculator();

		ret.ChemicalMegs = this.ChemicalMegs;
		ret.Meg = this.Meg;

		ret.ChemicalSamples = this.ChemicalSamples;
		ret.SampleTime = this.SampleTime;

		ret.Group = this.Group;
		ret.Chemical = this.Chemical;
		ret.CasNumber = this.CasNumber;

		ret.DataRepresentativeness = this.DataRepresentativeness;
		ret.RateOfExposure = this.RateOfExposure;

		return ret;
	}
}

customElements.define('jhrm-risk-editor-page', JhrmRiskEditorPage);

export class JhrmMultipleChemicalRiskEditor extends HTMLElement
{
	constructor()
	{
		super();

		///// GUI /////

		this.innerHTML = `
			<div style="padding: 1em;">
				<jhrm-select id="ChemSelect" style="display: block; margin-right: 2em;"></jhrm-select>
				Representativeness of Data: <material-icon icon="help_outline" id="RepDataSelectHelp"></material-icon> <jhrm-select id="RepDataSelect" style="display: block; margin-right: 2em;"></jhrm-select>
					<jhrm-tooltip target = "RepDataSelectHelp">Measure of the how representative the field-collected data
						is of the true exposure concentration (Ref. TG230, Exhibit 3-3). Select whether the field data
						adequately, underestimates, or overestimates population exposure.
					</jhrm-tooltip>
				Rate of Exposure: <material-icon icon="help_outline" id="RateOfExposureSelectHelp"></material-icon> <jhrm-select id="RateOfExposureSelect" style="display: block;"></jhrm-select>
					<jhrm-tooltip target="RateOfExposureSelectHelp"> Measure of how different the population's rate of exposure (e.g., inhalation
					rate, water consumption rate, soil contact rate) is relative to the assumed rate used to develop
					the MEG (Ref. TG230, Exhibit 3-3). Select the appropriate rate of exposure.
					</jhrm-tooltip>
			</div>
			<div style="display: inline-grid; grid-template-columns: auto 1fr; border: 1px solid black; padding: 1em;">
				<jhrm-swap-space id="SampleDisplaySwap" style="grid-row: 1; grid-column: 1;"></jhrm-swap-space>
				<jhrm-align top center style="grid-row: 1; grid-column: 2; padding: 0 2em 1em 2em; text-align: center;">
					<div style="margin: 2em auto 0.5em auto; font-size: 80%;">Select Population Exposure Duration of Concern <material-icon icon="help_outline" id="SampleDisplaySwapHelp"></material-icon></div>
						<jhrm-tooltip target ="SampleDisplaySwapHelp">Select the appropriate exposure duration of concern for the population at risk. Note: JHRM will default to the duration of exposure time based on the sampling time.
						</jhrm-tooltip>
						<jhrm-swap-space id="SampleTimeSelectSwap" style="margin-bottom: 2em;"></jhrm-swap-space><br>
					<div style="margin: 0 auto 0.5em auto; font-size: 80%;">Select MEG Hazard Severity <material-icon icon="help_outline" id="SampleTimeSelectSwapHelp"></material-icon>
						<jhrm-tooltip target ="SampleTimeSelectSwapHelp">Select the appropriate MEG Hazard Severity. Note: JHRM will default to a MEG Hazard Severity based on the PEPC concentration.
						</jhrm-tooltip>
					</div>
					<jhrm-swap-space id="MegSelectSwap"></jhrm-swap-space>
				</jhrm-align>
			</div>
		`;

		this._SampleSwapSpace = selectAndStripId(this, "SampleDisplaySwap");
		this._MegSelectSwapSpace = selectAndStripId(this, "MegSelectSwap");
		this._SampleTimeSelectSwapSpace = selectAndStripId(this, "SampleTimeSelectSwap");

		this._LastSelection = undefined;

		this._ChemSelect = selectAndStripId(this, "ChemSelect");
		this._ChemSelect.onchange = function()
		{
			let val = this._ChemSelect.Value;

			if ( val )
			{
				this._SampleSwapSpace.VisibleElement = val;
				this._MegSelectSwapSpace.VisibleElement = val._MegSelect;
				this._SampleTimeSelectSwapSpace.VisibleElement = val._SampleTimeSelect;

				if ( this._LastSelection && !this._LastSelection.Completed )
				{
					this._LastSelection.OptionHandle.Style.color = "darkgreen";
					this._LastSelection.OptionHandle.Label += " - Completed";
					this._LastSelection.Completed = true;
				}

				this._LastSelection = val;
			}

		}.bind(this);

		let rep = RiskAssessment.Representativeness;
		let exp = RiskAssessment.ExposureRate;

		this._RepDataSelect = selectAndStripId(this, "RepDataSelect");
		this._RepDataSelect.addOption(rep.Overestimated.value, rep.Overestimated);
		this._RepDataSelect.addOption(rep.Adequate.value, rep.Adequate);
		this._RepDataSelect.addOption(rep.Underestimated.value, rep.Underestimated);
		this._RepDataSelect.onchange = this._applyRepOfData.bind(this);

		this._RateOfExposureSelect = selectAndStripId(this, "RateOfExposureSelect");
		this._RateOfExposureSelect.addOption(exp.Light.value, exp.Light);
		this._RateOfExposureSelect.addOption(exp.Typical.value, exp.Typical);
		this._RateOfExposureSelect.addOption(exp.Heavy.value, exp.Heavy);
		this._RateOfExposureSelect.onchange = this._applyRateofExp.bind(this);

		this.DataRepresentativeness = this._RepresentivenessOfData = RiskAssessment.Representativeness.Adequate;
		this.RateOfExposure = this._RateOfExposure = RiskAssessment.ExposureRate.Typical;

		///// data storage ////

		this.clear();
	}

	clear()
	{
		let groups = ChemGroup;

		this._ChemSelect.clear();
		this._SampleSwapSpace.clear();
		this._MegSelectSwapSpace.clear();
		this._SampleTimeSelectSwapSpace.clear();

		this._SampleDisplays = {};
		this._SampleDisplays[groups.AcutePeak] = {};
		this._SampleDisplays[groups.AcuteAverage] = {};
		this._SampleDisplays[groups.Acute] = {};
		this._SampleDisplays[groups.ChronicAverage] = {};

		this._MegsForChemical = {};
		this._SamplesForChemical = {};

		this._MegSelectors = {};
		this._SampleTimeSelectors = {};

		this._SampleSwapSpace.appendPage( document.createElement('div') );
		this._SampleSwapSpace.appendPage( document.createElement('div') );
		this._MegSelectSwapSpace.appendPage( document.createElement('div') );
		this._SampleTimeSelectSwapSpace.appendPage( document.createElement('div') );
	}

	/**
	 * Pushes the risk paramters selected in the editor to the middle tier.
	 *
	 * @param { string } hra_id
	 * @returns { promise } The promise object retured by the post operation.
	 */
	push(hra_id)
	{
		// api/enhancedhra/router.js line 748

		let upload_data = {};

		let pack = (display) =>
		{
			// this._elmRiskDisplay
			return {
				severity :
				{
					value : display.Severity
				},
				degreeOfExposure : display.DegreeOfExposure,
				durationOfExposure : display.DurationOfExposure,
				representativenessOfData : display._elmRiskDisplay.DataRepresentativeness,
				rateOfExposure : display._elmRiskDisplay.RateOfExposure,
				probability :
					{
						score : display.Score,
						value : display.Probability
					},
				risk : display.RiskLevel,
				meg : display.Meg,
				cas_number : display.CasNumber,
				confidence : "NONE",
				custom_exposure_duration : display.SampleTime
			};
		};

		let all_displays = this._getAllDisplays();

		for ( let index in all_displays)
		{
			let curr_risk = all_displays[index];
			let cas = curr_risk.CasNumber;

			if (upload_data[cas] == undefined)
			{
				upload_data[cas] = {};
				upload_data[cas].cas_number = cas;
			}

			let packed = pack(curr_risk);

			if (curr_risk.Group.localeCompare(ChemGroup.AcutePeak) == 0)
				upload_data[cas].acute_peak = packed;
			else if (curr_risk.Group.localeCompare(ChemGroup.AcuteAverage) == 0)
				upload_data[cas].acute_average = packed;
			else if (curr_risk.Group.localeCompare(ChemGroup.ChronicAverage) == 0)
				upload_data[cas].chronic_average = packed;
		}

		return Request.post(
			`/api/enhancedhra/hra/${hra_id}/risk`,
			Object.values(upload_data)
		);
	}

	/**
	 *
	 * @param { string } cas The CAS ID of the chemical.
	 * @param { ChemGroup } group
	 */
	_getMegSelector(cas, group)
	{
		try
		{
			let ret = this._MegSelectors[cas][group];

			if (undefined != ret)
				return ret;
		}
		catch
		{
			if ( undefined == this._MegSelectors[cas] )
				this._MegSelectors[cas] = {};
		}

		let options = [...this._MegsForChemical[cas]];

		if (group == ChemGroup.ChronicAverage)
		{
			options = options.filter( meg =>
				meg.timeframe == EXPOSURE_DURATION.YEAR_1
			);
		}
		else
		{
			options = options.filter( meg =>
				!( meg.timeframe == EXPOSURE_DURATION.YEAR_1 )
			);
		}

		let selector = new JhrmMegTableSelect();
		selector.setOptions(options);

		this._MegSelectors[cas][group] = selector;
		this._MegSelectSwapSpace.appendPage(selector);

		return selector;
	}

	_getSampleTimeSelector(cas, group)
	{
		try
		{
			let ret = this._SampleTimeSelectors[cas][group];

			if (undefined != ret)
				return ret;
		}
		catch
		{
			if ( undefined == this._SampleTimeSelectors[cas] )
				this._SampleTimeSelectors[cas] = {};
		}

		let selector = new JhrmHraDurationSelector();

		if (group == ChemGroup.ChronicAverage)
		{
			selector.setDurationVisible(EXPOSURE_DURATION.MINUTES_10, false);
			selector.setDurationVisible(EXPOSURE_DURATION.HOUR_1, false);
			selector.setDurationVisible(EXPOSURE_DURATION.HOURS_8, false);
			selector.setDurationVisible(EXPOSURE_DURATION.HOURS_24, false);
			selector.setDurationVisible(EXPOSURE_DURATION.DAYS_7, false);
			selector.setDurationVisible(EXPOSURE_DURATION.DAYS_14, false);
		}
		else
		{
			selector.setDurationVisible(EXPOSURE_DURATION.YEAR_1, false);
			selector.setDurationVisible(JhrmHraDurationSelector.OTHER, false);
		}

		this._SampleTimeSelectors[cas][group] = selector;
		this._SampleTimeSelectSwapSpace.appendPage(selector);

		return selector;
	}

	_getAllDisplays()
	{
		let ret = [];

		let groups = ChemGroup;

		for ( let sample_key in this._SampleDisplays )
		{
			let groupDisplays = this._SampleDisplays[sample_key];
			for (let key in groupDisplays)
				ret.push(groupDisplays[key]);
		}

		return ret;
	}

	_applyRepOfData()
	{
		let rep = this._RepDataSelect.Value;
		let all_displays = this._getAllDisplays();

		for ( const key in all_displays )
			all_displays[key]._elmRiskDisplay.DataRepresentativeness = rep;
	}

	_applyRateofExp()
	{
		let exp = this._RateOfExposureSelect.Value;
		let all_displays = this._getAllDisplays();

		for ( const key in all_displays )
			all_displays[key]._elmRiskDisplay.RateOfExposure = exp;
	}

	/**
	 * @param { string } chemical
	 * @param { EXPOSURE_DURATION }
	 */
	setChemSampleTime(cas, exp_dur)
	{
		this._getSampleTimeSelector(cas).Value = exp_dur;
	}

	/**
	 *
	 * @param { cas_number ] samples
	 * @param { ChemGroup } chem_group
	 * @param {Object} assessment assessment as returned by GET /api/enhancedhra/hra/[ID]/risk
	 * @param {Number} assessment.id
	 * @param {String} assessment.chem_rapid_hra_id
	 * @param {String} assessment.type
	 * @param {String} assessment.chemical_name
	 * @param {String} assessment.cas_number
	 * @param {Number} assessment.meg
	 * @param {String} assessment.meg_name
	 * @param {String} assessment.meg_units
	 * @param {String} assessment.meg_version
	 * @param {String} assessment.severity
	 * @param {String} assessment.prob_degree_of_exp
	 * @param {Number} assessment.prob_degree_of_exp_score
	 * @param {String} assessment.prob_rep_of_data
	 * @param {Number} assessment.prob_rep_of_data_score
	 * @param {String} assessment.prob_duration_of_exp
	 * @param {Number} assessment.prob_duration_of_exp_score
	 * @param {String} assessment.prob_rate_of_exp
	 * @param {Number} assessment.prob_rate_of_exp_score
	 * @param {String} assessment.probability
	 * @param {Number} assessment.probability_score
	 * @param {String} assessment.risk_level
	 * @param {String} assessment.confidence
	 * @param {Number} assessment.custom_exposure_duration
	 * @param {Number} assessment.custom_meg
	 */
	_createEditor(cas_number, chem_group, assessment)
	{
		let samples = this._SamplesForChemical[cas_number];
		let chemical_name = samples[0].chemical_name;

		let RiskEditor = new JhrmRiskEditorPage();
		RiskEditor.Chemical = chemical_name;
		RiskEditor.CasNumber = cas_number;
		RiskEditor.ChemicalSamples = samples;
		RiskEditor.ChemicalMegs = this._MegsForChemical[cas_number];
		RiskEditor.Group = chem_group;

		RiskEditor._elmRiskDisplay.DataRepresentativeness = this.DataRepresentativeness;
		RiskEditor._elmRiskDisplay.RateOfExposure = this.RateOfExposure;

		/////////// Meg Selection ///////////////
		RiskEditor._MegSelect = this._getMegSelector(cas_number, chem_group);
		RiskEditor._MegSelect.addEventListener(
			"change",
			function(evt)
			{
				this.Meg = evt.detail;
			}.bind(RiskEditor)
		);

		let meg_select_value = RiskEditor._MegSelect.Value;

		if (meg_select_value)
		{
			RiskEditor.Meg = meg_select_value;
		}
		///////////////////////////////////////


		/////////// Sample Time Selection ///////////////
		RiskEditor._SampleTimeSelect = this._getSampleTimeSelector(cas_number, chem_group);
		RiskEditor._SampleTimeSelect.addEventListener(
			"change",
			function(evt)
			{
				this.SampleTime = evt.detail;
			}.bind(RiskEditor)
		);

		///////////////////////

		this._SampleDisplays[chem_group][cas_number] = RiskEditor;
		this._SampleSwapSpace.appendPage(RiskEditor);

		let string_append = (chem_group != ChemGroup.none) ? ` (${chem_group})` : "";

		let handle = this._ChemSelect.addOption(
			`${chemical_name}${string_append}`,
			RiskEditor
		);

		handle.Value.OptionHandle = handle;
		handle.Style.color = "red";

		this.DataRepresentativeness =
			Object.values(RiskAssessment.Representativeness)
				.find(item=>assessment && item.score == assessment.prob_rep_of_data_score) ||
			RiskAssessment.Representativeness.Adequate;
		this.RateOfExposure =
			Object.values(RiskAssessment.ExposureRate)
				.find(item=>assessment && item.score == assessment.prob_rate_of_exp_score) ||
			RiskAssessment.ExposureRate.Typical;

		RiskEditor._SampleTimeSelect.Value = assessment ? assessment.custom_exposure_duration : RiskEditor.DefaultSampleTime;

		let def_meg = RiskEditor.DefaultMeg;
		let selected_meg = RiskEditor._MegSelect.Options.find(
			item => assessment && assessment.meg_name && assessment.meg_name == item.parm
		);

		if(!selected_meg)
		{
			selected_meg = RiskEditor._MegSelect.Options.find(
				item => item.parm == def_meg.parm
			);
		}

		if ( selected_meg )
			RiskEditor._MegSelect.Value = selected_meg;
	}

	/**
	 * @param { ChemicalSample[] } samples
	 * @param { MEG[] } megs
	 * @param {Object[]} assessment[] Array of assessments as returned by GET /api/enhancedhra/hra/[ID]/risk
	 * @param {Number} assessment.id
	 * @param {String} assessment.chem_rapid_hra_id
	 * @param {String} assessment.type
	 * @param {String} assessment.chemical_name
	 * @param {String} assessment.cas_number
	 * @param {Number} assessment.meg
	 * @param {String} assessment.meg_name
	 * @param {String} assessment.meg_units
	 * @param {String} assessment.meg_version
	 * @param {String} assessment.severity
	 * @param {String} assessment.prob_degree_of_exp
	 * @param {Number} assessment.prob_degree_of_exp_score
	 * @param {String} assessment.prob_rep_of_data
	 * @param {Number} assessment.prob_rep_of_data_score
	 * @param {String} assessment.prob_duration_of_exp
	 * @param {Number} assessment.prob_duration_of_exp_score
	 * @param {String} assessment.prob_rate_of_exp
	 * @param {Number} assessment.prob_rate_of_exp_score
	 * @param {String} assessment.probability
	 * @param {Number} assessment.probability_score
	 * @param {String} assessment.risk_level
	 * @param {String} assessment.confidence
	 * @param {Number} assessment.custom_exposure_duration
	 * @param {Number} assessment.custom_meg
	 */
	setSamplesAndMegs(samples, megs, assessments)
	{
		this.clear();

		assessments = assessments || [];

		let groups = ChemGroup;

		let grouped_megs = this._MegsForChemical;
		for (let i = 0; i < megs.length; ++i)
		{
			let current_meg = megs[i];

			if ( !grouped_megs[current_meg.cas_number])
				grouped_megs[current_meg.cas_number] = [];

			grouped_megs[current_meg.cas_number].push(current_meg);
		}

		let grouped_samples = this._SamplesForChemical;
		for (let i = 0; i < samples.length; ++i)
		{
			let sample = samples[i];
			let cas_number = sample.cas_number;

			if ( !grouped_samples[cas_number])
				grouped_samples[cas_number] = [];

			grouped_samples[cas_number].push(sample);
		}

		for (const cas_number in grouped_samples)
		{
			let samples = grouped_samples[cas_number];
			let megs = grouped_megs[cas_number];

			let peak_pepc = calculatePepc.getPeakConcentration(samples, megs[0]);
			let avg_pepc = calculatePepc.getAvgConcentration(samples, megs[0]);

			let is_acute = calculatePepc.acutePrescreen(peak_pepc, megs).exceeded;
			let is_chronic = calculatePepc.chronicPrescreen(avg_pepc, megs).exceeded;

			if (is_acute && is_chronic)
			{
				this._createEditor(cas_number, groups.AcutePeak, assessments.find(item=>item.cas_number==cas_number&&item.type=='acute_peak'));
				this._createEditor(cas_number, groups.AcuteAverage, assessments.find(item=>item.cas_number==cas_number&&item.type=='acute_average'));
				this._createEditor(cas_number, groups.ChronicAverage, assessments.find(item=>item.cas_number==cas_number&&item.type=='chronic_average'));
			}
			else if (is_acute)
			{
				this._createEditor(cas_number, groups.AcutePeak, assessments.find(item=>item.cas_number==cas_number&&item.type=='acute_peak'));
				this._createEditor(cas_number, groups.AcuteAverage, assessments.find(item=>item.cas_number==cas_number&&item.type=='acute_average'));
			}
			else if (is_chronic)
			{
				this._createEditor(cas_number, groups.ChronicAverage, assessments.find(item=>item.cas_number==cas_number&&item.type=='chronic_average'));
			}
		}
	}

	/**
	 * @param { RiskAssessment.Representativeness }
	 */
	set DataRepresentativeness(rep)
	{
		this._RepDataSelect.Value = rep;
	}

	get DataRepresentativeness()
	{
		return this._RepDataSelect.Value;
	}

	/**
	 * @param { RiskAssessment.ExposureRate }
	 */
	set RateOfExposure(exp)
	{
		this._RateOfExposureSelect.Value = exp;
	}

	/**
	 * @returns { RiskAssessment.ExposureRate }
	 */
	get RateOfExposure()
	{
		return this._RateOfExposureSelect.Value;
	}

	get AllCalculators()
	{
		let ret = {};

		this._getAllDisplays().forEach(
			(display) =>
			{
				let calc = display.Calculator;

				calc.DataRepresentativeness = this.DataRepresentativeness;
				calc.RateOfExposure = this.RateOfExposure;

				if ( undefined == ret[calc.CasNumber])
					ret[calc.CasNumber] = {};

				ret[calc.CasNumber][calc.Group] = calc;
			}
		);

		SampleCalculator._attachMemberFunctions(ret);
		return ret;
	}

	/**
	 * Gets the index of the selected item.
	 */
	get Index()
	{
		return this._ChemSelect.Index;
	}

	/**
	 * Sets the selection by index and raises a change event.  If idx is out of
	 * range or already selected, nothing will happen.
	 */
	set Index(idx)
	{
		this._ChemSelect.Index = idx;
	}

	get length()
	{
		return this._ChemSelect.length;
	}

	next()
	{
		this._ChemSelect.Index = this._ChemSelect.Index + 1;
	}

	previous()
	{
		this._ChemSelect.Index = this._ChemSelect.Index - 1;
	}
}

customElements.define('jhrm-multiple-chemical-risk-editor', JhrmMultipleChemicalRiskEditor);
