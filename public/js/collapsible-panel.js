import { MaterialIcon } from "./material_icon.js"

export class JhrmCollapsiblePanel extends HTMLElement {

	constructor() 
	{
		super(); 

		let shadowRoot = this.attachShadow({mode: 'open'});
		
		shadowRoot.innerHTML = `
		<link rel="stylesheet" type="text/css" href="./css/material-design-icons.css">
		<style>
			:host {
				display: block;
			}

			#CollapsibleTitle 
			{
				border-bottom:2px solid black;
				display:grid;
				grid-template-columns: min-content auto;
				grid-gap:1em;
			}
			#CollapsibleTitle *
			{
				margin:0;
				padding:0;
				display:inline-block;
			}
			#CollapsibleToggle
			{
				display:flex;
				align-items:center;
			}
			#CollapsibleToggle .material-icons
			{
				transform-origin: 50% 50%; 
				transition: transform 0.25s ease-out;
				
			}
			.open #CollapsibleToggle .material-icons
			{
				transform: rotate(90deg);
			}

			#CollapsibleContent, #CollapsibleLoading
			{
				display : none;
				overflow:auto;                
				padding:1em
			}
			.open.loaded #CollapsibleContent
			{
				display:block;
			}
			.open.loading #CollapsibleLoading 
			{
				display:block;
			}
			#CollapsibleLoading p {
				line-height:44px;
				margin:0;
				padding:0;                
			}
			#CollapsibleLoading .material-icons {
				vertical-align:middle;
				position:relative;
				top:-2px;
				animation: spin 3s linear infinite;
			}
			@keyframes spin { 100% { transform:rotate(-360deg); } }
			@media print {
				#CollapsibleContent
				{
					display:block!important;
				}
				#CollapsibleToggle
				{
					display:none!important;
				}
				#CollapsibleTitle 
				{
					grid-template-columns: auto auto;
				}
				#CollapsibleLoading{
					display:none;
				}
			}
		</style>
		<div id="Collapsible">
			<div id="CollapsibleTitle">
				<div id="CollapsibleToggle"><material-icon icon="play_circle_filled"></material-icon></div><slot name="title"></slot>
			</div>
			<div id="CollapsibleLoading">
				<p><material-icon icon="loop"></material-icon>Loading content</p>
			</div>
			<div id="CollapsibleContent">
				<slot></slot>
			</div>
		</div>
		`;
	}
	
	connectedCallback() 
	{
		this._content = this.shadowRoot.querySelector('#Collapsible');
		this._title = this.shadowRoot.querySelector('#CollapsibleTitle');
		this._boundOnTitleClick = this.toggleOpen.bind(this);
		this._title.addEventListener('click', this._boundOnTitleClick);
		this.open = this.getAttribute('open') == "true";
		this.loading = this.getAttribute('loading') == "true";
	}
	
	disconnectedCallback() 
	{
		this._title.removeEventListener('click', this._boundOnTitleClick);
	}

	setClasses()
	{
		this._content.className = `${this.open ? "open" : "closed"} ${this.loading ? "loading" : "loaded"}`
	}

	get open()
	{
		return !!this._open;
	}

	set open(val)
	{
		this._open = val;
		this.setClasses();
	}

	get loading()
	{
		return !!this._loading;
	}

	set loading(val)
	{
		this._loading = val;
		this.setClasses();
	}

	toggleOpen()
	{
		this.open = !this.open;
		return this.open;
	}
	
  };

  customElements.define('jhrm-collapsible-panel', JhrmCollapsiblePanel);