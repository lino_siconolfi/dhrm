'use strict';

export function FormatDimensionedValue(value, audit)
{
	return audit ?
	    value.history.join("<br>") :
	    parseFloat(value.value.toPrecision(value.precision)).toString() + ' ' + value.units;
}
