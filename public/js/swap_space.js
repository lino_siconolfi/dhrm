import { Positioning } from "./positioning.js"
import { removeElement } from "./utility.js"

export class JhrmSwapSpacePage extends HTMLElement
{
	constructor()
	{
		super();

		this._wrapped = undefined;
		this._built = false;
	}

	connectedCallback()
	{
		if ( !this._built )
		{
			this.style.margin = "0";
			this.style.padding = "0";

			this._built = true;
		}
	}

	wrap(elm)
	{
		this.innerHTML = "";
		this._wrapped = elm;

		this.append(elm);
	}

	get ContainedElement()
	{
		return this._wrapped;
	}
}

customElements.define('jhrm-swap-space-page', JhrmSwapSpacePage);

export class JhrmSwapSpace extends HTMLElement
{
	constructor()
	{
		super();

		this._ChildElements = [];
		this._CurrentIndex = -1;
		this._EnableWrap = true;
		this._SizeToCurrentPage = true;

		this.style.display = "inline-grid";

		let html_children = [];

		if (this.hasAttribute('size-to-visible'))
			this._SizeToCurrentPage = true;

		for(let i = 0; i < this.children.length; ++i)
			html_children.push(this.children.item(i));

		this.innerHTML = "";

		for(let i = 0; i < html_children.length; ++i)
		{
			let element = new JhrmSwapSpacePage();
			element.style.gridRow = "1";
			element.style.gridColumn = "1";

			element.wrap(html_children[i]);
			html_children[i] = element;
		}

		this._ChildElements = [...html_children];

		if ( this._CurrentIndex < 0 && this._ChildElements.length > 0 )
			this._CurrentIndex = 0;
		
		this.append(...this._ChildElements);

		this._updateVisibility();
	}

	_elementVisible(elm, visible)
	{
		if (!elm)
			return;

		if (visible)
		{
			elm.pointerEvents = "auto";
			elm.style.display = "inline-block";
			elm.style.visibility = null;
		}
		else
		{
			elm.pointerEvents = "none";

			if (this._SizeToCurrentPage)
			{
				elm.style.display = "none";
				elm.style.visibility = null;
			}
			else
			{
				elm.style.display = "inline-block";
				elm.style.visibility = "hidden";
			}
		}
	}

	_updateVisibility()
	{
		for(let i = 0; i < this._ChildElements.length; ++i)
			this._elementVisible(this._ChildElements[i], i == this._CurrentIndex);
	}

	_raiseChangeEvent()
	{
		this.dispatchEvent(new CustomEvent('swap', { detail: this }));

		if ( this._on_change )
			this._on_change(this.VisibleElement);
	}

	clear()
	{
		this.innerHTML = "";
		
		this._ChildElements = [];
		this._CurrentIndex = -1;
	}

	/**
	 * Appends a page to the swap display. This will be after any child elements found from inner HTML.
	 * 
	 * @param { HTMLElement } page
	 */
	appendPage(page)
	{
		this.insertAt(page, this._ChildElements.length)
	}

	/**
	 * Adds a page to the swap space at the specified index.
	 * @param {*} page 
	 * @param {*} index 
	 */
	insertAt(page, index)
	{
		let raise_change = (index == this._ChildElements.length);

		if (index > this._ChildElements.length)
			throw new Error("Index out of range.");

		let element = new JhrmSwapSpacePage();
		element.style.gridRow = "1";
		element.style.gridColumn = "1";

		element.wrap(page);
		
		if (this._ChildElements.length <= index)
			this._ChildElements.push(element);
		else
			arr.splice(index, 0, item);

		this.append(element);

		if ( this._CurrentIndex < 0 )
		{
			this._CurrentIndex = 0;
			this._elementVisible(element, true);
		}
		else
		{
			this._elementVisible(element, false);
		}

		if (raise_change)
			this._raiseChangeEvent();
	}

	deleteAt(index)
	{
		if (index >= this._ChildElements.length)
			throw new Error("Index out of range.");

		let elm = this._ChildElements[index];
		this._ChildElements.splice(index, 1);
		removeElement(elm);

		this._CurrentIndex = ( this._CurrentIndex >= this._ChildElements.length ) ?
			this._ChildElements.length - 1 : this._CurrentIndex;

		this._elementVisible(this._ChildElements[this._CurrentIndex], true);
		return elm.ContainedElement;
	}

	next()
	{
		if (this._ChildElements.length <= 0)
			return;

		let nextIndex = (this._CurrentIndex + 1) % this._ChildElements.length;

		if (nextIndex < this._CurrentIndex && !this._EnableWrap)
			return;

		this.VisibleIndex = nextIndex;
	}

	previous()
	{
		if (this._ChildElements.length <= 0)
			return;
		
		let nextIndex = this._CurrentIndex - 1;

		if (nextIndex < 0)
			nextIndex = this._ChildElements.length - 1;
		
		if (nextIndex > this._CurrentIndex && !this._EnableWrap)
			return;

		this.VisibleIndex = nextIndex;
	}

	indexOf(elm)
	{
		for (let i = 0; i < this._ChildElements.length; ++i)
		{
			if ( elm == this._ChildElements[i].ContainedElement )
				return i;
		}

		return undefined;
	}

	elementAt(index)
	{
		if (index >= 0 && index < this._ChildElements.length)
			return this._ChildElements[index].ContainedElement;

		return undefined;
	}

	/**
	 * @brief
	 * 	Sets the index of the visible page. If EnableWrapAround any positive value will "wrap".  Otherwise
	 *  this must be a in the range of [0, Count - 1].  This is not valid if there are no pages to display.
	 */
	set VisibleIndex(vIndex)
	{
		if ( isNaN(vIndex) || vIndex < 0 || (vIndex >= this._ChildElements.length && !this._EnableWrap))
			throw new Error("Index out of range.");

		if (vIndex != this._CurrentIndex)
		{
			if (vIndex >= this._ChildElements.length)
				vIndex = vIndex % this._ChildElements.length;

			let currElement = this._ChildElements[this._CurrentIndex];
			let nextElement = this._ChildElements[vIndex];

			this._elementVisible(currElement, false);
			this._elementVisible(nextElement, true);

			this._CurrentIndex = vIndex;
			this._raiseChangeEvent();
		}
	}

	/**
	 * @brief
	 *  The index of the currently visible element.
	 */
	get VisibleIndex()
	{
		return this._CurrentIndex;
	}

	set VisibleElement(vElm)
	{
		for(let i = 0; i < this._ChildElements.length; ++i)
		{
			if (this._ChildElements[i].ContainedElement == vElm)
			{
				this.VisibleIndex = i;
				return;
			}
		}

		throw new Error("Element not contained in swap space.");
	}

	get VisibleElement()
	{
		if (this._ChildElements.length <= 0)
			return undefined;

		return this._ChildElements[this._CurrentIndex].ContainedElement;
	}

	get NextElement()
	{
		if (this._ChildElements.length <= 0)
			return undefined;

		let nextIndex = this._CurrentIndex + 1;
		
		if (this._ChildElements.length == nextIndex)
		{
			if (this._EnableWrap)
				nextIndex = 0;
			else
				return undefined;
		}
		
		return this._ChildElements[nextIndex].ContainedElement;
	}

	get PreviousElement()
	{
		if (this._ChildElements.length <= 0)
			return undefined;

		let nextIndex = 0;

		if ( 0 == this._CurrentIndex)
		{
			if (this._EnableWrap)
				nextIndex = this._ChildElements.length - 1;
			else
				return undefined;
		}
		else
		{
			nextIndex = this._CurrentIndex - 1;
		}

		return this._ChildElements[nextIndex].ContainedElement;
	}

	get isOnFirstElement()
	{
		return (this._CurrentIndex <= 0);
	}

	get isOnLastElement()
	{
		return (this._CurrentIndex == this._ChildElements.length - 1);
	}

	get ElementCount()
	{
		return this._ChildElements.length;
	}

	set EnableWrapAround(vWrap)
	{
		this._EnableWrap = vWrap;
	}

	set SizeToCurrentPage(val)
	{
		if (val != this._SizeToCurrentPage)
		{
			this._SizeToCurrentPage = val;
			this._updateVisibility();
		}
	}

	get AllPages()
	{
		let ret = [];

		for (let i = 0; i < this._ChildElements.length; ++i)
			ret.push(this._ChildElements[i].ContainedElement);

		return ret;
	}

	get Count()
	{
		return this._ChildElements.length;
	}

	/**
	 * Sets a change handler
	 */
	set onchange(func)
	{
		this._on_change = func;
	}
}

customElements.define('jhrm-swap-space', JhrmSwapSpace);