import { JhrmTooltip } from "./tooltip.js"
import { MaterialIcon } from "./material_icon.js"

export class JhrmHelpIcon extends HTMLElement
{
	constructor()
	{
		super();

		this._built = false;
	}

	connectedCallback()
	{
		if (!this._built)
		{
			let icon = MaterialIcon.Create("help_outline");
			icon.style.fontSize = "0.9em";
		
			let tooltip = new JhrmTooltip();
			tooltip.Target = icon;
			
			let Children = [];
			if ( this.childNodes.length > 0 )
			{
				for (let i = 0; i < this.childNodes.length; ++i)
					Children.push(this.childNodes.item(i));
			}

			tooltip.append(...Children);
			tooltip.style.fontSize = "1rem";

			this.innerHTML = "";
			this.append(icon, tooltip);

			this._built = true;
		}
	}
}


customElements.define('jhrm-help-icon', JhrmHelpIcon);