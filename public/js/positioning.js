export class Positioning
{
	static pxToInt(stringPixels)
	{
		let numString = stringPixels.substring(0, stringPixels.length - 2);
		return parseInt(numString);
	};

	/**
	 * Creates an element to contain the passed element as its center.
	 * 
	 * @param {*} elm Element that will be centered in the returned element.
	 */
	static center(elm)
	{
		let elmGrid = document.createElement('div');
		elmGrid.style.display = "grid";
		elmGrid.style.gridTemplateColumns = "1fr auto 1fr";
		elmGrid.style.gridTemplateRows = "1fr auto 1fr";
		elmGrid.style.width = "100%";
		elmGrid.style.height = "100%";
		elmGrid.style.border = "0";
		elmGrid.style.padding = "0";

		elm.style.gridRow = "2";
		elm.style.gridColumn = "2";

		elmGrid.append(elm);
		return elmGrid;
	};

	/**
	 * Creates an element to contain the passed element as its vertical center.
	 * 
	 * @param {*} elm Element that will be centered vertically in the returned element.
	 */
	static vCenter(elm)
	{
		let elmGrid = document.createElement('div');
		elmGrid.style.display = "grid";
		elmGrid.style.gridTemplateRows = "1fr auto 1fr";
		elmGrid.style.width = "100%";
		elmGrid.style.height = "100%";
		elmGrid.style.border = "0";
		elmGrid.style.padding = "0";

		elm.style.gridRow = "2";

		elmGrid.append(elm);
		return elmGrid;
	};

	/**
	 * Gets the element's position relative to the document.  The return box will include
	 * margins, borders, and padding.
	 * 
	 * @param {*} elm 
	 */
	static documentPosition(elm)
	{
		let pxToInt = Positioning.pxToInt;

		let Style = getComputedStyle(elm);
		let rect = elm.getBoundingClientRect();

		let marginTop = pxToInt(Style.marginTop);
		let marginBottom = pxToInt(Style.marginBottom);
		let marginLeft = pxToInt(Style.marginLeft);
		let marginRight = pxToInt(Style.marginRight);

		let rTop = rect.top + window.scrollY - marginTop;
		let rBottom = rect.bottom + window.scrollY + marginBottom;
		let rLeft = rect.left + window.scrollX - marginLeft;
		let rRight = rect.right + window.scrollX + marginRight;

		return {
			top : rTop,
			left : rLeft,
			bottom : rBottom,
			right : rRight,
			width : rect.width + marginLeft + marginRight,
			height : rect.height + marginTop + marginBottom
		};
	}

	/**
	 * Gets the element's position relative to its parent element.
	 * 
	 * @param {*} elm 
	 */
	static relativePosition(elm, rootElm)
	{
		let documentPosition = Positioning.documentPosition;

		let parentRelative = undefined;
		
		if (rootElm == undefined)
			parentRelative = documentPosition(elm.parentElement);
		else
			parentRelative = documentPosition(rootElm);
		
		let elmRelative = documentPosition(elm);

		let rTop = elmRelative.top - parentRelative.top;
		let rBottom = elmRelative.bottom - parentRelative.bottom;
		let rLeft = elmRelative.left - parentRelative.left;
		let rRight = elmRelative.right - parentRelative.right;

		return {
			top : rTop,
			left : rLeft,
			bottom : rBottom,
			right : rRight,
			width : elmRelative.width,
			height : elmRelative.height
		};
	}

	static boxes(elm)
	{
		let pxToInt = Positioning.pxToInt;

		let nativeRec = elm.getBoundingClientRect();
		let Style = getComputedStyle(elm);
		
		let marginTop = pxToInt(Style.marginTop);
		let marginBottom = pxToInt(Style.marginBottom);
		let marginLeft = pxToInt(Style.marginLeft);
		let marginRight = pxToInt(Style.marginRight);

		let paddingTop = pxToInt(Style.paddingTop);
		let paddingBottom = pxToInt(Style.paddingBottom);
		let paddingLeft = pxToInt(Style.paddingLeft);
		let paddingRight = pxToInt(Style.paddingRight);

		let borderTop = pxToInt(Style.borderTopWidth);
		let borderBottom = pxToInt(Style.borderBottomWidth);
		let borderLeft = pxToInt(Style.borderLeftWidth);
		let borderRight = pxToInt(Style.borderRightWidth);

		let borderRect = {
			top : nativeRec.top + window.scrollY,
			bottom : nativeRec.bottom + window.scrollY,
			left : nativeRec.left + window.scrollX,
			right : nativeRec.right + window.scrollX
		};

		let paddingRect = {
			top : borderRect.top + borderTop,
			bottom : borderRect.bottom - borderBottom,
			left : borderRect.left + borderLeft,
			right : borderRect.right - borderRight
		};

		let contentRect = {
			top : paddingRect.top + paddingTop,
			bottom : paddingRect.bottom - paddingBottom,
			left : paddingRect.left + paddingLeft,
			right : paddingRect.right - paddingRight
		};

		let marginRect = {
			left : borderRect.left - marginLeft,
			right : borderRect.right + marginRight,
			bottom : borderRect.bottom + marginBottom,
			top : borderRect.top - marginTop
		};

		borderRect.width = borderRect.right - borderRect.left;
		paddingRect.width = paddingRect.right - paddingRect.left;
		contentRect.width = contentRect.right - contentRect.left;
		marginRect.width = contentRect.right - marginRect.left;
		
		borderRect.height = borderRect.bottom - borderRect.top;
		paddingRect.height = paddingRect.bottom - paddingRect.top;
		contentRect.height = contentRect.bottom - contentRect.top;
		marginRect.height = contentRect.bottom - marginRect.top;

		return {
			margin : marginRect,
			border : borderRect,
			padding : paddingRect,
			content : contentRect
		};
	}
}