import { MaterialIcon } from "./material_icon.js"

const sortedClass = "sorted";
const ascendingClass="ascending";
const descendingClass="descending";


/**
 * This is a mixin that adds sortable behavior to HTML tables. 
 * 
 * @param {*} table 
 */
export function Sortable(table)
{
	table.querySelector('thead').style.cursor="pointer";
	table.className += " sortable"; 

	table.addEventListener('jhrmContentUpdated', function(e){
		let cols = table.querySelector('thead').childNodes;

		// Figure out the index of the column to sort by
		cols.forEach((col, idx)=>{
			let classes = col.className.split(' ');
			if(classes.includes(sortedClass))
			{
				doSort(table, idx, classes.includes(ascendingClass) ? ascendingClass : descendingClass);
			}
		});
	});

	table.addEventListener('click', function(e){
		let element = e.target;

		// This was a click on the header
		if(element.nodeName=='TH')
		{
			let row = element.parentNode;
			let cols = row.querySelectorAll('th');
			let direction = ascendingClass;
			let index = 0;

			// Figure out the index of the column to sort by
			cols.forEach((col, idx)=>{
				if(col == element)
				{
					// This is the column index. Set direction. 
					direction = getDirection(col);
					setSortClasses(col, [sortedClass, direction]);
					index = idx;
				}
				else
				{
					// Remove all sort classes
					setSortClasses(col);
				}
			});

			// Do Sort
			doSort(table, index, direction);
		}
	});
}

function doSort(table, index, direction)
{
	let body = table.tBodies[0];

	// Get all rows 
	let rows = [].slice.call(body.rows).filter(row=>!row.classList.contains('nosort'));

	// Sort rows.
	rows.sort((a, b)=>{
		let items = [];
		if(a.cells[index].querySelector("input[type=checkbox"))
		{
			items.push(a.cells[index].querySelector("input[type=checkbox").checked ? 0 : 1);
		}
		else 
		{
			items.push(a.cells[index].innerText);
		}
		if(b.cells[index].querySelector("input[type=checkbox"))
		{
		    items.push(b.cells[index].querySelector("input[type=checkbox").checked ? 0 : 1);
		}
		else 
		{
		    items.push(b.cells[index].innerText);
		}
		if(direction==descendingClass)
		{
			items.reverse();
		}
		return isNaN(items[0] - items[1]) ? items[0].toString().localeCompare(items[1].toString()) : items[0] - items[1];
	});

	while (body.lastElementChild) 
	{
		body.removeChild(body.lastElementChild);
	}

	rows.forEach(row=>body.appendChild(row));
}

function getDirection(element)
{
	let classes = element.className.split(' ');
	if(!classes.includes(sortedClass) || classes.includes(descendingClass))
	{
		return ascendingClass;
	}
	return descendingClass;
}

function setSortClasses(element, classes)
{
	let allClasses=[sortedClass, ascendingClass, descendingClass];
	element.className = element.className
		.split(' ')
		.filter(el=>!allClasses.includes(el))
		.concat(classes)
		.join(' ');
}