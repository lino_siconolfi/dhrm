export class ChemicalSample
{
	constructor()
	{
		// Known empty function
	}

	static fromMiddleTier(obj, sample)
	{
		let ret = new ChemicalSample();
		Object.assign(ret, obj);
		ret.sample = sample;

		ret.exceeds_1_year_neg = (ret.exceeds_1_year_neg.toLowerCase() == 'yes');

		return ret;
	}

	/**
	 * @param { ChemicalSample } sample
	 */
	static toMiddleTier(sample)
	{
		let ret = {};
		Object.assign(ret, sample);

		ret.exceeds_1_year_neg = (ret.exceeds_1_year_neg ? 'Yes' : 'No');

		return ret;
	}
}
