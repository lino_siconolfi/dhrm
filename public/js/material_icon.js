let materialLink = document.createElement('link');
materialLink.rel = 'stylesheet';
materialLink.href = "./css/material-design-icons.css";

document.head.appendChild(materialLink);


export class MaterialIcon extends HTMLElement
{
	constructor()
	{
		super();

		this._Built = false;
		this._icon = undefined;
	}

	connectedCallback()
	{
		if ( !this._Built )
		{
			let iElm = document.createElement('i');
			
			iElm.className = "material-icons";
			iElm.style.fontSize = "inherit";
			iElm.innerHTML = (undefined == this._icon) ?
				 this.getAttribute("icon") : this._icon;

			this.append(iElm);

			this._Built = true;
		}
	}

	static Create(iconName)
	{
		let ret = new MaterialIcon();
		ret._icon = iconName;

		return ret;
	}
}

customElements.define('material-icon', MaterialIcon);