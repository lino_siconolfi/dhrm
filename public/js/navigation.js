import { JhrmSwapSpace } from "./swap_space.js"
import { JhrmAlign } from "./align.js"
import { QANotification } from './Enhanced_HRA/qa_notification.js'
import { BlastQANotification } from './blast/qa_notification.js'

let style_for_level_1 = function(elm)
{
	elm.style.display = "inline-block";
	elm.style.fontSize = "1.2rem";
	elm.style.fontWeight = "bold";
	elm.style.padding = "0.5rem";
	elm.style.whiteSpace = "nowrap";
}

let style_for_level_2 = function(elm)
{
	elm.style.display = "inline-block";
	elm.style.fontSize = "0.9rem";
	elm.style.fontWeight = "bold";
	elm.style.padding = "0.5rem 0.5rem 0.5rem 2rem";
	elm.style.whiteSpace = "nowrap";
}

const background_color = "#343434";
const active_background_color = "#606060";
const active_highlight_color = "#d3c952";

export class JhrmNavLink extends HTMLElement
{
	constructor()
	{
		super();

		this._built = false;

		this._elmLeft = document.createElement('div');
		this._elmLeft.style.gridColumn = "1";
		this._elmLeft.style.height = "auto";

		this._elmText = document.createElement('div');
		
		this._elmTextContainer = new JhrmAlign();
		this._elmTextContainer.Alignment = JhrmAlign.Middle;
		this._elmTextContainer.Alignment = JhrmAlign.Left;
		this._elmTextContainer.style.gridColumn = "2";
		this._elmTextContainer.append(this._elmText);


		this._active = false;
		this._href = undefined;
		this._new_window = false;
		this._level = 1;
		this._section = "";
	}

	connectedCallback()
	{
		if (!this._built)
		{
			this.style.display = "grid";
			this.style.gridTemplateColumns = "0.5rem 1fr";

			this.append(this._elmLeft, this._elmTextContainer);

			this.onclick = function()
			{
				if (this._href)
				{
					if (this._new_window)
						window.open(this._href);
					else
						document.location.replace(this._href);
				}
			}.bind(this);

			this._built = true;

			if (this.hasAttribute('href'))
				this.HREF = this.getAttribute('href');

			if (this.hasAttribute('new-window'))
				this._new_window  = true;

			if (this.hasAttribute('text'))
				this._elmText.innerText = this.getAttribute('text');

			if (this.hasAttribute('section'))
				this._section = this.getAttribute('section');

			if (this.hasAttribute('level'))
				this.OutlineLevel = Number.parseInt(this.getAttribute('level'));
			if (this.hasAttribute('qaNotif')) {
				this._elmText.append(document.createElement("qa-notification"));
				this._elmText.style.display = "flex";
				this._elmText.style.alignItems = "center";
			}
			else
				this.OutlineLevel = this._level;

			if (this.hasAttribute('blastQANotif')) {
				this._elmText.append(document.createElement("blast-qa-notification"));
				this._elmText.style.display = "flex";
				this._elmText.style.alignItems = "center";
			}

			this._updateStyles();
		}
	}

	_updateStyles()
	{
		if (this._built)
		{
			this._elmLeft.style.backgroundColor = this._active ? active_highlight_color : background_color;
			this._elmTextContainer.style.backgroundColor = this._active ? active_background_color : background_color;
			this.style.cursor = this._active ? null : "pointer";
		}
	}

	get Active()
	{
		return this._active;
	}

	set Active(val)
	{
		this._active = !!val;
		this._updateStyles();
	}

	set Section(val)
	{
		this._section = val;
	}
	get Section()
	{
		return this._section;
	}

	set HREF(val)
	{
		this._href = val;
		this._active = (window.location.pathname.endsWith(this._href));

		this._updateStyles();
	}

	get HREF()
	{
		return this._href;
	}

	set NewWindow(val)
	{
		this._new_window = val;
	}

	get NewWindow()
	{
		return this._new_window;
	}

	set Text(val)
	{
		this._elmText.innerHTML = val;
	}

	get Text()
	{
		return this._elmText.textContent;
	}

	set OutlineLevel(val)
	{
		this._level = val;

		if (val == 1)
			style_for_level_1(this._elmText);
		else if (val == 2)
			style_for_level_2(this._elmText);
	}
}

customElements.define('jhrm-nav-link', JhrmNavLink);

export class JhrmNavDrawer extends HTMLElement
{
	constructor()
	{
		super();

		this._built = false;

		this._elmTitle = document.createElement('div');
		style_for_level_1(this._elmTitle);

		this._elmExpandedIndicater = document.createElement('div');
		this._elmExpandedIndicater.style.padding = "0.5em";
		this._elmExpandedIndicater.style.fontSize = "1rem";

		this._elmContent = document.createElement('div');

		this._expanded = false;
	}

	connectedCallback()
	{
		if (!this._built)
		{
			this.style.cursor = "pointer";
			this.style.display = "flex";
			this.style.flexDirection = "column";

			let TitleContainer = new JhrmAlign();
			TitleContainer.style.gridRow = "1";
			TitleContainer.style.gridColumn = "1";
			TitleContainer.Alignment = JhrmAlign.Middle;
			TitleContainer.Alignment = JhrmAlign.Left;
			TitleContainer.append(this._elmTitle);

			let ExpandIndContainer = new JhrmAlign();
			ExpandIndContainer.style.gridRow = "1";
			ExpandIndContainer.style.gridColumn = "2";
			ExpandIndContainer.Alignment = JhrmAlign.Middle;
			ExpandIndContainer.Alignment = JhrmAlign.Center;
			ExpandIndContainer.append(this._elmExpandedIndicater);

			let Drawer = document.createElement('div');
			Drawer.style.display = "grid";
			Drawer.style.gridTemplateColumns = "1fr auto";
			Drawer.onclick = this.toggle.bind(this);

			Drawer.append(TitleContainer, ExpandIndContainer);

			let Active = false;

			let Children = [];
			for (let i = 0; i < this.children.length; ++i)
			{
				if (this.children[i].tagName == "JHRM-NAV-LINK")
				{
					Active |= this.children[i].Active;
					Children.push(this.children[i]);
				}
			}
			
			this.innerHTML = "";

			this._elmContent.style.display = "flex";
			this._elmContent.style.flexDirection = "column";
			this._elmContent.append(...Children);

			this.append(Drawer, this._elmContent);

			if (this.hasAttribute('title'))
				this.Title = this.getAttribute('title');

			for (let i = 0; i < Children.length; ++i)
			{
				Active |= Children[i].Active;
			}

			this.Expanded = this._expanded | Active;

			this._built = true;
		}
	}

	set Title(val)
	{
		this._elmTitle.innerText = val;
	}

	get Title()
	{
		return this._elmTitle.textContent;
	}

	set Expanded(val)
	{
		this._expanded = val;
		this._elmExpandedIndicater.innerHTML = this._expanded ? '&#x25bc;' : '&#x25c0;';
		this._elmContent.style.maxHeight = this._expanded ? null : "0";
		this._elmContent.style.overflowY = this._expanded ? null : "hidden";
	}

	get Expanded()
	{
		return this._expanded;
	}

	toggle()
	{
		this.Expanded = !this.Expanded;
	}
}

customElements.define('jhrm-nav-drawer', JhrmNavDrawer);

export class JhrmNavigation extends HTMLElement
{
	constructor()
	{
		super();

		this.style.color = "white";
		this.style.backgroundColor = background_color;
		this.innerHTML = `
		<jhrm-swap-space size-to-visible>
			<div id="nav_page" style="display: grid; grid-template-rows: auto 1fr auto; height: 100%; min-height: min-content; padding: 0;">
				<jhrm-nav-link style="grid-row: 1; border-bottom: 1px solid white;" href="welcome.html" text="Home"></jhrm-nav-link>
				<div id="menu_area" style="grid-row: 2;">
					<jhrm-nav-drawer title="Tools">
						<jhrm-nav-link section="sshra" level="2" href="enhanced_hra.html" text="EH Sample Chemical Exposure&#10;Health Risk Assessment" qaNotif="true"></jhrm-nav-link>
						<jhrm-nav-link section="rhra" level="2" href="rapid_hra.html" text="Direct Reading Instrument&#10;Chemical Exposure HRA"></jhrm-nav-link>
						<jhrm-nav-link section="issuance" level="2" href="issuance_tool.html" text="Issuance Tool"></jhrm-nav-link>
						<jhrm-nav-link section="rad" level="2" href="rad.html" text="Radiation Exposure HRA"></jhrm-nav-link>
						<jhrm-nav-link section="blast" level="2" href="blast.html" text="Blast Overpressure&#10;Exposure IM/IT" blastQANotif="true"></jhrm-nav-link>
						<jhrm-nav-link section="tic" level="2" href="tic_exposure.html" text="Toxic Industrial Chemical (TIC)&#10;Exposure Estimation"></jhrm-nav-link>
						<jhrm-nav-link section="ifa" level="2" href="JhrmEhIncInitFieldAcct.html" text="JHRM EH Incident Initial&#10;Field Account"></jhrm-nav-link>
					</jhrm-nav-drawer>
					<jhrm-nav-drawer title="Reports">
						<jhrm-nav-link section="" level="2" href="individual_radiation.html" text="Individual Exposures"></jhrm-nav-link>
						<jhrm-nav-link section="" level="2" href="field.html" text="Field Observations"></jhrm-nav-link>
					</jhrm-nav-drawer>
					<jhrm-nav-drawer title="Set Up Tools">
						<jhrm-nav-link level="2" href="multiraeingestion.html" text="MultiRAE Ingestion"></jhrm-nav-link>
						<jhrm-nav-link level="2" href="personal_metadata.html" text="Collector Information"></jhrm-nav-link>
						<jhrm-nav-link section="meta" level="2" href="metadata_summary.html" text="Create/Modify new Sensor/Array"></jhrm-nav-link>
						<jhrm-nav-link level="2" href="mhce.html" text="Blast Gauge Data Export"></jhrm-nav-link>
						<jhrm-nav-link level="2" href="mhce-conquer.html" text="CONQUER Blast Gauge Data Export"></jhrm-nav-link>
					</jhrm-nav-drawer>
					<jhrm-nav-drawer title="References">
						<jhrm-nav-link section="" level="2" href="TG230-DeploymentEHRA-and-MEGs-2013-Revision.pdf" text="TG230 Document" new-window></jhrm-nav-link>
						<jhrm-nav-link section="" level="2" href="rd230_meg.html" text="RD230/Health Outcome Search" new-window></jhrm-nav-link>
						<jhrm-nav-link section="" level="2" href="NIOSH.pdf" text="NIOSH Pocket Guide to Chemical Hazards" new-window></jhrm-nav-link>
						<jhrm-nav-link section="" level="2" href="ChemExpHraFromDri.html" text="Chemical Exposure HRA from DRI"></jhrm-nav-link>
						<jhrm-nav-link section="" level="2" href="TG230TrainingWhatisaMEGv3.pdf" text="What is a MEG?" new-window></jhrm-nav-link>
						<jhrm-nav-link section="" level="2" href="TG230TrainingAirSampleRiskAssessmentv2.pdf" text="Air Risk Assessment Methodology" new-window></jhrm-nav-link>
						<jhrm-nav-link section="" level="2" href="20201130JHRM101BriefforOpDemo.pdf" text="JHRM-ECD Demo Brief" new-window></jhrm-nav-link>
						<jhrm-nav-link section="" level="2" href="QuickStartGuideEHSampleCHRATool_v2.pdf" text="Quick Start Guide - EH Sample Chemical HRA Tool" new-window></jhrm-nav-link>
						<jhrm-nav-link section="" level="2" href="Quick_Start_Guide_DRI_CHRA_Tool.pdf" text="Quick Start Guide - DRI Chemical HRA Tool" new-window></jhrm-nav-link>
						<jhrm-nav-link section="" level="2" href="Quick_Start_Guide_IFA_Portal.pdf" text="Quick Start Guide - IFA Tool" new-window></jhrm-nav-link>
						<jhrm-nav-link section="" level="2" href="IssuanceToolQuickStartGuide.pdf" text="Quick Start Guide - Issuance Tool" new-window></jhrm-nav-link>
						<jhrm-nav-link section="" level="2" href="Quick_Start_Guide_BOP_Exposure_Data_QA_v1.pdf" text="Quick Start Guide - Blast Overpressure Exposure Data QA" new-window></jhrm-nav-link>
					</jhrm-nav-drawer>
					<jhrm-nav-drawer title="External">
						<jhrm-nav-link level="2" href="https://www.atsdr.cdc.gov/toxfaqs/index.asp" text="Tox FAQs" new-window></jhrm-nav-link>
					</jhrm-nav-drawer>
				</div>
				<div id="collapse_button" style="grid-row: 3; display: block; cursor: pointer; border-top: 1px solid white; padding: 1.5rem 1rem;"><span style="font-size: 1rem;">&#x25c0;</span> Collapse Menu</div>
			</div>
			<jhrm-align middle id="hide_bar" style="dsiplay: block; cursor: pointer; height: 100%; min-height: min-content; padding: 1rem;">&#x25b6;</jhrm-align>
		</jhrm-swap-space>
		`;
		this._SwapElement = this.querySelector('jhrm-swap-space');
		this._SwapElement.style.width = "100%";
		this._SwapElement.style.minWidth = "min-content";
		this._SwapElement.style.height = "100%";
		this._SwapElement.style.minHeight = "min-content";
		this._SwapElement.style.color = "white";
		this._SwapElement.style.backgroundColor = background_color;
		this._SwapElement.style.margin = "0";
		this._SwapElement.style.padding = "0";
		this._hide_bar = this._SwapElement.querySelector(`#hide_bar`);
		this._nav_page = this._SwapElement.querySelector(`#nav_page`);
		this._collapse_button = this._SwapElement.querySelector(`#collapse_button`);

		style_for_level_1(this._collapse_button);

		this.Initialized = false;
	}

	connectedCallback()
	{
		if (false == this.Initialized)
		{
			this._collapse_button.onclick = function()
			{
				localStorage.setItem('JhrmSidebarState', JhrmNavigation.STATE.CLOSED);
				this._SwapElement.VisibleElement = this._hide_bar;
			}.bind(this);

			this._hide_bar.onclick = function()
			{
				localStorage.setItem('JhrmSidebarState', JhrmNavigation.STATE.OPEN);
				this._SwapElement.VisibleElement = this._nav_page;
			}.bind(this);

			let state = localStorage.getItem('JhrmSidebarState');
			if(state === JhrmNavigation.STATE.CLOSED)
			{
				this._SwapElement.VisibleElement = this._hide_bar;
			}

			this.Initialized = true;
			this.links = [...this.querySelectorAll('jhrm-nav-link')];
		}
		let section = this.getAttribute('section');
		if(section)
		{
			this.links.forEach(link=>{
				if(link.Section==section)
				{
					link.Active = true;
					link.parentElement.parentElement.Expanded = true;
				}
			})
		}
	}
}
JhrmNavigation.STATE = {
	OPEN : 'OPEN',
	CLOSED : 'CLOSED'
}
customElements.define('jhrm-navigation', JhrmNavigation);
