import { EXPOSURE_DURATION } from "/js/shared/Constants.js"
import { JhrmRadioGroup } from "./radio_group.js"
import {selectAndStripId } from "./utility.js"

export class JhrmHraDurationSelector extends HTMLElement
{
	constructor()
	{
		super();

		this._radio_group = new JhrmRadioGroup();

		let addRadio = this._radio_group.genElementText.bind(this._radio_group);
		this._DurationElements = {};


		this.innerHTML = `
			<table style="margin:0 auto">
				<thead>
					<tr>
					</tr>
				</thead>
				<tbody>
					<tr>
					</tr>
				</tbody>
			</table>
		`;

		let thead = this.querySelector("thead tr");
		let tbody = this.querySelector("tbody tr");
		for (const key in EXPOSURE_DURATION)
		{
			try
			{
				let current_exp = EXPOSURE_DURATION[key];
				let current_exp_string = EXPOSURE_DURATION.toString(current_exp);
				let els = this._DurationElements[current_exp] = {
					th : document.createElement('th'),
					td : document.createElement('td')
				};
				els.th.innerHTML = current_exp_string;
				els.td.innerHTML = addRadio(current_exp);
				thead.appendChild(els.th);
				tbody.appendChild(els.td);
			}
			catch ( err )
			{
				// do nothing
			}
		}
		let els = this._DurationElements[JhrmHraDurationSelector.OTHER] = {
			th : document.createElement('th'),
			td : document.createElement('td')
		};
		els.th.innerHTML = JhrmHraDurationSelector.OTHER;
		els.td.innerHTML = addRadio(JhrmHraDurationSelector.OTHER) + ` (<input type="number" value="12" min="1" max="36" style="width:3em"> months)`;
		thead.appendChild(els.th);
		tbody.appendChild(els.td);
		

		this._onchange = undefined;
		this._number_input = this._DurationElements[JhrmHraDurationSelector.OTHER].td.querySelector("input[type=number]");
		this._number_input.onchange = (evt)=>{
			if(this._number_input.value < 1)
			{
				this._number_input.value = 1;
			}
			this._RaiseChangeEvent(evt)
		};
		this._radio_group.onchange = 
			this._RaiseChangeEvent.bind(this);
			
		this._Built = false;
	}

	connectedCallback()
	{
		if (!this._Built)
		{
			if ( this.hasAttribute('acute') )
			{
				this.setDurationVisible(EXPOSURE_DURATION.YEAR_1, false);
				this.setDurationVisible(EXPOSURE_DURATION.DAYS_7, false);
				this.setDurationVisible(JhrmHraDurationSelector.OTHER, false);
			}

			this._Built = true;
		}
	}

	_RaiseChangeEvent(e)
	{
		// Do not allow events to bubble out of this component. 
		try{e.stopPropagation();}catch(e){};
		
		let val = this._radio_group.Value;

		if(val == JhrmHraDurationSelector.OTHER)
		{
			// Because "months" are not equal, we treat them as 1/12 of a year for the purposes of the 
			// calculation. 
			val = this._number_input.value * (EXPOSURE_DURATION.YEAR_1 / 12);
		}

		let event = new CustomEvent(
			"change",
			{
				detail : val
			}
		);

		this.dispatchEvent(event);

		if (this._onchange)
			this._onchange(this, val);
	}

	/**
	 * @param { int }
	 * 
	 * We want to set the nearest visible value, or Other and then also set the
	 * number of months. 
	 */
	set Value(val)
	{
		let other = this._DurationElements[JhrmHraDurationSelector.OTHER].td.style.display!='none';

		if(other)
		{
			if(this._DurationElements[val])
			{
				this._radio_group.Value = val;
			}
			else 
			{
				this._radio_group.Value = JhrmHraDurationSelector.OTHER;
				this._number_input.value = Math.ceil(val/(EXPOSURE_DURATION.YEAR_1 / 12))
			}
		}
		else 
		{
			let closest = undefined;
			for(const key in this._DurationElements)
			{
				if(this._DurationElements[key].td.style.display!='none')
				{
					if(!closest || Math.abs(val-closest) > Math.abs(val-key))
					{
						closest = key;
					}
				}
			}
			this._radio_group.Value = closest;
		}
	}

	/**
	 * @returns { EXPOSURE_DURATION }
	 */
	get Value()
	{
		return this._radio_group.Value;
	}

	/**
	 * The handler will be called with the first parameter being this duration selector, and
	 * the second parameter being the new value.
	 */
	set onchange(handler)
	{
		this._onchange = handler;
	}

	/**
	 * Set the visibility of heading, table data, and radio select elements for a duration. By default, all
	 * are visibile.  If a selected element is hidden, it does not automatically deselect the value.  It is
	 * up to client code to use Value getters and setters to handle that possibility.
	 * 
	 * @param { EXPOSURE_DURATION } dur - The duration for which selection elements should be set.
	 * @param { boolean } visible - True if the elements for the duration should be shwon, false otherwise.
	 */
	setDurationVisible(dur, visible)
	{
		let disp = visible ? "table-cell" : "none";

		this._DurationElements[dur].th.style.display = disp;
		this._DurationElements[dur].td.style.display = disp;
	}

	/**
	 * @returns {EXPOSURE_DURATION[]} Array of visible exposure durations
	 */
	get Visible()
	{
		return Object.keys(this._DurationElements).filter(key=>this._DurationElements[key].td.style.display!='none')
	}
}
JhrmHraDurationSelector.OTHER = "OTHER";

customElements.define('jhrm-hra-duration-selector', JhrmHraDurationSelector);