import { Positioning } from "./positioning.js"

export class JhrmTooltip extends HTMLElement
{
	constructor()
	{
		super();

		this._built = false;

		this._visible = false;
		this._target = undefined;
	}

	connectedCallback()
	{
		if (!this._built)
		{
			this.style.position = "absolute";
			this.style.boxSizing = "border-box";
			this.style.visibility = "hidden";
			this.style.display = "block";
			this.style.zIndex = "999";
			this.style.width = "auto";
			this.style.minWidth = "min-content";
			this.style.fontSize = "1rem";

			this._onEnter = function ()
			{
				this.style.left = "0px";
				this.style.top = "0px";
	
				let tipPos = Positioning.documentPosition(this);
				let elmPos = Positioning.documentPosition(this._target);
				let relPosition = Positioning.relativePosition(this, this._target);
	
				this.style.top = (-relPosition.top + elmPos.height + 3).toString() + "px";
				if(elmPos.right > window.innerWidth / 2)
				{
					this.style.left = (-relPosition.left - tipPos.width - 3).toString() + "px";
				}
				else 
				{
					this.style.left = (-relPosition.left + 3).toString() + "px";
				}
	
				this.style.visibility = "inherit";
			}.bind(this);

			this._onLeave = function ()
			{
				this.style.visibility = "hidden";
			}.bind(this);

			this._built = true;

			if ( this._target == undefined )
			{
				this.Target = document.getElementById(this.getAttribute('target'));
			}
			else
			{
				this._target.addEventListener('mouseenter', this._onEnter);
				this._target.addEventListener('mouseleave', this._onLeave);
			}
		}
	}

	get Target()
	{
		return this._target;
	}

	set Target(elm)
	{
		if (this._target != undefined && this._built)
		{
			this._target.removeEventListener('mouseenter', this._onEnter);
			this._target.removeEventListener('mouseleave', this._onLeave);
		}

		this._target = elm;

		if (this._target != undefined && this._built)
		{
			this._target.addEventListener('mouseenter', this._onEnter);
			this._target.addEventListener('mouseleave', this._onLeave);
		}
	}
}

window.customElements.define('jhrm-tooltip', JhrmTooltip);
