import { ScannerInputDecoder } from "./scanner_input_decoder.js"

/**
 * This class is designed to work with a barcode scanner that has been 
 * configured to send input as keyboard events. 
 * 
 * Once created, this class will listen for a chosen trigger, by default 
 * the trigger is CTRL+s
 * 
 * When the trigger is detected, all input will be captured until the enter 
 * or esc key is pressed.  The trigger, the input, and the end events will 
 * be cancelled after detection, so they will not affect the UI. For example, 
 * even if a text box is selected, the events will not result in text being 
 * entered in the text box. 
 * 
 * Note that this means that this class can be used / tested even without a 
 * barcode scanner, by manually entering the trigger sequence, and then 
 * typing the code manually, followed by the enter key. This is useful both 
 * for testing without a scanner, and for manual input in the case of a 
 * smudged or damaged barcode. 
 * 
 * @property {Function} onScanStart
 *     If set, called when the trigger is detected. 
 * @property {Function} onScanData
 *     If set, called when data is scanned. Will be called once for each 
 *     character. Function is passed a single argument consisting of the 
 *     KeyboardEvent. 
 * @property {Function} onScanComplete
 *     If set, called when a scan has been completed. Function is passed a 
 *     single argument: The string value of this._data
 * @property {function} onStateChange
 *     If set, called whenever the state of the scanner changes. 
 * @property {String} State 
 *     
 * @property {Boolean} Enabled 
 *     
 */
export class ScannerInputListener
{
	/**
	 * Create event listeners and starting state. 
	 * 
	 * @param {MODIFIER_KEYS} trigger 
	 * @param {char} modifier Single lowercase character
	 */
	constructor(trigger, modifier, enabled)
	{
		this._data = '';
		this.onScanStart = undefined;
		this.onScanComplete = undefined;
		this.onScanData = undefined;
		this.onStateChange = undefined;
		this._state = undefined;

		if(modifier)
		{
			if(Object.values(ScannerInputListener.MODIFIER_KEYS).includes(modifier))
			{
				this._modifier = modifier;
			}
			else 
			{
				throw new Error('Invalid modifier.')
			}
		}
		else 
		{
			this._modifier = ScannerInputListener.MODIFIER_KEYS.CONTROL;
		}
		if(trigger)
		{
			if((typeof trigger === 'string' && trigger.match(/^[a-z]$/)))
			{
				this._trigger = trigger;
			}
			else 
			{
				throw new Error('Invalid trigger.');
			}
		}
		else 
		{
			this._trigger = 's';
		}
		if(enabled)
		{
			this.State = document.hasFocus() ?
				ScannerInputListener.STATE.AVAILABLE :
				ScannerInputListener.STATE.UNAVAILABLE;
		}
		else 
		{
			this.State = ScannerInputListener.STATE.DISABLED;
		}
		
		Object.seal(this);

		let focusHander = ()=>{
			switch (this.State)
			{
				case ScannerInputListener.STATE.DISABLED:
					// Do nothing if the state is disabled.
					return;

				case ScannerInputListener.STATE.SCANNING:
					// End scan if the state is scanning. Defer the state change
					// so that we can fall through and handle it here. 
					this.ScanComplete(ScannerInputListener.DEFER);

				case ScannerInputListener.STATE.AVAILABLE:
				case ScannerInputListener.STATE.UNAVAILABLE:
					this.State = document.hasFocus() ?
						ScannerInputListener.STATE.AVAILABLE :
						ScannerInputListener.STATE.UNAVAILABLE;
			}
		};
		
		window.addEventListener('focus', focusHander);
		window.addEventListener('blur', focusHander);
		
		document.addEventListener('keydown', (event) => {
			switch (this.State)
			{
				case ScannerInputListener.STATE.UNAVAILABLE:
					// Should not be possible, but do nothing.
					return;

				case ScannerInputListener.STATE.DISABLED:
					// Do nothing if the state is disabled.
					return;

				case ScannerInputListener.STATE.SCANNING:
					// If we are currently scanning, check for the end key
					event.preventDefault();
					if(this.isEnd(event))
					{
						this.ScanComplete();
						return;
					}
					else 
					{
						this.ScanData(event);
						return;
					}

				case ScannerInputListener.STATE.AVAILABLE:
					// If scanning is available, look for the configured trigger
					if(this.isTrigger(event))
					{
						event.preventDefault();
						this.ScanStart();
					}
			}
		});
	}

	/**
	 * Returns true of the event is the configured scan trigger. 
	 * 
	 * @param {KeyboardEvent} event 
	 * @returns {Boolean}
	 */
	isTrigger(event)
	{
		return event.key == this._trigger && (
			(this._modifier == ScannerInputListener.MODIFIER_KEYS.ALT && event.altKey) || 
			(this._modifier == ScannerInputListener.MODIFIER_KEYS.CONTROL && event.ctrlKey) || 
			(this._modifier == ScannerInputListener.MODIFIER_KEYS.META && event.metaKey)
		);
	}

	/**
	 * Returns true of the event is the configured scan end key. 
	 * 
	 * @param {KeyboardEvent} event 
	 * @returns {Boolean}
	 */
	isEnd(event)
	{
		return Object.values(ScannerInputListener.END_KEYS).includes(event.key);
	}

	/**
	 * Called when the trigger is detected. Handles state and callbacks.
	 */
	ScanStart()
	{
		// Because this can be called externally, we need to verify that we 
		// are in a state that can transition to scanning. 
		switch (this.State)
		{
			case ScannerInputListener.STATE.SCANNING:
			case ScannerInputListener.STATE.UNAVAILABLE:
			case ScannerInputListener.STATE.DISABLED:
				// Ignore invalid state transition
				console.error("Invalid state transition");
				return;

			case ScannerInputListener.STATE.AVAILABLE:
				this.State = ScannerInputListener.STATE.SCANNING;
				this._data = '';
				if(typeof this.onScanStart === 'function')
				{
					this.onScanStart();
				}
		}
	}

	/**
	 * Called when data is scanned. Handles state and callbacks. Only captures
	 * printable keys. 
	 * 
	 * @param {KeyboardEvent} event 
	 */
	ScanData(event)
	{
		if(event.key.length === 1)
		{
			this._data += event.key;
			if(typeof this.onScanData === 'function')
			{
				this.onScanData(event);
			}
		}
	}

	/**
	 * Called when the scan is completed. Handles state and callbacks. 
	 */
	ScanComplete(defer)
	{
		// Because this can be called externally, we need to verify that we 
		// are in a state that can transition to scanning. 
		switch (this.State)
		{
			case ScannerInputListener.STATE.AVAILABLE:
			case ScannerInputListener.STATE.UNAVAILABLE:
			case ScannerInputListener.STATE.DISABLED:
				// Ignore invalid state transition
				console.error("Invalid state transition");
				return;

			case ScannerInputListener.STATE.SCANNING:
				if(!defer)
				{
					this.State = document.hasFocus() ?
						ScannerInputListener.STATE.AVAILABLE :
						ScannerInputListener.STATE.UNAVAILABLE;
				}
				if(typeof this.onScanComplete === 'function')
				{
					this.onScanComplete(ScannerInputDecoder.Decode(this._data));
				}
		}
	}

	/**
	 * @param {ScannerInputListener.STATE} 
	 */
	set State(val)
	{
		if(Object.values(ScannerInputListener.STATE).includes(val))
		{
			if(this._state != val)
			{
				this._state = val;
				if(typeof this.onStateChange === 'function')
				{
					this.onStateChange(this._state);
				}
			}
		}
		else 
		{
			throw new Error("Invalid state.");
		}
	}

	/**
	 * @returns {ScannerInputListener.STATE}
	 */
	get State()
	{
		return this._state;
	}

	/**
	 * @param {Boolean} val 
	 */
	set Enabled(val)
	{
		switch (this.State)
		{
			case ScannerInputListener.STATE.SCANNING:
				// End scan if the state is scanning. Defer the state change
					// so that we can fall through and handle it here. 
				this.ScanComplete(ScannerInputListener.DEFER);

			case ScannerInputListener.STATE.DISABLED:
			case ScannerInputListener.STATE.AVAILABLE:
			case ScannerInputListener.STATE.UNAVAILABLE:
				if(val)
				{
					this.State = document.hasFocus() ?
						ScannerInputListener.STATE.AVAILABLE :
						ScannerInputListener.STATE.UNAVAILABLE;
				}
				else 
				{
					this.State = ScannerInputListener.STATE.DISABLED;
				}
		}
	}

	/**
	 * @returns {Boolean}
	 */
	get Enabled()
	{
		return this.State != ScannerInputListener.STATE.DISABLED;
	}
}

ScannerInputListener.DEFER = 'DEFER';

ScannerInputListener.MODIFIER_KEYS = {
	ALT : 'Alt',
	CONTROL : 'Control',
	META : 'Meta',
	SHIFT : 'Shift'
}
ScannerInputListener.END_KEYS = {
	ENTER : 'Enter',
	ESCAPE : 'Escape'
}

ScannerInputListener.STATE = {
	DISABLED : "Disabled",
	AVAILABLE : "Available",
	UNAVAILABLE : "Unavailable",
	SCANNING : "Scanning"
};