import { 
	ISS_DEVICE_TYPE_ID,
	ISS_WEAR_LOCATION
} from "../shared/Constants.js";

/**
 * Handle decoding scanned data. Each decoder should be defined as an instance 
 * of this class, with a type and a run function which is responsible for 
 * doing the actual decoding. 
 * 
 * Each decoder is responsible for identifying whether the data it is 
 * passed is a type that it should attempt to parse. If not, the decoder 
 * should return false, and parsing will move to the next decoder. This means 
 * that the output of ScannerInputHandler.Decode() may depend on the order in 
 * which the decoders are defined. More general decoders should be defined 
 * after more specific ones for this reason. 
 * 
 * A type may have multiple decoders, for example, a "SENSOR" type might have 
 * decoders for each type of sensor, or even each manufacturer. 
 */
export class ScannerInputDecoder
{
	static TYPE = {
		CAC : 'CAC',
		SENSOR : 'SENSOR',
		UNKNOWN : 'UNKNOWN'
	};

	/**
	 * Take character data from a barcode reader and determine which decoder is 
	 * appropriate for it, and return the parsed data. 
	 * 
	 * @param {Sring} data 
	 * @returns {Object}
	 */
	static Decode(data)
	{
		for(const decoder of ScannerInputDecoder.decoders)
		{
			let match = decoder.run(data);
			if(match===false)
			{
				continue;
			}
			return {
				type : decoder.type,
				raw : data,
				parsed : match
			};
		}
		return {
			type : ScannerInputDecoder.TYPE.UNKNOWN,
			raw : data
		};
	}

	static decoders = [];

	/**
	 * Define a decoder 
	 * 
	 * @param {ScannerInputDecoder.TYPE} type 
	 * @param {Function} run 
	 */
	constructor(type, run)
	{
		// These are essentially a compile-time errors - if they throw, 
		// it means you defined the decoders wrong. 
		if(!Object.keys(ScannerInputDecoder.TYPE).includes(type))
		{
		   
			throw new Error("Scanner input decoder must handle a known format.")
		}
		if(typeof run !== 'function')
		{
			throw new Error("Scanner input decoder must have a run function.")
		}

		this.type = type;
		this.run = run;

		// Add the new decoder to the list of decoders. 
		ScannerInputDecoder.decoders.push(this);
	}
}

// Default CAC decoder. 
new ScannerInputDecoder(
	ScannerInputDecoder.TYPE.CAC,
	function(data)
	{
		// This regex matches the 88 or 89 characters of a CAC PDF417 barcode of
		// type "1" or "N". (The difference between them being the inclusion of 
		// a middle initial at the end in type "N".) On a successful match, 17 
		// capture groups are returned, matching the 17 fields stored in the 
		// barcode (encoding included only where needed for this application).
		//
		// Number indicates match position in array:
		// 
		// 1. Barcode Version: "1" or "N"
		// 2. Personal Designator Identifier: Base 32 encoded
		// 3. Personal Designator Type
		// 4. EDIPI: Base 32 encoded
		// 5. First Name: Plain text space padded to 26 chars
		// 6. Last Name: Plain text space padded to 26 chars
		// 7. Date of Birth:  Base 32 encoded days since 1/1/1000
		// 8. Personnel Category Code
		// 9. Branch Code
		// 10. Personnel Entitlement Condition Type
		// 11. Rank: Plain text
		// 12. Pay Plan Code
		// 13. Pay Plan Grade Code
		// 14. Card Issue Date:	Base 32 encoded days since 1/1/1000
		// 15. Card Expiration Date:	Base 32 encoded days since 1/1/1000
		// 16. Card Instance Identifier
		// 17. Middle Initial (Version N only): Plain text	
		const capture = /^(1|N)(\w{6})(.)(\w{7})(.{20})(.{26})(\w{4})(.)(.)(.{2})(.{6})(.{2})(.{2})(\w{4})(\w{4})(.)(.?)$/;
		let match = data.match(capture);
		if(match===null)
		{
			return false;
		}

		try 
		{
			return {
				first_name : match[5].trim(),
				last_name : match[6].trim(),
				grade : match[13].trim(),
				dodid : base32Decode(match[4]),
				issued : julianDateDecode(base32Decode(match[14])),
				expires : julianDateDecode(base32Decode(match[15])),
			}
		}
		catch(e) 
		{
			// Something went wrong in the parsing. Assume that this is not 
			// a valid CAC and bail out here. 
			return false;
		}
	}
)

// Blast gauge decoder
new ScannerInputDecoder(
    ScannerInputDecoder.TYPE.SENSOR,
    function(data)
	{
		// This regex matches five letters or numbers, followed by H, C, or S. 
		// This is all we have to go on, because it is all the barcode contains. 
		const capture = /^[A-Z0-9]{5}([HCS])$/;
		let match = data.match(capture);
		if(match===null)
		{
			return false;
		}

		try 
		{
			return {
				serial_number : match[0],
				device_type_id : ISS_DEVICE_TYPE_ID.BLAST_GAUGE,
				manufacturer : "Blackbox Biometrics, Inc", // hard coded for now
				model_number : "Gen 7.0", // hard coded for now
				wear_location : match[1] == 'H' ? ISS_WEAR_LOCATION.HEAD :
					match[1] == 'S' ? ISS_WEAR_LOCATION.SHOULDER :
					match[1] == 'C' ? ISS_WEAR_LOCATION.CHEST :
					ISS_WEAR_LOCATION.OTHER
			}
		}
		catch(e) 
		{
			// Something went wrong in the parsing. Assume that this is not 
			// a valid Blast Gauge. 
			return false;
		}
	}
)

// The fillowing utility functions have been inlined here as they are very 
// specific to the task at hand, and unlikely to be needed elsewhere in the 
// application. 

/**
 * This is a specialized decoder for base 32 encoded data on Military ID and CAC
 * cards. It only operates on strings representing decimal numbers, and is not
 * suitable for arbitrary base32 encoded binary data. No provision is made for 
 * padding. Also note that the alphabet used is is 0-V, also known as base32hex,
 * and NOT the standard RFC4648 base32 alphabet
 * 
 * @param {String} input
 * @returns {Number}
 */
function base32Decode(input)
{
	// Am empty string decodes to 0. 
	if(input.length==0)
	{
		return 0;
	}

	// US Mil standard base32 alphabet. 
	let alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUV";
	return input

		// Break input string into array of chracters
		.split('')

		// For each character, calculate decimal value from base32 value.
		.map((curr, i, arr)=>{
			const base = 32;
			const exp = arr.length-1-i;
			const digit_value = alphabet.indexOf(curr);

			// We can't recover from this.
			if(digit_value===-1)
			{
				throw new Error("Invalid base 32 input.");
			}

			// To calculate the value of the given place, raise 32 to the power
			// of the place (position counting right to left from 0) multiplied 
			// by the value of the digit. 
			return digit_value * Math.pow(base, exp);
		})

		// Sum value of each place for total
		.reduce((a, b) => a + b, 0);
}

/**
 * Take a number representing the Julian Date - January 1, 1000 for a given date
 * and return a JavaScript Date object. 
 * 
 * We ignore leap seconds but not the fact that Junian Dates go from noon to noon 
 * instead of midnight to midnight, because our precision for this calculation is 
 * 1 day. 
 * 
 * @param {Number} input 
 * @returns {Date}
 */
function julianDateDecode(input)
{
	// Julian date of unix epoch. the .5 is the noon / midnight thing.
	const julian_unix_epoch = 2440587.5;

	// Julian date of the day cac dates are calculated from. 
	const julian_cac_epoch = 2086303;

	// Full julian date of field
	let julian = julian_cac_epoch+input;
	
	// Unix time if the field: seconds since unix epoch is equal to 86400 * days
	// since unix epoch. 
	let unix = (julian - julian_unix_epoch) * 86400;

	// Javascript uses milliseconds since epoch
	return new Date(unix * 1000);
}