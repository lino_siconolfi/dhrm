import { ScannerInputListener } from "./scanner_input_listener.js"
import { MaterialIcon } from "../material_icon.js"
import { JhrmModal } from "../modal.js"
import { JhrmAlign } from "../align.js"

const MODIFIER = ScannerInputListener.MODIFIER_KEYS.CONTROL;
const TRIGGER = 'b';

/**
 * This component presents a visual indicator showing if scanning is available 
 * and UI to the test and setup screens for the scanner. It also provides higher
 * level handling of scanner input.
 * 
 * When this component is included on a page, the page can handle input from a 
 * scanner. 
 */
export class Scanner extends HTMLElement 
{
	constructor() 
	{
		super(); 
		this._on_scan_complete = () => null;
		this._modal_open = false;
		this._built = false;
		this._setup_input =undefined;
		this._listener = undefined;
		this._status = undefined;
		this._scan_button = undefined;
		this._cancel_button = undefined;
		this._help_button = undefined;
		this._modal = undefined;
		this._css = `
			<style id="jhrm-scanner-styles">

				.scanner-container
				{
					background-color : var(--button-background, #333);
					display:inline-block;
					float:right;
					padding: 1em;
					color:#fff;
					font-size:1em;
					line-height:1em;
					border-radius: 4px;
				}

				.scanner-widget
				{
					margin:0;
					padding:0;
				}

				.scanner-widget button
				{
					margin-left:0.3em;
					background-color:#ffffff33;
					font-size:1rem;
					padding:5px;
					border-radius: 4px;
					cursor: pointer;
				}

				.scanner-widget material-icon
				{
					margin:0;
					padding:0;
					font-size:1.3em;
				}

				.scanner-cancel
				{
					color:#ffaaaaaa;
				}

				.scanner-widget material-icon i
				{
					vertical-align: middle;
					height:1em;
				}

				.scanner-widget .scanner-status 
				{
					display: inline-block;
					min-width:6em;
				}

				.scanner-modal-inner
				{
					background-color: white; 
					color: black;
					padding: 2em; 
					border-radius: 1em; 
					border: 2px solid black;
					width:max-content;
					max-width:30em;
					height:max-content;
				}

				.scanner-modal-background
				{
					width: 100%; 
					height: 100%; 
					background-color: rgba(0, 0, 0, 0.5);
				}

				.scanner-modal-buttons button
				{
					display:block;
					width:100%;
					margin:0.5em auto;
					font-size:1rem;
				}

				.scanner-modal img 
				{
					width:100%;
					max-width:24em;
					display: block;
					margin: 0 auto;
				}	

				.scanner-modal-error{
					color:darkred;
					font-weight:bold;
				}
			
			</style>
		`;
		this.classList.add("scanner-container");
	}
	
	/**
	 * Called when the component is added to the page. 
	 * 
	 * Most of the logic happens here, with the setup of handlers and listeners.
	 */
	connectedCallback() 
	{
		if(!document.getElementById('jhrm-scanner-styles'))
        {
            document.head.innerHTML += this._css;
		}
		
		if(!this._built)
		{
			let saved = localStorage.getItem('JhrmScannerState');
			this._listener = new ScannerInputListener(TRIGGER, MODIFIER,
				saved===ScannerInputListener.STATE.AVAILABLE);

			this.innerHTML = `
				<p class="scanner-widget">
					Scanning: <span class="scanner-status"></span>
					<button class="scanner-scan"><material-icon icon="control_camera"></material-icon></button>
					<button class="scanner-cancel"><material-icon icon="cancel"></material-icon></button>
					<button class="scanner-help"><material-icon class="scanner-help" icon="help"></material-icon></button>
					<button class="scanner-setup">Setup</button>
				</p>
				<jhrm-modal class="scanner-modal">
					<jhrm-align class="scanner-modal-background" middle center>
						<div class="scanner-modal-inner">
							<h3>Setup Scanner</h3>
							<p>
								Scan the code below to verify that your scanner is connected 
								and enable scanning. This window will disappear automatically 
								if the scan is successful. 
							</p>
							<p class="scanner-modal-error">Unexpected scanner input. Try again, or select the help button below for detailed instructions."</p>
							<img src="/img/test-code.png">
							<div class="scanner-modal-buttons">
							<button class="scanner-modal-help">Setup Help</button>
							<button class="scanner-modal-enable">Enable manual mode</button>
							<button class="scanner-modal-disable">Disable scanner</button>
							<button class="scanner-modal-close">Close</button>
							</div>
						</div>
					</jhrm-align>
				</jhrm-modal>
			`;
			this._status =  this.querySelector('.scanner-status');
			this._scan_button =  this.querySelector('.scanner-scan');
			this._cancel_button =  this.querySelector('.scanner-cancel');
			this._help_button =  this.querySelector('.scanner-help');
			this._setup_button =  this.querySelector('.scanner-setup');
			this._modal =  this.querySelector('.scanner-modal');
			this._modal_error =  this.querySelector('.scanner-modal-error');
			this._modal_help_button = this.querySelector('.scanner-modal-help');
			this._modal_enable_button = this.querySelector('.scanner-modal-enable');
			this._modal_disable_button = this.querySelector('.scanner-modal-disable');
			this._modal_close_button = this.querySelector('.scanner-modal-close');

			this.querySelector('.scanner-modal').onclick = (e) => {
				if(this.querySelector('.scanner-modal')==e.target)
				{
					this.closeHelp();
				}
			};

			this._modal_help_button.onclick = (e) => {
				e.preventDefault();
				window.open("/references/jhrm-issuance-tool-scanner-setup.pdf");
			};

			this._modal_enable_button.onclick = (e) => {
				e.preventDefault();
				this.closeHelp();
				this._listener.Enabled = true;
				localStorage.setItem('JhrmScannerState', ScannerInputListener.STATE.AVAILABLE);
			};

			this._modal_disable_button.onclick = (e) => {
				e.preventDefault();
				this.closeHelp();
				this._listener.Enabled = false;
				localStorage.setItem('JhrmScannerState', ScannerInputListener.STATE.DISABLED);
			};

			this._modal_close_button.onclick = (e) => {
				e.preventDefault();
				this.closeHelp();
			};

			this._cancel_button.onclick = () => {
				this._listener.ScanComplete();
			};

			this._scan_button.onclick = () => {
				this._listener.ScanStart();
			};

			this._setup_button.onclick = 
			this._help_button.onclick = () => {
				this.openHelp();
			};
			
			// Turn off all buttons to start. 
			this._cancel_button.style.display = "none";
			this._scan_button.style.display = "none";
			this._help_button.style.display = "none";
			this._setup_button.style.display = "none";

			this._listener.onScanComplete = (scan) => { 
				this._on_scan_complete(scan);
			 };

			let handleState = (state) => {

				// For all states, set the state text
				this._status.innerHTML = state;

				switch (state)
				{
					case ScannerInputListener.STATE.DISABLED:
						this._cancel_button.style.display = "none";
						this._scan_button.style.display = "none";
						this._help_button.style.display = "none";
						this._setup_button.style.display = "inline-block";
						this._modal_enable_button.style.display = "block";
						this._modal_disable_button.style.display = "none";
						break;
					case ScannerInputListener.STATE.AVAILABLE:
						this._cancel_button.style.display = "none";
						this._scan_button.style.display = "inline-block";
						this._help_button.style.display = "inline-block";
						this._setup_button.style.display = "none";
						this._modal_enable_button.style.display = "none";
						this._modal_disable_button.style.display = "block";
						break;
					case ScannerInputListener.STATE.UNAVAILABLE:
						this._cancel_button.style.display = "none";
						this._scan_button.style.display = "none";
						this._help_button.style.display = "inline-block";
						this._setup_button.style.display = "none";
						this._modal_enable_button.style.display = "none";
						this._modal_disable_button.style.display = "block";
						break;
					case ScannerInputListener.STATE.SCANNING:
						this._cancel_button.style.display = "inline-block";
						this._scan_button.style.display = "none";
						this._help_button.style.display = "inline-block";
						this._setup_button.style.display = "none";
						this._modal_enable_button.style.display = "none";
						this._modal_disable_button.style.display = "block";
						break;
				}
			};

			// Set up initial state
			handleState(this._listener.State);

			// Handle state changes. 
			this._listener.onStateChange = handleState;

			// When the modal is open, we listen for key events to see if the 
			// setup barcode is scanned. We do not use the scanner driver for 
			// this because of chickens and eggs: this is used to turn the 
			// driver on. 
			document.addEventListener('keydown', (event) => {
				if(!this._modal_open)
				{
					return;
				}

				event.preventDefault();

				// What we expect to see some in if everything is working. 
				let expected = [
					MODIFIER,
					TRIGGER,
					ScannerInputListener.MODIFIER_KEYS.SHIFT,
					"J",
					ScannerInputListener.MODIFIER_KEYS.SHIFT,
					"H",
					ScannerInputListener.MODIFIER_KEYS.SHIFT,
					"R",
					ScannerInputListener.MODIFIER_KEYS.SHIFT,
					"M",
					ScannerInputListener.MODIFIER_KEYS.SHIFT,
					"T",
					ScannerInputListener.MODIFIER_KEYS.SHIFT,
					"E",
					ScannerInputListener.MODIFIER_KEYS.SHIFT,
					"S",
					ScannerInputListener.MODIFIER_KEYS.SHIFT,
					"T",
					ScannerInputListener.MODIFIER_KEYS.SHIFT,
					"S",
					ScannerInputListener.MODIFIER_KEYS.SHIFT,
					"C",
					ScannerInputListener.MODIFIER_KEYS.SHIFT,
					"A",
					ScannerInputListener.MODIFIER_KEYS.SHIFT,
					"N",
					"1",
					"2",
					"3",
					ScannerInputListener.END_KEYS.ENTER
				];

				if(event.key === expected[this._setup_input.length])
				{
					this._setup_input.push(event.key);
					if(this._setup_input.length===expected.length)
					{
						this._listener.Enabled = true;
						localStorage.setItem('JhrmScannerState', ScannerInputListener.STATE.AVAILABLE);

						// This prevents a race condition that caused the modal 
						// not to close in some cases. 
						window.setTimeout(()=>this.closeHelp());
					}
				}
				else 
				{
					this._setup_input = [];
					this._modal_error.style.display = "block";
				}

				
			});


			this._built = true;
		}
	}

	/**
	 * Add handler for scan complete events. 
	 */
	set onScanComplete(fn)
	{
		if(typeof fn === 'function')
		{
			this._on_scan_complete = fn;
		}
	}

	/**
	 * Manage opening the modal help window. 
	 */
	openHelp()
	{
		this._modal_error.style.display = "none";
		this._modal_open = true;
		this._setup_input = [];
		this._modal.Visible = true;
	}

	/**
	 * Manage closing the modal help window.
	 */
	closeHelp()
	{
		this._modal_open = false;
		this._modal.Visible = false;
	}
}

customElements.define('jhrm-scanner', Scanner);