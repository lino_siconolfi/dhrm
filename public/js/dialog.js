'use strict'

import { JhrmModal } from "./modal.js"
import { selectAndStripId, removeElement } from "./utility.js"

/**
 * This dialog class presents a dialog to the user that is model, and with a standard button set
 * when the show function is called, with the contents of the tag.  The button-set attribute determines
 * which set of buttons will be shown and can be set as "Ok", OkCancel", "SaveCancel", or "YesNo".
 * 
 * Responding to dialog is done by setting handlers to the OnAccept and OnReject properties.
 * 
 * <jhrm-dialog button-set="Ok">Dialog Content</jhrm-dialog>
 */
export class JhrmDialog extends HTMLElement
{
	constructor()
	{
		super();

		this._onAccept = function()
		{
			return true;
		};

		this._onReject = function()
		{
			return true;
		};

		this._ModalWrapper = new JhrmModal();
		this._ModalWrapper.innerHTML = `
			<jhrm-align middle center style="width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5);">
				<div style="background-color: white; padding: 1.2em; border-radius: 1em; border: 2px solid black;">
					<div id="user_content" style="display: block;"></div>
					<div style="display: grid; padding-top: 1em;">
						<jhrm-align left id="additional_buttons_area" style="grid-row: 1; grid-column: 1; padding-right: 4em;">
						</jhrm-align>
						<jhrm-align right style="grid-row: 1; grid-column: 2;">
							<button id="accept_button"></button>
							<button id="reject_button"></button>
						</jhrm-align>
					</div>
				</div>
			</jhrm-align>
		`;

		this.style.width = "100%";
		this.style.height = "100%";
		this.style.padding = "0";
		this.style.margin = "0";

		this._AcceptButton = selectAndStripId(this._ModalWrapper, "accept_button");
		this._RejectButton = selectAndStripId(this._ModalWrapper, "reject_button");
		this._UserContent = selectAndStripId(this._ModalWrapper, "user_content");
		this._additional_buttons_area = selectAndStripId(this._ModalWrapper, "additional_buttons_area");

		this._close_window = () =>
		{
			this._ModalWrapper.Visible = false;

			if (this._detach_on_close)
			{
				removeElement(this);
				this._detach_on_close = false;
			}
		}

		this._AcceptButton.onclick = 
			(async() => {
				if ( await this._onAccept() )
					this._close_window();
			}).bind(this);

		this._RejectButton.onclick =
			function()
			{
				if ( this._onReject() )
					this._close_window();
			}.bind(this);

		this._built = false;
	}

	connectedCallback()
	{
		if (!this._built)
		{
			let Children = [];
			for (let i = 0; i < this.childNodes.length; ++i)
				Children.push(this.childNodes[i]);

			this._UserContent.append(...Children);
			this.append(this._ModalWrapper);

			let bs = this.getAttribute("button-set");
			let BsVals = JhrmDialog.ButtonSet;

			if (bs == "OkCancel")
				this.Buttons = BsVals.OkCancel;
			else if (bs == "SaveCancel")
				this.Buttons = BsVals.SaveCancel;
			else if (bs == "CloseCancel")
				this.Buttons = BsVals.CloseCancel;
			else if (bs == "YesNo")
				this.Buttons = BsVals.YesNo;
			else if (bs == "Ok")
				this.Buttons = BsVals.Ok;
			else if (bs == "AddCancel")
				this.Buttons = BsVals.AddCancel;

			this._built = true;
		}
	}

	/**
	 * Sets the handler of an accepting of the dialog (OK, Save, Yes).  The handler returns
	 * true if the dialog should close after being run, false otherwise.  The default
	 * handler will close the dialog.
	 */
	set OnAccept(handler)
	{
		this._onAccept = handler;
	}

	/**
	 * Sets the handler of a rejecting of the dialog (Cancel, No).  The handler returns
	 * true if the dialog should close after being run, false otherwise.  The default
	 * handler will close the dialog.
	 */
	set OnReject(handler)
	{
		this._onReject = handler;
	}

	/**
	 * @brief
	 *  Adds a button to the lower left area of the component, and returns 
	 *  a reference to that button.
	 * 
	 * @param { string } text 
	 * @param { function } handler 
	 */
	addButton(text, handler)
	{
		let ret = document.createElement('button');
		ret.style.marginLeft = "1em";
		ret.innerHTML = text;
		ret.onclick = handler;

		this._additional_buttons_area.append(ret);
	}

	/**
	 * Sets the accept and reject buttons of the dialog.
	 * 
	 * @param {JhrmDialog.ButtonSet} button_set
	 */
	set Buttons(button_set)
	{
		let ButtonSet = JhrmDialog.ButtonSet;

		switch(button_set)
		{
		case ButtonSet.Ok:
			this._AcceptButton.innerHTML = "OK";
			this._AcceptButton.style.display = "inline-block";
			
			this._RejectButton.style.display = "none";
			break;
		case ButtonSet.OkCancel:
			this._AcceptButton.innerHTML = "OK";
			this._AcceptButton.style.display = "inline-block";
			
			this._RejectButton.innerHTML = "Cancel";
			this._RejectButton.style.display = "inline-block";
			break;
		case ButtonSet.SaveCancel:
			this._AcceptButton.innerHTML = "Save";
			this._AcceptButton.style.display = "inline-block";
			
			this._RejectButton.innerHTML = "Cancel";
			this._RejectButton.style.display = "inline-block";
			break;
		case ButtonSet.CloseCancel:
			this._AcceptButton.innerHTML = "Close";
			this._AcceptButton.style.display = "inline-block";
			
			this._RejectButton.innerHTML = "Cancel";
			this._RejectButton.style.display = "inline-block";
			break;
		case ButtonSet.YesNo:
			this._AcceptButton.innerHTML = "Yes";
			this._AcceptButton.style.display = "inline-block";
			
			this._RejectButton.innerHTML = "No";
			this._RejectButton.style.display = "inline-block";
			break;
		case ButtonSet.AddCancel:
			this._AcceptButton.innerHTML = "Add";
			this._AcceptButton.style.display = "inline-block";
			
			this._RejectButton.innerHTML = "Cancel";
			this._RejectButton.style.display = "inline-block";
			break;
		}
	}

	/**
	 * Shows the dialog to the user.
	 */
	show()
	{
		if ( !this.parentElement )
		{
			document.body.append(this);
			this._detach_on_close = true;
		}
		else
		{
			this._detach_on_close = false;
		}

		this._ModalWrapper.Visible = true;
	}

	append()
	{
		if (this._built)
			this._UserContent.append(...arguments);
		else
			super.append(...arguments);
	}

	appendChild(node)
	{
		if (this._built)
			this._UserContent.appendChild(node);
		else
			super.appendChild(node);
	}
}

JhrmDialog.ButtonSet = 
{
	Ok : 1,
	OkCancel : 2,
	SaveCancel : 3,
	CloseCancel : 4,
	YesNo : 5,
	AddCancel : 6
}

customElements.define('jhrm-dialog', JhrmDialog);