'use strict'

import { Sortable } from "./sortable.js"

export class JhrmJsonTable extends HTMLElement {

	constructor() 
	{
		super(); 
		this.innerHTML = `
		<table class="JsonTable">
			<thead>

			</thead>
			<tbody>

			</tbody>
		</table>
		`;
	}
	
	connectedCallback() 
	{
		this._table =  this.querySelector('.JsonTable');
		if(this.getAttribute('sortable') == "true")
		{
			Sortable(this._table);
		}
	}

	/**
	 * Set the content of the table from an array of javascript objects. This 
	 * can be called as many times as you like, but each call will completely 
	 * replace the current contents of the table. 
	 * 
	 * @param {array} json Array where each object element will become a row in the 
	 *     table. There will be one column for each unique object key in the entire
	 *     array of objects. Table columns will be in the order they are defined. 
	 * 
	 *     [
	 *         { type : "Book", title : "Romeo and Juliet" },
	 *         ...
	 *     ]
	 *     -----
	 *     <table>
	 *         <thead><tr>
	 *             <td>type</td>
	 *             <td>title</td>
	 *          </tr></thead>
	 *          <tbody><tr> 
	 *              <td>Book</td>
	 *              <td>Romeo and Juliet</td>
	 *         </tr><tr>
	 *         ...
	 *         </tbody>
	 *     </table>
	 * 
	 *     The special key "_settings" can be used to set attributes on the generated 
	 *     TD elements in each row. _settings should be a dictionary object of column 
	 *     keys associated with dictionary objects of key value pairs to add to the tag
	 * 
     *     [
	 *         { type : "Book", title : "Romeo and Juliet", _settings : {
	 *              title : {
	 *                  style : "color:red",
	 *                  class : "myclassname"
	 *              }
	 *         }},
	 *         ...
	 *     ]
	 *     -----
	 *     <table>
	 *         <thead><tr>
	 *             <td>type</td>
	 *             <td>title</td>
	 *          </tr></thead>
	 *          <tbody><tr> 
	 *              <td>Book</td>
	 *              <td style="color:red" class="myclassname"> Romeo and Juliet</td>
	 *         </tr><tr>
	 *         ...
	 *         </tbody>
	 *     </table>
	 *  
	 *     If all objects do not have the same keys, it is up to the caller to set 
	 *     colspan and rowspan as necessary to make the table display correctly:
	 * 
     *     [
	 *         { type : "Book", title : "Romeo and Juliet", _settings : {
	 *              title : {
	 *                  colspan : 2
	 *              }
	 *         }},
	 *         { type: "Movie", title : "Gone with the Wind", runtime : "2.5 Hours"}
	 *         ...
	 *     ]
	 *     -----
	 *     <table>
	 *         <thead><tr>
	 *             <td>type</td>
	 *             <td>title</td>
	 *             <td>runtime</td>
	 *          </tr></thead>
	 *          <tbody><tr> 
	 *              <td>Book</td>
	 *              <td colspan="2"> Romeo and Juliet</td>
	 *         </tr><tr> 
	 *              <td>Movie</td>
	 *              <td>Gone with the Wind</td>
	 *              <td>2.5 Hours</td>
	 *         </tr><tr>
	 *         ...
	 *         </tbody>
	 *     </table>
	 * 
	 *     There is a special case when this parameter is an object instead of 
	 *     an array. In that case, the table is pivoted, so that each object
	 *     key represents a row instead of a column. 
	 * 
	 * @param {object} headings Optionally override the default table headings 
	 *     by specifying a dictionary object associating table column keys to 
	 *     heading names. 
	 */
	setData(json, headings)
	{
		const tbody = this._table.querySelector("tbody"); 
		const thead = this._table.querySelector("thead");
		headings = headings || {};

		function getSettings(settings, key)
		{
			if(!settings || !settings[key])
			{
				return "";
			}
			return Object.keys(settings[key]).map(setting=>` ${setting}="${settings[key][setting]}"`).join("");
		}

		if(json.length)
		{
			const keys = json.reduce((acc, cur)=>{
				Object.keys(cur).forEach(key=>{
					if(!acc.includes(key) && key !== "_settings")
					{
						acc.push(key);
					}
				});
				return acc;
			},[]);
			let th = keys.map(key=>headings[key] ? `<th>${headings[key]}</th>` : `<th>${key}</th>`).join("");
			thead.innerHTML = `<tr>${th}</tr>`;
			tbody.innerHTML = json.map(rowdata=>{
				let td = keys.map(key=>rowdata[key]!==undefined?`<td${getSettings(rowdata._settings, key)}>${rowdata[key]!==null?rowdata[key]:''}</td>`:"").join("");
				return `<tr>${td}</tr>`;
			}).join("");  
		}
		else
		{
			thead.innerHtml="";
			const keys = Object.keys(json);
			const max = keys.reduce((max, cur) => (Array.isArray(json[cur]) && json[cur].length > max) ? json[cur].length : max, 1);
			tbody.innerHTML = keys.map(key=>{
				let items = Array.isArray(json[key]) ? json[key] : [json[key]];
				let td = items.map((item,i) => {
					let colspan = (items.length < max && i+1==items.length) ?
					    `colspan="${1+max-items.length}"` : '';
					
					return `<td ${colspan}>${item!==null ? item : ''}</td>`
				}).join('');
				return `<tr><th>${headings[key] ? headings[key] : key}</th>${td}</tr>`;
			}).join("");  
		}
		
 
	}
	
  };

  customElements.define('jhrm-json-table', JhrmJsonTable);