'use strict';
import { revive } from '/js/shared/revive.js'


/**
 * This is a class to manage a state object and optionally persist some members to the
 * URL query string when the state is changed. It uses the history API to allow the back
 * button to revert to previous states, and will load the initial state from the URI if
 * it is available.
 *
 * onChangeFn is called whenever the state is changed, not only when the URI is changed.
 */
export class State
{
	/**
	 * @param
	 * 	{ URLSearchParams } urlSearchParams
	 * @returns
	 * 	{ Object } An object with properties corresponding the values in urlSearchParams.
	 */
	static decodeQueryParams(urlSearchParams)
	{
		let ret = {};

		urlSearchParams.forEach(
			function(value, key)
			{
				ret[key] = value;
			}
		);

		return ret;
	}

	/**
	 *
	 * @param { Object } obj
	 * @returns { URLSearchParams }
	 */
	static encodeQueryParams(obj)
	{
		let QueryParams = new URLSearchParams();

		for (let attribute in obj)
			QueryParams.set(attribute, obj[attribute]);

		return QueryParams;
	}

	constructor(initialState, persistKeys, onChangeFn)
	{
		if ( initialState instanceof URLSearchParams)
			this._state = State.decodeQueryParams(initialState);
		else
			this._state = initialState || {};

		this._persistKeys = persistKeys || [];
		this._onChangeFn = onChangeFn || function(){
      // Known empty function 
    };
		this._baseUrl = window.location.href.replace(window.location.search,'');

		window.history.replaceState(JSON.stringify(this._state), null, "");
		this.render(this._state);

		window.onpopstate = (event) => {
			if (event.state) {
				this._state = JSON.parse(event.state, revive);
			}
			this.render(this._state);
		};
	}

	get(key)
	{
		return this._state[key] || undefined;
	}

	set(obj, replace, force)
	{
		let updatePageUrl = false;
		let changed = {};

		for (let attribute in obj)
		{
			if ( this._state[attribute] != obj[attribute] || force)
			{
				this._state[attribute] = obj[attribute];
				changed[attribute] = this._state[attribute];

				updatePageUrl = updatePageUrl || this._persistKeys.find(k=>k==attribute) ? true : false;
			}
		}

		if(updatePageUrl)
		{
			let urlQueryParams = new URLSearchParams();

			this._persistKeys.forEach(
				function (key)
				{
					if (this._state[key] !== undefined)
						urlQueryParams.set(key, this._state[key]);
				}.bind(this)
			);

			let urlString = this._baseUrl + "?" + urlQueryParams.toString();

			if(replace)
			{
				window.history.replaceState(
					JSON.stringify(this._state),
					null,
					urlString
				);
			}
			else
			{
				window.history.pushState(
					JSON.stringify(this._state),
					null,
					urlString
				);
			}
		}
		else
		{
			window.history.replaceState(
				JSON.stringify(this._state),
				null,
				""
			);
		}

		if (Object.keys(changed).length > 0)
		{
			this.render(changed, replace);
		}
	}

	render(change, replace)
	{
		this._onChangeFn.call(this, this._state, change, replace);
	}
}
