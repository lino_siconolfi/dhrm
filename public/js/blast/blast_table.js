import { Sortable } from '../sortable.js';
import { Filterable } from "../filterable.js";

export class BlastRow extends HTMLTableRowElement
{
	static fromObject(obj)
	{
		let row = new BlastRow();
		row.EventId = obj.event_id;
		row.DeviceId = obj.device_id;
		row.DeviceDataId = obj.device_data_id;
		row.EventTime = obj.event_date_time;
		row.BGType = obj.mounting_location;
		row.PO = obj.peak_overpressure;
		row.TPI = obj.total_positive_impulse;
		row.APM = obj.acceleration_peak_magnitude;
		row.DataExists = obj.raw_data_exists;
		row.DataFlagged = obj.data_flags;
		row.ExcludeData = obj.exclude_data;
		row.EventPressure = obj.est_max_peak_overpressure;
		return row;
	}

	constructor()
	{
		super();

		this._Built = false;
		this._IsSubRow = false;
		this._SubRows = [];

		this._ElmSel = document.createElement('td');
		this._ElmEventId = document.createElement('td');
		this._ElmEventTime = document.createElement('td');
		this._ElmBGType = document.createElement('td');
		this._ElmPO = document.createElement('td');
		this._ElmTPI = document.createElement('td');
		this._ElmAPM = document.createElement('td');
		this._ElmDataExists = document.createElement('td');
		this._ElmDataFlagged = document.createElement('td');
		this._ElmEventPressure = document.createElement('td');
		this._ElmExcludeData = document.createElement('td');

		this._DeviceId = null;
		this._EventTime = null;
		this._DataFlagged = null;
		this._DataExists=0;

		this._ElmSel.innerHTML = `<input name="blastGaugeEventSelection" type="radio"></input>`;
		this._ElmExcludeData.innerHTML = `<input type="checkbox"></input>`;
	}

	/**
	 * Set a row as a subrow of this one. It will always be displayed after this
	 * row in the table. 
	 * 
	 * @param {BlastRow} row 
	 */
	 addSubRow(row)
	 {
		row.makeSubRow();
		this._SubRows.push(row);
		row.style.backgroundColor = this.style.backgroundColor;
		this.parentNode.insertBefore(row, this.nextSibling);
		this.updateRowspans();	 
	 }
 
	 /**
	  * This method updates the rowspans for this row to take into account any 
	  * subrows. 
	  * 
	  */
	 updateRowspans()
	 {
		this._ElmSel.rowSpan = 
		this._ElmEventId.rowSpan = 
		this._ElmEventPressure.rowSpan = 
		this.Rows;
	 }
 
	 /**
	  * This method causes a row to hide all the duplicate table cells which will 
	  * be filled in by rowspan expanded cells from the parent. 
	  * 
	  * Subrows are non-sortable. 
	  */
	makeSubRow()
	{
		this.SubRow = true;
		this.classList.add('nosort');

		this._ElmSel.style.display = 
		this._ElmEventId.style.display = 
		this._ElmEventPressure.style.display = "none";
	}
 
	 get Rows()
	 {
		 return this._SubRows.length + 1;
	 }
 
	 get SubRow()
	 {
		 return this._IsSubRow;
	 }
 
	 get SubRows()
	 {
		 return this._SubRows;
	 }
 
	 set SubRow(val)
	 {
		 this._IsSubRow = val;
	 }

	connectedCallback()
	{
		if ( !this._Built )
		{
			this.append(
                this._ElmSel,
				this._ElmEventId,
                this._ElmEventTime,
                this._ElmBGType,
                this._ElmPO,
                this._ElmTPI,
                this._ElmAPM,
                this._ElmDataExists,
                this._ElmDataFlagged,
                this._ElmEventPressure,
                this._ElmExcludeData
			);		
			this._Built = true;

			this._ElmSel.onclick = () => {
				// finds and unhides the Raw Data table
				let hidden = document.querySelectorAll(".hidden")
				for(let item of hidden) {
					item.classList.remove("hidden")
				}
				// you will need to clear the raw data table using its clearTable function, and add the rows associated with the selected event
			}
		}
		// This is intentionally outside of the _Built guard! 
		// Whenever this row is moved, sorted, or replaced, it should re-render
		// its sub rows and re-stripe itself. Striping cannot be dne with CSS 
		// in tables with rowspans. Which is lame. 
		if(!this.SubRow)
		{
			if([].slice.call(this.parentNode.childNodes).filter(row=>!row.SubRow).indexOf(this) % 2 == 0)
			{
				this.style.backgroundColor = "#ffffff";
			}
			else 
			{
				this.style.backgroundColor = "#e3e5e8";
			}

			this._SubRows.forEach(row=>{
				row.style.backgroundColor = this.style.backgroundColor;
				this.parentNode.insertBefore(row, this.nextSibling);
				this.updateRowspans();
			});
		}
	}

	set Selected(val)
	{
		this.querySelector("input[type=radio]").checked = !!val;
	}
	get Selected()
	{
		return this.querySelector("input[type=radio]").checked;
	}

	set EventId(val)
	{
		this._ElmEventId.innerText = val;
	}

	get EventId()
	{
		return this._ElmEventId.textContent;
	}

	set DeviceId(val)
	{
		this._DeviceId = val;
	}

	get DeviceId()
	{
		return this._DeviceId;
	}

	set DeviceDataId(val)
	{
		this._DeviceDataId = val;
	}

	get DeviceDataId()
	{
		return this._DeviceDataId;
	}

	set EventTime(val)
	{
		this._EventTime = val;
		this._ElmEventTime.innerText = val;
	}

	get EventTime()
	{
		return this._EventTime;
	}

	set BGType(val)
	{
		this._ElmBGType.innerText = val;
	}

	get BGType()
	{
		return this._ElmBGType.textContent;
	}

	set PO(val)
	{
		this._ElmPO.innerText = val;
	}

	get PO()
	{
		return this._ElmPO.textContent;
	}

	set TPI(val)
	{
		this._ElmTPI.innerText = val;
	}

	get TPI()
	{
		return this._ElmTPI.textContent;
	}

	set APM(val)
	{
		this._ElmAPM.innerText = val;
	}

	get APM()
	{
		return this._ElmAPM.textContent;
	}

	set DataExists(val)
	{
		this._DataExists = val;
		this._ElmDataExists.innerText = this._DataExists ? "YES" : "NO";
	}

	get DataExists()
	{
		return this._DataExists;
	}

	set DataFlagged(val)
	{
		this._DataFlagged = val;
		this._ElmDataFlagged.innerText = val ? val : "N";
	}

	get DataFlagged()
	{
		return this._DataFlagged;
	}

	set EventPressure(val)
	{
		this._ElmEventPressure.innerText = val
	}

	get EventPressure()
	{
		return this._ElmEventPressure.textContent;
	}

	set ExcludeData(val) 
	{
		this._ElmExcludeData.querySelector('input[type="checkbox"]').checked = val;
	}

	get ExcludeData() 
	{
		return this._ElmExcludeData.querySelector('input[type=checkbox]').checked;
	}
}

customElements.define('jhrm-blast-row', BlastRow, { extends: 'tr' });

export class BlastTable extends HTMLElement
{
	constructor()
	{
		super();

		this.innerHTML = `
		<div class="tablewrapper" id="blastgaugerow">
			<table>
				<thead>
					<th>Sel.</th>
					<th>Event ID</th>
					<th>Event Time (Zulu)</th>
					<th>Blast Gauge Type</th>
					<th>PO* (psi)</th>
					<th>TPI* (psi-msec)</th>
					<th>APM* (g)</th>
					<th>Raw Data Exists <material-icon icon="help_outline" id="RawDataHelp"></material-icon></th>
					<th>Data Flagged <material-icon icon="help_outline" id="DataFlaggedHelp"></material-icon></th>
					<th>Estimated Event Pressure (psi) <material-icon icon="help_outline" id="EstEventPressureHelp"></material-icon></th>
					<th>Exclude Data <material-icon icon="help_outline" id="ExcludeDataHelp"></material-icon></th>
				</thead>
				<tbody>
				
				</tbody>
			</table>
		</div>
		<jhrm-tooltip target="RawDataHelp"> False = Non-Real Algorithm is not applicable.</jhrm-tooltip>
		<jhrm-tooltip target="DataFlaggedHelp"> Record flagged data as NR-Non-Real, MG-Missing Gauge, DM-Data Mismatch, or N-No Flag.</jhrm-tooltip>
		<jhrm-tooltip target="EstEventPressureHelp"> Calculated estimated event pressure from event gauges.</jhrm-tooltip>
		<jhrm-tooltip target="ExcludeDataHelp"> Check the box to exclude the data record.</jhrm-tooltip>
		`;

		this._ElmHeader = this.querySelector('thead');
		this._ElmBody = this.querySelector('tbody');
		this._ElmTable = this.querySelector('table');
		this._ElmTableWrapper = this.querySelector('.tablewrapper');
		Sortable(this._ElmTable);
		new Filterable(this._ElmTable);
	}

	appendSummaryRow(newRow)
	{
		if (newRow instanceof BlastRow)
		{
			let existing = this.Rows.find(row=>row.EventId==newRow.EventId);
			
			if(existing)
			{
				existing.addSubRow(newRow);
			}
			else 
			{
				this._ElmBody.append(newRow);
			}
		}
	}

	get Rows()
	{
		return Array.from(this._ElmBody.children);
	}

	get Selected()
	{
		return this.Rows.find(row=>row.Selected==true);
	}

	get Checked()
	{
		return this.Rows.filter(row=>row.ExcludeData==true);
	}
}

customElements.define('jhrm-blast-table', BlastTable);

export class RawDataRow extends HTMLTableRowElement {
	constructor() {
		super();
		
		this._PressureDataTime = document.createElement("td")
		this._HeadOverpressure = document.createElement("td")
		this._ShoulderOverpressure = document.createElement("td")
		this._ChestOverpressure = document.createElement("td")
		this.append(this._PressureDataTime, this._HeadOverpressure, this._ShoulderOverpressure, this._ChestOverpressure)
	}

	set PressureDataTime(val) {
		this._PressureDataTime.innerText = val
	}

	set HeadOverpressure(val) {
		this._HeadOverpressure.innerText = val
	}

	set ShoulderOverpressure(val) {
		this._ShoulderOverpressure.innerText = val
	}

	set ChestOverpressure(val) {
		this._ChestOverpressure.innerText = val
	}

	static fromObjData(record)
	{
		let nextRow = new RawDataRow(record);
		nextRow.PressureDataTime = record.PressureDataTime
		nextRow.HeadOverpressure = record.HeadOverpressure
		nextRow.ChestOverpressure = record.ChestOverpressure
		nextRow.ShoulderOverpressure = record.ShoulderOverpressure

		return nextRow;
	}
}

customElements.define('jhrm-raw-data-row', RawDataRow, { extends: 'tr' })

export class RawDataTable extends HTMLElement {
	constructor() {
		super();

		this.innerHTML = `
		<div class="tablewrapper" id="blastgaugerow">
			<table>
				<thead>
					<th>Pressure Data Time (msec)</th>
					<th>Head Overpressure (psi)</th>
					<th>Shoulder Overpressure (psi)</th>
					<th>Chest Overpressure (psi)</th>
				</thead>
				<tbody>
				
				</tbody>
			</table>
		</div>
		`;

		this._ElmHeader = this.querySelector('thead');
		this._ElmBody = this.querySelector('tbody');
		this._ElmTable = this.querySelector('table');
		this._ElmTableWrapper = this.querySelector('.tablewrapper');
		Sortable(this._ElmTable);
	}

	BlastGaugeRawData(results)
		{
			this.clearTable();
			let rows = Math.max(...results.map(item=>item.length));
			for(var i = 0; i < rows; i++)
			{
				let row = {
					HeadOverpressure : "",
					ShoulderOverpressure : "",
					ChestOverpressure : ""
				}
			    Object.keys(row).forEach((val, index)=>{
					if(results[index][i])
					{
						row[val] = results[index][i].overpressure;
						row.PressureDataTime = results[index][i].time;
					}
				});
				
				let nextRow = RawDataRow.fromObjData(row);
				this.appendRawDataRow(nextRow);
			}
		}

	appendRawDataRow(row)
	{
		if (row instanceof RawDataRow)
			this._ElmBody.append(row);
	}

	clearTable() {
		while(this._ElmBody.firstChild) {
			this._ElmBody.removeChild(this._ElmBody.firstChild)
		}
	}
}

customElements.define('jhrm-raw-data-table', RawDataTable);