import { Request } from "../request.js"

export class BlastQANotification extends HTMLElement {
    constructor() {
        super();
        
        this._cookie = document.cookie;
    }

    async connectedCallback() {
        let user = await Request.get('./api/user');
        let response = await Request.get('/api/blast/summarydata');
     
        if(response.length > 0 && user.role.hra_qa) {
            this.style.width = "2em";
            this.style.height = "2em";
            this.style.borderRadius = "50%";
            this.style.backgroundColor= "red";
            this.style.color = "white";
            this.style.fontSize = "1.4em";
            this.style.padding = "10px"
            this.style.textAlign = "center";
            this.style.fontWeight = "600";
            this.style.display = "inline-block";
            this.style.marginLeft = "15px";
            this.innerText = response.length;
        } else {
            this.style.dispay = "none";
        }
    }
}

customElements.define("blast-qa-notification", BlastQANotification);