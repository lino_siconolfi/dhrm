import { Sortable } from "../sortable.js";
import { Datetime } from "../shared/datetime.js";
import { Filterable } from "../filterable.js";

export class BlastQaSummaryRow extends HTMLTableRowElement
{
	constructor()
	{
		// Always have to call super in components
		super();
		
		
		this.innerHTML = `
			<td><input type="radio" name="BlastQaSummaryRowSelected"></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td><input type="checkbox"></td>
		`;
 
		this._ElmSelected = this.querySelector('td:nth-child(1) input');
		
		this._ElmDODId = this.querySelector('td:nth-child(2)');
		this._ElmName = this.querySelector('td:nth-child(3)');
		this._ElmDownloadDate = this.querySelector('td:nth-child(4)');
		this._ElmNumberEvents = this.querySelector('td:nth-child(5)');
		this._ElmMaxPeak = this.querySelector('td:nth-child(6)');
		this._ElmGreater4Psi = this.querySelector('td:nth-child(7)');
		this._ElmGreater16Psi = this.querySelector('td:nth-child(8)');
		this._ElmEstimatedMaxPeak = this.querySelector('td:nth-child(9)');
		this._ElmDataFlagged = this.querySelector('td:nth-child(10)');
		this._ElmQaApproved = this.querySelector('td:nth-child(11) input');

		this._DownloadDate = null;
		this._DataFlagged = false;
		this._Events = 0;
	}

	connectedCallback()
	{
		// This sends a custom event when a new row is selected. The parent page can listen for it. 
		this._ElmSelected.onchange = (e) => {
			let evt = new CustomEvent('selectionChanged', {
				bubbles: true
			});
			this.dispatchEvent(evt);
		};
		
	}

	set Selected(val)
	{
		this._ElmSelected.checked = !!val;
	}

	get Selected()
	{
		return this._ElmSelected.checked;
	}

	get Events()
	{
		return this._Events;
	}

	set Events(val)
	{
		this._Events = val;
	}

	set DODId(val)
	{	
		this._ElmDODId.innerText = val;
	}
	
	get DODId()
	{
		return this._ElmDODId.textContent;
	}

	set Name(val)
	{
		this._ElmName.innerText = val;
	}

	get Name()
	{
		return this._ElmName.textContent;
	}

	set DownloadDate(val)
	{
		this._DownloadDate = val;
		this._ElmDownloadDate.innerHTML = `<a href="./blast_qa.html?dodid=${this.DODId}&download_date=${val}">${val}</a>`;
	}

	get DownloadDate()
	{
		return this._DownloadDate;
	} 

	set NumberEvents(val)
	{
		this._ElmNumberEvents.innerText = val;
	}

	get NumberEvents()
	{
		return Number(this._ElmNumberEvents.textContent);
	}

	set MaxPeak(val)
	{
		this._ElmMaxPeak.innerText = val;
	}

	get MaxPeak()
	{
		return Number(this._ElmMaxPeak.textContent);
	}

	set Greater4Psi(val)
	{
		this._ElmGreater4Psi.innerText = val;
	}

	get Greater4Psi()
	{
		return Number(this._ElmGreater4Psi.textContent);
	}

	set Greater16Psi(val)
	{
		this._ElmGreater16Psi.innerText = val;
	}

	get Greater16Psi()
	{
		return Number(this._ElmGreater16Psi.textContent);
	}

	set EstimatedMaxPeak(val)
	{
		this._ElmEstimatedMaxPeak.innerText = val;
	}

	get EstimatedMaxPeak()
	{
		return this._ElmEstimatedMaxPeak.textContent;
	}
	
	set DataFlagged(val)
	{
		this._DataFlagged = val;

		this._ElmDataFlagged.innerText = (val) ? "Yes" : "No";
	}

	get DataFlagged()
	{
		return this._DataFlagged;
	}

	set QaApproved(val)
	{
		this._ElmQaApproved.checked = val;
	}

	get QaApproved()
	{
		return this._ElmQaApproved.checked;
	}
	
	static fromObjDataBlast(record)
	{
		let nextRow = new BlastQaSummaryRow(record);
		nextRow.DODId = record.dodid;
		nextRow.Name = record.first_name + " " + record.last_name;
		nextRow.DownloadDate = record.download_date;
		nextRow.NumberEvents = record.numEvents;
		nextRow.MaxPeak = record.maxpeak;
		nextRow.Greater4Psi = record.over4psi;
		nextRow.Greater16Psi = record.over16psi;
		nextRow.EstimatedMaxPeak = record.estimatedpeak;
		nextRow.DataFlagged = record.flagged;
		nextRow.QaApproved = record.qa_approved;
		nextRow.Events = record.events;
		return nextRow;
	}
}
customElements.define('jhrm-blast-summary-row', BlastQaSummaryRow, { extends: 'tr' });

export class BlastQaSummaryTable extends HTMLElement
{
	constructor()
	{
		super();

		this.innerHTML = `
		<div class="tablewrapper">
			<table>
				<thead>
					<th>Sel.</th>
					<th>DODID</th>
					<th>Name</th>
					<th>Download Date</th>
					<th># of Events</th>
					<th>Max Peak (psi)
						<material-icon icon="help_outline" id="MaxPeakHelp"></material-icon>
					</th>
					<th># > 4 psi
						<material-icon icon="help_outline" id="4PSIHelp"></material-icon>
					</th>
					<th># > 16 psi
						<material-icon icon="help_outline" id="16PSIHelp"></material-icon>
					</th>
					<th>Incident Pre Field Event Pressure (psi)
						<material-icon icon="help_outline" id="HighestEstimatedPeakHelp"></material-icon>
					</th>
					<th>Data Flagged
						<material-icon icon="help_outline" id="DataFlaggedHelp"></material-icon>
					</th>
					<th>Approved
						<material-icon icon="help_outline" id="ApprovedHelp"></material-icon>
						
					</th>
				</thead>
				<tbody>
				
				</tbody>
			</table>
		</div>
		<jhrm-tooltip target="MaxPeakHelp"> Highest max peak reading associated with the dataset.</jhrm-tooltip>
		<jhrm-tooltip target="DataFlaggedHelp"> Indicates there are records that are flagged as non-real data, missing gauge data, or data mismatch.</jhrm-tooltip>
		<jhrm-tooltip target="HighestEstimatedPeakHelp"> Single highest estimated event pressure across the total number of records.</jhrm-tooltip>
		<jhrm-tooltip target="16PSIHelp"> Number of records exceeding 16 psi.</jhrm-tooltip>
		<jhrm-tooltip target="4PSIHelp"> Number of records exceeding 4 psi.</jhrm-tooltip>
		<jhrm-tooltip target="ApprovedHelp"> Select approved when all flagged records have been QA'd and removed.</jhrm-tooltip>
		`;

		this._ElmTable = this.querySelector('table');
		this._ElmBody = this.querySelector('tbody');
		
		Sortable(this._ElmTable);
		new Filterable(this._ElmTable);
		
	}

	/**
	 * Add a row to the table. 
	 * 
	 * @param {BlastQaSummaryRow} row 
	 */
	appendRow(row)
	{
		if (row instanceof BlastQaSummaryRow)
		{
			this._ElmBody.append(row);
		}
		else 
		{
			throw new Error("Bad row. ");
		}
	}

	get Rows()
	{
		return Array.from(this._ElmBody.children);
	}

	get Selected()
	{
		return this.Rows.find(row=>row.Selected==true);
	}

	get Checked()
	{
		return this.Rows.filter(row=>row.QaApproved==true);
	}

	/**
	 * Remove all rows. 
	 */
	empty()
	{
		// This is unsafe with event listeners.
		this._ElmBody.innerHTML = '';
	}

}
customElements.define('jhrm-blast-summary-table', BlastQaSummaryTable);

export class RawDataRow extends HTMLTableRowElement {
	constructor() {
		super();
		
		this._SensorLocation = document.createElement("td")
		this._SerialNumber = document.createElement("td")
		this._IssueDate = document.createElement("td")
		this._ReturnDate = document.createElement("td")
		this._InitializationDate = document.createElement("td")
		this.append(this._SensorLocation,this._SerialNumber,this._IssueDate,this._ReturnDate,this._InitializationDate)
	}

	set SensorLocation(val) {
		this._SensorLocation.innerText = val
	}

	set SerialNumber(val) {
		this._SerialNumber.innerText = val
	}

	set IssueDate(val) {
		this._IssueDate.innerText = val
	}

	set ReturnDate(val) {
		this._ReturnDate.innerText = val
	}
	
	set InitializationDate(val) {
		this._InitializationDate.innerText = val
	}
	
	static fromObjDataBlast(item) {
		let row = new RawDataRow()
		row.SensorLocation = item.wear_location
		row.SerialNumber = item.serial_number
		row.IssueDate = item.issue_date;
		row.ReturnDate = item.returned_date;
		row.InitializationDate = item.initialization_date;
		return row
	}
}
customElements.define('jhrm-raw-data-row', RawDataRow, { extends: 'tr' });

export class RawDataTable extends HTMLElement {
	constructor() {
		super();

		let createTH = (text) =>
		{
			let elm = document.createElement('th');
			elm.innerHTML = text;

			return elm;
		}

		this._ElmHeader = document.createElement('thead');
		this._ElmBody = document.createElement('tbody');

		this._ElmHeader.append(
			
			createTH('Sensor Location'),
			createTH('Serial Number'),
			createTH('Issue Date'),
			createTH('Return Date'),
			createTH('Initialization Date'),
		);
		
		this._ElmTable = document.createElement('table');
		this._ElmTable.append(this._ElmHeader, this._ElmBody);
		this._ElmTable.classList.add('jhrm-boxed-section')
		Sortable(this._ElmTable)
		this.append(this._ElmTable);
	}

	empty() {
		while(this._ElmBody.firstChild) {
			this._ElmBody.removeChild(this._ElmBody.firstChild)
		}
	}

	appendRow(row)
	{
		if (row instanceof RawDataRow)
		{
			this._ElmBody.append(row);
		}
		else 
		{
			throw new Error("Bad row. ");
		}
	}
}

customElements.define('jhrm-raw-data-table', RawDataTable);

