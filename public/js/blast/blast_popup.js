import { Request } from "../request.js"
export class BlastPopup extends HTMLElement {
    constructor() {
        super();
        let shadowRoot = this.attachShadow({mode: "open"});
        this.style.display = "none"
        const action = this.getAttribute("action") || "exclude"
        let headString = `Are you sure you want to ${action} the blast overpressure data?`
        let commentString = action === "approve" ? "Enter QA Comments" : "Provide details on why a recorded event was excluded (e.g. non-real data flag)"
        shadowRoot.innerHTML = 
        `
        <link rel="stylesheet" type="text/css" href="./css/core.css">
        <style>
        .popup {
            border: 2px solid black;
            border-radius: 10px;
            padding: 15px 10px;
            width: fit-content;
            position: absolute;
            left: 45vw;
            top: 45vh;
            background: white;
            z-index: 2;
        }
 
        .btnContainer {
            display: flex;
            width: 100%;
            justify-content: flex-end;
            align-items: center;
        }
 
        .btnContainer > button {
            margin: 7px;
        }
 
        .BGLayer {
            position: absolute;
            height: 100vh;
            width: 100vw;
            z-index: -1;
            top:0;
            left:0;
        }
 
        .secondary {
            display: none;
        }
 
        .secondary textarea {
            width: 90%;
            height: 10vh;
            margin: 10px;
        }
 
        .big-button:hover {
            cursor: pointer;
        }
 
        .warning {
            color: red;
            display: none;
        }
        </style>
            <div class="popup">
                <strong>${headString}</strong>
                <div id="btn1" class="btnContainer">
                    <button class="big-button">Yes</button>
                    <button class="big-button">No</button>
                </div>
                <div class="secondary">
                    <textarea id="comments" placeholder='${commentString}'></textarea>
                    <div id="btn2" class="btnContainer">
                        <span class="warning">Please Enter Comments Before Saving</span>
                        <button class="big-button">Save</button>
                        <button class="big-button">Cancel</button>
                    </div>
                </div>
            </div>
        `
    }
    // had to use   instead of spaces on line 70, as spaces seemed to be escaping the quotes

    connectedCallback() 
    {
        let buttons = this.shadowRoot.querySelectorAll(".big-button");
        buttons[0].onclick = () => this.handleClick(true);
        buttons[1].onclick = () => this.handleClick(false);
        buttons[2].onclick = () => this.handleClick(false, true);
        buttons[3].onclick = () => this.handleClick(false);
    }
 
    async handleClick(val, save=false) {
        
        if(val) {
            this.shadowRoot.querySelector(".secondary").style.display = "block"
            this.shadowRoot.querySelector("#btn1").style.display = "none"
        } else {
            if(save) {
                switch(this.getAttribute('action')) {
                    case 'exclude': {
                        let row;
                        if(this.getAttribute('action') === "approve"){
                        // row is the object of the selected record
                            let radio = document.querySelector("jhrm-blast-table").querySelector("input[type='radio']:checked")
                            row = radio.parentElement.parentElement
                        }

                        let bgtabledata = [];          
                        let rows = document.querySelector("jhrm-blast-table").Rows;
                        rows.forEach(row => {
            
                            if(row != undefined)
                            {
                                bgtabledata.push({
                                    bgd_device_id : row.DeviceId,
                                    bgd_event_id : row.EventId,
                                    exclude_data : row.ExcludeData,
                                    exclude_data_comments : this.shadowRoot.querySelector("textarea").value 
                                })
                            }
                        })
            
                        let response = await Request.post('/api/blast/exposure/event',
                            bgtabledata
                        );
                    }
                    break
                    case 'approve': {
                        let table = document
                        .querySelector("jhrm-blast-summary-table");
                        let rows = table.Checked.reduce((acc, cur)=>{
                            return acc.concat(cur.Events);
                        },[]);
                        await Request.post('/api/blast/summaryqaapproved',  {
                            qa_comments : this.shadowRoot.querySelector('textarea').value,
                            events : rows
                        })
                    }
                }
                if(this.shadowRoot.querySelector("textarea").value.length == 0) {
                    this.shadowRoot.querySelector(".warning").style.display = "block"
                    return;
                }
            }
            window.location.href = "./blast.html";
        }
    }
}
 
customElements.define("jhrm-blast-popup", BlastPopup);

