import { JhrmRadioGroup } from "./radio_group.js"
import { MaterialIcon } from "./material_icon.js"
import { JhrmSwapSpace } from "./swap_space.js"
import { selectAndStripId } from "./utility.js"

/**
 * This component provides a Yes/No radio select, and is set using true and false
 * respectively.
 */
export class YesNoSelect extends HTMLElement
{
	constructor()
	{
		super();

		this._radio_options = new JhrmRadioGroup();
		let genOption = this._radio_options.genElementAndLabel.bind(this._radio_options);

		this.append(
				...genOption(true, "Yes"),
				...genOption(false, "No")
		);

		this._radio_options.Value = false;

		this._built = false;
	}

	/**
	 * Gets the selected radio option.
	 *
	 * @returns { Boolean }
	 */
	get SelectedOption()
	{
		return this._radio_options.Value;
	}

	/**
	 * Sets the selected radio option.
	 *
	 * @param { Boolean } val
	 */
	set SelectedOption(val)
	{
		this._radio_options.Value = val;
	}

	/**
	 * Sets the handler for a change in the selection.
	 * The parameter passed to the handler is a
	 * YesNoUnknownOption value. No return is expected.
	 */
	set onchange(handler)
	{
		this._radio_options.onchange = handler;
	}
}

customElements.define('jhrm-yes-no-select', YesNoSelect);

export const YesNoUnknownOption = {
	Yes : "Yes",
	No : "No",
	Unknown : "Unknown"
}

export class YesNoUnknownSelect extends HTMLElement
{
	constructor()
	{
		super();

		this._radio_options = new JhrmRadioGroup();
		let genOption = this._radio_options.genElementAndLabel.bind(this._radio_options);
		let options = YesNoUnknownOption;

		this.append(
				...genOption(options.Yes, "Yes"),
				...genOption(options.No, "No"),
				...genOption(options.Unknown, "Unknown")
		);

		this._radio_options.Value = "No";

		this._built = false;
	}

	/**
	 * Gets the selected radio option.
	 *
	 * @returns { YesNoUnknownOption }
	 */
	get SelectedOption()
	{
		return this._radio_options.Value;
	}

	/**
	 * Sets the selected radio option.
	 *
	 * @param { YesNoUnknownOption } val
	 */
	set SelectedOption(val)
	{
		this._radio_options.Value = val;
	}

	/**
	 * Sets the handler for a change in the selection.
	 * The parameter passed to the handler is a
	 * YesNoUnknownOption value. No return is expected.
	 */
	set onchange(handler)
	{
		this._radio_options.onchange = handler;
	}
}

customElements.define('jhrm-yes-no-unknown-select', YesNoUnknownSelect);

export class YesNoUnknownDescription extends HTMLElement
{
	constructor()
	{
		super();

		this._description_text = document.createElement('input');
		this._description_text.type = 'text';

		this._radio_options = new JhrmRadioGroup();
		let genOption = this._radio_options.genElementAndLabel.bind(this._radio_options);
		let options = YesNoUnknownOption;

		this._radio_elements =
			[
				...genOption(options.Yes, "Yes"),
				...genOption(options.No, "No"),
				...genOption(options.Unknown, "Unknown")
			];

		this._built = false;
	}

	connectedCallback()
	{
		if ( !this._built)
		{
			let question_nodes =  Array.from(this.childNodes);

			this.innerHTML = "";

			let description_container = document.createElement('div')
			description_container.style.cssText = "margin-left: 1em; display: inline;";
			description_container.append("Description: ", this._description_text);

			this.append(
				...question_nodes,
				...this._radio_elements,
				description_container
			);

			this._built = true;
		}
	}

	/**
	 * Gets the selected radio option.
	 *
	 * @returns { YesNoUnknownOption }
	 */
	get SelectedOption()
	{
		return this._radio_options.Value;
	}

	/**
	 * Sets the selected radio option.
	 *
	 * @param { YesNoUnknownOption } val
	 */
	set SelectedOption(val)
	{
		this._radio_options.Value = val;
	}

	/**
	 * Gets the text of the description field.
	 *
	 * @param { YesNoUnknownOption }
	 */
	get Description()
	{
		return this._description_text.value;
	}

	/**
	 * Sets the text of the description field.
	 *
	 * @param { string } val
	 */
	set Description(val)
	{
		this._description_text.value = val;
	}
}

customElements.define('jhrm-yes-no-unknown-description', YesNoUnknownDescription);

/**
 * This component is an inline option for text fields that make it apparent to the user
 * that they can be edited and that some handling more substantial than a simple changing
 * of a local value (such as pushing data to a backend) will happen once a new value has
 * been accepted.
 */
export class PushTextEdit extends HTMLElement
{
	constructor()
	{
		super();

		this.style.display = "inline-grid";
		this.style.gridTemplateColumns = "1fr auto";
		this.style.gap = "0.5em";

		this.innerHTML = `
			<input id="text_input" style="grid-row: 1; grid-column: 1; width: 100%;"></input>
			<jhrm-swap-space id="button_set_swap" size-to-visible style="grid-row: 1; grid-column: 2;">
				<div id="inactive_buttons">
					<material-icon icon="edit" id="edit_button"></material-icon>
				</div>
				<div id="active_buttons" style="white-space: nowrap;">
					<material-icon icon="check" id="accept_button" style="color: green;"></material-icon>
					<material-icon icon="close" id="reject_button" style="color: red;"></material-icon>
				</div>
			</jhrm-swap-space>
		`;

		this._text_input = selectAndStripId(this, 'text_input');

		this._button_set_swap = selectAndStripId(this, 'button_set_swap');
		this._inactive_buttons = selectAndStripId(this, 'inactive_buttons');
		this._active_buttons = selectAndStripId(this, 'active_buttons');

		this._accept_button = selectAndStripId(this, 'accept_button');
		this._reject_button = selectAndStripId(this, 'reject_button');
		this._edit_button = selectAndStripId(this, 'edit_button');

		this._text_input.style.border = "0";
		this._text_input.style.background = "transparent";

		this._accept_button.style.cursor = "pointer";
		this._reject_button.style.cursor = "pointer";
		this._edit_button.style.cursor = "pointer";

		if ( this.hasAttribute("placeholder") )
			this.Placeholder = this.getAttribute("placeholder");

		this._change_handler = (val) => { return true; };

		this._stored_value = "";

		this._edit_button.onclick = () =>
		{
			this._text_input.focus();
		};

		this._text_input.onfocus = () =>
		{
			if ( this._button_set_swap.VisibleElement == this._inactive_buttons)
			{
				this._button_set_swap.VisibleElement = this._active_buttons;
				this._text_input.style.background = "#fff";
			}
		};

		this._reject_button.onclick = () =>
		{
			this._button_set_swap.VisibleElement = this._inactive_buttons;
			this._text_input.value = this._stored_value;

			this._text_input.blur();
		};

		this._accept_button.onclick = () =>
		{
			if (this._change_handler(this._text_input.value))
			{
				this._button_set_swap.VisibleElement = this._inactive_buttons;
				this._text_input.style.background = "transparent";
				this._text_input.blur();

				this._stored_value = this._text_input.value;
			}
			else
			{
				this._text_input.focus();
			}
		};
	}

	connectedCallback()
	{
    // Known empty function
	}

	/**
	 * @returns { string }
	 *
	 * This returns the text in the edit box, whether it has been
	 * saved or not.
	 */
	get Text()
	{
		return this._text_input.value;
	}

	/**
	 * @returns { string }
	 *
	 * This sets the text in the edit box, but does not change the stored value.
	 */
	get Text()
	{
		return this._text_input.value;
	}

	/**
	 * Gets the place holder text for the input.
	 */
	get Placeholder()
	{
		return this._text_input.placeholder;
	}

	/**
	 * Sets the place holder text for the input.
	 */
	set Placeholder(text)
	{
		this._text_input.placeholder = text;
	}

	/**
	 * @returns { string }
	 *
	 * Ths gets the last saved value.
	 */
	get Value()
	{
		return this._stored_value;
	}

	/**
	 * @param { string } val
	 *
	 * This sets the text and the stored value, but does not trigger an onchange event.
	 */
	set Value(val)
	{
		this._stored_value = val;
		this._text_input.value = val;
	}

	/**
	 * Handles a confirmed change event with the new value passed as the
	 * function parameter.
	 *
	 * This is after the chceckbox has been clicked.
	 * This is a hook to validate and do custom handling of the change.
	 * The handler function should return true if the input is acceptable, which
	 * will cause the widget to store the value and go back into an inactive state
	 * Returning false will refocus the widget and keep the state active.
	 */
	set onchange(handler)
	{
		this._change_handler = (handler) ? handler : (val) => { return true; };
	}

	/**
	 * Returns true if the component is active.  This will indicate that
	 * the component is in an editing state, and has not been saved or
	 * rejected by the user.
	 */
	get Active()
	{
		return (this._button_set_swap.VisibleElement == this._active_buttons);
	}
}

customElements.define('jhrm-push-text-edit', PushTextEdit);

export class DataTimeEdit extends HTMLElement
{
	constructor()
	{
		super();

		this.innerHTML = `<jhrm-datepicker id="elm_date"></jhrm-datepicker><jhrm-timepicker id="elm_time"></jhrm-timepicker>`;

		this._elm_date = selectAndStripId(this, 'elm_date');
		this._elm_time = selectAndStripId(this, 'elm_time');
	}

	/**
	 * @returns { Date }
	 */
	get Value()
	{
		// Note that this will interperet date in the local time of the browser.
		return new Date(`${this._elm_date.value}T${this._elm_time.value}`);
	}

	/**
	 * @param { Date }
	 */
	set Value(val)
	{
		let time_text = `${val.getHours().toString().padStart(2, '0')}:${val.getMinutes().toString().padStart(2, '0')}`;
		let date_text = `${val.getFullYear()}-${(val.getMonth() + 1).toString().padStart(2, '0')}-${val.getDate().toString().padStart(2, '0')}`;

		this._elm_time.value = time_text;
		this._elm_date.value = date_text;
	}
};

customElements.define('jhrm-date-time-edit', DataTimeEdit);
