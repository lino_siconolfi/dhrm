'use strict'

import { Request } from "../request.js"
import { 
    IFA_INCIDENT_SCENARIO, 
    IFA_STATUS, 
    IFA_INCIDENT_TYPE, 
    IFA_INCIDENT_CAUSE, 
    IFA_TRINARY
} from "../shared/Constants.js";

/**
 * Overview display for an IFA 
 * 
 * @element jhrm-ifa-overview
 * @attribute {bool} sortable 
 */
export class IfaOverview extends HTMLElement
{
    /**
     * Constructor 
     * 
     * @param {String} ifa Optional IFA ID. 
     */
	constructor(ifa)
	{
        super();

        this._css = `
            <style id="ifa-overview-styles">
                .ifa-overview h3, .ifa-overview dt, .ifa-overview dd, .ifa-overview dl 
                {
                    margin:0;
                    padding:0;
                }
                .ifa-overview{
                    display:grid;
                    grid-gap:2px;
                    border:2px solid black;
                    background-color:black;
                    grid-template-columns:repeat(6, 1fr);
                }
                .ifa-overview dl 
                {
                    display:contents;
                }
                .ifa-overview h3, .ifa-overview dt, .ifa-overview dd 
                {
                    background-color:#ddd;
                    padding:0.75em;
                }
                .ifa-overview h3 {
                    background-color:#eee;
                    grid-column-start:span 6;
                    font-weight:normal;
                }
                .ifa-overview dt{
                    grid-column-start:span 1;
                    font-style: italic;
                    background-color:#ccc;
                }
                .ifa-overview dt.large{
                    grid-column-start:span 2;
                }
                .ifa-overview dd {
                    grid-column-start:span 2;
                }
                .ifa-overview dd.large {
                    grid-column-start:span 5;
                }
                .ifa-overview dd.small {
                    grid-column-start:span 1;
                }
                .ifa-option{
                    padding-right:1em;
                    position:relative;
                    display:inline-block;
                }
                .ifa-option material-icon
                {
                    position:relative;
                    top:2px
                }  
            </style>
        `;
        
        // If an IFA ID is passed in with the constructor or in an attribute, 
        // set it. 
        if(ifa)
        {
            this.ifa = ifa;
        }
        else if (this.getAttribute('ifa'))
		{
			this.ifa = this.getAttribute('ifa');
        }
    }

    /**
     * When the node is attached, make sure styles are applied. 
     */
    connectedCallback()
    {
        // If the styles for this component are not already included, add them 
        // to this page. This approach allows default styling with CSS (which
        // allows it to be overridden) without requiring the use of the shadow 
        // DOM. 
        if(!document.getElementById('ifa-overview-styles'))
        {
            document.head.innerHTML += this._css;
        }
    }
    
    /**
     * Set the ID and fetch the IFA from the API. 
     * 
     * @param (String) IFA ID
     */
    set ifa(report)
    {
        this._id = id;
        Request.get('/api/ifa/report/' + id )
            .then(report=>this.render(report))
            .catch(err=>this.renderError("Unable to load IFA."));
    }

    /**
     * Render the view. 
     * 
     * @param {Object} report 
     */
    render(report)
    {
        this.innerHTML = `
            <div class="ifa-overview">
                <h3>General IFA Information</h3>
                <dl>
                    <dt>Report Start Date / Time</dt>
                    <dd>${report.report_start_datetime}</dd>

                    <dt>Status</dt>
                    <dd>${IFA_STATUS.toString(report.status)}</dd>

                    <dt>Initial Preparer's Name</dt>
                    <dd>${report.initial_preparer && report.initial_preparer.name ? report.initial_preparer.name : "N/A"}</dd>

                    <dt>Initial Preparer's Email</dt>
                    <dd>${report.initial_preparer && report.initial_preparer.email ? report.initial_preparer.email : "N/A"}</dd>

                    <dt>Initial Preparer's Phone</dt>
                    <dd>${report.initial_preparer && report.initial_preparer.phone_num ? report.initial_preparer.phone_num : "N/A"}</dd>

                    <dt>Initial Preparer's Unit</dt>
                    <dd>${report.initial_preparer && report.initial_preparer.unit ? report.initial_preparer.unit : "N/A"}</dd>
                </dl>
            
                <h3>General Incident Information</h3>
                <dl>
                    <dt>Incident Name</dt>
                    <dd>${report.short_name}</dd>

                    <dt>Incident Cause</dt>
                    <dd>${this.getOptions(IFA_INCIDENT_CAUSE, report.cause, 'radio')}</dd>

                    <dt>CCIR/SigAct#</dt>
                    <dd>${report.ccir || "N/A"}</dd>

                    <dt>Incident Scenario</dt>
                    <dd>
                        ${this.getOptions(IFA_INCIDENT_SCENARIO, report.scenario, 'radio')}
                    </dd>

                    <dt>Incident Start Date / Time</dt>
                    <dd>${report.start_datetime}</dd>

                    <dt>Incident End Date / Time</dt>
                    <dd>${report.end_datetime}</dd>

                    <dt>Incident Type</dt>
                    <dd>${this.getOptions(IFA_INCIDENT_TYPE, report.incident_type)}</dd>

                    <dt>Incident Type Description 
                        <material-icon icon="help_outline" id="IncidentTypeDescriptionHelp"></material-icon>
                        <jhrm-tooltip target="IncidentTypeDescriptionHelp">
                            Country Code - Incident Type - Incident Scenario - Incident Cause - Date Time Group.  Example: IQ-CON-OPS-EA-062720(Z)0800 (IQ - Iraq, CON - Concussive, OPS - Allied Operations, EA - Enemy Action)
                        </jhrm-tooltip>
                    </dt>
                    
                    <dd>${report.name}</dd>

                    <dt>Incident Summary</dt>
                    <dd class="large">${report.summary}</dd>
                </dl>
            
                <h3>Incident Location</h3>
                <dl>
                    <dt>MGRS Coordinates</dt>
                    <dd>${report.location ? report.location : "N/A"}</dd>

                    <dt>Map Image Available</dt>
                    <dd>${report.map_image || 'N/A'}</dd>

                    <dt>Coordinates (Latitude/Longitude)</dt>
                    <dd class="large">${report.location ? report.location.point[1] : "N/A"} / ${report.location ? report.location.point[0] : "N/A"}</dd>

                </dl>
            
                <h3>Incident Details</h3>
                <dl>
                    <dt class="large">Are there personnel associated to the incident?</dt>
                    <dd class="small">${this.getOptions(IFA_TRINARY, report.personnel_associated, 'radio')}</dd>

                    <dt class="large">Total number of personnel associated</dt>
                    <dd class="small">${report.personnel_associated_count || "N/A"}</dd>

                    <dt class="large">Signs / symptoms reported?</dt>
                    <dd class="small">${this.getOptions(IFA_TRINARY, report.symptoms_associated, 'radio')}</dd>

                    <dt class="large">Number of personnel reporting symptoms</dt>
                    <dd class="small">${report.symptoms_associated_count || "N/A"}</dd>

                    <dt class="large">Unit(s) involved</dt>
                    <dd class="small">${report.units_involved || "N/A"}</dd>

                    <dt class="large">Were there any odors reported?</dt>
                    <dd class="small">${this.getOptions(IFA_TRINARY, report.odors, 'radio')}</dd>

                    <dt class="large">Field monitor / detector data collected??</dt>
                    <dd class="small">${this.getOptions(IFA_TRINARY, report.data_produced, 'radio')}</dd>

                    <dt class="large">Were samples collected?</dt>
                    <dd class="small">${this.getOptions(IFA_TRINARY, report.samples_collected, 'radio')}</dd>
                </dl>
            </div>
        `;
    }

    /**
     * Render an error
     * 
     * @param {String} message 
     */
    renderError(message)
    {
        this.innerHtml = `<p class="error">${message}</p>`
    }

    /**
     * List a set of options from a constant and mark the selected value. 
     * 
     * This could be its own web component, but as it is only intended to 
     * display and not to be used for input, I expect it will never be used 
     * outside of this component. 
     * 
     * As a part of the future IFA web portal work, it may make sense to make 
     * this a more general component and also use it for inputs. 
     * 
     * @param {array} constant 
     * @param {string} value 
     * @param {string} type 
     */
    getOptions(constant, value, type)
    {
        let checked, unchecked;
        if(type=='radio')
        {
            checked = 'radio_button_checked';
            unchecked = 'radio_button_unchecked';
        }
        else
        {
            checked = 'check_box';
            unchecked = 'check_box_outline_blank';
        }
        if(!Array.isArray(value))
        {
            value = [value];
        }
        return Object
            .keys(constant)
            .map(option => `<span class="ifa-option"><material-icon icon="${value.includes(option) ? checked : unchecked}"></material-icon> ${constant.toString(option)}</span>`)
            .join('');
    }

}

customElements.define('jhrm-ifa-overview', IfaOverview);