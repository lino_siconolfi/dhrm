export class IFAHazardMitigationRow extends HTMLTableRowElement
{
	constructor(_ShowSelection)
	{
		super();

		this._Built = false;
		this._ShowSelection = _ShowSelection;
		this._ElmSelected = document.createElement('input');
		this._ElmSelected.type = 'checkbox';
		this._ElmAction = document.createElement('td');
		this._ElmDescription = document.createElement('input');
		this._ElmDescription.type = 'text';

	}

	connectedCallback()
	{
		if ( !this._Built )
		{
			let TDSelected = document.createElement('td');
			TDSelected.append(this._ElmSelected);
			let TDAction = document.createElement('td');
			TDAction.append(this._ElmAction);
			TDAction.style.textAlign = "left";
			let TDDescription = document.createElement('td');
			TDDescription.append(this._ElmDescription);

			if( this._ShowSelection )
			{
				this.append(
					TDSelected
				);
			}

			this.append(
				TDAction, TDDescription
			);

			this._Built = true;
		}
	}

	//setting if selected
	set Selected(val)
	{
		this._ElmSelected.checked = val;
	}

	//getting if selected
	get Selected()
	{
		return this._ElmSelected.checked;
	}

	//set risk communication description
	set RiskCommunication(val)
	{
		this._ElmRiskCommsInput.innerText = val;
	}

	// get risk communication description
	get RiskCommunication()
	{
		return this._ElmRiskCommsInput.textContent;
	}

	// set general summary of incident/exposure description
	set GeneralSummaryOfIncidentExposure(val)
	{
		this._ElmGenSummaryInput.innerText = val;
	}

	//get general summary of incident/exposure description
	get GeneralSummaryOfIncidentExposure()
	{
		return this._ElmGenSummaryInput.textContent;
	}

	//set Oher text description
	set OtherText(val)
	{
		this._ElmOtherInput.innerText = val;
	}

	//get other text description
	get OtherText()
	{
		return this._ElmOtherInput.textContent;
	}

	static fromObjData(record)
	{
		let ret = new IFAHazardMitigationRow(true);

		if (record.action == "Risk Communication")
			{
				ret._ElmAction = record.action;
				ret._RiskCommunication = record.inputDescription;
			}
		if (record.action == "General Summary of Incident / Exposures")
			{
				ret._ElmAction = record.action;
				ret._GeneralSummaryOfIncidentExposure = record.inputDescription;
			}
		if (record.action == "Other")
			{
				ret._ElmAction = record.action;
				ret._OtherText = record.inputDescription;
			}
	

		return ret;

	}
}

customElements.define('jhrm-ifa-hazard-mitigation-row', IFAHazardMitigationRow, { extends: 'tr' });

export class IFAHazardMitigationTable extends HTMLElement
{
	constructor()
	{
		super();
		
		this._Built = false;

		let createTH = (text) =>
		{
			let elm = document.createElement('th');
			elm.innerHTML = text;

			return elm;
		}
		let createTHDouble = (text) =>
		{
			let elm = document.createElement('th');
			elm.innerHTML = text;
			elm.colSpan = "2";

			return elm;
		}

		this._ElmHeader = document.createElement('thead');
		this._ElmBody = document.createElement('tbody');

		this._ElmHeader.append(
			createTHDouble('Action'),
			createTH('Description')
		);

		this._ElmTable = document.createElement('table');
		this._ElmTable.append(this._ElmHeader, this._ElmBody);
		this._ElmTableWrapper = document.createElement('div');
		this._ElmTableWrapper.append(this._ElmTable);

		this._ElmBody.innerHTML = "";
		let val = IFAHazardMitigationRow.fromObjData({"action" : "Risk Communication", "inputDescription" : this._ElmRiskCommsInput });        
		this.appendSampleRow(val);

		val = IFAHazardMitigationRow.fromObjData({"action" : "General Summary of Incident / Exposures", "inputDescription" : this._ElmGenSummaryInput});        
		this.appendSampleRow(val);

		val = IFAHazardMitigationRow.fromObjData({"action" : "Other", "inputDescription" : this._ElmOtherInput});     
		this.appendSampleRow(val);
	}
	
	connectedCallback()
	{
		if (!this._Built)
		{
			this._ElmTableWrapper.className="tablewrapper";
			this.append(this._ElmTableWrapper);
			this._Built = true;
		}
	}

	appendSampleRow(row)
	{
		if (row instanceof IFAHazardMitigationRow)
			this._ElmBody.append(row);
	}

	get SelectedIDs()
	{
		let tBody = this._ElmBody;
		let ret = [];

		for (let i = 0; i < tBody.children.length; ++i)
		{
			let element = tBody.children.item(i);

			if (element.Selected)
				ret.push(element.SampleID);
		}
		return ret;
	}
}

customElements.define('jhrm-ifa-hazard-mitigation-table', IFAHazardMitigationTable);
