'use strict'

import { Sortable } from "../sortable.js"
import { Request } from "../request.js"
import { 
    IFA_INCIDENT_SCENARIO, 
    IFA_INCIDENT_TYPE,
    IFA_INCIDENT_CAUSE,
    IFA_TRINARY,
    IFA_SUBMISSION_STATUS,
    IFA_QA_STATUS,
    IFA_STATUS,
    IFA_PREPARER_TYPE
} from "../shared/Constants.js";

/**
 * Extend the TR element for displaying an IFA summary view row. Does not 
 * implement getters or seters for the individual columns at this time, as the
 * usage does not require that functionality. 
 * 
 * @element jhrm-ifa-summary-row
 */
export class IfaSummaryRow extends HTMLTableRowElement
{
	constructor(rowdata)
	{
        super();
        this._rowdata = rowdata || {};
    }
    
    /**
     * Render the row contents from the values in this._rowdata. 
     */
    render()
    {
        this.innerHTML = `
            <td><a href="/ifa.html?ifa=${this._rowdata.id}">${this._rowdata.short_name}</a></td>
            <td>${this._rowdata.location ? this._rowdata.location : 'Unknown'}</td>
            <td>${this._rowdata.start_datetime}</td>
            <td>${this._rowdata.end_datetime}</td>
            
            <td>${IFA_TRINARY.toString(this._rowdata.personnel_associated)}</td>
            <td>${IFA_TRINARY.toString(this._rowdata.data_produced)}</td>
            <td>${IFA_TRINARY.toString(this._rowdata.images_associated)}</td>

            <td>${this._rowdata.last_preparer ? this._rowdata.last_preparer.name : 'Unknown'}</td>
            <td>${IFA_STATUS.toString(this._rowdata.status)}</td>
            <td>${IFA_SUBMISSION_STATUS.toString(this._rowdata.submission_status)}</td>
        `;
    }

	connectedCallback()
	{
        this.render();
	}
}

customElements.define('jhrm-ifa-summary-row', IfaSummaryRow, { extends: 'tr' });

/**
 * Custom scrollable table for listing IFAs. 
 * 
 * @element jhrm-ifa-summary-table
 * @attribute {bool} sortable 
 */
export class IfaSummaryTable extends HTMLElement
{
	constructor()
	{
		super();
        this.innerHTML = `
        <div class="tablewrapper">
            <table class="IfaSummaryTable">
                <thead>
                    <tr>
                        <th>Incident Name</th>
                        <th>Location</th>
                        <th>Incident Start Date / Time</th>
                        <th>Incident End Date / Time</th>
                        <th>Individuals Associated</th>
                        <th>Field Data Produced</th>
                        <th>Photos Attached</th>
                        <th>Last Edited By</th>
                        <th>IFA Status</th>
                        <th>Submission Status</th>
                    </tr>
                </thead>
                <tbody>
                
                </tobdy>
            </table>
        </div>
        `;

        // Not using await because I don't think you can make a constructor async.
        // Or at least I got an error and did not want to dig into it when just 
        // using promise syntax fixed it. 
        Request.get('/api/ifa/reports').then(
            reports=>reports.forEach(
                report=>this.appendSummaryRow(new IfaSummaryRow(report))
            )
        );

        this._ElmTable =  this.querySelector('.IfaSummaryTable');
        this._ElmTbody = this._ElmTable.querySelector('tbody');
		if(this.getAttribute('sortable') == "true")
		{
			Sortable(this._ElmTable);
		}
	}

    /**
     * 
     * @param {*} row 
     */
	appendSummaryRow(row)
	{
		if (row instanceof IfaSummaryRow)
		{
            this._ElmTbody.append(row);
        }
	}

}

customElements.define('jhrm-ifa-summary-table', IfaSummaryTable);