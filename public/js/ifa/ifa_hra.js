import { selectAndStripId, removeElement } from "../utility.js"
import { RiskAssessment } from "../risk_assessment.js"
import { Datetime } from "../shared/datetime.js"
import { JhrmAlign } from "../align.js"

/**
 * IFA HRA expect the following properties with the expected types:
 * 
 * { Datetime } CreationTime
 * { string } AverageRiskLevelConfidence
 * { string } PeakRiskLevelConfidence
 * { RiskAssessment.RiskLevel } AverageRiskLevel
 * { RiskAssessment.RiskLevel } PeakRiskLevel
 * { string } PeakMilitaryExposureGuideline 
 * { string } AverageMilitaryExposureGuideline 
 * { Number } PeakPepc - in mg/m3
 * { Number } AveragePepc - in mg/m3
 * { string } ChemicalOfConcern
 * { string } Id
 * 
 * This utility functions in this class can be used to automate the transfer of named
 * properties to and from any object which adheres to the property spec, or to set
 * default values.
 */
export class IfaHra
{
	/**
	 * Creates an IfaHra object from the corresponding fields on the backend.
	 * 
	 * @param { object } obj
	 */
	static fromBackendFields(obj)
	{
		let ret = new IfaHra();

		ret.AverageRiskLevelConfidence = obj.acute_average_confidence;
		ret.PeakRiskLevelConfidence = obj.acute_peak_confidence;
		ret.AverageRiskLevel = obj.acute_average_risk;
		ret.PeakRiskLevel = obj.acute_peak_risk;
		ret.PeakMilitaryExposureGuideline = obj.acute_peak_meg_name;
		ret.AverageMilitaryExposureGuideline = obj.acute_average_meg_name;
		ret.PeakPepc = obj.acute_peak_pepc.value;
		ret.AveragePepc = obj.acute_average_pepc.value;
		ret.ChemicalOfConcern = obj.chemical_name;
		ret.CreationTime = obj.last_edited_on;
		ret.Id = obj.id;

		return ret;
	}

	/**
	 * Sets IfaHra fields to their default values on the passed object.
	 */
	static setDefaults(obj)
	{
		obj.CreationTime = null;
		obj.AverageRiskLevelConfidence = "";
		obj.PeakRiskLevelConfidence = "";
		obj.AverageRiskLevel = RiskAssessment.RiskLevel.None;
		obj.PeakRiskLevel = RiskAssessment.RiskLevel.None;
		obj.PeakMilitaryExposureGuideline = "";
		obj.AverageMilitaryExposureGuideline = "";
		obj.PeakPepc = 0.0;
		obj.AveragePepc = 0.0;
		obj.ChemicalOfConcern = "";
		obj.Id = "";
	}

	/**
	 * Copies IfaHra fields present from src into dest.  Any missing fields in src are ignored
	 * and left at their original values in dest.
	 * 
	 * @param { object } src
	 * @param { object } dest
	 */
	static copyFields(src, dest)
	{
		if ( src.CreationTime )
			dest.CreationTime = src.CreationTime;

		if ( src.AverageRiskLevelConfidence )
			dest.AverageRiskLevelConfidence = src.AverageRiskLevelConfidence;

		if ( src.PeakRiskLevelConfidence )
			dest.PeakRiskLevelConfidence = src.PeakRiskLevelConfidence;

		if ( src.AverageRiskLevel )
			dest.AverageRiskLevel = src.AverageRiskLevel;

		if ( src.PeakRiskLevel )
			dest.PeakRiskLevel = src.PeakRiskLevel;

		if ( src.PeakMilitaryExposureGuideline )
			dest.PeakMilitaryExposureGuideline = src.PeakMilitaryExposureGuideline;

		if ( src.AverageMilitaryExposureGuideline )
			dest.AverageMilitaryExposureGuideline = src.AverageMilitaryExposureGuideline;

		if ( src.PeakPepc )
			dest.PeakPepc = src.PeakPepc;

		if ( src.AveragePepc )
			dest.AveragePepc = src.AveragePepc;

		if ( src.ChemicalOfConcern )
			dest.ChemicalOfConcern = src.ChemicalOfConcern;

		if ( src.Id )
			dest.Id = src.Id;
	}

	constructor(obj)
	{
		IfaHra.setDefaults(this);
		Object.seal(this);

		if (obj)
			IfaHra.copyFields(obj, this);
	}

	/**
	 * Gets an object populated with properties named according to backend conventions.
	 * 
	 * @returns { object }
	 */
	toBackendFields()
	{
		return {
			acute_average_confidence : this.AverageRiskLevelConfidence,
			acute_peak_confidence : this.PeakRiskLevelConfidence,
			acute_average_risk : this.AverageRiskLevel,
			acute_peak_risk : this.PeakRiskLevel,
			acute_peak_meg_name : this.PeakMilitaryExposureGuideline,
			acute_average_meg_name : this.AverageMilitaryExposureGuideline,
			acute_peak_pepc : { value : this.PeakPepc },
			acute_average_pepc : { value : this.AveragePepc },
			chemical_name : this.ChemicalOfConcern,
			id : this.Id
		};
	}
}

export class IfaHraDisplay extends HTMLElement
{
	constructor()
	{
		super();

		this.innerHTML = `
			<table style="border-collapse: collapse; margin: 0.5em 0;">
				<tr>
					<td>
						<span id="hra_id_label">HRA ID: </span><span id="hra_id"></span>
					</td>
					<th style="text-align: center;">Acute Risk Peak</th>
					<th style="text-align: center;">Acute Risk Average</th>
				</tr>
				<tr>
					<th style="text-align: right;">Chemical of Concern</th>
					<td colspan="2" id="chem_of_concern"></td>
				</tr>
				<tr>
					<th style="text-align: right;">PEPC Value</th>
					<td id="peak_pepc"></td>
					<td id="average_pepc"></td>
				</tr>
				<tr>
					<th style="text-align: right;">Military Exposure Guideline</th>
					<td id="peak_mil_exp_guide"></td>
					<td id="average_mil_exp_guide"></td>
				</tr>
				<tr>
					<th style="text-align: right;">Risk Level</th>
					<td id="peak_risk_level"></td>
					<td id="average_risk_level"></td>
				</tr>
				<tr>
					<th style="text-align: right;">Risk Level Confidence</th>
					<td id="peak_risk_level_conf"></td>
					<td id="average_risk_level_conf"></td>
				</tr>
				<tr>
					<th style="text-align: right;">Date Risk Assessment Completed</th>
					<td colspan="2" id="creation_time"></td>
				</tr>
			</table>
		`;

		this._hra_id = selectAndStripId(this, "hra_id");
		this._chem_of_concern = selectAndStripId(this, "chem_of_concern");
		
		this._peak_pepc = selectAndStripId(this, "peak_pepc");
		this._average_pepc = selectAndStripId(this, "average_pepc");
		
		this._peak_mil_exp_guide = selectAndStripId(this, "peak_mil_exp_guide");
		this._average_mil_exp_guide = selectAndStripId(this, "average_mil_exp_guide");

		this._peak_risk_level = selectAndStripId(this, "peak_risk_level");
		this._average_risk_level = selectAndStripId(this, "average_risk_level");

		this._peak_risk_level_conf = selectAndStripId(this, "peak_risk_level_conf");
		this._average_risk_level_conf = selectAndStripId(this, "average_risk_level_conf");

		this._creation_time = selectAndStripId(this, "creation_time");

		this.CreationTime = null;

		this.PeakPepc = 0.0;
		this.AveragePepc = 0.0;

		this.PeakRiskLevel = RiskAssessment.RiskLevel.None;
		this.AverageRiskLevel = RiskAssessment.RiskLevel.None;
		
		this.querySelectorAll("td, th").forEach(
			(elm) =>
			{
				elm.style.backgroundColor = "lightgray";
				elm.style.border = "2px solid black";
				elm.style.padding = "0.35em 1em";
			}
		);
		
		this.querySelectorAll("td").forEach(
			(elm) =>
			{
				elm.style.textAlign = "center";
			}
		);
	}

	/**
	 * @returns { string }
	 */
	get Id()
	{
		return this._hra_id.innerHTML;
	}

	/**
	 * @param { string }
	 */
	set Id(val)
	{
		this._hra_id.innerHTML = val;
	}

	/**
	 * @returns { string }
	 */
	get ChemicalOfConcern()
	{
		return this._chem_of_concern.innerHTML;
	}

	/**
	 * @param { string }
	 */
	set ChemicalOfConcern(val)
	{
		this._chem_of_concern.innerHTML = val;
	}

	/**
	 * @returns { Number }
	 */
	get PeakPepc()
	{
		return this._PeakPepc;
	}

	/**
	 * @param { Number } val Units mg/m3
	 */
	set PeakPepc(val)
	{
		this._PeakPepc = val;
		this._peak_pepc.innerHTML = `${val} mg/m<sup>3</sup>`;
	}

	/**
	 * @returns { Number }
	 */
	get AveragePepc()
	{
		return this._AveragePepc;
	}

	/**
	 * @param { Number } val Units mg/m3
	 */
	set AveragePepc(val)
	{
		this._AveragePepc = val;
		this._average_pepc.innerHTML = `${val} mg/m<sup>3</sup>`;
	}

	/**
	 * @returns { string }
	 */
	get PeakMilitaryExposureGuideline()
	{
		return this._peak_mil_exp_guide.innerHTML;
	}

	/**
	 * @param { string }
	 */
	set PeakMilitaryExposureGuideline(val)
	{
		this._peak_mil_exp_guide.innerHTML = val;
	}

	/**
	 * @returns { string }
	 */
	get AverageMilitaryExposureGuideline()
	{
		return this._average_mil_exp_guide.innerHTML;
	}

	/**
	 * @param { string }
	 */
	set AverageMilitaryExposureGuideline(val)
	{
		this._average_mil_exp_guide.innerHTML = val;
	}

	/**
	 * @returns { RiskAssessment.RiskLevel }
	 */
	get PeakRiskLevel()
	{
		return this._PeakRiskLevel;
	}

	/**
	 * @param { RiskAssessment.RiskLevel }
	 */
	set PeakRiskLevel(val)
	{
		this._PeakRiskLevel = val;

		this._peak_risk_level.innerHTML = val;
		RiskAssessment.styleForRisk(this._peak_risk_level, val);
	}

	/**
	 * @returns { RiskAssessment.RiskLevel }
	 */
	get AverageRiskLevel()
	{
		return this._AverageRiskLevel;
	}

	/**
	 * @param { RiskAssessment.RiskLevel }
	 */
	set AverageRiskLevel(val)
	{
		this._AverageRiskLevel = val;

		this._average_risk_level.innerHTML = val;
		RiskAssessment.styleForRisk(this._average_risk_level, val);
	}

	/**
	 * @returns { string }
	 */
	get PeakRiskLevelConfidence()
	{
		return this._peak_risk_level_conf.innerHTML;
	}

	/**
	 * @param { string }
	 */
	set PeakRiskLevelConfidence(val)
	{
		this._peak_risk_level_conf.innerHTML = val;
	}

	/**
	 * @returns { string }
	 */
	get AverageRiskLevelConfidence()
	{
		return this._average_risk_level_conf.innerText;
	}

	/**
	 * @param { string }
	 */
	set AverageRiskLevelConfidence(val)
	{
		this._average_risk_level_conf.innerHTML = val;
	}

	/**
	 * @returns { Datetime }
	 */
	get CreationTime()
	{
		return this._CreateTime;
	}

	/**
	 * @param { Datetime }
	 */
	set CreationTime(val)
	{
		this._CreateTime = val;
		if(val!==null)
		{
			this._creation_time.innerHTML = val.toString();
		}
	}

	/**
	 * @returns { IfaHra }
	 */
	get Hra()
	{
		return new IfaHra(this);
	}

	/**
	 * @param { IfaHra }
	 */
	set Hra(hra)
	{
		IfaHra.copyFields(hra, this);
	}
}

customElements.define('jhrm-ifa-hra-display', IfaHraDisplay);

export class IfaHraBrowser extends HTMLElement
{
	constructor()
	{
		super();

		let btnMrg = IfaHraBrowser._BTN_MARGIN;

		this.innerHTML = `
			<div style="display: grid; width: auto; grid-template-columns: min-content auto min-content 1fr;">
				
				<jhrm-swap-space id="swap_assoc_hras" style="grid-row: 1; grid-column: 1/4;">
				</jhrm-swap-space>
				<jhrm-align left style="grid-row: 2; grid-column: 1; white-space: nowrap;">
					<button id="prev_button" style="width: 10em; margin-right: ${btnMrg};">Previous</button>
					<button id="next_button" style="width: 10em; margin-right: ${btnMrg};">Next</button>
				</jhrm-align>
				<jhrm-align center middle style="grid-row: 2; grid-column: 2;">
					<span id="page_count"></span>
				</jhrm-align>
				<jhrm-align right style="grid-row: 2; grid-column: 3;">
					<span id="button_area" style="white-space: nowrap;"></span>
				</jhrm-align>
			</div>
		`;

		this._prev_button = selectAndStripId(this, "prev_button");
		this._next_button = selectAndStripId(this, "next_button");
		this._page_count = selectAndStripId(this, "page_count");
		this._swap_assoc_hras = selectAndStripId(this, "swap_assoc_hras");
		this._button_area = selectAndStripId(this, "button_area");

		this._block_events = 0;
		this._elm_at_lock = null;

		this._updateButtons = () =>
		{
			let enableButton = (btn, enabled) =>
			{
				if ( enabled )
					btn.removeAttribute("disabled");
				else
					btn.setAttribute("disabled", "");
			};
			
			let buttons_visiblity = "hidden";

			if ( this._swap_assoc_hras.Count > 1 )
			{
				buttons_visiblity = "inherit";
				this._page_count.innerText = `${this._swap_assoc_hras.VisibleIndex} / ${this._swap_assoc_hras.Count - 1}`;
			}
			
			this._next_button.style.visibility = buttons_visiblity;
			this._prev_button.style.visibility = buttons_visiblity;
			this._page_count.style.visibility = buttons_visiblity;
			this._button_area.style.visibility = buttons_visiblity;
			
			enableButton(this._prev_button, this._swap_assoc_hras.VisibleIndex > 1);
			enableButton(this._next_button, this._swap_assoc_hras.Count > 0 && !this._swap_assoc_hras.isOnLastElement);
		};

		this._prev_button.onclick = () =>
		{
			try
			{
				this._pushChangeLock();
				this._swap_assoc_hras.previous();
			}
			finally
			{
				this._popChangeLock();
			}
		};

		this._next_button.onclick = () =>
		{
			try
			{
				this._pushChangeLock();
				this._swap_assoc_hras.next();
			}
			finally
			{
				this._popChangeLock();
			}
		};

		this.clear();
	}

	_pushChangeLock()
	{
		if ( 0 == this._block_events)
			this._elm_at_lock = this._swap_assoc_hras.VisibleElement;
		
		++this._block_events;
	}

	_popChangeLock()
	{
		if (0 == --this._block_events)
		{
			if (this._elm_at_lock != this._swap_assoc_hras.VisibleElement)
			{
				this._updateButtons();
				this._raiseChangeEvent();
			}
			
			this._elm_at_lock = null;
		}
	}

	_raiseChangeEvent()
	{
		if (this._block_events == 0 )
		{
			let currSel = ( this._swap_assoc_hras.VisibleIndex == 0 ) ?
				undefined : new IfaHra(this._swap_assoc_hras.VisibleElement);

			if (this._onchange)
				this._onchange(currSel);

			var event = new CustomEvent(
				"change",
				{
					detail: currSel
				}
			);
		}
	}

	/**
	 * @returns An array of bjects that can be used to get and
	 *  set the fields of each item.
	 * 
	 * @returns { IfaHra[] }
	 */
	get HRAs()
	{
		let ret = [];

		for (let i = 1; i < this._swap_assoc_hras.Count; ++i)
			ret.push(this._swap_assoc_hras.elementAt(i));

		return ret;
	}

	/**
	 * @param { IfaHra[] } vals
	 */
	set HRAs(vals)
	{
		try
		{
			this._pushChangeLock();

			this.clear();

			vals.forEach(
				function (item)
				{
					this._add_hra(item);
				}.bind(this)
			);
			
			if (vals.length > 0)
				this._swap_assoc_hras.VisibleIndex = 1;
			
			this._updateButtons();
		}
		finally
		{
			this._popChangeLock();
		}
	}

	clear()
	{
		try
		{
			this._pushChangeLock();

			this._swap_assoc_hras.clear();

			let empty_msg_elm = new JhrmAlign();
			empty_msg_elm.innerHTML = `<span>There are currently no HRAs to display.</span>`;
			empty_msg_elm.Alignment = JhrmAlign.Middle;
			empty_msg_elm.Alignment = JhrmAlign.Center;
			empty_msg_elm.style.whiteSpace = "nowrap";

			this._swap_assoc_hras.appendPage(empty_msg_elm);
			this._updateButtons();
		}
		finally
		{
			this._popChangeLock();
		}
	}

	/**
	 * @param { Number } index
	 *  One based index of the HRA summary to remove.
	 * 
	 * @returns { IfaHra }
	 *  An object with the values of the removed HRA.
	 */
	removeAtIndex(index)
	{
		try
		{
			this._pushChangeLock();

			let elm = this._swap_assoc_hras.deleteAt(index);
			this._updateButtons();

			return new IfaHra(elm);
		}
		catch (error)
		{
			return undefined;
		}
		finally
		{
			this._popChangeLock();
		}
	}

	removeHRA(id)
	{
		try
		{
			this._pushChangeLock();
			
			if (undefined == id)
				return undefined;

			let index = -1;
			let found = this._swap_assoc_hras.AllPages.find(
				(elm, idx) =>
				{
					if (elm.Id == id)
					{
						index = idx;
						return true;
					}

					return false;
				}
			);

			return this.removeAtIndex(index);
		}
		finally
		{
			this._popChangeLock();
		}
	}

	_add_hra(hra)
	{
		try
		{
			this._pushChangeLock();

			if (!hra)
				throw new Error("hra cannot be undefined.");

			let elm = new IfaHraDisplay();
			elm.Hra = hra;

			this._swap_assoc_hras.appendPage(elm);
		}
		finally
		{
			this._popChangeLock();
		}
	}

	addHra(hra)
	{
		try
		{
			this._pushChangeLock();

			this._add_hra(hra);

			// Two because one page would be the one displayed when there are no HRAs
			// to display, and a new page would be the second.
			if (this._swap_assoc_hras.Count == 2)
				this._swap_assoc_hras.VisibleIndex = 1;
			
			this._updateButtons();
		}
		finally
		{
			this._popChangeLock();
		}
	}

	/**
	 * @brief
	 *  Adds a button to the lower right area of the component, and returns 
	 *  a reference to that button.
	 * 
	 * @param { string } text 
	 * @param { function } handler 
	 */
	addButton(text, handler)
	{
		let ret = document.createElement('button');
		ret.style.marginLeft = IfaHraBrowser._BTN_MARGIN;
		ret.innerHTML = text;
		ret.onclick = handler;

		this._button_area.append(ret);

		return ret;
	}

	/**
	 * @returns
	 *  The currently displayed HRA with the same object specs used by the HRAs get/set functions.
	 */
	get CurrentHRA()
	{
		let idx = this._swap_assoc_hras.VisibleIndex;
		if ( idx > 0 )
		{
			let ret = new IfaHra(this._swap_assoc_hras.VisibleElement);

			return ret;
		}

		return undefined;
	}

	set onchange(handler)
	{
		this._onchange = handler;
	}
}

IfaHraBrowser._BTN_MARGIN = "0.4rem";

customElements.define('jhrm-ifa-hra-browser', IfaHraBrowser);