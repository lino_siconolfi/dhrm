import { JhrmTooltip } from "../tooltip.js"
import { MaterialIcon } from "../material_icon.js"
import { Request } from "../request.js"
import { IFA_ATTACHMENT_TYPE } from "../shared/Constants.js";

let createTooltip = function(tip)
{
	let icon = MaterialIcon.Create("help_outline");
	icon.style.fontSize = "0.9em";

	let tooltip = new JhrmTooltip();
	tooltip.Target = icon;
	tooltip.append(tip);

	let ret = document.createElement('span');
	ret.append(icon, tooltip);

	return ret;
}

let createLabel = function(text, row, col)
{
	let ret = document.createElement('div');
	ret.style.gridRow = row.toString();
	ret.style.gridColumn = col.toString();
	ret.style.whiteSpace = "nowrap";

	ret.append(text);

	return ret;
};

export class IFAAttachmentsForm extends HTMLElement
{
	constructor()
	{
		super();
		this.innerHTML = `
			<input type="file" style="display: none;">
			<input type="text" value="Click Here to Select a File">
			<button class="upload-button small-button">Upload</button>
			<button class="download-button small-button">Download</button>
		`;
		this._Built = false;
	}

	connectedCallback()
	{
		if ( !this._Built )
		{
			this._uploadButton = this.querySelector('.upload-button');
			this._downloadButton = this.querySelector('.download-button');
			this._fileInput = this.querySelector('input[type=file]');
			this._textInput = this.querySelector('input[type=text]');
			this._type = (this.hasAttribute('type') && IFA_ATTACHMENT_TYPE.isValid(this.getAttribute('type')))
				? this.getAttribute('type') 
				: IFA_ATTACHMENT_TYPE.OTHER;
			this._files = [];
			this._ifaId = new URLSearchParams(window.location.search).get('ifa');

			// Set the type of files this input allows. 
			if(this.hasAttribute('accept'))
			{
				this.accept = this.getAttribute('accept');
			}

			// Set the maximum allowed size 
			this._max = 10485760;
			if(this.hasAttribute('max'))
			{
				let max = parseInt( this.getAttribute('max'), 10);
				if(max)
				{
					this._max = max;
				}
			}
		

			// Using Promise syntax here because connectedCallback can't be async. 
			// We will fetch all files for the given attachment type. 
			if(this._ifaId)
			{
				Request.get('/api/ifa/report/' + this._ifaId + '/fileattachments', {
					'attachment_type' : this._type
				},)
				.then(files => {
					this.files = files;
				});
			}

			this._textInput.onclick = (evt) => {
				this._fileInput.click(evt);
			}

			this._uploadButton.onclick = this.upload.bind(this);
			this._downloadButton.onclick = this.download.bind(this);

			this._fileInput.addEventListener("change", (evt) => {
				if(this._fileInput.value)
				{
					this._textInput.value = this._fileInput.value.match(/[\/\\]([\w\d\s\.\-\(\)]+)$/)[1];
				}
			});

			this._Built = true;
		}
	}

	get value() 
	{
		return this._fileInput.value;
	}

	/**
	 * @param {fileObj[]} val
	 */
	set files(val)
	{
		if(!Array.isArray(val))
		{
			val = [val];
		}
		this._files = val;

		// If there is no file, do not allow download.
		if(this._files.length == 0)
		{
			this._textInput.value='Click Here to Select a File';
			this._downloadButton.style.visibility = 'hidden';
		}
		else 
		{
			// Only one file is supported for now. 
			this._textInput.value = this._files[0].file_name;
			this._downloadButton.style.visibility = 'visible';
		}
	}

	/**
	 * @returns {fileobj[]}
	 */
	get files()
	{
		return this._files;
	}

	/**
	 * @pram {string} val;
	 */
	set accept(val)
	{
		this._accept = val;
		this._fileInput.accept = this._accept;
	}

	/**
	 * @returns {string}
	 */
	get accept()
	{
		return this._accept;
	}


	/**
	 * Upload a file if one is selected in the file input. This will replace 
	 * the existing file, if one exists, because only a single file is currenty 
	 * supproted. 
	 */
	async upload()
	{
		try
		{
			if (this._fileInput.files.length > 0 && this._ifaId)
			{
				if(this._fileInput.files[0].size > this._max)
				{
					alert("File exceeds maximum allowed size.");
					return;
				}
				let response = await Request.postUpload(
					'/api/ifa/report/' + this._ifaId + '/uploadfile', {

						// Only one file is supported for now. 
						uploadfile : this._fileInput.files[0],
						attachment_type : this._type

					});

				this.files = response.file;
				alert(response.status);
			}
			else
			{
				alert("Select a File to Upload");
			}
		}
		catch(error)
		{
			alert(error.message);
		}
	}

	/**
	 * Donwload the file connected to this element. Only a single file is currently 
	 * supported. 
	 */
	download()
	{
		if(this.files.length > 0)
		{
			let URL = '/api/ifa/report/' + this._ifaId + '/downloadfile/' + this.files[0].id;
		    window.open(URL, "_blank");
		}
	}
}

customElements.define('jhrm-ifa-attachments-form', IFAAttachmentsForm);