import { selectAndStripId, removeElement, arrayDifference, arrayUnion } from "../utility.js"
import { YesNoUnknownOption, YesNoUnknownSelect,
		 PushTextEdit, DataTimeEdit,
		 YesNoSelect }
		 from "../form_components.js"
import { DateFormat } from "../dateformat.js"
import { JhrmRadioGroup } from "../radio_group.js"
import { JhrmDialog } from "../dialog.js"
import { JhrmTabs } from "../tabs.js"
import { JhrmSwapSpace } from "../swap_space.js"
import { JhrmSelect } from "../select.js"
import { JhrmAlign } from "../align.js"
import { MaterialIcon } from "../material_icon.js"
import { IFA_BLAST_GAUGE_COLOR, IFA_BLAST_GAUGE_LOCATION, IFA_CONCUSSIVE_PPE } from "../shared/Constants.js"
import { Datetime } from "../shared/datetime.js"
import { FormValidator } from "../validate.js";
import { RiskAssessment } from "../risk_assessment.js"
import { Request } from "../request.js";
import { IfaHra, IfaHraDisplay, IfaHraBrowser } from "./ifa_hra.js"

const not_implmented = function()
{
	alert("Not Yet Implemented.");
};

class PpeEditor extends HTMLElement
{
	constructor()
	{
		super();

		this.innerHTML = `
			<jhrm-tabs class="jhrm-personnel-ppe-editor">
				<jhrm-swap-space size-to-visible>
					<div data-tab="PCE">
						<div class="ifa-field">
							<label>PPE Worn</label>
							<jhrm-constant-options 
								class="ifa-input" 
								constant="IFA_CONCUSSIVE_PPE" 
								type="checkbox" 
								id="ConcussivePpeInput">
							</jhrm-constant-options>
						</div>
						<jhrm-constant-options-togglable 
							class="ifa-field ifa-required" 
							id="ConcussivePpeOther"
							show="OTHER">
							<label>Please Describe</label>
							<input data-validate-required class="ifa-input" id="ConcussivePpeOtherInput" type="text">
						</jhrm-constant-options-togglable>
						<p>* PCE is a Potentially Concussive Event</p>
					</div>
					<div data-tab="Chemical">
						<div class="ifa-field">
							<label>PPE Worn</label>
							<jhrm-constant-options 
								class="ifa-input" 
								constant="IFA_CHEMICAL_PPE" 
								type="checkbox" 
								id="ChemicalPpeInput">
							</jhrm-constant-options>
						</div>
						<jhrm-constant-options-togglable 
							class="ifa-field ifa-required" 
							id="ChemicalPpeOther"
							show="OTHER">
							<label>Please Describe</label>
							<input data-validate-required class="ifa-input" id="ChemicalPpeOtherInput" type="text">
						</jhrm-constant-options-togglable>
					</div>
				</jhrm-swap-space>
			</jhrm-tabs>
		`;

		this._css = `
			<style id="jhrm-personnel-ppe-styles">
				.jhrm-personnel-ppe-editor jhrm-constant-options label 
				{
					display:block;
				}
			</style>
		`;

		if(!document.getElementById('jhrm-personnel-ppe-styles'))
		{
			document.head.innerHTML += this._css;
		}

		this._ConcussivePpeInput = selectAndStripId(this, "ConcussivePpeInput");
		this._ConcussivePpeOtherInput = selectAndStripId(this, "ConcussivePpeOtherInput");
		this._ConcussivePpeOther = selectAndStripId(this, "ConcussivePpeOther");
		this._ConcussivePpeOther.Source = this._ConcussivePpeInput;

		this._ChemicalPpeInput = selectAndStripId(this, "ChemicalPpeInput");
		this._ChemicalPpeOtherInput = selectAndStripId(this, "ChemicalPpeOtherInput");
		this._ChemicalPpeOther = selectAndStripId(this, "ChemicalPpeOther");
		this._ChemicalPpeOther.Source = this._ChemicalPpeInput;
	}

	/**
	 * @returns {IFA_CHEMICAL_PPE[]}
	 */
	get ChemicalPpe() 
	{
		return this._ChemicalPpeInput.value;
	}

	/**
	 * @param {IFA_CHEMICAL_PPE[]} val
	 */
	set ChemicalPpe(val) 
	{
		this._ChemicalPpeInput.value = val;
	}

	/**
	 * @returns {string}
	 */
	get ChemicalPpeOther() 
	{
		return this._ChemicalPpeOtherInput.value;
	}

	/**
	 * @param {string} val
	 */
	set ChemicalPpeOther(val)
	{
		this._ChemicalPpeOtherInput.value = val;
	}

	/**
	 * @returns {IFA_CONCUSSIVE_PPE[]}
	 */
	get ConcussivePpe() 
	{
		return this._ConcussivePpeInput.value;
	}

	/**
	 * @param {IFA_CONCUSSIVE_PPE[]} val
	 */
	set ConcussivePpe(val) 
	{
		this._ConcussivePpeInput.value = val;
	}

	/**
	 * @returns {string}
	 */
	get ConcussivePpeOther() 
	{
		return this._ConcussivePpeOtherInput.value;
	}

	/**
	 * @param {string} val
	 */
	set ConcussivePpeOther(val)
	{
		this._ConcussivePpeOtherInput.value = val;
	}

}

customElements.define('jhrm-ifa-ppe-editor', PpeEditor);

class DetectorSensorEditorConcussiveRow extends HTMLTableRowElement
{
	constructor()
	{
		super();

		this._color_radio_group = new JhrmRadioGroup();

		this.innerHTML = `
			<th id="elm_blast_gauge" style="background-color: lightgray;"></th>
			<td><jhrm-push-text-edit id="elm_set_serial"></jhrm-push-text-edit></td>
			<td>${this._color_radio_group.genElementText(IFA_BLAST_GAUGE_COLOR.RED)}</td>
			<td>${this._color_radio_group.genElementText(IFA_BLAST_GAUGE_COLOR.YELLOW)}</td>
			<td>${this._color_radio_group.genElementText(IFA_BLAST_GAUGE_COLOR.FLASHING_GREEN)}</td>
			<td>${this._color_radio_group.genElementText(IFA_BLAST_GAUGE_COLOR.GREEN)}</td>
			<td><jhrm-push-text-edit id="elm_peak_op"></jhrm-push-text-edit></td>
		`;

		this._elm_blast_gauge = selectAndStripId(this, "elm_blast_gauge");
		this._elm_set_serial = selectAndStripId(this, "elm_set_serial");
		this._elm_peak_op = selectAndStripId(this, "elm_peak_op");

		this._elm_peak_op.Value = "0.0";
		this._elm_peak_op.onchange = function(next_val)
		{
			if ( isNaN(next_val) )
			{
				alert("PeakOp must be a number.");
				return false;
			}

			return true;
		};
	}

	get BlastGauge()
	{
		return this._elm_blast_gauge.innerText;
	}

	set BlastGauge(val)
	{
		this._elm_blast_gauge.innerText = val;
	}

	/**
	 * @returns { string }
	 */
	get SetSerial()
	{
		return this._elm_set_serial.Value
	}

	/**
	 * @param { string }
	 */
	set SetSerial(val)
	{
		this._elm_set_serial.Value = val;
	}

	/**
	 * @returns { IFA_BLAST_GAUGE_COLOR } 
	 */
	get Color()
	{
		return this._color_radio_group.Value;
	}


	/**
	 * @param { IFA_BLAST_GAUGE_COLOR } val
	 */
	set Color(val)
	{
		if(!IFA_BLAST_GAUGE_COLOR.isValid(val))
		{
			throw new Error("Argument is not a valid IFA Blast Gauge Color");
		}
		this._color_radio_group.Value = val;
	}

	/**
	 * @returns { Number }
	 */
	get PeakOp()
	{
		return Number.parseFloat(this._elm_peak_op.Value);
	}

	/**
	 * @param { Number }
	 */
	set PeakOp(val)
	{
		if ( isNaN(val) )
			throw new Error("PeakOp must be a number.");

		this._elm_peak_op.Value = val.toString();
	}

	get _EditingActive()
	{
		return (this._elm_set_serial.Active || this._elm_peak_op.Active);
	}
}

customElements.define('jhrm-detector-sensor-editor-concussive-row', DetectorSensorEditorConcussiveRow, {extends: 'tr'});

/**
 * This is a component provides a user controls to add and remove HRAs that are
 * associated to an incedent.  Client code initializes the component by setting
 * the AssociatedHras, LocationHras, and IfaHras properties, then presents the 
 * component to the user.  The AssociatedHras and AddedToALL can be used to get
 * the modifications to the sets from the user during the lifecycle of the component.
 */
class AssociatedDriHraEditor extends HTMLElement
{
	constructor()
	{
		super();

		this.innerHTML = `
			<jhrm-ifa-hra-browser id="associated_chemical_browser"></jhrm-ifa-hra-browser>
			<h3>Search and select DRI HRAs</h3>
			<div id="source_select_section" style="white-space: nowrap; margin-top: 1em;">
			</div>
			<jhrm-swap-space id="hra_browser_swap">
				<jhrm-ifa-hra-browser id="add_chemical_ifa_browser"></jhrm-ifa-hra-browser>
				<jhrm-ifa-hra-browser id="add_chemical_location_browser"></jhrm-ifa-hra-browser>
			</jhrm-swap-space>
		`;

		this._associated_chemical_browser = selectAndStripId(this, "associated_chemical_browser");
		let add_chemical_ifa_browser = selectAndStripId(this, "add_chemical_ifa_browser");
		let add_chemical_location_browser = selectAndStripId(this, "add_chemical_location_browser");

		this._source_items = {
			ByIfa : {
				Browser : add_chemical_ifa_browser,
				AddButton : add_chemical_ifa_browser.addButton("Add", this._onAddButton.bind(this) ),
				AddToAllButton : add_chemical_ifa_browser.addButton("Add to All", this._onAddToAllButton.bind(this) )
			},
			ByLocation : {
				Browser : add_chemical_location_browser,
				AddButton : add_chemical_location_browser.addButton("Add", this._onAddButton.bind(this) ),
				AddToAllButton : add_chemical_location_browser.addButton("Add to All", this._onAddToAllButton.bind(this) )
			}
		};

		this._remove_button = this._associated_chemical_browser.addButton("Remove", this._onRemoveButton.bind(this) );
		this._hra_browser_swap = selectAndStripId(this, "hra_browser_swap");

		this._hra_source = new JhrmRadioGroup();
		this._hra_source.onchange = () =>
		{
			this._hra_browser_swap.VisibleElement = this._hra_source.Value.Browser;
		};		

		this._source_select_section = selectAndStripId(this, "source_select_section");
		this._source_select_section.append(
			...this._hra_source.genElementAndLabel(this._source_items.ByIfa, "From this IFA"),
			...this._hra_source.genElementAndLabel(this._source_items.ByLocation, "From this Location")
		);

		this._source_items.ByIfa.Browser.onchange =
			() =>
			{
				this._updateAddButton(this._source_items.ByIfa);
			};
			
		this._source_items.ByLocation.Browser.onchange =
			() =>
			{
				this._updateAddButton(this._source_items.ByLocation);
			};

		this._added_to_all = [];
	}

	_currentAddHra()
	{
		if ( this._hra_browser_swap.VisibleElement == this._add_chemical_ifa_table )
			return this._add_chemical_ifa_table.CurrentHRA;
		else if ( this._hra_browser_swap.VisibleElement == this._add_chemical_location_table )
			return this._add_chemical_location_table.CurrentHRA;

		return undefined;
	}

	_updateAddButton(source_item)
	{
		if (undefined == source_item )
		{
			this._updateAddButton(this._source_items.ByIfa);
			this._updateAddButton(this._source_items.ByLocation);
		}
		else
		{
			let visible_add_hra = source_item.Browser.CurrentHRA;

			if ( visible_add_hra )
			{
				let assoc_hras = this._associated_chemical_browser.HRAs;
				let add_button = source_item.AddButton;

				let found_index = assoc_hras.findIndex(
					function(item)
					{
						return (item.Id == visible_add_hra.Id)
					}
				);
				
				add_button.disabled = ( 0 <= found_index );
			}
		}
	}

	/**
	 * Called when the "Remove" button is clicked.
	 */
	_onRemoveButton()
	{
		this._associated_chemical_browser.removeHRA(
			this._associated_chemical_browser.CurrentHRA.Id
		);

		this._updateAddButton();
	}

	/**
	 * Called when the "Add" button is clicked.
	 */
	_onAddButton()
	{
		this._associated_chemical_browser.addHra(
			this._hra_source.Value.Browser.CurrentHRA
		);

		this._updateAddButton();
	}

	/**
	 * Called when the "Add to All" button is clicked.
	 */
	_onAddToAllButton()
	{
		let assoc_hras = this._associated_chemical_browser.HRAs;
		let current_hra = this._hra_source.Value.Browser.CurrentHRA;

		if ( 0 > assoc_hras.findIndex( elm => elm.Id == current_hra.Id ) )
		{
			this._associated_chemical_browser.addHra(current_hra);
			this._updateAddButton();
		}

		if ( 0 > this._added_to_all.findIndex( elm => elm.Id == current_hra.id ) )
			this._added_to_all.push( new IfaHra(current_hra) );
	}

	/**
	 * Sets the HRAs that are shown as associated to the incident.
	 * 
	 * @returns { IfaHra[] }
	 */
	get AssociatedHras()
	{
		return this._associated_chemical_browser.HRAs.map(
			elm => new IfaHra(elm)
		);
	}

	/**
	 * Get the HRAs that are associated with the incident.
	 * 
	 * @param { IfaHra[] } hras
	 */
	set AssociatedHras(hras)
	{
		this._associated_chemical_browser.HRAs = hras;
		this._updateAddButton();
	}

	/**
	 * @returns { IfaHra[] }
	 */
	get LocationHras()
	{
		return this._source_items.ByLocation.Browser.HRAs.map(
			elm => new IfaHra(elm)
		);
	}

	/**
	 * @param { IfaHra[] } hras
	 */
	set LocationHras(hras)
	{
		this._source_items.ByLocation.Browser.HRAs = hras;
	}

	/**
	 * @returns { IfaHra[] }
	 */
	get IfaHras()
	{
		return this._source_items.ByIfa.Browser.HRAs.map(
			elm => new IfaHra(elm)
		);
	}

	/**
	 * @param { IfaHra[] } hras
	 */
	set IfaHras(hras)
	{
		this._source_items.ByIfa.Browser.HRAs = hras;
	}

	/**
	 * @returns { IfaHra[] }
	 */
	get AddedToAll()
	{
		return [...this._added_to_all];
	}

	/**
	 * @returns { IfaHra[] }
	 */
	set AddedToAll(hras)
	{
		this._added_to_all = hras.map(
			item => new IfaHra(item)
		);
	}
};

AssociatedDriHraEditor._HRA_SOURCE = {
	IFA : 1,
	Location : 2
};

customElements.define('jhrm-associated-dri-hra-editor', AssociatedDriHraEditor);

class DetectorSensorEditor extends HTMLElement
{
	static styleCellsBorders(parent)
	{
		let table_elements = Array.from(parent.getElementsByTagName('th'));
		table_elements.push( ...Array.from(parent.getElementsByTagName('td')));

		table_elements.forEach(
			element => {
				element.style.border = "2px solid black";
				element.style.padding = "1em";
				element.style.textAlign = "center";
			}
		);
	}

	constructor()
	{
		super();

		this.innerHTML = `
			<jhrm-tabs>
				<jhrm-swap-space style="margin: 1em;">
					<div data-tab="PCE">
						<div>
							<span style="display: inline-block; margin-right: 3em">Name: <span id="concussive_name"></span></span>
							<span style="display: inline-block; margin-right: 3em">DODID#: <span id="concussive_dodid"></span></span>
							<span>Date/Time: <jhrm-date-time-edit id="concussive_date_time"></jhrm-date-time-edit></span>
						</div>
						<table id="concussive_table" style="margin: 1em 0 1em 0; border-collapse: collapse;">
							<tr>
								<th style="background-color: lightblue;">Blast Gauge <material-icon icon="help_outline" id="DetectorSensorConcussiveHelp"></material-icon> </th>
								<jhrm-tooltip target="DetectorSensorConcussiveHelp">
									Review/enter blast gauge data for each sensor, if available.
								</jhrm-tooltip>
								<th style="background-color: lightblue;">Set Serial #</th>
								<th style="background-color: red;">${IFA_BLAST_GAUGE_COLOR.toString(IFA_BLAST_GAUGE_COLOR.RED)}</th>
								<th style="background-color: yellow;">${IFA_BLAST_GAUGE_COLOR.toString(IFA_BLAST_GAUGE_COLOR.YELLOW)}</th>
								<th style="background-color: lightgreen;">${IFA_BLAST_GAUGE_COLOR.toString(IFA_BLAST_GAUGE_COLOR.FLASHING_GREEN)}</th>
								<th style="background-color: green;">${IFA_BLAST_GAUGE_COLOR.toString(IFA_BLAST_GAUGE_COLOR.GREEN)}</th>
								<th style="background-color: lightblue;">Peak OP (psi)</th>
							</tr>
						</table>
						<fieldset style="display: inline-block;">
							<legend>Add Blast Gauge</legend>
							<input id="concussive_add_blast_gauge_text"></input>
							<button id="concussive_add_blast_gauge_button">Add</button>
						</fieldset>
						<p>* PCE is a Potentially Concussive Event</p>
					</div>
					<div data-tab="Chemical">
						<H2 sytle="white-space: nowrap;">Associated DRI HRA <material-icon icon="help_outline" id="ChemicalDetectorSensorHelp"></material-icon></H2>
						<jhrm-tooltip target="ChemicalDetectorSensorHelp">
							Search and select the applicable health risk assessment.
						</jhrm-tooltip>
						<jhrm-associated-dri-hra-editor id="associated_dri_editor"></jhrm-associated-dri-hra-editor>
					</div>
					<div data-tab="Radiological"></div>
					<div data-tab="Other"></div>
				</jhrm-swap-space>
			</jhrm-tabs>
		`;

		this._concussive_elm_name = selectAndStripId(this, "concussive_name");
		this._concussive_elm_dodid = selectAndStripId(this, "concussive_dodid");
		this._concussive_elm_date_time = selectAndStripId(this, "concussive_date_time");
		this._concussive_table = selectAndStripId(this, "concussive_table");

		this._associated_dri_editor = selectAndStripId(this, "associated_dri_editor");
		
		let concussive_add_blast_gauge_button = selectAndStripId(this, "concussive_add_blast_gauge_button");
		let concussive_add_blast_gauge_text = selectAndStripId(this, "concussive_add_blast_gauge_text");

		DetectorSensorEditor.styleCellsBorders(this._concussive_table);

		Object.keys(IFA_BLAST_GAUGE_LOCATION).forEach(item=>{
			let gauge = this.newRow();
			gauge.BlastGauge = IFA_BLAST_GAUGE_LOCATION.toString(item);
		});
		this.ConcussiveDate = new Date();

		concussive_add_blast_gauge_button.onclick = function()
		{
			let next_row = this.newRow();
			next_row.BlastGauge = concussive_add_blast_gauge_text.value;
			concussive_add_blast_gauge_text.value = "";
		}.bind(this);
	}

	/**
	 * Create a new row, style it correctly, and add it to the table. 
	 * 
	 * @returns { DetectorSensorEditorConcussiveRow }
	 */
	newRow()
	{
		let next_row = new DetectorSensorEditorConcussiveRow();
		DetectorSensorEditor.styleCellsBorders(next_row);
		this._concussive_table.append(next_row);

		return next_row;
	}

	/**
	 * Clear all rows. 
	 * 
	 * @returns { void }
	 */
	clear()
	{
		this.ConcussiveRows.forEach(item=>{
			removeElement(item);
		});
	}
	
	/**
	 * @param { string }
	 */
	set ConcussiveName(val)
	{
		this._concussive_elm_name.innerText = val;
	}

	/**
	 * @returns { string }
	 */
	get ConcussiveName()
	{
		return this._concussive_elm_name.innerText;
	}

	/**
	 * @returns { string }
	 */
	set ConcussiveDodid(val)
	{
		this._concussive_elm_dodid.innerText = val;
	}

	/**
	 * @param { string }
	 */
	get ConcussiveDodid()
	{
		return this._concussive_elm_dodid.innerText;
	}

	/**
	 * @param { Date }
	 */
	set ConcussiveDate(val)
	{
		this._concussive_elm_date_time.Value = val;
	}

	/**
	 * @returns { Date }
	 */
	get ConcussiveDate()
	{
		return this._concussive_elm_date_time.Value;
	}

	get ConcussiveRows()
	{
		let ret = Array.from(this._concussive_table.getElementsByTagName('tr'));
		ret.shift();

		return ret;
	}

	/**
	 * @returns { AssociatedDriHraEditor }
	 */
	get ChemicalInfo()
	{
		return this._associated_dri_editor;
	}
}

customElements.define('jhrm-ifa-detector-sensor-editor', DetectorSensorEditor);

class MedicalInformationEditor extends HTMLElement
{
	constructor()
	{
		super();

		this.innerHTML = `
			<div>
				<span style="text-decoration: underline; font-size: 110%; margin-bottom: 0.5em;">Medical Information</span>
				<span style="font-size: 80%;">* means required</span>
			</div>
			<div class="ifa-field" style="margin: 0.5em 0;">
				<label>Personnel Decontamination Performed?</label>
				<jhrm-constant-options value="UNKNOWN" class="ifa-input" constant="IFA_TRINARY" type="radio" id="PersDeconPerf"></jhrm-constant-options>
			</div>
			<jhrm-constant-options-togglable id="DeconDescrToggle" class="ifa-field ifa-required" show="YES" style="margin: 0.5em 0;">
				<label>Decontamination Description</label> 
				<input data-validate-required class="ifa-input" id="DeconDescr" type="text">
			</jhrm-constant-options-togglable>
			<div class="ifa-field" style="margin: 0.5em 0;">
				<label>Sought Medical Treatment</label>
				<jhrm-constant-options value="UNKNOWN" class="ifa-input" constant="IFA_TRINARY" type="radio" id="SoughtMedTreat"></jhrm-constant-options>
			</div>
			
			<jhrm-constant-options-togglable id="MedicalCareToggle" class="ifa-field" show="YES" style="margin: 0.5em 0;">
				<fieldset>
					<legend>Type of Medical Care Received:
					<material-icon icon="help_outline" id="IfaMedicalCareHelp"></material-icon>
					</legend>
					<jhrm-tooltip target="IfaMedicalCareHelp">
							Describe location (if known) for each type of medical care received.
					</jhrm-tooltip>
					<div style="display:grid;grid-template-columns:1fr 1fr">
						<label style="grid-column:1"><input type="checkbox" id="UnitMedic">Unit Medic</label>
						<jhrm-checkbox-togglable id="UnitMedicDescrToggle" class="ifa-field" show="1">
							<label>Describe Location</label> 
							<input class="ifa-input" id="UnitMedicDescr" type="text">
						</jhrm-checkbox-togglable>
						<label style="grid-column:1"><input type="checkbox" id="BattalionAideStation">Battalion Aide Station</label>
						<jhrm-checkbox-togglable id="BattalionAideStationDescrToggle" class="ifa-field" show="1">
							<label>Describe Location</label> 
							<input class="ifa-input" id="BattalionAideStationDescr" type="text">
						</jhrm-checkbox-togglable>
						<label style="grid-column:1"><input type="checkbox" id="MtfCs">MTF/CS</label>
						<jhrm-checkbox-togglable id="MtfCsDescrToggle" class="ifa-field" show="1">
							<label>Describe Location</label> 
							<input class="ifa-input" id="MtfCsDescr" type="text">
						</jhrm-checkbox-togglable>
					</div>
				</fieldset>
			</jhrm-constant-options-togglable>

			<div style="margin: 0.5em 0;">*Resulting Duty Status: <jhrm-select id="ResultingDutyStatus"></jhrm-select></div>
			<div style="display: grid; grid-template-columns: auto 1fr; gap: 0.5em;">
				<jhrm-align top style="grid-row: 1; grid-column: 1;">Comments: </jhrm-align>
				<textarea style="grid-row: 1; grid-column: 2; height: 6em; resize: none;" id="Comments"></textarea>
			<div>
		`;

		this._PersDeconPerf = selectAndStripId(this, "PersDeconPerf");
		this._DeconDescr = selectAndStripId(this, "DeconDescr");
		this._SoughtMedTreat = selectAndStripId(this, "SoughtMedTreat");
		this._UnitMedic = selectAndStripId(this, "UnitMedic");
		this._UnitMedicDescr = selectAndStripId(this, "UnitMedicDescr");
		this._BattalionAideStation = selectAndStripId(this, "BattalionAideStation");
		this._BattalionAideStationDescr = selectAndStripId(this, "BattalionAideStationDescr");
		this._MtfCs = selectAndStripId(this, "MtfCs");
		this._MtfCsDescr = selectAndStripId(this, "MtfCsDescr");
		this._Comments = selectAndStripId(this, "Comments");
		this._DeconDescrToggle = selectAndStripId(this, "DeconDescrToggle");
		this._MedicalCareToggle = selectAndStripId(this, "MedicalCareToggle");
		this._UnitMedicDescrToggle = selectAndStripId(this, "UnitMedicDescrToggle");
		this._BattalionAideStationDescrToggle = selectAndStripId(this, "BattalionAideStationDescrToggle");
		this._MtfCsDescrToggle = selectAndStripId(this, "MtfCsDescrToggle");

		// Do not rely on IDs for the source, as we strip them. 
		this._DeconDescrToggle.Source = this._PersDeconPerf;
		this._MedicalCareToggle.Source = this._SoughtMedTreat;
		this._UnitMedicDescrToggle.Source = this._UnitMedic;
		this._BattalionAideStationDescrToggle.Source = this._BattalionAideStation;
		this._MtfCsDescrToggle.Source = this._MtfCs;

		let options = MedicalInformationEditor.ResultingDutyStatusOption;

		this._ResultingDutyStatus = selectAndStripId(this, "ResultingDutyStatus");
		this._ResultingDutyStatus.addOption("Return to Duty (RTD)", options.ReturnToDuty);
		this._ResultingDutyStatus.addOption("Medical Evacuation for Further Medical Treatment (MDEVC)", options.MedicalEvacuation);
		this._ResultingDutyStatus.addOption("Other", options.Other);
		this._ResultingDutyStatus.addOption("Unknown", options.Unknown);

		this._ResultingDutyStatus.Value = options.Unknown;
	}

	/**
	 * @returns { YesNoUnknownOption }
	 */
	get PersonnelDecontaminationPerformed()
	{
		return this._PersDeconPerf.Value;
	}

	/**
	 * @param { YesNoUnknownOption } val
	 */
	set PersonnelDecontaminationPerformed(val)
	{
		this._PersDeconPerf.Value = val;
	}

	/**
	 * @returns { string }
	 */
	get DecontaminationDescription()
	{
		return this._DeconDescr.value;
	}

	/**
	 * @param { string } val
	 */
	set DecontaminationDescription(val)
	{
		this._DeconDescr.value = val;
	}

	/**
	 * @returns { YesNoUnknownOption }
	 */
	get SoughtMedicalTreatment()
	{
		return this._SoughtMedTreat.Value;
	}

	/**
	 * @param { YesNoUnknownOption } val
	 */
	set SoughtMedicalTreatment(val)
	{
		this._SoughtMedTreat.Value = val;
	}

	/**
	 * @returns { boolean }
	 */
	get UnitMedic()
	{
		return this._UnitMedic.checked;
	}

	/**
	 * @param { boolean } val
	 */
	set UnitMedic(val)
	{
		this._UnitMedic.checked = val;
	}

	/**
	 * @returns { string }
	 */
	get UnitMedicDescr()
	{
		return this._UnitMedicDescr.value;
	}

	/**
	 * @param { string } val
	 */
	set UnitMedicDescr(val)
	{
		this._UnitMedicDescr.value = val;
	}

	/**
	 * @returns { boolean }
	 */
	get BattalionAideStation()
	{
		return this._BattalionAideStation.checked;
	}

	/**
	 * @param { boolean } val
	 */
	set BattalionAideStation(val)
	{
		this._BattalionAideStation.checked = val;
	}

	/**
	 * @returns { string }
	 */
	get BattalionAideStationDescr()
	{
		return this._BattalionAideStationDescr.value;
	}

	/**
	 * @param { string } val
	 */
	set BattalionAideStationDescr(val)
	{
		this._BattalionAideStationDescr.value = val;
	}

	/**
	 * @returns { boolean }
	 */
	get MtfCs()
	{
		return this._MtfCs.checked;
	}

	/**
	 * @param { boolean } val
	 */
	set MtfCs(val)
	{
		this._MtfCs.checked = val;
	}

	/**
	 * @returns { string }
	 */
	get MtfCsDescr()
	{
		return this._MtfCsDescr.value;
	}

	/**
	 * @param { string } val
	 */
	set MtfCsDescr(val)
	{
		this._MtfCsDescr.value = val;
	}

	/**
	 * @returns { MedicalInformationEditor.ResultingDutyStatusOption }
	 */
	get ResultingDutyStatus()
	{
		return this._ResultingDutyStatus.Value;
	}

	/**
	 * @param { MedicalInformationEditor.ResultingDutyStatusOption } val
	 */
	set ResultingDutyStatus(val)
	{
		this._ResultingDutyStatus.Value = val;
	}

	/**
	 * @returns { string }
	 */
	get Comments()
	{
		return this._Comments.value;
	}

	/**
	 * @param { string } val
	 */
	set Comments(val)
	{
		this._Comments.value = val;
	}
}

MedicalInformationEditor.ResultingDutyStatusOption = {
	ReturnToDuty : "RTD",
	MedicalEvacuation : "MDEVC",
	Other : "OTHER",
	Unknown : "UNKNOWN"
};

customElements.define('jhrm-ifa-medical-information-editor', MedicalInformationEditor);

class SignsSymptomsEditor extends HTMLElement
{
	constructor()
	{
		super();

		this.innerHTML = `
			<jhrm-tabs class="jhrm-personnel-signs-symptoms-editor">
				<jhrm-swap-space>
					<div data-tab="PCE">
						
						<table>
						<caption>Injury Evaluation Distance (IED) Symptom Checklist</caption>
							<tr>
								<td colspan="2"><span style="text-decoration: underline;">Injury</span></td>
							</tr>
							<tr>
								<td>Physical Damage to Body</td>
								<td><jhrm-yes-no-select id="PhysDamBody"></jhrm-yes-no-select></td>
							</tr>
							<tr>
								<td colspan="2"><span style="text-decoration: underline;">Evaluation</span></td>
							</tr>
							<tr>
								<td>H - Headaches and/or Vomiting</td>
								<td><jhrm-yes-no-select id="HeadVomit"></jhrm-yes-no-select></td>
							</tr>
							<tr>
								<td>E - Ear Ringing</td>
								<td><jhrm-yes-no-select id="EarRinging"></jhrm-yes-no-select></td>
							</tr>
							<tr>
								<td>A - Amnesia Loss of Consciousness</td>
								<td><jhrm-yes-no-select id="AmnesLossConsc"></jhrm-yes-no-select></td>
							</tr>
							<tr>
								<td>D - Double vision, dizziness</td>
								<td><jhrm-yes-no-select id="DoubVisDizz"></jhrm-yes-no-select></td>
							</tr>
							<tr>
								<td>S - Something feels wrong</td>
								<td><jhrm-yes-no-select id="SomeFeelWrong"></jhrm-yes-no-select></td>
							</tr>
							<tr>
								<td colspan="2"><span style="text-decoration: underline;">Distance</span></td>
							</tr>
							<tr>
								<td>Within 50 meters</td>
								<td><jhrm-yes-no-select id="Within50Meters"></jhrm-yes-no-select></td>
							</tr>
						</table>
						<p>* PCE is a Potentially Concussive Event</p>
					</div>
					<div data-tab="Chemical">
						<table>
							<caption>Signs/Symptoms</caption>
							<tr>
								<td rowspan="2"><span>Eyes</span></td>
								<td><input type="checkbox" id="EyeIrriBurning"></td>
								<td>Irritation or Burning</td>
							</tr>
							<tr>
								<td><input type="checkbox" id="PinPtedPupils"></td>
								<td>Pin-Pointed Pupils</td>
							</tr>
							<tr>
								<td rowspan="3"><span>Respiratory</span></td>
								<td><input type="checkbox" id="RespIrriBurning"></td>
								<td>Irritation or Burning</td>
							</tr>
							<tr>
								<td><input type="checkbox" id="RespCoughing"></td>
								<td>Coughing</td>
							</tr>
							<tr>
								<td><input type="checkbox" id="RespTroubleBreathing"></td>
								<td>Trouble Breathing</td>
							</tr>
							<tr>
								<td><span>Gastrointestinal</span></td>
								<td><input type="checkbox" id="GastroNauseaVomiting"></td>
								<td>Nausea or Vomiting</td>
							</tr>
							<tr>
								<td rowspan="2"><span>Neurological</span></td>
								<td><input type="checkbox" id="NeuroDizziness"></td>
								<td>Dizziness</td>
							</tr>
							<tr>
								<td><input type="checkbox" id="NeuroSeizures"></td>
								<td>Seizures</td>
							</tr>
							<tr>
								<td rowspan="2"><span>Skin</span></td>
								<td><input type="checkbox" id="SkinIrriBurning"></td>
								<td>Irritation or Burning</td>
							</tr>
							<tr>
								<td><input type="checkbox" id="SkinBlistering"></td>
								<td>Blistering</td>
							</tr>
						</table>
					</div>
				</jhrm-swap-space>
			</jhrm-tabs>
		`;

		this._css = `
			<style id="jhrm-personnel-signs-symptoms-styles">
				.jhrm-personnel-signs-symptoms-editor table 
				{
					width:100%;
					border-collapse:collapse;
				}
				.jhrm-personnel-signs-symptoms-editor td
				{
					padding: 1em;
					border: 2px solid black;
				}
				.jhrm-personnel-signs-symptoms-editor table caption
				{
					background-color:#333;
					padding:0.5em;
					color:#fff
				}
			</style>
		`;

		if(!document.getElementById('jhrm-personnel-signs-symptoms-styles'))
		{
			document.head.innerHTML += this._css;
		}

		this._PhysDamBody = selectAndStripId(this, "PhysDamBody");
		this._HeadVomit = selectAndStripId(this, "HeadVomit");
		this._EarRinging = selectAndStripId(this, "EarRinging");
		this._AmnesLossConsc = selectAndStripId(this, "AmnesLossConsc");
		this._DoubVisDizz = selectAndStripId(this, "DoubVisDizz");
		this._SomeFeelWrong = selectAndStripId(this, "SomeFeelWrong");
		this._Within50Meters = selectAndStripId(this, "Within50Meters");

		this._EyeIrriBurning = selectAndStripId(this, "EyeIrriBurning");
		this._PinPtedPupils = selectAndStripId(this, "PinPtedPupils");
		this._RespIrriBurning = selectAndStripId(this, "RespIrriBurning");
		this._RespCoughing = selectAndStripId(this, "RespCoughing");
		this._RespTroubleBreathing = selectAndStripId(this, "RespTroubleBreathing");
		this._GastroNauseaVomiting = selectAndStripId(this, "GastroNauseaVomiting");
		this._NeuroDizziness = selectAndStripId(this, "NeuroDizziness");
		this._NeuroSeizures = selectAndStripId(this, "NeuroSeizures");
		this._SkinIrriBurning = selectAndStripId(this, "SkinIrriBurning");
		this._SkinBlistering = selectAndStripId(this, "SkinBlistering");
	}

	/**
	 * @returns { Boolean }
	 */
	get PhysicalDamageToBody()
	{
		return this._PhysDamBody.SelectedOption;
	}

	/**
	 * @param { Boolean }
	 */
	set PhysicalDamageToBody(val)
	{
		this._PhysDamBody.SelectedOption = val;
	}

	/**
	 * @returns { Boolean }
	 */
	get HeadachesAndOrVomiting()
	{
		return this._HeadVomit.SelectedOption;
	}

	/**
	 * @param { Boolean }
	 */
	set HeadachesAndOrVomiting(val)
	{
		this._HeadVomit.SelectedOption = val;
	}
	
	/**
	 * @returns { Boolean }
	 */
	get EarRinging()
	{
		return this._EarRinging.SelectedOption;
	}

	/**
	 * @param { Boolean }
	 */
	set EarRinging(val)
	{
		this._EarRinging.SelectedOption = val;
	}
	
	/**
	 * @returns { Boolean }
	 */
	get AmnesiaLossOfConsciousness()
	{
		return this._AmnesLossConsc.SelectedOption;
	}

	/**
	 * @param { Boolean }
	 */
	set AmnesiaLossOfConsciousness(val)
	{
		this._AmnesLossConsc.SelectedOption = val;
	}
	
	/**
	 * @returns { Boolean }
	 */
	get DoubleVisionDizziness()
	{
		return this._DoubVisDizz.SelectedOption;
	}

	/**
	 * @param { Boolean }
	 */
	set DoubleVisionDizziness(val)
	{
		this._DoubVisDizz.SelectedOption = val;
	}
	
	/**
	 * @returns { Boolean }
	 */
	get SomethingFeelsWrong()
	{
		return this._SomeFeelWrong.SelectedOption;
	}

	/**
	 * @param { Boolean }
	 */
	set SomethingFeelsWrong(val)
	{
		this._SomeFeelWrong.SelectedOption = val;
	}
	
	/**
	 * @returns { Boolean }
	 */
	get Within50Meters()
	{
		return this._Within50Meters.SelectedOption;
	}

	/**
	 * @param { Boolean }
	 */
	set Within50Meters(val)
	{
		this._Within50Meters.SelectedOption = val;
	}

	/******* Chemical *******/
	
	/**
	 * @returns { Boolean }
	 */
	get EyeIrritationBurning()
	{
		return this._EyeIrriBurning.checked;
	}

	/**
	 * @param { Boolean }
	 */
	set EyeIrritationBurning(val)
	{
		this._EyeIrriBurning.checked = val;
	}

	/**
	 * @returns { Boolean }
	 */
	get PinPointedPupils()
	{
		return this._PinPtedPupils.checked;
	}

	/**
	 * @param { Boolean }
	 */
	set PinPointedPupils(val)
	{
		this._PinPtedPupils.checked = val;
	}

	/**
	 * @returns { Boolean }
	 */
	get RespiratoryIrritationBurning()
	{
		return this._RespIrriBurning.checked;
	}

	/**
	 * @param { Boolean }
	 */
	set RespiratoryIrritationBurning(val)
	{
		this._RespIrriBurning.checked = val;
	}

	/**
	 * @returns { Boolean }
	 */
	get Coughing()
	{
		return this._RespCoughing.checked;
	}

	/**
	 * @param { Boolean }
	 */
	set Coughing(val)
	{
		this._RespCoughing.checked = val;
	}
	
	/**
	 * @returns { Boolean }
	 */
	get TroubleBreathing()
	{
		return this._RespTroubleBreathing.checked;
	}

	/**
	 * @param { Boolean }
	 */
	set TroubleBreathing(val)
	{
		this._RespTroubleBreathing.checked = val;
	}

	/**
	 * @returns { Boolean }
	 */
	get NauseaVomiting()
	{
		return this._GastroNauseaVomiting.checked;
	}

	/**
	 * @param { Boolean }
	 */
	set NauseaVomiting(val)
	{
		this._GastroNauseaVomiting.checked = val;
	}

	/**
	 * @returns { Boolean }
	 */
	get Dizziness()
	{
		return this._NeuroDizziness.checked;
	}

	/**
	 * @param { Boolean }
	 */
	set Dizziness(val)
	{
		this._NeuroDizziness.checked = val;
	}

	/**
	 * @returns { Boolean }
	 */
	get Seizures()
	{
		return this._NeuroSeizures.checked;
	}

	/**
	 * @param { Boolean }
	 */
	set Seizures(val)
	{
		this._NeuroSeizures.checked = val;
	}

	/**
	 * @returns { Boolean }
	 */
	get SkinIrritationBurning()
	{
		return this._SkinIrriBurning.checked;
	}

	/**
	 * @param { Boolean }
	 */
	set SkinIrritationBurning(val)
	{
		this._SkinIrriBurning.checked = val;
	}

	/**
	 * @returns { Boolean }
	 */
	get Blistering()
	{
		return this._SkinBlistering.checked;
	}

	/**
	 * @param { Boolean }
	 */
	set Blistering(val)
	{
		this._SkinBlistering.checked = val;
	}
};

customElements.define('jhrm-ifa-signs-symptoms-editor', SignsSymptomsEditor);

export class IfaPersonnelIdEditor extends HTMLElement
{
	constructor()
	{
		super();

		this.style.display = "inline-grid";
		this.style.gridTemplateColumns = "auto 1fr";
		this.innerHTML = `
			<select style="grid-row: 1; grid-column: 1" id="id_type">
				<option value="DODID">DODID</option>
				<option value="FN">Foreign National #</option>
			</select>
			<input style="grid-row: 1; grid-column: 2" id="number_input"></input>
		`;

		this._id_type = selectAndStripId(this, "id_type");
		this._number_input = selectAndStripId(this, "number_input");
	}

	validate()
	{
		if ( this._id_type.value == "DODID" )
			return RegExp(/^\d{10}$/).test(this._number_input.value);
		else if ( this._id_type.value == "FN")
			return RegExp(/^\d{1,}$/).test(this._number_input.value);
		else
			return false;
	}

	get Value()
	{
		return this._number_input.value;
	}

	set Value(val)
	{
		this._number_input.value = val.toUpperCase();
	}

	get Type()
	{
		return this._id_type.value;
	}

	set Type(val)
	{
		this._id_type.value = val;
	}
}

customElements.define('jhrm-ifa-personnel-id-editor', IfaPersonnelIdEditor);

class IfaPersonnelRow extends HTMLTableRowElement
{
	constructor()
	{
		super();

		this.innerHTML = `
			<td><input type="checkbox" id="AssociateToIncident"></td>
			<td><jhrm-push-text-edit id="Name"></jhrm-push-text-edit></td>
			<td>
				<span id="DodidFn"></span>
				<material-icon icon="edit" id="DodidFn_Edit" style="cursor: pointer;"></material-icon>
			</td>
			<td><input type="checkbox" id="NoAcuteSymptoms"></td>
			<td>
				<jhrm-yes-no-unknown-select id="SignsSymptoms"></jhrm-yes-no-unknown-select><br>
				<button type="button" id="_btnSignsSymptoms">Add/Review Details</button>
			</td>
			<td>
				<jhrm-yes-no-unknown-select id="PpeWorn"></jhrm-yes-no-unknown-select><br>
				<button type="button" id="_btnPpeWorn">Add/Review Details</button>
			</td>
			<td>
				<jhrm-yes-no-unknown-select id="DetectorSensorData"></jhrm-yes-no-unknown-select><br>
				<button type="button" id="_btnDetectorSensorData">Add/Review Details</button>
			</td>
			<td>
				<button type="button" id="_btnMedicalInformation">Add/Review Details</button>
			</td>
		`;

		this._elmAssociateToIncident = selectAndStripId(this, "AssociateToIncident");
		this._elmName = selectAndStripId(this, "Name");
		this._elmDodidFn = selectAndStripId(this, "DodidFn");
		this._elmNoAcuteSymptoms = selectAndStripId(this, "NoAcuteSymptoms");
		this._elmSignsSymptoms = selectAndStripId(this, "SignsSymptoms");
		this._elmDetectorSensorData = selectAndStripId(this, "DetectorSensorData");
		this._elmPpeWorn = selectAndStripId(this, "PpeWorn");

		this._btnSignsSymptoms = selectAndStripId(this, "_btnSignsSymptoms");
		this._btnPpeWorn = selectAndStripId(this, "_btnPpeWorn");
		this._btnDetectorSensorData = selectAndStripId(this, "_btnDetectorSensorData");

		this._DodidFn_Edit = selectAndStripId(this, "DodidFn_Edit");

		this._btnSignsSymptoms.onclick = not_implmented;

		this._DodidFn_Edit.onclick = () =>
		{
			let id_edit = new IfaPersonnelIdEditor();
			id_edit.Value = this.DodidFn;
			id_edit.Type = "DODID";

			if ( !id_edit.validate() )
				id_edit.Type = "FN";

			if ( this.IsFN == true )
			{
				id_edit.Type = "FN";
				id_edit.IsFN = true;
			}

			let dialog = new JhrmDialog();
			dialog.Buttons = JhrmDialog.ButtonSet.SaveCancel;
			dialog.append(id_edit);

			dialog.OnAccept = () =>
			{
				if ( id_edit.validate() )
				{
					this.DodidFn = id_edit.Value;
					this.IsFN = id_edit.Type == "FN" ? true : false;
					return true;
				}
				else
				{
					alert("ID must be all digits, and 10 digits if it is a DODID.");
					return false;
				}
			}

			dialog.show();
		};

		this._signSymptomsDataEditor = new SignsSymptomsEditor();
		this._btnSignsSymptoms.onclick = () =>
		{
			let body = document.querySelector("body");
			let dialog = new JhrmDialog();
			let editor = this._signSymptomsDataEditor;

			dialog.Buttons = JhrmDialog.ButtonSet.SaveCancel;

			dialog.append(editor);
			body.append(dialog);
			editor.querySelector('jhrm-tabs').render();

			let previous = {
				//concussive
				PhysicalDamageToBody : editor.PhysicalDamageToBody,
				HeadachesAndOrVomiting : editor.HeadachesAndOrVomiting,
				EarRinging : editor.EarRinging,
				AmnesiaLossOfConsciousness : editor.AmnesiaLossOfConsciousness,
				DoubleVisionDizziness : editor.DoubleVisionDizziness,
				SomethingFeelsWrong : editor.SomethingFeelsWrong,
				Within50Meters : editor.Within50Meters,
				//chemical
				EyeIrritationBurning : editor.EyeIrritationBurning,
				PinPointedPupils : editor.PinPointedPupils,
				RespiratoryIrritationBurning : editor.RespiratoryIrritationBurning,
				Coughing : editor.Coughing,
				TroubleBreathing : editor.TroubleBreathing,
				NauseaVomiting : editor.NauseaVomiting,
				Dizziness : editor.Dizziness,
				Seizures : editor.Seizures,
				SkinIrriationBurning : editor.SkinIrriationBurning,
				Blistering : editor.Blistering
			};
			
			dialog.OnReject = () =>
			{
				removeElement(dialog);
				Object.assign(editor, previous);
				return false;
			};

			dialog.OnAccept = () =>
			{
				this.dispatchEvent(new Event("PersonnelDialogAccepted",{bubbles:true}));
				removeElement(dialog);
				return false;
			}

			dialog.show();
		}

		this._ppeWornEditor = new PpeEditor();
		this._btnPpeWorn.onclick = () =>
		{
			let dialog = new JhrmDialog();
			let editor = this._ppeWornEditor;
			let previous = {
				ChemicalPpe : editor.ChemicalPpe,
				ChemicalPpeOther : editor.ChemicalPpeOther,
				ConcussivePpe: editor.ConcussivePpe,
				ConcussivePpeOther : editor.ConcussivePpeOther
			};

			dialog.Buttons = JhrmDialog.ButtonSet.SaveCancel;

			dialog.append(editor);
			document.body.append(dialog);
			editor.querySelector('jhrm-tabs').render();

			dialog.OnReject = () =>
			{
				Object.assign(editor, previous);
				removeElement(dialog);
				return false;
			};

			dialog.OnAccept = () =>
			{
				let validator = new FormValidator(editor, {
					inputSelector : '.ifa-input'
				});
				let clean = validator.validate();
				if(!clean)
				{
					alert(validator.errors.join("\n"));
					return;
				}
				this.dispatchEvent(new Event("PersonnelDialogAccepted",{bubbles:true}));
				removeElement(dialog);
				return false;
			}

			dialog.show();
		}

		function processIfaHraFields(results)
		{
			return results.map(
				( item ) =>
				{
					return IfaHra.fromBackendFields(item);
				}
			);
		}

		this._detectorSensorDataEditor = new DetectorSensorEditor();
		this._btnDetectorSensorData.onclick = async () =>
		{
			this._ifaId = new URLSearchParams(window.location.search).get('ifa');
			let results;

			let dialog = new JhrmDialog();
			let editor = this._detectorSensorDataEditor;
			let previous = {
				HRAs : this._detectorSensorDataEditor.ChemicalInfo.AssociatedHras,
				ConcussiveDate : this._detectorSensorDataEditor.ConcussiveDate,
				ConcussiveRows : this._detectorSensorDataEditor.ConcussiveRows.map(item=>{
					return {
						BlastGauge : item.BlastGauge,
						SetSerial : item.SetSerial,
						Color : item.Color,
						PeakOp : item.PeakOp,
					};
				}),
			};

			editor.ConcussiveName = this.Name;
			editor.ConcussiveDodid = this.DodidFn;

			dialog.Buttons = JhrmDialog.ButtonSet.SaveCancel;

			dialog.append(editor);
			document.body.append(dialog);
			editor.querySelector('jhrm-tabs').render();


			results = await Request.get('/api/ifa/report/' + this._ifaId + '/localdrihraslist');

			if(results.length > 0)
			{
				let driHRAs = [];

				for(let i = 0; i < results.length; i++)
					driHRAs.push(await Request.get('/api/rapidhra/hra/' + results[i].chem_rapid_hra_id));

				this._detectorSensorDataEditor._associated_dri_editor.IfaHras = processIfaHraFields(driHRAs);;
			}
			
			results = await Request.get('/api/ifa/report/' + this._ifaId + '/drihrasids');

			if(results.length > 0)
			{
				let driHRAs = [];

				for(let i = 0; i < results.length; i++)
					driHRAs.push(await Request.get('/api/rapidhra/hra/' + results[i].chem_rapid_hra_id));

				this._detectorSensorDataEditor._associated_dri_editor.LocationHras = processIfaHraFields(driHRAs);
			}

			dialog.OnReject = () =>
			{
				editor.clear();
				editor.ConcussiveDate = previous.ConcussiveDate;
				previous.ConcussiveRows.forEach(item=>{
					let row = editor.newRow();
					Object.assign(row, item);
				});
				removeElement(dialog);
				return false;
			};

			dialog.OnAccept = () =>
			{
				let rows = editor.ConcussiveRows;

				for (let i = 0; i < rows.length; ++i)
				{
					if (rows[i]._EditingActive)
					{
						alert("Please complete data editing before continuing.");
						return false;
					}
				}

				let parent_table_rows = this.closest("jhrm-ifa-personnel-table").AllRows;
				let added_to_all = editor.ChemicalInfo.AddedToAll;

				for (let i = 0; i < parent_table_rows.length; ++i)
				{
					let row_chem_info = parent_table_rows[i].DetectorSensorChemicalEditor.ChemicalInfo;

					row_chem_info.AssociatedHras = arrayUnion(
						row_chem_info.AssociatedHras, added_to_all,
						function(left, right)
						{
							return left.Id = right.Id;
						}
					);
				}

				editor.ChemicalInfo.AddedToAll = [];
				this.dispatchEvent(new Event("PersonnelDialogAccepted",{bubbles:true}));
				removeElement(dialog);
				return false;
			}

			dialog.show();
		};

		this._medicalInformationDataEditor = new MedicalInformationEditor();
		selectAndStripId(this, "_btnMedicalInformation").onclick = () =>
		{
			let body = document.querySelector("body");
			let dialog = new JhrmDialog();
			let editor = this._medicalInformationDataEditor; 
			
			dialog.Buttons = JhrmDialog.ButtonSet.SaveCancel;

			dialog.append(editor);
			body.append(dialog);

			let previous = {
				PersonnelDecontaminationPerformed : editor.PersonnelDecontaminationPerformed,
				DecontaminationDescription : editor.DecontaminationDescription,
				SoughtMedicalTreatment : editor.SoughtMedicalTreatment,
				UnitMedic : editor.UnitMedic,
				UnitMedicDescr : editor.UnitMedicDescr,
				BattalionAideStation : editor.BattalionAideStation,
				BattalionAideStationDescr : editor.BattalionAideStationDescr,
				MtfCs : editor.MtfCs,
				MtfCsDescr : editor.MtfCsDescr,
				ResultingDutyStatus : editor.ResultingDutyStatus,
				Comments : editor.Comments
			};

			dialog.OnReject = () =>
			{
				Object.assign(editor, previous);
				removeElement(dialog);
				return false;
			};

			dialog.OnAccept = () =>
			{
				let validator = new FormValidator(editor, {
					inputSelector : '.ifa-input'
				});
				let clean = validator.validate();
				if(!clean)
				{
					alert(validator.errors.join("\n"));
					return;
				}
				this.dispatchEvent(new Event("PersonnelDialogAccepted",{bubbles:true}));
				removeElement(dialog);
				return false;
			};

			dialog.show();
		};

		this._elmNoAcuteSymptoms.onchange = (evt) =>
		{
			if(this.NoAcuteSymptoms)
			{
				this.SignsSymptoms = YesNoUnknownOption.No;
			}
		}

		this._elmSignsSymptoms.onchange = (evt) =>
		{
			this._btnSignsSymptoms.disabled = (evt != YesNoUnknownOption.Yes);
			if(evt != YesNoUnknownOption.No)
			{
				this.NoAcuteSymptoms = false;
			}
		};

		this._elmPpeWorn.onchange = (evt) =>
		{
			this._btnPpeWorn.disabled = (evt != YesNoUnknownOption.Yes);
		};

		this._elmDetectorSensorData.onchange = (evt) =>
		{
			this._btnDetectorSensorData.disabled = (evt != YesNoUnknownOption.Yes);
		};


		this._btnSignsSymptoms.disabled = (this._elmSignsSymptoms.SelectedOption != YesNoUnknownOption.Yes);
		this._btnPpeWorn.disabled = (this._elmPpeWorn.SelectedOption != YesNoUnknownOption.Yes);
		this._btnDetectorSensorData.disabled = (this._elmDetectorSensorData.SelectedOption != YesNoUnknownOption.Yes);
	}

	/**
	 * @returns { boolean }
	 */
	get AssociateToIncident()
	{
		return this._elmAssociateToIncident.checked;
	}

	/**
	 * @param { boolean } val
	 */
	set AssociateToIncident(val)
	{
		this._elmAssociateToIncident.checked = val;
	}

	/**
	 * @returns { string }
	 */
	get Name()
	{
		return this._elmName.Value;
	}

	/**
	 * @param { string } val
	 */
	set Name(val)
	{
		this._elmName.Value = val;
	}

	/**
	 * @returns { string }
	 */
	get DodidFn()
	{
		return this._elmDodidFn.innerHTML;
	}

	/**
	 * @param { string } val
	 */
	set DodidFn(val)
	{
		this._elmDodidFn.innerHTML = val;
	}

		/**
	 * @returns { boolean }
	 */
	get IsFN()
	{
		return this._elmIsFN;
	}

	/**
	 * @param { boolean } val
	 */
	set IsFN(val)
	{
		this._elmIsFN = val;
	}

	/**
	 * @returns { boolean }
	 */
	get NoAcuteSymptoms()
	{
		return this._elmNoAcuteSymptoms.checked;
	}

	/**
	 * @param { boolean } val
	 */
	set NoAcuteSymptoms(val)
	{
		this._elmNoAcuteSymptoms.checked = val;
	}

	/**
	 * @returns { YesNoUnknownOption }
	 */
	get SignsSymptoms()
	{
		return this._elmSignsSymptoms.SelectedOption;
	}

	/**
	 * @param { YesNoUnknownOption } val
	 */
	set SignsSymptoms(val)
	{
		this._elmSignsSymptoms.SelectedOption = val;
	}

	/**
	 * @returns { YesNoUnknownOption }
	 */
	get DetectorSensorData()
	{
		return this._elmDetectorSensorData.SelectedOption;
	}

	/**
	 * @param { YesNoUnknownOption } val
	 */
	set DetectorSensorData(val)
	{
		this._elmDetectorSensorData.SelectedOption = val;
	}

	/**
	 * @returns { YesNoUnknownOption }
	 */
	get PpeWorn()
	{
		return this._elmPpeWorn.SelectedOption;
	}

	/**
	 * @param { YesNoUnknownOption } val
	 */
	set PpeWorn(val)
	{
		this._elmPpeWorn.SelectedOption = val;
	}

	/**
	 * @returns { PpeEditor }
	 */
	get PpeWornEditor()
	{
		return this._ppeWornEditor;
	}

	/**
	 * Accesses the editor for the Detector/Sensor data of the incident.
	 * 
	 * @returns { DetectorSensorEditor }
	 */
	get DetectorSensorDataEditor()
	{
		return this._detectorSensorDataEditor;
	}

	/**
	 * @returns { MedicalInformationEditor} 
	 */
	get MedicalInformationDataEditor()
	{
		return this._medicalInformationDataEditor;
	}

	/**
	 * @returns { SignsSymptomsEditor }
	 */
	get SignsSymptomsDataEditor()
	{
		return this._signSymptomsDataEditor;
	}

	/**
	 * @returns { DetectorSensorEditor }
	 */
	get DetectorSensorChemicalEditor()
	{
		return this._detectorSensorDataEditor
	}
}

customElements.define('jhrm-ifa-personnel-row', IfaPersonnelRow, { extends: 'tr' });

class IfaPersonnelTable extends HTMLElement
{
	constructor()
	{
		super();

		this._table = document.createElement('table');
		this._table.innerHTML = `
			<thead>
				<tr>
					<th>Associate To Incident <material-icon icon="help_outline" id="AssociateToIncidentHelp"></material-icon></th>
					<jhrm-tooltip target="AssociateToIncidentHelp">
							Check to associate or disassociate an individual to/from the incident.
					</jhrm-tooltip>
					<th>Name</th>
					<th>DODID/FN#</th>
					<th>No Acute Symptoms <material-icon icon="help_outline" id="NoAcuteSymptomsHelp"></material-icon> </th>
					<jhrm-tooltip target="NoAcuteSymptomsHelp">
						Check if the individual does not exhibit any acute symptoms.
					</jhrm-tooltip>
					<th>Signs/Symptoms <material-icon icon="help_outline" id="SignSymptomHelp"></material-icon> </th>
					<jhrm-tooltip target="SignSymptomHelp">
						Select "Yes" if the individual showed signs/symptoms.
					</jhrm-tooltip>
					<th>PPE Worn <material-icon icon="help_outline" id="PpeWornHelp"></material-icon> </th>
					<jhrm-tooltip target="PpeWornHelp">
						Select "Yes" if the individual was wearing Personal Protective Equipment (PPE).
					</jhrm-tooltip>
					<th>Detector/Sensor Data <material-icon icon="help_outline" id="DetectorSensorDataHelp"></material-icon> </th>
					<jhrm-tooltip target="DetectorSensorDataHelp">
						Select "Yes" if any detector/sensor data is available.
					</jhrm-tooltip>
					<th>Medical Information <material-icon icon="help_outline" id="MedicalInformationHelp"></material-icon> </th>
					<jhrm-tooltip target="MedicalInformationHelp">
						Enter medical information (e.g., personal decontamination, medical care received).
					</jhrm-tooltip>
				</tr>
			</thead>
			<tbody id="table-body">
			</tbody>
		`;
		
		this.append(this._table);
		this._tbody = selectAndStripId(this._table, "table-body");
	}

	/**
	 * 
	 * @param { IfaPersonnelRow } row 
	 * 
	 * @returns The row that was added.
	 */
	addRow(row)
	{
		if (undefined != row)
		{
			if (!(row instanceof IfaPersonnelRow))
				throw new Error("Expecting IfaPersonnelRow parameter.");

			this._tbody.append(row);

			return row;
		}
		else
		{
			let addedRow = new IfaPersonnelRow();
			this._tbody.append(addedRow);

			return addedRow;
		}
	}

	clear()
	{
		this._tbody.innerHTML = "";
	}

	/**
	 * All IfaPersonnelRow items in the table.
	 * 
	 * @returns { IfaPersonnelRow []}
	 */
	get AllRows()
	{
		return Array.from(this._tbody.children);
	}
}

customElements.define('jhrm-ifa-personnel-table', IfaPersonnelTable);