
/**
 * Searches for an element with the provided id under parent, strips it
 * of that id, and returns the element.  This prevents name collisions
 * for ids in situations where they might otherwise be likely.
 * 
 * @param {*} parent 
 *   The parent element to search.
 * @param {*} id 
 *   The id of the element to find.
 */
export var selectAndStripId = function(parent, id)
{
	let ret = parent.querySelector(`#${id}`);

	if ( ret )
		ret.removeAttribute('id');
	
	return ret;
};

export var removeElement = function(elm)
{
	let parent = elm.parentElement;
	
	if (parent)
		parent.removeChild(elm);
};

export var randomChars = function(string_length)
{
	const charset = "abcdefghijklmnopqrstuvwxyz";
	var text = "";
	
	for (var i = 0; i < string_length; i++)
		text += charset.charAt(Math.floor(Math.random() * charset.length));
	
	return text;
};

let default_is_equal = function(left, right) { return (left == right); }

/**
 * Creates an array with elements that are in both left and right.
 * 
 * @param { object[] } left 
 * @param { object[] } right 
 * @param { function(elm_left, elm_right) } is_equal
 *  An optional comparison function that will return true if elm_left
 *  and elm_right should be considered equal. Otherwise, default equality is used.
 */
export var arrayIntersection = function(left, right, is_equal)
{
	if (undefined == is_equal)
		is_equal = default_is_equal;

	return left.filter( elm_left => (0 <= right.findIndex(elm_right => is_equal(elm_left, elm_right))) );
}

/**
 * Creates an array with the contents of left that are not in the contents of right.
 * 
 * @param { object[] } left 
 * @param { object[] } right 
 * @param { function(elm_left, elm_right) } is_equal
 *  An optional comparison function that will return true if elm_left
 *  and elm_right should be considered equal. Otherwise, default equality is used.
 */
export var arrayDifference = function(left, right, is_equal)
{
	if (undefined == is_equal)
		is_equal = default_is_equal;

	return left.filter( elm_left => (0 > right.findIndex(elm_right => is_equal(elm_left, elm_right))) );
}

/**
 * Creates an array with the contents of left that are not in the contents of right.
 * 
 * @param { object[] } left 
 * @param { object[] } right 
 * @param { function(elm_left, elm_right) } is_equal
 *  An optional comparison function that will return true if elm_left
 *  and elm_right should be considered equal. Otherwise, default equality is used.
 */
export var arrayUnion = function(left, right, is_equal)
{
	return [...arrayDifference(left, right, is_equal), ...right];
}