
export class AnalyticalMethodLimits
{
}

AnalyticalMethodLimits.T4 = {
    UPPER : 1584,
    LOWER : 1296
}

AnalyticalMethodLimits.T9 = {
    UPPER : 1584,
    LOWER : 1296
}

AnalyticalMethodLimits.T13 = {
    UPPER : 1584,
    LOWER : 1296
}

AnalyticalMethodLimits.T14 = {
    UPPER : 624,
    LOWER : 336
}

AnalyticalMethodLimits.T15 = {
    UPPER : 1584,
    LOWER : 1296
}

AnalyticalMethodLimits.EPA_GRAV = {
    UPPER : 1544,
    LOWER : 1256
}


export class validateAnalyticalMethodLimits
{
    
    analyticalMethodChecker(aVal, sampleTime)
    {
        if(aVal.includes('200.8') || aVal.includes('GRAV'))
			{
				if (sampleTime < AnalyticalMethodLimits.EPA_GRAV.LOWER || sampleTime > AnalyticalMethodLimits.EPA_GRAV.UPPER)
				{
					return ( true );
				}
			}
			else if(aVal.includes('TO-4'))
			{
				if (sampleTime < AnalyticalMethodLimits.T4.LOWER || sampleTime > AnalyticalMethodLimits.T4.UPPER)
				{
					return ( true );
				}
			}
			else if(aVal.includes('TO-9'))
			{
				if (sampleTime < AnalyticalMethodLimits.T9.LOWER || sampleTime > AnalyticalMethodLimits.T9.UPPER)
				{
					return ( true );
				}
			}
			else if(aVal.includes('TO-13'))
			{
				if (sampleTime < AnalyticalMethodLimits.T13.LOWER || sampleTime > AnalyticalMethodLimits.T13.UPPER)
				{
					return ( true );
				}
			}
			else if(aVal.includes('TO-14'))
			{
				if (sampleTime < AnalyticalMethodLimits.T14.LOWER || sampleTime > AnalyticalMethodLimits.T14.UPPER)
				{
					return ( true );
				}
			}
			else if(aVal.includes('TO-15'))
			{
				if (sampleTime < AnalyticalMethodLimits.T15.LOWER || sampleTime > AnalyticalMethodLimits.T15.UPPER)
				{
					return ( true );
				}
            }
        
            return false;

    }
}