import { Login } from "./login.js"

export class JhrmHeader extends HTMLElement
{
	constructor()
	{
		super();

		this.Initialized = false;
	}

	connectedCallback()
	{
		/*
		
			jhrm-header
			{
				text-align: left;
			}

			jhrm-header h1
			{
				font-variant: small-caps;
				color: blue;
				margin-bottom: 0.1em;
			}

			jhrm-header h2
			{
				font-variant: small-caps;
				font-size: 105%;
				color: rgb(64, 64, 256);
				margin-top: 0.1em;
			}
		*/

		if (false == this.Initialized)
		{
			this.style.display = "flex";
			this.style.color = "white";
			this.style.backgroundColor = "#1b365d";
			this.style.fontVariant = "small-caps"

			let elmLogo = document.createElement('img');
			elmLogo.src = "./img/JPEO-CBD.png";
			elmLogo.style.height = "6em";
			elmLogo.style.margin = "0.5em";


			let elmHeader = document.createElement('div');
			elmHeader.innerHTML = `
				<div style="font-size: 2rem;">Joint Health Risk Management</div>
				<div>Enhanced Capability Demonstration (JHRM - ECD)</div>
			`;
			elmHeader.style.flexGrow = "1";
			elmHeader.style.margin = "auto";

			let elmLoginStatus = document.createElement('div');
			elmLoginStatus.style.margin = "auto 2em auto auto";
			elmLoginStatus.style.fontStyle = "italic";
			elmLoginStatus.style.textAlign = "center";

			if (Login.LoggedIn)
			{
				let elmLogoutLink = document.createElement('a');
				elmLogoutLink.innerText = "Logout";
				elmLogoutLink.href = "#";
				elmLogoutLink.style.color = "#0076ba";
				elmLogoutLink.style.textDecoration = "none";
				elmLogoutLink.onclick = () =>
				{
					Login.signout();
					location.replace("login.html");
				};

				let nameText = document.createElement('div');
				nameText.innerText = `${Login.FirstName} ${Login.LastName}`;

				elmLoginStatus.append(
					nameText,
					elmLogoutLink
				);
			}
			else
			{
				elmLoginStatus.innerHTML = " ";
			}

			this.append(elmLogo, elmHeader, elmLoginStatus);

			this.Initialized = true;
		}
	}
}

customElements.define('jhrm-header', JhrmHeader);