'use strict';
import { revive } from '/js/shared/revive.js'

/**
 * This is a simple class to manage an XHR request. 
 */

export class Request
{
	static get(url, params)
	{
		return Request.request('GET', url, params);
	}

	static post(url, body, params)
	{
		return Request.request('POST', url, params, body);
	}

	static postUpload(url, body)
	{
		return Request.request('POST', url, {}, body, Request.FORMATS.FORM);
	}

	static request(method, url, params, body, format)
	{
		let xhr = new XMLHttpRequest();
		format = format || Request.FORMATS.JSON;

		if(format == Request.FORMATS.JSON)
		{
			body = body ? JSON.stringify(body) : '';
		}
		else 
		{
			let formData = new FormData();
			if(body)
			{
				Object.keys(body).forEach(key=>{
					formData.append(key, body[key]);
				});
			}
			body = formData
		}

		let paramString = params ? 
			Object.keys(params).map(key => key + '=' + params[key]).join('&') :
			'';
		let urlString = paramString.length ? url + '?' + paramString : url;

		return new Promise(function (resolve, reject)
		{
			xhr.onreadystatechange = () => {

			 if (xhr.readyState !== 4)
			 {
				return;
			 }

			try 
			{
				let response = JSON.parse(xhr.responseText, revive);
				if (xhr.status >= 200 && xhr.status < 300) 
				{
					resolve(response);
				}
				else 
				{
					reject({
						status: xhr.status,
						message: response.message ? response.message : response
					});
				}
			}
			catch(e)
			{
				console.log(e);
				reject({
					status: xhr.status,
					message: xhr.responseText
				});
			}

			};
			
			xhr.open(method || 'GET', urlString);
			if(format == Request.FORMATS.JSON)
			{
				xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
			}
			xhr.send(body);
		});
	}

	static FORMATS = {
		JSON : 'JSON',
		FORM : 'FORM'
	}
}