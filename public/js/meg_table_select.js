import { JhrmRadioGroup } from "./radio_group.js"
import { EXPOSURE_DURATION, SEVERITY } from "/js/shared/Constants.js"

let ExpDur = EXPOSURE_DURATION;

export class JhrmMegTableSelect extends HTMLElement
{
	constructor()
	{
		super();

		this._radio_group = undefined;

		this._header = document.createElement('thead');
		this._header.innerHTML = `
			<tr>
				<th>MEG Time</th>
				<th>MEG Severity</th>
				<th>mg/m<sup>3</sup></th>
				<th> </th>
			</tr>
		`;

		this._body = document.createElement('tbody');

		this._table_element = document.createElement('table');
		this._table_element.style.borderCollapse = "collapse";
		this._table_element.append(this._header, this._body);

		this._onchange = undefined;

		this._initialized = false;
	}

	connectedCallback()
	{
		if (!this._initialized)
		{
			this.append(this._table_element);
			this._initialized = true;
		}
	}

	_RaiseChangeEvent()
	{
		let val = this._radio_group.Value;

		let event = new CustomEvent(
			"change",
			{
				detail : val
			}
		);

		this.dispatchEvent(event);

		if (this._onchange)
			this._onchange(this, val);
	}

	get Value()
	{
		if ( this._radio_group )
			return this._radio_group.Value;
		
		return undefined;
	}

	set Value(val)
	{
		this._radio_group.Value = val;
	}

	set onchange(func)
	{
		this._onchange = func;
	}

	/**
	 * Sets the options that will be selectable in the table.
	 * 
	 * @param { Object[] } options - The options that will be selectable in the table.
	 * @param { EXPOSURE_DURATION } options[].timeframe - The MEG Time duration.JhrmRadioGroup.
	 * @param { SEVERITY } options[].severity - The MEG severity.
	 * @param { float } options[].Concentration - The concentration in mg/m3.
	 */
	setOptions(options)
	{
		if (options == undefined || options.length <= 0)
			return;

		options.sort(
			(a, b) =>
			{
				if (a.timeframe < b.timeframe)
					return -1;
				
				if (a.timeframe > b.timeframe)
					return 1;

				if (a.severity < b.severity)
					return -1;
				
				if (a.severity > b.severity)
					return 1;

				if (a.Concentration < b.Concentration)
					return -1;
				
				if (a.Concentration > b.Concentration)
					return 1;

				return 0;
			}
		);

		this._radio_group = new JhrmRadioGroup();
		this._body.innerHTML = "";

		let style_def = `border: 1px solid black; color: black; text-align: center; ` +
		                `vertical-align: middle; padding: 0.5em; background-color: white;`;

		let last_duration = undefined;
		let current_row = undefined;
		let duration_header = undefined;
		let row_count = 0;

		let FirstSelected = undefined;

		for (let i = 0; i < options.length; ++i)
		{
			current_row = document.createElement('tr');
			this._body.append(current_row);

			if (options[i].timeframe != last_duration)
			{
				if ( duration_header )
					duration_header.rowSpan = row_count.toString();

				duration_header = document.createElement('td');
				duration_header.style.cssText = style_def;
				duration_header.innerHTML = EXPOSURE_DURATION.toString(options[i].timeframe);

				current_row.append(duration_header);

				last_duration = options[i].timeframe;
				row_count = 0;
			}
			
			++row_count;

			let td_severity = document.createElement('td');
			td_severity.style.cssText = style_def;
			td_severity.innerHTML = SEVERITY.toString(options[i].severity);

			let td_concentration = document.createElement('td');
			td_concentration.style.cssText = style_def;
			td_concentration.innerHTML = options[i].value.value.toString();

			let td_radio = document.createElement('td');
			td_radio.style.cssText = style_def;
			td_radio.append(this._radio_group.genElement(options[i]));

			current_row.append(td_severity, td_concentration, td_radio);

			if ( options[i].Selected && undefined == FirstSelected)
				FirstSelected = options[i];
		}

		if ( duration_header )
			duration_header.rowSpan = row_count.toString();

		if ( FirstSelected )
		{
			this._radio_group.Value = FirstSelected;
			FirstSelected.Selected = undefined;
		}

		this._RaiseChangeEvent();
		this._radio_group.onchange = this._RaiseChangeEvent.bind(this);
	}

	/**
	 * @returns All selectable meg options
	 */
	get Options()
	{
		return this._radio_group.Options;
	}
}

customElements.define('jhrm-meg-table-select', JhrmMegTableSelect);
