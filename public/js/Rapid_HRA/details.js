import { JhrmTooltip } from "../tooltip.js"
import { MaterialIcon } from "../material_icon.js"
import { JhrmLocationSelect } from "../location_select.js"
import { JhrmSelect } from "../select.js"
import { Request } from "../request.js"
import { EXPOSURE_PATHWAY } from "/js/shared/Constants.js"

let createTooltip = function(tip)
{
	let icon = MaterialIcon.Create("help_outline");
	icon.style.fontSize = "0.9em";

	let tooltip = new JhrmTooltip();
	tooltip.Target = icon;
	tooltip.append(tip);

	let ret = document.createElement('span');
	ret.append(icon, tooltip);

	return ret;
}

let createRadio = function(name, value, col, text)
{
	let input = document.createElement('input');
	input.type = "radio";
	input.name = name;
	input.value = value;
	let elmText = (undefined == text) ? value : text;

	let ret = document.createElement('div');
	ret.style.gridColumn = col.toString();
	ret.style.whiteSpace = "nowrap";
	ret.append(input, document.createTextNode(" " + elmText) );

	ret.RadioElement = input;

	return ret;
};

let createRadioGrid = function(row, col)
{
	let elm = document.createElement('div');

	elm.style.display = "grid";
	elm.style.gridRow = row.toString();
	elm.style.gridColumn = col.toString();
	elm.style.gridTemplateColumns = "1fr 1fr 5fr 5fr";
	elm.style.gridColumnGap = "0.75em";

	return elm;
};

let createLabel = function(text, row, col)
{
	let ret = document.createElement('div');
	ret.style.gridRow = row.toString();
	ret.style.gridColumn = col.toString();
	ret.style.whiteSpace = "nowrap";

	ret.append(text);

	return ret;
};

export class RapidHraDetailsForm extends HTMLElement
{
	constructor()
	{
		super();

		let makeOr = function()
		{
			let elm = document.createElement('span');
			elm.style.display = "inline-block";
			elm.style.margin = "0 1em 0 1em";
			elm.innerText = "OR";

			return elm;
		};

		let makeBr = function()
		{
			return document.createElement('br');
		};

		///////////////////

		this._SiteExposurePathwayRadioElements = [
			createRadio("exposurepathway", EXPOSURE_PATHWAY.NONE, 1, EXPOSURE_PATHWAY.NONE),
			createRadio("exposurepathway", EXPOSURE_PATHWAY.NewExposurePathway, 2, EXPOSURE_PATHWAY.NewExposurePathway),
			createRadio("exposurepathway", EXPOSURE_PATHWAY.ExistingSensorPathway, 3, EXPOSURE_PATHWAY.ExistingSensorPathway)
		];

		this._RadioSamplingReasonGrid = createRadioGrid(4, 2);
		this._RadioSamplingReasonGrid.style.minWidth = "1em";
		this._RadioSamplingReasonGrid.append( ...this._SiteExposurePathwayRadioElements );
		this._SiteExposurePathwayRadioElements[0] = this._SiteExposurePathwayRadioElements[0].RadioElement;
		this._SiteExposurePathwayRadioElements[1] = this._SiteExposurePathwayRadioElements[1].RadioElement;
		this._SiteExposurePathwayRadioElements[2] = this._SiteExposurePathwayRadioElements[2].RadioElement;

		this._SiteExposurePathwayRadioElements[0].checked = true;

		///////////////////

		this.main_layout = document.createElement('div');
		this.main_layout.style.display = "grid";
		this.main_layout.style.gridTemplateColumns = "auto 1fr";
		this.main_layout.style.gridGap = "0.5em";

		let textIndicatesRequired = document.createElement('div');
		textIndicatesRequired.style.marginBottom = "1em";
		textIndicatesRequired.style.fontWeight = "bold";
		textIndicatesRequired.innerText = "* Indicates required";

		this._inputLocation = new JhrmSelect();
		this._inputLocation.style.width = "30em";
		this._inputLocation.addOption('none', 'none');

		Request.get('/api/enhancedhra/locations').then(
			function(response)
			{
				for ( let record of response)
				{
					this._inputLocation.addOption(record.location_name, record.location_name);
				}
			}.bind(this),
			function(error)
			{
				// Known empty function
			}
		);

		this._inputSiteMGRS = document.createElement('input');
		this._inputSiteMGRS.style.width = "15em";
		this._inputSiteMGRS.style.margin = "0 1em 0 1em";
		this._inputSiteMGRS.style.marginRight = "0.5em";

		let tooltipLocationText = document.createElement('div');
		tooltipLocationText.style.textAlign = "center";
		tooltipLocationText.innerHTML =
				'Select the location for the samples you wish to use.';

		let tooltipLocation = createTooltip(tooltipLocationText);

		let layoutLocation = document.createElement('span');
		layoutLocation.style.gridRow = "2";
		layoutLocation.style.gridColumn = "2";
		layoutLocation.append(
			this._inputLocation,this._inputSiteMGRS,tooltipLocation
		);

		// Objective input

		this._inputObjective = document.createElement('input');
		this._inputObjective.style.width = "30em";
		this._inputObjective.style.marginRight = "0.5em";

		let tooltipObjectiveText = document.createElement('div');
		tooltipObjectiveText.style.textAlign = "center";
		tooltipObjectiveText.innerHTML =
			"128 character limit<br>" +
			"Example: Assess the parameter around the barracks";

		let tooltipObjective = createTooltip(tooltipObjectiveText);

		let layoutObjective = document.createElement('span');
		layoutObjective.append(this._inputObjective, tooltipObjective);
		layoutObjective.style.gridRow = "3";
		layoutObjective.style.gridColumn = "2";

		// Pupulation / Indivudals At Risk input

		this._inputPupulationAtRisk = document.createElement('input');
		this._inputPupulationAtRisk.style.width = "20em";
		this._inputPupulationAtRisk.style.marginRight = "0.5em";

		let tooltipPupulationAtRiskText = document.createElement('div');
		tooltipPupulationAtRiskText.style.textAlign = "center";
		tooltipPupulationAtRiskText.innerHTML =
			"256 character limit<br>" +
			"Example: Describe the population affected by the event or chemicals.";

		let tooltipPupulationAtRisk = createTooltip(tooltipPupulationAtRiskText);

		let labelIndividualsAtRisk = document.createElement('span');
		labelIndividualsAtRisk.style.margin = "0 0.5em 0 3em";
		labelIndividualsAtRisk.innerText = "# of Individuals at Risk:"

		this._inputIndividualsAtRisk = document.createElement('input');
		this._inputIndividualsAtRisk.style.width = "10em";
		this._inputIndividualsAtRisk.style.marginRight = "0.5em";

		let tooltipIndividualsAtRisk = createTooltip("Example: Input a number ie 10,000.");

		let layoutRisk = document.createElement('span');
		layoutRisk.style.gridRow = "5";
		layoutRisk.style.gridColumn = "2";
		layoutRisk.append(
			this._inputPupulationAtRisk, tooltipPupulationAtRisk, labelIndividualsAtRisk,
			this._inputIndividualsAtRisk, tooltipIndividualsAtRisk
		);

		///// SAP File /////

		let tooltipSAPuploadText = document.createElement('div');
		tooltipSAPuploadText.style.textAlign = "center";
		tooltipSAPuploadText.innerHTML = "Upload the sampling and analysis plan (SAP) or select a " +
		"completed SAP from the list.";
		let tooltipSAPupload = createTooltip(tooltipSAPuploadText);

		this._inputSapFile = document.createElement('input');
		this._inputSapFile.id = "SAP_File_Input";

		this._buttonUploadSap = document.createElement('button');
		this._buttonUploadSap.className = "small-button";
		this._buttonUploadSap.id = "bUpload";
		this._buttonUploadSap.style.margin = "0 1em 0 1em";
		this._buttonUploadSap.innerText = "Upload";

		this._buttonPickSap = document.createElement('button');
		this._buttonPickSap.className = "small-button";
		this._buttonPickSap.id = "bPickCompletedButton";
		this._buttonPickSap.style.margin = "0 1em 0 1em";
		this._buttonPickSap.innerText = "Pick from Completed SAPs";

		let layoutSap = document.createElement('span');
		layoutSap.style.gridRow = "6";
		layoutSap.style.gridColumn = "2";
		layoutSap.append(
			this._inputSapFile,
			this._buttonUploadSap,
			makeOr(),
			this._buttonPickSap, tooltipSAPupload
		);

		//////////////

		this.main_layout.append(
			createLabel(textIndicatesRequired, 1, "1/3"),
			createLabel("* Location: (MGRS) ", 2, 1),
			layoutLocation,
			createLabel("* HRA Objective: ", 3, 1),
			layoutObjective,
			createLabel("Selection by: ", 4, 1),
			this._RadioSamplingReasonGrid,
			createLabel("* Population at Risk: ", 5, 1),
			layoutRisk,
			createLabel("SAP File: ", 6, 1),
			layoutSap
		);

		//////////////////////////////

		this._Built = false;
	}

	connectedCallback()
	{
		if ( !this._Built )
		{
			this.append(this.main_layout);
			this._Built = true;
		}
	}

	get Objective()
	{
		return this._inputObjective.value;
	}

	set Objective(val)
	{
		this._inputObjective.value = val;
	}

	get SiteMGRS()
	{
		return this._inputSiteMGRS.value;
	}

	set SiteMGRS(val)
	{
		this._inputSiteMGRS.value = val;
	}

	get PopulationAtRisk()
	{
		return this._inputPupulationAtRisk.value;
	}

	set PopulationAtRisk(val)
	{
		this._inputPupulationAtRisk.value = val;
	}

	get SapFile()
	{
		return this._inputSapFile.value;
	}

	set SapFile(val)
	{
		this._inputSapFile.value = val;
	}

	get SelectedExposurePathway()
	{
		let elements = this._SiteExposurePathwayRadioElements;

		for(let i = 0; i < elements.length; i++)
		{
			if( elements[i].checked )
				return elements[i].value;
		}
	}

	set SelectedExposurePathway(val)
	{
		let elements = this._SiteExposurePathwayRadioElements;

		for(let i = 0; i < elements.length; i++)
		{
			if( elements[i].value == val )
			{
				elements[i].checked = true;
				return;
			}
		}
	}

	get Location()
	{
		try
		{
			return this._inputLocation.Value;
		}
		catch(err)
		{
			return undefined;
		}
	}

	set Location(val)
	{
		this._inputLocation.Value = val;
	}

	get IndividualsAtRisk()
	{
		return Number.parseInt( this._inputIndividualsAtRisk.value.split(',').join('') );
	}

	set IndividualsAtRisk(val)
	{
		this._inputIndividualsAtRisk.value = val ? val.toString() : '';
	}

	set onSearch(func)
	{
		this._buttonSearch.onclick = func;
	}

	set onLocationChange(func)
	{
		this._inputLocation.onchange = func;
	}

	set onRadioSelectionChange(func)
	{
		this._RadioSamplingReasonGrid.onchange = func;
	}
}

customElements.define('jhrm-rhra-details-form', RapidHraDetailsForm);
