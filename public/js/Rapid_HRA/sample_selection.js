import { Request } from "../request.js";
import { Sortable } from "../sortable.js";
import { Datetime } from "../shared/datetime.js";


export class RapidHraSampleSelectRow extends HTMLTableRowElement
{
	constructor(_ShowSelection)
	{
		super();

		this._Built = false;
		this._ShowSelection = _ShowSelection;

		this._ElmSelected = document.createElement('input');
		this._ElmSelected.type = 'checkbox';

		this._ElmSampleId = document.createElement('td');
		this._ElmSamplingPoint = document.createElement('td');
		this._ElmOperationMode = document.createElement('td');
		this._ElmChemicalOfConcern = document.createElement('td');
		this._ElmCASRN = document.createElement('td');
		this._ElmStartDateTime = document.createElement('td');
		this._ElmEndDateTime = document.createElement('td');
		this._ElmDuration = document.createElement('td');
		this._ElmPeakExceedsNegMeg = document.createElement('td');

		this._StartDateTime = new Date();
		this._EndDateTime = new Date();
	}

	connectedCallback()
	{
		if ( !this._Built )
		{
			let TdSelected = document.createElement('td');
			TdSelected.append(this._ElmSelected);

			if( this._ShowSelection )
			{
				this.append(
					TdSelected
				);
			}

			this.append(
				this._ElmSampleId,
				this._ElmSamplingPoint,
				this._ElmOperationMode,
				this._ElmChemicalOfConcern,
				this._ElmCASRN,
				this._ElmStartDateTime,
				this._ElmEndDateTime,
				this._ElmDuration,
				this._ElmPeakExceedsNegMeg
			);

			this._Built = true;
		}
	}

	set Selected(val)
	{
		this._ElmSelected.checked = val;
	}

	get Selected()
	{
		return this._ElmSelected.checked;
	}

	set SampleID(val)
	{
		this._ElmSelected.value = val;
		this._ElmSampleId.innerText = val;
	}

	get SampleID()
	{
		return this._ElmSampleId.textContent;
	}

	set SamplingPoint(val)
	{
		this._ElmSamplingPoint.innerText = val;
	}

	get SamplingPoint()
	{
		return this._ElmSamplingPoint.textContent;
	}

	set OperationMode(val)
	{
	//if sensor type = jcad, operation mode= monitor mode, if hapsite, gs/ms, if multirae, continuous or survey
	/* This is sudo code placeholder for now until we tie in the real sample data from the sensors and get access to the sensor type field 	
	if(sensor == jcad){
			this._ElmOperationMode.innerText = "Monitor Mode";
		}
		elseif(sensor == hapsite){
				this._ElmOperationMode.innerText = "GS/MS";
		}
		elseif(sensor == multirae){
				if(//multirae mode = continuous
					){
				this._ElmOperationMode.innerText = "Continuous";
					}
				elseif( //multrae mode = survey
					){
						this._ElmOperationMode.innerText = "Survey";
					}  
		}
		else{ */
			this._ElmOperationMode.innerText = val;
		//}

	}

	get OperationMode()
	{
		return this._ElmOperationMode.textContent;
	}

	set ChemicalOfConcern(val)
	{
		this._ElmChemicalOfConcern.innerText = val;
	}

	get ChemicalOfConcern()
	{
		return this._ElmChemicalOfConcern.textContent;
	}

	set CasNumber(val)
	{
		this._ElmCASRN.innerText = val;
	}

	get CasNumber()
	{
		return this._ElmCASRN.textContent;
	}	

	set StartDateTime(val)
	{
		
		if ( !(val instanceof Datetime) )
			throw new Error("Start Time needs to be a Datetime.");

		this._StartDateTime = val;
		this._ElmStartDateTime.innerText = val;
	}

	get StartDateTime()
	{
		return this._StartDate;
	}

	set EndDateTime(val)
	{
		if ( !(val instanceof Datetime) )
			throw new Error("End Time needs to be a Datetime.");

		this._EndDateTime = val;
		this._ElmEndDateTime.innerText = val;
	}

	get EndDateTime()
	{
		return this._EndDateTime;
	}

	set Duration(val)
	{
		this._ElmDuration.innerText = val;
	}

	get Duration()
	{
		return this._ElmDuration.textContent;
	}

	set PeakExceedsNegMeg(val)
	{
		if(val == 'Yes')
		{
			this.style.backgroundColor = "yellow";
		}
		this._ElmPeakExceedsNegMeg.innerText = val;
	}

	get PeakExceedsNegMeg()
	{
		return this._ElmPeakExceedsNegMeg.textContent;
	}

	static fromObjData(record, _ShowSelection)
	{
		let ret = new RapidHraSampleSelectRow(_ShowSelection);
		ret.StartDateTime = record.start_date_time;
		ret.EndDateTime = record.end_date_time;
		ret.SamplingPoint = record.sampling_point;
		ret.OperationMode = record.operation_mode == null ? 'N/A' : record.operation_mode;
		ret.SampleID  = record.id;
		ret.ChemicalOfConcern = record.chemical_name;
		ret.CasNumber = record.cas_number;
		ret.Duration = record.duration;
		ret.PeakExceedsNegMeg = record.exceeds_meg;
		ret.Selected = record.selected;
		return ret;
	}
}

customElements.define('jhrm-rhra-sample-select-row', RapidHraSampleSelectRow, { extends: 'tr' });

export class RapidHraSampleSelectTable extends HTMLElement
{
	constructor()
	{
		super();

		
		this._Built = false;

		this._ShowSelection = this.getAttribute('show-selection') == "true";

		let createTH = (text) =>
		{
			let elm = document.createElement('th');
			elm.innerHTML = text;

			return elm;
		}

		this._ElmHeader = document.createElement('thead');
		this._ElmBody = document.createElement('tbody');

		if(this._ShowSelection)
		{
			this._ElmHeader.append(createTH('Sel.'))
		}

		this._ElmHeader.append(
			createTH('Sample ID'),
			createTH('Sampling Point'),
			createTH('Operation Mode'),
			createTH('Chemical of Concern'),
			createTH('CAS Number'),
			createTH('Start Date/Time'),
			createTH('End Date/Time'),
			createTH('Duration (min)'),
			createTH('Peak Exceeds Screening MEG (Y/N)')
		);
		
		this._ElmTable = document.createElement('table');
		this._ElmTable.append(this._ElmHeader, this._ElmBody);
		this._ElmTableWrapper = document.createElement('div');
		this._ElmTableWrapper.append(this._ElmTable);

		Sortable(this._ElmTable);
	}

	connectedCallback()
	{
		if (!this._Built)
		{
			this.style.display = "grid";
			this.style.gridTemplateColumns = "auto 1fr";
			this.style.gridTemplateRows = "auto 1fr";
			this.style.rowGap = "0.5rem";
			this._ElmTableWrapper.style.gridRow = "2";
			this._ElmTableWrapper.style.gridColumn = "1/3";

			this._ElmTableWrapper.className = "tablewrapper";
			this.append(this._ElmTableWrapper);
			this._Built = true;
		}
	}

	appendSampleRow(row)
	{
		if (row instanceof RapidHraSampleSelectRow)
			this._ElmBody.append(row);
	}

	populateFromJSON(data)
	{
		this._ElmBody.innerHTML = "";
		data.forEach(record => {
			let nextRow = RapidHraSampleSelectRow.fromObjData(record, this._ShowSelection);
			this.appendSampleRow(nextRow);
		});
		this._ElmTable.dispatchEvent(new Event("jhrmContentUpdated"));
	}

	SetSelectionById(id, state)
	{
		let tBody = this._ElmBody;

		for (let i = 0; i < tBody.children.length; ++i)
		{
			let element = tBody.children.item(i);

			if (element.SampleID == id)
			{
				element.Selected = state;
			}
		}

	}

	get SelectedIDs()
	{
		let tBody = this._ElmBody;
		let ret = [];

		for (let i = 0; i < tBody.children.length; ++i)
		{
			let element = tBody.children.item(i);

			if (element.Selected)
				ret.push(element.SampleID);
		}

		return ret;
	}
}

customElements.define('jhrm-rhra-sample-select-table', RapidHraSampleSelectTable);
