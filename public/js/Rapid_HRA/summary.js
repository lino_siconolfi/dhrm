import { RiskAssessment } from "../risk_assessment.js"
import { JhrmHelpIcon } from "../help_icon.js"
import { Request } from "../request.js";
import { EHRA_STATUS } from "../shared/Constants.js"
import { Sortable } from "../sortable.js";
import { Datetime } from "../shared/datetime.js"
import { Filterable } from "../filterable.js";

export class RapidHraSummaryRow extends HTMLTableRowElement
{
	constructor()
	{
		super();

		this._Built = false;

		this._ElmId = document.createElement('td');
		this._ElmLocation = document.createElement('td');
		this._ElmChemicalOfConcern = document.createElement('td');
		this._ElmCasrn = document.createElement('td');
		this._ElmAcutePeak = document.createElement('td');
		this._ElmAcuteAverage = document.createElement('td');
		this._ElmCreatedDate = document.createElement('td');
		this._ElmCreatedBy = document.createElement('td');
		this._ElmStatus = document.createElement('td');
		this._ElmUploadedToDoehrs = document.createElement('td');
		this._ElmQaApproved = document.createElement('td');

		this._CreatedDate = undefined;
		this._UploadedToDoehrs = false;
		this._Id = undefined;
		this._QaApproved = false;

		this.Levels = RiskAssessment.RiskLevel;
	}

	connectedCallback()
	{
		if ( !this._Built )
		{
			this.append(
				this._ElmId,
				this._ElmLocation,
				this._ElmChemicalOfConcern,
				this._ElmCasrn,
				this._ElmAcutePeak,
				this._ElmAcuteAverage,
				this._ElmCreatedDate,
				this._ElmCreatedBy,
				this._ElmStatus,
				this._ElmUploadedToDoehrs,
				this._ElmQaApproved
			);

			this._Built = true;
		}
	}

	set Id(val)
	{	
		this._Id = val.ID;
		this._IDStatus = val.Status

		if (this._IDStatus === "COMPLETE")
		{

			let searchParams = new URLSearchParams();
			searchParams.append("rhra", this._Id);
			searchParams.append("page", "summary_page_two");

			let queryString = "?" + searchParams.toString();

			let anchorElement = document.createElement('a');
			anchorElement.href = "./new_rhra.html" + queryString;
			anchorElement.innerText = this._Id;

			this._ElmId.innerHTML = "";
			this._ElmId.append(anchorElement);

		}
		else 
		{
			let searchParams = new URLSearchParams();
			searchParams.append("rhra", this._Id);
			searchParams.append("page", "RapidDetailsPage");

			let queryString = "?" + searchParams.toString();

			let anchorElement = document.createElement('a');
			anchorElement.href = "./new_rhra.html" + queryString;
			anchorElement.innerText = this._Id;

			this._ElmId.innerHTML = "";
			this._ElmId.append(anchorElement);
		}
	}

	get Id()
	{
		return this._Id;
	}

	set Location(val)
	{
		this._ElmLocation.innerText = val;
	}

	get Location()
	{
		return this._ElmLocation.textContent;
	}

	set ChemicalOfConcern(val)
	{
		this._ElmChemicalOfConcern.innerText = val;
	}

	get ChemicalOfConcern()
	{
		return this._ElmChemicalOfConcern.textContent;
	}

	set CASRN(val)
	{
		this._ElmCasrn.innerText = val;
	}

	get CASRN()
	{
		return this._ElmCasrn.textContent;
	}

	set CreatedDate(val)
	{
		if ( !(val instanceof Datetime) )
			throw new Error("Start Time needs to be a Date.");

		this._CreatedDate = val;
		this._ElmCreatedDate.innerText = val;
	}

	get CreatedDate()
	{
		return this._CreatedDate;
	}

	set AcuteRiskPeakLevel(val)
	{
		if (val.acuteRisk == this.Levels.ExtremelyHigh)
		{	
			this._ElmAcutePeak.style.backgroundColor="Black";
			this._ElmAcutePeak.style.color = "White"
		}else if (val.acuteRisk == this.Levels.High)
		{
			this._ElmAcutePeak.style.backgroundColor="Red";
		}else if (val.acuteRisk == this.Levels.Moderate)
		{
			this._ElmAcutePeak.style.backgroundColor="Yellow";
		}else if (val.acuteRisk == this.Levels.Low)
		{
			this._ElmAcutePeak.style.backgroundColor="Green";
		}
		
		this._ElmAcutePeak.innerText = val.acuteRisk + '\n' + val.acuteConfidence;
	}

	get AcuteRiskPeakLevel()
	{
		return this._ElmAcuteRiskLevel.textContent;
	}

	set AcuteRiskAverageLevel(val)
	{
		if (val.averageRisk == this.Levels.ExtremelyHigh)
		{	
			this._ElmAcuteAverage.style.backgroundColor="Black";
			this._ElmAcuteAverage.style.color = "White"
		}else if (val.averageRisk == this.Levels.High)
		{
			this._ElmAcuteAverage.style.backgroundColor="Red";
		}else if (val.averageRisk == this.Levels.Moderate)
		{
			this._ElmAcuteAverage.style.backgroundColor="Yellow";
		}else if (val.averageRisk == this.Levels.Low)
		{
			this._ElmAcuteAverage.style.backgroundColor="Green";
		}
		
		this._ElmAcuteAverage.innerText = val.averageRisk + '\n' + val.averageConfidence;
	}

	get AcuteRiskAverageLevel()
	{
		return this._ElmAcuteAverage.textContent;
	}
	set CreatedBy(val)
	{
		this._ElmCreatedBy.innerText = val;
	}

	get CreatedBy()
	{
		return this._ElmCreatedBy.textContent;
	}

	set Status(val)
	{
		if (val.status === EHRA_STATUS.COMPLETE)
		{
			let searchParams = new URLSearchParams();
			searchParams.append("hra", val.id);
			
			let anchorElement = document.createElement('a');
			anchorElement.href = "./rhra_audit.html?" + searchParams.toString();
			anchorElement.innerText = val.status;

			this._ElmStatus.innerHTML = "";
			this._ElmStatus.append(anchorElement);

		}
		else 
		{
			this._ElmStatus.innerText = val.status;
		}
	}

	get Status()
	{
		return this._IDStatus;
	}

	set UploadedToDoehrs(val)
	{		
		this._UploadedToDoehrs = val;
		this._ElmUploadedToDoehrs.innerText = (val) ? "Yes" : "No";	
	}

	get UploadedToDoehrs()
	{
		return this._UploadedToDoehrs;
	}

	set QaApproved(val)
	{
		this._QaApproved = val;
		this._ElmQaApproved.innerText = (val) ? "Yes" : "No";
	}

	get QaApproved()
	{
		return this._QaApproved;
	}

	static fromObjData(record)
	{
		let nextRow = new RapidHraSummaryRow(record);
		nextRow.Id = {ID: record.id, Status : record.status};
		nextRow.Location = record.location;
		nextRow.ChemicalOfConcern = record.chemical_name;
		nextRow.CASRN = record.cas_number;
		if (record.status === 'INCOMPLETE' || record.status === null)
		{
			nextRow.AcuteRiskPeakLevel = {
				acuteRisk : '',
				acuteConfidence : ''
			};
			nextRow.AcuteRiskAverageLevel = {
				averageRisk : '',
				averageConfidence : '' 
			};
		}
		else
		{
			nextRow.AcuteRiskPeakLevel =  {
				acuteRisk : record.acute_peak_risk,
				acuteConfidence : '(' + record.acute_peak_confidence + ')'
			};
			nextRow.AcuteRiskAverageLevel = {
				averageRisk: record.acute_average_risk,
				averageConfidence : '(' + record.acute_average_confidence + ')'
			};
		}
		nextRow.CreatedDate = record.created_on;
		nextRow.CreatedBy = record.created_by;
		nextRow.Status =  record;
		nextRow.UploadedToDoehrs = (record.uploaded_to_doehrs == 1 ? true : false);
		nextRow.QaApproved = (record.qa_approved == 1 ?  true : false);

		return nextRow;
	}
}

customElements.define('jhrm-rhra-summary-row', RapidHraSummaryRow, { extends: 'tr' });

export class RapidHraSummaryTable extends HTMLElement
{
	constructor()
	{
		super();

		this._Built = false;

		let createTH = (contents) =>
		{
			let elm = document.createElement('th');
			elm.append(contents);

			return elm;
		}

		this._ElmHeader = document.createElement('thead');
		this._ElmBody = document.createElement('tbody');

		let IdHeader = document.createElement('th');
		IdHeader.style.whiteSpace = "nowrap";

		let HelpIcon = new JhrmHelpIcon();
		HelpIcon.style.whiteSpace = "normal";
		HelpIcon.innerHTML =
			"The links in the JHRM ID column are not clickable at this time and are a future functionality. " +
			"The link will take the user to the HRA directly.";

		IdHeader.append('HRA ID ', HelpIcon);

		let AcutePeakText = document.createElement('span');
		AcutePeakText.innerHTML = 'Acute&nbsp;Risk&nbsp;Peak/<wbr>Confidence&nbsp;Level';

		let AcuteAverageText = document.createElement('span');
		AcuteAverageText.innerHTML = 'Acute&nbsp;Risk&nbsp;Average/<wbr>Confidence&nbsp;Level';

		this._ElmHeader.append(
			IdHeader,
			createTH('Location'),
			createTH('Chemical of Concern'),
			createTH('CASRN'),
			createTH(AcutePeakText),
			createTH(AcuteAverageText),
			createTH('Created Date'),
			createTH('Created By'),
			createTH('Status'),
			createTH('Uploaded to DOEHRS'),
			createTH('QA Approved')
		);
		
		this._ElmTable = document.createElement('table');
		this._ElmTable.append(this._ElmHeader, this._ElmBody);
		this._ElmTableWrapper = document.createElement('div');
		this._ElmTableWrapper.append(this._ElmTable);

		this.SummaryRapidHRA();

		Sortable(this._ElmTable);
		new Filterable(this._ElmTable);
		
	}

	connectedCallback()
	{
		if (!this._Built)
		{
			this._ElmTableWrapper.className="tablewrapper";
			this.append(this._ElmTableWrapper);
			this._Built = true;
		}
	}

	async SummaryRapidHRA()
		{
			let results = await Request.get('/api/rapidhra/hra');
				results.forEach(
					record =>
					{
						let nextRow = RapidHraSummaryRow.fromObjData(record);
						this.appendSummaryRow(nextRow);
					}
				);
		}

	appendSummaryRow(row)
	{
		if (row instanceof RapidHraSummaryRow)
			this._ElmBody.append(row);
	}
}

customElements.define('jhrm-rhra-summary-table', RapidHraSummaryTable);

export class RapidHraSummaryDisplay extends HTMLElement
{
	constructor()
	{
		super();

		this._elmLocation = document.createElement('TD');
		this._elmExposurePathway = document.createElement('TD');
		this._elmChemicalOfConcern = document.createElement('TD');
		this._elmCasrn = document.createElement('TD');
		this._elmPopulationAtRisk = document.createElement('TD');
		this._elmPotentialHealth = document.createElement('TD');
		this._elmSamplingStartEnd = document.createElement('TD');
		this._elmAdditionalHra = document.createElement('TD');
		this._elmRecommended = document.createElement('TD');


		this._SamplingStart = null;
		this._SamplingEnd = null;

		this._view = RapidHraSummaryDisplay.InterimView;

		let makeRow = function (labelText, valueTdElement)
		{
			let row = document.createElement('tr');

			let heading = document.createElement('td');
			heading.innerHTML = labelText;

			row.append(heading, valueTdElement);

			return row;
		};

		this._tableElement = document.createElement('table');

		let tableData = document.createElement('tdata');

		this._tableElement.append(tableData);

		this._rowAddtlHra = makeRow("Additional HRA", this._elmAdditionalHra);
		this._rowRecommended = makeRow("Recommended", this._elmRecommended);

		if (this._view == RapidHraSummaryDisplay.InterimView)
		{
			this._rowAddtlHra.style.display = "none";
			this._rowRecommended.style.display = "none";
		}

		tableData.append(
			makeRow("HRA Location:", this._elmLocation),
			makeRow("Exposure Pathway", this._elmExposurePathway),
			makeRow("Chemical Of Concern", this._elmChemicalOfConcern),
			makeRow("CASRN:", this._elmCasrn),
			makeRow("Population at Risk:", this._elmPopulationAtRisk),
			makeRow("Potential Health Outcomes", this._elmPotentialHealth),
			makeRow("Sampling Start & End Date/Time", this._elmSamplingStartEnd),
			this._rowAddtlHra,
			this._rowRecommended
		);

		this._built = false;
	}

	connectedCallback()
	{
		if (!this._built)
		{
			this.style.display = "inline-block";
			this.append(this._tableElement);
			this._updateSamplingTimes();

			this._built = true;
		}
	}

	_updateSamplingTimes()
	{
		this._elmSamplingStartEnd.innerHTML = `${this._SamplingStart}<br>${this._SamplingEnd}`;
	}

	get Location()
	{
		return this._elmLocation.textContent;
	}

	set Location(val)
	{
		this._elmLocation.innerText = val;
	}

	get PopulationAtRisk()
	{
		return this._elmPopulationAtRisk.textContent;
	}

	set PopulationAtRisk(val)
	{
		this._elmPopulationAtRisk.innerText = val;
	}

	get SamplingStart()
	{
		return this._SamplingStart;
	}

	set SamplingStart(val)
	{
		if (val instanceof Datetime)
		{
			this._SamplingStart = val;
			this._updateSamplingTimes();
		}
		else
		{
			throw new Error("Argument must be a Date.");
		}
	}

	get SamplingEnd()
	{
		return this._SamplingEnd;
	}

	set SamplingEnd(val)
	{
		if (val instanceof Datetime)
		{
			this._SamplingEnd = val;
			this._updateSamplingTimes();
		}
		else
		{
			throw new Error("Argument must be a Date.");
		}
	}

	get PotentialHealth()
	{
		return this._elmPotentialHealth.textContent;
	}

	set PotentialHealth(val)
	{
		this._elmPotentialHealth.innerText = val;
	}

	get ChemicalOfConcern()
	{
		return this._elmChemicalOfConcern.textContent;
	}

	set ChemicalOfConcern(val)
	{
		this._elmChemicalOfConcern.innerText = val;
	}

	get Casrn()
	{
		return this._elmCasrn.textContent;
	}

	set Casrn(val)
	{
		this._elmCasrn.innerText = val;
	}

	get ExposurePathway()
	{
		return this._elmExposurePathway.textContent;
	}

	set ExposurePathway(val)
	{
		this._elmExposurePathway.innerText = val;
	}

	get AdditionalHra()
	{
		return this._elmAdditionalHra.textContent;
	}

	set AdditionalHra(val)
	{
		this._elmAdditionalHra.innerText = val;
	}

	get Recommended()
	{
		return this._elmRecommended.textContent;
	}

	set Recommended(val)
	{
		this._elmRecommended.innerText = val;
	}

	get ViewMode()
	{
		return this._view;
	}

	set ViewMode(val)
	{
		if (val == this._view)
			return;

		this._view = val;

		if (this._view == RapidHraSummaryDisplay.InterimView)
		{
			this._rowAddtlHra.style.display = "table-row";
			this._rowRecommended.style.display = "table-row";
		}
		else if (this._view == RapidHraSummaryDisplay.DetailView)
		{
			this._rowAddtlHra.style.display = "none";
			this._rowRecommended.style.display = "none";
		}
	}
}

RapidHraSummaryDisplay.InterimView = 0;
RapidHraSummaryDisplay.DetailView = 1;

customElements.define('jhrm-rhra-summary-display', RapidHraSummaryDisplay);