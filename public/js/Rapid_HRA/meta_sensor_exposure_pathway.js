import { Request } from "../request.js";
import { Sortable } from "../sortable.js";
import { JhrmTooltip } from "../tooltip.js"
import { MaterialIcon } from "../material_icon.js"

export class EnhancedDriExposurePathwayRow extends HTMLTableRowElement
{

	constructor()
	{
		super();

		this._Built = false;

		this._ElmSelected = document.createElement('input');
		this._ElmSelected.type = 'radio';

		this._ElmPathwayId = document.createElement('td');
		this._ElmPathwayName = document.createElement('td');
		this._ElmThreatSource = document.createElement('td');
		this._ElmHealthHazard = document.createElement('td');
		this._ElmExposurePointArea = document.createElement('td');
		this._ElmExposureMedium = document.createElement('td');
		this._ElmExposureRoute = document.createElement('td');
		this._ElmExposureDurationofConcern = document.createElement('td');
		this._ElmPopulationAtRiskDescription = document.createElement('td');

		this._PopulationAtRisk = undefined;
		this._PathwayId = undefined;
		this._Location = undefined;
	}

	connectedCallback()
	{
		if ( !this._Built )
		{
			let TdSelected = document.createElement('td');
			TdSelected.append(this._ElmSelected);

			this.append(
				TdSelected, this._ElmPathwayId,
				this._ElmPathwayName,this._ElmThreatSource, 
				this._ElmHealthHazard,this._ElmExposurePointArea,
				this._ElmExposureMedium,this._ElmExposureRoute,
				this._ElmExposureDurationofConcern,this._ElmPopulationAtRiskDescription
			);

			this._Built = true;
		}
	}

	set GroupName(val)
	{
		this._ElmSelected.name = val;
	}

	set Selected(val)
	{
		this._ElmSelected.checked = val;
	}

	get Selected()
	{
		return this._ElmSelected.checked && this.Visible;
	}

	set PathwayId(val)
	{
		this._PathwayId = val;

		let strVal = val.toString();
		this._ElmPathwayId.innerText = strVal;
	}

	get PathwayId()
	{
		return this._PathwayId;
	}

	set PathwayName(val)
	{
		this._ElmPathwayName.innerText = val;
	}

	get PathwayName()
	{
		return this._ElmPathwayName.textContent;
	}

	set ThreatSource(val)
	{
		this._ElmThreatSource.innerText = val;
	}

	get ThreatSource()
	{
		return this._ElmThreatSource.textContent;
	}

	set ExposureRoute(val)
	{
		this._ElmExposureRoute.innerText = val;
	}

	get ExposureRoute()
	{
		return this._ElmExposureRoute.textContent;
	}

	set HealthHazard(val)
	{
		this._ElmHealthHazard.innerText = val;
	}

	get HealthHazard()
	{
		return this._ElmHealthHazard.textContent;
	}

	set Location(val)
	{
		this._ElmLocation = val;
	}

	get Location()
	{
		return this._ElmLocation.textContent;
	}
	
	set ExposurePointArea(val)
	{
		this._ElmExposurePointArea.innerText = val;
	}

	get ExposurePointArea()
	{
	 	return this._ElmExposurePointArea.textContent;
	}

	set ExposureMedium(val)
	{
		this._ElmExposureMedium.innerText = val;
	}

	get ExposureMedium()
	{
	 	return this._ElmExposureMedium.textContent;
	}

	set ExposureDurationofConcern(val)
	{
		this._ElmExposureDurationofConcern.innerText = val;
	}

	get ExposureDurationofConcern()
	{
	 	return this._ElmExposureDurationofConcern.textContent;
	}

	set PopulationAtRiskDescription(val)
	{
		this._PopulationAtRiskDescription = val;
		this._ElmPopulationAtRiskDescription.innerText = val.toString();
	}

	get PopulationAtRiskDescription()
	{
		return this._PopulationAtRiskDescription;
	}

	set Visible(val)
	{
		this.style.display = val ? "table-row" : "none";
	}

	get Visible()
	{
		let style = window.getComputedStyle(this);
		return (style.display != "none");
	}
}

customElements.define('jhrm-dri-exposure-pathway-row', EnhancedDriExposurePathwayRow, { extends: 'tr' });

export class EnhancedDriExposurePathwayTable extends HTMLElement
{
	constructor()
	{
		super();

		this._Built = false;
		this._onupdate = undefined;

		let createTooltip = function(tip)
		{
			let icon = MaterialIcon.Create("help_outline");
			icon.style.fontSize = "0.9em";

			let tooltip = new JhrmTooltip();
			tooltip.Target = icon;
			tooltip.append(tip);

			let ret = document.createElement('span');
			ret.append(icon, tooltip);

			return ret;
		}

		let makeBr = function()
		{
			return document.createElement('br');
		};

		this._Layout = document.createElement('div');

		let SubLayout = document.createElement('div');
		SubLayout.style.display = "grid";
		SubLayout.style.gridTemplateColumns = "auto 1fr";
		SubLayout.style.gridGap = "0.5em";
		SubLayout.style.lineHeight = "2em";
		SubLayout.innerText = "* Sensor Exposure Pathways";

		let tooltipSensorExposurePathwayText = document.createElement('span');
		tooltipSensorExposurePathwayText.style.textAlign = "center";
		tooltipSensorExposurePathwayText.innerHTML = "Select the appropriate exposure pathway for the DRI.";
		
		let tooltipSensorExposurePathway = createTooltip(tooltipSensorExposurePathwayText);

		SubLayout.append(tooltipSensorExposurePathway)

		this._Layout.append(
			makeBr(),
			SubLayout
			
		);

		let createTH = (text) =>
		{
			let elm = document.createElement('th');
			elm.innerHTML = text;

			return elm;
		}

		this._ElmHeader = document.createElement('thead');
		this._ElmBody = document.createElement('tbody');

		this._ElmHeader.append(
			createTH('Sel.'),
			createTH('Exposure Pathway ID'),
			createTH('Name of Exposure Pathway'),
			createTH('Threat Source'),
			createTH('Health Hazard'),
			createTH('Exposure Point Area'),
			createTH('Exposure Medium'),
			createTH('Exposure Route'),
			createTH('Exposure Duration of Concern'),
			createTH('Population at Risk Description')
			
		);
		
		this._ElmTable = document.createElement('table');
		this._ElmTable.append(this._ElmHeader, this._ElmBody);
		this._ElmTableWrapper = document.createElement('div');
		this._ElmTableWrapper.append(this._ElmTable);

		this._AllRows = [];

		Sortable(this._ElmTable);
	}

	connectedCallback()
	{
		if (!this._Built)
		{
			this._ElmTableWrapper.className="tablewrapper";
			this.append(this._Layout,this._ElmTableWrapper);

			Request.get('/api/rapidhra/meta/pathway').then(
				function(response)
				{
					for (let index in response)
					{
						let record = response[index];
						let nextRow = new EnhancedDriExposurePathwayRow();
						
						nextRow.PathwayId = record.id;
						nextRow.PathwayName = record.name;
						nextRow.Location = record.location;
						nextRow.ThreatSource = record.threat_source;
						nextRow.HealthHazard = record.health_hazard;
						nextRow.ExposurePointArea = record.exposure_point_area;
						nextRow.ExposureMedium = record.exposure_medium;
						nextRow.ExposureRoute = record.exposure_route;
						nextRow.ExposureDurationofConcern = record.exposure_duration_of_concern;
						nextRow.PopulationAtRiskDescription = record.exposed_population;
						nextRow.GroupName = this.id;

						this._ElmBody.append(nextRow);
						this._AllRows.push(nextRow);

						if (undefined != this._onupdate)
							this._onupdate();
					}
				}.bind(this)
			);

			this._Built = true;
		}
	}

	appendExposureRow(row)
	{
		if (row instanceof EnhancedDriExposurePathwayRow)
		{
			row.GroupName = this.id;
			this._ElmBody.append(row);
		}
	}

	filter(location, media)
	{
		let tBody = this._ElmBody;

		let filterByLocation = (location != undefined);
		let filterByMedia = (media != undefined);

		if ( filterByMedia )
			media = media.toLowerCase();
		
		if ( filterByLocation )
			location = location.toLowerCase();

		for (let i = 0; i < this._AllRows.length; ++i)
		{
			let element = this._AllRows[i];

			element.Visible = 
				(!filterByLocation || element.Location .toLowerCase() === location) &&
				(!filterByMedia || element.EnvironmentalMedia.toLowerCase() === media);
		}

		return undefined;
	}

	/**
	 * 
	 * @param { number } id
	 * 	The id of the pathway that should be selected.
	 * 
	 * @param { boolean } selectInvisible
	 * 	If true, the pathway will be selected even if filtering makes it invisible.
	 * 	this defaults to false.
	 * 
	 * @returns
	 * 	True if the pathway was found and selected, false otherwise.
	 */
	selectPathwayId(id, selectInvisible)
	{

		let tBody = this._ElmBody;

		for (let i = 0; i < tBody.children.length; ++i)
		{
			let element = tBody.children.item(i);

			if (element.PathwayId == id)
			{
				if (selectInvisible || element.Visible)
				{
					element.Selected = true;
					return true;
				}
				else
				{
					return false;
				}
			}
		}

		return false;
	}

	get SelectedRow()
	{
		let tBody = this._ElmBody;

		for (let i = 0; i < this._AllRows.length; ++i)
		{
			let element = this._AllRows[i];

			if (element.Selected)
				return element;
		}

		return undefined;
	}

	get SelectedID()
	{
		let row = this.SelectedRow;
		return (undefined != row) ? row.PathwayId : undefined;
	}

	set onupdate(func)
	{
		this._onupdate = func;
	}

	get Locations()
	{
		let tBody = this._ElmBody;
		let ret = new Set();

		for (let i = 0; i < this._AllRows.length; ++i)
		{
			let element = this._AllRows[i];
			ret.add(element.Location);
		}

		let retArray = Array.from(ret);
		retArray.sort();

		return retArray;
	}
}

customElements.define('jhrm-dri-exposure-pathway-table', EnhancedDriExposurePathwayTable);