module.exports = {
	"debug" : true,
	"staticMaxAge" : 0,
	"port" :  9000,
	"session" : {
		"secret" : "hns7eyshfkt9384hdfkfd0385756stgbdmf7364gfdkf76rh",
		"cookieMaxAge" : null
	},
	"api" : {
		"name" : "JHRM Rest API",
		"version" : "1.0.0"
	},
	"db" : {
		"connection" : {
			"host": 'host.docker.internal',
			"user" : 'root',
			"database" : 'jhrmecddb'
	  }     
	},
	"smartabase" : 
	{
		"host" : "vse.smartabase.com",
		"path" : "/sandbox/api/v1",
		"default_username" : "lyla.vela",
		"default_password" : "ABc1234567"
	}
};