module.exports = {
	"debug" : true,
	"maxFileUploadSize" : 2000000,
	"staticMaxAge" : 0,
	"port" :  9000,
	"security" : {
		"okta" : {
			"enabled" : true,
			"issuer" : "https://lmigov.okta.com/oauth2/default",
			"audience" : "api://default"
		}
	},
	"session" : {
		"secret" : "hns7eyshfkt9384hdfkfd0385756stgbdmf7364gfdkf76rh",
		"cookieMaxAge" : null
	},
	"api" : {
		"name" : "JHRM Rest API",
		"version" : "1.0.0"
	},
	"db" : {
		"connection" : {
			"host": 'localhost',
			"user" : 'root',
			"database" : 'jhrmecddb'
	  }     
	},
	"smartabase" : 
	{
		"host" : "vse.smartabase.com",
		"path" : "/sandbox/api/v1",
		"default_username" : "lyla.vela",
		"default_password" : "ABc1234567"
	},
	"mhce" : 
	{
		// Development address devmcare.tatrc.org
    	// Production address mcare.tatrc.org
		"host" : "devmcare.tatrc.org",
		"default_username" : "tburkeapi",
		"default_password" : "vZ8opdq!jrq3e.8UMdxn#"
	}
};