# JHRM ECD NodeJS Application

## Setup

Follow these instructions to get set up for development and testing of the JHRM web portal. For deployment instructions, see confluence. 

### Requirements

1. Install NodeJS v16 or greater
2. MySQL or MariaDB
3. A git client such a Torrtoise, SourceTree, or GitKraken
4. Visual Studio Code
5. MySQL workbench is also helpful
6. Review the readme.txt in the database directory for information regarding the installation and use of db-migrate to perform backward/forward migrations.

### Install (Terminal)

In a terminal application in the project directioy, run the following command: 

```npm install```

Two options for database preparation...

1.
Open Terminal and change to the 'database' directory and call
```db-migrate up --config devdb.json```

The database will be created and loaded OR deleted, created, and loaded if the database already existed.

For more information see:
https://code.lmi.org/confluence/display/JHRM/Database+Migration+System+Proposal

OR,

2.
Run the SQL scripts in /database/init_Scripts, either manually or via the batch file. 

```mysql -u root -p < ./database/init_scripts/create_tables.sql``` 
```mysql -u root -p < ./database/init_scripts/populate_tables.sql```

### Troubleshooting

#### ER_NOT_SUPPORTED_AUTH_MODE: Client does not support authentication protocol requested by server

This error means that the version of MySQL or MariaDB you are using is expecting a different hashing method than NodeJS. To fix it, run the following command in MySQL Workbench:

```
ALTER USER '[yourusername]'@'localhost' 
IDENTIFIED WITH mysql_native_password 
BY '[yourpassword]'
```

#### SQL error mentioning "group by"

This error means that You are using a version of 

```
ALTER USER '[yourusername]'@'localhost' 
IDENTIFIED WITH mysql_native_password 
BY '[yourpassword]'
```

## Running the project

The project can be run by setting the `NODE_ENV` environment variable and telling node to execute server.js. There are several scripts defined in the package.json file to assist with this.

### Development

Create a file in `./config` called `local.js`, using `./config/default.js` as a template, and update the database connection information to match your local environment. 

Run the following command:

```npm run dev```

### Docker

Docker must be installed for the following.

Create a file in `./config` called `local-docker.js`, using `./config/default-docker.js` as a template, and update the database connection information to match your local environment. 

Set necessary environment variables in the docker-compose-local.yml file

Run the following command:

```docker-compose -f docker-compose-local.yml up --build -d```

### Production

Ensure that the configuration in `./config/production.js` is correct. 

Run the following command:

```npm start```

### Other Configurations

To run with a different configuration file, such as in production, you will need to sed the environment variable `NODE_ENV`. The way you accomplish this depends on the system. Options for doing so from the command below: 

#### Windows

```SET NODE_ENV=my_env && node server.js```

#### Mac / Linux

```NODE_ENV=my_env && node server.js```

## Project structure

### ./config

Project configuration. You are encouraged to create your own configuration file which is not checked in to bitbucket in order to configure the project for your development environment. 

To use configuration values in your code, require the config package:

```var config = require('config');```

And then use the get method with the path to the config item you want:

```config.get('my.item')```

### ./public

All the files in this folder will be served by the NodeJs server, unless they begin with a dot ".". This is the place for browser-side code. 

### ./src

Where our custom code for the front end and the middletier live. This is the place for server-side code. 

### ./src/api

Middletier code. 

#### ./src/handlers

Other route handlers that need to be defined such as health checks. 

#### ./src/pages

Front end code that needs to be compuled or processed. Pages defined here will be built with webpack and moved to `../public`. The webpack configuration will look for a file with a matching name in `./src/pages/js` and will compile and minify the code there along with any react or other plugins used, and will also add script tags to the html automatically. 

For an example of this, see `./src/pages/issuance_tool.html` and `./src/pages/js/issuance_tool.js`. 

#### ./src/scripts

Other miscelaneous scripts, such as database loading, etc. 

#### ./src/shared

This is a special folder, and files here can be loaded both by the server and the front end. 

#### ./src/scripts

This is for code that will not run as a part of the project itself, but for performing certain tasks, such as loading data into the database. 

### serve.js

The entrypoint for the application. Sets up ExpressJs and serves the frontend. 

### package.json

The project definition. Declares dependancies. 

## Libraries / Frameworks 

### Express / Routing

This project uses the express framework to handle HTTP requests and responses.

Express works by attaching handlers to paths which are defined by strings. A handler can resolve the request, or it can take some action, such as updating the log file, and then pass the request on to the next matching handler. 

The first handler that is defined in `server.js` is a middleware called Express Static which will serve static files based on their path and filename. This is confgured to serve the files in `/public`.

After that, we use custom handlers to implement the REST API. 

Each handler is passed in a request and a response object as the first two parameters. The following will match `\my-path` and execute the function. 

```
app.get('/my-path', function(req, res , next){
    //handle request here. 
    res.send("This is my response!");
});
```

### Knex / Database

To connect to the database, you ca use the knex library. Simply require it in your file like so: 

```
const db = require('./src/util/db');

db('myTable')
    .select('myColumn')
    .where('myCondition',myValue)
    .then(result=>{
        // Do something with result
    });
```

See the knex documentation for the full capabilities. 

### Express Session / Sessions

Sessions can be accessed from the req object in any request handler. Simply use the req.session object, and data placed there will be persisted in the database. 

````
router.get('/my-path', function(req, res){
    // Check the session for a flag and update a counter if it is found
    if(req.session.myProperty === true)
    {
        req.session.counter++;
    }
    req.end("All done!");
});
````

## Coding Style / Tips

### Handling asynchronous calls

Many of the calls you will make, such as querying the database or an external API, will be asynchronous and use promises or callbacks. Express supports this in route handlers by waiting for you to call res.json() or res.end(), which could happen in a callback or promise like so:

```
router.get('/events', function (req, res) {
	db('issuance_event')
	    .then(function(result){
	    	res.json(result);
	    })
})
```

However, sometimes you will need to make several calls, and chaining promises together can be less clear. It is recommended to use async / await to make it easier to reason about the code. It will allow you to write the code as if it was synchronous, without having to explicitly define callbacks or promise handlers. Note that the function itself must be defined as async before you can use await inside it. 

```
router.get('/events', async function (req, res) {
	var response = await db('issuance_event');
    if(response.length == 0)
    {
        // Do something
    }
    else
    {
        res.json(response);
    }	    
})
```

### Documentation / Comments

API Documentation is generated automatically from commend blocks on the request handlers. At a minimum, the @api comment tag is required in order to create an entry in the documentation. The following is an example:

```
/**
 * @api {get} /api/enhancedhra/pathway/:pathwayId/samples Get All Samples
 * @apiDescription Get all samples associated with an exposure pathway
 * @apiParam {Number} pathwayId DOEHRS ID of the exposure pathway
 * @apiGroup Enhanced HRA
 * @apiSuccess {Object[]} Sample[] Array of Samples
 * @apiSuccess {String} sample.id
 * @apiSuccess {Number} sample.id_doehrs_pathway_id
 * @apiSuccess {String} sample.local_sample_id 
 * @apiSuccess {String} sample.location 
 * @apiSuccess {String} sample.sampling_point 
 * @apiSuccess {Date} sample.start_time 
 * @apiSuccess {String} sample.exposure_notes 
 * @apiSuccess {String} sample.lab_import_status 
 * @apiSuccess {String} sample.status 
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    [{
 *        "id": "0000D9HN",
 *        "id_doehrs_pathway_id": 14686,
 *        "local_sample_id": "IRQ_TAJI_20150228_TO1401",
 *        "location": "TAJI",
 *        "sampling_point": "FLIGHT LINE",
 *        "start_time": "2015-02-28T19:15:00.000Z",
 *        "exposure_notes": "",
 *        "lab_import_status": "Results Loaded",
 *        "status": "Approved by QA"
 *    }]
 * @apiErrorExample {none} Not Found Response
 *    HTTP/1.1 404 Not Found
 * @apiErrorExample {none} Server Error Response
 *    HTTP/1.1 500 Internal Server Error
 */
```
