// Initial test file for Jest. Reference Jest documentation at https://jestjs.io/docs/getting-started

const IfaObject = require('../../../src/shared/IFA');
const dateTime = require('../../../src/shared/datetime');
const constants = require('../../../src/shared/Constants');

let ifa = new IfaObject.IfaReport();
let now = new dateTime.Datetime(new Date());


ifa.short_name = 'ifa name'; 
ifa.summary = 'ifa summary';
ifa.start_datetime = now;
ifa.id = 9;
ifa.submission_status = constants.IFA_SUBMISSION_STATUS.NOT_SUBMITTED;
ifa.status = constants.IFA_STATUS.INCOMPLETE;
ifa.report_start_datetime = now;
ifa.report_completion_datetime = now;
ifa.name = "Volcanic Eruption";
ifa.start_datetime = now;
ifa.end_datetime = now;
ifa.ccir_sigact_num = "CDA128674";
ifa.scenario = constants.IFA_INCIDENT_SCENARIO.TRAINING;
ifa.incident_type = constants.IFA_INCIDENT_TYPE.CHEMICAL;
ifa.cause = constants.IFA_INCIDENT_CAUSE.UNKNOWN
ifa.cause_descr = "Chemical Exposure";
ifa.units_involved = "ounce"
ifa.were_you_at_incident_location = true;
ifa.ppe_available = "Masks";
ifa.ppe_decontamination = "Sterilize";
ifa.area_equip_decontamination = "Anti Bacterial";
ifa.countermeasures_used = "Neutralizer";
ifa.action_risk_comms = false;
ifa.risk_comms_descr = "Risk Comms";

describe("IFA object", () => {
  test('has property short_name', () => {
    expect(ifa).toHaveProperty('short_name', 'ifa name');
  });

});

describe("IFA object", () => {
  test('has property summary', () => {
    expect(ifa).toHaveProperty('summary', 'ifa summary');
  });

});

describe("IFA object", () => {
  test('has property id', () => {
    expect(ifa.id).toBe(9);
  });

});

describe("IFA object", () => {
  test('has property submission status', () => {
    expect(ifa).toHaveProperty('submission_status', constants.IFA_SUBMISSION_STATUS.NOT_SUBMITTED);
  });
});

describe("IFA object", () => {
  test('has property submission status', () => {
    expect(ifa).toHaveProperty('status', constants.IFA_STATUS.INCOMPLETE);
  });
});

describe("IFA object", () => {
  test('has property report start datetime', () => {
    expect(ifa).toHaveProperty('report_start_datetime', now);
  });
});

describe("IFA object", () => {
  test('has property report completion datetime', () => {
    expect(ifa).toHaveProperty('report_completion_datetime', now);
  });
});

describe("IFA object", () => {
  test('has property name', () => {
    expect(ifa).toHaveProperty('name', 'Volcanic Eruption');
  });
});

describe("IFA object", () => {
  test('has property start datetime', () => {
    expect(ifa).toHaveProperty('start_datetime', now);
  });
});

describe("IFA object", () => {
  test('has property report end datetime', () => {
    expect(ifa).toHaveProperty('end_datetime', now);
  });
});

describe("IFA object", () => {
  test('has property CCIR Sig AC Num', () => {
    expect(ifa).toHaveProperty('ccir_sigact_num', "CDA128674");
  });
});

describe("IFA object", () => {
  test('has property incident scenario', () => {
    expect(ifa).toHaveProperty('scenario', constants.IFA_INCIDENT_SCENARIO.TRAINING);
  });
});

/*describe("IFA object", () => {
  test('has property incident type', () => {
    expect(ifa.incident_type).toHaveProperty(constants.IFA_INCIDENT_TYPE.CHEMICAL);
  });
});*/

describe("IFA object", () => {
  test('has property incident scenario', () => {
    expect(ifa).toHaveProperty('cause', constants.IFA_INCIDENT_CAUSE.UNKNOWN);
  });
});

describe("IFA object", () => {
  test('has property cause description', () => {
    expect(ifa).toHaveProperty('cause_descr', 'Chemical Exposure');
  });
});

describe("IFA object", () => {
  test('has property units involved', () => {
    expect(ifa).toHaveProperty('units_involved', 'ounce');
  });
});

describe("IFA object", () => {
  test('has property were you at the incident location', () => {
    expect(ifa.were_you_at_incident_location).toBeTruthy;
  });
});

describe("IFA object", () => {
  test('has property PPE available', () => {
    expect(ifa).toHaveProperty('ppe_available', 'Masks');
  });
});

describe("IFA object", () => {
  test('has property PPE decontamination', () => {
    expect(ifa).toHaveProperty('ppe_decontamination', 'Sterilize');
  });
});

describe("IFA object", () => {
  test('has property area equip decontamination', () => {
    expect(ifa).toHaveProperty('area_equip_decontamination', 'Anti Bacterial');
  });
});

describe("IFA object", () => {
  test('has property countermeasures used', () => {
    expect(ifa).toHaveProperty('countermeasures_used', 'Neutralizer');
  });
});

describe("IFA object", () => {
  test('has property action risk comms', () => {
    expect(ifa.action_risk_comms).toBeFalsy;
  });
});

describe("IFA object", () => {
  test('has property risk communications description', () => {
    expect(ifa).toHaveProperty('risk_comms_descr', 'Risk Comms');
  });
});
