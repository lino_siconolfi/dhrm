const locObject = require('../../../src/shared/location');

let location = new locObject.Location(38.769985, -77.157998);

describe("Verify longitude", () => {
    test('has value of 38.769985', () => {
      expect(location.lon).toBe(38.769985);     
    }); 
});

describe("Verify latitude", () => {
    test('has value of -77.157998', () => {
      expect(location.lat).toBe(-77.157998);     
    }); 
});

describe("Verify gridRef MGRS", () => {
    test('has value of 37CDQ9429335584', () => {
      expect(location.gridRef).toEqual("37CDQ9429335584");     
    }); 
}); 
 
describe("Verify location MGRS", () => {
    test('has value of 37CDQ9429335584', () => {
      expect(location.mgrs).toEqual("37CDQ9429335584");    
    }); 
}); 

describe("Verify longitude of a point", () => {
    test('has a value of 38.769985', () => {
        let val = location.point;
        expect(val[0]).toBe(38.769985);
    });
});

describe("Verify latitude of a point", () => {
    test('has a value of -77.157998', () => {
        let val = location.point;
        expect(val[1]).toBe(-77.157998);
    });
});

describe("Verify mgrs string", () => {
    test('has a value of 37CDQ9429335584', () => {
        expect(location.toString()).toEqual("37CDQ9429335584");
    });
});

let loc = new locObject.Location("37CDQ9429335584");

describe("Verify MGRS", () => {
    test('has value of 37CDQ9429335584', () => {
        expect(loc.mgrs).toEqual("37CDQ9429335584") ;
    }); 
});

describe("Verify point", () => {
    test("should throw an error if called without a number", () => {
        expect(() => loc.point("45")).toThrow(Error);
      });
});
