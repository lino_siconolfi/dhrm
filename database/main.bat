@ECHO off
setlocal enabledelayedexpansion

SET directory=%1

IF [%1] == [] (
	SET directory=.
)

ECHO This batch file will run two sql scripts, create_tables.sql and populate_tables.sql
ECHO.
ECHO Now dropping database if exist and creating database and tables
:: The create_tables script will create tables in the jhrmecddb database
:: The populate_tables script will populate the tables in the jhrmecddb database
mysql --default-character-set=utf8mb4 -h 127.0.0.1 -u root < %directory%/init_scripts/create_tables.sql
ECHO Now populating dataBase tables
mysql --default-character-set=utf8mb4 -h 127.0.0.1 -u root < %directory%/init_scripts/populate_tables.sql