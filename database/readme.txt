For Initialization:
----------------------------------------------------
Make sure <MySqlServerInstall>/bin is in your PATH.

Open a command line to the directory where this was unzipped.

Run the Following command:
mysqld --defaults-file="config.ini" --initialize-insecure --console

(Add steps to run the initialize_db.sql script on the database)
----------------------------------------------------



For development and debugging:
------------------------------
This comand will run the server:
mysqld --defaults-file="config.ini"
------------------------------

In MySql workbench, connect to the database.

Run init_scripts/create_tables to create the tables and data structure for the database.
Run init_scripts/populate_tables to populate the database with some test data.


Batch File to Create and Populate Tables:
----------------------------------------------------
Before running the batch file, make sure the <MySqlServerInstall> is set in your path.
For example, 'C:\Program Files\MySQL\MySQL Server 8.0\bin'

Open a command line to the directory where this was unzipped.
Run the Following command in Administrative Mode: main.bat
----------------------------------------------------


Using db-migrate to roll backwards and forwards:
-------------------------------------------------------
https://code.lmi.org/confluence/display/JHRM/Database+Migration+System+Proposal
The db-migrate command can be issued from the database directory to perform rollback and replays of database updates.

For new changes, call:
	db-migrate create mymigrationlabel --config devdb.json

  or, if you need the full path

  ../node_modules/db-migrate/bin/db-migrate create mymigrationlabel --config devdb.json

  Then modify the files in the sqls directory to incorporate the desired database changes for the specific revision.

To roll back:
	db-migrate down --config devdb.json

To roll forward:
	db-migrate up --config devdb.json
