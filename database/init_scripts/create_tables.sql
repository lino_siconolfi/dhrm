DROP DATABASE IF EXISTS `jhrmecddb`;
CREATE DATABASE IF NOT EXISTS `jhrmecddb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `jhrmecddb`;
-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: jhrmecddb
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE TABLE `jhrm` (
    `db_version` VARCHAR(128) NOT NULL DEFAULT '',
    PRIMARY KEY (`db_version`)
)  ENGINE=INNODB DEFAULT CHARSET=LATIN1;
INSERT INTO `jhrm` (`db_version`)
VALUES ('$Id: 882685f746d7b5b00ea3a211091de5d2c59c0314 $');
--
-- Table structure for table `jhrm_smartabase_mapping`
--
CREATE TABLE `jhrm_smartabase_mapping` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `jhrm_dod_id` VARCHAR(32) NOT NULL DEFAULT '',
    `sb_person_uid` VARCHAR(16) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`),
      UNIQUE KEY `id_UNIQUE` (`id`)
)  ENGINE=INNODB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `cehra_assessment`
--
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cehra_assessment` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `chem_enhanced_hra_id` VARCHAR(12) NOT NULL DEFAULT '',
    `type` VARCHAR(32) NOT NULL DEFAULT '',
    `meg` FLOAT NOT NULL,
    `meg_name` VARCHAR(16) NOT NULL DEFAULT '',
    `meg_units` VARCHAR(16) NOT NULL DEFAULT '',
    `meg_version` VARCHAR(32) NOT NULL DEFAULT '',
    `severity` VARCHAR(32) NOT NULL,
    `prob_degree_of_exp` VARCHAR(32) NOT NULL DEFAULT '',
    `prob_degree_of_exp_score` INT(11) NOT NULL,
    `prob_rep_of_data` VARCHAR(32) NOT NULL DEFAULT '',
    `prob_rep_of_data_score` INT(11) NOT NULL,
    `prob_duration_of_exp` VARCHAR(32) NOT NULL DEFAULT '',
    `prob_duration_of_exp_score` INT(11) NOT NULL,
    `prob_rate_of_exp` VARCHAR(32) NOT NULL DEFAULT '',
    `prob_rate_of_exp_score` INT(11) NOT NULL,
    `probability` VARCHAR(32) NOT NULL DEFAULT '',
    `probability_score` INT(11) NOT NULL,
    `risk_level` VARCHAR(32) NOT NULL,
    `confidence` VARCHAR(32) NOT NULL,
    `cas_number` varchar(32),
    `custom_exposure_duration` int(11) DEFAULT NULL,
    `custom_meg` tinyint(11) NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id_UNIQUE` (`id`),
    KEY `chem_enhanced_hra_id` (`chem_enhanced_hra_id`),
    CONSTRAINT `cehra_assessment_ibfk_1` FOREIGN KEY (`chem_enhanced_hra_id`)
        REFERENCES `chem_enhanced_hra` (`id`)
        ON DELETE NO ACTION ON UPDATE NO ACTION
)  ENGINE=INNODB DEFAULT CHARSET=LATIN1;
/*!40101 SET character_set_client = @saved_cs_client */;



--
-- Table structure for table `cehra_prescreen`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cehra_prescreen` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `chem_enhanced_hra_id` VARCHAR(12) NOT NULL DEFAULT '',
    `chemical_name` VARCHAR(32) NOT NULL DEFAULT '',
    `cas_number` VARCHAR(32) NOT NULL DEFAULT '',
    `valid_samples` TINYINT(32) NOT NULL,
    `peak_pepc` FLOAT NOT NULL,
    `peak_pepc_units` VARCHAR(16) NOT NULL DEFAULT '',
    `avg_pepc` FLOAT NOT NULL,
    `avg_pepc_units` VARCHAR(16) NOT NULL DEFAULT '',
    `ar_meg` FLOAT NOT NULL,
    `ar_meg_name` VARCHAR(16) NOT NULL DEFAULT '',
    `ar_meg_units` VARCHAR(16) NOT NULL DEFAULT '',
    `ar_meg_exceeded` TINYINT(32) NOT NULL,
    `ar_meg_version` VARCHAR(32) NOT NULL DEFAULT '',
    `cr_meg` FLOAT NOT NULL,
    `cr_meg_name` VARCHAR(16) NOT NULL DEFAULT '',
    `cr_meg_units` VARCHAR(16) NOT NULL DEFAULT '',
    `cr_meg_exceeded` TINYINT(32) NOT NULL,
    `cr_meg_version` VARCHAR(32) NOT NULL DEFAULT '',
    `cr_detection_frequency` INT(11) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id_UNIQUE` (`id`),
    KEY `chem_enhanced_hra_id` (`chem_enhanced_hra_id`),
    CONSTRAINT `cehra_prescreen_ibfk_1` FOREIGN KEY (`chem_enhanced_hra_id`)
        REFERENCES `chem_enhanced_hra` (`id`)
        ON DELETE NO ACTION ON UPDATE NO ACTION
)  ENGINE=INNODB DEFAULT CHARSET=LATIN1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `cehra_megs`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cehra_megs` (
    `chem_enhanced_hra_id` VARCHAR(12) NOT NULL DEFAULT '',
    `ID` INT(11) NOT NULL,
    `CHEMICAL_NAME` VARCHAR(120) DEFAULT NULL,
    `CASRN` VARCHAR(50) DEFAULT NULL,
    `MEG_VALUE` FLOAT DEFAULT NULL,
    `UNITS` VARCHAR(24) DEFAULT NULL,
    `BASIS` VARCHAR(30) DEFAULT NULL,
    `MEDIA` VARCHAR(12) DEFAULT NULL,
    `SEVERITY` VARCHAR(30) DEFAULT NULL,
    `TIMEFRAME` VARCHAR(24) DEFAULT NULL,
    `INTAKE_RATE` VARCHAR(24) DEFAULT NULL,
    `MEG_PARM` VARCHAR(30) DEFAULT NULL,
    `isCWA` TINYINT(4) DEFAULT '0',
    `version_number` VARCHAR(30) DEFAULT NULL,
    PRIMARY KEY (`chem_enhanced_hra_id` , `ID`),
    KEY `chem_enhanced_hra_id` (`chem_enhanced_hra_id`),
    CONSTRAINT `cehra_megs_ibfk_1` FOREIGN KEY (`chem_enhanced_hra_id`)
        REFERENCES `chem_enhanced_hra` (`id`)
        ON DELETE NO ACTION ON UPDATE NO ACTION
)  ENGINE=INNODB DEFAULT CHARSET=LATIN1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `cehra_report`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cehra_report` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `creation_time` DATETIME DEFAULT NOW(),
    `chem_enhanced_hra_id` VARCHAR(12) NOT NULL DEFAULT '',
    `recommended_mitigation` TEXT DEFAULT NULL,
    `potential_health_outcomes_acute` VARCHAR(256) DEFAULT NULL,
    `potential_health_outcomes_chronic` VARCHAR(256) DEFAULT NULL,
    `executive_summary` TEXT NOT NULL DEFAULT '',
    `force_health_protection_recommendations` TEXT NOT NULL DEFAULT '',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id_UNIQUE` (`id`),
    KEY `chem_enhanced_hra_id` (`chem_enhanced_hra_id`),
    CONSTRAINT `cehra_report_ibfk_1` FOREIGN KEY (`chem_enhanced_hra_id`)
        REFERENCES `chem_enhanced_hra` (`id`)
        ON DELETE NO ACTION ON UPDATE NO ACTION
)  ENGINE=INNODB DEFAULT CHARSET=LATIN1;
/*!40101 SET character_set_client = @saved_cs_client */;



--
-- Table structure for table `cehra_sample`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cehra_sample` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `id_doehrs_sample_id` VARCHAR(16) NOT NULL,
    `chem_enhanced_hra_id` VARCHAR(12) NOT NULL DEFAULT '',
    `sampling_point` VARCHAR(64) NOT NULL,
    `start_date_time` DATETIME NOT NULL,
    `exposure_notes` VARCHAR(256) NOT NULL,
    `sample_time` DECIMAL(6 , 2 ) NOT NULL,
    `approved_by_qa` TINYINT(4) DEFAULT NULL,
    `media` VARCHAR(16) DEFAULT NULL,
    `selected` TINYINT(4) DEFAULT 0,
    PRIMARY KEY (`id`),
    KEY `fk_cehra_sample_chem_enhanced_hra_idx` (`chem_enhanced_hra_id`),
    CONSTRAINT `cehra_sample_ibfk_1` FOREIGN KEY (`chem_enhanced_hra_id`)
        REFERENCES `chem_enhanced_hra` (`id`)
        ON DELETE NO ACTION ON UPDATE NO ACTION
)  ENGINE=INNODB DEFAULT CHARSET=LATIN1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `cehra_sample_results`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cehra_sample_results` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `id_doehrs_result_id` INT(11) UNSIGNED NOT NULL,
    `id_doehrs_sample_id` VARCHAR(16) NOT NULL,
    `chem_enhanced_hra_id` VARCHAR(12) NOT NULL DEFAULT '',
    `chemical_name` VARCHAR(128) NOT NULL,
    `cas_number` VARCHAR(16) NOT NULL,
    `concentration` FLOAT NOT NULL,
    `units` VARCHAR(16) NOT NULL,
    `analytical_method` VARCHAR(32) NOT NULL,
    `concentration_below_threshold` TINYINT(4) DEFAULT 0,
    `selected` TINYINT(4) DEFAULT 0,
    PRIMARY KEY (`id`),
    KEY `fk_cehra_sample_results_chem_enhanced_hra_idx` (`chem_enhanced_hra_id`),
    CONSTRAINT `cehra_sample_results_ibfk_1` FOREIGN KEY (`chem_enhanced_hra_id`)
        REFERENCES `chem_enhanced_hra` (`id`)
        ON DELETE NO ACTION ON UPDATE NO ACTION
)  ENGINE=INNODB DEFAULT CHARSET=LATIN1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `chem_enhanced_hra`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chem_enhanced_hra` (
    `id` VARCHAR(12) NOT NULL,
    `hra_objective` VARCHAR(128) NOT NULL,
    `location` VARCHAR(128) NOT NULL,
    `latitude` FLOAT DEFAULT NULL,
    `longitude` FLOAT DEFAULT NULL,
    `sap_file_name` VARCHAR(128) DEFAULT NULL,
    `id_doehrs_pathway_id` INT(11) NOT NULL,
    `exposure_pathway_name` VARCHAR(126) DEFAULT NULL,
    `chemical_name` VARCHAR(128) DEFAULT NULL,
    `exposure_route` VARCHAR(64) DEFAULT NULL,
    `source` VARCHAR(64) DEFAULT NULL,
    `population_at_risk` VARCHAR(64) DEFAULT NULL,
    `media` VARCHAR(64) DEFAULT NULL,
    `created_by` VARCHAR(45) NOT NULL,
    `created_on` DATETIME NOT NULL,
    `last_edited_by` VARCHAR(45) DEFAULT NULL,
    `lasted_edited_on` DATETIME DEFAULT NULL,
    `approved_by` VARCHAR(45) DEFAULT NULL,
    `priority` VARCHAR(16) DEFAULT NULL,
    `status` VARCHAR(16) DEFAULT NULL,
    PRIMARY KEY (`id`)
)  ENGINE=INNODB DEFAULT CHARSET=LATIN1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table 'chem_enhanced_hra_qa_tool'
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
create table `chem_enhanced_hra_qa_tool` (
	`id` INT NOT NULL AUTO_INCREMENT,
    `hra_id` VARCHAR(12) NOT NULL,
    `hra_objective_comments` VARCHAR(256) DEFAULT NULL,
    `hra_objective_approve` BOOLEAN DEFAULT FALSE,
    `hra_objective_edit` BOOLEAN DEFAULT FALSE,
    `hazard_probability_comments` VARCHAR(256) DEFAULT NULL,
    `hazard_probability_approve` BOOLEAN DEFAULT FALSE,
    `hazard_probability_edit` BOOLEAN DEFAULT FALSE,
    `executive_summary_comments` VARCHAR(256) DEFAULT NULL,
    `executive_summary_approve` BOOLEAN DEFAULT FALSE,
    `executive_summary_edit` BOOLEAN DEFAULT FALSE,
    `fhp_recommendations_comments` VARCHAR(256) DEFAULT NULL,
    `fhp_recommendations_approve` BOOLEAN DEFAULT FALSE,
    `fhp_recommendations_edit` BOOLEAN DEFAULT FALSE,
    `samples_included_in_hra_comments` VARCHAR(256) DEFAULT NULL,
    `samples_included_in_hra_approve` BOOLEAN DEFAULT FALSE,
	`samples_included_in_hra_edit` BOOLEAN DEFAULT FALSE,
	PRIMARY KEY (`id`),
        FOREIGN KEY (`hra_id`)
        REFERENCES `chem_enhanced_hra` (`id`)
        ON DELETE NO ACTION ON UPDATE NO ACTION
)  ENGINE=INNODB DEFAULT CHARSET=LATIN1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `id_doehrs_ih_locations`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `id_doehrs_ih_locations` (
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `location_name` VARCHAR(256) DEFAULT NULL,
    `location_type` VARCHAR(64) DEFAULT NULL,
    `country` VARCHAR(128) DEFAULT NULL,
    `comments` VARCHAR(256) DEFAULT NULL,
    `start_stop_date` DATE DEFAULT NULL,
    PRIMARY KEY (`id`)
)  ENGINE=INNODB AUTO_INCREMENT=2046 DEFAULT CHARSET=LATIN1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `id_doehrs_pathway`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `id_doehrs_pathway` (
    `id` INT(11) UNSIGNED NOT NULL,
    `location` VARCHAR(128) NOT NULL DEFAULT '',
    `name` VARCHAR(255) NOT NULL DEFAULT '',
    `source` VARCHAR(128) NOT NULL DEFAULT '',
    `environmental_media` VARCHAR(128) NOT NULL DEFAULT '',
    `health_threat` TEXT NOT NULL,
    `exposure_route` VARCHAR(128) NOT NULL DEFAULT '',
    `affected_personnel` INT(11) NOT NULL,
    `population_at_risk_descr` VARCHAR(256) NOT NULL DEFAULT '',
    `priority` VARCHAR(128) NOT NULL DEFAULT '',
    `start_date` DATE NOT NULL,
    `stop_date` DATE DEFAULT NULL,
    PRIMARY KEY (`id`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `id_doehrs_result`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `id_doehrs_result` (
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `id_doehrs_sample_id` VARCHAR(16) NOT NULL DEFAULT '',
    `parameter` VARCHAR(128) NOT NULL DEFAULT '',
    `concentration` VARCHAR(16) NOT NULL DEFAULT '',
    `units` VARCHAR(128) NOT NULL DEFAULT '',
    `cas_number` VARCHAR(128) NOT NULL DEFAULT '',
    `class` VARCHAR(128) NOT NULL DEFAULT '',
    `concentration_below_threshold` TINYINT(4) DEFAULT 0,
    `analytical_method` VARCHAR(32) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `id_doehrs_sample_id` (`id_doehrs_sample_id`),
    CONSTRAINT `id_doehrs_result_ibfk_1` FOREIGN KEY (`id_doehrs_sample_id`)
        REFERENCES `id_doehrs_sample` (`id`)
        ON DELETE CASCADE ON UPDATE CASCADE
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `id_doehrs_sample`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `id_doehrs_sample` (
    `id` VARCHAR(16) NOT NULL DEFAULT '',
    `id_doehrs_pathway_id` INT(11) UNSIGNED DEFAULT NULL,
    `local_sample_id` VARCHAR(128) NOT NULL DEFAULT '',
    `location` VARCHAR(128) NOT NULL DEFAULT '',
    `sampling_point` VARCHAR(128) NOT NULL DEFAULT '',
    `start_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `exposure_notes` TEXT,
    `lab_import_status` VARCHAR(128) NOT NULL DEFAULT '',
    `status` VARCHAR(128) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`),
    KEY `id_doehrs_pathway_id` (`id_doehrs_pathway_id`),
    CONSTRAINT `id_doehrs_sample_ibfk_1` FOREIGN KEY (`id_doehrs_pathway_id`)
        REFERENCES `id_doehrs_pathway` (`id`)
        ON DELETE CASCADE ON UPDATE CASCADE
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `id_doehrs_sample_air`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `id_doehrs_sample_air` (
    `id_doehrs_sample_id` VARCHAR(16) NOT NULL DEFAULT '',
    `sample_time` VARCHAR(128) DEFAULT NULL,
    `invalid_sample` VARCHAR(128) DEFAULT NULL,
    `canister_serial_number` VARCHAR(128) NOT NULL DEFAULT '',
    PRIMARY KEY (`id_doehrs_sample_id`),
    CONSTRAINT `id_doehrs_sample_air_ibfk_1` FOREIGN KEY (`id_doehrs_sample_id`)
        REFERENCES `id_doehrs_sample` (`id`)
        ON DELETE CASCADE ON UPDATE CASCADE
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `id_doehrs_sample_soil`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `id_doehrs_sample_soil` (
    `id_doehrs_sample_id` VARCHAR(16) NOT NULL DEFAULT '',
    `collection_type` VARCHAR(128) DEFAULT NULL,
    PRIMARY KEY (`id_doehrs_sample_id`),
    CONSTRAINT `id_doehrs_sample_soil_ibfk_1` FOREIGN KEY (`id_doehrs_sample_id`)
        REFERENCES `id_doehrs_sample` (`id`)
        ON DELETE CASCADE ON UPDATE CASCADE
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `id_doehrs_sample_water`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `id_doehrs_sample_water` (
    `id_doehrs_sample_id` VARCHAR(16) NOT NULL DEFAULT '',
    `is_treated_water` TINYINT(1) NOT NULL,
    `water_type` VARCHAR(128) NOT NULL DEFAULT '',
    `water_current_use` VARCHAR(128) NOT NULL DEFAULT '',
    `water_intended_use` VARCHAR(128) DEFAULT NULL,
    PRIMARY KEY (`id_doehrs_sample_id`),
    CONSTRAINT `id_doehrs_sample_water_ibfk_1` FOREIGN KEY (`id_doehrs_sample_id`)
        REFERENCES `id_doehrs_sample` (`id`)
        ON DELETE CASCADE ON UPDATE CASCADE
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `id_hapsite_result`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hapsite_result` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `hapsite_sample_id` INT UNSIGNED NOT NULL,
    `time_received` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `parameter` VARCHAR(128) DEFAULT NULL,
    `concentration` FLOAT DEFAULT NULL,
    `units` VARCHAR(16) DEFAULT NULL,
    `purity` INT DEFAULT NULL,
    `start_time` DATETIME DEFAULT NULL,
    `end_time` DATETIME DEFAULT NULL,
    `cas_number` VARCHAR(16) DEFAULT NULL,
    `alarm_level` INT DEFAULT NULL,
    `fit` DOUBLE DEFAULT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`hapsite_sample_id`)
        REFERENCES `hapsite_sample` (`id`)
        ON DELETE CASCADE ON UPDATE CASCADE
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;

--
-- Table structure for table `id_hapsite_sample`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hapsite_sample` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `hapsite_sensor_id` INT NOT NULL,
    `result_id` INT NOT NULL,
	`latitude` FLOAT DEFAULT 0.0,
    `longitude` FLOAT DEFAULT 0.0,
    `mgrs` varchar(32) DEFAULT NULL,
    `time_received` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `method_type` VARCHAR(32) NOT NULL DEFAULT '',
    `method_state` VARCHAR(32) NOT NULL DEFAULT '',
    `method_name` VARCHAR(255) NOT NULL,
    `scan_mode` INT NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`hapsite_sensor_id`)
        REFERENCES `hapsite_sensors` (`id`)
        ON DELETE CASCADE
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;

CREATE TABLE `hapsite_sensor_status` (
    `id` INT AUTO_INCREMENT NOT NULL,
    `hapsite_sensor_id` INT NOT NULL,
    `time_received` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `net_status` VARCHAR(32) DEFAULT 'Online',
    `latitude` FLOAT DEFAULT 0.0,
    `longitude` FLOAT DEFAULT 0.0,
    `mgrs` varchar(32) DEFAULT NULL,
    `hae` FLOAT DEFAULT 0.0,
    `battery_state` INTEGER DEFAULT 0,
    `voltage` FLOAT DEFAULT 0.0,
    `system_state` VARCHAR(64),
    `charge` FLOAT DEFAULT 0.0,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`hapsite_sensor_id`)
        REFERENCES `hapsite_sensors` (`id`)
        ON DELETE CASCADE
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;

CREATE TABLE `hapsite_sensors` (
    `id` INT AUTO_INCREMENT NOT NULL,
    `sensor_type` VARCHAR(16) DEFAULT 'HAPSITE',
    `uci` VARCHAR(128) NOT NULL,
    `message_id` VARCHAR(128) NOT NULL,
    `sender` VARCHAR(128) NOT NULL,
    `time_received` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `mil_identity` VARCHAR(255) NOT NULL DEFAULT 'EFOPEB------',
    `host_name` VARCHAR(32) NOT NULL DEFAULT 'Hapsite_Host',
    `serial_number` VARCHAR(32) NOT NULL DEFAULT 'Not Received',
    `software_version` VARCHAR(16) NOT NULL DEFAULT '1.0',
    `gc_version` VARCHAR(16),
    `ms_version` VARCHAR(16),
    `probe_version` VARCHAR(16),
    `headspace_version` VARCHAR(16),
    `situ_probe_version` VARCHAR(16),
	`ext_software_version` VARCHAR(32),
	`model` VARCHAR(32),
	`dsc_version` VARCHAR(16),
    PRIMARY KEY (`id`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;

CREATE TABLE `jcad_chemical_readings` (
    `id` INT AUTO_INCREMENT NOT NULL,
    `jcad_sensor_id` INT NOT NULL,
    `time_received` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`latitude` FLOAT DEFAULT 0.0,
    `longitude` FLOAT DEFAULT 0.0,
    `mgrs` varchar(32) DEFAULT NULL,
    `event_id` BIGINT NOT NULL,
    `event_type` VARCHAR(16) NOT NULL DEFAULT 'OTHER',
    `material_name` VARCHAR(255) DEFAULT 'NONE',
    `material_class` VARCHAR(32) DEFAULT 'UNSPECIFIED',
    `cas_number` VARCHAR(32) DEFAULT NULL,
    `harmful` BOOLEAN DEFAULT FALSE,
    `bars` SMALLINT DEFAULT 0,
    `peak_bars` SMALLINT DEFAULT 0,
    `concentration` FLOAT DEFAULT 0,
    `units` VARCHAR(8) NOT NULL DEFAULT 'kg/m3',
    `positive_fg_volts` SMALLINT DEFAULT 0,
    `negative_fg_volts` SMALLINT DEFAULT 0,
    `pos_noise_g` SMALLINT DEFAULT 0,
    `neg_noise_h` SMALLINT DEFAULT 0,
    `posMobCalibFac` SMALLINT DEFAULT 0,
    `negMobCalibFac` SMALLINT DEFAULT 0,
    `pos_temperature` SMALLINT DEFAULT 0,
    `neg_temperature` SMALLINT DEFAULT 0,
    `positive_rip_amplitude` SMALLINT DEFAULT 0,
    `positive_rip_mobility` SMALLINT DEFAULT 0,
    `positive_rip_noise` SMALLINT DEFAULT 0,
    `negative_rip_amplitude` SMALLINT DEFAULT 0,
    `negative_rip_mobility` SMALLINT DEFAULT 0,
    `negative_rip_noise` SMALLINT DEFAULT 0,
    `pos_rip_chk_amp1` SMALLINT DEFAULT 0,
    `pos_rip_chk_mob1` SMALLINT DEFAULT 0,
    `pos_rip_chk_amp2` SMALLINT DEFAULT 0,
    `pos_rip_chk_mob2` SMALLINT DEFAULT 0,
    `pos_rip_chk_amp3` SMALLINT DEFAULT 0,
    `pos_rip_chk_mob3` SMALLINT DEFAULT 0,
    `neg_rip_chk_amp1` SMALLINT DEFAULT 0,
    `neg_rip_chk_mob1` SMALLINT DEFAULT 0,
    `neg_rip_chk_amp2` SMALLINT DEFAULT 0,
    `neg_rip_chk_mob2` SMALLINT DEFAULT 0,
    `neg_rip_chk_amp3` SMALLINT DEFAULT 0,
    `neg_rip_chk_mob3` SMALLINT DEFAULT 0,
    `positive_peak1_amplitude` SMALLINT DEFAULT 0,
    `positive_peak1_mobility` SMALLINT DEFAULT 0,
    `positive_peak1_noise` SMALLINT DEFAULT 0,
    `positive_peak2_amplitude` SMALLINT DEFAULT 0,
    `positive_peak2_mobility` SMALLINT DEFAULT 0,
    `positive_peak2_noise` SMALLINT DEFAULT 0,
    `positive_peak3_amplitude` SMALLINT DEFAULT 0,
    `positive_peak3_mobility` SMALLINT DEFAULT 0,
    `positive_peak3_noise` SMALLINT DEFAULT 0,
    `negative_peak1_amplitude` SMALLINT DEFAULT 0,
    `negative_peak1_mobility` SMALLINT DEFAULT 0,
    `negative_peak1_noise` SMALLINT DEFAULT 0,
    `negative_peak2_amplitude` SMALLINT DEFAULT 0,
    `negative_peak2_mobility` SMALLINT DEFAULT 0,
    `negative_peak2_noise` SMALLINT DEFAULT 0,
    `negative_peak3_amplitude` SMALLINT DEFAULT 0,
    `negative_peak3_mobility` SMALLINT DEFAULT 0,
    `negative_peak3_noise` SMALLINT DEFAULT 0,
    `negative_spectrum` TEXT DEFAULT NULL,
    `positive_spectrum` TEXT DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `jcad_sensor_id` (`jcad_sensor_id`),
    CONSTRAINT `jcad_chemical_readings_ibfk_1` FOREIGN KEY (`jcad_sensor_id`)
        REFERENCES `jcad_sensors` (`id`)
        ON DELETE CASCADE
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;


CREATE TABLE `jcad_sensor_status` (
    `id` INT AUTO_INCREMENT NOT NULL,
    `jcad_sensor_id` INT NOT NULL,
    `time_received` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `net_status` VARCHAR(32) DEFAULT 'Online',
    `latitude` FLOAT DEFAULT 0.0,
    `longitude` FLOAT DEFAULT 0.0,
    `mgrs` varchar(32) DEFAULT NULL,
    `altitude` FLOAT DEFAULT 0.0,
    `detection_mode` VARCHAR(16) NOT NULL DEFAULT 'Confidence Check',
    `system_alert_status` VARCHAR(16) NOT NULL DEFAULT 'NONE',
    `operating_mode` VARCHAR(16) NOT NULL DEFAULT 'WAIT',
    `clock` DATETIME DEFAULT NULL,
    `runtime_hrs` SMALLINT DEFAULT 0,
    `runtime_min` SMALLINT DEFAULT 0,
    `sieve_life_remaining` SMALLINT DEFAULT 0,
    `atmospheric_pressure` SMALLINT DEFAULT 0,
    `sensor_temperature` SMALLINT DEFAULT 0,
    `calibration_mode` BOOLEAN DEFAULT FALSE,
    `sieve_low_warning` BOOLEAN DEFAULT FALSE,
    `battery_low_warning` BOOLEAN DEFAULT FALSE,
    `change_battery_warning` BOOLEAN DEFAULT FALSE,
    `health_check_fault` BOOLEAN DEFAULT FALSE,
    `change_sieve_pack` BOOLEAN DEFAULT FALSE,
    `major_fault` BOOLEAN DEFAULT FALSE,
    `change_battery_fault` BOOLEAN DEFAULT FALSE,
    `eeprom_checksum_fault` BOOLEAN DEFAULT FALSE,
    `datalog_fault` BOOLEAN DEFAULT FALSE,
    `clock_battery_fault` BOOLEAN DEFAULT FALSE,
    `persistent_health_fault` BOOLEAN DEFAULT FALSE,
    `vibration_detected` BOOLEAN DEFAULT FALSE,
    `saturation_protection` BOOLEAN DEFAULT FALSE,
    `power_status` SMALLINT DEFAULT NULL,
    `message_1` VARCHAR(128) DEFAULT NULL,
    `message_2` VARCHAR(128) DEFAULT NULL,
    `message_3` VARCHAR(128) DEFAULT NULL,
    `message_4` VARCHAR(128) DEFAULT NULL,
    `message_5` VARCHAR(128) DEFAULT NULL,
    `message_6` VARCHAR(128) DEFAULT NULL,
    `message_7` VARCHAR(128) DEFAULT NULL,
    `message_8` VARCHAR(128) DEFAULT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`jcad_sensor_id`)
        REFERENCES `jcad_sensors` (`id`)
        ON DELETE CASCADE
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;


CREATE TABLE `jcad_sensors` (
    `id` INT AUTO_INCREMENT NOT NULL,
    `sensor_type` VARCHAR(16) DEFAULT 'M4A1 JCAD',
    `uci` VARCHAR(128) NOT NULL,
    `message_id` VARCHAR(128) NOT NULL,
    `sender` VARCHAR(128) NOT NULL,
    `time_received` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `mil_identity` VARCHAR(255) NOT NULL DEFAULT 'EFOPEB------',
    `drawing_number` SMALLINT NOT NULL DEFAULT 0,
    `system_id` BIGINT DEFAULT 0,
    `issue_number` SMALLINT DEFAULT 0,
    `hw_version_string` VARCHAR(32) DEFAULT '',
    `loader_software_drawing` VARCHAR(32) DEFAULT '',
    `app_sw_drawing` VARCHAR(32) DEFAULT '',
    `serial_number` VARCHAR(32) DEFAULT '',
    `hardware_ident` VARCHAR(32) DEFAULT '',
    `boot_loader` VARCHAR(32) DEFAULT '',
    `model` VARCHAR(32) DEFAULT '',
    `manufacturer` VARCHAR(128) DEFAULT 'Smith\'s Detection',
    PRIMARY KEY (`id`),
    KEY (`sensor_type`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;

--
-- Table structure for table `meg_critical_effect`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_critical_effect` (
    `id` INT(11) NOT NULL,
    `chemical_name` VARCHAR(128) DEFAULT NULL,
    `casrn` VARCHAR(32) DEFAULT NULL,
    `meg_value` FLOAT DEFAULT NULL,
    `units` VARCHAR(16) DEFAULT NULL,
    `basis` VARCHAR(32) DEFAULT NULL,
    `media` VARCHAR(16) DEFAULT NULL,
    `severity` VARCHAR(32) DEFAULT NULL,
    `timeframe` VARCHAR(16) DEFAULT NULL,
    `intake_rate` VARCHAR(16) DEFAULT NULL,
    `meg_parm` VARCHAR(32) DEFAULT NULL,
    `available` VARCHAR(32) DEFAULT NULL,
    `basic_lookup` VARCHAR(16) DEFAULT NULL,
    `health_effects_basis` VARCHAR(1024) DEFAULT NULL,
    PRIMARY KEY (`id`)
)  ENGINE=INNODB DEFAULT CHARSET=LATIN1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `rate_of_exposure_lookup`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rate_of_exposure_lookup` (
    `id` INT(11) NOT NULL,
    `media` VARCHAR(16) DEFAULT NULL,
    `name` VARCHAR(128) DEFAULT NULL,
    `score` INT(11) DEFAULT NULL,
    PRIMARY KEY (`id`)
)  ENGINE=INNODB DEFAULT CHARSET=LATIN1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `rep_of_data_lookup`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rep_of_data_lookup` (
    `id` INT(11) NOT NULL,
    `name` VARCHAR(128) DEFAULT NULL,
    `score` INT(11) DEFAULT NULL,
    PRIMARY KEY (`id`)
)  ENGINE=INNODB DEFAULT CHARSET=LATIN1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `sessions`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `sid` varchar(255) NOT NULL,
  `sess` json NOT NULL,
  `expired` datetime NOT NULL,
  PRIMARY KEY (`sid`),
  KEY `sessions_expired_index` (`expired`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `tg230_master_meg`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tg230_master_meg` (
    `ID` INT(11) NOT NULL,
    `CHEMICAL_NAME` VARCHAR(120) DEFAULT NULL,
    `CASRN` VARCHAR(50) DEFAULT NULL,
    `MEG_VALUE` FLOAT DEFAULT NULL,
    `UNITS` VARCHAR(24) DEFAULT NULL,
    `BASIS` VARCHAR(30) DEFAULT NULL,
    `MEDIA` VARCHAR(12) DEFAULT NULL,
    `SEVERITY` VARCHAR(30) DEFAULT NULL,
    `TIMEFRAME` VARCHAR(24) DEFAULT NULL,
    `INTAKE_RATE` VARCHAR(24) DEFAULT NULL,
    `MEG_PARM` VARCHAR(30) DEFAULT NULL,
    `isCWA` TINYINT(4) DEFAULT '0',
    `version_number` VARCHAR(30) DEFAULT NULL,
    PRIMARY KEY (`ID`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8;
/*!40101 SET character_set_client = @saved_cs_client */;




CREATE TABLE `chem_rapid_hra` (
    `id` VARCHAR(32) NOT NULL,
    `hra_objective` VARCHAR(128) NOT NULL DEFAULT '',
    `selected_pathway` BOOLEAN DEFAULT FALSE,
    `location` VARCHAR(128) NOT NULL,
    `sap_file_name` VARCHAR(128) DEFAULT NULL,
    `casrn` VARCHAR(32) DEFAULT NULL,
    `created_by` VARCHAR(64) NOT NULL,
    `created_on` DATETIME NOT NULL,
    `approved_by` VARCHAR(64) NULL,
    `last_edited_by` VARCHAR(64) DEFAULT NULL,
    `last_edited_on` DATETIME DEFAULT NULL,
    `uploaded_to_doehrs` BOOLEAN DEFAULT FALSE,
    `qa_approved` BOOLEAN DEFAULT FALSE,
    `status` VARCHAR(16) DEFAULT NULL,
    `sensor_type` VARCHAR(32) DEFAULT NULL,
    `media_type` VARCHAR(8) DEFAULT NULL,
    `population_at_risk` VARCHAR(256) NOT NULL DEFAULT '',
    `individuals_at_risk` INT(8) NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id_unique` (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `id_DRI_pathway`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rhra_pathway` (
  `id` varchar(32) NOT NULL,
  `chem_rapid_hra_id` varchar(12) NOT NULL DEFAULT '',
  `sensor_array_id` varchar(12) DEFAULT '',
  `location` varchar(128) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL,
  `threat_source` varchar(128) NOT NULL,
  `health_hazard` text NOT NULL,
  `exposure_point_area` varchar(64) NOT NULL,
  `exposure_medium` varchar(128) NOT NULL DEFAULT '',
  `exposure_route` varchar(128) NOT NULL DEFAULT '',
  `exposure_duration_of_concern` varchar(128) NOT NULL DEFAULT '',
  `exposed_population` varchar(256) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;


CREATE TABLE `rhra_megs` (
    `chem_rapid_hra_id` VARCHAR(12) NOT NULL DEFAULT '',
    `ID` INT(11) NOT NULL,
    `CHEMICAL_NAME` VARCHAR(120) DEFAULT NULL,
    `CASRN` VARCHAR(50) DEFAULT NULL,
    `MEG_VALUE` FLOAT DEFAULT NULL,
    `UNITS` VARCHAR(24) DEFAULT NULL,
    `BASIS` VARCHAR(30) DEFAULT NULL,
    `MEDIA` VARCHAR(12) DEFAULT NULL,
    `SEVERITY` VARCHAR(30) DEFAULT NULL,
    `TIMEFRAME` VARCHAR(24) DEFAULT NULL,
    `INTAKE_RATE` VARCHAR(24) DEFAULT NULL,
    `MEG_PARM` VARCHAR(30) DEFAULT NULL,
    `isCWA` TINYINT(4) DEFAULT '0',
    `version_number` VARCHAR(30) DEFAULT NULL,
    PRIMARY KEY (`chem_rapid_hra_id` , `ID`),
    KEY `chem_rapid_hra_id` (`chem_rapid_hra_id`),
    CONSTRAINT `rhra_megs_ibfk_1` FOREIGN KEY (`chem_rapid_hra_id`)
        REFERENCES `chem_rapid_hra` (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



CREATE TABLE `rhra_sample` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `chem_rapid_hra_id` VARCHAR(32) NOT NULL,
    `unique_sensor_id` VARCHAR(32) NOT NULL,
    `unique_sample_id` VARCHAR(32) NOT NULL,
    `mgrs` varchar(32) DEFAULT NULL,
    `sensor_type` VARCHAR(32) NOT NULL,
    `sampling_point` VARCHAR(128) NOT NULL,
    `media` VARCHAR(32) NULL DEFAULT '',
    `start_date_time` DATETIME NOT NULL,
    `end_date_time` DATETIME NOT NULL,
    `sample_time` DECIMAL(6 , 2 ) NOT NULL,
    `exposure_pathway_name` VARCHAR(128) DEFAULT NULL,
    `selected` TINYINT(4) DEFAULT 0,
    PRIMARY KEY (`id`),
    KEY `fk_rhra_sample_idx` (`chem_rapid_hra_id`),
    KEY `fk_sensor_type` (`sensor_type`),
    CONSTRAINT `rhra_sample_ibfk_1` FOREIGN KEY (`chem_rapid_hra_id`)
        REFERENCES `chem_rapid_hra` (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `rhra_sample_results` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `chem_rapid_hra_id` VARCHAR(12) NOT NULL DEFAULT '',
    `unique_sample_id` VARCHAR(32) NOT NULL,
    `unique_result_id` VARCHAR(32) NOT NULL,
    `chemical_name` VARCHAR(128) NOT NULL,
    `cas_number` VARCHAR(16) NOT NULL,
    `concentration` FLOAT NOT NULL,
    `units` VARCHAR(16) NOT NULL,
    `analytical_method` VARCHAR(32) NOT NULL,
    `concentration_below_threshold` TINYINT(4) DEFAULT 0,
    `selected` TINYINT(4) DEFAULT 0,
    PRIMARY KEY (`id`),
    KEY `fk_rhra_sample_results_chem_rapid_hra_idx` (`chem_rapid_hra_id`),
    CONSTRAINT `rhra_sample_results_ibfk_1` FOREIGN KEY (`chem_rapid_hra_id`)
        REFERENCES `chem_rapid_hra` (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



CREATE TABLE `rhra_prescreen` (
    `id` int(32) unsigned NOT NULL AUTO_INCREMENT,
    `chem_rapid_hra_id` VARCHAR(32) NOT NULL,
    `chemical_name` VARCHAR(32) NOT NULL DEFAULT '',
    `cas_number` VARCHAR(32) NOT NULL DEFAULT '',
    `valid_samples` TINYINT(32) NOT NULL,
    `peak_pepc` DOUBLE NOT NULL,
    `peak_pepc_units` VARCHAR(16) NOT NULL DEFAULT '',
    `avg_pepc` DOUBLE NOT NULL,
    `avg_pepc_units` VARCHAR(16) NOT NULL DEFAULT '',
    `ar_meg` DOUBLE NOT NULL,
    `ar_meg_name` VARCHAR(16) NOT NULL DEFAULT '',
    `ar_meg_units` VARCHAR(16) NOT NULL DEFAULT '',
    `ar_meg_exceeded` BOOLEAN NOT NULL,
    `ar_meg_version` VARCHAR(32) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id_unique` (`id`),
    KEY `chem_rapid_hra_id` (`chem_rapid_hra_id`),
    CONSTRAINT `rhra_assessment_ibfk_1` FOREIGN KEY (`chem_rapid_hra_id`)
        REFERENCES `chem_rapid_hra` (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



CREATE TABLE `rhra_assessment` (
  `id` int(32) unsigned NOT NULL AUTO_INCREMENT,
  `chem_rapid_hra_id` varchar(32) NOT NULL,
  `type` varchar(32) NOT NULL DEFAULT '',
  `chemical_name` varchar(32) NOT NULL DEFAULT '',
  `cas_number` varchar(32) NOT NULL DEFAULT '',
  `meg` float NOT NULL,
  `meg_name` varchar(16) NOT NULL DEFAULT '',
  `meg_units` varchar(16) NOT NULL DEFAULT '',
  `meg_version` varchar(32) NOT NULL DEFAULT '',
  `severity` varchar(32) NOT NULL,
  `prob_degree_of_exp` varchar(32) NOT NULL DEFAULT '',
  `prob_degree_of_exp_score` int(11) NOT NULL,
  `prob_rep_of_data` varchar(32) NOT NULL DEFAULT '',
  `prob_rep_of_data_score` int(11) NOT NULL,
  `prob_duration_of_exp` varchar(32) NOT NULL DEFAULT '',
  `prob_duration_of_exp_score` int(11) NOT NULL,
  `prob_rate_of_exp` varchar(32) NOT NULL DEFAULT '',
  `prob_rate_of_exp_score` int(11) NOT NULL,
  `probability` varchar(32) NOT NULL DEFAULT '',
  `probability_score` int(11) NOT NULL,
  `risk_level` varchar(32) NOT NULL,
  `confidence` varchar(32) NOT NULL,
  `custom_exposure_duration` int(11) DEFAULT NULL,
  `custom_meg` tinyint(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `chem_rapid_hra_id` (`chem_rapid_hra_id`),
  CONSTRAINT `rhra_assessment_summary_ibfk_1` FOREIGN KEY (`chem_rapid_hra_id`) REFERENCES `chem_rapid_hra` (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



CREATE TABLE `rhra_report` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `chem_rapid_hra_id` VARCHAR(32) NOT NULL,
    `potential_health_outcomes` VARCHAR(256) DEFAULT NULL,
    `additional_information` TEXT DEFAULT NULL,
    `recommended_mitigation` TEXT DEFAULT NULL,
    `communicated_to_name` VARCHAR(128) DEFAULT NULL,
    `communicated_to_position` VARCHAR(128) DEFAULT NULL,
    `communicated_to_contact` VARCHAR(128) DEFAULT NULL,
    `communicated_date` DATE DEFAULT NULL,
    `communicated_by` VARCHAR(128) DEFAULT NULL,
    `communicated_how` VARCHAR(128) DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id_UNIQUE` (`id`),
    KEY `chem_rapid_hra_id` (`chem_rapid_hra_id`),
    CONSTRAINT `rhra_report_ibfk_1` FOREIGN KEY (`chem_rapid_hra_id`)
        REFERENCES `chem_rapid_hra` (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `chemical_info` (
    `cas_number` VARCHAR(32) NOT NULL,
    `formula` VARCHAR(128) DEFAULT NULL,
    `molecular_weight` DOUBLE DEFAULT NULL,
    `other_names` TEXT DEFAULT NULL,
    PRIMARY KEY (`cas_number`)
);

--
-- One record per MultiRAE sensor
--
CREATE TABLE `multirae_sensors` (
  `id` INTEGER AUTO_INCREMENT NOT NULL,
  `uci` VARCHAR(128) NOT NULL,
  `sender` VARCHAR(128) NOT NULL,
  `time_received` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `mil_identity` VARCHAR(255) NOT NULL DEFAULT "EFOPEB------" ,
  `message_id` VARCHAR(128) NOT NULL,
  `sensor_sn` VARCHAR(16) NOT NULL,
  `model_number` VARCHAR(32) NOT NULL,
  `sensor_type` VARCHAR(16) DEFAULT "MULTIRAE",
  `unit_firmware_version` VARCHAR(8) NULL,
  `running_mode` VARCHAR(32) NULL,
  `datalog_mode` VARCHAR(16) NULL,
  `diagnostic_mode` VARCHAR(8) NULL,
  `stop_reason` VARCHAR(64) NULL,
  `site_id` VARCHAR(16) NULL,
  `user_id` VARCHAR(16) NULL,
  `sample_period_length` INT NOT NULL DEFAULT 0,
  `number_of_records` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY (`sensor_type`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `multirae_sensor_status` (
  `id` INTEGER AUTO_INCREMENT NOT NULL,
  `sensor_id` INTEGER NOT NULL,
  `time_received` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `net_status` VARCHAR(32) DEFAULT "Online" ,
  `latitude` FLOAT DEFAULT 0.0,
  `longitude` FLOAT DEFAULT 0.0,
  `mgrs` varchar(32) DEFAULT NULL,
  `altitude` float DEFAULT 0.0,
  `power_normal` BOOLEAN DEFAULT 1,
  `battery_low` BOOLEAN DEFAULT 0,
  `pump_stall` BOOLEAN DEFAULT 0,
  `memory_full` BOOLEAN DEFAULT 0,
  `sensor_alarm` BOOLEAN DEFAULT 0,
  `unit_failure` BOOLEAN DEFAULT 0,
  `alarm_mode_latch` BOOLEAN DEFAULT 0,
	PRIMARY KEY (`id`),
  FOREIGN KEY (`sensor_id`) REFERENCES `multirae_sensors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `multirae_sensing_component` (
  `id` INTEGER AUTO_INCREMENT NOT NULL,
  `time_received` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sensing_component_sn` VARCHAR(16) NOT NULL DEFAULT '000000',
  `sensor_id` INTEGER NOT NULL,
  `sensor_chemical` VARCHAR(16) NOT NULL,
  `sensor_type` VARCHAR(16) NOT NULL,
  `measure_units` VARCHAR(8) NOT NULL,
  `measure_type` VARCHAR(24) NOT NULL DEFAULT 'UNKNOWN',
  `span` INT NOT NULL DEFAULT 0,
  `low_alarm` DOUBLE NULL,
  `high_alarm` DOUBLE NULL,
  `over_alarm` DOUBLE NULL,
  `stel_alarm` DOUBLE NULL DEFAULT 0.0,
  `twa_alarm` DOUBLE NULL DEFAULT 0.0,
  `over_range` BOOLEAN DEFAULT 0,
  `max_saturated` BOOLEAN DEFAULT 0,
  `failed` BOOLEAN DEFAULT 0,
  `high_alarm_active` BOOLEAN DEFAULT 0,
  `low_alarm_limit` BOOLEAN DEFAULT 0,
  `stel_alarm_active` BOOLEAN DEFAULT 0,
  `twa_alarm_active` BOOLEAN DEFAULT 0,
  `drift_active` BOOLEAN DEFAULT 0,
  `calibration_time` DATETIME NULL,
  `peak` DOUBLE NULL,
  `min` DOUBLE NULL,
  `average` DOUBLE NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`sensor_id`) REFERENCES `multirae_sensors` (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `multirae_sensing_data` (
  `id` INTEGER AUTO_INCREMENT NOT NULL,
  `time_received` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sensing_component_id` INTEGER NOT NULL,
  `sample_id` VARCHAR(64) NOT NULL,
  `latitude` FLOAT DEFAULT 0.0,
  `longitude` FLOAT DEFAULT 0.0,
  `mgrs` varchar(32) DEFAULT NULL,
  `start_time` DATETIME NOT NULL,
  `end_time` DATETIME NOT NULL,
  `chemical_name` VARCHAR(16) NOT NULL,
  `cas_number` VARCHAR(32),
  `chem_measure_value` DOUBLE NOT NULL,   -- This is the 'real' value
  `chem_measure_units` VARCHAR(8) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`sensing_component_id`) REFERENCES `multirae_sensing_component` (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- One record each time that a particular sensing component changes these values.
--
CREATE TABLE `twa_stel_data` (   -- Short-Term Exposure Limit (STEL) and Time-Weighted Average (TWA)
  `id` INTEGER AUTO_INCREMENT NOT NULL,
  `time_received` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sensing_component_id` INTEGER NOT NULL,
  `sample_id` VARCHAR(64) NOT NULL,
  `date_time` DATETIME NOT NULL,
  `twa` FLOAT DEFAULT NULL,
  `stel` FLOAT DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`sensing_component_id`) REFERENCES `multirae_sensing_component` (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `sensor_metadata` (
    `id` INT AUTO_INCREMENT NOT NULL,
    `sample_id` VARCHAR(16) NOT NULL,
    `sensor_type` VARCHAR(16) NOT NULL,
    `sampling_point` TEXT,
    `latitude` DOUBLE DEFAULT NULL,
    `longitude` DOUBLE DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY (`sample_id`)
) ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;


CREATE TABLE `cot_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `xpath` varchar(255) NOT NULL,
  `datatype` varchar(32) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_uniq` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cot_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `cot_type` varchar(255) NOT NULL,
  `cot_time` timestamp NOT NULL,
  `start` timestamp NOT NULL,
  `stale` timestamp NOT NULL,
  `how` varchar(255) NOT NULL,
  `point_lat` varchar(64) NOT NULL DEFAULT '',
  `point_lon` varchar(64) NOT NULL DEFAULT '',
  `point_hae` varchar(64) NOT NULL DEFAULT '',
  `point_ce` varchar(64) NOT NULL DEFAULT '',
  `point_le` varchar(64) NOT NULL DEFAULT '',
  `point_sat` varchar(64) NOT NULL DEFAULT '',
  `xml_message` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_uniq` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cot_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cot_event_id` int(11) NOT NULL,
  `cot_attribute_id` int(11) NOT NULL,
  `value` varchar(1024) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_cot_event` (`cot_event_id`),
  KEY `fk_cot_attribute` (`cot_attribute_id`),
  CONSTRAINT `fk_cot_attribute` FOREIGN KEY (`cot_attribute_id`) REFERENCES `cot_attributes` (`id`),
  CONSTRAINT `fk_cot_event` FOREIGN KEY (`cot_event_id`) REFERENCES `cot_events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `meta_user_info` (
    `id` INT AUTO_INCREMENT NOT NULL,
    `full_name` VARCHAR(64) NOT NULL,
    `email` VARCHAR(64) NOT NULL,
    `commercial_phone_number` VARCHAR(16) DEFAULT NULL,
    `dsn_phone_number` VARCHAR(16) DEFAULT NULL,
    `unit` VARCHAR(64) NOT NULL,
    `user_name` VARCHAR(64) NOT NULL,
    `date_updated` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;

CREATE TABLE `meta_array_profile` (
    `sensor_array_id` VARCHAR(64) NOT NULL,
    `date_created` DATETIME NOT NULL,
    `array_type` VARCHAR(64) NOT NULL,
    `array_mgrs` VARCHAR(32) NOT NULL,
    `doehrs_location` VARCHAR(32) NOT NULL,
    `array_location_description` VARCHAR(1024) DEFAULT NULL,
    `sampling_reason` VARCHAR(64) NOT NULL,
    `site_occupation_date` DATE DEFAULT NULL,
    `sampling_reason_narrative` VARCHAR(1024) DEFAULT NULL,
    `sampling_data_received` TINYINT(4) DEFAULT 0,
    `number_of_sensors_in_array` INT DEFAULT NULL,
    `attachments` MEDIUMBLOB DEFAULT NULL,
    PRIMARY KEY (`sensor_array_id`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meta_array_pathway` (
  `id` varchar(32) NOT NULL,
  `sensor_array_id` varchar(12) NOT NULL DEFAULT '',
  `location` varchar(128) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL,
  `threat_source` varchar(128) NOT NULL,
  `health_hazard` text NOT NULL,
  `exposure_point_area` varchar(64) NOT NULL,
  `exposure_medium` varchar(128) NOT NULL DEFAULT '',
  `exposure_route` varchar(128) NOT NULL DEFAULT '',
  `exposure_duration_of_concern` varchar(128) NOT NULL DEFAULT '',
  `exposed_population` varchar(256) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

CREATE TABLE `meta_sensor_profile` (
    `sensor_metadata_id` VARCHAR(64) NOT NULL,
    `sensor_id` VARCHAR(64) NOT NULL,
    `sensor_type` ENUM('JCAD', 'MultiRAE', 'Hapsite') NOT NULL,
    `serial_number` VARCHAR(32) NOT NULL,
    `software_version` VARCHAR(32) NOT NULL,
    `hardware_version` float default null,
    `mfr_date` DATE default NULL,
    `sensor_mgrs_location` VARCHAR(64) not null,
    `sensor_location_description` VARCHAR(1024) not null,
    `sensor_employment_type` ENUM('Single Sample(s)', 'Datalog') DEFAULT NULL,
    FOREIGN KEY (`sensor_metadata_id`)
        REFERENCES `meta_array_profile` (`sensor_array_id`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;

-- -----------------------------------------------------
-- Table `ifa_incident`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ifa_incident` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `incident_type` set('OTHER','CONCUSSIVE','CHEMICAL','RADIOLOGICAL') NOT NULL,
  `report_start_datetime` datetime DEFAULT NULL,
  `report_completion_datetime` datetime DEFAULT NULL,
  `last_edited_by` int(11) DEFAULT NULL,
  `last_edited_on` DATETIME DEFAULT NULL,
  `status` enum('INCOMPLETE','COMPLETE','DELETED','IN_PROGRESS','APPROVED_BY_QA') DEFAULT 'INCOMPLETE',
  `name` varchar(128) DEFAULT NULL,
  `short_name` varchar(128) DEFAULT NULL,
  `ccir_sigact_num` varchar(32) DEFAULT NULL,
  `scenario` enum('ALLIED_OPERATIONS','TRAINING') DEFAULT NULL,
  `start_datetime` datetime DEFAULT NULL,
  `end_datetime` datetime DEFAULT NULL,
  `cause` enum('ENEMY_ACTION','ACCIDENTAL_RELEASE','UNKNOWN','OTHER') DEFAULT NULL,
  `cause_descr` varchar(128) DEFAULT NULL,
  `summary` text DEFAULT NULL,
  `mgrs` varchar(32) DEFAULT NULL,
  `map_image_available` tinyint(4) DEFAULT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `pers_assoc_to_incident` enum('YES','NO','UNKNOWN') DEFAULT NULL,
  `pers_assoc_to_incident_descr` varchar(128) DEFAULT NULL,
  `signs_symptoms_reported` enum('YES','NO','UNKNOWN') DEFAULT NULL,
  `signs_symptoms_reported_descr` varchar(128) DEFAULT NULL,
  `units_involved` varchar(128) DEFAULT NULL,
  `were_you_at_incident_location` tinyint(4) DEFAULT NULL,
  `qa_status` enum('IN_PROGRESS','READY_FOR_QA','APPROVED_BY_QA') DEFAULT 'IN_PROGRESS',
  `submission_status` enum('NOT_SUBMITTED','SUBMITTED','ACCEPTED') DEFAULT 'NOT_SUBMITTED',
  `mitigation_ppe` text DEFAULT NULL,
  `mitigation_personal_decontamination` text DEFAULT NULL,
  `mitigation_area_decontamination` text DEFAULT NULL,
  `mitigation_medical_countermeasures` text DEFAULT NULL,
  `action_risk_communication` boolean DEFAULT false,
  `action_risk_communication_descr` varchar(128) DEFAULT NULL,
  `action_summary_of_exposures` boolean DEFAULT false,
  `action_summary_of_exposures_desc` varchar(128) DEFAULT NULL,
  `action_other` boolean DEFAULT false,
  `action_other_desc` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB;

-- -----------------------------------------------------
-- Table `ifa_personnel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ifa_personnel` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ssn` VARCHAR(16) NULL DEFAULT NULL,
  `dodid` VARCHAR(16) NULL DEFAULT NULL,
  `name` VARCHAR(64) NULL DEFAULT NULL,
  `unit` VARCHAR(64) NULL DEFAULT NULL,
  `isfn` boolean DEFAULT false,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `ifa_preparer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ifa_preparer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ifa_incident_id` INT NOT NULL,
  `type` ENUM('INITIAL','ADDITIONAL') NOT NULL,
  `name` VARCHAR(64) NULL DEFAULT NULL,
  `email` VARCHAR(64) NULL DEFAULT NULL,
  `phone_num` VARCHAR(16) NULL DEFAULT NULL,
  `unit` VARCHAR(64) NULL DEFAULT NULL,
  `edit_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_ifa_preparer_ifa_incident_info1_idx` (`ifa_incident_id` ASC),
  CONSTRAINT `fk_ifa_preparer_ifa_incident1`
    FOREIGN KEY (`ifa_incident_id`)
    REFERENCES `ifa_incident` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `ifa_associated_personnel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ifa_associated_personnel` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ifa_incident_id` INT NOT NULL,
  `ifa_personnel_id` INT NOT NULL,
  `no_acute_symptoms` TINYINT NULL DEFAULT NULL,
  `signs_symptoms` enum('YES','NO','UNKNOWN') DEFAULT NULL,
  `ppe_worn` enum('YES','NO','UNKNOWN') DEFAULT NULL,
  `detector_sensor_data` enum('YES','NO','UNKNOWN') DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_ifa_associated_personnel_ifa_incident1_idx` (`ifa_incident_id` ASC),
  INDEX `fk_ifa_associated_personnel_ifa_personnel1_idx` (`ifa_personnel_id` ASC),
  CONSTRAINT `fk_ifa_associated_personnel_ifa_incident_conc1`
    FOREIGN KEY (`ifa_incident_id`)
    REFERENCES `ifa_incident` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ifa_associated_personnel_ifa_personnel1`
    FOREIGN KEY (`ifa_personnel_id`)
    REFERENCES `ifa_personnel` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `ifa_incident_concussive`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ifa_incident_concussive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ifa_incident_id` int(11) NOT NULL,
  `cause` set('BLUNT_OBJECT','FALL','FRAGMENT','VEHICLE_CRASH','ASSAULT','SPORTS_INJURY','GUN_SHOT_WOUND','BLAST','DEMOLITIONS','WEAPONS','OTHER') DEFAULT NULL,
  `cause_descr` varchar(128) DEFAULT NULL,
  `cause_est_distance` int(11) DEFAULT NULL,
  `field_mon_detec_data_collected` enum('YES','NO','UNKNOWN') DEFAULT NULL,
  `field_mon_detec_data_collected_descr` varchar(128) DEFAULT NULL,
  `field_mon_detec_data_collected_by` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_ifa_incident_concussive_ifa_selected_incident_type1_idx` (`ifa_incident_id`),
  CONSTRAINT `ifa_incident_concussive_ibfk_1` FOREIGN KEY (`ifa_incident_id`) REFERENCES `ifa_incident` (`id`)
) ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ifa_incident_chemical`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ifa_incident_chemical` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ifa_incident_id` int(11) NOT NULL,
  `exposure_routes` set('INHALATION','INGESTION','SKIN_ABSORPTION','SKIN_OR_EYE_CONTACT') DEFAULT NULL,
  `any_odors_reported` enum('YES','NO','UNKNOWN') DEFAULT NULL,
  `any_odors_reported_descr` varchar(128) DEFAULT NULL,
  `field_mon_detec_data_collected` enum('YES','NO','UNKNOWN') DEFAULT NULL,
  `field_mon_detec_data_collected_descr` varchar(128) DEFAULT NULL,
  `field_mon_detec_data_collected_by` varchar(128) DEFAULT NULL,
  `equipment_selected` set('M8_PAPER','M9_TAPE','M256_KIT','HAPSITE','JCAD','MULTIRAE','OTHER') DEFAULT NULL,
  `equipment_selected_descr` varchar(256) DEFAULT NULL,
  `samples_collected` enum('YES','NO','UNKNOWN') DEFAULT NULL,
  `samples_collected_descr` varchar(128) DEFAULT NULL,
  `samples_collected_by` varchar(128) DEFAULT NULL,
  `samples_collected_type` set('AIR','WATER','SOIL','WIPE','OTHER') DEFAULT NULL,
  `samples_collected_type_descr` varchar(128) DEFAULT NULL,
  `samples_collected_sent` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_ifa_incident_chemical_ifa_selected_incident_type1_idx` (`ifa_incident_id`),
  CONSTRAINT `ifa_incident_chemical_ibfk_1` FOREIGN KEY (`ifa_incident_id`) REFERENCES `ifa_incident` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB;


-- -----------------------------------------------------
-- Table `ifa_concussive_ied_symptom_checklist`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ifa_concussive_ied_symptom_checklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ifa_associated_personnel_id` INT NOT NULL,
  `physical_damage_to_body` BOOLEAN NULL DEFAULT NULL,
  `headaches_and_or_vomiting` BOOLEAN NULL DEFAULT NULL,
  `ear_ringing` BOOLEAN NULL DEFAULT NULL,
  `amnesia_loss_of_consiousness` BOOLEAN NULL DEFAULT NULL,
  `double_vision_dizziness` BOOLEAN NULL DEFAULT NULL,
  `something_feels_wrong` BOOLEAN NULL DEFAULT NULL,
  `within_50_meters` BOOLEAN NULL DEFAULT NULL,
  `wearing_blast_gauges` BOOLEAN NULL DEFAULT NULL,
  `max_peak_overpressure` FLOAT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ifa_concussive_ied_symptom_checklist_ifa_personnel_assoc_idx` (`ifa_associated_personnel_id` ASC),
  CONSTRAINT `fk_ifa_concussive_ied_symptom_checklist_ifa_personnel_associa1`
    FOREIGN KEY (`ifa_associated_personnel_id`)
    REFERENCES `ifa_associated_personnel` (`id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE
)ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ifa_concussive_ppe_worn`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ifa_concussive_ppe_worn` (
    `id` INT NOT NULL auto_increment,
    `ifa_associated_personnel_id` INT default 0,
    `helmet` BOOLEAN DEFAULT FALSE,
    `breaching_shield` BOOLEAN DEFAULT FALSE,
    `pri_hearing_protection` BOOLEAN DEFAULT FALSE,
    `sec_hearing_protection` BOOLEAN DEFAULT FALSE,
    `other` BOOLEAN DEFAULT FALSE,
    `other_descr` VARCHAR(64) NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_ifa_concussive_ppe_worn_ifa_personnel_associated_with_co_idx` (`ifa_associated_personnel_id` ASC),
    CONSTRAINT `fk_ifa_concussive_ppe_worn_ifa_personnel_associated_with_conc1`
		FOREIGN KEY (`ifa_associated_personnel_id`)
        REFERENCES `ifa_associated_personnel` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
)  ENGINE=INNODB;

-- -----------------------------------------------------
-- Table `ifa_concussive_detector_sensor_data`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ifa_concussive_detector_sensor_data` (
  `id` INT NOT NULL auto_increment,
  `ifa_associated_personnel_id` INT NOT NULL,
  `datetime` DATETIME NULL DEFAULT NULL,
  `blast_gauge_location` VARCHAR(16) NULL DEFAULT NULL,
  `set_serial_num` VARCHAR(32) NULL DEFAULT NULL,
  `color` VARCHAR(16) NULL DEFAULT NULL,
  `peak_overpressure` FLOAT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ifa_concussive_detector_sensor_data_ifa_personnel_associ_idx` (`ifa_associated_personnel_id` ASC),
  CONSTRAINT `fk_ifa_concussive_detector_sensor_data_ifa_personnel_associat1`
    FOREIGN KEY (`ifa_associated_personnel_id`)
    REFERENCES `ifa_associated_personnel` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ifa_concussive_medical`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ifa_concussive_medical` (
  `id` INT NOT NULL auto_increment,
  `ifa_associated_personnel_id` INT NOT NULL,
  `pers_decontamination_performed` enum('YES','NO','UNKNOWN') DEFAULT NULL,
  `decontamination_descr` VARCHAR(64) NULL DEFAULT NULL,
  `sought_medical_treatment` enum('YES','NO','UNKNOWN') DEFAULT NULL,
  `unitmedic_care_received` BOOLEAN DEFAULT FALSE,
  `unitmedic_care_received_descr` varchar(100) DEFAULT NULL,
  `battalionaidestation_care_received` BOOLEAN DEFAULT FALSE,
  `battalionaidestation_care_received_descr` varchar(100) DEFAULT NULL,
  `mtfcs_care_received` BOOLEAN DEFAULT FALSE,
  `mtfcs_care_received_descr` varchar(100) DEFAULT NULL,
  `resulting_duty_status` enum('RTD','MDEVC','OTHER', 'UNKNOWN') DEFAULT NULL,
  `comments` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ifa_concussive_medial_info_ifa_personnel_associated_with_idx` (`ifa_associated_personnel_id` ASC),
  CONSTRAINT `fk_ifa_concussive_medical_ifa_personnel_associated_with_c1`
    FOREIGN KEY (`ifa_associated_personnel_id`)
    REFERENCES `ifa_associated_personnel` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ifa_chemical_signs_or_symptoms`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ifa_chemical_signs_or_symptoms` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ifa_associated_personnel_id` INT NOT NULL,
  `eyes_irritation_burning` BOOLEAN DEFAULT FALSE,
  `eyes_pin_pointed_pupils` BOOLEAN DEFAULT FALSE,
  `resp_irritation_burning` BOOLEAN DEFAULT FALSE,
  `resp_coughing` BOOLEAN DEFAULT FALSE,
  `resp_trouble_breathing` BOOLEAN DEFAULT FALSE,
  `gastro_nausea_vomiting` BOOLEAN DEFAULT FALSE,
  `neuro_dizziness` BOOLEAN DEFAULT FALSE,
  `neuro_seizures` BOOLEAN DEFAULT FALSE,
  `skin_irritation_burning` BOOLEAN DEFAULT FALSE,
  `skin_blistering` BOOLEAN DEFAULT FALSE,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_ifa_chemical_signs_or_symptoms_ifa_personnel_associated__idx` (`ifa_associated_personnel_id` ASC),
  CONSTRAINT `fk_ifa_chemical_signs_or_symptoms_ifa_personnel_associated_wi1`
    FOREIGN KEY (`ifa_associated_personnel_id`)
    REFERENCES `ifa_associated_personnel` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ifa_chemical_ppe_worn`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ifa_chemical_ppe_worn` (
    `id` INT NOT NULL auto_increment,
    `ifa_associated_personnel_id` INT NOT NULL,
    `gas_mask` BOOLEAN DEFAULT FALSE,
    `suit` BOOLEAN DEFAULT FALSE,
    `gloves` BOOLEAN DEFAULT FALSE,
    `boots` BOOLEAN DEFAULT FALSE,
    `other` BOOLEAN DEFAULT FALSE,
    `other_descr` VARCHAR(64) NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_ifa_chemical_ppe_worn_ifa_personnel_associated_with_chem_idx` (`ifa_associated_personnel_id` ASC),
    CONSTRAINT `fk_ifa_chemical_ppe_worn_ifa_personnel_associated_with_chemic1`
		FOREIGN KEY (`ifa_associated_personnel_id`)
        REFERENCES `ifa_associated_personnel` (`id`)
        ON DELETE CASCADE
        ON UPDATE NO ACTION
)  ENGINE=INNODB;

-- -----------------------------------------------------
-- Table `ifa_chemical_detector_sensor_data`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ifa_chemical_detector_sensor_data` (
  `ifa_associated_personnel_id` INT NOT NULL,
  `chem_rapid_hra_id` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`ifa_associated_personnel_id`, `chem_rapid_hra_id`),
  INDEX `fk_ifa_chemical_detector_sensor_data_ifa_personnel_associ_idx` (`ifa_associated_personnel_id` ASC),
  INDEX `fk_ifa_chemical_detector_sensor_data_chem_rapid_hra_idx` (`chem_rapid_hra_id` ASC),
  CONSTRAINT `fk_ifa_chemical_detector_sensor_data_ifa_personnel1`
    FOREIGN KEY (`ifa_associated_personnel_id`)
    REFERENCES `ifa_associated_personnel` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ifa_chemical_detector_sensor_data_chem_rapid1`
    FOREIGN KEY (`chem_rapid_hra_id`)
    REFERENCES `chem_rapid_hra` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION
)
ENGINE = InnoDB DEFAULT CHARSET=utf8mb4;

-- -----------------------------------------------------
-- Table `ifa_incident_image`
-- Do not expect file size to exceed 22M Bytes
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ifa_upload_attachment` (
  `id`INTEGER AUTO_INCREMENT NOT NULL,
  `ifa_incident_id` int(11) NOT NULL,
  `file_name` VARCHAR(64) NOT NULL,
  `file_type` VARCHAR(64) NOT NULL,
  `file` MEDIUMBLOB NOT NULL,
  `attachment_type` ENUM('ROSTER', 'MEDSITREP', 'SENSORRAWDATA', 'PHOTOS', 'OTHER', 'COMPLETEHRA') NOT NULL,
  `file_uploaded_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `ifa_upload_attachment_ibfk_1`  FOREIGN KEY (`ifa_incident_id`)
        REFERENCES `ifa_incident` (`id`))
ENGINE = InnoDB;



CREATE TABLE IF NOT EXISTS `iss_device_type` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  `requires_initialization` BOOLEAN NOT NULL DEFAULT FALSE,
  `requires_calibration` BOOLEAN NOT NULL DEFAULT FALSE,
  `calibration_schedule` VARCHAR(128) NULL,
  `requires_read_between_issuance` BOOLEAN NOT NULL DEFAULT FALSE,
  `expected_serial_number_format` VARCHAR(128) NOT NULL,
  `wear_location_options` SET('RIGHT_WRIST','LEFT_WRIST', 'HEAD', 'SHOULDER', 'CHEST', 'OTHER') NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

-- -----------------------------------------------------
-- Table `iss_device`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `iss_device` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `iss_device_type_id` INT UNSIGNED NOT NULL,
  `serial_number` VARCHAR(32) NOT NULL,
  `lot_number` VARCHAR(32) NULL,
  `model_number` VARCHAR(32) NULL,
  `manufacturer` VARCHAR(64),
  `status` ENUM('SERVICEABLE', 'ISSUED', 'NONSERVICEABLE', 'DELETED') NOT NULL,
  `expiration_date` DATETIME NULL,
  `initialization_date` DATETIME NOT NULL,
  `last_calibration_event_id` INT UNSIGNED NULL,
  `last_status_change_event_id` INT UNSIGNED NULL,
  `last_issued_event_id` INT UNSIGNED NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`iss_device_type_id`) REFERENCES `iss_device_type` (`id`),
  FOREIGN KEY (`last_calibration_event_id`) REFERENCES `iss_event` (`id`),
  FOREIGN KEY (`last_status_change_event_id`) REFERENCES `iss_event` (`id`),
  FOREIGN KEY (`last_issued_event_id`) REFERENCES `iss_event` (`id`))
  
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

-- -----------------------------------------------------
-- Table `iss_personnel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `iss_personnel` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(128) NOT NULL,
  `last_name` VARCHAR(128) NOT NULL,
  `dodid` VARCHAR(10) NOT NULL UNIQUE,
  `unit` VARCHAR(128) NOT NULL,
  `grade` VARCHAR(64) NULL DEFAULT NULL,
  `mos_code` VARCHAR(9) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

-- -----------------------------------------------------
-- Table `iss_event`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `iss_event` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_by` VARCHAR(128) NOT NULL,
  `created_on` DATETIME NOT NULL DEFAULT NOW(),
  `iss_device_id` INT UNSIGNED NOT NULL,
  `iss_personnel_id` INT UNSIGNED DEFAULT NULL,
  `date` DATETIME NOT NULL,
  `location` VARCHAR(255) DEFAULT NULL,
  `wear_location` ENUM('RIGHT_WRIST','LEFT_WRIST','WRIST', 'HEAD', 'SHOULDER', 'CHEST', 'OTHER') DEFAULT NULL,
  `wear_location_description` VARCHAR(255) DEFAULT NULL,
  `initial_value` VARCHAR(255) DEFAULT NULL,
  `type` ENUM('ISSUED', 'VERIFIED', 'LOST', 'DESTROYED', 'DAMAGED', 'REPAIRED', 'INITIALIZED', 'CALIBRATED', 'RETURNED', 'OTHER') NOT NULL,
  `device_status` ENUM('SERVICEABLE', 'ISSUED', 'NONSERVICEABLE') NOT NULL,
  `notes` VARCHAR(255) NULL DEFAULT NULL,
  `personnel_association` ENUM('SCAN', 'SELECTION', 'MANUAL') NOT NULL DEFAULT 'MANUAL',
  `sensor_association` ENUM('SCAN', 'SELECTION', 'MANUAL') NOT NULL DEFAULT 'MANUAL',
  `location_association` ENUM('GPS', 'MANUAL') NOT NULL DEFAULT 'MANUAL',
  `date_association` ENUM('SYSTEM', 'GPS', 'MANUAL') NOT NULL DEFAULT 'MANUAL',
  PRIMARY KEY (`id`),
  FOREIGN KEY (`iss_device_id`) REFERENCES `iss_device` (`id`),
  FOREIGN KEY (`iss_personnel_id`) REFERENCES `iss_personnel` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

-- -----------------------------------------------------
-- Table `iss_device_set`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `iss_device_set` (
  `iss_device_id` INT UNSIGNED NOT NULL,
  `iss_set_id` INT UNSIGNED NOT NULL,
  `date_added` DATETIME NOT NULL DEFAULT NOW(),
  `date_removed` DATETIME NULL,
  PRIMARY KEY (`iss_device_id`, `iss_set_id`),
  FOREIGN KEY (`iss_device_id`) REFERENCES `iss_device` (`id`),
  FOREIGN KEY (`iss_set_id`) REFERENCES `iss_set` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

-- -----------------------------------------------------
-- Table `iss_device_blast`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `iss_device_blast` (
  `iss_device_id` INT UNSIGNED NOT NULL,
  `device_type` ENUM('HEAD', 'SHOULDER', 'CHEST', 'OTHER') NOT NULL,
  PRIMARY KEY (`iss_device_id`),
  FOREIGN KEY (`iss_device_id`) REFERENCES `iss_device` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

-- -----------------------------------------------------
-- Table `iss_set`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `iss_set` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `date_created` DATETIME NOT NULL DEFAULT NOW(),
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


CREATE TABLE multirae_ingestion_files (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`multirae_sensor_id` INT NOT NULL,
  `file_name` VARCHAR(32) NOT NULL,
	`file` MEDIUMBLOB DEFAULT NULL,
  `created` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
	FOREIGN KEY (`multirae_sensor_id`) REFERENCES `multirae_sensors` (`id`)
);

-- -----------------------------------------------------
-- Table `bgd_personnel`
-- -----------------------------------------------------
CREATE TABLE `bgd_personnel` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `display_name` varchar(64) DEFAULT NULL,
  `dod_id` varchar(32) NOT NULL,
  `unit_name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB
DEFAULT CHARACTER SET = utf8mb4;

-- -----------------------------------------------------
-- Table `bgd_event`
-- -----------------------------------------------------

CREATE TABLE `bgd_event` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `bgd_device_id` INT NULL,
  `bgd_personnel_id` INT NULL,
  `event_date_time` datetime NOT NULL,
  `env_hazard_type` VARCHAR(32) DEFAULT NULL,
  `est_exp_max_peak_overpressure` FLOAT NULL,
  `est_exp_total_positive_impulse` FLOAT NULL,
  `bop_exposure_summary` TEXT NULL DEFAULT NULL,
  `bop_exposure_hra_summary` TEXT NULL DEFAULT NULL,
  `group_event_id` varchar(32) DEFAULT NULL,
  `external_event_id` varchar(32) DEFAULT NULL,
  `download_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `bgd_personnel_id` (`bgd_personnel_id`),
  CONSTRAINT `bgd_pers_ibfk_1` FOREIGN KEY (`bgd_personnel_id`) REFERENCES `bgd_personnel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB
DEFAULT CHARACTER SET = utf8mb4;

-- -----------------------------------------------------
-- Table `bgd_device_data`
-- -----------------------------------------------------
CREATE TABLE `bgd_device_data` (
   `id` INT NOT NULL AUTO_INCREMENT,
   `bgd_event_id` INT NOT NULL,
   `bgd_device_id` INT NOT NULL,
   `peak_overpressure` double NOT NULL,
   `total_positive_impulse` float DEFAULT NULL,
   `acceleration_peak_magnitude` float DEFAULT NULL,
   `raw_data_exists` BOOLEAN NOT NULL,
   `real_notreal_flag` enum( 'REAL' , 'NOTREAL' , 'ERROR' ) DEFAULT NULL,
   `exclude_data` BOOLEAN DEFAULT FALSE,
   `exclude_data_comments` TEXT DEFAULT NULL,
   `qa_approved` BOOLEAN DEFAULT FALSE,
   `qa_approved_date` DATETIME DEFAULT NULL,
   `qa_comments` TEXT DEFAULT NULL,
   PRIMARY KEY ( `id` ),
   KEY `bgd_device_id` (`bgd_device_id`),
   CONSTRAINT `bgd_event_ibfk_2` FOREIGN KEY( `bgd_event_id` ) REFERENCES `bgd_event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
   CONSTRAINT `bgd_device_ibfk_2` FOREIGN KEY( `bgd_device_id` ) REFERENCES `bgd_device` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB
DEFAULT CHARACTER SET = utf8mb4;

-- -----------------------------------------------------
-- Table `bgd_device`
-- -----------------------------------------------------
CREATE TABLE `bgd_device` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `bgd_personnel_id` INT NULL,
  `uid` varchar(64) DEFAULT NULL,
  `gauge_serial_number` varchar(32) DEFAULT NULL,
  `iss_device_serial_number` varchar(32) DEFAULT NULL,
  `mounting_location` varchar(32) NOT NULL,
  `activation_date` datetime DEFAULT NULL,
  `firmware_rev` varchar(32) DEFAULT NULL,
  `battery_level` INT DEFAULT NULL,
  `b3var100` INT DEFAULT NULL,
  `b3var101` INT DEFAULT NULL,
  `remaining_life` varchar(16) DEFAULT NULL,
  `end_of_life_reached` BOOLEAN NOT NULL DEFAULT 0,
  `setup_mode` varchar(16) DEFAULT NULL,
  `wireless_state` varchar(16) DEFAULT NULL,
  `summary_pressure_thresh` float DEFAULT NULL,
  `moderate_pressure_thresh` float DEFAULT NULL,
  `severe_pressure_thresh` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `bgd_personnel_id` (`bgd_personnel_id`),
  CONSTRAINT `bgd_device_ibfk_1` FOREIGN KEY (`bgd_personnel_id`) REFERENCES `bgd_personnel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB
DEFAULT CHARACTER SET = utf8mb4;

-- -----------------------------------------------------
-- Table `bgd_event_gauge_raw_data`
-- -----------------------------------------------------
CREATE TABLE `bgd_event_raw_data` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `bgd_device_data_id` INT NOT NULL,
  `time` float NOT NULL,
  `overpressure` float NOT NULL,
  `bgd_event_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  KEY `bgd_event_id` (`bgd_event_id`),
  CONSTRAINT `bgd_event_raw_data_ibfk_1` FOREIGN KEY (`bgd_event_id`) REFERENCES `bgd_event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  KEY `bgd_dev_data_id` (`bgd_device_data_id`),
  CONSTRAINT `bgd_event_raw_devd_ibfk_1` FOREIGN KEY (`bgd_device_data_id`) REFERENCES `bgd_device_data` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB
DEFAULT CHARACTER SET = utf8mb4;

-- -----------------------------------------------------
-- Table `bgd_event_gauge_acceleration`
-- -----------------------------------------------------
CREATE TABLE `bgd_event_acceleration` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `time` float NOT NULL,
  `accel` float NOT NULL,
  `bgd_event_id` INT NOT NULL,
  `bgd_device_data_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  KEY `bgd_event_id` (`bgd_event_id`),
  CONSTRAINT `bgd_event_acceleration_ibfk_1` FOREIGN KEY (`bgd_event_id`) REFERENCES `bgd_event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  KEY `bgd_devdat_accel_id` (`bgd_device_data_id`),
  CONSTRAINT `bgd_event_accel_devdat_ibfk_1` FOREIGN KEY (`bgd_device_data_id`) REFERENCES `bgd_device_data` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB
DEFAULT CHARACTER SET = utf8mb4;

-- -----------------------------------------------------
-- Table `wfl_weapon_firing_log`
-- -----------------------------------------------------
CREATE TABLE wfl_weapon_firing_log (
  dod_id varchar(32) NOT NULL,
	blast_wfl_id			INT NOT NULL,
	document_id				VARCHAR(50),
	app_nucleus_id			VARCHAR(50),
	blast_record_id			INT NOT NULL,
	blast_web_user_id		INT,
	user_name				VARCHAR(50),
	approval_status			VARCHAR(50),
	capture_method			VARCHAR(50),
	num_of_weapons			INT,
	other_users				VARCHAR(50),
	from_date_time			DATETIME,
	to_date_time			DATETIME,
	time_zone				VARCHAR(50),
	firing_range			VARCHAR(50) NOT NULL,
	location                VARCHAR(50),
	event_title             VARCHAR(50),
	unit_name               VARCHAR(50),
	bop_sensor				BOOLEAN NOT NULL  DEFAULT 0,
	in_ear_protection       BOOLEAN NOT NULL  DEFAULT 0,
	over_ear_protection     BOOLEAN NOT NULL  DEFAULT 0,
	PRIMARY KEY (blast_wfl_id)									
) ENGINE=InnoDB
DEFAULT CHARACTER SET = utf8mb4;

-- -----------------------------------------------------
-- Table `wfl_weapons_fired`
-- -----------------------------------------------------
CREATE TABLE wfl_weapons_fired (
	wfl_weapons_fired_id		INT NOT NULL,
	blast_wfl_id			INT NOT NULL,
	weapon_system_type		VARCHAR(50) NOT NULL,
	weapon_system			VARCHAR(50) NOT NULL,
  weapon_system_other VARCHAR(50) DEFAULT NULL,
	rounds_fired			INT NOT NULL,
	body_position			VARCHAR(50) NOT NULL,
    crew_position		    VARCHAR(50),
	enclosed_space			VARCHAR(50),
	weapons_mod				VARCHAR(50),
	breaching_door			VARCHAR(50),
	breaching_wall			VARCHAR(50),
	munition_type			VARCHAR(50),
	munition_type_other     VARCHAR(50),
	other_details			VARCHAR(225),
	proximity_to_source     FLOAT,
	stacking_position       INT,
  breaching_umbc VARCHAR(255) DEFAULT NULL,
  breaching_sof VARCHAR(255) DEFAULT NULL,
  under_water VARCHAR(50) DEFAULT NULL,
  breaching_cust_conf_desc VARCHAR(512) DEFAULT NULL,
  breaching_cust_weight decimal(5,2) DEFAULT NULL,
  breaching_wall_type VARCHAR(50) DEFAULT NULL,
  breaching_door_type VARCHAR(50) DEFAULT NULL,
  breaching_target_door BOOLEAN DEFAULT NULL,
  breaching_target_wall BOOLEAN DEFAULT NULL,
  breaching_target_other BOOLEAN DEFAULT NULL,
  target_other_details VARCHAR(255) DEFAULT NULL,
	PRIMARY KEY (wfl_weapons_fired_id),
		CONSTRAINT `wfl_weapons_fired_ibfk_1` FOREIGN KEY( `blast_wfl_id` ) REFERENCES `wfl_weapon_firing_log` (`blast_wfl_id`) ON DELETE NO ACTION ON UPDATE NO ACTION										
) ENGINE=InnoDB
DEFAULT CHARACTER SET = utf8mb4;

--
-- Dumping routines for database 'jhrmecddb'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-19 22:31:19
