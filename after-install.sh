#!/bin/bash

#Copy the original config files back in
cp /tmp/production.js.bak /srv/jhrm-ecd/config/production.js
cp /tmp/local.js.bak /srv/jhrm-ecd/config/local.js

#Refresh the database with create and populate scripts
mysql -h jhrmdb01.cc3xs7k1c2o9.us-gov-west-1.rds.amazonaws.com -P 3306 -u masteruser -p 'boats-ROBES-722#' < /srv/jhrm-ecd/database/init_scripts/create_tables.sql
mysql -h jhrmdb01.cc3xs7k1c2o9.us-gov-west-1.rds.amazonaws.com -P 3306 -u masteruser -p 'boats-ROBES-722#' < /srv/jhrm-ecd/database/init_scripts/populate_tables.sql

#Set PM2 environment variable for this script
export PM2_HOME="/home/ec2-user/.pm2"

#Restart the server using PM2
pm2 restart server