const esmImport = require('esm')(module);
const https = require('follow-redirects').https;
const config = require('config');
const { Datetime } = esmImport("../shared/datetime.js");

/**
 * This class exposes methods to call the SmartaBase API. Configuration should 
 * be provided in the application config file local.js. 
 * 
 * Public methods correspond to smartabase commands. Class is instantiated upon 
 * import.
 * 
 * const SmartabaseApi = require("src/smartabase/api-library");
 * SmartabaseApi.eventImportIFA()
 * SmartabaseApi.eventImportBlastGauge()
 * 
 * Private methods are prefixed by an underscore.
 */
class SmartabaseApi 
{
    /**
     * Set up configuration for the class. If not provided here, default 
     * username and password from the application config will be used. 
     * 
     * @param {string} username 
     * @param {string} password 
     */
    constructor(username, password)
    {
        this._username = username || config.get("smartabase.default_username");
        this._password = password || config.get("smartabase.default_password");
        this._host = config.get("smartabase.host");
        this._path = config.get("smartabase.path");
        this._auth_header = `Basic ${Buffer.from(this._username + ':' + this._password, 'utf8').toString('base64')}`;
        this._userId = null;
    }

    /**
     * Private 
     * 
     * Prepare requests to be sent to the API. This should only be called 
     * internally, by the command methods. 
     * 
     * This method returns a promise 
     * 
     * @param {string} command 
     * @param {object} payload 
     * @returns Promise
     */
    _send(command, payload)
    {
        return new Promise((resolve, reject) =>
        {
            let options = {
                'method': 'POST',
                'hostname': this._host,
                'path': `${this._path}/${command}?informat=json&format=json`,
                'headers': {
                    'Authorization': this._auth_header,
                    'Content-Type': 'application/json'
                },
                'maxRedirects': 20
            };

            var request = https.request(options, (res) => {

                let chunks = [];

                res.on("data", function (chunk)
                {
                    chunks.push(chunk);
                });

                res.on("end", function (chunk)
                {
                    // If the response is not complete when the connection ends
                    // we have to assume something went wrong and stop here. 
                    if (!res.complete)
                    {
                        reject("Connection interrupted.");
                    }

                    // If the status code is not in the 2xx range (success) then
                    // we will not try to parse the response body, because it 
                    // may not be JSON. 
                    if (res.statusCode < 200 || res.statusCode > 299)
                    {
                        reject(res.statusMessage);
                    }

                    // Try to parse the response body a JSON and return it to 
                    // the caller. 
                    let body = Buffer.concat(chunks).toString();
                    try 
                    {
                        let parsed = JSON.parse(body);
                        resolve(parsed);
                    }
                    catch (e)
                    {
                        reject("Invalid response format");
                    }
                });

                // Generally this will be a protocol error. 
                res.on("error", function (error)
                {
                    reject(error);
                });

            });

            request.write(JSON.stringify(payload));
            request.end();
        });
    }

    /**
     * Call the user authentication endpoint with configured credentials in the 
     * body, and set the user ID based on the response. 
     * 
     * If userid is already set, will not be called multiple times. 
     * 
     * @returns Promise
     */
    async _auth()
    {
        if(!this.userId)
        {
            let response = await this._send('userauthentication', {
                username: this._username,
                password: this._password
            });
            this.userId = response.userId;
        }
    }

    /**
     * Import Blast Gauge data to SmartaBase
     * 
     * 
     * @param {object} payloadData
     * @returns
     */
    async eventImportBlastGauge(payloadData)
    {
        await this._auth();
        const form = "JHRM Blast Gauge V_1";
        payloadData['Event Date Time'] = new Datetime(payloadData['Event Date Time']);
        payloadData['Event Date'] = SmartabaseApi.getFormattedDate(payloadData['Event Date Time']);

        // Create payload for the Blast Gauge.
        let payloadBG = SmartabaseApi.getFormattedObject(payloadData);
      
        let payload = {
            "formName": form,
            "startDate": SmartabaseApi.getFormattedDate(eventDateTime),
            "startTime": SmartabaseApi.getFormattedTime(eventDateTime),
            "finishDate": SmartabaseApi.getFormattedDate(eventDateTime),
            "finishTime": SmartabaseApi.getFormattedTime(eventDateTime),
            "userId": {"userId" : this.userId }, // This id is part of the header for Smartabase, although it needs to be reviewed to update it later, to match the user of the person's DodId in this form.
            "enteredByUserId": this.userId,
            "rows": [
                        {"row": 0,
                         "pairs" : payloadBG
                        }
                    ]
        };
                  
        return this._send('eventimport', payload);
    }

    /**
     * Import IfaReport to Smartabase 
     * 
     * This is currently in progress and included as an example. 
     * 
     * @param {IfaReport} IfaReport 
     * @returns 
     */
    async eventImportIFA(IfaReport)
    {
        await this._auth();
        const form = "JHRMS ifa_incident Prototype Copy";

        // Generate smartabase objects for each of the IFA Associated Personnel
        // with all IFA fields. 
        let ifas = IfaReport.personnel.map(personnel => {

            // Build out the IFA payload object. It will then be processed by 
            // getFormattedOvject to handle string values, nulls, etc. Note that
            // this is broken into sections to match the Smartabase Form for 
            // ease of debugging / comparing values. 
            let ifa = {};

            // IFA incident report information
            ifa['start_date'] = SmartabaseApi.getFormattedDate(IfaReport.start_datetime);
            ifa['start_time'] = SmartabaseApi.getFormattedTime(IfaReport.start_datetime);
            ifa['end_date'] = SmartabaseApi.getFormattedDate(IfaReport.end_datetime);
            ifa['end_time'] = SmartabaseApi.getFormattedTime(IfaReport.end_datetime);
            ifa['start_report_date'] = SmartabaseApi.getFormattedDate(IfaReport.report_start_datetime);
            ifa['start_report_time'] = SmartabaseApi.getFormattedTime(IfaReport.report_start_datetime);
            ifa['end_report_completion_date'] = SmartabaseApi.getFormattedDate(IfaReport.report_completion_datetime);
            ifa['end_report_completion_time'] = SmartabaseApi.getFormattedTime(IfaReport.report_completion_datetime);
            ifa['start_date(text)'] = IfaReport.start_datetime.toString();
            ifa['end_date(text)'] = IfaReport.end_datetime ? IfaReport.end_datetime.toString() : null;
            
            // IFA report information
            ifa['report_name'] = IfaReport.name;
            ifa['report_short_name'] = IfaReport.short_name;
            ifa['data_produced'] = IfaReport.data_produced ? "Yes" : "No";
            ifa['dodid'] = personnel.dodid;
            ifa['isfn'] = personnel.isfn ? "Yes" : "No";
            ifa['IFA_id'] = IfaReport.id;
            
            // Preparers here - they get added below, because there are multiple
            // rows of preparers. 
            
            // ifa_incident Categoricals
            ifa['incident_type'] = IfaReport.incident_type;
            ifa['status'] = IfaReport.status;
            ifa['cause'] = IfaReport.cause;
            ifa['submission_status'] = IfaReport.submission_status;
            ifa['qa_status'] = IfaReport.qa_status;
            ifa['signs_symptoms_reported'] = IfaReport.symptoms_associated;
            ifa['pers_assoc_to_incident'] = IfaReport.personnel_associated;
            ifa['scenario'] = IfaReport.scenario;

            // ifa_incident
            ifa['CCIR_SigAct_Num'] = IfaReport.ccir_sigact_num;
            ifa['cause_descr'] = IfaReport.cause_descr;
            ifa['summary'] = IfaReport.summary;
            ifa['mgrs'] = IfaReport.location ? IfaReport.location.mgrs : null;
            ifa['map_image_available'] = IfaReport.map_image_available ? "True" : "False";
            ifa['latitude'] = IfaReport.location ? IfaReport.location.lat.toString() : null;
            ifa['longitude'] = IfaReport.location ? IfaReport.location.lon.toString(): null;
            ifa['units_involved'] = IfaReport.units_involved;
            ifa['were_you_at_incident_location'] = IfaReport.were_you_at_incident_location ? "Yes" : "No";
            ifa['mitigation_ppe'] = IfaReport.ppe_available;
            ifa['mitigation_personal_decontamination'] = IfaReport.ppe_decontamination;
            ifa['mitigation_area_decontamination'] = IfaReport.area_equip_decontamination;
            ifa['mitigation_medical_countermeasures'] = IfaReport.countermeasures_used;
            ifa['action_risk_communication'] =  IfaReport.action_risk_comms ? "Yes" : "No";
            ifa['action_risk_communication_descr'] = IfaReport.risk_comms_descr;
            ifa['action_summary_of_exposures'] = IfaReport.action_gen_summary ? "Yes" : "No";
            ifa['action_summary_of_exposures_desc'] = IfaReport.gen_summary_descr;

            // These two fields cannot be sent until the Smartabase form is fixed. Their types are swapped. 
            // ifa['action_other'] =  IfaReport.action_other ? "Yes" : "No";
            // ifa['action_other_desc'] = IfaReport.other_descr;
            
            // ifa_associated_personnel
            ifa['acute_symptoms'] = !personnel.no_acute_symptoms ? "True" : "False";
            ifa['name'] =  personnel.name,
            ifa['signs_symptoms'] =  personnel.signs_symptoms;
            ifa['PPE_Worn'] =  personnel.ppe_worn; 
            ifa['detector_sensor_data'] =  personnel.detector_sensor_data; 

            // Concussive
            if(IfaReport.concussive_details)
            {
                let item = IfaReport.concussive_details
                ifa['concussive_cause'] = item.cause;
                ifa['concussive_cause_desr'] = item.cause_descr;
                ifa['cause_est_distance'] = item.cause_est_distance;
                ifa['field_mon_detec_data_collected'] = item.field_mon_detec_data_collected;
                ifa['field_mon_detec_data_collected_descr'] = item.field_mon_detec_data_collected_descr;
                ifa['field_mon_detec_data_collected_by'] = item.field_mon_detec_data_collected_by;
            }
            
            // Chemical
            if(IfaReport.chemical_details)
            {
                let item = IfaReport.chemical_details;
                ifa['exposure_routes'] = item.exposure_routes;
                ifa['any_odors_reported'] = item.any_odors_reported;
                ifa['odors_description'] = item.any_odors_reported_descr;
                ifa['chemical_field_mon_detec_data_collected'] = item.field_mon_detec_data_collected;
                ifa['chem_field_mon_detec_data_collected_descr'] = item.field_mon_detec_data_collected_descr;
                ifa['chem_field_mon_detec_data_collected_by'] = item.field_mon_detec_data_collected_by;
                ifa['equipment_selected'] = item.equipment_selected;
                ifa['chem_equipment_selected_descr'] = item.equipment_selected_descr;
                ifa['chem_samples_collected'] = item.samples_collected;
                ifa['chem_samples_collected_descr'] = item.samples_collected_descr;
                ifa['chem_samples_collected_by'] = item.samples_collected_by;
                ifa['chem_samples_collected_type'] = item.samples_collected_type;
                ifa['chem_samples_collected_type_descr'] = item.samples_collected_type_descr;
                ifa['chem_samples_collected_sent'] = item.samples_collected_sent;
            }
            
            // Concussive IED
            if(personnel.concussive_signs_symptoms)
            {
                let item = personnel.concussive_signs_symptoms;
                ifa['physical_damage_to_body'] = item.physical_damage_to_body ? "Yes" : "No";
                ifa['headaches_and_or_vomiting'] = item.headaches_and_or_vomiting ? "Yes" : "No";
                ifa['ear_ringing'] = item.ear_ringing ? "Yes" : "No";
                ifa['amnesia_loss_of_consiousness'] = item.amnesia_loss_of_consiousness ? "Yes" : "No";
                ifa['double_vision_dizziness'] = item.double_vision_dizziness ? "Yes" : "No";
                ifa['something_feels_wrong'] = item.something_feels_wrong ? "Yes" : "No";
                ifa['within_50_meters'] = item.within_50_meters ? "Yes" : "No";
            }

            // Concussive PPE
            if(personnel.concussive_ppe_worn)
            {
                let item = personnel.concussive_ppe_worn;
                ifa['concussive_ppe_helmet'] = item.helmet ? "Yes" : "No";
                ifa['concussive_ppe_breaching_shield'] = item.breaching_shield ? "Yes" : "No";
                ifa['concussive_ppe_pri_hearing_protection'] = item.pri_hearing_protection ? "Yes" : "No";
                ifa['concussive_ppe_sec_hearing_protection'] = item.sec_hearing_protection ? "Yes" : "No";
                ifa['concussive_ppe_other'] = item.other ? "Yes" : "No";
                ifa['concussive_ppe_other_descr'] = item.concussive_ppe_other_descr;
            }

            // Concussive Detector
            // Note that this is taking only the first element in the array for 
            // now, as Smartabase is not currently set up to take this as a 
            // table. 
            if(personnel.concussive_detector_sensor_data && personnel.concussive_detector_sensor_data.length)
            {
                let item = personnel.concussive_detector_sensor_data;
                ifa['concussive_detector_date'] =  SmartabaseApi.getFormattedDate(item[0].datetime);
                ifa['concussive_detector_time'] =  SmartabaseApi.getFormattedTime(item[0].datetime);
                ifa['blast_gauge_location'] = item[0].blast_gauge_location;
                ifa['blast_gauge_serial_number'] = item[0].set_serial_num;
                ifa['blast_gauge_color'] = item[0].color;
                ifa['peak_overpressure'] = item[0].peak_overpressure;    
            }
            
            // Concussive Medical
            if(personnel.concussive_medical_information)
            {
                let item = personnel.concussive_medical_information;
                ifa['pers_decontamination_performed'] = item.pers_decontamination_performed;
                ifa['decontamination_descr'] = item.decontamination_descr;
                ifa['sought_medical_treatment'] = item.sought_medical_treatment;
                ifa['unitmedic_care_received'] = item.unitmedic_care_received ? "True" : "False";
                ifa['unitmedic_care_received_descr'] = item.unitmedic_care_received_descr;
                ifa['battalionaidestation_care_received'] = item.battalionaidestation_care_received ? "True" : "False";
                ifa['battalionaidestation_care_received_descr'] = item.battalionaidestation_care_received_descr;
                ifa['mtfcs_care_received'] = item.mtfcs_care_received ? "True" : "False";
                ifa['mtfcs_care_received_descr'] = item.mtfcs_care_received_descr;
                ifa['resulting_duty_status'] = item.resulting_duty_status;
                ifa['comments'] = item.comments;
            }

            // Chemical Signs or Symptoms
            if(personnel.chemical_signs_symptoms)
            {
                let item = personnel.chemical_signs_symptoms;
                ifa['eyes_irritation_burning'] = item.eyes_irritation_burning ? "Yes" : "No";
                ifa['eyes_pin_pointed_pupils'] = item.eyes_pin_pointed_pupils ? "Yes" : "No";
                ifa['resp_irritation_burning'] = item.resp_irritation_burning ? "Yes" : "No";
                ifa['resp_coughing'] =  item.resp_coughing ? "Yes" : "No";
                ifa['resp_trouble_breathing'] = item.resp_trouble_breathing ? "Yes" : "No";
                ifa['gastro_nausea_vomiting'] = item.gastro_nausea_vomiting ? "Yes" : "No";
                ifa['neuro_dizziness'] = item.neuro_dizzines ? "Yes" : "No";
                ifa['neuro_seizures'] = item.neuro_seizure ? "Yes" : "No";
                ifa['skin_irritation_burning'] = item.skin_irritation_burning ? "Yes" : "No";
                ifa['skin_blistering'] = item.skin_blistering ? "Yes" : "No";
            }
            
            // Chemical PPE
            if(personnel.chemical_ppe_worn)
            {
                let item = personnel.chemical_ppe_worn;
                ifa['gas_mask'] = item.gas_mask ? "Yes" : "No";
                ifa['Suit'] = item.suit ? "Yes" : "No";
                ifa['Gloves'] = item.gloves ? "Yes" : "No";
                ifa['Boots'] = item.boots ? "Yes" : "No";
                ifa['Other'] = item.other ? "Yes" : "No";
                ifa['chemical_PPE_other_description'] = item.other_desc;
            }
            
            return SmartabaseApi.getFormattedObject(ifa);
        });

        // Generate the list of preparers that will be sent with each of the 
        // above IFA objects. 
        let preparers = IfaReport.preparers
            .reduce((items, item) => {
                return items.find(x => x.email === item.email) ? items : [...items, item];
            }, [])
            .map((item=>{
                return SmartabaseApi.getFormattedObject({
                    'initial_preparer' : item.type,
                    'preparer_email' : item.email,
                    'preparer_name' : item.name,
                    'preparer_phone_number' : item.phone_num,
                    'preparer_unit': item.unit,
                });
            }))


        // For each of the IFA objects, merge it with the preparer array, and 
        // then send it to Smartabase, awaiting the response. 
        await Promise.all(ifas.map(async ifa => {
            let rows = preparers.map((row, i) => {
                return {
                    row : i,
                    pairs : i === 0 ? row.concat(ifa) : row
                };
            })
            let payload = {
                "formName" : form,
                "startDate" : SmartabaseApi.getFormattedDate(IfaReport.start_datetime),
                "startTime" : SmartabaseApi.getFormattedTime(IfaReport.start_datetime),
                "finishDate" : SmartabaseApi.getFormattedDate(IfaReport.end_datetime) || SmartabaseApi.getFormattedTime(IfaReport.start_datetime),
                "finishTime" : SmartabaseApi.getFormattedTime(IfaReport.end_datetime) || SmartabaseApi.getFormattedTime(IfaReport.start_datetime),
                "userId" : { 
                    "userId" : this.userId // This will need updating to match the user of the person's DodId in this form.
                }, 
                "enteredByUserId" : this.userId,
                "rows": rows
            };

            // Send to Smartabase
            return await this._send('eventimport', payload); 
        }));
    }

    // This method Posts a full WFL, including all weapons fired tables, to Smartabase.
    // This method takes in 'payloadData' which is an array containing the WFL and all the WFL weapons fired tables
    // The array is organised to always have the WFL table info as the first element of the array, then each element thereafter contains all of the WFL weapons fired entries corresponding to the WFL
    async eventImportWFL(payloadData)
    {
        await this._auth();
        const form = "JHRM WFLCopy"

        let wfl = {};
        let otherFired = {};
        let breachingFired = {};
        let newTableRows = [];
        
        let startDateTime = new Datetime(payloadData[0].from_date_time)
        let toDateTime = new Datetime(payloadData[0].to_date_time)
        
        // Data from database table wfl_weapon_firing_log
        wfl['dodid'] = payloadData[0].dod_id;
        wfl['User Name'] = payloadData[0].user_name;
        wfl['Unit Name'] = payloadData[0].unit_name;
        wfl['Event Title'] = payloadData[0].event_title;
        wfl['Number of Weapons'] = payloadData[0].num_of_weapons;
        wfl['From Date Time'] = SmartabaseApi.getFormattedDate(startDateTime);
        wfl['From Date Time (text)'] = startDateTime;
        wfl['To Date Time'] = SmartabaseApi.getFormattedDate(toDateTime);
        wfl['To Date Time(text)'] = toDateTime;
        wfl['Fire Range'] = payloadData[0].firing_range;
        wfl['Location'] = payloadData[0].location;
        wfl['Approval Status'] = payloadData[0].approval_status;
        wfl['In Ear Protection'] = payloadData[0].in_ear_protection;
        wfl['Overear Protection'] = payloadData[0].over_ear_protection;
        wfl['Blast WFL ID'] = payloadData[0].blast_wfl_id;
        wfl['Document ID'] = payloadData[0].document_id;
        wfl['App Nucleus ID'] = payloadData[0].app_nucleus_id;
        wfl['Blast Record ID'] = payloadData[0].blast_record_id;
        wfl['Blast Web User ID'] = payloadData[0].blast_web_user_id;
        wfl['Other Users'] = payloadData[0].other_users;
        wfl['Capture Method'] = payloadData[0].capture_method;
        wfl['Time Zone'] = payloadData[0].time_zone;
        wfl['Blast Sensors'] = payloadData[0].bop_sensor;

        // Weapons fired data for Breaching weapon type
        // weapon_system_type value determines which of the two tables to populate

        let breachingCount = 0;
        let otherCount = 0;

        for(let i = 1; i < payloadData.length; i++) {   
            
            // Populates the Smartabase table Breaching Weapons Fired Table based upon the weapon system type
            if (payloadData[i].weapon_system_type === 'Breaching and Explosives') {
                if (breachingCount === 0) {                
                    wfl['Weapon System Type'] = payloadData[i].weapon_system_type;
                    wfl['Weapon System'] = payloadData[i].weapon_system;
                    wfl['Weapon system Other1'] = payloadData[i].weapon_system_other;
                    wfl['WFL Weapons Fired ID1'] = payloadData[i].wfl_weapons_fired_id;
                    wfl['Breaching umbc'] = payloadData[i].breaching_umbc;
                    wfl['Breaching sof'] = payloadData[i].breaching_sof;
                    wfl['Breaching Cust Conf Desc'] = payloadData[i].breaching_cust_conf_desc;
                    wfl['Breaching Cust Weight'] = payloadData[i].breaching_cust_weight;
                    wfl['Breaching Wall Type'] = payloadData[i].breaching_wall_type;
                    wfl['Breaching Door Type'] = payloadData[i].breaching_door_type;
                    wfl['Breaching Target Door'] = payloadData[i].breaching_target_door;
                    wfl['Breaching Target Wall'] = payloadData[i].breaching_target_wall;
                    wfl['Breaching Target Other'] = payloadData[i].breaching_target_other;
                    wfl['Target Other Details'] = payloadData[i].target_other_details;
                } else {
                    breachingFired['Weapon System Type'] = payloadData[i].weapon_system_type;
                    breachingFired['Weapon System'] = payloadData[i].weapon_system;
                    breachingFired['Weapon system Other1'] = payloadData[i].weapon_system_other;
                    breachingFired['WFL Weapons Fired ID1'] = payloadData[i].wfl_weapons_fired_id;
                    breachingFired['Breaching umbc'] = payloadData[i].breaching_umbc;
                    breachingFired['Breaching sof'] = payloadData[i].breaching_sof;
                    breachingFired['Breaching Cust Conf Desc'] = payloadData[i].breaching_cust_conf_desc;
                    breachingFired['Breaching Cust Weight'] = payloadData[i].breaching_cust_weight;
                    breachingFired['Breaching Wall Type'] = payloadData[i].breaching_wall_type;
                    breachingFired['Breaching Door Type'] = payloadData[i].breaching_door_type;
                    breachingFired['Breaching Target Door'] = payloadData[i].breaching_target_door;
                    breachingFired['Breaching Target Wall'] = payloadData[i].breaching_target_wall;
                    breachingFired['Breaching Target Other'] = payloadData[i].breaching_target_other;
                    breachingFired['Target Other Details'] = payloadData[i].target_other_details;
                    if (newTableRows[breachingCount - 1]) {
                        newTableRows[breachingCount -1].pairs.push(...SmartabaseApi.getFormattedObject(breachingFired));
                    } else {
                        newTableRows.push({
                            "pairs": SmartabaseApi.getFormattedObject(breachingCount),
                            "row": (breachingCount)
                        })
                    }
                }
                breachingCount++;
            }
            // Populates the Smartabase table Other Weapons Fired Table based upon the weapon system type
            else {                
                if (otherCount === 0) {
                    wfl['Weapon System Type2'] = payloadData[i].weapon_system_type;
                    wfl['Weapon System2'] = payloadData[i].weapon_system;
                    wfl['Weapon system Other'] = payloadData[i].weapon_system_other;
                    wfl['WFL Weapons Fired ID'] = payloadData[i].wfl_weapons_fired_id;
                    wfl['Rounds Fired'] = payloadData[i].rounds_fired;
                    wfl['Body Position'] = payloadData[i].body_position;
                    wfl['Crew Position'] = payloadData[i].crew_position;
                    wfl['Enclosed Space'] = payloadData[i].enclosed_space;
                    wfl['Weapons Mod'] = payloadData[i].weapons_mod;
                    wfl['Munition Type'] = payloadData[i].munition_type;
                    wfl['Munition Type Other'] = payloadData[i].munition_type_other;
                    wfl['Other Details'] = payloadData[i].other_details;
                    wfl['Proximity to Source'] = payloadData[i].proximity_to_source;
                    wfl['Stacking Position'] = payloadData[i].stacking_position;
                    wfl['Under Water'] = payloadData[i].under_water;
                }
                else {
                    otherFired['Weapon System Type2'] = payloadData[i].weapon_system_type;
                    otherFired['Weapon System2'] = payloadData[i].weapon_system;
                    otherFired['Weapon system Other'] = payloadData[i].weapon_system_other;
                    otherFired['WFL Weapons Fired ID'] = payloadData[i].wfl_weapons_fired_id;
                    otherFired['Rounds Fired'] = payloadData[i].rounds_fired;
                    otherFired['Body Position'] = payloadData[i].body_position;
                    otherFired['Crew Position'] = payloadData[i].crew_position;
                    otherFired['Enclosed Space'] = payloadData[i].enclosed_space;
                    otherFired['Weapons Mod'] = payloadData[i].weapons_mod;
                    otherFired['Munition Type'] = payloadData[i].munition_type;
                    otherFired['Munition Type Other'] = payloadData[i].munition_type_other;
                    otherFired['Other Details'] = payloadData[i].other_details;
                    otherFired['Proximity to Source'] = payloadData[i].proximity_to_source;
                    otherFired['Stacking Position'] = payloadData[i].stacking_position;
                    otherFired['Under Water'] = payloadData[i].under_water;
                    if (newTableRows[otherCount - 1]) {
                        newTableRows[otherCount -1].pairs.push(...SmartabaseApi.getFormattedObject(otherFired));
                    } else {
                        newTableRows.push({
                            "pairs": SmartabaseApi.getFormattedObject(otherFired),
                            "row": (otherCount)
                        })
                    }
                }
                otherCount++;
            }            
        }

        let rows = [
            {
                "pairs": SmartabaseApi.getFormattedObject(wfl),
                "row": 0
            }
        ]

        newTableRows.forEach(element => {
            rows.push(element)    
        });

        let payload = 
        {
            "formName": form,
            "startDate": SmartabaseApi.getFormattedDate(startDateTime),
            "startTime": SmartabaseApi.getFormattedTime(startDateTime),
            "finishDate": SmartabaseApi.getFormattedDate(toDateTime),
            "finishTime": SmartabaseApi.getFormattedTime(toDateTime),
            "userId": {"userId" : this.userId},
            "enteredByUserId": this.userId,
            "rows": rows			 
        };

        return this._send('eventimport', payload);
    }

    /**
     * Call the event search endpoint.
     * 
     * @param {string | array} form 
     *     a string indicating the form, or an array of forms, to search.
     * @param {string | array} user 
     *     A string ndicating the user, or an array of users. 
     *     @TODO Should this be the current user and not an option? 
     * @param {string} search 
     *     Search. Defaults to "ALL"
     * @returns 
     */
    async eventsearch(form, user, search)
    {
        search = search || "ALL";
        form = Array.isArray(form) ? form : [form];
        user = Array.isArray(user) ? user : [user];

        return this._send('eventsearch', {
            formNames: form,
            userIds: user,
            search: search
        });

    }

    /**
    * Take a JHRM Datetime object and get a formatted Smartabase date.
    * 
    * Example response: 16/02/2021
    * 
    * @param {Datetime} date 
    */
    static getFormattedDate(date)
    {
        return date && date.date && !isNaN(date.date.getTime()) ? date.date.getUTCDate().toString().padStart(2, '0') + "/"
            + (date.date.getUTCMonth() + 1).toString().padStart(2, '0') + "/"
            + date.date.getUTCFullYear() : null;
    }

    /**
     * Take a JHRM Datetime object and get a formatted Smartabase time:
     * 
     * Example response: 01:45 PM
     * 
     * Note that this rounds times down to the previous 15 minute interval. This
     * is to prevent rounding from changing the hour. 
     * 
     * @param {Datetime} date 
     */
    static getFormattedTime(date)
    {
        if(date && date.date && !isNaN(date.date.getTime()))
        {
            let hours = date.date.getUTCHours() % 12;
            let ampm = hours < 12 ? 'AM' : 'PM';
            hours = hours % 12;
            hours = hours || 12;
            return hours.toString().padStart(2, '0') + ":"
                + (Math.floor(date.date.getUTCMinutes()/15)*15).toString().padStart(2, '0')
                + " " + ampm;
        }
        else 
        {
            return null;
        }
    } 

    /**
     * Check if a value is an empty type that should not be sent to SmartaBase
     * 
     * @param {*} value 
     * @returns {boolean}
     */
    static isEmpty(value)
    {
        return (typeof value === "undefined" || value === null || value.length == 0);
    }

    /**
     * Take an object and format it into an array of key value pairs as required
     * by Smartabase. 
     * 
     * Example input: 
     * 
     * {
     *     name : "Tom",
     *     date : "01/01/2001"
     * }
     * 
     * Example Output: 
     * 
     * [
     *     {
     *         key : "name",
     *         value : "Tom"
     *     },
     *     {
     *         key : "date",
     *         value : "01/01/2001"
     *     }
     * ]
     * 
     * Beware: This does not detect or handle nested objects! 
     * 
     * @param {object} data 
     * @returns {array}
     */
    static getFormattedObject(data)
    {
        let returnArray = [];
        for (const [innerKey, innerValue] of Object.entries(data))
        {
            if (!SmartabaseApi.isEmpty(innerValue))
            {   
                let returnArrayItem = {
                    key: innerKey,
                    value: innerValue.toString()
                };
                returnArray.push(returnArrayItem);       
            }
        }
        return returnArray;
    }

}
module.exports = new SmartabaseApi();
