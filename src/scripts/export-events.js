const db = require('../util/db');
const fs = require('fs');
const defaults = {
    output : "events.json",
    min : "",
    max : "",
    help : false
};

let config = Object.assign([], defaults);
if(process.argv.length > 2)
{
    let args = process.argv.slice(2);
    let keys = Object.keys(config);
    for(let i = 0; i < args.length; i++)
    {
        let key = args[i].replace(/-/g,'');
        if(keys.find(k=>k==key))
        {
            if((typeof config[key] === 'string') && args.length > i+1)
            {
                config[key] = args[i+1];
                i++;
            }
            else
            {
                config[key] = true;
            }
        }
        else 
        {
            config["output"] = args[i];
        }
    }
}

// If asking for help, do not do anything else. 
if(!config.help)
{
    run();
}
else 
{
    console.log(`
-------------------------------------------------------------------------------
This tool will generate a json file containing events from the bgd_* tables, 
in a format that will allow them to be imported without colliding ids when 
imported with the import-events.js script. This is not a compact or minimal
export format, but allows importing when pure SQL would require a lot of manual
ID updates. 
-------------------------------------------------------------------------------

Options: 

--min: Optional lower bound of bgd_event.id ids to export. 
--max: Optional upper bound of bgd_event.id ids to export. 

Usage: 

# node ./src/scripts/export-events.js --min 0 --max 100 old-events.json

`);
}

async function run()
{
    let event_query = db("bgd_event");
    if(config.min)
    {
        event_query.where("id", ">=", config.min);
    }
    if(config.max)
    {
        event_query.where("id", "<=", config.max);
    }
    let events = await event_query.select();

    for(let i = 0; i < events.length ; i++)
    {
        // Get personnel 
        let personnel = await db("bgd_personnel").where('id', events[i].bgd_personnel_id);
        events[i].PERSONNEL = personnel[0];

        // Get device data 
        events[i].DEVICE_DATA = await db("bgd_device_data").where('bgd_event_id', events[i].id);

        // Get devices and raw data
        for(var j = 0 ; j <  events[i].DEVICE_DATA.length ; j++)
        {
            let dd = await db("bgd_device").where('id',  events[i].DEVICE_DATA[j].bgd_device_id);
            events[i].DEVICE_DATA[j].DEVICE = dd[0];
            events[i].DEVICE_DATA[j].RAW = await db("bgd_event_raw_data").where('bgd_device_data_id',  events[i].DEVICE_DATA[j].id);
        }
    }

    fs.writeFile(config.output, JSON.stringify(events), function(err) {
        if (err) {
            console.log(err);
        }
        console.log("Complete!");
        process.exit();
    });


}