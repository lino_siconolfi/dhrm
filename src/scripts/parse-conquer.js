const db = require('../util/db');
const fs = require('fs');
var pty = require('node-pty');
const config = require('config');
const papa = require('papaparse');
const readXlsxFile = require('read-excel-file/node');
const defaults = {
    input : "",
    "dry-run" : false,
    "run-ara" : false,
    "skip-update-device" : false,
    "skip-update-personnel" : false,
    "skip-update-events" : false,
    "only-update-missing-ara" : false,
    help : false
};

let settings = Object.assign({}, defaults);
if(process.argv.length > 2)
{
    let args = process.argv.slice(2);
    let keys = Object.keys(settings);
    for(let i = 0; i < args.length; i++)
    {
        let key = args[i].replace(/^-+/,'');
        if(keys.find(k=>k==key))
        {
            if((typeof settings[key] === 'string') && args.length > i+1)
            {
                settings[key] = args[i+1].replaceAll('"','');
                i++;
            }
            else
            {
                settings[key] = true;
            }
        }
        else 
        {
            settings["input"] = args[i];
        }
    }
}

// If asking for help, do not do anything else. 
if(!settings.help)
{
    run(settings);
}
else 
{
    console.log(`
-------------------------------------------------------------------------------
This tool will import a JSON file of blast gauge events and associated data into
the database bgd_* tables. 
-------------------------------------------------------------------------------

Options: 

--dry-run: this will run all validation, but roll back the database transaction
    so no data is actually saved. 
--skip-update-device: Devices will be matched by serial number. If this option is 
    set, an existing device row will not be updated with the values in the import. 
--skip-update-personnel: Personnel will be matched by dod_id / EDIPI. If this option
    is set, an existing personnel row will not be udpated with the values in the import. 
--skip-update-events: Events will be matched based on external event id, or if that
    is not available, on personnel id and date. If this flag is set, records 
    in device_data and raw_data will also be updated. 
--only-update-missing-ara Do not attempt to validate edipi / data validity
--run-ara Attempt to run the ARA algorithm. Note that Java must be installed for 
    this to work.


Usage: 

# node ./src/scripts/parse-conquer.js --run-ara  ~/path/to/folder/or/file

`);
}

async function run(settings)
{
    if(settings.input.indexOf(".xlsx")!==-1)
    {
        // looks like a file. Try to treat it like a file. 
        console.log("Parsing single file");
        await parsefile(settings, settings.input);
        console.log("Complete!");
        process.exit();
    }
    else
    {
        fs.readdir(settings.input, async (err, filenames) =>
        {
            console.log("Parsing folder");
            if (err) 
            {
                console.error("Failed to parse folder")
            }

            for(let i = 0 ; i < filenames.length ; i++)
            {
                if(filenames[i].indexOf(".xlsx")!==-1 && filenames[i].indexOf("~$")===-1)
                {
                    console.log(`Parsing file ${filenames[i]}`);
                    await parsefile(settings, settings.input + filenames[i]);
                }
            }
            console.log("Complete!");
            process.exit();
        });
    }

   
}

async function parsefile(settings, input)
{
    try 
    {
        await db.transaction(async db => 
        {
            let xlsx_sheets = await readXlsxFile(input, { getSheets: true }).then(sheets=>sheets.map(item=>item.name));
            let xlsx_summary = await readXlsxFile(input, { sheet: xlsx_sheets[0] });

            // Extract information
            let edipi = validate_edipi(xlsx_summary[3][1]);
            let expectedEvents = xlsx_summary.find(row=>row[0]=="Total Number of Events")[1];
            let events = xlsx_sheets.filter(val=>val.indexOf("Event")===0).map(item=>{
                return {
                    sheet : item,
                    jhrm_id : null
                };
            }); 
            if(events.length != expectedEvents)
            {
                console.log(`WARNING: Mismatch in expected number of events. Found ${events.length} expected ${expectedEvents}.`);
            }

            // Check to see if there is a matching personnel record
            let jhrm_personnel_id = null;
            let personnelQuery = await db("bgd_personnel").where("dod_id", edipi);
            if(personnelQuery.length)
            {
                console.log(`Personnel record found for edipi ${edipi}`);
                jhrm_personnel_id = personnelQuery[0].id;

                // Update the personnel record if necessary
                if(!settings['skip-update-personnel'])
                {
                    let update = {
                        display_name : xlsx_summary[1][1],
                        unit_name : xlsx_summary[2][1]
                    };
                    await db('bgd_personnel').update(update).where("id", jhrm_personnel_id);
                    console.log(`Personnel record updated for edipi ${edipi}`);
                }
            }
            else 
            {
                // Insert personnel record
                let insert = {
                    display_name : xlsx_summary[1][1],
                    unit_name : xlsx_summary[2][1],
                    dod_id : edipi
                };
                let result = await db('bgd_personnel').insert(insert);
                jhrm_personnel_id = result;
                console.log(`Personnel record created for edipi ${edipi}`);
            }


            // Create devices
            let devices = [];
            let device_tabs = xlsx_sheets.filter(val=>val.indexOf("Devices -")===0);
            for(let i = 0 ; i < device_tabs.length ; i++)
            {
                let xlsx_device_tab = await readXlsxFile(input, { sheet: device_tabs[i] });
                for(let j = 0 ; j < xlsx_device_tab[0].length ; j++)
                {
                    let match = xlsx_device_tab[0][j].match(/\((.*)\)/);
                    if(match)
                    {
                        let device = {
                            sn : match[1],
                            location : xlsx_device_tab[2][j],
                            jhrm_id : null          
                        };
                        devices.push(device);
                        let deviceQuery = await db("bgd_device").where("iss_device_serial_number", device.sn);
                        if(deviceQuery.length)
                        {
                            console.log(`Device record found for serial number ${device.sn}`);
                            device.jhrm_id = deviceQuery[0].id;

                            // Update device is necessary
                            if(!settings['skip-update-device'])
                            {
                                let update = {
                                    bgd_personnel_id : jhrm_personnel_id,
                                    uid : xlsx_device_tab[1][j],
                                    iss_device_serial_number : device.sn,
                                    mounting_location : device.location,
                                    activation_date : new Date(xlsx_device_tab[4][j]),
                                    firmware_rev : xlsx_device_tab[3][j],
                                    battery_level : parseInt(xlsx_device_tab[5][j], 10),
                                    remaining_life : xlsx_device_tab[6][j],
                                    end_of_life_reached : xlsx_device_tab[13][j]=="No" ? 0 : 1,
                                    setup_mode : xlsx_device_tab[16][j],
                                    wireless_state : xlsx_device_tab[17][j],
                                    summary_pressure_thresh : Number(xlsx_device_tab[18][j]).toFixed(2),
                                    moderate_pressure_thresh : Number(xlsx_device_tab[19][j]).toFixed(2),
                                    severe_pressure_thresh : Number(xlsx_device_tab[20][j]).toFixed(2)
                                };
                                await db("bgd_device").update(update).where("id", device.jhrm_id);
                                console.log(`Device record updated for serial number ${device.sn}`);
                            }
                        }
                        else 
                        {
                            // Insert device
                            let insert = {
                                bgd_personnel_id : jhrm_personnel_id,
                                uid : xlsx_device_tab[1][j],
                                iss_device_serial_number : device.sn,
                                mounting_location : device.location,
                                activation_date : new Date(xlsx_device_tab[4][j]),
                                firmware_rev : xlsx_device_tab[3][j],
                                battery_level : parseInt(xlsx_device_tab[5][j], 10),
                                remaining_life : xlsx_device_tab[6][j],
                                end_of_life_reached : xlsx_device_tab[13][j]=="No" ? 0 : 1,
                                setup_mode : xlsx_device_tab[16][j],
                                wireless_state : xlsx_device_tab[17][j],
                                summary_pressure_thresh : Number(xlsx_device_tab[18][j]).toFixed(2),
                                moderate_pressure_thresh : Number(xlsx_device_tab[19][j]).toFixed(2),
                                severe_pressure_thresh : Number(xlsx_device_tab[20][j]).toFixed(2)
                            };
                            let result = await db("bgd_device").insert(insert);
                            device.jhrm_id = result;
                            console.log(`Device record created for serial number ${device.sn}`);
                        }
                    }
                }
            }             

            // Create events
            for( let i = 0 ; i < events.length ; i ++)
            {
                let xlsx_event = await readXlsxFile(input, { sheet: events[i].sheet });
                let event_date = getEventDate(events[i].sheet );
                let event_devices = devices.filter(device=>xlsx_event[1].includes(device.sn));

                // Match events on personnel and time
                let eventQuery = await db("bgd_event")
                    .where('bgd_personnel_id', jhrm_personnel_id)
                    .where('event_date_time', event_date);
                if(eventQuery.length && eventQuery[0].external_event_id==events[i].sheet)
                {
                    console.log(`Event record found for personnel id ${jhrm_personnel_id} and time ${event_date}`);
                    events[i].jhrm_id = eventQuery[0].id;

                    if(!settings['skip-update-events'])
                    {
                        let update = {
                            download_date : new Date()
                        };
                        await db("bgd_event").update(update).where("id", events[i].jhrm_id);
                        console.log(`Event record updated for personnel id ${jhrm_personnel_id} and time ${event_date}`);
                    }
                }
                else 
                {
                    let insert = {
                        bgd_device_id : devices[0].jhrm_id, // Dummy, but needs to be something. 
                        bgd_personnel_id : jhrm_personnel_id,
                        event_date_time : event_date,
                        download_date : new Date(),
                        external_event_id : events[i].sheet
                    };
                    let result = await db("bgd_event").insert(insert);
                    events[i].jhrm_id = result;
                    console.log(`Event record created for personnel id ${jhrm_personnel_id} and time ${event_date}`);
                }

                // Now for each device that is part of the event. 
                for(let j = 0 ; j < event_devices.length ; j++)
                {
                    let jhrm_device_data_id = null;
                    let device_data_index = xlsx_event[1].indexOf(event_devices[j].sn);
                    let raw_data_exists = xlsx_event[6][device_data_index]=="True" ? 1 : 0;

                    let deviceDataQuery = await db("bgd_device_data")
                        .where("bgd_event_id", events[i].jhrm_id)
                        .where("bgd_device_id", event_devices[j].jhrm_id);
                    if(deviceDataQuery.length)
                    {
                        console.log(`Device Data record found for event id ${events[i].jhrm_id} and device id ${event_devices[j].jhrm_id}`);
                        jhrm_device_data_id = deviceDataQuery[0].id;
                        
                        if(!settings['skip-update-events'])
                        {
                            let update = {
                                bgd_event_id : events[i].jhrm_id,
                                bgd_device_id : event_devices[j].jhrm_id,
                                peak_overpressure : Number(xlsx_event[3][device_data_index]).toFixed(2),
                                total_positive_impulse : Number(xlsx_event[4][device_data_index]).toFixed(2),
                                acceleration_peak_magnitude : Number(xlsx_event[5][device_data_index]).toFixed(2),
                                raw_data_exists : raw_data_exists
                            };
                            await db("bgd_device_data").update(update).where("id", jhrm_device_data_id);
                            console.log(`Device Data record updated for event id ${events[i].jhrm_id} and device id ${event_devices[j].jhrm_id}`);
                        }
                    }
                    else 
                    {
                        let insert = {
                            bgd_event_id : events[i].jhrm_id,
                            bgd_device_id : event_devices[j].jhrm_id,
                            peak_overpressure : Number(xlsx_event[3][device_data_index]).toFixed(2),
                            total_positive_impulse : Number(xlsx_event[4][device_data_index]).toFixed(2),
                            acceleration_peak_magnitude : Number(xlsx_event[5][device_data_index]).toFixed(2),
                            raw_data_exists : raw_data_exists
                        };
                        let result = await db("bgd_device_data").insert(insert);
                        jhrm_device_data_id = result;
                        console.log(`Device Data record created for event id ${events[i].jhrm_id} and device id ${event_devices[j].jhrm_id}`);
                    }

                    if(raw_data_exists)
                    {
                        let raw_data_index = device_data_index-1;
                        let insert = true;
                        let rawQuery = await db("bgd_event_raw_data").where("bgd_device_data_id", jhrm_device_data_id);
                        if(rawQuery.length)
                        {
                            insert = false;
                            console.log(`Existing raw data found for device data id ${jhrm_device_data_id}`);
                            if(!settings['skip-update-events'])
                            {
                                insert = true;
                                await db("bgd_event_raw_data").where("bgd_device_data_id", jhrm_device_data_id).del();
                                console.log(`Existing raw data deleted for device data id ${jhrm_device_data_id}`);
                            }
                        }
                        let data = xlsx_event.slice(10,2026).map(row=>{

                            if(row[raw_data_index]===null)
                            {
                                throw new Error("Invalid data.");
                            }
                            return {
                                bgd_device_data_id : jhrm_device_data_id,
                                time : Number(row[0]).toFixed(2),
                                overpressure : Number(row[raw_data_index]).toFixed(4),
                                bgd_event_id : events[i].jhrm_id
                            };
                        });
                        if(insert)
                        {
                            await db("bgd_event_raw_data").insert(data);
                            console.log(`Raw data added for device data id ${jhrm_device_data_id}`);
                        }
                        if(insert || (settings["only-update-missing-ara"] && deviceDataQuery.length && deviceDataQuery[0].real_notreal_flag===null))
                        {
                            if(settings["run-ara"])
                            {
                                try 
                                {
                                    let ara = await getAra(data);
                                    let flag = 'ERROR';
                                    if(ara.indexOf("not_flagged") !== -1)
                                    {
                                        flag = 'REAL';
                                    }
                                    else if(ara.indexOf("flagged") !== -1)
                                    {
                                        flag = 'NOTREAL';
                                    }
                                    else 
                                    {
                                        console.log(ara);
                                    }
                                    await db("bgd_device_data").update({
                                        'real_notreal_flag' : flag
                                    }).where("id", jhrm_device_data_id);
                                    console.log(`ARA output is ${flag}`)
                                }
                                catch(e)
                                {
                                    console.log(e);
                                    console.log("ARA Error");
                                }
                            }
                        }
                    }
                }
            }
            if(settings['dry-run'])
            {
                console.log("Dry run: rolling back transaction.")
                await db.rollback();
            }
            else 
            {
                console.log("Committing transaction.")
                await db.commit();
            }
        });
    }catch(e)
    {
        console.log("Transaction rolled back.")
        throw e;
    }
}

/**
 * todo 
 * @param {*} edipi 
 * @returns 
 */
function validate_edipi(edipi)
{
    if((typeof edipi == 'string') && edipi.indexOf("CODID-") !== 0)
    {
        console.log(`WARNING: Invalid edipi '${edipi}'`);
    }
    return edipi;
}

function getEventDate(string)
{
    let p = string.match(/\((.*?)\)?$/)[1].split(' ');
    return new Date(`${p[0]} ${p[1]} ${p[2]} ${p[3]}:${p[4]}:${p[5]}`)
}


var araHandle;
var araResolve;
var buffer = "";
async function getAra(data)
{
    if(!araHandle)
    {
        araHandle = pty.spawn('bash', [], {
            name: 'xterm-color',
            cols: 80,
            rows: 30,
            cwd: process.cwd(),
            env: process.env
        });
        araHandle.write('java -jar ./bin/blastfilter.jar\n');

        await (new Promise((res)=>{setTimeout(()=>{res()},500)}));
        araHandle.on('data', (data)=>{
            if(araResolve)
            {
                buffer = buffer + data;
                if(buffer.indexOf("type 'Q' to exit") !== -1)
                {
                    araResolve(buffer);
                    araResolve = null;
                    buffer="";
                }
            }
        });
    }
        
    fs.writeFileSync('./tmp.csv', papa.unparse(data.map(item=>{
        return {
            time : item.time,
            overpressure : item.overpressure
        }
    })));

    return new Promise((resolve, reject)=>
    {
        araResolve = resolve;
        araHandle.write('./tmp.csv\n');
    });
}