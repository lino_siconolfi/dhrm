const db = require('../util/db');
const fs = require('fs');
const { BAD_DELIMITERS } = require('papaparse');
const { update } = require('../util/db');
const defaults = {
    input : "",
    "device-identifier" : "gauge_serial_number",
    "update-device" : false,
    "update-personnel" : false,
    "update-events" : false,
    "issuance-inventory" : false,
    "issuance-issue" : "",
    help : false
};
var deviceIdMap = [];
let config = Object.assign({}, defaults);
if(process.argv.length > 2)
{
    let args = process.argv.slice(2);
    let keys = Object.keys(config);
    for(let i = 0; i < args.length; i++)
    {
        let key = args[i].replace(/^-+/,'');
        if(keys.find(k=>k==key))
        {
            if((typeof config[key] === 'string') && args.length > i+1)
            {
                config[key] = args[i+1];
                i++;
            }
            else
            {
                config[key] = true;
            }
        }
        else 
        {
            config["input"] = args[i];
        }
    }
}

// If asking for help, do not do anything else. 
if(!config.help)
{
    run();
}
else 
{
    console.log(`
-------------------------------------------------------------------------------
This tool will import a JSON file of blast gauge events and associated data into
the database bgd_* tables. 
-------------------------------------------------------------------------------

Options: 

--device-identifier: There are three different ways to identify a device, the 
    uid, gauge_serial_number, and iss_device_serial_number. This setting allows
    you to specify which one to use. Note that it can cause problems if you get 
    this setting wrong. The default is gauge_serial_number. 
--issuance-inventory: Make sure there the device is added to the issuance tool 
    inventory. 
--issuance-issue: Set to a unit name, and if the device is not already in the 
    issuance inventory, it will also create a fake personnel record and issue 
    the sensor to that person. 
--update-device: Devices will be matched by serial number. If this option is 
    set, the device row will be updated with the values in the import. 
--update-personnel: Personnel will be matched by dod_id / EDIPI. If this option
    is set, the personnel row will be udpated with the values in the import. 
--update-events: Events will be matched based on external event id, or if that
    is not available, on personnel id and date. If this flag is set, records 
    in device_data and raw_data will also be updated. 

Usage: 

# node ./src/scripts/import-events.js --update-events events.json

`);
}

async function run()
{
    if(!config.input)
    {
        console.log("File is required.");
        process.exit(1);
    }
    var events = JSON.parse(fs.readFileSync(config.input).toString());

    for(let i = 0 ; i < events.length ; i++)
    {
        let event = events[i];
        console.log(`Importing event ${i+1} of ${events.length}`);

        // Check to see if there is a matching personnel record
        let personnelQuery = await db("bgd_personnel").where("dod_id", event.PERSONNEL.dod_id);
        if(personnelQuery.length)
        {
            console.log(`Personnel record found for dodid ${event.PERSONNEL.dod_id}`);
            event.PERSONNEL.id = personnelQuery[0].id;

            // Update the personnel record if necessary
            if(config['update-personnel'])
            {
                let update = Object.assign({}, event.PERSONNEL);
                delete update.id;
                await db('bgd_personnel').update(update).where("id", event.PERSONNEL.id);
                console.log(`Personnel record updated for dodid ${event.PERSONNEL.dod_id}`);
            }
        }
        else 
        {
            // Insert personnel record
            let insert = Object.assign({}, event.PERSONNEL);
            delete insert.id;
            let result = await db('bgd_personnel').insert(insert);
            event.PERSONNEL.id = result;
            console.log(`Personnel record created for dodid ${event.PERSONNEL.dod_id}`);
        }
        event.bgd_personnel_id = event.PERSONNEL.id;
        event.DEVICE_DATA.forEach(device_data => {
            device_data.DEVICE.bgd_personnel_id = event.PERSONNEL.id;
        });

        // Create Devices
        for(let j = 0 ; j < event.DEVICE_DATA.length ; j++)
        {
            let DEVICE_DATA = event.DEVICE_DATA[j];
            DEVICE_DATA.DEVICE.activation_date = new Date(DEVICE_DATA.DEVICE.activation_date);
            let deviceQuery = await db("bgd_device").where(config['device-identifier'], DEVICE_DATA.DEVICE[config['device-identifier']]);
            if(deviceQuery.length)
            {
                console.log(`Device record found for ${config['device-identifier']} ${DEVICE_DATA.DEVICE[config['device-identifier']]}`);
                DEVICE_DATA.DEVICE.id = deviceQuery[0].id;

                // Update device is necessary
                if(config['update-device'])
                {
                    let update = Object.assign({}. DEVICE_DATA.DEVICE);
                    delete update.id;
                    await db("bgd_device").update(update).where("id", DEVICE_DATA.DEVICE.id);
                    console.log(`Device record updated for ${config['device-identifier']} ${DEVICE_DATA.DEVICE[config['device-identifier']]}`);
                }
            }
            else 
            {
                // Insert device
                let insert = Object.assign({}, DEVICE_DATA.DEVICE);
                delete insert.id;
                let result = await db("bgd_device").insert(insert);
                DEVICE_DATA.DEVICE.id = result;
                console.log(`Device record created for ${config['device-identifier']} ${DEVICE_DATA.DEVICE[config['device-identifier']]}`);
            }
            DEVICE_DATA.bgd_device_id = DEVICE_DATA.DEVICE.id;

            if(config['issuance-issue'] || config['issuance-inventory'])
            {
                let issuanceDeviceQuery = await db("iss_device")
                    .where("iss_device_type_id", 1)
                    .where("serial_number", DEVICE_DATA.DEVICE.iss_device_serial_number);
                if(issuanceDeviceQuery.length)
                {
                    console.log(`Device with serial number ${DEVICE_DATA.DEVICE.iss_device_serial_number} already exists in issuance tool inventory.`)
                }
                else 
                {
                    let iss_device_id = await db("iss_device").insert({
                        "iss_device_type_id" : 1,
                        "serial_number" : DEVICE_DATA.DEVICE.iss_device_serial_number,
                        "status" : "SERVICEABLE",
                        "initialization_date" : DEVICE_DATA.DEVICE.activation_date
                    });
                    await db("iss_device_blast").insert({
                        iss_device_id : iss_device_id,
                        device_type : DEVICE_DATA.DEVICE.mounting_location
                    });
                    console.log(`Device with serial number ${DEVICE_DATA.DEVICE.iss_device_serial_number} added to issuance tool inventory.`)

                    if(config['issuance-issue'])
                    {
                        if(!deviceIdMap[DEVICE_DATA.DEVICE.id])
                        {
                            event.DEVICE_DATA.forEach(item=>{
                                if(deviceIdMap[item.DEVICE.id])
                                {
                                    deviceIdMap[DEVICE_DATA.DEVICE.id] = deviceIdMap[item.DEVICE.id];
                                }
                            });
                            if(!deviceIdMap[DEVICE_DATA.DEVICE.id])
                            {
                                deviceIdMap[DEVICE_DATA.DEVICE.id] = await db("iss_personnel").insert({
                                    first_name : name(),
                                    last_name : name(),
                                    dodid : "00000" + Math.floor(Math.random()*(99999-10000)+10000).toString(),
                                    unit : config['issuance-issue']
                                });
                            }
                        }
                        
                        let last_issued_event_id = await db("iss_event").insert({
                            created_by : "Automatic Import",
                            created_on : new Date(),
                            iss_device_id : iss_device_id,
                            iss_personnel_id: deviceIdMap[DEVICE_DATA.DEVICE.id],
                            date : new Date(1),
                            wear_location : DEVICE_DATA.DEVICE.mounting_location,
                            type : "ISSUED", 
                            device_status : "SERVICEABLE"
                        });
                        await db("iss_device").update({
                            last_issued_event_id : last_issued_event_id
                        }).where("id", iss_device_id);
                        console.log(`Device with serial number ${DEVICE_DATA.DEVICE.iss_device_serial_number} issued.`)
                    }
                }
            }

            // Only after the first device, create or update the event. 
            if(j===0)
            {
                event.bgd_device_id = DEVICE_DATA.DEVICE.id;
                event.download_date = new Date(event.download_date);
                event.event_date_time = new Date(event.event_date_time);

                // Match events on personnel and time
                let eventQuery = await db("bgd_event")
                    .where('bgd_personnel_id', event.bgd_personnel_id)
                    .where('event_date_time', event.event_date_time);
                if(eventQuery.length)
                {
                    console.log(`Event record found for personnel id ${event.bgd_personnel_id} and time ${event.event_date_time}`);
                    event.id = eventQuery[0].id;

                    if(config['update-events'])
                    {
                        let update = Object.assign({}, event);
                        delete update.PERSONNEL;
                        delete update.DEVICE_DATA;
                        delete update.id;
                        await db("bgd_event").update(update).where("id", event.id);
                        console.log(`Event record updated for personnel id ${event.bgd_personnel_id} and time ${event.event_date_time}`);
                    }
                }
                else 
                {
                    let insert = Object.assign({}, event);
                    delete insert.PERSONNEL;
                    delete insert.DEVICE_DATA;
                    delete insert.id;
                    let result = await db("bgd_event").insert(insert);
                    event.id = result;
                    console.log(`Event record created for personnel id ${event.bgd_personnel_id} and time ${event.event_date_time}`);
                }
                event.DEVICE_DATA.forEach(device_data => {
                    device_data.bgd_event_id = event.id;
                }); 
            }

            // Now continue updating the device data and raw data. 
            let deviceDataQuery = await db("bgd_device_data")
                .where("bgd_event_id", event.id)
                .where("bgd_device_id", DEVICE_DATA.DEVICE.id);
            if(deviceDataQuery.length)
            {
                console.log(`Device Data record found for event id ${event.id} and device id ${DEVICE_DATA.DEVICE.id}`);
                DEVICE_DATA.id = deviceDataQuery[0].id;
                
                if(config['update-events'])
                {
                    let update = Object.assign({}, DEVICE_DATA);
                    delete update.DEVICE;
                    delete update.RAW;
                    delete update.id;
                    await db("bgd_device_data").update(update).where("id", DEVICE_DATA.id);
                    console.log(`Device Data record updated for event id ${event.id} and device id ${DEVICE_DATA.DEVICE.id}`);
                }
            }
            else 
            {
                let insert = Object.assign({}, DEVICE_DATA);
                delete insert.DEVICE;
                delete insert.RAW;
                delete insert.id;
                let result = await db("bgd_device_data").insert(insert);
                DEVICE_DATA.id = result;
                console.log(`Device Data record created for event id ${event.id} and device id ${DEVICE_DATA.DEVICE.id}`);
            }
            DEVICE_DATA.RAW.forEach(raw=>{
                raw.bgd_device_data_id = DEVICE_DATA.id;
                raw.bgd_event_id = event.id;
            });

            let rawQuery = await db("bgd_event_raw_data").where("bgd_device_data_id", DEVICE_DATA.id);
            let insert = true;
            if(rawQuery.length)
            {
                insert = false;
                console.log(`Raw data found for device data id ${DEVICE_DATA.id}`);
                if(config['update-events'])
                {
                    insert = true;
                    console.log(`Raw data deleted for device data id ${DEVICE_DATA.id}`);
                    await db("bgd_event_raw_data").where("bgd_device_data_id", DEVICE_DATA.id).del();
                }
            }
            if(insert)
            {
                await db("bgd_event_raw_data").insert(DEVICE_DATA.RAW.map(item=>{
                    delete item.id;
                    return item;
                }));
                console.log(`Raw data added for device data id ${DEVICE_DATA.id}`);
            }
        }
    }
    console.log("Complete!");
    process.exit();
}

function name(){
    var names = [  "Abbott", "Abernathy", "Abshire", "Adams", "Altenwerth", "Anderson", 
    "Ankunding", "Armstrong", "Auer", "Aufderhar", "Bahringer", "Bailey", "Balistreri", 
    "Barrows", "Bartell", "Bartoletti", "Barton", "Bashirian", "Batz", "Bauch", 
    "Baumbach", "Bayer", "Beahan", "Beatty", "Bechtelar", "Becker", "Bednar", "Beer", 
    "Beier", "Berge", "Bergnaum", "Bergstrom", "Bernhard", "Bernier", "Bins", "Blanda", 
    "Blick", "Block", "Bode", "Boehm", "Bogan", "Bogisich", "Borer", "Bosco", "Botsford", 
    "Boyer", "Boyle", "Bradtke", "Brakus", "Braun", "Breitenberg", "Brekke", "Brown", 
    "Bruen", "Buckridge", "Carroll", "Carter", "Cartwright", "Casper", "Cassin", 
    "Champlin", "Christiansen", "Cole", "Collier", "Collins", "Conn", "Connelly", 
    "Conroy", "Considine", "Corkery", "Cormier", "Corwin", "Cremin", "Crist", "Crona", 
    "Cronin", "Crooks", "Cruickshank", "Cummerata", "Cummings", "Dach", "D'Amore", 
    "Daniel", "Dare", "Daugherty", "Davis", "Deckow", "Denesik", "Dibbert", "Dickens", 
    "Dicki", "Dickinson", "Dietrich", "Donnelly", "Dooley", "Douglas", "Doyle", 
    "DuBuque", "Durgan", "Ebert", "Effertz", "Emard", "Emmerich", "Erdman", "Ernser", 
    "Fadel", "Fahey", "Farrell", "Fay", "Feeney", "Feest", "Feil", "Ferry", 
    "Fisher", "Flatley", "Frami", "Franecki", "Friesen", "Fritsch", "Funk",
     "Gerhold", "Gerlach", "Gibson", "Gislason", "Gleason", "Gleichner", "Glover", 
     "Goldner", "Goodwin", "Gorczany", "Gottlieb", "Goyette", "Grady", "Graham", 
     "Grant", "Green", "Greenfelder", "Greenholt", "Grimes", "Gulgowski", "Gusikowski", 
     "Gutkowski", "Gutmann", "Haag", "Hackett", "Hagenes", "Hahn", "Haley", "Halvorson", 
     "Hamill", "Hammes", "Hand", "Hane", "Hansen", "Harber", "Harris", "Hartmann"];
  
    return names[Math.floor(Math.random()*(names.length-1))];
  }