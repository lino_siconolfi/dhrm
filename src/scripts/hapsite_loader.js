var knex = require('knex')({
    client: 'mysql',
    connection: {
      socketPath : '/tmp/mysql.sock',
      user : 'tom',
      database : 'jhrmecddb'
    }
  });
  
  var types = {"Blast Gauge":1, "Heat Stress Monitor":2,"JPDI":4,"Passive Chemical Sampler":3};
  
  
  
  var data = getData();
  data.forEach(sample => {
    knex('id_hapsite_sample').insert({
        method_type : sample.detail.hapsite.results.m_MethodType,
        method_state : sample.detail.hapsite.results.m_MethodState,
        method_name : sample.detail.hapsite.results.m_MethodName,
        scan_mode : sample.detail.hapsite.results.m_ScanMode,
        latitude : sample.detail.hapsite.point.lat,
        longitude : sample.detail.hapsite.point.lon,
        hae : sample.detail.hapsite.point.hae
    }).then(sampleId=>{
        sample.id = sampleId;
        sample.detail.hapsite.results.m_SearchResults.forEach(result=>{
            knex('id_hapsite_result').insert({
                id_hapsite_sample_id : sample.id,
                parameter : result.id,
                concentration : result.concentration,
                units : result.units, 
                purity : result.purity, 
                time : getTime(result.time),
                cas_number : result.casnum,
                alarm_level : result.alarmLevel,
                fit : result.fit
            }).then();
        });
    });
  });

  function getTime(input)
  {
      var [minutes, seconds] = input.split(':');
      return parseInt(minutes) * 60 + parseInt(seconds);
  }

function getData()
{
    return [
    {
       "version": "2.0",
       "uid": "ASD_uM06_HAPSITE_hapsite",
       "time": "2017-09-27T06:07:03.00Z",
       "start": "2017-09-27T06:07:03.00Z",
       "stale": "2017-09-27T06:07:03.00Z",
       "how": "m-g",
       "point": {
          "lat": "37.470400",
          "lon": "-121.940178",
          "hae": "-3.94",
          "ce": "32",
          "le": "32",
          "sat": "7"
       },
       "detail": {
          "_flow-tags_": {
             "ASD_uM06_HAPSITE": "2017-09-27T06:07:03.00Z",
             "TAKServer22e69e81": "2017-10-03T18:46:03Z"
          },
          "remarks": "09/27/17 06:07:03, stype=hapsite",
          "hapsite": {
             "version": "1.0",
             "fid": "1506485184180",
             "source": "H1778-70023384",
             "time": "2017-09-27T04:06:24",
             "point": {
                "lat": "37.4705",
                "lon": "-121.94",
                "hae": "5805.67",
                "sat": "8"
             },
             "results": {
                "m_MethodType": "HPS_MTHTYPE_SURVEY",
                "m_ScanMode": "HPS_MTH_SIM",
                "m_MethodState": "HPS_MTHSTATE_FINISHED",
                "m_MethodName": "/Haps/Method/Analyze/Concentrator/ER_Air_Tri-Bed_PPB_Quant.mth",
                "m_DataFileName": [],
                "m_SearchCount": "1",
                "m_SearchResults": [
                   {
                      "id": "Carbon Tetrachloride",
                      "concentration": "0.069",
                      "time": "01:42.3",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.581",
                      "purity": "0.175",
                      "area": "402039",
                      "endTime": "01:47.8",
                      "casnum": " 56-23-5"
                   },
                   {
                      "id": "Ethane, 1,1,2-trichloro-1,2,2-trifluoro-",
                      "concentration": "0",
                      "time": "00:56.2",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.761",
                      "purity": "0.135",
                      "area": "32124",
                      "endTime": "00:56.7",
                      "casnum": "76-13-1"
                   },
                   {
                      "id": "Benzene",
                      "concentration": "0.104",
                      "time": "01:39.9",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.997",
                      "purity": "0.888",
                      "area": "557635",
                      "endTime": "01:42.3",
                      "casnum": "71-43-2"
                   },
                   {
                      "id": "Tetrachloroethylene",
                      "concentration": "0",
                      "time": "04:09.5",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0",
                      "purity": "0",
                      "area": "0",
                      "endTime": "",
                      "casnum": "127-18-4"
                   }
                ]
             }
          }
       }
    },
    {
       "version": "2.0",
       "uid": "ASD_uM06_HAPSITE_hapsite",
       "time": "2017-09-27T06:07:04.00Z",
       "start": "2017-09-27T06:07:04.00Z",
       "stale": "2017-09-27T06:07:04.00Z",
       "how": "m-g",
       "point": {
          "lat": "37.470400",
          "lon": "-121.940178",
          "hae": "-3.94",
          "ce": "32",
          "le": "32",
          "sat": "7"
       },
       "detail": {
          "_flow-tags_": {
             "ASD_uM06_HAPSITE": "2017-09-27T06:07:04.00Z",
             "TAKServer22e69e81": "2017-10-03T18:46:31Z"
          },
          "remarks": "09/27/17 06:07:04, stype=hapsite",
          "hapsite": {
             "version": "1.0",
             "fid": "1506485184238",
             "source": "H1778-70023384",
             "time": "2017-09-27T04:06:24",
             "point": {
                "lat": "37.4705",
                "lon": "-121.94",
                "hae": "5805.67",
                "sat": "8"
             },
             "results": {
                "m_MethodType": "HPS_MTHTYPE_SURVEY",
                "m_ScanMode": "HPS_MTH_SIM",
                "m_MethodState": "HPS_MTHSTATE_FINISHED",
                "m_MethodName": "/Haps/Method/Analyze/Concentrator/ER_Air_Tri-Bed_PPB_Quant.mth",
                "m_DataFileName": [],
                "m_SearchCount": "1",
                "m_SearchResults": [
                   {
                      "id": "Carbon Tetrachloride",
                      "concentration": "0.069",
                      "time": "01:42.3",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.581",
                      "purity": "0.175",
                      "area": "402039",
                      "endTime": "01:47.8",
                      "casnum": " 56-23-5"
                   },
                   {
                      "id": "Ethane, 1,1,2-trichloro-1,2,2-trifluoro-",
                      "concentration": "0",
                      "time": "00:56.2",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.761",
                      "purity": "0.135",
                      "area": "32124",
                      "endTime": "00:56.7",
                      "casnum": "76-13-1"
                   },
                   {
                      "id": "Benzene",
                      "concentration": "0.104",
                      "time": "01:39.9",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.997",
                      "purity": "0.888",
                      "area": "557635",
                      "endTime": "01:42.3",
                      "casnum": "71-43-2"
                   },
                   {
                      "id": "Tetrachloroethylene",
                      "concentration": "0",
                      "time": "04:09.5",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0",
                      "purity": "0",
                      "area": "0",
                      "endTime": "",
                      "casnum": "127-18-4"
                   }
                ]
             }
          }
       }
    },
    {
       "version": "2.0",
       "uid": "ASD_uM06_HAPSITE_hapsite",
       "time": "2017-09-27T06:07:03.00Z",
       "start": "2017-09-27T06:07:03.00Z",
       "stale": "2017-09-27T06:07:03.00Z",
       "how": "m-g",
       "point": {
          "lat": "37.470400",
          "lon": "-121.940178",
          "hae": "-3.94",
          "ce": "32",
          "le": "32",
          "sat": "7"
       },
       "detail": {
          "_flow-tags_": {
             "ASD_uM06_HAPSITE": "2017-09-27T06:07:03.00Z",
             "TAKServer22e69e81": "2017-10-03T18:46:30Z"
          },
          "remarks": "09/27/17 06:07:03, stype=hapsite",
          "hapsite": {
             "version": "1.0",
             "fid": "1506485184180",
             "source": "H1778-70023384",
             "time": "2017-09-27T04:06:24",
             "point": {
                "lat": "37.4705",
                "lon": "-121.94",
                "hae": "5805.67",
                "sat": "8"
             },
             "results": {
                "m_MethodType": "HPS_MTHTYPE_SURVEY",
                "m_ScanMode": "HPS_MTH_SIM",
                "m_MethodState": "HPS_MTHSTATE_FINISHED",
                "m_MethodName": "/Haps/Method/Analyze/Concentrator/ER_Air_Tri-Bed_PPB_Quant.mth",
                "m_DataFileName": [],
                "m_SearchCount": "1",
                "m_SearchResults": [
                   {
                      "id": "Carbon Tetrachloride",
                      "concentration": "0.069",
                      "time": "01:42.3",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.581",
                      "purity": "0.175",
                      "area": "402039",
                      "endTime": "01:47.8",
                      "casnum": " 56-23-5"
                   },
                   {
                      "id": "Ethane, 1,1,2-trichloro-1,2,2-trifluoro-",
                      "concentration": "0",
                      "time": "00:56.2",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.761",
                      "purity": "0.135",
                      "area": "32124",
                      "endTime": "00:56.7",
                      "casnum": "76-13-1"
                   },
                   {
                      "id": "Benzene",
                      "concentration": "0.104",
                      "time": "01:39.9",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.997",
                      "purity": "0.888",
                      "area": "557635",
                      "endTime": "01:42.3",
                      "casnum": "71-43-2"
                   },
                   {
                      "id": "Tetrachloroethylene",
                      "concentration": "0",
                      "time": "04:09.5",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0",
                      "purity": "0",
                      "area": "0",
                      "endTime": "",
                      "casnum": "127-18-4"
                   }
                ]
             }
          }
       }
    },
    {
       "version": "2.0",
       "uid": "ASD_uM06_HAPSITE_hapsite",
       "time": "2017-09-27T06:07:06.00Z",
       "start": "2017-09-27T06:07:06.00Z",
       "stale": "2017-09-27T06:07:06.00Z",
       "how": "m-g",
       "point": {
          "lat": "37.470397",
          "lon": "-121.940177",
          "hae": "-3.94",
          "ce": "32",
          "le": "32",
          "sat": "7"
       },
       "detail": {
          "_flow-tags_": {
             "ASD_uM06_HAPSITE": "2017-09-27T06:07:06.00Z",
             "TAKServer22e69e81": "2017-10-03T18:49:00Z"
          },
          "remarks": "09/27/17 06:07:06, stype=hapsite",
          "hapsite": {
             "version": "1.0",
             "fid": "1506485184248",
             "source": "H1778-70023384",
             "time": "2017-09-27T04:06:24",
             "point": {
                "lat": "37.4705",
                "lon": "-121.94",
                "hae": "5805.67",
                "sat": "8"
             },
             "results": {
                "m_MethodType": "HPS_MTHTYPE_SURVEY",
                "m_ScanMode": "HPS_MTH_SIM",
                "m_MethodState": "HPS_MTHSTATE_FINISHED",
                "m_MethodName": "/Haps/Method/Analyze/Concentrator/ER_Air_Tri-Bed_PPB_Quant.mth",
                "m_DataFileName": [],
                "m_SearchCount": "1",
                "m_SearchResults": [
                   {
                      "id": "Carbon Tetrachloride",
                      "concentration": "0.069",
                      "time": "01:42.3",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.581",
                      "purity": "0.175",
                      "area": "402039",
                      "endTime": "01:47.8",
                      "casnum": " 56-23-5"
                   },
                   {
                      "id": "Ethane, 1,1,2-trichloro-1,2,2-trifluoro-",
                      "concentration": "0",
                      "time": "00:56.2",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.761",
                      "purity": "0.135",
                      "area": "32124",
                      "endTime": "00:56.7",
                      "casnum": "76-13-1"
                   },
                   {
                      "id": "Benzene",
                      "concentration": "0.104",
                      "time": "01:39.9",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.997",
                      "purity": "0.888",
                      "area": "557635",
                      "endTime": "01:42.3",
                      "casnum": "71-43-2"
                   },
                   {
                      "id": "Tetrachloroethylene",
                      "concentration": "0",
                      "time": "04:09.5",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0",
                      "purity": "0",
                      "area": "0",
                      "endTime": "",
                      "casnum": "127-18-4"
                   }
                ]
             }
          }
       }
    },
    {
       "version": "2.0",
       "uid": "ASD_uM06_HAPSITE_hapsite",
       "time": "2017-09-27T06:07:06.00Z",
       "start": "2017-09-27T06:07:06.00Z",
       "stale": "2017-09-27T06:07:06.00Z",
       "how": "m-g",
       "point": {
          "lat": "37.470397",
          "lon": "-121.940177",
          "hae": "-3.94",
          "ce": "32",
          "le": "32",
          "sat": "7"
       },
       "detail": {
          "_flow-tags_": {
             "ASD_uM06_HAPSITE": "2017-09-27T06:07:06.00Z",
             "TAKServer22e69e81": "2017-10-03T18:49:33Z"
          },
          "remarks": "09/27/17 06:07:06, stype=hapsite",
          "hapsite": {
             "version": "1.0",
             "fid": "1506485184248",
             "source": "H1778-70023384",
             "time": "2017-09-27T04:06:24",
             "point": {
                "lat": "37.4705",
                "lon": "-121.94",
                "hae": "5805.67",
                "sat": "8"
             },
             "results": {
                "m_MethodType": "HPS_MTHTYPE_SURVEY",
                "m_ScanMode": "HPS_MTH_SIM",
                "m_MethodState": "HPS_MTHSTATE_FINISHED",
                "m_MethodName": "/Haps/Method/Analyze/Concentrator/ER_Air_Tri-Bed_PPB_Quant.mth",
                "m_DataFileName": [],
                "m_SearchCount": "1",
                "m_SearchResults": [
                   {
                      "id": "Carbon Tetrachloride",
                      "concentration": "0.069",
                      "time": "01:42.3",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.581",
                      "purity": "0.175",
                      "area": "402039",
                      "endTime": "01:47.8",
                      "casnum": " 56-23-5"
                   },
                   {
                      "id": "Ethane, 1,1,2-trichloro-1,2,2-trifluoro-",
                      "concentration": "0",
                      "time": "00:56.2",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.761",
                      "purity": "0.135",
                      "area": "32124",
                      "endTime": "00:56.7",
                      "casnum": "76-13-1"
                   },
                   {
                      "id": "Benzene",
                      "concentration": "0.104",
                      "time": "01:39.9",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.997",
                      "purity": "0.888",
                      "area": "557635",
                      "endTime": "01:42.3",
                      "casnum": "71-43-2"
                   },
                   {
                      "id": "Tetrachloroethylene",
                      "concentration": "0",
                      "time": "04:09.5",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0",
                      "purity": "0",
                      "area": "0",
                      "endTime": "",
                      "casnum": "127-18-4"
                   }
                ]
             }
          }
       }
    },
    {
       "version": "2.0",
       "uid": "ASD_uM06_HAPSITE_hapsite",
       "time": "2017-09-27T06:07:04.00Z",
       "start": "2017-09-27T06:07:04.00Z",
       "stale": "2017-09-27T06:07:04.00Z",
       "how": "m-g",
       "point": {
          "lat": "37.470400",
          "lon": "-121.940178",
          "hae": "-3.94",
          "ce": "32",
          "le": "32",
          "sat": "7"
       },
       "detail": {
          "_flow-tags_": {
             "ASD_uM06_HAPSITE": "2017-09-27T06:07:04.00Z",
             "TAKServer22e69e81": "2017-10-03T18:51:46Z"
          },
          "remarks": "09/27/17 06:07:04, stype=hapsite",
          "hapsite": {
             "version": "1.0",
             "fid": "1506485184238",
             "source": "H1778-70023384",
             "time": "2017-09-27T04:06:24",
             "point": {
                "lat": "37.4705",
                "lon": "-121.94",
                "hae": "5805.67",
                "sat": "8"
             },
             "results": {
                "m_MethodType": "HPS_MTHTYPE_SURVEY",
                "m_ScanMode": "HPS_MTH_SIM",
                "m_MethodState": "HPS_MTHSTATE_FINISHED",
                "m_MethodName": "/Haps/Method/Analyze/Concentrator/ER_Air_Tri-Bed_PPB_Quant.mth",
                "m_DataFileName": [],
                "m_SearchCount": "1",
                "m_SearchResults": [
                   {
                      "id": "Carbon Tetrachloride",
                      "concentration": "0.069",
                      "time": "01:42.3",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.581",
                      "purity": "0.175",
                      "area": "402039",
                      "endTime": "01:47.8",
                      "casnum": " 56-23-5"
                   },
                   {
                      "id": "Ethane, 1,1,2-trichloro-1,2,2-trifluoro-",
                      "concentration": "0",
                      "time": "00:56.2",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.761",
                      "purity": "0.135",
                      "area": "32124",
                      "endTime": "00:56.7",
                      "casnum": "76-13-1"
                   },
                   {
                      "id": "Benzene",
                      "concentration": "0.104",
                      "time": "01:39.9",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.997",
                      "purity": "0.888",
                      "area": "557635",
                      "endTime": "01:42.3",
                      "casnum": "71-43-2"
                   },
                   {
                      "id": "Tetrachloroethylene",
                      "concentration": "0",
                      "time": "04:09.5",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0",
                      "purity": "0",
                      "area": "0",
                      "endTime": "",
                      "casnum": "127-18-4"
                   }
                ]
             }
          }
       }
    },
    {
       "version": "2.0",
       "uid": "ASD_uM06_HAPSITE_hapsite",
       "time": "2017-09-27T06:07:00.00Z",
       "start": "2017-09-27T06:07:00.00Z",
       "stale": "2018-09-27T06:07:00.00Z",
       "how": "m-g",
       "point": {
          "lat": "37.470400",
          "lon": "-121.940177",
          "hae": "-3.94",
          "ce": "32",
          "le": "32",
          "sat": "7"
       },
       "detail": {
          "_flow-tags_": {
             "ASD_uM06_HAPSITE": "2017-09-27T06:07:00.00Z",
             "TAKServer22e69e81": "2017-10-03T18:45:55Z"
          },
          "remarks": "09/27/17 06:07:00, stype=hapsite",
          "hapsite": {
             "version": "1.0",
             "fid": "1506485184206",
             "source": "H1778-70023384",
             "time": "2017-09-27T04:06:24",
             "point": {
                "lat": "37.4705",
                "lon": "-121.94",
                "hae": "5805.67",
                "sat": "8"
             },
             "results": {
                "m_MethodType": "HPS_MTHTYPE_SURVEY",
                "m_ScanMode": "HPS_MTH_SIM",
                "m_MethodState": "HPS_MTHSTATE_FINISHED",
                "m_MethodName": "/Haps/Method/Analyze/Concentrator/ER_Air_Tri-Bed_PPB_Quant.mth",
                "m_DataFileName": [],
                "m_SearchCount": "1",
                "m_SearchResults": [
                   {
                      "id": "Carbon Tetrachloride",
                      "concentration": "0.069",
                      "time": "01:42.3",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.581",
                      "purity": "0.175",
                      "area": "402039",
                      "endTime": "01:47.8",
                      "casnum": " 56-23-5"
                   },
                   {
                      "id": "Ethane, 1,1,2-trichloro-1,2,2-trifluoro-",
                      "concentration": "0",
                      "time": "00:56.2",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.761",
                      "purity": "0.135",
                      "area": "32124",
                      "endTime": "00:56.7",
                      "casnum": "76-13-1"
                   },
                   {
                      "id": "Benzene",
                      "concentration": "0.104",
                      "time": "01:39.9",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.997",
                      "purity": "0.888",
                      "area": "557635",
                      "endTime": "01:42.3",
                      "casnum": "71-43-2"
                   },
                   {
                      "id": "Tetrachloroethylene",
                      "concentration": "0",
                      "time": "04:09.5",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0",
                      "purity": "0",
                      "area": "0",
                      "endTime": "",
                      "casnum": "127-18-4"
                   }
                ]
             }
          }
       }
    },
    {
       "version": "2.0",
       "uid": "ASD_uM06_HAPSITE_hapsite",
       "time": "2017-09-27T06:07:00.00Z",
       "start": "2017-09-27T06:07:00.00Z",
       "stale": "2018-09-27T06:07:00.00Z",
       "how": "m-g",
       "point": {
          "lat": "37.470400",
          "lon": "-121.940177",
          "hae": "-3.94",
          "ce": "32",
          "le": "32",
          "sat": "7"
       },
       "detail": {
          "_flow-tags_": {
             "ASD_uM06_HAPSITE": "2017-09-27T06:07:00.00Z",
             "TAKServer22e69e81": "2017-10-03T18:45:53Z"
          },
          "remarks": "09/27/17 06:07:00, stype=hapsite",
          "hapsite": {
             "version": "1.0",
             "fid": "1506485184206",
             "source": "H1778-70023384",
             "time": "2017-09-27T04:06:24",
             "point": {
                "lat": "37.4705",
                "lon": "-121.94",
                "hae": "5805.67",
                "sat": "8"
             },
             "results": {
                "m_MethodType": "HPS_MTHTYPE_SURVEY",
                "m_ScanMode": "HPS_MTH_SIM",
                "m_MethodState": "HPS_MTHSTATE_FINISHED",
                "m_MethodName": "/Haps/Method/Analyze/Concentrator/ER_Air_Tri-Bed_PPB_Quant.mth",
                "m_DataFileName": [],
                "m_SearchCount": "1",
                "m_SearchResults": [
                   {
                      "id": "Carbon Tetrachloride",
                      "concentration": "0.069",
                      "time": "01:42.3",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.581",
                      "purity": "0.175",
                      "area": "402039",
                      "endTime": "01:47.8",
                      "casnum": " 56-23-5"
                   },
                   {
                      "id": "Ethane, 1,1,2-trichloro-1,2,2-trifluoro-",
                      "concentration": "0",
                      "time": "00:56.2",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.761",
                      "purity": "0.135",
                      "area": "32124",
                      "endTime": "00:56.7",
                      "casnum": "76-13-1"
                   },
                   {
                      "id": "Benzene",
                      "concentration": "0.104",
                      "time": "01:39.9",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.997",
                      "purity": "0.888",
                      "area": "557635",
                      "endTime": "01:42.3",
                      "casnum": "71-43-2"
                   },
                   {
                      "id": "Tetrachloroethylene",
                      "concentration": "0",
                      "time": "04:09.5",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0",
                      "purity": "0",
                      "area": "0",
                      "endTime": "",
                      "casnum": "127-18-4"
                   }
                ]
             }
          }
       }
    },
    {
       "version": "2.0",
       "uid": "ASD_uM06_HAPSITE_hapsite",
       "time": "2017-09-27T06:07:00.00Z",
       "start": "2017-09-27T06:07:00.00Z",
       "stale": "2018-09-27T06:07:00.00Z",
       "how": "m-g",
       "point": {
          "lat": "37.470400",
          "lon": "-121.940177",
          "hae": "-3.94",
          "ce": "32",
          "le": "32",
          "sat": "7"
       },
       "detail": {
          "_flow-tags_": {
             "ASD_uM06_HAPSITE": "2017-09-27T06:07:00.00Z",
             "TAKServer22e69e81": "2017-10-03T18:48:29Z"
          },
          "remarks": "09/27/17 06:07:00, stype=hapsite",
          "hapsite": {
             "version": "1.0",
             "fid": "1506485184107",
             "source": "H1778-70023384",
             "time": "2017-09-27T04:06:24",
             "point": {
                "lat": "37.4705",
                "lon": "-121.94",
                "hae": "5805.67",
                "sat": "8"
             },
             "results": {
                "m_MethodType": "HPS_MTHTYPE_SURVEY",
                "m_ScanMode": "HPS_MTH_SIM",
                "m_MethodState": "HPS_MTHSTATE_FINISHED",
                "m_MethodName": "/Haps/Method/Analyze/Concentrator/ER_Air_Tri-Bed_PPB_Quant.mth",
                "m_DataFileName": [],
                "m_SearchCount": "1",
                "m_SearchResults": [
                   {
                      "id": "Carbon Tetrachloride",
                      "concentration": "0.069",
                      "time": "01:42.3",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.581",
                      "purity": "0.175",
                      "area": "402039",
                      "endTime": "01:47.8",
                      "casnum": " 56-23-5"
                   },
                   {
                      "id": "Ethane, 1,1,2-trichloro-1,2,2-trifluoro-",
                      "concentration": "0",
                      "time": "00:56.2",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.761",
                      "purity": "0.135",
                      "area": "32124",
                      "endTime": "00:56.7",
                      "casnum": "76-13-1"
                   },
                   {
                      "id": "Benzene",
                      "concentration": "0.104",
                      "time": "01:39.9",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.997",
                      "purity": "0.888",
                      "area": "557635",
                      "endTime": "01:42.3",
                      "casnum": "71-43-2"
                   },
                   {
                      "id": "Tetrachloroethylene",
                      "concentration": "0",
                      "time": "04:09.5",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0",
                      "purity": "0",
                      "area": "0",
                      "endTime": "",
                      "casnum": "127-18-4"
                   }
                ]
             }
          }
       }
    },
    {
       "version": "2.0",
       "uid": "ASD_uM06_HAPSITE_hapsite",
       "time": "2017-09-27T06:06:58.00Z",
       "start": "2017-09-27T06:06:58.00Z",
       "stale": "2018-09-27T06:06:58.00Z",
       "how": "m-g",
       "point": {
          "lat": "37.470400",
          "lon": "-121.940178",
          "hae": "-3.94",
          "ce": "32",
          "le": "32",
          "sat": "7"
       },
       "detail": {
          "_flow-tags_": {
             "ASD_uM06_HAPSITE": "2017-09-27T06:06:58.00Z",
             "TAKServer22e69e81": "2017-10-03T18:49:02Z"
          },
          "remarks": "09/27/17 06:06:58, stype=hapsite",
          "hapsite": {
             "version": "1.0",
             "fid": "1506485184228",
             "source": "H1778-70023384",
             "time": "2017-09-27T04:06:24",
             "point": {
                "lat": "37.4705",
                "lon": "-121.94",
                "hae": "5805.67",
                "sat": "8"
             },
             "results": {
                "m_MethodType": "HPS_MTHTYPE_SURVEY",
                "m_ScanMode": "HPS_MTH_SIM",
                "m_MethodState": "HPS_MTHSTATE_FINISHED",
                "m_MethodName": "/Haps/Method/Analyze/Concentrator/ER_Air_Tri-Bed_PPB_Quant.mth",
                "m_DataFileName": [],
                "m_SearchCount": "1",
                "m_SearchResults": [
                   {
                      "id": "Carbon Tetrachloride",
                      "concentration": "0.069",
                      "time": "01:42.3",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.581",
                      "purity": "0.175",
                      "area": "402039",
                      "endTime": "01:47.8",
                      "casnum": " 56-23-5"
                   },
                   {
                      "id": "Ethane, 1,1,2-trichloro-1,2,2-trifluoro-",
                      "concentration": "0",
                      "time": "00:56.2",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.761",
                      "purity": "0.135",
                      "area": "32124",
                      "endTime": "00:56.7",
                      "casnum": "76-13-1"
                   },
                   {
                      "id": "Benzene",
                      "concentration": "0.104",
                      "time": "01:39.9",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.997",
                      "purity": "0.888",
                      "area": "557635",
                      "endTime": "01:42.3",
                      "casnum": "71-43-2"
                   },
                   {
                      "id": "Tetrachloroethylene",
                      "concentration": "0",
                      "time": "04:09.5",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0",
                      "purity": "0",
                      "area": "0",
                      "endTime": "",
                      "casnum": "127-18-4"
                   }
                ]
             }
          }
       }
    },
    {
       "version": "2.0",
       "uid": "ASD_uM06_HAPSITE_hapsite",
       "time": "2017-09-27T06:07:05.00Z",
       "start": "2017-09-27T06:07:05.00Z",
       "stale": "2018-09-27T06:07:05.00Z",
       "how": "m-g",
       "point": {
          "lat": "37.470397",
          "lon": "-121.940177",
          "hae": "-3.94",
          "ce": "32",
          "le": "32",
          "sat": "7"
       },
       "detail": {
          "_flow-tags_": {
             "ASD_uM06_HAPSITE": "2017-09-27T06:07:05.00Z",
             "TAKServer22e69e81": "2017-10-03T18:49:34Z"
          },
          "remarks": "09/27/17 06:07:05, stype=hapsite",
          "hapsite": {
             "version": "1.0",
             "fid": "1506485184129",
             "source": "H1778-70023384",
             "time": "2017-09-27T04:06:24",
             "point": {
                "lat": "37.4705",
                "lon": "-121.94",
                "hae": "5805.67",
                "sat": "8"
             },
             "results": {
                "m_MethodType": "HPS_MTHTYPE_SURVEY",
                "m_ScanMode": "HPS_MTH_SIM",
                "m_MethodState": "HPS_MTHSTATE_FINISHED",
                "m_MethodName": "/Haps/Method/Analyze/Concentrator/ER_Air_Tri-Bed_PPB_Quant.mth",
                "m_DataFileName": [],
                "m_SearchCount": "1",
                "m_SearchResults": [
                   {
                      "id": "Carbon Tetrachloride",
                      "concentration": "0.069",
                      "time": "01:42.3",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.581",
                      "purity": "0.175",
                      "area": "402039",
                      "endTime": "01:47.8",
                      "casnum": " 56-23-5"
                   },
                   {
                      "id": "Ethane, 1,1,2-trichloro-1,2,2-trifluoro-",
                      "concentration": "0",
                      "time": "00:56.2",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.761",
                      "purity": "0.135",
                      "area": "32124",
                      "endTime": "00:56.7",
                      "casnum": "76-13-1"
                   },
                   {
                      "id": "Benzene",
                      "concentration": "0.104",
                      "time": "01:39.9",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.997",
                      "purity": "0.888",
                      "area": "557635",
                      "endTime": "01:42.3",
                      "casnum": "71-43-2"
                   },
                   {
                      "id": "Tetrachloroethylene",
                      "concentration": "0",
                      "time": "04:09.5",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0",
                      "purity": "0",
                      "area": "0",
                      "endTime": "",
                      "casnum": "127-18-4"
                   }
                ]
             }
          }
       }
    },
    {
       "version": "2.0",
       "uid": "ASD_uM06_HAPSITE_hapsite",
       "time": "2017-09-27T06:07:05.00Z",
       "start": "2017-09-27T06:07:05.00Z",
       "stale": "2018-09-27T06:07:05.00Z",
       "how": "m-g",
       "point": {
          "lat": "37.470397",
          "lon": "-121.940177",
          "hae": "-3.94",
          "ce": "32",
          "le": "32",
          "sat": "7"
       },
       "detail": {
          "_flow-tags_": {
             "ASD_uM06_HAPSITE": "2017-09-27T06:07:05.00Z",
             "TAKServer22e69e81": "2017-10-03T18:52:13Z"
          },
          "remarks": "09/27/17 06:07:05, stype=hapsite",
          "hapsite": {
             "version": "1.0",
             "fid": "1506485184129",
             "source": "H1778-70023384",
             "time": "2017-09-27T04:06:24",
             "point": {
                "lat": "37.4705",
                "lon": "-121.94",
                "hae": "5805.67",
                "sat": "8"
             },
             "results": {
                "m_MethodType": "HPS_MTHTYPE_SURVEY",
                "m_ScanMode": "HPS_MTH_SIM",
                "m_MethodState": "HPS_MTHSTATE_FINISHED",
                "m_MethodName": "/Haps/Method/Analyze/Concentrator/ER_Air_Tri-Bed_PPB_Quant.mth",
                "m_DataFileName": [],
                "m_SearchCount": "1",
                "m_SearchResults": [
                   {
                      "id": "Carbon Tetrachloride",
                      "concentration": "0.069",
                      "time": "01:42.3",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.581",
                      "purity": "0.175",
                      "area": "402039",
                      "endTime": "01:47.8",
                      "casnum": " 56-23-5"
                   },
                   {
                      "id": "Ethane, 1,1,2-trichloro-1,2,2-trifluoro-",
                      "concentration": "0",
                      "time": "00:56.2",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.761",
                      "purity": "0.135",
                      "area": "32124",
                      "endTime": "00:56.7",
                      "casnum": "76-13-1"
                   },
                   {
                      "id": "Benzene",
                      "concentration": "0.104",
                      "time": "01:39.9",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.997",
                      "purity": "0.888",
                      "area": "557635",
                      "endTime": "01:42.3",
                      "casnum": "71-43-2"
                   },
                   {
                      "id": "Tetrachloroethylene",
                      "concentration": "0",
                      "time": "04:09.5",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0",
                      "purity": "0",
                      "area": "0",
                      "endTime": "",
                      "casnum": "127-18-4"
                   }
                ]
             }
          }
       }
    },
    {
       "version": "2.0",
       "uid": "ASD_uM06_HAPSITE_hapsite",
       "time": "2017-09-27T06:07:05.00Z",
       "start": "2017-09-27T06:07:05.00Z",
       "stale": "2018-09-27T06:07:05.00Z",
       "how": "m-g",
       "point": {
          "lat": "37.470397",
          "lon": "-121.940177",
          "hae": "-3.94",
          "ce": "32",
          "le": "32",
          "sat": "7"
       },
       "detail": {
          "_flow-tags_": {
             "ASD_uM06_HAPSITE": "2017-09-27T06:07:05.00Z",
             "TAKServer22e69e81": "2017-10-03T18:55:00Z"
          },
          "remarks": "09/27/17 06:07:05, stype=hapsite",
          "hapsite": {
             "version": "1.0",
             "fid": "1506485184129",
             "source": "H1778-70023384",
             "time": "2017-09-27T04:06:24",
             "point": {
                "lat": "37.4705",
                "lon": "-121.94",
                "hae": "5805.67",
                "sat": "8"
             },
             "results": {
                "m_MethodType": "HPS_MTHTYPE_SURVEY",
                "m_ScanMode": "HPS_MTH_SIM",
                "m_MethodState": "HPS_MTHSTATE_FINISHED",
                "m_MethodName": "/Haps/Method/Analyze/Concentrator/ER_Air_Tri-Bed_PPB_Quant.mth",
                "m_DataFileName": [],
                "m_SearchCount": "1",
                "m_SearchResults": [
                   {
                      "id": "Carbon Tetrachloride",
                      "concentration": "0.069",
                      "time": "01:42.3",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.581",
                      "purity": "0.175",
                      "area": "402039",
                      "endTime": "01:47.8",
                      "casnum": " 56-23-5"
                   },
                   {
                      "id": "Ethane, 1,1,2-trichloro-1,2,2-trifluoro-",
                      "concentration": "0",
                      "time": "00:56.2",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.761",
                      "purity": "0.135",
                      "area": "32124",
                      "endTime": "00:56.7",
                      "casnum": "76-13-1"
                   },
                   {
                      "id": "Benzene",
                      "concentration": "0.104",
                      "time": "01:39.9",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0.997",
                      "purity": "0.888",
                      "area": "557635",
                      "endTime": "01:42.3",
                      "casnum": "71-43-2"
                   },
                   {
                      "id": "Tetrachloroethylene",
                      "concentration": "0",
                      "time": "04:09.5",
                      "units": "ppb",
                      "alarmLevel": "0.0",
                      "fit": "0",
                      "purity": "0",
                      "area": "0",
                      "endTime": "",
                      "casnum": "127-18-4"
                   }
                ]
             }
          }
       }
    }
 ]}