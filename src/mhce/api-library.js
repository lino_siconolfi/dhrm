const https = require('follow-redirects').https;
const config = require('config');

class MhceApi 
{
    constructor() {
        this._host = config.get("mhce.host");
        this._username = config.get("mhce.default_username");
        this._password = config.get("mhce.default_password");
    }

    /**
     * Send a request to the MHCE API. 
     * 
     * This is copied from the Smartaabse API library and will need modification
     */
    _send(payload) 
    {
        let options = {
            'method': 'POST',
            'hostname': this._host,
            'path': '/api/reauth',
            'headers': {
                'Authorization': this._auth_header,
                'Content-Type': 'application/json'
            },
            'maxRedirects': 20
        };

        let request = https.request(options, (res) => {

            let chunks = [];

            res.on("data", function (chunk)
            {
                chunks.push(chunk);
            });

            res.on("end", function (chunk)
            {
                // If the response is not complete when the connection ends
                // we have to assume something went wrong and stop here. 
                if (!res.complete)
                {
                    reject("Connection interrupted.");
                }

                // If the status code is not in the 2xx range (success) then
                // we will not try to parse the response body, because it 
                // may not be JSON. 
                if (res.statusCode < 200 || res.statusCode > 299)
                {
                    reject(res.statusMessage);
                }

                // Try to parse the response body a JSON and return it to 
                // the caller. 
                let body = Buffer.concat(chunks).toString();
                try 
                {
                    let parsed = JSON.parse(body);
                    resolve(parsed);
                }
                catch (e)
                {
                    reject("Invalid response format");
                }
            });

            // Generally this will be a protocol error. 
            res.on("error", function (error)
            {
                reject(error);
            });

        });

        request.write(JSON.stringify(payload));
        request.end();
    }

    /**
     * Get new WFL records.
     * 
     * Currently this returns static data. 
     */
    getWfls() 
    {
        return {
            data : [{
                "Blast_WFL_Id": 4817,
                "Document_Id": "",
                "AppNucleus_Id": "",
                "BlastRecord_Id": 8,
                "BlastWebUser_Id": 8,
                "Username": "MattQA",
                "ApprovalStatus": "Pending Approval",
                "CaptureMethod": "observer",
                "NumberOfWeapons": 3,
                "OtherUsers": "",
                "FromDateTime": "2021-12-01T07:00:00",
                "ToDateTime": "2021-12-02T07:00:00",
                "Timezone": "Zulu Time (GMT)",
                "Range": "Range 3",
                "Location": "Gordon",
                "EventTitle": "Scott Test 2",
                "UnitName": "Scott Unit",
                "BOPSensor": false,
                "InEarProtection": false,
                "OverEarProtection": false,
                "DOD": "3216549875",
                "WeaponsFired": [{
                        "WeaponsFired_Id": 4053,
                        "WeaponSystemType": "Recoilless Rifle",
                        "WeaponSystem": "M40/105mm",
                        "RoundsFired": 22,
                        "BodyPosition": "",
                        "CrewPosition": "",
                        "EnclosedSpace": "No",
                        "WeaponsMod": "",
                        "BreachingDoor": "",
                        "BreachingWall": "",
                        "MunitionType": "type",
                        "MunitionTypeOther": "",
                        "OtherDetails": "",
                        "ProximityToSource": null,
                        "StackingPosition": 0,
                        "BreachingUmbc": "",
                        "BreachingSOF": "",
                        "UnderWater": "",
                        "BreachingCustConfDesc": "",
                        "BreachingCustWeight": 0.00,
                        "BreachingWallType": "",
                        "BreachingDoorType": "",
                        "BreachingTargetDoor": false,
                        "BreachingTargetWall": false,
                        "BreachingTargetOther": false,
                        "TargetOtherDetails": ""
                    },
                    {
                        "WeaponsFired_Id": 4054,
                        "WeaponSystemType": "Gunnery",
                        "WeaponSystem": "M2/M2A1 12.7mm (.50 CAL) MACHINE GUN",
                        "RoundsFired": 55,
                        "BodyPosition": "",
                        "CrewPosition": "",
                        "EnclosedSpace": "No",
                        "WeaponsMod": "",
                        "BreachingDoor": "",
                        "BreachingWall": "",
                        "MunitionType": "type",
                        "MunitionTypeOther": "",
                        "OtherDetails": "",
                        "ProximityToSource": null,
                        "StackingPosition": 0,
                        "BreachingUmbc": "",
                        "BreachingSOF": "",
                        "UnderWater": "",
                        "BreachingCustConfDesc": "",
                        "BreachingCustWeight": 0.00,
                        "BreachingWallType": "",
                        "BreachingDoorType": "",
                        "BreachingTargetDoor": false,
                        "BreachingTargetWall": false,
                        "BreachingTargetOther": false,
                        "TargetOtherDetails": ""
                    }
                ]
            }],
            updates : [{
                "Blast_WFL_Id": 2804,
                "Document_Id": "",
                "AppNucleus_Id": "",
                "BlastRecord_Id": 8,
                "BlastWebUser_Id": 8,
                "Username": "MattQA",
                "ApprovalStatus": "Pending Approval",
                "CaptureMethod": "observer",
                "NumberOfWeapons": 3,
                "OtherUsers": "",
                "FromDateTime": "2021-12-01T07:00:00",
                "ToDateTime": "2021-12-02T07:00:00",
                "Timezone": "Zulu Time (GMT)",
                "Range": "Range 3",
                "Location": "Gordon",
                "EventTitle": "Scott Test 2",
                "UnitName": "Scott Unit",
                "BOPSensor": false,
                "InEarProtection": false,
                "OverEarProtection": false,
                "DOD": "3216549875",
                "WeaponsFired": [{
                        "WeaponsFired_Id": 3053,
                        "WeaponSystemType": "Recoilless Rifle",
                        "WeaponSystem": "M40/105mm",
                        "RoundsFired": 22,
                        "BodyPosition": "",
                        "CrewPosition": "",
                        "EnclosedSpace": "No",
                        "WeaponsMod": "",
                        "BreachingDoor": "",
                        "BreachingWall": "",
                        "MunitionType": "type",
                        "MunitionTypeOther": "",
                        "OtherDetails": "",
                        "ProximityToSource": null,
                        "StackingPosition": 0,
                        "BreachingUmbc": "",
                        "BreachingSOF": "",
                        "UnderWater": "",
                        "BreachingCustConfDesc": "",
                        "BreachingCustWeight": 0.00,
                        "BreachingWallType": "",
                        "BreachingDoorType": "",
                        "BreachingTargetDoor": false,
                        "BreachingTargetWall": false,
                        "BreachingTargetOther": false,
                        "TargetOtherDetails": ""
                    },
                    {
                        "WeaponsFired_Id": 3054,
                        "WeaponSystemType": "Gunnery",
                        "WeaponSystem": "M2/M2A1 12.7mm (.50 CAL) MACHINE GUN",
                        "RoundsFired": 55,
                        "BodyPosition": "",
                        "CrewPosition": "",
                        "EnclosedSpace": "No",
                        "WeaponsMod": "",
                        "BreachingDoor": "",
                        "BreachingWall": "",
                        "MunitionType": "type",
                        "MunitionTypeOther": "",
                        "OtherDetails": "",
                        "ProximityToSource": null,
                        "StackingPosition": 0,
                        "BreachingUmbc": "",
                        "BreachingSOF": "",
                        "UnderWater": "",
                        "BreachingCustConfDesc": "",
                        "BreachingCustWeight": 0.00,
                        "BreachingWallType": "",
                        "BreachingDoorType": "",
                        "BreachingTargetDoor": false,
                        "BreachingTargetWall": false,
                        "BreachingTargetOther": false,
                        "TargetOtherDetails": ""
                    }
                ]
            }]
        };
    }
}

module.exports = new MhceApi();