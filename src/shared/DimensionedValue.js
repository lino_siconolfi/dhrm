import { normalizeUnits } from "./normalizeUnits.js"
import { Portable } from "./portable.js"

/**
 * Type to make sure dimensioned values are represented in the same way. 
 * The assumption is that this is immutable, and that calculation will result in 
 * new objects.
 */
export class DimensionedValue extends Portable
{
	/**
	 * 
	 * 
	 * @TODO Provide validation for supported units. More detailed parameter documentation.
	 * 
	 * @param {*} value 
	 * @param { String } units 
	 * @param {*} valid 
	 * @param {*} precision 
	 * @param {*} history 
	 */
	constructor (value, units, valid, precision)
	{
		super();

		// Value is expected to be a number or a string following the standard that 
		// a non detection is represented by a '<' followed by the threshold 
		// sensitivity. 
		if ( typeof value === 'string' )
		{
			if (value[0]=='<')
			{
				this.isValid = false;
			}
			else 
			{
				this.value = parseFloat(value);
			}
		}
		else if ( typeof value === 'number' )
		{
			this.value = value;
			this.isValid = true;
		}
		else if ( value instanceof Object )
		{
			return this.fromObject(value);
		}
		else 
		{
			this.isValid = false;
			return;
		}

		// Save original value
		this.originalValue = this.value;
		
		// Units are interpereted as provided. 
		this.units = ( units === undefined ) ? 'Unknown' : units;
		
		// If validity is set, we take that over anything we tried to parse
		if ( valid !== undefined )
		{
			this.isValid = valid;
		}

		this.precision = precision || this.value.toExponential().replace(/e[\+\-0-9]*$/, '').replace( /(^0\.?0*)|\./, '').length || 1;
		
		this.history = [];
		this.history.push(`Current value: ${this.print()}`);

		if (!this.isValid)
		{
			this.value = this.value / 2;
			this.history.push(`Value divided by 2 due to being below the detection limit.`);
			this.history.push(`Current value: ${this.print()}`);
		}
	}

	print(bare)
	{
		return parseFloat(this.value.toPrecision(this.precision)).toString() + (bare ? '' : (' ' + this.units));
	}

	toString()
	{
		return this.print();
	}

	/**
	 * @param { DimensionedValue } left 
	 * @param { DimensionedValue } right 
	 * @param {*} context 
	 */
	static compare(left, right, context)
	{
		// If the values are not valid, they cannot be compared
		if ( !left.isValid || !right.isValid )
		{
			return 0;
		}

		// If the units are not the same, they must be normalized. 
		if ( left.units !== right.units )
		{
			left = normalizeUnits(left, context);
			right = normalizeUnits(right, context);
		}
		
		if ( left.value > right.value )
		{ 
			return 1;
		}
		else if ( left.value < right.value )
		{ 
			return -1;
		}
		else 
		{ 
			return 0;
		}
	}
}