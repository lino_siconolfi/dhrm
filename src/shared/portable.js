/**
 * Base class for classes which can be serialized to JSON for transport and
 * recreated automatically on deserialization. 
 */
export class Portable
{
    /**
     * Should only be called when creating an object from a serialized form.
     * 
     * Objects that require custom handling can override. 
     * 
     * @param {object} obj Generic object defining properties to be added
     * @param {bool} strict Whether to allow unknown properties
     */
    fromObject(obj, strict)
    {
        // Don't add this to the object itself. It's only for the serialized 
        // json version. 
        delete obj.PortableType;

        // Strict mode will only set properties that are defined on the class.
        if(strict)
        {
            Object.keys(obj).forEach(key=>{
                if(this.hasOwnProperty(key))
                {
                    this[key] = obj[key];
                }
            });
        }
        else 
        {
            Object.assign(this, obj);
        }
    }


    /**
     * Add the object type to the serialized JSON. 
     * 
     * @param {string} key 
     */
    toJSON(key)
    {
        // Add type
        let ret = {
            PortableType : this.constructor.name
        };

        // Filter to own enumerable properties only
        Object.keys(this).forEach(key=>{
            if(this.hasOwnProperty(key))
            {
                ret[key] = this[key];
            }
        });

        return ret;
    }
}