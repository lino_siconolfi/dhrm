export function validDodid(dodid)
{
    return RegExp(/^\d{10}$/).test(dodid);
}