export const EHRA_STATUS = {
	INCOMPLETE : 'INCOMPLETE',
	COMPLETE : 'COMPLETE',
	DELETED : 'DELETED'
}

/**
 * Exposure durations that are commonly selectable and displayed 
 * through much of the UI.  Their values are their times in minutes.
 */
export const EXPOSURE_DURATION = {
	MINUTES_10 : 10,
	HOUR_1 : 60,
	HOURS_8 : 480,
	HOURS_24 : 1440,
	DAYS_7 : 10080,
	DAYS_14 : 20160,
	YEAR_1 : 525600
}

EXPOSURE_DURATION.Options = [...Object.values(EXPOSURE_DURATION)];

EXPOSURE_DURATION.toString = function(val, abbreviated)
{
	switch(val)
	{
	case EXPOSURE_DURATION.MINUTES_10:
		return (abbreviated) ? "10 min" : "10 Minutes";
	case EXPOSURE_DURATION.HOUR_1:
		return (abbreviated) ? "1 hr" : "1 Hour";
	case EXPOSURE_DURATION.HOURS_8:
		return (abbreviated) ? "8 hr" : "8 Hours";
	case EXPOSURE_DURATION.HOURS_24:
		return (abbreviated) ? "24 hr" : "24 Hours";
	case EXPOSURE_DURATION.DAYS_7:
		return (abbreviated) ? "7 d" : "7 Days";
	case EXPOSURE_DURATION.DAYS_14:
		return (abbreviated) ? "14 d" : "14 Days";
	case EXPOSURE_DURATION.YEAR_1:
		return (abbreviated) ? "1 yr" : "1 Year";
	}

	throw new Error("Argument is not a valid exposure duration.");
}

EXPOSURE_DURATION.getNearest = function(val)
{
	if ( isNaN(val) )
		throw new Error("EXPOSURE_DURATION.getNearest() requires a numebric parameter.");

	let min_difference = Number.MAX_VALUE;
	let best_option = undefined;

	let EDO = EXPOSURE_DURATION.Options;

	for (let key in EDO )
	{
		let diff = Math.abs(val - EDO[key]);
		if ( diff < min_difference)
		{
			min_difference = diff;
			best_option = EDO[key];
		}
	}

	return best_option;
}

export const SEVERITY = {
	Negligible : 1,
	Marginal : 2,
	Critical : 3,
	Catastrophic : 4
}

SEVERITY.toString = function(val)
{
	switch(val)
	{
	case SEVERITY.Negligible:
		return "Negligible";
	case SEVERITY.Marginal:
		return "Marginal";
	case SEVERITY.Critical:
		return "Critical";
	case SEVERITY.Catastrophic:
		return "Catastrophic";
	}

	throw new Error("Argument is not a valid severity.");
}

export const IFA_INCIDENT_SCENARIO = {
	ALLIED_OPERATIONS : 'ALLIED_OPERATIONS',
	TRAINING : 'TRAINING'
};
Object.defineProperties(IFA_INCIDENT_SCENARIO, {

	isValid :{ value: function(val)
	{
		return !!Object.keys(this).includes(val);
	}},
	toString :{ value: function(val)
	{
		switch(val)
		{
			case IFA_INCIDENT_SCENARIO.ALLIED_OPERATIONS:
				return "Allied Operations";
			case IFA_INCIDENT_SCENARIO.TRAINING:
				return "Training";
		}

		throw new Error("Argument is not a valid Ifa Incident Scenario.");
	}}
});

export const IFA_INCIDENT_TYPE = {
	CONCUSSIVE : 'CONCUSSIVE',
	CHEMICAL : 'CHEMICAL',
	RADIOLOGICAL : 'RADIOLOGICAL',
	OTHER : 'OTHER'
};
Object.defineProperties(IFA_INCIDENT_TYPE, {

	isValid :{ value: function(val)
	{
		return !!Object.keys(this).includes(val);
	}},
	toString :{ value: function(val)
	{
		switch(val)
		{
			case IFA_INCIDENT_TYPE.CONCUSSIVE:
				return "Potentially Concussive Event";
			case IFA_INCIDENT_TYPE.CHEMICAL:
				return "Chemical";
			case IFA_INCIDENT_TYPE.RADIOLOGICAL:
				return "Radiological";
			case IFA_INCIDENT_TYPE.OTHER:
				return "Other";
		}

		throw new Error("Argument is not a valid Ifa Incident Type.");
	}}
});

export const IFA_INCIDENT_CAUSE = {
	ENEMY_ACTION : 'ENEMY_ACTION',
	ACCIDENTAL_RELEASE : 'ACCIDENTAL_RELEASE',
	UNKNOWN : 'UNKNOWN',
	OTHER : 'OTHER'
};
Object.defineProperties(IFA_INCIDENT_CAUSE, {

	isValid :{ value: function(val)
	{
		return !!Object.keys(this).includes(val);
	}},
	toString :{ value: function(val)
	{
		switch(val)
		{
			case IFA_INCIDENT_CAUSE.ENEMY_ACTION:
				return "Enemy Action";
			case IFA_INCIDENT_CAUSE.ACCIDENTAL_RELEASE:
				return "Accidental Release";
			case IFA_INCIDENT_CAUSE.UNKNOWN:
				return "Unknown";
			case IFA_INCIDENT_CAUSE.OTHER:
				return "Other";
		}

		throw new Error("Argument is not a valid Ifa Incident Cause.");
	}}
});

export const IFA_TRINARY = {
	YES : 'YES',
	NO : 'NO',
	UNKNOWN : 'UNKNOWN'
};
Object.defineProperties(IFA_TRINARY, {

	isValid :{ value: function(val)
	{
		return !!Object.keys(this).includes(val);
	}},
	toString :{ value: function(val)
	{
		switch(val)
		{
			case IFA_TRINARY.YES:
				return "Yes";
			case IFA_TRINARY.NO:
				return "No";
			case IFA_TRINARY.UNKNOWN:
				return "Unknown";
		}

		throw new Error("Argument is not a valid Ifa Trinary Option.");
	}},
	fromString :{ value: function(val)
	{
		switch(val)
		{
			case "Yes":
				return IFA_TRINARY.YES;
			case "No":
				return IFA_TRINARY.NO;
			case "Unknown":
				return IFA_TRINARY.UNKNOWN;
		}

		throw new Error("Argument is not a valid Ifa Trinary Option.");
	}}
});

export const IFA_QA_STATUS = {
	IN_PROGRESS : 'IN_PROGRESS',
	READY_FOR_QA : 'READY_FOR_QA',
	APPROVED_BY_QA : 'APPROVED_BY_QA'
};
Object.defineProperties(IFA_QA_STATUS, {

	isValid :{ value: function(val)
	{
		return !!Object.keys(this).includes(val);
	}},
	toString :{ value: function(val)
	{
		switch(val)
		{
			case IFA_QA_STATUS.IN_PROGRESS:
				return "In Progress";
			case IFA_QA_STATUS.READY_FOR_QA:
				return "Ready for QA";
			case IFA_QA_STATUS.APPROVED_BY_QA:
				return "Approved by QA";
		}

		throw new Error("Argument is not a valid QA Status.");
	}}
});

export const IFA_SUBMISSION_STATUS = {
	NOT_SUBMITTED : 'NOT_SUBMITTED',
	SUBMITTED : 'SUBMITTED',
	ACCEPTED : 'ACCEPTED'
};
Object.defineProperties(IFA_SUBMISSION_STATUS, {

	isValid :{ value: function(val)
	{
		return !!Object.keys(this).includes(val);
	}},
	toString :{ value: function(val)
	{
		switch(val)
		{
			case IFA_SUBMISSION_STATUS.NOT_SUBMITTED:
				return "Not Submitted";
			case IFA_SUBMISSION_STATUS.SUBMITTED:
				return "Submitted";
			case IFA_SUBMISSION_STATUS.ACCEPTED:
				return "Accepted";
		}

		throw new Error("Argument is not a valid Submission Status.");
	}}
});

export const IFA_STATUS = {
	COMPLETE : 'COMPLETE',
	INCOMPLETE : 'INCOMPLETE',
	DELETED : 'DELETED',
	IN_PROGRESS : 'IN_PROGRESS',
	APPROVED_BY_QA : 'APPROVED_BY_QA'
};
Object.defineProperties(IFA_STATUS, {

	isValid :{ value: function(val)
	{
		return !!Object.keys(this).includes(val);
	}},
	toString :{ value: function(val)
	{
		switch(val)
		{
			case IFA_STATUS.COMPLETE:
				return "Complete";
			case IFA_STATUS.INCOMPLETE:
				return "Incomplete";
			case IFA_QA_STATUS.APPROVED_BY_QA:
				return "Approved by QA";
			case IFA_QA_STATUS.IN_PROGRESS:
				return "In Progress";
			case IFA_STATUS.DELETED:
				return "Deleted";
		}

		throw new Error("Argument is not a valid Submission Status.");
	}}
});

export const IFA_PREPARER_TYPE = {
	INITIAL : 'INITIAL',
	ADDITIONAL : 'ADDITIONAL'
};
Object.defineProperties(IFA_PREPARER_TYPE, {

	isValid :{ value: function(val)
	{
		return !!Object.keys(this).includes(val);
	}},
	toString :{ value: function(val)
	{
		switch(val)
		{
			case IFA_PREPARER_TYPE.INITIAL:
				return "Initial";
			case IFA_PREPARER_TYPE.ADDITIONAL:
				return "Additional";
		}

		throw new Error("Argument is not a valid Submission Status.");
	}}
});


export const IFA_CONCUSSION_CAUSE = {
	BLUNT_OBJECT : 'BLUNT_OBJECT',
	FALL : 'FALL',
	FRAGMENT : 'FRAGMENT',
	VEHICLE_CRASH : 'VEHICLE_CRASH',
	ASSAULT : 'ASSAULT',
	SPORTS_INJURY : 'SPORTS_INJURY',
	GUN_SHOT_WOUND : 'GUN_SHOT_WOUND',
	BLAST : 'BLAST',
	DEMOLITIONS : 'DEMOLITIONS',
	WEAPONS : 'WEAPONS',
	OTHER : 'OTHER'
};
Object.defineProperties(IFA_CONCUSSION_CAUSE, {

	isValid :{ value: function(val)
	{
		return !!Object.keys(this).includes(val);
	}},
	toString :{ value: function(val)
	{
		switch(val)
		{
			case IFA_CONCUSSION_CAUSE.BLUNT_OBJECT:
				return "Blunt Object";
			case IFA_CONCUSSION_CAUSE.FALL:
				return "Fall";
			case IFA_CONCUSSION_CAUSE.FRAGMENT:
				return "Fragment";
			case IFA_CONCUSSION_CAUSE.VEHICLE_CRASH:
				return "Vehicle Crash";
			case IFA_CONCUSSION_CAUSE.ASSAULT:
				return "Assault";
			case IFA_CONCUSSION_CAUSE.SPORTS_INJURY:
				return "Sports Injury";
			case IFA_CONCUSSION_CAUSE.GUN_SHOT_WOUND:
				return "Gun Shot Wound";
			case IFA_CONCUSSION_CAUSE.BLAST:
				return "Explosion / Blast";
			case IFA_CONCUSSION_CAUSE.DEMOLITIONS:
				return "Breaching / Demolitions";
			case IFA_CONCUSSION_CAUSE.WEAPONS:
				return "Weapons / Munitions Firing";
			case IFA_CONCUSSION_CAUSE.OTHER:
				return "Other";
		}

		throw new Error("Argument is not a valid Concussion Cause.");
	}}
});

export const IFA_BLAST_GAUGE_COLOR = {
	RED : 'RED',
	YELLOW : 'YELLOW',
	FLASHING_GREEN : 'FLASHING_GREEN',
	GREEN : 'GREEN'
};
Object.defineProperties(IFA_BLAST_GAUGE_COLOR, {

	isValid :{ value: function(val)
	{
		return !!Object.keys(this).includes(val);
	}},
	toString :{ value: function(val)
	{
		switch(val)
		{
			case IFA_BLAST_GAUGE_COLOR.RED:
				return "Red";
			case IFA_BLAST_GAUGE_COLOR.YELLOW:
				return "Yellow";
			case IFA_BLAST_GAUGE_COLOR.FLASHING_GREEN:
				return "Flashing Green";
			case IFA_BLAST_GAUGE_COLOR.GREEN:
				return "Green";
		}

		throw new Error("Argument is not a valid IFA Blast Gauge Color.");
	}}
});

export const IFA_BLAST_GAUGE_LOCATION = {
	HEAD : 'HEAD',
	CHEST : 'CHEST',
	SHOULDER : 'SHOULDER'
};
Object.defineProperties(IFA_BLAST_GAUGE_LOCATION, {

	isValid :{ value: function(val)
	{
		return !!Object.keys(this).includes(val);
	}},
	toString :{ value: function(val)
	{
		switch(val)
		{
			case IFA_BLAST_GAUGE_LOCATION.HEAD:
				return "Head";
			case IFA_BLAST_GAUGE_LOCATION.CHEST:
				return "Chest";
			case IFA_BLAST_GAUGE_LOCATION.SHOULDER:
				return "Shoulder";
		}

		throw new Error("Argument is not a valid IFA Blast Gauge Wear Location.");
	}}
});

export const IFA_EQUIPMENT_SELECTED = {
	M8_PAPER : 'M8_PAPER',
	M9_TAPE : 'M9_TAPE',
	M256_KIT : 'M256_KIT',
	HAPSITE : 'HAPSITE',
	JCAD : 'JCAD',
	MULTIRAE : 'MULTIRAE',
	OTHER : 'OTHER'
};
Object.defineProperties(IFA_EQUIPMENT_SELECTED, {

	isValid :{ value: function(val)
	{
		return !!Object.keys(this).includes(val);
	}},
	toString :{ value: function(val)
	{
		switch(val)
		{
			case IFA_EQUIPMENT_SELECTED.M8_PAPER:
				return "M8 Paper";
			case IFA_EQUIPMENT_SELECTED.M9_TAPE:
				return "M9 Tape";
			case IFA_EQUIPMENT_SELECTED.M256_KIT:
				return "M256 Kit";
			case IFA_EQUIPMENT_SELECTED.HAPSITE:
				return "HAPSITE";
			case IFA_EQUIPMENT_SELECTED.JCAD:
				return "JCAD";
			case IFA_EQUIPMENT_SELECTED.MULTIRAE:
				return "MultiRAE";
			case IFA_EQUIPMENT_SELECTED.OTHER:
				return "Other";
		}

		throw new Error("Argument is not a valid Submission Status.");
	}}
});

export const IFA_EXPOSURE_ROUTE = {
	INHALATION : 'INHALATION',
	INGESTION : 'INGESTION',
	SKIN_ABSORPTION : 'SKIN_ABSORPTION',
	SKIN_OR_EYE_CONTACT : 'SKIN_OR_EYE_CONTACT'
};
Object.defineProperties(IFA_EXPOSURE_ROUTE, {

	isValid :{ value: function(val)
	{
		return !!Object.keys(this).includes(val);
	}},
	toString :{ value: function(val)
	{
		switch(val)
		{
			case IFA_EXPOSURE_ROUTE.INHALATION:
				return "Inhalation";
			case IFA_EXPOSURE_ROUTE.INGESTION:
				return "Ingestion";
			case IFA_EXPOSURE_ROUTE.SKIN_ABSORPTION:
				return "Skin Absorption";
			case IFA_EXPOSURE_ROUTE.SKIN_OR_EYE_CONTACT:
				return "Skin and / or Eye Contact";
		}

		throw new Error("Argument is not a valid Submission Status.");
	}}
});

export const IFA_SAMPLE_TYPE = {
	AIR : 'AIR',
	SOIL : 'SOIL',
	WATER : 'WATER',
	WIPE : 'WIPE',
	OTHER : 'OTHER'
};
Object.defineProperties(IFA_SAMPLE_TYPE, {

	isValid :{ value: function(val)
	{
		return !!Object.keys(this).includes(val);
	}},
	toString :{ value: function(val)
	{
		switch(val)
		{
			case IFA_SAMPLE_TYPE.AIR:
				return "Air";
			case IFA_SAMPLE_TYPE.SOIL:
				return "Soil";
			case IFA_SAMPLE_TYPE.WATER:
				return "Water";
			case IFA_SAMPLE_TYPE.WIPE:
				return "Wipe";
			case IFA_SAMPLE_TYPE.OTHER:
				return "Other";
	}

		throw new Error("Argument is not a valid Submission Status.");
	}}
});

export const IFA_ATTACHMENT_TYPE = {
	ROSTER : 'ROSTER',
	MEDSITREP : 'MEDSITREP',
	SENSORRAWDATA : 'SENSORRAWDATA',
	PHOTOS : 'PHOTOS',
	OTHER : 'OTHER',
	COMPLETEHRA : 'COMPLETEHRA'
};
Object.defineProperties(IFA_ATTACHMENT_TYPE, {

	isValid :{ value: function(val)
	{
		return !!Object.keys(this).includes(val);
	}},
	toString :{ value: function(val)
	{
		switch(val)
		{
			case IFA_ATTACHMENT_TYPE.ROSTER:
				return "Roster";
			case IFA_ATTACHMENT_TYPE.MEDSITREP:
				return "Medsitrep";
			case IFA_ATTACHMENT_TYPE.SENSORRAWDATA:
				return "Sensor Raw Data";
			case IFA_ATTACHMENT_TYPE.PHOTOS:
				return "Photos";
			case IFA_ATTACHMENT_TYPE.OTHER:
				return "Other";
	}

		throw new Error("Argument is not a valid Submission Status.");
	}}
});

export const IFA_CONCUSSIVE_PPE = {
	HELMET : 'HELMET',
	BREACHING_SHIELD : 'BREACHING_SHIELD',
	PRIMARY_HEARING_PROTECTION : 'PRIMARY_HEARING_PROTECTION',
	SECONDARY_HEARING_PROTECTION : 'SECONDARY_HEARING_PROTECTION',
	OTHER : 'OTHER'
};
Object.defineProperties(IFA_CONCUSSIVE_PPE, {

	isValid :{ value: function(val)
	{
		return !!Object.keys(this).includes(val);
	}},
	toString :{ value: function(val)
	{
		switch(val)
		{
			case IFA_CONCUSSIVE_PPE.HELMET:
				return "Helmet";
			case IFA_CONCUSSIVE_PPE.BREACHING_SHIELD:
				return "Breaching Shield";
			case IFA_CONCUSSIVE_PPE.PRIMARY_HEARING_PROTECTION:
				return "In Ear Hearing Protection (Primary)";
			case IFA_CONCUSSIVE_PPE.SECONDARY_HEARING_PROTECTION:
				return "Over Ear Hearing Protection (Secondary)";
			case IFA_CONCUSSIVE_PPE.OTHER:
				return "Other";
	}

		throw new Error("Argument is not a valid concussive PPE item.");
	}}
});

export const IFA_CHEMICAL_PPE = {
	GAS_MASK : 'GAS_MASK',
	SUIT : 'SUIT',
	GLOVES : 'GLOVES',
	BOOTS : 'BOOTS',
	OTHER : 'OTHER'
};
Object.defineProperties(IFA_CHEMICAL_PPE, {

	isValid :{ value: function(val)
	{
		return !!Object.keys(this).includes(val);
	}},
	toString :{ value: function(val)
	{
		switch(val)
		{
			case IFA_CHEMICAL_PPE.GAS_MASK:
				return "Gas Mask";
			case IFA_CHEMICAL_PPE.SUIT:
				return "Suit";
			case IFA_CHEMICAL_PPE.GLOVES:
				return "Gloves";
			case IFA_CHEMICAL_PPE.BOOTS:
				return "Boots";
			case IFA_CHEMICAL_PPE.OTHER:
				return "Other";
	}

		throw new Error("Argument is not a valid chemical PPE item.");
	}}
});

export const SAMPLING_REASON = 
{
	OEHSA : "OEHSA Exposure Pathway",
	CBRN : "CBRN Force Protection",
	Incident_Other : "Incident/Other"
};

//array setup, array type, fixed, mobile or combination of both
export const SITE_ARRAY_OPTION = 
{
	Yes : "Yes",
	No : "No",
	Combo : "Combo"
};

//constants for metadata sensor setup if the sensor will take single samples or datalog for samples
export const SENSOR_EMPLOYMENT_OPTION =
{
	SingleSample : "Single Sample(s)",
	Datalog : "Datalog"
};

//constants for metadata sensor setup choosing the sensor type to be added to an array
export const SENSOR_TYPE =
{
	JCAD : "JCAD",
	MultiRAE : "MultiRAE",
	Hapsite : "Hapsite"
};

//constants for dri exposure pathway selection
export const EXPOSURE_PATHWAY =
{
	NONE : "No Exposure Pathway",
	NewExposurePathway : "New Exposure Pathway",
	ExistingSensorPathway : "Existing Exposure Pathway"
};

//constants for issuance sensors

// @TODO
// Note, this is actually a mix of event types and device statuses, and should
// be refactored. 
export const ISS_DEVICE_STATUS = 
{
	ISSUED : "ISSUED",
	DAMAGED : "DAMAGED",
	NONSERVICEABLE : "NONSERVICEABLE",
	LOST : "LOST",
	DATAMISMATCH : "DATAMISMATCH",
	SERVICEABLE : "SERVICEABLE",
	DELETED:"DELETED"
};
Object.defineProperties(ISS_DEVICE_STATUS, {

	isValid :{ value: function(val)
	{
		return !!Object.keys(this).includes(val);
	}},
	toString :{ value: function(val)
	{
		switch(val)
		{
			case ISS_DEVICE_STATUS.ISSUED:
				return "ISSUED";
		}
		throw new Error("Argument is not a valid Device Status.");
	}}
});

//constants for issuance sensors
export const ISS_ASSOCIATION = 
{
	MANUAL : "MANUAL",
	SELECTION : "SELECTION",
	GPS : "GPS",
	SYSTEM : "SYSTEM",
	SCAN : "SCAN"
};
Object.defineProperties(ISS_ASSOCIATION, {

	isValid :{ value: function(val)
	{
		return !!Object.keys(this).includes(val);
	}},
	toString :{ value: function(val)
	{
		switch(val)
		{
			case ISS_ASSOCIATION.MANUAL:
				return "Manual";
			case ISS_ASSOCIATION.SELECTION:
				return "Selection";
			case ISS_ASSOCIATION.GPS:
				return "GPS";
			case ISS_ASSOCIATION.SYSTEM:
				return "System";
			case ISS_ASSOCIATION.SCAN:
				return "Scan";
		}
		throw new Error("Argument is not a valid Association type.");
	}}
});

//constants for issuance sensors
export const ISS_EVENT_TYPE = 
{
	ISSUED : "ISSUED",
	VERIFIED : "VERIFIED",
	LOST : "LOST",
	DESTROYED : "DESTROYED",
	DAMAGED : "DAMAGED",
	REPAIRED : "REPAIRED",
	INITIALIZED : "INITIALIZED",
	CALIBRATED : "CALIBRATED",
	RETURNED : "RETURNED",
	OTHER : "OTHER",
};
Object.defineProperties(ISS_EVENT_TYPE, {

	isValid :{ value: function(val)
	{
		return !!Object.keys(this).includes(val);
	}}
});

export const ISS_WEAR_LOCATION = 
{
	HEAD : "HEAD",
	CHEST : "CHEST",
	SHOULDER : "SHOULDER",
	LEFT_WRIST : "LEFT_WRIST",
	RIGHT_WRIST : "RIGHT_WRIST",
	OTHER : "OTHER",
};
Object.defineProperties(ISS_WEAR_LOCATION, {

	isValid :{ value: function(val)
	{
		return !!Object.keys(this).includes(val);
	}},
	toString :{ value: function(val)
		{
			switch(val)
			{
				case ISS_WEAR_LOCATION.HEAD:
					return "Head";
				case ISS_WEAR_LOCATION.CHEST:
					return "Chest";
				case ISS_WEAR_LOCATION.SHOULDER:
					return "Shoulder";
				case ISS_WEAR_LOCATION.LEFT_WRIST:
					return "Left Wrist";
				case ISS_WEAR_LOCATION.RIGHT_WRIST:
					return "Right Wrist";
				case ISS_WEAR_LOCATION.OTHER:
					return "Other";
			}
			throw new Error("Argument is not a valid Wear Location.");
		}},
	fromString :{ value: function(val)
	{
		val = val.toLowerCase();

		switch(val)
		{
			case "head":
				return ISS_WEAR_LOCATION.HEAD;
			case "chest":
				return ISS_WEAR_LOCATION.CHEST;
			case "shoulder":
				return ISS_WEAR_LOCATION.SHOULDER;
			case "left wrist":
				return ISS_WEAR_LOCATION.LEFT_WRIST;
			case "right wrist":
				return ISS_WEAR_LOCATION.RIGHT_WRIST;
			case "other":
				return ISS_WEAR_LOCATION.OTHER;
		}
		throw new Error("Argument is not a valid Wear Location.");
	}}
});

//constants for issuance sensor device types
export const ISS_DEVICE_TYPE_ID = 
{
	NONE : 0,
	BLAST_GAUGE : 1,
	JPDI : 2,
	PASSIVE_CHEMICAL_SAMPLER : 3
};
Object.defineProperties(ISS_DEVICE_TYPE_ID, {
	toString :{ value: function(val)
		{
			switch(val)
			{
				case ISS_DEVICE_TYPE_ID.NONE:
					return "None";
				case ISS_DEVICE_TYPE_ID.BLAST_GAUGE:
					return "Blast Gauge";
				case ISS_DEVICE_TYPE_ID.JPDI:
					return "JPDI";
				case ISS_DEVICE_TYPE_ID.PASSIVE_CHEMICAL_SAMPLER:
					return "Passive Chemical Sampler";
			}
			throw new Error("Argument is not a valid Device Type.");
		}},
		fromString :{ value: function(val)
		{
			val = val.toLowerCase();
	
			switch(val)
			{
				case "none":
					return ISS_DEVICE_TYPE_ID.NONE;
				case "blast gauge":
					return ISS_DEVICE_TYPE_ID.BLAST_GAUGE;
				case "jpdi":
					return ISS_DEVICE_TYPE_ID.JPDI;
				case "passive chemical sampler":
					return ISS_DEVICE_TYPE_ID.PASSIVE_CHEMICAL_SAMPLER;
			}
			throw new Error("Argument is not a valid Device Type.");
		}}
	});

//constants for dri exposure pathway selection
export const ASSESSMENT_TYPE =
{
	ACUTE_PEAK : 'acute_peak',
	ACUTE_AVERAGE : 'acute_average',
	CHRONIC_AVERAGE : 'chronic_average'
};