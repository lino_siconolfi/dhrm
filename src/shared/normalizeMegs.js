import { MEG } from "./MEG.js"

/**
 * This is basically a stub now, calling functionality in MEG.js
 * 
 * @param {object} megs Megs as returned from the database
 */
export function normalizeMegs(megs)
{
    return megs.map(meg=>{
        return MEG.normalize(meg);
    });
}

