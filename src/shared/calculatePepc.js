import { DimensionedValue } from "./DimensionedValue.js"
import { normalizeUnits } from "./normalizeUnits.js"

/**
 * In order to proceed with the pre-screen, there must be at leas tone reading 
 * which is a detection. 
 * 
 * @param {} readings 
 */
export function prescreen(readings)
{
    return readings.length && readings.reduce((result, current)=>{
        return result || current.concentration_below_threshold==0;
    }, false);
}

/**
 * Return the normalized peak concentration in a set of readings. This function
 * does not do any validation that the set of readings are comparable. That is 
 * up to the calling code. 
 * 
 * @param {[]} readings Array of readings.  
 * @returns DimensionedValue
 */
export function getPeakConcentration(readings, meg_context)
{
    return readings
        .map(el=>normalizeUnits(new DimensionedValue(el.concentration, el.units, el.concentration_below_threshold==0), meg_context))
        .reduce((max, cur)=>(!max || cur.value) > max.value ? cur : max);
}

/**
 * Return the normalized average concentration in a set of readings. 
 * 
 * @param {[]} readings Array of readings.  
 * @returns DimensionedValue
 */
export function getAvgConcentration(readings, meg_context)
{
    var dimensionedValues = readings
        .map(el => normalizeUnits(new DimensionedValue(el.concentration, el.units, el.concentration_below_threshold == 0), meg_context));
    var includeInvalid = true;

    var stats = dimensionedValues.reduce((total, cur)=>{
        if(cur.isValid || includeInvalid)
        {
            total.sum.value += cur.value;
            total.count++;
            if(cur.precision > total.precision)
            {
                total.precision = cur.precision;
            }
        }
        return total;
    }, {
        count : 0,
        precision : 0,
        sum : new DimensionedValue(0, dimensionedValues[0].units)
    });

    return new DimensionedValue(stats.sum.value / stats.count, stats.sum.units, true, stats.precision)
}

export function getDetectionFrequency(readings)
{
    return (readings.reduce((detections,cur)=>cur.concentration_below_threshold==0 ? detections+1 : detections, 0) / readings.length) * 100;
}

/**
 * Acute val is 14DayNEG except in the following cases: 
 * 
 * If the 14DayNEG equals the 1YearNEG or there is no 14DayNEG, use 8HourNEG
 * If there is no 8HourNEG, use 1HourNEG
 * 
 * Also important, but not yet implemented: 
 * If chemical warfare agent or particulate, use 24HourNEG
 * 
 * @param { DimensionedValue } value 
 * @param { []tg230Meg } megs 
 * @returns { Boolean }
 */
export function acutePrescreen(value, megs)
{
    // Get chronic meg for comparison
    var chronicMeg = megs.find(el=>el.parm=='1yearNEG');

    // Special handling for pm2.5 data
    if(megs[0].cas_number=="PM2.5" || megs[0].is_cwa)
    {
        var acuteMeg = megs.find(el=>el.parm=='24hourNEG');
        var audit = "If the chemical is a CWA or Particulate matter, use the 24h NEG MEG";
    }
    else 
    {
        // Case 1, using 14 day negligable
        var acuteMeg = megs.find(el=>el.parm=='14dayNEG');
        var audit = "Use the default selection of the 14d NEG MEG";

        // Case 2, using 8 hour negligable
        if (!acuteMeg || (chronicMeg && DimensionedValue.compare(acuteMeg.value, chronicMeg.value) === 0))
        {
            if (acuteMeg)
            {
                audit = "If the 14d NEG MEG is equal to the 1-year NEG MEG use the 8 hour NEG MEG";
            }
            else 
            {
                audit = "If there is no 14d NEG MEG use the 8 hour NEG MEG";
            }
            acuteMeg = megs.find(el => el.parm == '8hourNEG');
        }

        // Case 3, using 1 hour negligable
        if (!acuteMeg)
        {
            acuteMeg = megs.find(el=>el.parm=='1hourNEG');
            audit = "If there is no 8hour NEG MEG use the 1hour NEG MEG";
        }
    }

    // Error if no appropriate meg found. 
    if(!acuteMeg)
    {
        throw new Error("Acute MEG not found.");
    }
    
    return {
        meg : acuteMeg,
        audit : audit,
        exceeded : DimensionedValue.compare(
            value, 
            acuteMeg.value,
            acuteMeg
        ) >= 1
    };
}

/**
 * Chronic meg is always 1YearNEG
 * 
 * @TODO What do we do if there is no 1YearNEG for this chemical? 
 * 
 * @param {DimensionedValue} value 
 * @param {[]tg230Meg} megs 
 * @returns {Boolean}
 */
export function chronicPrescreen(value, megs)
{
    var chronicMeg = megs.find(el=>el.parm=='1yearNEG');

    if(!chronicMeg)
    {
        throw new Error("Chronic MEG not found.");
    }

    return {
        meg : chronicMeg,
        exceeded : DimensionedValue.compare(
            value, 
            chronicMeg.value,
            chronicMeg
        ) >= 1
    };
}

/**
 * @param {DimensionedValue} pepc 
 * @param {meg[]} megs 
 * @param {EXPOSURE_DURATION} duration 
 * @param {Boolean} acute 
 */
export function getComparisonMegForSeverity(pepc, megs, duration, chronic)
{
    // TG230 G.5 ~ 1 Year is not used for acute severity. 
    megs = chronic ? 
    megs.filter(meg=>meg.rawTimeframe=='1year') : 
    megs.filter(meg=>meg.rawTimeframe!='1year');

    // There are different processes depending on how complete the set of megs
    // for this chemical is. 
    let closestTimeframe = getClosestMegTimeframe(megs, duration);
    let timeframeMegs = chronic ? megs : megs.filter(meg=>meg.rawTimeframe == closestTimeframe.rawTimeframe);

    // TG230: "Unlike the other chemicals, there are multiple sets of MEGs available for these chemicals. 
    // That is, for each of the exposure durations of 10 minutes, 1 hour, 8 hours, and 24 hours, there are 
    // Negligible, Marginal, Critical, and sometimes Catastrophic MEGs. Severity should be ranked using the 
    // MEG exposure duration most closely aligned with the exposure duration experienced by the population."
    //
    // NOTE: In addition to CWAs, we use this process for any chemical with a complete set of megs for the 
    // timeframe matching the sample duration. 
    if(megs[0].is_cwa || timeframeMegs.length == 3 || chronic)
    {
        megs = timeframeMegs;
        
        // Sort megs in order of concentration
        megs.sort((a,b)=>{
            return DimensionedValue.compare(a.value, b.value)
        });

        // Find the index of the first meg (in sorted order) that is greater than our pepc
        var index = megs.findIndex(meg => DimensionedValue.compare(
            pepc,
            meg.value,
            meg
        ) < 0);

        // The sample is lower than the lowest severity meg. 
        if(index == 0)
        {
            return megs[0];
        }

        // The sample is higher than the highest severity meg
        else if(index == -1)
        {
            return megs[megs.length - 1];
        }

        // The sample is bounded by two megs
        else
        {
            return megs[index - 1];
        }
    }

    // Doing this the hard way. Could be a loop, but this makes it more 
    // explicit that it is following the tg230 decision tree. 
    var meg = megs.find(meg=>meg.parm=='1hourCRIT');
    if(meg && DimensionedValue.compare(pepc, meg.value) === 1)
    {
        return meg;
    }
    meg = megs.find(meg=>meg.parm=='1hourMARG');
    if(meg && DimensionedValue.compare(pepc, meg.value) === 1)
    {
        return meg;
    }
    meg = megs.find(meg=>meg.parm=='1hourNEG');
    if(meg && DimensionedValue.compare(pepc, meg.value) === 1)
    {
        return meg;
    }
    meg = megs.find(meg=>meg.parm=='8hourNEG');
    if(meg && DimensionedValue.compare(pepc, meg.value) === 1)
    {
        return meg;
    }
    meg = megs.find(meg=>meg.parm=='14dayNEG');
    if(meg && DimensionedValue.compare(pepc, meg.value) === 1)
    {
        return meg;
    }

    // Return 14 day neg if below 14 day neg. 
    return meg;
    

}

export function getBoundingMegsFromComparisonMeg(comparisonMeg, megs, duration)
{
    // TG230: Unlike the other chemicals, there are multiple sets of MEGs available for these chemicals. 
    // That is, for each of the exposure durations of 10 minutes, 1 hour, 8 hours, and 24 hours, there are 
    // Negligible, Marginal, Critical, and sometimes Catastrophic MEGs. Severity should be ranked using the 
    // MEG exposure duration most closely aligned with the exposure duration experienced by the population.
    let closestTimeframe = getClosestMegTimeframe(megs, duration);
    let timeframeMegs = megs.filter(meg => meg.rawTimeframe == closestTimeframe.rawTimeframe);
    if (comparisonMeg.is_cwa || timeframeMegs.length == 3)
    {
        megs = timeframeMegs;
    }

    // If the comparison meg is 1 year, this is a chronic comparison, and all we care about is 1year. 
    else if(comparisonMeg.rawTimeframe == '1year')
    {
        megs = megs.filter(meg => meg.rawTimeframe == '1year');
    }

    // This is an acute non-cwa comparison. Filter out the 1 year megs. 
    else 
    {
        megs = megs.filter(meg => meg.rawTimeframe != '1year');
    }

    // Sort megs in order of concentration
    megs.sort(
        (a, b) => {
            return DimensionedValue.compare(a.value, b.value)
        }
    );

    // Find the index of the first meg (in sorted order) that is greater than 
    // the given meg. 
    var index = megs.findIndex(meg => DimensionedValue.compare(
        comparisonMeg.value,
        meg.value,
        meg
    ) < 0);

    // The sample is higher than the highest severity meg
    if (index == -1)
    {
        return {
            upper : null,
            lower : comparisonMeg
        };
    }

    // The sample is bounded by two megs
    else
    {
        var upperIndex = index;
        while (megs[upperIndex + 1] && megs[upperIndex].rawSeverity == comparisonMeg.rawSeverity)
        {
            upperIndex++;
        }
        if (megs[upperIndex].rawSeverity == comparisonMeg.rawSeverity)
        {
            return {
                upper : null,
                lower : comparisonMeg
            }
        }
        else 
        {
            return {
                upper : megs[upperIndex],
                lower : comparisonMeg
            }
        }
    }
}


export function getClosestMegTimeframe(megs, timeframe)
{
    return megs.reduce((previous, current)=>{
        return (Math.abs(current.minutes-timeframe) < Math.abs(previous.minutes-timeframe)) ? 
            current : previous;
    });
}

/**
 * Calculate Duration of Exposure
 * 
 * TG230: Divide sample time by meg timeframe
 *     If the result is less than 1 score a 1
 *     If the result is greater than or equal to 1 and less than 3 score a 2
 *     If the result is greater than or equal to 3 score a 3
 * 
 * @param {*} megs 
 */
export function calculateDurationOfExposure(sample_time, megs)
{
    var meg = getClosestMegTimeframe(megs, sample_time);

    var ratio = sample_time / meg.minutes;
    var score = 0;
    if(ratio < 1)
    {
        score = 1;
    }
    else if (ratio >=1 && ratio < 3)
    {
        score = 2;
    }
    else 
    {
        score = 3;
    }
    return {
        value : timeframeFormat(sample_time) + ' / ' + meg.rawTimeframe,
        score : score
    }
}


/**
 * Calculate the degree of exposure
 * 
 * @param {DimensionedValue} pepc 
 * @param {DimensionedValue} boundingMegs 
 */
export function calculateDegreeOfExposure(pepc, boundingMegs, audit)
{
    var ret;

    // TG230: PEPC exceeds the highest severity MEG score 2
    if(!boundingMegs.upper)
    {
        ret = {
            value : "> 75%",
            score: 2
        }
        if ( audit )
        {
            ret.audit = "If the PEPC exceeds the highest severity MEG score 2";
        }
        return ret;
    }

    // TG230: PEPC lower than negligible MEG score 1
    if ( !boundingMegs.lower )
    {
        ret = {
            value : "< 25%",
            score: 1
        }
        if ( audit )
        {
            ret.audit = "If less than the lowest severity meg, score 1";
        }
        return ret;
    }

    // TG230: 
    //    PEPC is below the 25th percentile of the severity range - 1
    //    PEPC is at or between the 25th and 75th percentiles of the severity range - 2
    //    PEPC is above the 75th percentile of the severity range - 3
    var magnitude = pepc.value - boundingMegs.lower.value.value;
    var range = boundingMegs.upper.value.value-boundingMegs.lower.value.value;
    var percentage = Math.round(magnitude / range * 100);
    var score = 0;
    var value = '';
    var auditlog = `PEPC of ${pepc.value} is ${percentage}% of the range ${boundingMegs.lower.value.value} - ${boundingMegs.upper.value.value}. Score `;
    if ( percentage <= 25 )
    {
        score = 1;
        value = "< 25%"
    }
    else if ( percentage <= 75 )
    {
        score = 2;
        value = "25% to 75%"
    }
    else 
    {
        score = 3;
        value = "> 75%"
    }
    ret = {
        value : `${value}`,
        score : score
    }
    if ( audit )
    {
        ret.audit = auditlog + score;
    }
    
    return ret;
}

export function timeframeFormat(minutes, forceMinutes)
{
    if(minutes < 60 || forceMinutes)
    {
        return minutes + "min";
    }

    if(minutes < 1440)
    {
        return Math.round(minutes / 60) + "hour";
    }

    if(minutes < 1440*365)
    {
        return Math.round(minutes / 1440) + "day";
    }

    return Math.round(minutes / (1440*365)) + "year";
}

/**
 * Return the set of time weighted averages for the given meg timeframes. 
 * 
 * Assumes all values are in the same units. 
 * 
 * @param {*} datalog 
 * @param {*} megs 
 */
export function getTimeIntervalPeaks(sample, datalog, megs)
{
    let timeframes = [...new Set(megs.map(meg => meg.minutes))];
    return timeframes.map(minutes => {

        // There can be no time weighted average if the time step is smaller 
        // than the data step, or if the time step is larger than the dataset. 
        if(datalog[0].duration > minutes || sample.duration < minutes)
        {
            return {
                timeframe : minutes,
                concentration : null,
            }
        } 

        let window = Math.ceil(minutes / datalog[0].duration);
        let arr = [];
        let rolling = [];
        for(let i = 0; i < datalog.length; i++)
        {
            arr.push(datalog[i].concentration.value);
            if(arr.length == window)
            {
                rolling.push(arr.reduce((acc, cur) => acc + cur, 0)/window);
                arr.shift();
            }
        }

        return {
            timeframe : minutes,
            concentration : new DimensionedValue(rolling.reduce( (prev, cur) => cur > prev ? cur : prev, 0), datalog[0].concentration.units, true, sample.peak_concentration.precision)
        }
    });
}

/**
 * Take a DRI sample and return a faked data log with concentrations that conform 
 * to the sample peak and average concentrations. Usind a seeded RNG to ensure 
 * samples always return the same data log. 
 * 
 * @param {object} sample 
 */
export function fakeDataLog(sample)
{
    // We will fake one reading per minute
    let readings = [];
    seededRandom.seed = sample.id;
    let idx = Math.floor(seededRandom() * sample.duration);
    let sum = 0;
    let total = sample.duration * sample.average_concentration.value;
    for(let i = 0; i < sample.duration; i++)
    {
        let rand = seededRandom();
        sum += rand;
        readings.push({
            start_time : new Date(new Date(sample.start_date_time).valueOf() + (i*60000)),
            duration : 1,
            rand : i==idx ? null : rand,
            cas_number : sample.cas_number,
            units : sample.peak_concentration.units, 
            concentration : i==idx ? sample.peak_concentration.value : null
        });
    }

    return readings.map(reading=>{
        if(reading.rand)
        {
            reading.concentration = (reading.rand / sum) * total;
        }
        delete reading.rand;
        reading.concentration = new DimensionedValue(reading.concentration, reading.units);
        return reading;
    });


}

function seededRandom()
{
    seededRandom.seed = seededRandom.seed || 1;
    var x = Math.sin(seededRandom.seed++) * 10000;
    return x - Math.floor(x);
}
