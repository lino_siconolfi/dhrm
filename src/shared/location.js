import { Portable } from "./portable.js";
import * as MGRS from "./vendor/mgrs/mgrs.js"

/**
 * Manage locations in a consistant way across the JHRM application. Instances 
 * of Location serialized to JSON from the middletier and fetched via the Request 
 * class will automatically be recreated as instances of the Location class on 
 * the front end.
 * 
 * @property id
 * @property point 
 * @property gridRef 
 * @property name 
 * @property description 
 */
export class Location extends Portable
{
    /**
     * Construct a new Location object
     * 
     * @param {string | number | object} lon Either an MGRS coordinate string, 
     *     a numeric WGS84 Longitude, or an onject representing an instance of
     *     this class that has been JSON serialized and deserialized
     * @param {number} lat WGS84 Latitude
     */
    constructor(lon, lat)
    {
        super();

        // By definining these properties here, Portable can use them to determine
        // which properties to set from the passed in object
        this.lon = undefined;
        this.lat = undefined;
        this.mgrs = undefined;
        this.id = undefined; 
        this.name = undefined;
        this.description = undefined;

        if ( typeof lon === 'string' )
		{
            // This is MGRS. 
			this.gridRef = lon;
		}
		else if ( typeof lon === 'number' )
		{
            // This is a point. Save it and calculate MGRS. 
            this.point = [lon, lat];
		}
		else if ( typeof lon === 'object' )
		{
            // This an object. Attempt parsing. Use strict mode. 
			this.fromObject(lon, true);
		}
    }

    /**
     * @returns {string} MGRS grid reference
     */
    toString()
    {
        return this.mgrs || 'Unknown Location';
    }

    /**
     * Get point
     * 
     * Point is only saved on the object if it was explicitly set, otherwise
     * we use the MGRS coordinate as the primary source of truth. 
     * 
     * @returns void
     */
    get point()
    {
        if( typeof this.lon === 'number' )
        {
            return [this.lon, this.lat];
        }
        else 
        {
            return undefined;
        }
    }

    /**
     * Set point 
     * 
     * Updates MGRS based on the provided location. 
     * 
     * @param val {array} Standard point array of [lon, lat]
     * @returns void
     */
    set point(val)
    {
        if(!Array.isArray(val) || val.length != 2 || typeof val[0] != 'number' || typeof val[1] != 'number')
        {
            throw new Error("Point must be an array of longitude and latitude");
        }
        this.mgrs = MGRS.forward(val)
        this.lon = val[0];
        this.lat = val[1];
    }

    /**
     * Get gridRef 
     * 
     * @returns {string} MGRS grid reference
     */
    get gridRef()
    {
        return this.mgrs;
    }

    /**
     * Set gridRef
     * 
     * @param val MGRS coordinate string
     * @returns void
     */
    set gridRef(val)
    {
        let point = MGRS.toPoint(val);
        this.lat = point[1];
        this.lon = point[0]; 
        this.mgrs = val;
    }
}