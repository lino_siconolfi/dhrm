import { Portable } from "./portable.js";

/**
 * Represents an Enahnced HRA QA.
 */
export class EnhancedHraQa extends Portable
{
    constructor(obj)
    {
        super();

        // Set defaults. 
        this._source = {
            id : null,
            hra_id : null,
            hra_objective_comments : '',
            hra_objective_approve : false,
            hra_objective_edit : false,
            hazard_probability_comments : '',
            hazard_probability_approve : false,
            hazard_probability_edit : false,
            executive_summary_comments : '',
            executive_summary_approve : false,
            executive_summary_edit : false,
            fhp_recommendations_comments : '',
            fhp_recommendations_approve : false,
            fhp_recommendations_edit : false,
            samples_included_in_hra_comments : '',
            samples_included_in_hra_approve : false,
            samples_included_in_hra_edit : false,
        };
        Object.seal(this._source);

        if(obj) 
        {
            delete obj.PortableType;
            Object.assign(this._source, obj._source ? obj._source : obj);
        }
    }


    get id()
    {
        return this._source.id;
    }
    set id(val)
    {
        this._source.id = val;
    }

    get hra_id()
    {
        return this._source.hra_id;
    }
    set hra_id(val)
    {
        this._source.hra_id = val;
    }

    get hra_objective_comments()
    {
        return this._source.hra_objective_comments;
    }
    set hra_objective_comments(val)
    {
        this._source.hra_objective_comments = val.toString();
    }

    get hra_objective_approve()
    {
        return this._source.hra_objective_approve;
    }
    set hra_objective_approve(val)
    {
        this._source.hra_objective_approve = !!val;
    }

    get hra_objective_edit()
    {
        return this._source.hra_objective_edit;
    }
    set hra_objective_edit(val)
    {
        this._source.hra_objective_edit = !!val;
    }

    get hazard_probability_comments()
    {
        return this._source.hazard_probability_comments;
    }
    set hazard_probability_comments(val)
    {
        this._source.hazard_probability_comments = val.toString();
    }

    get hazard_probability_approve()
    {
        return this._source.hazard_probability_approve;
    }
    set hazard_probability_approve(val)
    {
        this._source.hazard_probability_approve = !!val;
    }

    get hazard_probability_edit()
    {
        return this._source.hazard_probability_edit;
    }
    set hazard_probability_edit(val)
    {
        this._source.hazard_probability_edit = !!val;
    }

    get executive_summary_comments()
    {
        return this._source.executive_summary_comments;
    }
    set executive_summary_comments(val)
    {
        this._source.executive_summary_comments = val.toString();
    }

    get executive_summary_approve()
    {
        return this._source.executive_summary_approve;
    }
    set executive_summary_approve(val)
    {
        this._source.executive_summary_approve = !!val;
    }

    get executive_summary_edit()
    {
        return this._source.executive_summary_edit;
    }
    set executive_summary_edit(val)
    {
        this._source.executive_summary_edit = !!val;
    }

    get fhp_recommendations_comments()
    {
        return this._source.fhp_recommendations_comments;
    }
    set fhp_recommendations_comments(val)
    {
        this._source.fhp_recommendations_comments = val.toString();
    }

    get fhp_recommendations_approve()
    {
        return this._source.fhp_recommendations_approve;
    }
    set fhp_recommendations_approve(val)
    {
        this._source.fhp_recommendations_approve = !!val;
    }

    get fhp_recommendations_edit()
    {
        return this._source.fhp_recommendations_edit;
    }
    set fhp_recommendations_edit(val)
    {
        this._source.fhp_recommendations_edit = !!val;
    }

    get samples_included_in_hra_comments()
    {
        return this._source.samples_included_in_hra_comments;
    }
    set samples_included_in_hra_comments(val)
    {
        this._source.samples_included_in_hra_comments = val.toString();
    }

    get samples_included_in_hra_approve()
    {
        return this._source.samples_included_in_hra_approve;
    }
    set samples_included_in_hra_approve(val)
    {
        this._source.samples_included_in_hra_approve = !!val;
    }

    get samples_included_in_hra_edit()
    {
        return this._source.samples_included_in_hra_edit;
    }
    set samples_included_in_hra_edit(val)
    {
        this._source.samples_included_in_hra_edit = !!val;
    }

    /**
     * Custom toJSON so we can use our fancy getters and setters. 
     * 
     * @param {string} key 
     */
    toJSON(key)
    {
        // Add type
        let ret = {
            ...this._source,
            PortableType : this.constructor.name
        };

        return ret;
    }
}
