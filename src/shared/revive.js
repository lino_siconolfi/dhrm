import { Location } from './location.js';
import { Datetime } from './datetime.js';
import { MEG } from './MEG.js';
import { DimensionedValue } from './DimensionedValue.js';
import { DriHra } from "./DriHra.js"
import { EnhancedHraQa } from "./ehra_qa.js"
import { IfaPersonnel, IfaConcussiveDetails, IfaChemicalDetails, IfaReport, IfaPreparer } from "./IFA.js"

    /**
     * Reviver function to be passed to JSON.parse, and will be called once for 
     * each value in the parsed JSON. 
     * 
     * This function will check to see if the value is a serilized subclass of
     * Portable and if so, will instantiate it as an object of that type. All 
     * other values are passed throgh unmodified. 
     * 
     * In order to work without hacks like eval, this function must import and 
     * explicitly check for all portable types. 
     * 
     * This was initially a static method on Portable, but that caused a 
     * circular dependency problem. 
     * 
     * @param {string} key 
     * @param {*} value 
     */
    export function revive(key, value)
    {
        if(value && typeof value === 'object' && value.PortableType)
        {
            switch(value.PortableType)
            {
                case 'Location':
                    return new Location(value);
                case 'Datetime':
                    return new Datetime(value);
                case 'DimensionedValue':
                    return new DimensionedValue(value);
                case 'MEG':
                    return new MEG(value);
                case 'DriHra':
                    return new DriHra(value);
                case 'IfaReport':
                    return new IfaReport(value);
                case 'IfaPreparer':
                    return new IfaPreparer(value);
                case 'IfaConcussiveDetails':
                    return new IfaConcussiveDetails(value);
                case 'IfaChemicalDetails':
                    return new IfaChemicalDetails(value);
                case 'EnhancedHraQa':
                    return new EnhancedHraQa(value);
                case 'IfaPersonnel':
                    return new IfaPersonnel(value);
            }
        }
        else 
        {
            return value;
        }
    }