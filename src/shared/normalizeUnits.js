import { DimensionedValue } from "./DimensionedValue.js"

/**
 * Normalize a dimensioned value of supported type in the context of a MEG.
 * 
 * Air samples are generally expressed in mass/volume or pp[m,b,t]. 
 * Soil samples are generally expressed in mass/mass or pp[m,b,t].
 * Water samples are generally expressed in mass/volume or pp[m,b,t]
 * 
 * MEGs are always in mg/m3 (air), mg/L (water), and mg/kg (soil), and these
 * are treated as our targets for normalization.
 * 
 *    ***************** ONLY AIR IS CURRENTLY SUPPORTED *****************
 * 
 * @param {dimensionedValue} val Value to be normalized
 * @param {Object} meg_context All values are normalized in the context of a meg, 
 *     which defines the medium (air, water, soil) and the chemical properties 
 *     (molecular weight). 
 * @throws Error
 * @returns {Number}
 */
export function normalizeUnits(val, meg_context)
{
	// This only works on dimensioned values 
	if ( !val.units )
	{
		throw new Error("Unsupported value.");
	}

	// There must be a meg context
	if ( !meg_context || !meg_context.value.units )
	{
		throw new Error("Unsupported meg_context");
	}

	// No conversion necessary if the units are the same as the meg
	if ( val.units == meg_context.value.units )
	{ 
		return val;
	}

	switch (meg_context.media.toLowerCase())
	{
		case media.air:
			return normalizeAir(val, meg_context);
		default:
			throw new Error(`Unsupported media: ${meg_context.media}`);
	}
};

function normalizeAir(val, meg_context)
{
	let partsFactor = ppFactors[val.units.toLowerCase()];
	if ( partsFactor )
	{
		if ( !meg_context.molecular_weight )
		{
			throw new Error(`Unsupported conversion: No molecular weight defined for ${meg_context.chemical_name}`)
		}

		let ppm = val.value / partsFactor;

		return new DimensionedValue({
			value : ppm * 0.0409 * meg_context.molecular_weight,
			units: meg_context.value.units,
			isValid: val.isValid,
			precision: val.precision,
			history : [...val.history, `Converted from ${val.units} to ${meg_context.value.units} with equation: 0.0409 x ppm (${ppm}) x ${meg_context.molecular_weight}`]
		});
	}
	else 
	{
		var [mass, volume] = val.units.split('/');

		// If the conversion factor is not found, we can't proceed
		if ( !massFactors[mass] || !volumeFactors[volume] )
		{
			throw new Error(`Unsupported units: ${val.units}`);
		}
	
		return new DimensionedValue({
			value : val.value * massFactors[mass] * volumeFactors[volume],
			units : meg_context.value.units,
			isValid : val.isValid,
			precision : val.precision,
			history : [...val.history, `Converted from ${val.units} to ${meg_context.value.units} with equation: value (${val.value}) x mass conversion factor ${massFactors[mass]} X volume conversion factor ${volumeFactors[volume]}`]
		});
	}
}

const massFactors = {
	kg : 1000000,
	g : 1000,
	mg : 1,
	ug : .001,
};

const volumeFactors = {
	m3 : 1,
	L : 1000
};

const media = {
	air : "air",
	water : "water",
	soil : "soil"
};

const ppFactors = {
	'ppm' : 1,
	'ppb' : 1000,
	'ppt' : 1000000
};