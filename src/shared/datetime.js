import { Portable } from "./portable.js";

/**
 * This class wrapes a Javascript date object, and adds serialization and 
 * deserialization as well as a custom toString method. Instances of Datetime 
 * serialized to JSON from the middletier and fetched via the Request class 
 * will automatically be recreated as instances of the Datetime class on the 
 * front end. 
 * 
 * let response = Request.get('mypath');
 * 
 * This allows validation:
 * 
 * if(response.date instanceOf 'Datetime')
 * 
 * And standard formatting without any additional work:
 * 
 * el.innerText = response.date 
 */
export class Datetime extends Portable
{
    /**
     * Construct a new Datetime object
     * 
     * @param {string} datetime MySQL datetime string. 
     */
    constructor(val, format)
    {
        super();

        this.format = format || Datetime.FORMATS.DATETIME;

        if(val instanceof Date)
        {
            this.date = val;
        }
        else if ( typeof val === 'string')
		{
            // Being passed a string. Always interperet as a zulu date, but pay
            // attention to whether it comes from MySQL or Display format
            let t = val.split(/[- :\/]/);
            if(val.indexOf('/') != -1)
            {
                this.date =new Date(Date.UTC(t[2], t[1]-1, t[0], t[3]||0, t[4]||0, t[5]||0))
            }
            else 
            {
                this.date =new Date(Date.UTC(t[0], t[1]-1, t[2], t[3]||0, t[4]||0, t[5]||0))
            }
		}
        else if(typeof val === 'number' )
        {
            this.date = new Date(val);
        }
		else if ( val instanceof Object )
		{
            // This an object. Attempt parsing. Use strict mode. 
            this.date = val.date ? new Date(val.date) : null;
            this.format = val.format;
        }
        else 
        {
            this.date = new Date();
        }
    }

    /**
     * @returns {string} Date in YYY-MM-DD as expected by date inputs. 
     */
    get dateValue()
    {
        return this.date ? this.date.toISOString().split("T")[0] : null;
    }

    /**
     * @returns {string} Time in HH:MM as expected by time inputs 
     */
    get timeValue()
    {
        if(this.format==Datetime.FORMATS.DATE)
        {
            throw new Error("Date format does not include time.")
        }
        return this.date ? this.date.toISOString().split("T")[1].substring(0, 5) : null;
    }

    /**
     * @returns {string} Time in YYY-MM-DD HH:MM::SS as expected by sql 
     */
    get sqlValue()
    {
        return this.date.toISOString().split("T").join(" ")
    }

    /**
     * @returns {string} Standard JHRM date format. 
     */
    toString()
    {
        if(this.date)
        {
            if(this.format == Datetime.FORMATS.DATE)
            {
                return  this.date.getUTCDate().toString().padStart(2, '0') + "/"
                + (this.date.getUTCMonth() + 1).toString().padStart(2, '0') + "/"
                + this.date.getUTCFullYear();
            }
            else 
            {
                return this.date.getUTCDate().toString().padStart(2, '0') + "/"
                + (this.date.getUTCMonth() + 1).toString().padStart(2, '0') + "/"
                + this.date.getUTCFullYear() + " "
                + this.date.getUTCHours().toString().padStart(2, '0') + ":"
                + this.date.getUTCMinutes().toString().padStart(2, '0');
            }
        }
        else 
        {
            return "N/A";
        }
    }

}

Datetime.FORMATS = {
    DATE : "DATE",
    DATETIME : "DATETIME"
}