import { EXPOSURE_DURATION, SEVERITY} from "./Constants.js"
import { DimensionedValue } from "./DimensionedValue.js"
import { Portable } from "./portable.js"

export const durationFromMiddle = {
	"10min" : EXPOSURE_DURATION.MINUTES_10,
	"1hour" : EXPOSURE_DURATION.HOUR_1,
	"8hour" : EXPOSURE_DURATION.HOURS_8,
	"24hour" : EXPOSURE_DURATION.HOURS_24,
	"7day" : EXPOSURE_DURATION.DAYS_7,
	"14day" : EXPOSURE_DURATION.DAYS_14,
	"1year" : EXPOSURE_DURATION.YEAR_1
};

let durationToMiddle = {};
durationToMiddle[EXPOSURE_DURATION.MINUTES_10] = "10min";
durationToMiddle[EXPOSURE_DURATION.HOUR_1] = "1hour";
durationToMiddle[EXPOSURE_DURATION.HOURS_8] = "8hour";
durationToMiddle[EXPOSURE_DURATION.HOURS_24] = "24hour";
durationToMiddle[EXPOSURE_DURATION.DAYS_14] = "14day";
durationToMiddle[EXPOSURE_DURATION.YEAR_1] = "1year";

export const severityFromMiddle = {
	"Negligible" : SEVERITY.Negligible,
	"Marginal" : SEVERITY.Marginal,
	"Critical" : SEVERITY.Critical,
	"Catastrophic" : SEVERITY.Catastrophic
};

let severityToMiddle = {};
severityToMiddle[SEVERITY.Negligible] = "Negligible";
severityToMiddle[SEVERITY.Marginal] = "Marginal";
severityToMiddle[SEVERITY.Critical] = "Critical";
severityToMiddle[SEVERITY.Catastrophic] = "Catastrophic";

/**
 * A standardized MEG object
 * 
 * @property {String} chemical_name 
 * @property {String} cas_number 
 * @property {DimensionedValue} value 
 * @property {String} basis 
 * @property {SEVERITY} severity 
 * @property {EXPOSURE_DURATION} timeframe 
 * @property {Number} minutes DEPRECATED
 * @property {String} parm 
 * @property {Bool} is_cwa 
 * @property {Number} molecular_weight 
 * @property {String} media 
 * @property {String} version
 */
export class MEG extends Portable
{
	constructor(obj)
	{
		super(obj);

		// Explicitly define all allowed properties. 
		this.chemical_name = undefined;
		this.cas_number = undefined;
		this.value = undefined;
		this.basis = undefined;
		this.severity = undefined;
		this.timeframe = undefined;
		this.minutes = undefined;
		this.parm = undefined;
		this.is_cwa = undefined;
		this.molecular_weight = undefined;
		this.media = undefined;
		this.version = undefined;
		Object.seal(this);

		if(obj)
		{
			this.fromObject(obj, true)
		}
	}

	get rawSeverity()
	{
		return severityToMiddle[this.severity];
	}

	get rawTimeframe()
	{
		return durationToMiddle[this.timeframe];
	}

	toString()
	{
		return `${this.parm} (${this.value})`;
	}

	/**
	 * 
	 * @param {*} meg MEG in tg230 data format. 
	 */
	static normalize(meg)
	{
		return new MEG({
			chemical_name :    meg.CHEMICAL_NAME,
			cas_number : meg.CASRN,
			value : new DimensionedValue(meg.MEG_VALUE, meg.UNITS),
			basis : meg.BASIS,
			severity : severityFromMiddle[meg.SEVERITY],
			timeframe : durationFromMiddle[meg.TIMEFRAME],
			minutes : durationFromMiddle[meg.TIMEFRAME],        
			parm : meg.MEG_PARM,
			is_cwa : !!meg.isCWA,
			molecular_weight : meg.molecular_weight,
			media : meg.MEDIA,
			version : meg.version_number
		});
	}
}