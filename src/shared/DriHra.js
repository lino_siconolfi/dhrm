import { Portable } from "./portable.js";
import { DimensionedValue } from './DimensionedValue.js';
import { Datetime } from './datetime.js'

/**
 * Very basic wrapper class for the DRI HRA object. 
 */
export class DriHra extends Portable
{
    constructor(obj)
    {
        super();
        this.fromObject(obj);
    }

    /**
     * Utility for transforming a raw collection of HRA fields in to a DRI 
     * HRA object. 
     * 
     * @param {Object} obj 
     */
    static normalize(obj)
    {
        return new DriHra({
            id : obj.id, 
            hra_objective : obj.hra_objective,
            selected_pathway : obj.selected_pathway,
            location : obj.location, 
            sap_file_name : obj.sap_file_name, 
            created_by : obj.created_by, 
            created_on : obj.created_on ? new Datetime(obj.created_on) : null, 
            approved_by : obj.approved_by, 
            last_edited_by : obj.last_edited_by, 
            last_edited_on : obj.last_edited_on ? new Datetime(obj.last_edited_on) : null, 
            uploaded_to_doehrs : obj.uploaded_to_doehrs, 
            qa_approved : obj.qa_approved, 
            status : obj.status, 
            sensor_type : obj.sensor_type, 
            media_type : obj.media_type, 
            population_at_risk : obj.population_at_risk, 
            individuals_at_risk : obj.individuals_at_risk, 
            chemical_name : obj.chemical_name, 
            cas_number : obj.cas_number, 
            acute_peak_pepc : null !== obj.acute_peak_pepc ? new DimensionedValue(
                obj.acute_peak_pepc, 
                obj.acute_peak_pepc_units
            ) : null,
            acute_peak_meg : null !== obj.acute_peak_meg ? new DimensionedValue(
                obj.acute_peak_meg, 
                obj.acute_peak_meg_units
            ) : null,
            acute_peak_meg_name : obj.acute_peak_meg_name, 

            acute_peak_risk : obj.acute_peak_risk, 
            acute_peak_confidence : obj.acute_peak_confidence, 
            acute_average_pepc : null !== obj.acute_average_pepc ? new DimensionedValue(
                obj.acute_average_pepc, 
                obj.acute_average_pepc_units
            ) : null,
            acute_average_meg : null !== obj.acute_average_meg ? new DimensionedValue(
                obj.acute_average_meg, 
                obj.acute_average_meg_units
            ) : null,
            acute_average_meg_name : obj.acute_average_meg_name, 
            acute_average_risk : obj.acute_average_risk, 
            acute_average_confidence : obj.acute_average_confidence, 
            pathway_id : obj.pathway_id, 
            pathway_name : obj.pathway_name
        });
    }
}