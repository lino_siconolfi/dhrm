import { Portable } from "./portable.js";
import { Datetime } from "./datetime.js";
import { Location } from "./location.js";
import { 
    IFA_INCIDENT_SCENARIO, 
    IFA_INCIDENT_TYPE,
    IFA_INCIDENT_CAUSE,
    IFA_TRINARY,
    IFA_SUBMISSION_STATUS,
    IFA_QA_STATUS,
    IFA_STATUS,
    IFA_PREPARER_TYPE,
    IFA_EXPOSURE_ROUTE,
    IFA_SAMPLE_TYPE,
    IFA_EQUIPMENT_SELECTED,
    IFA_CONCUSSION_CAUSE
} from "./Constants.js";

/**
 * Wrapper class for the IFA object. 
 * 
 * Has getters and setters to enforce property types and values. Might be 
 * overkill, but it will ensure that an IFA object edited or created on the
 * front end and sent over the wire will be valid. 
 */
export class IfaReport extends Portable
{
    constructor(obj)
    {
        super();

            // Set defaults. 
        this._source = {
            preparers : [],
            details : [],
            incident_type : [],
            status : IFA_STATUS.INCOMPLETE,
            submission_status : IFA_SUBMISSION_STATUS.NOT_SUBMITTED,
            qa_status : IFA_QA_STATUS.IN_PROGRESS,
            report_start_datetime : new Datetime()
        };

        if(obj) 
        {
            delete obj.PortableType;
            Object.assign(this._source, obj._source ? obj._source : obj);
        }
    }

    /**
     * Custom toJSON so we can use our fancy getters and setters. 
     * 
     * @param {string} key 
     */
    toJSON(key)
    {
        // Add type
        let ret = {
            ...this._source,
            PortableType : this.constructor.name
        };

        return ret;
    }

    /**
     * Get ID. 
     * 
     */
    get id()
    {
        return this._source.id;
    }

    /**
     * @param {int}
     */
    set id(val)
    {
        this._source.id = val;
    }
    
    /**
     * @return {IFA_SUBMISSION_STATUS}
     */
    get submission_status()
    {
        return this._source.submission_status;
    }

    /**
     * @param {IFA_SUBMISSION_STATUS}
     */
    set submission_status(val)
    {
        if(val && !IFA_SUBMISSION_STATUS.isValid(val))
        {
            throw new Error("Invalid value for submission_status.");
        }
        this._source.submission_status = val;
    }
    
    /**
     * @return {IFA_STATUS}
     */
    get status()
    {
        return this._source.status;
    }

    /**
     * @param {IFA_STATUS}
     */
    set status(val)
    {
        if(val && !IFA_STATUS.isValid(val))
        {
            throw new Error("Invalid value for status.");
        }
        this._source.status = val;
    }
    
    /**
     * @return {Datetime}
     */
    get report_start_datetime()
    {
        return this._source.report_start_datetime;
    }

    /**
     * @param {Datetime}
     */
    set report_start_datetime(val)
    {
        if(val && !(val instanceof Datetime))
        {
            throw new Error("report_start_datetime must be a Datetime object");
        }
        this._source.report_start_datetime = val;
    }
    
    /**
     * @return {Datetime}
     */
    get report_completion_datetime()
    {
        return this._source.report_completion_datetime;
    }

    /**
     * @param {Datetime}
     */
    set report_completion_datetime(val)
    {
        if(val && !(val instanceof Datetime))
        {
            throw new Error("report_completion_datetime must be a Datetime object");
        }
        this._source.report_completion_datetime = val;
    }
    
    /**
     * @return {string}
     */
    get name()
    {
        return this._source.name;
    }

    /**
     * @param {string} 
     */
    set name(val)
    {
        this._source.name = val;
    }

    /**
     * @return {string}
     */
    get short_name()
    {
        return this._source.short_name;
    }

    /**
     * @param {string} 
     */
    set short_name(val)
    {
        this._source.short_name = val;
    }
    
    /**
     * @return {Datetime}
     */
    get start_datetime()
    {
        return this._source.start_datetime;
    }

    /**
     * @param {Datetime}
     */
    set start_datetime(val)
    {
        if(val && !(val instanceof Datetime))
        {
            throw new Error("report_start_datetime must be a Datetime object");
        }
        this._source.start_datetime = val;
    }
    
    /**
     * @return {Datetime}
     */
    get end_datetime()
    {
        return this._source.end_datetime;
    }

    /**
     * @param {Datetime}
     */
    set end_datetime(val)
    {
        if(val && !(val instanceof Datetime))
        {
            throw new Error("report_start_datetime must be a Datetime object");
        }
        this._source.end_datetime = val;
    }
    
    /**
     * @return {string}
     */
    get ccir_sigact_num()
    {
        return this._source.ccir_sigact_num;
    }

    /**
     * @param {string}
     */
    set ccir_sigact_num(val)
    {
        this._source.ccir_sigact_num = val;
    }
    
    /**
     * @return {IFA_INCIDENT_SCENARIO}
     */
    get scenario()
    {
        return this._source.scenario;
    }

    /**
     * @param {IFA_INCIDENT_SCENARIO}
     */
    set scenario(val)
    {
        if(val && !IFA_INCIDENT_SCENARIO.isValid(val))
        {
            throw new Error("Invalid value for scenario");
        }
        this._source.scenario = val;
    }
    
    /**
     * @return {IFA_INCIDENT_TYPE[]}
     */
    get incident_type()
    {
        return this._source.incident_type;
    }

    /**
     * @param {IFA_INCIDENT_TYPE[]}
     */
    set incident_type(val)
    {
        if(!Array.isArray(val))
        {
            val = val ? [val] : [];
        }
        let valid = val.reduce((acc, cur)=>{
            return acc && IFA_INCIDENT_TYPE.isValid(cur)
        }, true);
        if(!valid)
        {
            throw new Error("Invalid value for incident_type.")
        }
        this._source.incident_type = val;
    }
    
    /**
     * @return {IFA_INCIDENT_CAUSE}
     */
    get cause()
    {
        return this._source.cause;
    }

    /**
     * @param {IFA_INCIDENT_CAUSE}
     */
    set cause(val)
    {
        if(val && !IFA_INCIDENT_CAUSE.isValid(val))
        {
            throw new Error("Invalid value for cause.");
        }
        this._source.cause = val;
    }
    
    /**
     * @return {string}
     */
    get cause_descr()
    {
        return this._source.cause_descr;
    }

    /**
     * @pram {string}
     */
    set cause_descr(val)
    {
        this._source.cause_descr = val;
    }
    
    /**
     * @return {string}
     */
    get summary()
    {
        return this._source.summary;
    }

    /**
     * @param {string}
     */
    set summary(val)
    {
        this._source.summary = val;
    }
    
    /**
     * @return {Location}
     */
    get location()
    {
        return this._source.location;
    }

    /**
     * @param {Location}
     */
    set location(val)
    {
        if(val && !(val instanceof Location))
        {
            throw new Error("location must be a Location object");
        }
        this._source.location = val;
    }
    
    /**
     * @return {string}
     */
    get units_involved()
    {
        return this._source.units_involved;
    }

    /**
     * @param {string}
     */
    set units_involved(val)
    {
        this._source.units_involved = val;
    }
    
    /**
     * @return {Boolean}
     */
    get were_you_at_incident_location()
    {
        return this._source.were_you_at_incident_location;
    }

    /**
     * @param {Boolean}
     */
    set were_you_at_incident_location(val)
    {
        this._source.were_you_at_incident_location = !!val;
    }

    /**
     * Read only property.
     * 
     * @return {IfaChemicalDetails|IfaConcussiveDetails[]}
     */
    get details() 
    {
        return this._source.details;
    }

    /**
     * Read only property.
     * 
     * @return {IfaChemicalDetails}
     */
    get chemical_details()
    {
        return this._source.details.find(detail=>detail instanceof IfaChemicalDetails);
    }

    /**
     * Read only property.
     * 
     * @return {IfaConcussiveDetails}
     */
    get concussive_details()
    {
        return this._source.details.find(detail=>detail instanceof IfaConcussiveDetails);
    }

    /**
     * Read only property.
     * 
     * @return {IfaPreparer[]}
     */
    get preparers() 
    {
        return this._source.preparers;
    }


    /**
     * Read only property.
     * 
     * Return the initial preparer who created the report. 
     * 
     * @return {IfaPreparer}
     */
    get initial_preparer()
    {
        return this._source.preparers.find(preparer=>preparer.type==IFA_PREPARER_TYPE.INITIAL);
    } 

    /**
     * Read only property.
     * 
     * Return the most recent preparer if known, or the initial preparer. 
     * 
     * @return {IfaPreparer}
     */
    get last_preparer()
    {
        let preparer = this._source.preparers.find(item=>item.id=this._source.last_edited_by);
        return preparer || this.initial_preparer;
    }

    /**
     * Read only property.
     * 
     * @return {IFA_TRINARY}
     */
    get images_associated()
    {
        // If the data has been loaded into the IFA, use that
        if(this.images && this.images.length)
        {
            return IFA_TRINARY.YES;
        }

        // Otherwise use the summary 
        return this._source.images_associated > 0 ? IFA_TRINARY.YES : IFA_TRINARY.NO;
    }

    /**
     * Read only property.
     * 
     * @return {IFA_TRINARY}
     */
    get personnel_associated()
    {
        // If the data has been loaded into the IFA, use that
        if(this.personnel && this.personnel.length)
        {
            return IFA_TRINARY.YES;
        }

        // Otherwise use the summary 
        return this._source.personnel_associated > 0 ? IFA_TRINARY.YES : IFA_TRINARY.NO;
    }

    /**
     * Read only property.
     * 
     * @return {Number}
     */
    get personnel_associated_count()
    {
        // If the data has been loaded into the IFA, use that
        if(this.personnel && this.personnel.length)
        {
            return this.personnel.length;
        }

        // Otherwise use the summary 
        return this._source.personnel_associated;
    }

    /**
     * Read only property.
     * 
     * @return {IFA_TRINARY}
     */
    get symptoms_associated()
    {
        // If the data has been loaded into the IFA, use that
        if(this.personnel && this.personnel.find(item=>item.signs_symptoms==IFA_TRINARY.YES))
        {
            return IFA_TRINARY.YES;
        }

        // Otherwise use the summary 
        return this._source.symptoms_associated > 0 ? IFA_TRINARY.YES : IFA_TRINARY.NO;
    }

    /**
     * Read only property.
     * 
     * @return {IFA_TRINARY}
     */
    get odors()
    {
        let chem = this.details.find(item=> item instanceof IfaChemicalDetails);
        return chem ? chem.any_odors_reported : null;
    }

    /**
     * Read only property.
     * 
     * @return {IFA_TRINARY}
     */
    get samples_collected()
    {
        let chem = this.details.find(item=> item instanceof IfaChemicalDetails);
        return chem ? chem.samples_collected : null
    }

    /**
     * Read only property.
     * 
     * @return {Number}
     */
    get symptoms_associated_count()
    {
        // If the data has been loaded into the IFA, use that
        if(this.personnel && this.personnel.length)
        {
            return this.personnel.filter(item=>item.signs_symptoms==IFA_TRINARY.YES).length;
        }

        // Otherwise use the summary 
        return this._source.symptoms_associated;
    }
    
    /**
     * Read only property.
     * 
     * @return {IFA_TRINARY}
     */
    get data_produced()
    {
        // If the data has been loaded into the IFA, use that
        if(this.details && this.details.length && this.details.find(item => item.field_mon_detec_data_collected == IFA_TRINARY.YES))
        {
            return IFA_TRINARY.YES;
        }

        // Otherwise use the summary 
        return this._source.data_produced > 0 ? IFA_TRINARY.YES : IFA_TRINARY.NO;
    }

    /**
     * @param {string}
     */
    set ppe_available(val)
    {
        this._source.ppe_available = val;
    }

    /**
     * @return {string}
     */
    get ppe_available()
    {
        return this._source.ppe_available;
    }

    /**
     * @param {string}
     */
    set ppe_decontamination(val)
    {
        this._source.ppe_decontamination = val;
    }

    /**
     * @return {string}
     */
    get ppe_decontamination()
    {
        return this._source.ppe_decontamination;
    }

    /**
     * @param {string}
     */
    set area_equip_decontamination(val)
    {
        this._source.area_equip_decontamination = val;
    }

    /**
     * @return {string}
     */
    get area_equip_decontamination()
    {
        return this._source.area_equip_decontamination;
    }

    /**
     * @param {string}
     */
    set countermeasures_used(val)
    {
        this._source.countermeasures_used = val;
    }

    /**
     * @return {string}
     */
    get countermeasures_used()
    {
        return this._source.countermeasures_used;
    }

    /**
     * @param {Boolean}
     */
    set action_risk_comms(val)
    {
        this._source.action_risk_comms = !!val;
    }

    /**
     * @return {Boolean}
     */
    get action_risk_comms()
    {
        return this._source.action_risk_comms;
    }

    /**
     * @param {string}
     */
    set risk_comms_descr(val)
    {
        this._source.risk_comms_descr = val;
    }

    /**
     * @return {string}
     */
    get risk_comms_descr()
    {
        return this._source.risk_comms_descr;
    }

    /**
     * @param {Boolean}
     */
    set action_gen_summary(val)
    {
        this._source.action_gen_summary = !!val;
    }

    /**
     * @return {Boolean}
     */
    get action_gen_summary()
    {
        return this._source.action_gen_summary;
    }

    /**
     * @param {string}
     */
    set gen_summary_descr(val)
    {
        this._source.gen_summary_descr = val;
    }

    /**
     * @return {string}
     */
    get gen_summary_descr()
    {
        return this._source.gen_summary_descr;
    }

    /**
     * @param {Boolean}
     */
    set action_other(val)
    {
        this._source.action_other = !!val;
    }

    /**
     * @return {Boolean}
     */
    get action_other()
    {
        return this._source.action_other;
    }

    /**
     * @param {string}
     */
    set other_descr(val)
    {
        this._source.other_descr = val;
    }

    /**
     * @return {string}
     */
    get other_descr()
    {
        return this._source.other_descr;
    }
 
    /**
     * @param {IfaPreparer}
     */
    addPreparer(preparer)
    {
        if(!(preparer instanceof IfaPreparer))
        {
            throw new Error("Not a valid IfaPreparer.")
        }
        this._source.preparers.push(preparer);
    }  
    
    /**
     * @param {IfaChemicalDetails | IfaConcussiveDetails}
     */
    addDetails(details)
    {
        if(!(details instanceof IfaChemicalDetails) && !(details instanceof IfaConcussiveDetails))
        {
            throw new Error("Not valid IfaDetails.")
        }
        this._source.details.push(details);
    }  

    /**
     * Utility for transforming a raw collection of IFA fields in to a IFA 
     * object with fields of the correct types. 
     * 
     * This method will not throw if a field has an invalid type as it is not 
     * intended for input validation, but for normalizing the results of a 
     * database query or API response. For validation, use the setters. 
     * 
     * If a field has an invalid value it will be set to null. 
     * 
     * @param {Object} obj 
     */
    static normalize(obj)
    {
        return new IfaReport({

            // Internal and metadata fields. 
            id : obj.id,
            qa_status : IFA_QA_STATUS.isValid(obj.qa_status) ? obj.qa_status : null,
            submission_status : IFA_SUBMISSION_STATUS.isValid(obj.submission_status) ? obj.submission_status : null, 
            
            // General information
            status : IFA_STATUS.isValid(obj.status) ? obj.status : null,
            report_start_datetime : new Datetime(obj.report_start_datetime),
            report_completion_datetime : new Datetime(obj.report_completion_datetime),
            last_edited_by : obj.last_edited_by,
            last_edited_on : obj.last_edited_on,

            // Incident information
            name : obj.name,
            short_name : obj.short_name,
            start_datetime : new Datetime(obj.start_datetime),
            end_datetime : new Datetime(obj.end_datetime),
            ccir_sigact_num : obj.ccir_sigact_num,
            scenario : IFA_INCIDENT_SCENARIO.isValid(obj.scenario) ? obj.scenario : null,
            
            incident_type : obj.incident_type.split(',').filter(val=>IFA_INCIDENT_TYPE.isValid(val)),
            cause : IFA_INCIDENT_CAUSE.isValid(obj.cause) ? obj.cause : null,
            cause_descr : obj.cause_descr,
            summary : obj.summary,

            // Location
            location : obj.mgrs ? new Location(obj.mgrs) : 
                obj.longitude ? new Location(obj.longitude, obj.latitude) :
                null,
            map_image_available : !!obj.map_image_available,

            // Details. 
            units_involved : obj.units_involved,

            // Other fields
            were_you_at_incident_location : obj.were_you_at_incident_location,

            // Summary fields. 
            images_associated  : obj.images_associated,
            personnel_associated  : obj.personnel_associated,
            symptoms_associated  : obj.symptoms_associated,
            data_produced : obj.data_produced,

            //hazard mitigation
            ppe_available : obj.mitigation_ppe,
            ppe_decontamination : obj.mitigation_personal_decontamination,
            area_equip_decontamination : obj.mitigation_area_decontamination,
            countermeasures_used : obj.mitigation_medical_countermeasures,
            action_risk_comms : obj.action_risk_communication,
            risk_comms_descr : obj.action_risk_communication_descr,
            action_gen_summary : obj.action_summary_of_exposures,
            gen_summary_descr : obj.action_summary_of_exposures_desc,
            action_other : obj.action_other,
            other_descr : obj.action_other_desc

        });
    }
}

export class IfaPreparer extends Portable
{
	constructor(obj)
	{
		super();

		// Explicitly define all allowed properties. 
        this.id = undefined;
        this.ifa_incident_id = undefined;
        this.type = undefined;
        this.name = undefined;
        this.email = undefined;
        this.phone_num = undefined;
        this.unit = undefined;
		Object.seal(this);

		if(obj)
		{
			this.fromObject(obj, true)
		}
    }
}

export class IfaChemicalDetails extends Portable
{
	constructor(obj)
	{
		super();

		// Explicitly define all allowed properties. 
        this.id = undefined;
        this.ifa_incident_id = undefined;
        this.exposure_routes = undefined;
        this.any_odors_reported = undefined;
        this.any_odors_reported_descr = undefined;
        this.field_mon_detec_data_collected = undefined;
        this.field_mon_detec_data_collected_descr = undefined;
        this.field_mon_detec_data_collected_by = undefined;
        this.equipment_selected = undefined;
        this.equipment_selected_descr = undefined;
        this.samples_collected = undefined;
        this.samples_collected_descr = undefined;
        this.samples_collected_by = undefined;
        this.samples_collected_type = undefined;
        this.samples_collected_type_descr = undefined;
        this.samples_collected_sent = undefined;
		Object.seal(this);

		if(obj)
		{
			this.fromObject(obj, true)
		}
    }

    static normalize(obj)
    {
        return new IfaChemicalDetails({
            id : obj.id,
            ifa_incident_id : obj.ifa_incident_id,
            exposure_routes : obj.exposure_routes.split(',').filter(item=>IFA_EXPOSURE_ROUTE.isValid(item)),
            any_odors_reported : IFA_TRINARY.isValid(obj.any_odors_reported) ? obj.any_odors_reported : null,
            any_odors_reported_descr : obj.any_odors_reported_descr,
            field_mon_detec_data_collected : IFA_TRINARY.isValid(obj.field_mon_detec_data_collected) ? obj.field_mon_detec_data_collected : null,
            field_mon_detec_data_collected_descr : obj.field_mon_detec_data_collected_descr,
            field_mon_detec_data_collected_by : obj.field_mon_detec_data_collected_by,
            equipment_selected : obj.equipment_selected.split(',').filter(item=>IFA_EQUIPMENT_SELECTED.isValid(item)),
            equipment_selected_descr : obj.equipment_selected_descr,
            samples_collected : IFA_TRINARY.isValid(obj.samples_collected) ? obj.samples_collected : null,
            samples_collected_descr : obj.samples_collected_descr,
            samples_collected_by : obj.samples_collected_by,
            samples_collected_type : obj.samples_collected_type.split(',').filter(item=>IFA_SAMPLE_TYPE.isValid(item)),
            samples_collected_type_descr : obj.samples_collected_type_descr,
            samples_collected_sent : obj.samples_collected_sent,
        });
    }
}

export class IfaConcussiveDetails extends Portable
{
	constructor(obj)
	{
		super();

		// Explicitly define all allowed properties. 
        this.id = undefined;
        this.ifa_incident_id = undefined;
        this.cause = undefined;
        this.cause_descr = undefined;
        this.cause_est_distance = undefined;
        this.field_mon_detec_data_collected = undefined;
        this.field_mon_detec_data_collected_descr = undefined;
        this.field_mon_detec_data_collected_by = undefined;
		Object.seal(this);

		if(obj)
		{
			this.fromObject(obj, true)
		}
    }

    static normalize(obj)
    {
        return new IfaConcussiveDetails({
            id : obj.id,
            ifa_incident_id : obj.ifa_incident_id,
            cause : obj.cause.split(',').filter(item=>IFA_CONCUSSION_CAUSE.isValid(item)),
            cause_descr : obj.cause_descr,
            cause_est_distance : obj.cause_est_distance,
            field_mon_detec_data_collected : IFA_TRINARY.isValid(obj.field_mon_detec_data_collected) ? obj.field_mon_detec_data_collected : null,
            field_mon_detec_data_collected_descr : obj.field_mon_detec_data_collected_descr,
            field_mon_detec_data_collected_by : obj.field_mon_detec_data_collected_by,
        });
    }
}

export class IfaPersonnel extends Portable
{
	constructor(obj)
	{
		super();

		// Explicitly define all allowed properties. 
        this.id = undefined;
        this.ifa_incident_id = undefined;
        this.ifa_personnel_id = undefined;
        this.name = undefined;
        this.ssn = undefined;
        this.dodid = undefined;
        this.isfn = undefined;
        this.unit = undefined;
        this.no_acute_symptoms = undefined;
        this.signs_symptoms = undefined;
        this.ppe_worn = undefined;
        this.concussive_ppe_worn = undefined;
        this.chemical_ppe_worn = undefined;
        this.detector_sensor_data = undefined;
        this.concussive_signs_symptoms = undefined;
        this.chemical_signs_symptoms = undefined;
        this.concussive_detector_sensor_data = undefined;
        this.concussive_medical_information = undefined;
        this.chemical_detector_sensor_data = undefined;
		Object.seal(this);

		if(obj)
		{
			this.fromObject(obj, true)
		}
    }

    static normalize(obj)
    {
        return new IfaPersonnel({
            id : obj.id,
            ifa_incident_id : obj.ifa_incident_id,
            ifa_personnel_id : obj.ifa_personnel_id,
            name : obj.name,
            ssn : obj.ssn,
            dodid : obj.dodid,
            isfn : obj.isfn,
            unit : obj.unit,
            no_acute_symptoms : !!obj.no_acute_symptoms,
            signs_symptoms : IFA_TRINARY.isValid(obj.signs_symptoms) ? obj.signs_symptoms : null,
            ppe_worn : IFA_TRINARY.isValid(obj.ppe_worn) ? obj.ppe_worn : null,
            detector_sensor_data : IFA_TRINARY.isValid(obj.detector_sensor_data) ? obj.detector_sensor_data : null
        });
    }
}