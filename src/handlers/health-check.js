var config = require('config');
var db = require('src/util/db');

/**
 * Very simple health check, fails if server is not running. 
 */
exports.simple = function (req, res)
{
	res.end('yes');
};

/**
 * Full health check, tried to connect to DB
 */
exports.full = async function (req, res)
{
	try 
	{	
		var test = await db.raw("select 1")
		res.end('yes');
	}
	catch(e)
	{
		// Database error 
		res.sendStatus(500);
		return;
	}
};
