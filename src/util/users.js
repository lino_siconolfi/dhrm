module.exports = [{
    first_name : "Joe",
    last_name : "Smith",
    email : 'jsmith@example.com',
    phone : '555-1414',
    unit : 'Charlie',
    username : "oeh_user",
    role :  
    {
        level : 1,
        description : "full access, read, write",
        hra_qa : false
    },
    password :  "Password1"
},
{
    first_name : "Johns Hopkins",
    last_name : "University",
    email : 'jhopkins@example.com',
    phone : '555-1313',
    unit : 'Bravo',
    username : "JHU_User",
    role : 
    {
        level : 1,
        description : "full access, read only for individual health record",
        hra_qa : false
    },
    password : "JHU_Password1"
},
{
    first_name : "John",
    last_name : "Doe",
    email : 'jdoe@example.com',
    phone : '555-1212',
    unit : 'Alpha',
    username : "administrator",
    role : 
    {
        level : 4,
        description : "admin/full access",
        hra_qa : false
    },
    password : "Password4"
},
{
    first_name : "Jane",
    last_name : "Doe",
    email : 'jdoe@example.com',
    phone : '555-1212',
    unit : 'Alpha',
    username : "qa",
    role : 
    {
        level : 4,
        description : "admin/full access",
        hra_qa : true
    },
    password : "Password4"
}];