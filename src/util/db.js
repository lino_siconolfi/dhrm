var config = require('config');

let connection = Object.assign({},config.get('db.connection'));
connection.timezone = 'UTC';

// Force DATETIME strings from MySQL to be interpereted as UTC, regardless of
// the timezone where Node is running. This ensures that all devs will see the
// same output even if they are running the code locally.
connection.typeCast = function (field, next) {
  if (field.type == 'DATETIME' || field.type == 'DATE') {
	  let t = (field.string() || '').split(/[- :]/);
      return new Date(Date.UTC(t[0], t[1]-1, t[2], t[3]||0, t[4]||0, t[5]||0));
  }
  return next();
}

const knex = require('knex')({
	  client: 'mysql',
	  connection: connection
});

knex.raw("SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))").then(function(){
   // Known empty function
}).catch(function(){
  // Known empty function
});

module.exports = knex;
