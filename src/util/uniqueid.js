const alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
const { customAlphabet } = require('nanoid');
const nanoid = customAlphabet(alphabet, 10)

module.exports = function(prefix)
{
    prefix = prefix || '';
    return prefix + nanoid();
}