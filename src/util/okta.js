const config = require('config');
const OktaJwtVerifier = require('@okta/jwt-verifier');
const verifier = new OktaJwtVerifier({
  issuer: config.get('security.okta.issuer')
});

module.exports = function authenticationRequired(req, res, next) 
{
    const match = (req.headers.authorization || '').match(/Bearer (.+)/);
  
    if (!match) 
    {
        return res.status(401).send('Unauthorized');
    }
  
    const accessToken = match[1];
  
    return verifier.verifyAccessToken(accessToken, config.get('security.okta.audience'))
    .then((jwt) => {
        req.jwt = jwt;
        next();
    })
    .catch((err) => {
        res.status(401).send(err.message);
    });
  }