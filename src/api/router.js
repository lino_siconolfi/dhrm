var express = require('express');
var users = require('../util/users');
var router = express.Router();
var config = require('config');
var bodyParser = require('body-parser');
var blastRouter = require('./blast/router');
var apiIssuanceRouter = require('./issuance/router');
var enhancedHRA = require('./enhancedhra/router');
var rapidHRA = require('./rapidhra/router');
var rd230Router = require('./rd230_meg/router');
var smartabaseRouter = require('./smartabase/router');
var metadata = require('./metadata/router');
var ifa = require('./ifa/router');
var wfl = require('./wfl/router');

const db = require('src/util/db');
const createError = require('http-errors');
const esmImport = require('esm')(module);
const { revive } = esmImport('../shared/revive.js');

// Middleware that runs for all API requests. For example, API access logging.


// All API requests expect JSON in the body. This middleware parses the posted
// JSON and provides the result at req.body. The passed "revive" function is
// responsible for turning serialized instances of classes which inherit from
// Portable back into proper instances of themselves instead of generic Object
// instances.
router.use(bodyParser.json({
	reviver : revive
}));


// Define the API base route
router.get('/', function (req, res) {
  res.json({
  	name : config.get('api.name'),
  	version : config.get('api.version')
  })
})

// Mount Blast API Endpoints
router.use('/blast', blastRouter);

// Mount Issuance Tool API Endpoints
router.use('/issuance', apiIssuanceRouter);

// Mount Enhanced HRA API Endpoints
router.use('/enhancedhra', enhancedHRA);

// Mount Rapid HRA API Endpoints
router.use('/rapidhra', rapidHRA);

// Mount rd230 API Endpoints
router.use('/rd230_meg', rd230Router);

// Mount metadata Endpoints
router.use('/smartabase', smartabaseRouter);

// Mount metadata Endpoints
router.use('/metadata', metadata);

// Mount ifa Endpoints
router.use('/ifa', ifa);

// Mount wfl Endpoints
router.use('/wfl', wfl);

// Port of login code from Spring
router.post('/login', function(req, res){
	let ret = new Object();
	ret.success = false;

	users.forEach(
		function(user)
		{
			if(req.body.username == user.username)
			{
				if(req.body.password == user.password)
				{
					ret.success = true;
					ret.username = user.username;
					ret.first_name = user.first_name;
					ret.last_name = user.last_name;
					ret.role = user.role;
				}
			}
		}
	);

	res.json(ret);
})

// Return the current user based on the login cookie. Do not return password
// or role information.
router.get('/user', function(req, res, next){
	let user = users.find(user=>user.username==req.cookies.user_name);
	if(user)
	{
		let clone = JSON.parse(JSON.stringify(user));
		delete clone.password;
		// temporary fix for accessing user role for QA table
		// delete clone.role;
		return res.json(clone);
	}
	return next(new createError.NotFound("User Not found."));
});

router.get('/version', async function(req, res){

	try
	{
		let packageConfig = require("./../../package.json");
		let db_version = await db.raw(`
		SELECT MD5(GROUP_CONCAT(db_version)) as v from jhrm
		`);

		res.json({
			version : packageConfig.version,
			db_version : db_version[0][0].v.substring(5,15)
		});
	}
	catch(err)
	{
		res.status(500).send(err.message);
	}
})

module.exports = router
