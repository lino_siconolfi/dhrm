const express = require('express');
const router = express.Router();
const db = require('src/util/db');
const createError = require('http-errors');
const papa = require('papaparse');
const adm = require('adm-zip');
const esmImport = require('esm')(module);
const { Datetime } = esmImport('../../shared/datetime');
const SmartabaseApi = require('src/smartabase/api-library');
var config = require('config');

/**
 * This function will set up the base query for blast gauge events and event 
 * data. 
 * 
 * You will then need to add where, column, and group clauses as required
 * for your specific use. 
 * 
 * This does not run the query! It only creates it. You must then await or 
 * .then() to execute. 
 * 
 * @param {Boolean} includeRawData 
 * @returns Knex Query Object
 */
 function blastGaugeEventQuery(includeRawData)
 {
    let query = db('bgd_event')
        .join('bgd_device_data', 'bgd_device_data.bgd_event_id', 'bgd_event.id')
        .join('bgd_device', 'bgd_device_data.bgd_device_id', 'bgd_device.id')
        .leftJoin('iss_device', 'bgd_device.iss_device_serial_number', 'iss_device.serial_number')
        .leftJoin('iss_event as iss_event_issued', 'iss_event_issued.id', db.raw(`
                SELECT id
                FROM iss_event e
                WHERE e.iss_device_id = iss_device.id
                AND e.date < bgd_event.event_date_time
                ORDER BY e.date DESC
                LIMIT 1
        `).wrap('(',')'))
        .leftJoin('iss_personnel', function(){
            this.on('iss_personnel.id', 'iss_event_issued.iss_personnel_id');
            this.andOn('iss_event_issued.type', db.raw("?", 'ISSUED'));
        })

    if(includeRawData)
    {
        query.join('bgd_event_raw_data', 'bgd_event_raw_data.bgd_device_data_id', 'bgd_device_data.id')
    }

    return query;
}

/**
 * This function will set up the base query for blast gauge events and event 
 * data with internal B3 fields for issuance informaton.
 * 
 * You will then need to add where, column, and group clauses as required
 * for your specific use. 
 * 
 * This does not run the query! It only creates it. You must then await or 
 * .then() to execute. 
 * 
 * @param {Boolean} includeRawData 
 * @returns Knex Query Object
 */
function blastGaugeConquerQuery()
{
    return db('bgd_event')
    .join('bgd_device_data', 'bgd_device_data.bgd_event_id', 'bgd_event.id')
    .join('bgd_device', 'bgd_device_data.bgd_device_id', 'bgd_device.id')
    .join('bgd_personnel', 'bgd_event.bgd_personnel_id',  'bgd_personnel.id')
    .column({
        "event_id" : "bgd_event.id",
        "device_data_id" : "bgd_device_data.id",
        "device_id" : "bgd_device.id",
        'dod_id' : db.raw("REPLACE(REPLACE(bgd_personnel.dod_id,'EDI-','9999999'),'CODID-','9999999')"),
        'display_name' : "bgd_personnel.display_name",
        'unit_name' : "bgd_personnel.unit_name",
        'event_date_time' : "bgd_event.event_date_time",
        'blast_gauge_serial_number' : "bgd_device.iss_device_serial_number",
        'mounting_location' : "bgd_device.mounting_location",
        "uid" : "bgd_device.uid",
        "firmware_rev" : "bgd_device.firmware_rev",
        "flag" : "bgd_device_data.real_notreal_flag",
        "raw" : "bgd_device_data.raw_data_exists"
    });
} 


/**
 * @api {get} /api/blast/export Blast gauge data with JHEM issuance information
 * @apiDescription Return blast gauge events in CSV format with DODID and Name as set in issuance tool
 * @apiGroup Blast
 * @apiSuccess Zip file
 */
router.get('/export', async function(req, res, next)
{
    try
    {	
        // Get all events and their associated metadata, filtering out ones 
        // marked as not real. 
        let event_query = blastGaugeEventQuery()
            .column({
                "event_id" : "bgd_event.id",
                "device_data_id" : "bgd_device_data.id",
                "device_id" : "bgd_device.id",
                'dod_id' : "iss_personnel.dodid",
                'display_name' : db.raw("CONCAT(iss_personnel.first_name, ' ',iss_personnel.last_name)"),
                'unit_name' : "iss_personnel.unit",
                'event_date_time' : "bgd_event.event_date_time",
                'blast_gauge_serial_number' : "iss_device.serial_number",
                'mounting_location' : "iss_event_issued.wear_location",
                "uid" : "bgd_device.uid",
                "firmware_rev" : "bgd_device.firmware_rev",
                "flag" : "bgd_device_data.real_notreal_flag",
                "raw" : "bgd_device_data.raw_data_exists"
            });

        if(req.query.from_date)
        {
            event_query.whereRaw("DATE(??) >= ?", ["bgd_event.event_date_time", req.query.from_date]);
        }

        if(req.query.to_date)
        {
            event_query.whereRaw("DATE(??) <= ?", ["bgd_event.event_date_time", req.query.to_date]);
        }

       if(req.query.download_date)
       {
           // Note that this compares only the date portion of the datetime column. 
            event_query.whereRaw("DATE(??) = ?", ["bgd_event.download_date", req.query.download_date]);
       }

        if(req.query.unit)
        {
            event_query.where("iss_personnel.unit", req.query.unit);
        }

        // This will handle the response. 
        await export_utility(res, event_query)
    } 
    catch (err) 
    {
        return next(err);
    }
});

/**
 * @api {get} /api/blast/export/unified Blast gauge data with internal issuance information
 * @apiDescription Return blast gauge events in JSON format with DODID and Name as set in the B3 Variables
 * @todo This method will need external access controls. 
 * @apiGroup Blast
 * @apiSuccess Zip file
 */
router.get('/export/unified', async function(req, res, next){
    try 
    {
        if(req.query.device_data_id === undefined)
        {
            throw new Error("Required parameter \"device_data_id\" Missing")
        }
        
        let event_query = blastGaugeConquerQuery();
        event_query.where('bgd_device_data.id', '>=', req.query.device_data_id);
        event_query.orderBy("bgd_device_data.id");
        if(req.query.size)
        {
            event_query.limit(req.query.size);
        }

        let events = await event_query.select();

        res.json(await Promise.all(events
            .filter(item=>item.flag!=="NOTREAL" && item.flag!=="ERROR")
            .map(async (event)=>{
                let overpressure = await db('bgd_event_raw_data')
                    .where('bgd_device_data_id', event.device_data_id)
                    .groupBy("time")
                    .column("overpressure");
                event.detail = overpressure.map((row=>row.overpressure));
                return event;
            }))
        );
    }
    catch(err)
    {
        return next(err);
    }
});

/**
 * @api {get} /api/blast/export/conquer Blast gauge data with internal issuance information
 * @apiDescription Return blast gauge events in CSV format with DODID and Name as set in the B3 Variables
 * @apiGroup Blast
 * @apiSuccess Zip file
 */
router.get('/export/conquer', async function(req, res, next)
{
    try
    {	
        // Get Events - custom query for conquer
       let event_query = blastGaugeConquerQuery();

        if(req.query.from_date)
        {
            event_query.whereRaw("DATE(??) >= ?", ["bgd_event.event_date_time", req.query.from_date]);
        }

       if(req.query.to_date)
       {
            event_query.whereRaw("DATE(??) <= ?", ["bgd_event.event_date_time", req.query.to_date]);
       }

       if(req.query.download_date)
       {
            event_query.whereRaw("DATE(??) = ?", ["bgd_event.download_date", req.query.download_date]);
       }

        if(req.query.unit)
        {
            event_query.where("bgd_personnel.unit_name", req.query.unit);
        }

        // This will handle the response. 
        await export_utility(res, event_query, true)
    } 
    catch (err) 
    {
        return next(err);
    }
});

/**
 * Return a zip file with the provided events, and their raw data. 
 * 
 * @param {Response} res Express Respnse Object
 * @param {Knex} event_query Query to fetch events
 * @param {Boolean} stats Indicate whether to include statistics about the data
 */
async function export_utility(res, event_query, stats)
{
    var zip = new adm();

    let event_response = await event_query.select();
    let real_events = event_response.filter(item=>item.flag!=="NOTREAL" && item.flag!=="ERROR");
    let device_data_ids = real_events.map(item=>item.device_data_id);
    let event_csv = papa.unparse(real_events, {
        columns: [
            "event_id",
            "device_data_id",
            "device_id",
            'dod_id',
            'display_name',
            'unit_name',
            'event_date_time',
            'blast_gauge_serial_number',
            'mounting_location',
            "uid",
            "firmware_rev"
        ]}
    );
    zip.addFile("events.csv", Buffer.alloc(event_csv.length, event_csv));

    // Get Raw
    let detail_query = db('bgd_event_raw_data')
    .join("bgd_device_data","bgd_device_data.id","bgd_event_raw_data.bgd_device_data_id")
    .whereIn('bgd_device_data_id', device_data_ids)
    .groupBy("bgd_device_data_id", "time")
    .column({
        "event_id" : "bgd_device_data.bgd_event_id",
        "device_data_id" : "bgd_device_data_id",
        "device_id" : "bgd_device_id",
        'time_in_ms' : "time",
        'overpressure_in_psi' : "overpressure"
    });

    let detail_response = await detail_query.select();
    detail_response = papa.unparse(detail_response);
    zip.addFile("detail.csv", Buffer.alloc(detail_response.length, detail_response));

    if(stats)
    {
        let stats = {
            events : new Set(),
            device_data : 0,
            waveforms : 0,
            notreal_waveforms : 0,
            events_with_real_waveforms : new Set(),
            individual : []
        };
        for(let i = 0 ; i < event_response.length ; i++)
        {
            let row = event_response[i];
            let individual = stats.individual.find(item=>item.dod_id==row.dod_id);
            if(!individual)
            {
                individual = {
                    events : new Set(),
                    device_data: 0,
                    waveforms : 0,
                    notreal_waveforms : 0,
                    events_with_real_waveforms : new Set(),
                    dod_id : row.dod_id
                }
                stats.individual.push(individual);
            }

            stats.events.add(row.event_id);
            individual.events.add(row.event_id);

            stats.device_data++;
            individual.device_data++;

            if(row.raw)
            {
                stats.waveforms++;
                individual.waveforms++;

                if(row.flag=="NOTREAL" || row.flag == "ERROR")
                {
                    stats.notreal_waveforms++;
                    individual.notreal_waveforms++;
                }
                else 
                {
                    stats.events_with_real_waveforms.add(row.event_id);
                    individual.events_with_real_waveforms.add(row.event_id);
                }
            }
        }

        let stats_response = 
`Export Statistics and QA Summary
---------------------------------

Total Number of Events: ${stats.events.size}
This is the total number of parsed events, regardless of the ARA result or whether
full waveform data was collected. It should match the total number of event tabs 
in all the personnel reports with dates / times within the range of this export. 

Total Number of Event Device Records: ${stats.device_data}
This is the total number of device activations. It should match the total number
of rows in the associated events.csv file, and the total number of device records
in all the event tabs from the personnel reports with dates / times within 
the range of this export. 

Event Device Records with Waveform Data: ${stats.waveforms}
Event Device Records marked NOTREAL: ${stats.notreal_waveforms}
These indicate the number of Event Device records with waveform data, and how many
of there were marked NOTREAL. 

Events with Real Waveform Data: ${stats.events_with_real_waveforms.size}
This is the total number of events expected to be displayed in the TFR from this 
export.

Excluded Events: ${stats.events.size-stats.events_with_real_waveforms.size}
This is the number of events that were excluded, either due to not having full 
waveforms or due to waveforms being flagged as NOTREAL. It should match the 
delta between the number of events in the TFR and the number of event tabs in the 
personnel reports. 

Individual Breakdowns

` + stats.individual.map(item=>{
    return `ID: ${item.dod_id}
Total Events: ${item.events.size} 
Event Device Records: ${item.device_data} 
Event Device Records with Waveform Data: ${item.waveforms} 
Event Device Records marked NOTREAL: ${item.notreal_waveforms}
Events with Real Waveform Data: ${item.events_with_real_waveforms.size}`
}).join("\n\n");

        zip.addFile("stats.txt", Buffer.alloc(stats_response.length, stats_response));

    }

    res.setHeader('Content-Type', 'application/zip');
    res.setHeader('Content-Disposition', `attachment; filename=mhce-blast-data-export.zip`);
    res.send(zip.toBuffer());
}

/**
 * @api {get}  /api/blast/exportrawdata Download (csv) the raw blast data per
 *              dodid and download date, and optionally event id 
 * @apiDescription Download Raw Blast Gauge Data to CSV file
 *  */
router.get('/exportrawdata', async function(req, res, next)
{
    try
    {    
        var response =  await db("bgd_event_raw_data")
            .select(db.raw(`
                bgd_device_data.bgd_event_id,
                bgd_event_raw_data.time,
                group_concat(DISTINCT if(bgd_device.mounting_location = 'HEAD', bgd_event_raw_data.overpressure, NULL))  as head_overpressure,
                group_concat(DISTINCT if(bgd_device.mounting_location = 'SHOULDER', bgd_event_raw_data.overpressure, NULL))  as shoulder_overpressure,
                group_concat(DISTINCT if(bgd_device.mounting_location = 'CHEST', bgd_event_raw_data.overpressure, NULL))  as chest_overpressure
            `))
            .join("bgd_device_data",  "bgd_event_raw_data.bgd_device_data_id", "bgd_device_data.id")
            .join("bgd_device",  "bgd_device_data.bgd_device_id", "bgd_device.id")
            .whereIn("bgd_device_data.bgd_event_id", req.query.events.split(','))
            .groupBy("bgd_device_data.bgd_event_id", "bgd_event_raw_data.time");
        let csv = papa.unparse(response,{ "quotes" : true,  "delimiter" : ","});
        res.setHeader('Content-Type', 'text/csv');
        res.setHeader('Content-Disposition', `attachment; filename=jhrm-raw-data-download.csv`);
        res.send(csv);
    }
    catch (err) 
    {
        return next(err);
    }
});

/**
 * @api {get}  /api/blast/exposure/events Get the list of Blast Gauge 
 *     Overpressure Exposure Events by DODID and Download Date
 * @apiDescription List all samples
 * @apiParam {Number} :dodid URL Parameter. The DODID value of Personnel
 * @apiGroup Blast
 * @apiSuccess {Object} localreferencedrihras[] Array of samples
 * @apiSuccess {String} localreferencedrihras.ifa_associated_personnel_id
 * @apiSuccess {String} localreferencedrihras.chem_rapid_hra_id
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *     [
 *      {
 *       "id": 2,
 *       "event_date_time": "2018-03-23T02:12:00.000Z",
 *       "mounting_location": "HEAD",
 *       "peak_overpressure": 6.47,
 *       "total_positive_impulse": 4.2,
 *       "acceleration_peak_magnitude": 13.54,
 *       "raw_data_exists": 1,
 *       "real_notreal_flag": null,
 *       "exclude_data": 0,
 *       "exclude_data_comments": "This is only a test for shoulder"
 *       }
 *     ] 
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 * @apiErrorExample Bad Request
 *     HTTP/1.1 400 Assessment missing required field.
 */
router.get('/exposure/events', async function(req, res, next){    
    try
    {
        let query = blastGaugeEventQuery(false);
        query.where("bgd_device_data.qa_approved", 0);

        if(req.query.dodid)
        {
            query.where('iss_personnel.dodid', req.query.dodid);
        }

        if(req.query.download_date)
        {
            query.where(db.raw('DATE(bgd_event.download_date)'), new Datetime(req.query.download_date).dateValue)
        }
        
        let response = await query.column({
            'event_id' : 'bgd_event.id',
            'event_date_time' : 'bgd_event.event_date_time',
            'device_id' : 'bgd_device.id',
            'device_data_id' : 'bgd_device_data.id',
            'download_date' : 'bgd_event.download_date',
            'mounting_location' : 'bgd_device.mounting_location',
            'peak_overpressure' : 'bgd_device_data.peak_overpressure',
            'total_positive_impulse' : 'bgd_device_data.total_positive_impulse',
            'acceleration_peak_magnitude' : 'bgd_device_data.acceleration_peak_magnitude',
            'raw_data_exists' : 'bgd_device_data.raw_data_exists',
            'data_flags' : db.raw("IF(bgd_device_data.real_notreal_flag = 'NOTREAL', 'NR', 'N')"),
            'exclude_data' : 'bgd_device_data.exclude_data',
            'est_max_peak_overpressure' : 'bgd_event.est_exp_max_peak_overpressure',
            'exclude_data_comments' : 'bgd_device_data.exclude_data_comments',
            'dodid' : 'iss_personnel.dodid'
        });

        if (response.length == 0 )
        {
            throw new createError.BadRequest("No events found for personnel.");
        }

        res.json(response.map(row=>{
            row.download_date = new Datetime(row.download_date);
            row.event_date_time = new Datetime(row.event_date_time);
            return row;
        }));

    }
    catch (err) 
    {
        return next(err);
    }
});

/**
 * @api {get}  /api/blast/exposure/rawdata/:eventid Get the list of Blast Gauges Raw Data Information by EVENTID
 * @apiDescription List Raw Data Samples
 * @apiParam {Number} :eventid URL Parameter. The eventid value of the event
 * @apiGroup Blast
 * @apiSuccess {Object} Array of HEAD, Array of SHOULDER, & Array of CHEST Samples
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    {
 *      1:Array(Qty) [RowDataPacket, RowDataPacket, RowDataPacket, …]
 *      2:Array(Qty) [RowDataPacket, RowDataPacket, RowDataPacket, …]
 *      3:Array(Qty) [RowDataPacket, RowDataPacket, RowDataPacket, …]
 *     }
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 * @apiErrorExample Bad Request
 *     HTTP/1.1 400 Assessment missing required field.
 */
router.get('/exposure/rawdata/:eventid', async function(req, res, next){
    
    try
    {
        let results = await db('bgd_event_raw_data ')
            .where('bgd_event_id',  req.params.eventid)
            .groupBy("bgd_device_data_id", "time");
        
        let output = {};
        results.forEach(row=>{
            if(!output[row.bgd_device_data_id])
            {
                output[row.bgd_device_data_id] = [];
                }
                output[row.bgd_device_data_id].push(row);
            });
        
        // Return the response, even if it is empty
        res.json(output);

    }
    catch (err) 
    {
        return next(err);
    }
});

/**
 * @api {post}  /api/blast/exposure/event
 * @apiDescription Update Blast Gauge Exclude Data and Exclude Data Comments
 * @apiGroup Blast
 * @apiSuccess {string} response.status
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    {
 *        "Status": '"Updates Successful"'
 *    }
 * @apiErrorExample Bad Request
 *     HTTP/1.1 400 Assessment missing required field.
 */
router.post('/exposure/event', async function(req,res,next){
    
    try
    {
        for(let i = 0; i < req.body.length; i++)
        {
            if (req.body[i].exclude_data == true)
            {
                await db('bgd_device_data')
                    .where('bgd_device_id',  req.body[i].bgd_device_id)
                    .where('bgd_event_id',  req.body[i].bgd_event_id)
                    .update({
                        exclude_data : req.body[i].exclude_data,
                        exclude_data_comments : req.body[i].exclude_data_comments
                    });
            }
            else
            {
                await db('bgd_device_data')
                .where('bgd_device_id',  req.body[i].bgd_device_id)
                .where('bgd_event_id',  req.body[i].bgd_event_id)
                .update({
                    exclude_data : req.body[i].exclude_data
                });
            }

                
        }

        return res.json({
            Status : "Updates Successful"
        })
    }
    catch(err)
    {
        return next(err);
    }
}

);

/**
 * @api {get} /api/blast/summarysensordata
 * @apiDescription Get 
 
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 */
router.get('/summarysensordata', async function(req, res, next) {
    try 
    {
        let query = blastGaugeEventQuery(true);
        query.leftJoin('iss_event as iss_event_returned', function(){
            this.on('iss_event_returned.id', db.raw(`
                SELECT id
                FROM iss_event e
                WHERE e.iss_device_id = iss_device.id
                AND e.date > bgd_event.event_date_time
                AND e.type = 'RETURNED'
                ORDER BY e.date ASC
                LIMIT 1
            `).wrap('(',')'));
            this.andOn('iss_event_issued.type', db.raw("?", 'ISSUED'));
        });
        query.where("iss_personnel.dodid", req.query.dodid);
        query.where("bgd_device_data.exclude_data", 0);
        query.where("bgd_device_data.qa_approved", 0);
        query.groupBy("iss_device.id");
        query.column({
            "wear_location" : "iss_event_issued.wear_location",
            "serial_number" : "iss_device.serial_number",
            "issue_date" : "iss_event_issued.date",
            "returned_date" : "iss_event_returned.date",
            "initialization_date" : "iss_device.initialization_date",
        });
        let response = await query.select();
        res.json(response.map(row=>{
            row.issue_date = row.issue_date ? new Datetime(row.issue_date) : null;
            row.returned_date = row.returned_date ? new Datetime(row.returned_date) : null;
            row.initialization_date = row.initialization_date ? 
                new Datetime(row.initialization_date) : 
                null;
            return row;
        }))
    } 
    catch(err) 
    {
        return next(err)
    }
})

/**
 * @api {get} /api/blast/summarydata
 * @apiDescription Get all samples associated with an exposure pathway
 
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 */
 router.get('/summarydata', async function(req, res, next){
    try
    { 
        let query = blastGaugeEventQuery(false);
        query.whereNotNull("iss_personnel.id");
        query.where("bgd_device_data.exclude_data", 0);
        query.where("bgd_device_data.qa_approved", 0);
        if(req.query.unit)
        {
            query.where("iss_personnel.unit", req.query.unit);
        }
        query.groupBy("bgd_event.id");
        query.max({
            "maxpeak" : "bgd_device_data.peak_overpressure",
            "qa_approved" : "bgd_device_data.qa_approved",
            "flagged" : "bgd_device_data.real_notreal_flag",
            "estimatedpeak" : "bgd_event.est_exp_max_peak_overpressure"
        })
        query.column({
            "event" : "bgd_event.id",
            "dodid" : "iss_personnel.dodid",
            "first_name" : "iss_personnel.first_name",
            "last_name" : "iss_personnel.last_name",
            "download_date" : db.raw("DATE(bgd_event.download_date)")
        })
        let result = await query.select();
        let ret = result.reduce((acc, cur) => {
            let group = acc.find(item=>item.dodid==cur.dodid&&item.download_date.getTime()==cur.download_date.getTime());
            if(!group)
            {
                cur.numEvents = cur.over4psi = cur.over16psi = 0;
                cur.events = [cur.event];
                delete cur.event;
                acc.push(cur);
                group = cur;
            }
            else 
            {
                group.events.push(cur.event);
            }

            group.numEvents++;
            group.devices = group.devices + "," + cur.devices;
            if(cur.maxpeak > 4)
            {
                group.over4psi++;
            }
            if(cur.maxpeak > 16)
            {
                group.over16psi++;
            }
            if(cur.maxpeak > group.maxpeak)
            {
                group.maxpeak = cur.maxpeak;
            }
            if(cur.estimatedpeak > group.estimatedpeak)
            {
                group.estimatedpeak = cur.estimatedpeak;
            }
            return acc;
        }, []).map(group=>{
            group.download_date = new Datetime(group.download_date, Datetime.FORMATS.DATE);
            return group;
        });
        res.json(ret);
    }
    catch (err) 
    {
        return next(err);
    }
});

/**
 * @api {post} /api/blast/summaryqaapproved
 * @apiDescription Update each line in the bgd_device_data table that has been approved with comments, date and the approval flag
 
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 */
 router.post('/summaryqaapproved', async function(req, res, next)
 {
    try 
    {
        let importBG_Records

        if(req.body.events && req.body.events.length)
        {
            await db('bgd_device_data').update(
            {
                qa_approved : 1,
                qa_approved_date : new Datetime().date,
                qa_comments : req.body.qa_comments
            })
            .whereIn('bgd_event_id', req.body.events)
            .where('exclude_data', 0)
            .where('qa_approved', 0);

            let query = blastGaugeEventQuery(false);    
            query.whereIn('bgd_event.id', req.body.events);
            query.join('bgd_personnel', 'bgd_personnel.id', 'bgd_event.bgd_personnel_id')
            query.leftJoin('iss_device_set', 'iss_device.id', 'iss_device_set.iss_device_id');
            let response = await query.column({
                'Environmental Hazard Type' : 'bgd_event.env_hazard_type',
                'DoD ID' : 'iss_personnel.dodid',
                'B3 BG EDIPI Field' : 'bgd_personnel.dod_id',
                'B3 BG Display Name Field' : 'bgd_personnel.display_name',
                'B3 BG Unit Name Field' : 'bgd_personnel.unit_name',
                'Sensor Make' : 'iss_device.manufacturer',
                'Sensor Model' : 'iss_device.model_number',
                'Firmware Revision' : 'bgd_device.firmware_rev',
                'Event Date Time' : 'bgd_event.event_date_time',
                'Gauge Set ID' : 'iss_device_set.iss_set_id',
                'Event Group ID' : 'bgd_event.id',
                'Measured Exposure Event Max Peak Pressure (PSIG)' : 'bgd_device_data.total_positive_impulse',
                'Measured Exposure Event Max Total Positive Impulse (PSI * ms)' : 'bgd_device_data.acceleration_peak_magnitude',
                'Estimated Freefield Exposure Event Max Peak Pressure (PSIG) ' : 'bgd_event.est_exp_max_peak_overpressure',
                'Estimated Freefield Exposure Event Total Positive Impulse' : 'bgd_event.est_exp_total_positive_impulse',
                'BOP Exposure Summary' : 'bgd_event.bop_exposure_summary',
                'BOP Exposure Health Risk Assessment Summary' : 'bgd_event.bop_exposure_hra_summary'
            });
            for(let item of response) 
            {
                await SmartabaseApi.eventImportBlastGauge(item);
            }
        }
        return res.json("success");
    } 
    catch (err) {
        return next(err);
    }
});


/**
 * Helper that standardizes error output to be an JSON object, and sets the
 * correct headers. Example body content: 
 *  
 * {
 *   "status": "Error",
 *   "statusCode": 404,
 *   "message": "HRA Not found."
 * }
 * 
 * Error output is santitized to avoid exposing implementation details unless 
 * running in debug mode. 
 * 
 * In order to make certain that this handler is called, especially if your 
 * method is asynchronous, make sure to call the next() function provided 
 * by express with the error as the first argument. 
 * 
 */
 router.use(function (err, req, res, next) {
    if (!err.statusCode) {
        err = createError(500, err);
    }
    res.status(err.statusCode).json({
        status: 'Error',
        statusCode: err.statusCode,
        message: err.expose || config.get('debug') ? err.message : "Internal Server Error."
    });
});


module.exports = router