const express = require('express');
const router = express.Router();
const https = require('follow-redirects').https;

//The eventimport is hard coded to update the JHRMS Simple API form in SmartaBase. 
//Smartabase requirements are currently being developed. 
function getpostdata(user, pass, index){

    var postData
    let date = new Date();
    let dateString = `${date.getDate()}/${date.getDay()}/${date.getFullYear()}`;
    let timeString = date.getHours() % 12;

    if (index == 0)
    {
        postData = JSON.stringify({"username" : user,"password" : pass});
    }
    else if (index == 1){
        postData = `{"formName": "JHRMS Simple API", "startDate": "28/07/2021", "startTime": "5:30 PM", "finishDate": "28/07/2021", "finishTime": "6:30 PM", "userId": {"userId" : 61184}, "enteredByUserId": 61184, "existingEventId": 11651968, "rows": [{"row": 0, "pairs": [{"key": "Single Line Test", "value": "Team Bravo"}, {"key": "Number", "value": "55.5"}]}]}`
    }
    else if (index  == 2){
        postData = `{"formName": "JHRM WFL", "startDate": "15/09/2021", "startTime": "10:30 AM", "finishDate": "15/09/2021", "finishTime": "11:30 AM", "userId": {"userId" : 61184}, "enteredByUserId": 61184, "existingEventId": 11819528, "rows": [{"row": 0, "pairs": [{"key": "Ear Protection", "value": "False"},{"key": "Overear Protection","value": "False"},{"key": "Blast Sensors", "value": "False"},{"key": "Weapon System Type","value": "Weapon System Type"},{"key": "Weapon System", "value": "Weapon System"},{"key": "Ammunition", "value": "Ammunition"},{"key": "Rounds Fired", "value": "55.0"},{"key": "Manning Position", "value": "Gate"},{"key": "Enclosed Space", "value": "No"}, {"key": "Posture", "value": "none"},{"key": "Role", "value": "guard"},{"key": "EDIPI", "value": "45.0"},{"key": "WFL Unique ID", "value": "777"},{"key": "Unit Name", "value": "Bravo"}, {"key": "QA_Reviewer_ID", "value": "2.0"}, {"key": "QA Approver Date", "value": "1631664000000"}, {"key": "QA Approver Status", "value": "No"}], "row": 0}]}`
    }
    else if (index  == 3){
        postData = `{"formName": "JHRM WFL", "startDate": "${dateString}", "startTime": "${timeString}", "finishDate": "${dateString}", "finishTime": "${timeString}", "userId": {"userId" : 61184}, "enteredByUserId": 61184, "rows": [{"row": 0, "pairs": [{"key": "Ear Protection", "value": "True"},{"key": "Overear Protection","value": "True"},{"key": "Blast Sensors", "value": "True"},{"key": "Weapon System Type","value": "Weapon System Type"},{"key": "Weapon System", "value": "Weapon System"},{"key": "Ammunition", "value": "Ammunition"},{"key": "Rounds Fired", "value": "55.0"},{"key": "Manning Position", "value": "Gate"},{"key": "Enclosed Space", "value": "No"}, {"key": "Posture", "value": "none"},{"key": "Role", "value": "guard"},{"key": "EDIPI", "value": "45.0"},{"key": "WFL Unique ID", "value": "777"},{"key": "Unit Name", "value": "Bravo"}, {"key": "QA_Reviewer_ID", "value": "2.0"}, {"key": "QA Approver Date", "value": "1631664000000"}, {"key" : "Proximity to Source", "value" : "2m"}, {"key": "QA Approver Status", "value": "No"}], "row": 0}]}`
    }

    return postData
}

async function getoption(username, password,command) {

    const auth = `Basic ${Buffer.from(username + ':' + password, 'utf8').toString('base64')}`;

    var options = {
        'method': 'POST',
        'hostname': 'vse.smartabase.com',
        'path': '/sandbox/api/v1/' + command + '?informat=json&format=json',
        'headers': {
            'Authorization': auth,
            'Content-Type': 'application/json'
        },
        'maxRedirects': 20
    };

    return options
}

async function httprequest(user,pass, command, index) {

        var options = await getoption(user,pass,command)

        var request = https.request(options, function (res) {
            var chunks = [];

            res.on("data", function (chunk) {
                chunks.push(chunk);
            });

            res.on("end", function (chunk) {
                var body = Buffer.concat(chunks);
                console.log(body.toString());
            });

            res.on("error", function (error) {
                console.error(error);
            });            

        });

        var postData = getpostdata(user,pass,index)

        request.write(postData);

        request.end();
}

/**
 * @api {post} /api/smartabase/smartabasecommand
 * @apiDescription Verify User SmartaBase Commands and return associated groups, roles, and import data
 * 
 * @apiErrorExample Server Error Reponses
 *     HTTP/1.1 400 Bad Request 
 *     HTTP/1.1 500 Internal Server Error
 */
router.post('/smartabasecommand', async function(req, res, next){

    try
    {

        let val = await httprequest(req.body.user,req.body.pass,req.body.command,req.body.index).then((data) => {
                const response = {
                    statusCode: 200,
                    body: JSON.stringify(data),
                };
            return response;
        });

        res.json(val)

    }
    catch(err) {
        console.log(err)
        return next(err)
    }
    
});

module.exports = router