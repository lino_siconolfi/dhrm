const esmImport = require('esm')(module);
const express = require('express');
const router = express.Router();
const moment = require('moment');
const db = require('src/util/db');
const uniqueid = require('src/util/uniqueid');
const hrastatus = require('src/util/hrastatus.js');
const { DimensionedValue } = esmImport("../../shared/DimensionedValue.js");
const calculatePepc = esmImport("../../shared/calculatePepc.js");
const { normalizeMegs } = esmImport("../../shared/normalizeMegs.js");
const { EnhancedHraQa } = esmImport("../../shared/ehra_qa.js");
const { Datetime } = esmImport('../../shared/datetime');
const users = require('../../util/users');

function canEdit(hra, req)
{
    // Incomplete HRA can be edited
    if(hra.status == hrastatus.INCOMPLETE)
    {
        return true;
    }

    // Complete HRA can only be edited if user has QA
    let user = users.find(user=>user.username==req.cookies.user_name);
    if(user.role.hra_qa)
    {
        return true;
    }

    // Default to false
    return false;
}

/**
 * @api {post} /api/enhancedhra/hra Save HRA
 * @apiDescription Save an HRA and return the ID. If an ID is not provided, a new HRA will be created. Otherwise the existing HRA will be updated. 
 * @apiParam {Object} hra Health Risk Assessment JSON object
 * @apiParam {String} hra.hra_objective
 * @apiParam {String} hra.location 
 * @apiParam {Float} hra.latitude
 * @apiParam {Float} hra.location
 * @apiParam {String} hra.sap_file_name
 * @apiParam {Int} hra.id_doehrs_pathway_id
 * @apiParam {String} hra.exposure_pathway_name
 * @apiParam {String} hra.chemical_name
 * @apiParam {String} hra.created_by
 * @apiParam {String} hra.priority
 * @apiGroup Enhanced HRA
 * @apiSuccess {Object} response Response JSON Object
 * @apiSuccess {String} response.Status
 * @apiSuccess {String} response.id
 * @apiSuccessExample Success
    HTTP/1.1 200 OK
    {
        Status: "Create Record Success"
        id: "E-FFGTUSD1ZS"
    }
 * @apiErrorExample Pathway Not Found
    HTTP/1.1 404 Pathway Not Found
 * @apiErrorExample Access Denied
    HTTP/1.1 403 Access Denied
 * @apiErrorExample Internal Server Error
    HTTP/1.1 500 Internal Server Error
 */
router.post('/hra', async function(req, res){

    try
    {
        // Fetch the pathway and validate 
        var pathway = await db('id_doehrs_pathway')
            .where('id', req.body.id_doehrs_pathway_id);

        if(!pathway.length==1)
        {
            return res.status(404).send('Pathway not found');
        }
        pathway = pathway[0];
        var id, samples=true;
        if(req.body.id)
        {
            var hra = await db('chem_enhanced_hra')
            .where("id", req.body.id);

            if(!hra.length)
            {
                return res.sendStatus(404);
            }
            hra = hra[0];
            if(!canEdit(hra, req))
            {
                // If the HRA is complete, this is not allowed
                return res.sendStatus(403);
            }
            id = req.body.id;
            await db('chem_enhanced_hra').update({
                'hra_objective' : req.body.hra_objective,
                'location' : req.body.location,
                'latitude' : req.body.latitude,
                'longitude' : req.body.longitude,
                'sap_file_name' : req.body.sap_file_name,
                'id_doehrs_pathway_id': req.body.id_doehrs_pathway_id,
                'exposure_pathway_name' : pathway.name,
                'exposure_route' : pathway.exposure_route,
                'source' : pathway.source,
                'population_at_risk' : pathway.population_at_risk_descr,
                'media' : pathway.environmental_media,
                'priority' : req.body.priority,
            }).where('id',req.body.id);

            if(req.body.id_doehrs_pathway_id != hra.id_doehrs_pathway_id)
             {
                // If the exposure pathway has changed, we will delete all 
                // previously associated samples
                await db('cehra_sample').where('chem_enhanced_hra_id', id).del();
            }
            else 
            {
                samples = false;
            }
        }
        else 
        {
            id = uniqueid('E-');
            var response = await db('chem_enhanced_hra').insert({
                'id' : id,
                'hra_objective' : req.body.hra_objective,
                'location' : req.body.location,
                'latitude' : req.body.latitude,
                'longitude' : req.body.longitude,
                'sap_file_name' : req.body.sap_file_name,
                'id_doehrs_pathway_id': req.body.id_doehrs_pathway_id,
                'exposure_pathway_name' : pathway.name,
                'exposure_route' : pathway.exposure_route,
                'source' : pathway.source,
                'population_at_risk' : pathway.population_at_risk_descr,
                'media' : pathway.environmental_media,
                'created_by' : req.body.created_by,
                'created_on' : db.fn.now(6),
                'priority' : req.body.priority,
                'status' : hrastatus.INCOMPLETE
            });
        }
        if(samples)
        {
            // Copy all samples for the selected exposure pathway
            let samplesToCopy = await db('id_doehrs_sample')
            .columns({
                id_doehrs_sample_id : 'id_doehrs_sample.id',
                sampling_point : 'id_doehrs_sample.sampling_point',
                start_date_time : 'id_doehrs_sample.start_time',
                exposure_notes : 'id_doehrs_sample.exposure_notes',
                sample_time : 'id_doehrs_sample_air.sample_time',
                approved_by_qa : 'id_doehrs_sample.status'
            })
            .leftJoin('id_doehrs_sample_air', 'id_doehrs_sample.id', 'id_doehrs_sample_air.id_doehrs_sample_id')
            .where('id_doehrs_sample.id_doehrs_pathway_id', req.body.id_doehrs_pathway_id);

            await db('cehra_sample').insert(samplesToCopy.map(sample=>{
                sample.chem_enhanced_hra_id = id;
                sample.sample_time = parseFloat(sample.sample_time);
                sample.approved_by_qa = sample.approved_by_qa == 'Approved by QA' ? 1 : 0;
                sample.media="AIR";
                return sample;
            }));
        }

        res.status(201).json({
            Status : 'Create Record Success',
            id : id
        });
    }
    catch(err)
    {
        res.status(500).send(err.message);
    }
})

/**
 * @api {get} /api/enhancedhra/hra List HRAs
 * @apiDescription List all HRAs 
 * @apiGroup Enhanced HRA
 * @apiSuccess {Object[]} hra[] Array of HRAs
 * @apiSuccess {string} hra.id
 * @apiSuccess {string} hra.hra_objective
 * @apiSuccess {string} hra.location
 * @apiSuccess {number} hra.latitude
 * @apiSuccess {number} hra.longitude
 * @apiSuccess {string} hra.sap_file_name
 * @apiSuccess {string} hra.id_doehrs_pathway_id
 * @apiSuccess {string} hra.exposure_pathway_name
 * @apiSuccess {string} hra.chemical_name
 * @apiSuccess {string} hra.exposure_route
 * @apiSuccess {string} hra.source
 * @apiSuccess {string} hra.population_at_risk
 * @apiSuccess {string} hra.media
 * @apiSuccess {string} hra.created_by
 * @apiSuccess {date} hra.created_on
 * @apiSuccess {string} hra.last_edited_by
 * @apiSuccess {date} hra.lasted_edited_on
 * @apiSuccess {string} hra.approved_by
 * @apiSuccess {string} hra.priority
 * @apiSuccess {string} hra.status
 * @apiSuccess {string} hra.recommended_mitigation
 * @apiSuccess {string} hra.potential_health_outcomes_acute
 * @apiSuccess {string} hra.potential_health_outcomes_chronic
 * @apiSuccess {string} hra.cas_number
 * @apiSuccess {string} hra.acute_peak_risk
 * @apiSuccess {string} hra.acute_peak_confidence
 * @apiSuccess {string} hra.acute_average_risk
 * @apiSuccess {string} hra.acute_average_confidence
 * @apiSuccess {string} hra.chronic_average_risk
 * @apiSuccess {string} hra.chronic_average_confidence
 * @apiSuccessExample Success
    HTTP/1.1 200 OK
    [{
        "id":"E-FFGTUSD1ZS",
        "hra_objective":"Sample HRA",
        "location":"Base Camp TAJI",
        "latitude":69.2634,
        "longitude":34.9462,
        "sap_file_name":null,
        "id_doehrs_pathway_id":14681,
        "exposure_pathway_name":"PM2.5 AMBIENT AIR",
        "chemical_name":"PARTICULATE <2.5M (PM-2.5)",
        "exposure_route":"Inhalation",
        "source":"Ambient Air",
        "population_at_risk":"Entire Camp",
        "media":"Air",
        "created_by":"Ally McBeal",
        "created_on":"2020-04-13T18:53:30.000Z",
        "last_edited_by":null,
        "lasted_edited_on":null,
        "approved_by":null,
        "priority":null,
        "status":"INCOMPLETE",
        "recommended_mitigation":null,
        "potential_health_outcomes_acute":null,
        "potential_health_outcomes_chronic":null,
        "cas_number":"PM2.5",
        "acute_peak_risk":"Moderate",
        "acute_peak_confidence":"medium",
        "acute_average_risk":"Moderate",
        "acute_average_confidence":"medium",
        "chronic_average_risk":"Low",
        "chronic_average_confidence":"medium"
    }] 
 * @apiErrorExample Internal Server Error
    HTTP/1.1 500 Internal Server Error
 */

router.get('/hra', async function(req,res){
    
    try
    {	
        var hras = await db.raw(`
            select 
                hra.*,
                rep.recommended_mitigation,
                rep.potential_health_outcomes_acute,
                rep.potential_health_outcomes_chronic,
                rep.executive_summary,
                rep.force_health_protection_recommendations,
                pre.cas_number,
                pre.chemical_name,
                peak.risk_level as acute_peak_risk,
                peak.confidence as acute_peak_confidence,
                avg.risk_level as acute_average_risk,
                avg.confidence as acute_average_confidence,
                chronic.risk_level as chronic_average_risk,
                chronic.confidence as chronic_average_confidence
            from chem_enhanced_hra hra
            left join cehra_report rep
                on rep.chem_enhanced_hra_id = hra.id
            left join cehra_prescreen pre
                on pre.chem_enhanced_hra_id = hra.id
            left join cehra_assessment peak
                on peak.type = "ACUTE_PEAK" 
                and peak.chem_enhanced_hra_id = hra.id
                and peak.cas_number = pre.cas_number
            left join cehra_assessment avg
                on avg.type = "ACUTE_AVERAGE" 
                and avg.chem_enhanced_hra_id = hra.id
                and avg.cas_number = pre.cas_number
            left join cehra_assessment chronic
                on chronic.type = "CHRONIC_AVERAGE" 
                and chronic.chem_enhanced_hra_id = hra.id	
                and chronic.cas_number = pre.cas_number
        `)
        
        // Return the response, even if it is empty
        res.json(hras[0].map(hra=>{
            hra.created_on = new Datetime(hra.created_on);
            return hra;
        }));
        
    }
    catch(err)
    {
        res.status(500).send(err.message);
    }
})

/**
 * @api {get} /api/enhancedhra/hra/:hraId Get HRA
 * @apiParam {Number} hraId URL Parameter. The JHRM ID of the HRA
 * @apiGroup Enhanced HRA
 * @apiSuccess {Object[]} hra HRA Object
 * @apiSuccess {string} hra.id
 * @apiSuccess {string} hra.hra_objective
 * @apiSuccess {string} hra.location
 * @apiSuccess {number} hra.latitude
 * @apiSuccess {number} hra.longitude
 * @apiSuccess {string} hra.sap_file_name
 * @apiSuccess {string} hra.id_doehrs_pathway_id
 * @apiSuccess {string} hra.exposure_pathway_name
 * @apiSuccess {string} hra.chemical_name
 * @apiSuccess {string} hra.exposure_route
 * @apiSuccess {string} hra.source
 * @apiSuccess {string} hra.population_at_risk
 * @apiSuccess {string} hra.media
 * @apiSuccess {string} hra.created_by
 * @apiSuccess {date} hra.created_on
 * @apiSuccess {string} hra.last_edited_by
 * @apiSuccess {date} hra.lasted_edited_on
 * @apiSuccess {string} hra.approved_by
 * @apiSuccess {string} hra.priority
 * @apiSuccess {string} hra.status
 * @apiSuccess {string} hra.recommended_mitigation
 * @apiSuccess {string} hra.potential_health_outcomes_acute
 * @apiSuccess {string} hra.potential_health_outcomes_chronic
 * @apiSuccess {string} hra.cas_number
 * @apiSuccess {string} hra.acute_peak_risk
 * @apiSuccess {string} hra.acute_peak_confidence
 * @apiSuccess {string} hra.acute_average_risk
 * @apiSuccess {string} hra.acute_average_confidence
 * @apiSuccess {string} hra.chronic_average_risk
 * @apiSuccess {string} hra.chronic_average_confidence
 * @apiSuccessExample Success
    HTTP/1.1 200 OK
    {
        "id":"E-FFGTUSD1ZS",
        "hra_objective":"Sample HRA",
        "location":"Base Camp TAJI",
        "latitude":69.2634,
        "longitude":34.9462,
        "sap_file_name":null,
        "id_doehrs_pathway_id":14681,
        "exposure_pathway_name":"PM2.5 AMBIENT AIR",
        "chemical_name":"PARTICULATE <2.5M (PM-2.5)",
        "exposure_route":"Inhalation",
        "source":"Ambient Air",
        "population_at_risk":"Entire Camp",
        "media":"Air",
        "created_by":"Ally McBeal",
        "created_on":"2020-04-13T18:53:30.000Z",
        "last_edited_by":null,
        "lasted_edited_on":null,
        "approved_by":null,
        "priority":null,
        "status":"INCOMPLETE",
        "recommended_mitigation":null,
        "potential_health_outcomes_acute":null,
        "potential_health_outcomes_chronic":null,
        "cas_number":"PM2.5",
        "acute_peak_risk":"Moderate",
        "acute_peak_confidence":"medium",
        "acute_average_risk":"Moderate",
        "acute_average_confidence":"medium",
        "chronic_average_risk":"Low",
        "chronic_average_confidence":"medium"
    }
 * @apiErrorExample HRA Not Found
    HTTP/1.1 404 HRA Not Found
 * @apiErrorExample Internal Server Error
    HTTP/1.1 500 Internal Server Error
 */
router.get('/hra/:hraId', async function(req, res){
    
    try
    {
        var hra = await db.raw(`
        select 
            hra.*,
            rep.recommended_mitigation,
            rep.potential_health_outcomes_acute,
            rep.potential_health_outcomes_chronic,
            rep.executive_summary,
            rep.force_health_protection_recommendations,
            pre.cas_number,
            pre.chemical_name,
            peak.risk_level as acute_peak_risk,
            peak.confidence as acute_peak_confidence,
            avg.risk_level as acute_average_risk,
            avg.confidence as acute_average_confidence,
            chronic.risk_level as chronic_average_risk,
            chronic.confidence as chronic_average_confidence
        from chem_enhanced_hra hra
        left join cehra_report rep
            on rep.chem_enhanced_hra_id = hra.id
        left join cehra_prescreen pre
            on pre.chem_enhanced_hra_id = hra.id
        left join cehra_assessment peak
            on peak.type = "ACUTE_PEAK" 
            and peak.chem_enhanced_hra_id = hra.id
            and peak.cas_number = pre.cas_number
        left join cehra_assessment avg
            on avg.type = "ACUTE_AVERAGE" 
            and avg.chem_enhanced_hra_id = hra.id
            and avg.cas_number = pre.cas_number
        left join cehra_assessment chronic
            on chronic.type = "CHRONIC_AVERAGE" 
            and chronic.chem_enhanced_hra_id = hra.id	
            and chronic.cas_number = pre.cas_number
            where hra.id = ?
        `, [req.params.hraId])

        // Return 404 if we do not find an HRA with the specified ID.
        if(hra.length === 0)
        {
            return res.status(404).send("HRA not found");
        }
        else 
        {
            res.json(hra[0].map(item=>{
                item.created_on = new Datetime(item.created_on);
                return item;
            }));
        }	
        
    }
    catch(err)
    {
        res.status(500).send(err.message);
    }
})

/**
 * @api {get} /api/enhancedhra/hra/:hraId/megs Create new HRA
 * @apiDescription Get all remated megs for an HRA. If no chemical is selected 
 *     yet for the HRA, this will return an error. 
 * @apiGroup Enhanced HRA
 */
router.get('/hra/:hraId/megs', async function(req, res){
    try
    {
        var hra = await db('chem_enhanced_hra')
        .where("id", req.params.hraId);
        if(!hra.length)
        {
            return res.sendStatus(404);
        }

		// Get results
		var results = await db('cehra_sample_results')
		.where('chem_enhanced_hra_id', req.params.hraId)
		.where('selected', 1);
		
		// If no results, 404
		if(!results.length)
		{
			return res.sendStatus(404);
		}
		
		// List chemicals. For now this will only be one chemical. 
		var chemicals = results.reduce((ret, sample)=>{
			if(!ret.find(chemical=>chemical==sample.cas_number))
			{
				ret.push(sample.cas_number);
			}
			return ret;
		},[]);
		
		// Get megs
		// @todo figure out how to limit to media dynamically
		var megs = normalizeMegs(await db('tg230_master_meg')
			.leftJoin('chemical_info', 'chemical_info.cas_number','tg230_master_meg.CASRN')
			.where('MEDIA','Air')
			.whereIn('CASRN', chemicals));

		res.json(megs);
        
    }
    catch(err)
    {
        res.status(500).send(err.message);
    }
});

/**
 * @api {get} /api/enhancedhra/hra/:hraId/risk Get Risk Tables
 * @apiDescription Get the data for the peak and average tables. Only tables that pass prescreen are returned. 
 * @apiParam {Number} hraId URL Parameter. The JHRM ID of the HRA
 * @apiGroup Enhanced HRA
 * @apiSuccess {Object} table Map of peak and average calculations
 * @apiSuccess {Object} row.acute_peak
 * @apiSuccess {String} row.acute_peak.severity
 * @apiSuccess {Object} row.acute_peak.degreeOfExposure
 * @apiSuccess {Object} row.acute_peak.durationOfExposure
 * @apiSuccess {Object} row.acute_peak.representativenessOfData
 * @apiSuccess {Object} row.acute_peak.rateOfExposure
 * @apiSuccess {Object} row.acute_peak.meg
 * @apiSuccess {Object} row.acute_average
 * @apiSuccess {String} row.acute_average.severity
 * @apiSuccess {Object} row.acute_average.degreeOfExposure
 * @apiSuccess {Object} row.acute_average.durationOfExposure
 * @apiSuccess {Object} row.acute_average.representativenessOfData
 * @apiSuccess {Object} row.acute_average.rateOfExposure
 * @apiSuccess {Object} row.acute_average.meg
 * @apiSuccess {Object} row.chronic_average
 * @apiSuccess {String} row.chronic_average.severity
 * @apiSuccess {Object} row.chronic_average.degreeOfExposure
 * @apiSuccess {Object} row.chronic_average.durationOfExposure
 * @apiSuccess {Object} row.chronic_average.representativenessOfData
 * @apiSuccess {Object} row.chronic_average.rateOfExposure
 * @apiSuccess {Object} row.chronic_average.meg
 * @apiSuccessExample {json} Success
    HTTP/1.1 200 OK
    {
    "acute_peak":{
        "meg":{
            "chemical_name":"Particulate matter < 2.5 µm",
            "cas_number":"PM2.5",
            "value":{
                "value":0.065,
                "isValid":true,
                "originalValue":0.065,
                "units":"mg/m3",
                "precision":5
            },
            "basis":"RD-230 Sec. 4-5",
            "severity":"Marginal",
            "timeframe":"1year",
            "minutes":525600,
            "parm":"1yearMARG",
            "isCWA":false
        },
        "severity":{
            "value":"Marginal"
        },
        "degreeOfExposure":{
            "value":"44% Range",
            "score":2
        },
        "durationOfExposure":{
            "value":"1day / 24hour",
            "score":3
        }
    },
    "acute_average":{
        "meg":{
            "chemical_name":"Particulate matter < 2.5 µm",
            "cas_number":"PM2.5",
            "value":{
                "value":0.065,
                "isValid":true,
                "originalValue":0.065,
                "units":"mg/m3",
                "precision":5
            },
            "basis":"RD-230 Sec. 4-5",
            "severity":"Marginal",
            "timeframe":"1year",
            "minutes":525600,
            "parm":"1yearMARG",
            "isCWA":false
        },
        "severity":{
            "value":"Marginal"
        },
        "degreeOfExposure":{
            "value":"15% Range",
            "score":1
        },
        "durationOfExposure":{
            "value":"1day / 24hour",
            "score":3
        }
    },
    "chronic_average":{
        "durationOfExposure":{
            "value":"9day / 1year",
            "score":1
        },
        "severity":{
            "value":"Negligible",
            "score":1
        },
        "degreeOfExposure":{
            "value":"> 0.015 mg/m3",
            "score":2
        },
        "meg":{
            "chemical_name":"Particulate matter < 2.5 µm",
            "cas_number":"PM2.5",
            "value":{
                "value":0.015,
                "isValid":true,
                "originalValue":0.015,
                "units":"mg/m3",
                "precision":5
            },
            "basis":"RD-230 Sec. 4-5",
            "severity":"Negligible",
            "timeframe":"1year",
            "minutes":525600,
            "parm":"1yearNEG",
            "isCWA":false
        }
    }
    }
 * @apiErrorExample Internal Server Error
    HTTP/1.1 500 Internal Server Error
 * @apiErrorExample  HRA not found
    HTTP/1.1 404 HRA not found
 */
router.get('/hra/:hraId/risk', async function(req, res){
    
    try 
    {
        // Validate the HRA ID with the database
        var hra = await db('chem_enhanced_hra')
        .where("id", req.params.hraId);
        if(!hra.length)
        {
            return res.sendStatus(404);
        }

        // Get previously submitted values, if any
        let assessments = await db('cehra_assessment').where('chem_enhanced_hra_id', req.params.hraId);

        return res.json(assessments);
    }
    catch(err)
    {
        res.status(500).send(err.message);
    }
    
});

/**
 * @api {post} /api/enhancedhra/hra/:hraId/risk Save Risk Tables
 * @apiDescription Save user-provided values from peak and average tables and return a summary
 * @apiParam {Number} hraId URL Parameter. The JHRM ID of the HRA
 * @apiParam {Object} table Map of peak and average calculations
 * @apiParam {Object} row.acute_peak
 * @apiParam {String} row.acute_peak.severity
 * @apiParam {Object} row.acute_peak.degreeOfExposure
 * @apiParam {Object} row.acute_peak.durationOfExposure
 * @apiParam {Object} row.acute_peak.representativenessOfData
 * @apiParam {Object} row.acute_peak.rateOfExposure
 * @apiParam {Object} row.acute_peak.meg
 * @apiParam {Object} row.acute_average
 * @apiParam {String} row.acute_average.severity
 * @apiParam {Object} row.acute_average.degreeOfExposure
 * @apiParam {Object} row.acute_average.durationOfExposure
 * @apiParam {Object} row.acute_average.representativenessOfData
 * @apiParam {Object} row.acute_average.rateOfExposure
 * @apiParam {Object} row.acute_average.meg
 * @apiParam {Object} row.chronic_average
 * @apiParam {String} row.chronic_average.severity
 * @apiParam {Object} row.chronic_average.degreeOfExposure
 * @apiParam {Object} row.chronic_average.durationOfExposure
 * @apiParam {Object} row.chronic_average.representativenessOfData
 * @apiParam {Object} row.chronic_average.rateOfExposure
 * @apiParam {Object} row.chronic_average.meg
 * @apiGroup Enhanced HRA
 * @apiSuccess {object} response
 * @apiSuccess {string} response.status
 * @apiSuccessExample Success
    HTTP/1.1 200 OK
    {
        "status":"success"
    }
 * @apiErrorExample Internal Server Error
    HTTP/1.1 500 Internal Server Error
 * @apiErrorExample  HRA not found
    HTTP/1.1 404 HRA not found
 */
router.post('/hra/:hraId/risk', async function(req, res){
    
    try 
    {
        // Validate the HRA ID with the database
        var hra = await db('chem_enhanced_hra')
            .where("id", req.params.hraId);

        if(!hra.length)
        {
            return res.sendStatus(404);
        }
        hra = hra[0];
        if(!canEdit(hra, req))
        {
            // If the HRA is complete, this is not allowed
            return res.sendStatus(403);
        }

        // Delete any existing assessments
        await db('cehra_assessment').where('chem_enhanced_hra_id', req.params.hraId).del();

        let add_risk_param = async function(params, risk_type, cas)
        {
            await db('cehra_assessment').insert({
                chem_enhanced_hra_id : req.params.hraId,
                type : risk_type,
                severity : params.severity.value,
                meg : params.meg.value.value,
                meg_name : params.meg.parm,
                meg_units : params.meg.value.units,
                meg_version : params.meg.version,
                prob_degree_of_exp : params.degreeOfExposure.value,
                prob_degree_of_exp_score : params.degreeOfExposure.score,
                prob_rep_of_data : params.representativenessOfData.value,
                prob_rep_of_data_score : params.representativenessOfData.score,
                prob_duration_of_exp : params.durationOfExposure.value,
                prob_duration_of_exp_score : params.durationOfExposure.score,
                prob_rate_of_exp : params.rateOfExposure.value,
                prob_rate_of_exp_score : params.rateOfExposure.score,
                probability : params.probability.value,
                probability_score : params.probability.score,
                risk_level : params.risk,
                confidence : params.confidence,
                cas_number : cas,
                custom_exposure_duration : params.custom_exposure_duration,
                custom_meg : params.custom_meg,
            });
        };

        if(req.body.length)
        {
            req.body.forEach(async chemical=>{
                if(chemical.acute_peak)
                    add_risk_param(chemical.acute_peak, 'acute_peak', chemical.cas_number);

                if (chemical.acute_average)
                    add_risk_param(chemical.acute_average, 'acute_average', chemical.cas_number);

                if (chemical.chronic_average)
                    add_risk_param(chemical.chronic_average, 'chronic_average', chemical.cas_number);
            });
        }

        // STUB 
        res.json({status:"success"});
    }
    catch(err)
    {
        res.status(500).send(err.message);
    }
});

/**
 * @api {post} /api/enhancedhra/hra/:hraId/outcomes Get potential health outcomes
 * @apiDescription Get potential health outcomes from the RD230 reference. Search by CAS number and MEG. 
 * @apiGroup Enhanced HRA
 * @apiParam {Number} hraId URL Parameter. The JHRM ID of the HRA
 * @apiSuccess {object} response
 * @apiSuccess {string} response.potential_health_outcomes_acute
 * @apiSuccess {string} response.potential_health_outcomes_chronic
 * @apiSuccessExample Success
    HTTP/1.1 200 OK
    {
        "potential_health_outcomes_acute":null,
        "potential_health_outcomes_chronic":null
    }
 * @apiErrorExample Internal Server Error
    HTTP/1.1 500 Internal Server Error
 * @apiErrorExample  HRA not found
    HTTP/1.1 404 HRA not found
 */
router.get('/hra/:hraId/outcomes', async function(req, res){
        
    // Validate the HRA ID with the database
    try{
        var hra = await db('chem_enhanced_hra')
            .where("id", req.params.hraId);
        if(!hra.length)
        {
            return res.sendStatus(404);
        }
        hra = hra[0];

        // Get CAS
        var cas = await db('cehra_prescreen')
            .where('chem_enhanced_hra_id', hra.id);
        if(!cas.length)
        {
            return res.sendStatus(404);
        }
        cas = cas[0];

        // Get Effects
        var effects = await db.raw(`
            select
                a.type,
                a.cas_number,
                m.health_effects_basis
            from cehra_assessment a
            left join meg_critical_effect m
                on m.casrn = a.cas_number
                and m.meg_parm = a.meg_name
                where a.chem_enhanced_hra_id = ?
        `, [hra.id]);

        res.json(effects[0]);
    }
    catch(err)
    {
        res.status(500).send(err.message);
    }
});

/**
 * @api {post} /api/enhancedhra/hra/:hraId/report Save report input
 * @apiDescription Save user-provided values from summary report input
 * @apiParam {Number} hraId URL Parameter. The JHRM ID of the HRA
 * @apiParam {Object} report JSON Body Parameter
 * @apiParam {String} report.title
 * @apiParam {String} report.potential_health_outcomes
 * @apiParam {String} report.recommended_mitigation
 * @apiGroup Enhanced HRA
 * @apiSuccess {object} response
 * @apiSuccess {string} response.status
 * @apiSuccessExample Success
    HTTP/1.1 200 OK
    {
        "status":"success"
    }
 * @apiErrorExample Internal Server Error
    HTTP/1.1 500 Internal Server Error
 * @apiErrorExample  HRA not found
    HTTP/1.1 404 HRA not found
 */
router.post('/hra/:hraId/report', async function(req, res){
    
    try 
    {
        // Validate the HRA ID with the database
        var hra = await db('chem_enhanced_hra')
            .where("id", req.params.hraId);

        if(!hra.length)
        {
            return res.sendStatus(404);
        }
        hra = hra[0];
        if(!canEdit(hra, req))
        {
            // If the HRA is complete, this is not allowed
            return res.sendStatus(403);
        }

        await db('cehra_report')
            .where('chem_enhanced_hra_id', req.params.hraId)
            .del();

        await db('cehra_assessment')
            .update({
                confidence : req.body.overall_acute_confidence
            })
            .where('chem_enhanced_hra_id', req.params.hraId)
            .whereIn('type', ['acute_peak','acute_average']);

        await db('cehra_assessment')
            .update({
                confidence : req.body.overall_chronic_confidence
            })
            .where('chem_enhanced_hra_id', req.params.hraId)
            .whereIn('type', ['chronic_average']);

        await db('cehra_report').insert({
            chem_enhanced_hra_id : req.params.hraId,
            recommended_mitigation : req.body.recommended_mitigation,
            potential_health_outcomes_acute : req.body.potential_health_outcomes_acute,
            potential_health_outcomes_chronic : req.body.potential_health_outcomes_chronic,
            executive_summary : req.body.executive_summary,
            force_health_protection_recommendations : req.body.force_health_protection_recommendations
        });

        await db('chem_enhanced_hra').update({
            'status' : hrastatus.COMPLETE
        }).where("id", req.params.hraId);

        res.json({status:"success"});
    }
    catch(err)
    {
        res.status(500).send(err.message);
    }
});

/**
 * @api {get} /api/enhancedhra/hra/:hraId/audit Audit Log
 * @apiDescription Generate audit log for HRA
 * @apiParam {Number} hraId URL Parameter. The JHRM ID of the HRA
 * @apiGroup Enhanced HRA
 * @apiSuccess {object} response
 * @apiSuccess {string} response.status
 * @apiSuccessExample Success
    HTTP/1.1 200 OK
    {
        "status":"success"
    }
 * @apiErrorExample Internal Server Error
    HTTP/1.1 500 Internal Server Error
 * @apiErrorExample  HRA not found
    HTTP/1.1 404 HRA not found
 */
router.get("/hra/:hraId/audit", async function(req, res){
    try 
    {
        // Validate the HRA ID with the database
        var hra = await db('chem_enhanced_hra')
            .where("id", req.params.hraId);

        if(!hra.length)
        {
            return res.sendStatus(404);
        }

        // Only allowed if HRA is complete
        if(hra.status = hrastatus.COMPLETE)
        {
            return res.sendStatus(403);
        }

        res.json({
            status: "Success",
            message : "This method is currently a stub. Validation has passed."
        })
    }
    catch(err)
    {
        res.status(500).send(err.message);
    }
});

router.get("/hra/:hraId/report_data", async function(req, res){

    try
    {
        let data = await db('cehra_report')
        .where('chem_enhanced_hra_id', req.params.hraId);
        data = data[0];
        data.creation_time = new Datetime(data.creation_time);

        res.json(data);
    }
    catch(err)
    {
        res.status(500).send(err.message);
    }
});

/**
 * @api {post} /api/enhancedhra/hra/:hraId/report Generate PDF Report
 * @apiDescription Generate PDF report for HRA
 * @apiGroup Enhanced HRA
 * @apiParam {Number} hraId URL Parameter. The JHRM ID of the HRA
 * @apiGroup Enhanced HRA
 * @apiSuccess {PDF} Report
 * @apiErrorExample Internal Server Error
    HTTP/1.1 500 Internal Server Error
 * @apiErrorExample  HRA not found
    HTTP/1.1 404 HRA not found
 */
router.get("/hra/:hraId/report", async function(req, res){

    let browser = await puppeteer.launch();

    let page = await browser.newPage();
    page.on(
        'load', async () =>
        {
            await page.evaluate(() => window.populate());

            let pdf = await page.pdf(
                {
                    printBackground : true,
                    format : "Letter"
                }
            );

            res.contentType("application/pdf");
            res.send(pdf);
        }
    );
    await page.goto(`http://localhost:9000/ehra_print_report.html?ehra=${req.params.hraId}`);
});

/**
 * @api {post} /api/enhancedhra/hra/:hraId/prescreen Get PEPC
 * @apiDescription Get the acute PEPC for the HRA
 * @apiParam {Number} hraId URL Parameter. The JHRM ID of the HRA
 * @apiGroup Enhanced HRA
 * @apiSuccess {Object[]} row[] Array of table rows
 * @apiSuccess {String} row.analyte_name
 * @apiSuccess {DimensionedValue} row.peak_value
 * @apiSuccess {DimensionedValue} row.avg_value
 * @apiSuccess {DimensionedValue} row.chronic_meg
 * @apiSuccess {String} row.chronic_meg_exceeded
 * @apiSuccess {String} row.acute_meg
 * @apiSuccess {DimensionedValue} row.acute_meg_value
 * @apiSuccess {String} row.acute_meg_exceeed
 * @apiSuccess {Number} row.chronic_meg_detection_frequency
 * @apiSuccessExample {json} Success
    HTTP/1.1 200 OK
    [
    {
        "analyte_name":"PARTICULATE <2.5M (PM-2.5)",
        "peak_value":{
            "value":0.14613,
            "isValid":true,
            "originalValue":0.14613,
            "units":"mg/m3",
            "precision":7
        },
        "avg_value":{
            "value":0.0918555,
            "isValid":true,
            "originalValue":0.0918555,
            "units":"mg/m3",
            "precision":9
        },
        "chronic_meg_value":{
            "value":0.015,
            "isValid":true,
            "originalValue":0.015,
            "units":"mg/m3",
            "precision":5
        },
        "chronic_meg":"1yearNEG",
        "chronic_meg_detection_frequency":100,
        "chronic_meg_exceeded":"Yes",
        "acute_meg":"24hourNEG",
        "acute_meg_value":{
            "value":0.065,
            "isValid":true,
            "originalValue":0.065,
            "units":"mg/m3",
            "precision":5
        },
        "acute_meg_exceeded":"Yes"
    }
    ]
 * @apiErrorExample Internal Server Error
    HTTP/1.1 500 Internal Server Error
 * @apiErrorExample  HRA not found
    HTTP/1.1 404 HRA not found
 */
router.get('/hra/:hraId/prescreen', async function (req, res){
    
    try 
    {
        // Validate the HRA ID with the database
        var hra = await db('chem_enhanced_hra')
            .where("id", req.params.hraId);
        if(!hra.length)
        {
            return res.sendStatus(404);
        }

        // Get prescreen
        var rows = await db('cehra_prescreen')
        .where('chem_enhanced_hra_id', req.params.hraId);
        if(!rows.length)
        {
            return res.sendStatus(404);
        }

        res.json(rows.map(prescreen => { 
            return {
                'analyte_name' : prescreen.chemical_name,
                'cas_number' : prescreen.cas_number,
                'peak_value' : new DimensionedValue(prescreen.peak_pepc, prescreen.peak_pepc_units),
                'avg_value' : new DimensionedValue(prescreen.avg_pepc, prescreen.avg_pepc_units),
                'chronic_meg_value' : new DimensionedValue(prescreen.cr_meg, prescreen.cr_meg_units),
                'chronic_meg' : '1yearNEG',
                'chronic_meg_detection_frequency' : prescreen.cr_detection_frequency,
                'chronic_meg_exceeded' : prescreen.cr_meg_exceeded ? "Yes" : "No",
                'acute_meg' : prescreen.ar_meg_name,
                'acute_meg_value' : new DimensionedValue(prescreen.ar_meg, prescreen.ar_meg_units),
                'acute_meg_exceeded' : prescreen.ar_meg_exceeded ? "Yes" : "No"
            };
        }));
    }
    catch(err)
    {
        res.status(500).send(err.message);
    }
    
});

/**
 * @api {post} /api/enhancedhra/hra/:hraId/results Associate Sample Results
 * @apiDescription Add sample results to an existing HRA
 * @apiParam {Number} hraId URL Parameter. The JHRM ID of the HRA that is being edited
 * @apiParam {Object} request JSON Body Parameter
 * @apiParam {number[]} object.resultIds The IDs of the results to add
 * @apiGroup Enhanced HRA
 * @apiSuccess {object} response
 * @apiSuccess {string} response.status
 * @apiSuccessExample Success
    HTTP/1.1 200 OK
    {
        "status":"success"
    }
 * @apiErrorExample Internal Server Error
    HTTP/1.1 500 Internal Server Error
 * @apiErrorExample  HRA not found
    HTTP/1.1 404 HRA not found
 */
router.post('/hra/:hraId/results', async function(req, res){
    
    try 
    {
        // Validate the HRA ID with the database
        var hra = await db('chem_enhanced_hra')
            .where("id", req.params.hraId);

        if(!hra.length)
        {
            return res.sendStatus(404);
        }
        hra = hra[0];
        if(!canEdit(hra, req))
        {
            // If the HRA is complete, this is not allowed
            return res.sendStatus(403);
        }

        // Get the results 
        var results = await db('cehra_sample_results')
        .where('chem_enhanced_hra_id', req.params.hraId)
        .whereIn('id', req.body.resultIds);

        // If we do not find the same number of results we were told to find, throw an error. 
        if(results.length != req.body.resultIds.length)
        {
            return res.sendStatus(404);
        }

        // Get previously selected 
        var selected = await db('cehra_sample_results')
        .where('chem_enhanced_hra_id', req.params.hraId)
        .where('selected', 1);

        // If the currently selected elements differ from the new ones
        if(selected.length != results.length || !selected.reduce((prev,curr)=>prev && req.body.resultIds.includes(curr.id), true))
        {
            // Copy megs
            let chemicals = [...new Set(results.map(result=>result.cas_number))];
            await db('cehra_megs').where('chem_enhanced_hra_id', req.params.hraId).del();
            let allMegs = await db('tg230_master_meg')
            .where('MEDIA','Air')
            .whereIn('CASRN', chemicals);
            await db('cehra_megs').insert(allMegs.map(meg=>{
                meg.chem_enhanced_hra_id = req.params.hraId;
                return meg;
            }))

            // Delete previous selection
            await db('cehra_sample_results')
            .where('chem_enhanced_hra_id', req.params.hraId)
            .update({
                selected : 0
            });

            // Set new selection
            let modified = await db('cehra_sample_results')
            .where('chem_enhanced_hra_id', req.params.hraId)
            .whereIn('id', req.body.resultIds)
            .update({
                selected : 1
            });

            // Calculate prescreen here for each chemical 
            await db('cehra_prescreen').where('chem_enhanced_hra_id',req.params.hraId).del();
            chemicals.forEach(async chemical=>{
                let megs = normalizeMegs(await db('cehra_megs')
                    .leftJoin('chemical_info', 'chemical_info.cas_number','cehra_megs.CASRN')
                    .where('cehra_megs.chem_enhanced_hra_id', req.params.hraId)
                    .where('cehra_megs.CASRN', chemical)
                );
                let chemicalResults = results.filter(result=>result.cas_number==chemical);
                let peak = calculatePepc.getPeakConcentration(chemicalResults, megs[0]);
                let avg = calculatePepc.getAvgConcentration(chemicalResults, megs[0]);
                let acute = calculatePepc.acutePrescreen(peak, megs);
                let chronic = calculatePepc.chronicPrescreen(peak, megs);
    
                await db('cehra_prescreen').insert({
                    'chem_enhanced_hra_id' : req.params.hraId,
                    'chemical_name' : chemicalResults[0].chemical_name,
                    'cas_number' : chemicalResults[0].cas_number,
                    'valid_samples' : calculatePepc.prescreen(chemicalResults) ? 1 : 0,
                    'peak_pepc' : peak.value,
                    'peak_pepc_units' : peak.units,
                    'avg_pepc' : avg.value,
                    'avg_pepc_units' : avg.units,
                    'ar_meg_name' : acute.meg.parm,
                    'ar_meg' : acute.meg.value.value,
                    'ar_meg_units' : acute.meg.value.units,
                    'ar_meg_exceeded' : acute.exceeded ? 1 : 0,
                    'cr_meg_name' : chronic.meg.parm,
                    'cr_meg' : chronic.meg.value.value,
                    'cr_meg_units' : chronic.meg.value.units,
                    'cr_meg_exceeded' : chronic.exceeded ? 1 : 0,
                    'cr_detection_frequency' : calculatePepc.getDetectionFrequency(chemicalResults)
                });
            });
        }
        
        res.json({
            status : 'Success'
        });
    }
    catch(err)
    {
        res.status(500).send(err.message);
    }
});

/**
 * @api {get} /api/enhancedhra/hra/:hraId/results List Associated Results
 * @apiDescription List sample results for an HRA based on the samples selected. 
 * @apiParam {Number} hraId The JHRM ID of the HRA
 * @apiParam {String} chemical Optional. Filter the results to a single chemical
 * @apiParam {Boolean} selected Optional. Only return selected results
 * @apiGroup Enhanced HRA
 * @apiSuccess {Object[]} Result[] Array of sample results
 * @apiSuccess {String} result.id
 * @apiSuccess {Number} result.id_doehrs_sample_id
 * @apiSuccess {Number} result.id_doehrs_result_id
 * @apiSuccess {String} result.chemical_name 
 * @apiSuccess {Date} result.cas_number 
 * @apiSuccess {String} result.concentration 
 * @apiSuccess {String} result.units
 * @apiSuccess {String} result.analytical_method
 * @apiSuccess {String} result.found_meg
 * @apiSuccess {String} result.exceeds_1_year_neg
 * @apiSuccess {String} result.selected
 * @apiSuccessExample {json} Success
    HTTP/1.1 200 OK
    [{
      "id":30,
      "id_doehrs_sample_id":"0000KQ1T",
      "id_doehrs_result_id":1172,
      "chemical_name":"Antimony",
      "concentration":0.069584,
      "units":"ug/m3",
      "cas_number":"7440-36-0",
      "analytical_method":"EPA 200.8",
      "found_meg":true,
      "exceeds_1_year_neg":"No",
      "selected":0
   }]
 * @apiErrorExample Internal Server Error
    HTTP/1.1 500 Internal Server Error
 * @apiErrorExample  HRA not found
    HTTP/1.1 404 HRA not found
 */
router.get('/hra/:hraId/results', async function(req, res){
    try 
    {
        // Validate the HRA ID with the database
        var hra = await db('chem_enhanced_hra')
            .where("id", req.params.hraId);

        if(!hra.length)
        {
            return res.sendStatus(404);
        }

        hra = hra[0]

		var query = db('cehra_sample_results')
			.columns({
				id : 'id',
				id_doehrs_sample_id : 'id_doehrs_sample_id',
				chemical_name : 'chemical_name',
				cas_number : 'cas_number',
				concentration : 'concentration',
				units : 'units',
				chem_enhanced_hra_id : 'chem_enhanced_hra_id',
				analytical_method : db.raw("IF(analytical_method IS NULL OR analytical_method = '', 'GRAV', analytical_method)"),
				concentration_below_threshold : 'concentration_below_threshold',
				id_doehrs_result_id : 'id_doehrs_result_id',
				selected : 'selected'
			})
			.where('chem_enhanced_hra_id', req.params.hraId);
		
		// No validation of chemical names
		if(req.query.chemical)
		{
			query.where('chemical_name', req.query.chemical);
		}

		// Limit to selected results if requested
		if(req.query.selected)
		{
			query.where('selected', 1);
		}

		// Send query
		var results = await query.select();

		// Get MEG(s)
		var megs = normalizeMegs(await db('tg230_master_meg')
		    .leftJoin('chemical_info', 'chemical_info.cas_number','tg230_master_meg.CASRN')
		    .whereIn('CASRN', results.map(result=>result.cas_number))
			.where('MEG_PARM', '1yearNEG')
			.where('MEDIA', 'Air'));

		// Return data, even if it is an empty array. 
		res.json(results.map(result=>{
			let meg = megs.find(meg=>meg.cas_number==result.cas_number);

			return {
				id : result.id,
				id_doehrs_sample_id : result.id_doehrs_sample_id,
				id_doehrs_result_id : result.id_doehrs_result_id,
				chemical_name : result.chemical_name,
				concentration : result.concentration,
				units : result.units,
				cas_number : result.cas_number,
				analytical_method: result.analytical_method,
                found_meg : meg ? true : false,
                concentration_below_threshold : result.concentration_below_threshold,
				exceeds_1_year_neg : meg && result.concentration_below_threshold==0 && DimensionedValue.compare(
					new DimensionedValue(result.concentration, result.units), 
					meg.value, 
					meg
				)>=0 ? 'Yes' : 'No',
				selected : result.selected
			}}
		));

    }
    catch(err)
    {
        res.status(500).send(err.message);
    }
});

/**
 * @api {post} /api/enhancedhra/hra/:hraId/samples Associate Samples
 * @apiDescription Add samples to an existing HRA
 * @apiParam {Number} hraId URL Parameter. The JHRM ID of the HRA that is being edited
 * @apiParam {Object} request JSON Body Parameter
 * @apiParam {number[]} object.sampleIds The IDs of the results to add
 * @apiGroup Enhanced HRA
 * @apiSuccess {object} response
 * @apiSuccess {string} response.status
 * @apiSuccessExample Success
    HTTP/1.1 200 OK
    {
        "status":"success"
    }
 * @apiErrorExample Internal Server Error
    HTTP/1.1 500 Internal Server Error
 * @apiErrorExample  HRA not found
    HTTP/1.1 404 HRA not found
 */
router.post('/hra/:hraId/samples', async function(req, res){
    
    try 
    {
        // Validate the HRA ID with the database
        var hra = await db('chem_enhanced_hra')
            .where("id", req.params.hraId);

        if(!hra.length)
        {
            return res.sendStatus(404);
        }
        hra = hra[0];
        if(!canEdit(hra, req))
        {
            // If the HRA is complete, this is not allowed
            return res.sendStatus(403);
        }

        // Get the samples from the doehrs tables
        var samples = await db('cehra_sample')
        .where('chem_enhanced_hra_id', req.params.hraId)
        .whereIn('id_doehrs_sample_id', req.body.sampleIds);

        // If we do not find the same number of samples we were told to find, throw an error. 
        if(samples.length != req.body.sampleIds.length)
        {
            return res.sendStatus(500);
        }

        // Get the selected samples from the doehrs tables
        var selected = await db('cehra_sample')
        .where('selected', 1)
        .where('chem_enhanced_hra_id', req.params.hraId)

        // If the currently selected elements differ from the new ones
        if(selected.length != samples.length || !selected.reduce((prev,curr)=>prev && req.body.sampleIds.includes(curr.id_doehrs_sample_id), true))
        {
            // Delete previous selection
            await db('cehra_sample')
            .where('chem_enhanced_hra_id', req.params.hraId)
            .update({
                selected : 0
            });

            // Set new selection
            let modified = await db('cehra_sample')
            .where('chem_enhanced_hra_id', req.params.hraId)
            .whereIn('id_doehrs_sample_id', req.body.sampleIds)
            .update({
                selected : 1
            });

            // Delete any previously associated results
            await db('cehra_sample_results').where('chem_enhanced_hra_id', req.params.hraId).del();

            // Copy all results from the selected samples
            var results = await db('id_doehrs_result')
            .whereIn('id_doehrs_sample_id', req.body.sampleIds);
            
            await db('cehra_sample_results').insert(results.map(result=>{

                var concentration, threshold;
                if(typeof result.concentration === 'string' && result.concentration[0]=='<')
                {
                    concentration = parseFloat(result.concentration.replace('<',''));
                    threshold = 1;
                }
                else if(result.concentration==='')
                {
                    concentration = 0;
                    threshold = 1;
                }
                else
                {
                    concentration = parseFloat(result.concentration);
                    threshold = result.concentration_below_threshold;
                }
                return {
                    id_doehrs_result_id : result.id,
                    id_doehrs_sample_id : result.id_doehrs_sample_id,
                    chem_enhanced_hra_id : req.params.hraId,
                    chemical_name : result.parameter,
                    cas_number : result.cas_number,
                    concentration : concentration,
                    units : result.units,
                    analytical_method : result.analytical_method,
                    concentration_below_threshold : threshold,
                };
            }))
            
        }

        res.json({
            status : 'Success'
        });
    }
    catch(err)
    {
        res.status(500).send(err.message);
    }
});

/**
 * @api {get} /api/enhancedhra/hra/:hraId/samples List Samples
 * @apiDescription Get samples for an HRA based on the exposure pathway and optionally filter to show only selected samples
 * @apiParam {Number} hraId The JHRM ID of the HRA
 * @apiParam {Boolean} selected Optional. Only return selected samples
 * @apiParam {String} start Optional. YYYY-MM-DD Filter the results by date. 
 * @apiParam {String} end Optional. YYY-MM-DD Filter the results by date
 * @apiGroup Enhanced HRA
 * @apiSuccess {Object[]} Sample[] Array of Samples
 * @apiSuccess {Number} sample.id
 * @apiSuccess {String} sample.id_doehrs_sample_id
 * @apiSuccess {Number} sample.chem_enhanced_hra_id
 * @apiSuccess {String} sample.sampling_point 
 * @apiSuccess {Date} sample.start_date_time 
 * @apiSuccess {String} sample.exposure_notes 
 * @apiSuccess {Number} sample.sample_time
 * @apiSuccess {Number} sample.approved_by_qa
 * @apiSuccess {Number} sample.media
 * @apiSuccess {Number} sample.selected
 * @apiSuccess {Number} sample.analytical_method
 * @apiSuccessExample {json} Success
    HTTP/1.1 200 OK
    [{
        "id":46,
        "id_doehrs_sample_id":"0000KQ1T",
        "chem_enhanced_hra_id":"E-FFGTUSD1ZS",
        "sampling_point":"Office Area",
        "start_date_time":"2018-01-25T13:30:00.000Z",
        "exposure_notes":"Continues air sampling",
        "sample_time":1440,
        "approved_by_qa":1,
        "media":"AIR",
        "selected":0,
        "analytical_method":"EPA 200.8,GRAV"
    }]
 * @apiErrorExample Internal Server Error
    HTTP/1.1 500 Internal Server Error
 * @apiErrorExample  HRA not found
    HTTP/1.1 404 HRA not found
 */
router.get('/hra/:hraId/samples', async function(req, res){
    try 
    {
        // Validate the HRA ID with the database
        var hra = await db('chem_enhanced_hra')
            .where("id", req.params.hraId);

        if(!hra.length)
        {
            return res.sendStatus(404);
        }

        hra = hra[0];

        // Get samples
        var query = db('cehra_sample')
        .columns({
            id : 'id',
            id_doehrs_sample_id : 'id_doehrs_sample_id',
            chem_enhanced_hra_id : 'chem_enhanced_hra_id',
            sampling_point : 'sampling_point',
            start_date_time : 'start_date_time',
            exposure_notes : 'exposure_notes',
            sample_time : 'sample_time',
            approved_by_qa : 'approved_by_qa',
            media : 'media',
            selected : 'selected',
            analytical_method : db.raw("(SELECT GROUP_CONCAT(DISTINCT IF(analytical_method IS NULL OR analytical_method = '', 'GRAV', analytical_method)) FROM `id_doehrs_result` WHERE `id_doehrs_sample_id` = `cehra_sample`.`id_doehrs_sample_id`)")
        })
        .where('chem_enhanced_hra_id', req.params.hraId)

        // Add selected filter
        if(req.query.selected)
        {
            query.where('selected', 1);
        }

        // Add start filter
        if(req.query.start)
        {
            var start = moment(req.query.start, "YYYY-MM-DD");
            if(!start.isValid())
            {
                return res.status(422).send("Invalid parameter: start");
            }
            query.where('start_date_time', '>', start.format('YYYY-MM-DD HH:mm:ss'));
        }

        // Add end filter
        if(req.query.end)
        {
            var end = moment(req.query.end, "YYYY-MM-DD");
            if(!end.isValid())
            {
                return res.status(422).send("Invalid parameter: end");
            }
            query.where('start_date_time', '<', end.format('YYYY-MM-DD HH:mm:ss'));
        }

        // Send query
        var samples = await query.select();
        
        // Return data, even if it is an empty array
        res.json(samples.map(sample=>{
            sample.start_date_time = new Datetime(sample.start_date_time);
            return sample;
        }));

    }
    catch(err)
    {
        res.status(500).send(err.message);
    }
});

/**
 * @api {post} /api/enhancedhra/hra/:hraId/duplicate Duplicat HRA
 * @apiDescription Duplicate and Enhanced HRA, copying all user inputs, and set status to INCOMPLETE
 * @apiParam {Number} hraId The JHRM ID of the HRA
 * @apiParam {String} created_by
 * @apiParam {String} priority
 * @apiGroup Enhanced HRA
 * @apiSuccess {Object} Data{} JSON Data
 * @apiSuccess {String} Status
 * @apiSuccessExample {json} Success
    HTTP/1.1 200 OK
    {
        "status": "Success"
        "hra_id": "H-s554he5s"
    }
 * @apiErrorExample Internal Server Error
    HTTP/1.1 500 Internal Server Error
 * @apiErrorExample  HRA not found
    HTTP/1.1 404 HRA not found
 */
router.post('/hra/:hraId/duplicate', async function(req,res){
    
    try{

        // Validate inputs
        if(!req.body.created_by)
        {
            return res.status(403).send('Field created_by is required.');
        }

        // Validate the HRA ID with the database
        let hra = await db('chem_enhanced_hra')
            .where("id", req.params.hraId);
        if(!hra.length)
        {
            return res.sendStatus(404);
        }
        hra = hra[0];

        // Perform the following queries in an transaction so we do not end up wth 
        // a partially copied HRA 
        await db.transaction(async trx => {

            // Create new HRA record. Change created time, person who created it, status, and reset qa approval.
            hra.id = uniqueid('E-');
            hra.created_by = req.body.created_by
            hra.created_on = db.fn.now(6),
            hra.priority = req.body.priority,
            hra.status = hrastatus.INCOMPLETE
            hra.approved_by = null
            await trx('chem_enhanced_hra').insert(hra);

            // Duplicate all related records, with new ids and updating the HRA id. 
            let tables = ['cehra_sample','cehra_sample_results','cehra_prescreen','cehra_assessment','cehra_report'];
            for(var i = 0; i < tables.length; i++)
            {
                let table = tables[i];
                let records = await trx(table).where('chem_enhanced_hra_id', req.params.hraId);
                if(records.length)
                {
                    await trx(table).insert(records.map(record=>{
                        delete record.id;
                        record.chem_enhanced_hra_id = hra.id;
                        return record;
                    }));
                }
            }
        })

        return res.json({
            status : "Success",
            hra_id : hra.id
        })
    }
    catch(err)
    {
        res.status(500).send(err.message);
    }
})


/**
 * @api {get} /api/enhancedhra/hra/:hraId/pathway Get Pathway
 * @apiDescription Get exposure pathway for the HRA if one exists. 
 * @apiParam {Number} hraId The JHRM ID of the HRA
 * @apiGroup Enhanced HRA
 * @apiSuccess {Object} pathway Exposure pathway
 * @apiSuccess {Number} pathway.id
 * @apiSuccess {String} pathway.population_at_risk_descr
 * @apiSuccess {String} pathway.name
 * @apiSuccess {String} pathway.source
 * @apiSuccess {String} pathway.environmental_media
 * @apiSuccess {String} pathway.health_threat
 * @apiSuccess {String} pathway.exposure_route
 * @apiSuccess {String} pathway.affected_personnel
 * @apiSuccess {String} pathway.priority
 * @apiSuccessExample {json} Success
    HTTP/1.1 200 OK
    [{
        "id":14681,
        "name":"PM2.5 AMBIENT AIR",
        "source":"Ambient Air",
        "environmental_media":"Air",
        "health_threat":"Airborne Contaminants/Particulate Matter",
        "exposure_route":"Inhalation",
        "affected_personnel":1,
        "population_at_risk_descr":"Entire Camp",
        "priority":"Low"
    }]
 * @apiErrorExample Internal Server Error
    HTTP/1.1 500 Internal Server Error
 * @apiErrorExample  HRA not found
    HTTP/1.1 404 HRA not found
 */
router.get('/hra/:hraId/pathway', async function(req,res){
    
    try{
        // Validate the HRA ID with the database
        var hra = await db('chem_enhanced_hra')
            .where("id", req.params.hraId);

        if(!hra.length)
        {
            return res.sendStatus(404);
        }

        hra = hra[0];
        
        var pathway = await db('id_doehrs_pathway')
            .select('id')
            .select('name')
            .select('source')
            .select('environmental_media')
            .select('health_threat')
            .select('exposure_route')
            .select('affected_personnel')
            .select('population_at_risk_descr')
            .select('priority')
            .where('id', hra.id_doehrs_pathway_id);

        res.json(pathway);
    }
    catch(err)
    {
        res.status(500).send(err.message);
    }
})

/**
 * @api {get} /api/enhancedhra/pathway List Exposure Pathways
 * @apiDescription List exposure pathways optionally filtered by location and environmental media. 
 * @apiParam {String} location Location (Optional)
 * @apiParam {String} env_media Environmental Media (Optional)
 * @apiGroup  EHRA DOEHRS Data
 * @apiSuccess {Object[]} pathway[] Array of exposure pathways
 * @apiSuccess {Number} pathway.id
 * @apiSuccess {String} pathway.population_at_risk_descr
 * @apiSuccess {String} pathway.name
 * @apiSuccess {String} pathway.source
 * @apiSuccess {String} pathway.environmental_media
 * @apiSuccess {String} pathway.health_threat
 * @apiSuccess {String} pathway.exposure_route
 * @apiSuccess {String} pathway.affected_personnel
 * @apiSuccess {String} pathway.priority
 * @apiSuccessExample {json} Success
    HTTP/1.1 200 OK
    [{
        "id":14681,
        "name":"PM2.5 AMBIENT AIR",
        "source":"Ambient Air",
        "environmental_media":"Air",
        "health_threat":"Airborne Contaminants/Particulate Matter",
        "exposure_route":"Inhalation",
        "affected_personnel":1,
        "population_at_risk_descr":"Entire Camp",
        "priority":"Low"
    }]
 * @apiErrorExample Invalid parameter
    HTTP/1.1 422 Invalid parameter
 * @apiErrorExample Internal Server Error
    HTTP/1.1 500 Internal Server Error
 */
router.get('/pathway', async function(req,res){

    try
    {		
        var query = db('id_doehrs_pathway')
            .select('id')
            .select('name')
            .select('source')
            .select('location')
            .select('environmental_media')
            .select('health_threat')
            .select('exposure_route')
            .select('affected_personnel')
            .select('population_at_risk_descr')
            .select('priority');

        // If a location ws specified, validate and add it as a filter
        if(req.query.location)
        {
            // Validate location in database. This is carried over from the earlier code. Not sure if it is 
            // necessary. Or if it should validate against the locations table. 
            var location = await db('id_doehrs_pathway')
            .where('location', req.query.location);

            if(location.length)
            {
                query.where('location', req.query.location);
            }
            else 
            {
                return res.status(422).send("Invalid parameter: end");
            }
        }

        if(req.query.env_media)
        {
            query.where('environmental_media', req.query.env_media);
        }

        var pathways = await query.select();
        res.json(pathways);

    }
    catch(err)
    {
        res.status(500).send(err.message);
    }
})

/**
 * @api {get} /api/enhancedhra/locations List Locations
 * @apiDescription List DOEHRS-IH locations. 
 * @apiGroup  EHRA DOEHRS Data
 * @apiSuccess {Object[]} location[] Array of locations
 * @apiSuccess {Number} location.location_name
 * @apiSuccessExample {json} Success
    HTTP/1.1 200 OK
    [{
        "ilocation_name":14681,
    }]
 * @apiErrorExample Internal Server Error
    HTTP/1.1 500 Internal Server Error
 */
router.get('/locations', async function(req,res){
    try{
        //////// Take out Iraq for production //////////
        let result = await db.distinct('location_name').from('jhrmecddb.id_doehrs_ih_locations').where('country', 'Iraq').orWhere('location_name','KANDAHAR AFLD').orderBy('location_name','asc');

        for (let i = 0; i < result.length; ++i)
            result[i].location_name = 'Base Camp ' + result[i].location_name;

        res.json(result);
    }
    catch(err)
    {
        res.status(500).send(err.message);
    }
})


/**
 * @api {get} /api/enhancedhra/qa/:hraId QA status of HRA
 * @apiDescription Return current QA status for a given HRA or 404 if no QA is 
 *     found. 
 * @apiGroup Enhanced HRA
 */
 router.get('/hra/qa/:hraId', async function(req, res){
    try
    {
        // Dont bother to check whether the HRA exists, it must exist for the 
        // QA to exist. 
        var qa = await db('chem_enhanced_hra_qa_tool')
            .where("hra_id", req.params.hraId);

        //On a first pass, this will not work.  Unless otherwise, we should remove it.
        /* if(!qa.length)
        {
            return res.sendStatus(404);
        } */

        return res.json(new EnhancedHraQa(qa[0]));
    }
    catch(err)
    {
        res.status(500).send(err.message);
    }
});

/**
 * @api {post} /api/enhancedhra/qa/:hraId Update QA status of HRA
 * @apiDescription Update the QA status, overwriting any existing QA status. 
 * @apiGroup Enhanced HRA
 */
 router.post('/hra/qa/:hraId', async function(req, res){
    try
    {
        var hra = await db('chem_enhanced_hra')
            .where("id", req.params.hraId);

        // If there is no HRA, we can't QA it. 
        if(!hra.length)
        {
            return res.sendStatus(404);
        }

        // If the HRA is incomplete, we can't QA it. 
        if(hra[0].status == hrastatus.INCOMPLETE)
        {
            return res.status(500)
                .send("Unable to save QA for incomplete HRA");
        }

        // HRA can only be QAed if user has QA
        let user = users.find(user=>user.username==req.cookies.user_name);
        if(!user.role.hra_qa)
        {
            return res.status(500)
                .send("User does not have QA permission");
        }

        // Must be a valid QA item 
        if(!(req.body instanceof EnhancedHraQa))
        {
            return res.status(500)
                .send("Not a valid QA");
        }

        // Force HRA ID based on the URL. 
        req.body.hra_id = req.params.hraId;
        
        // This is an edit
        let id = req.body.id;
        if(id!==null)
        {
            await db('chem_enhanced_hra_qa_tool')
                .update({
                    hra_id : req.body.hra_id,
                    hra_objective_comments : req.body.hra_objective_comments,
                    hra_objective_approve : req.body.hra_objective_approve,
                    hra_objective_edit : req.body.hra_objective_edit,
                    hazard_probability_comments : req.body.hazard_probability_comments,
                    hazard_probability_approve : req.body.hazard_probability_approve,
                    hazard_probability_edit : req.body.hazard_probability_edit,
                    executive_summary_comments : req.body.executive_summary_comments,
                    executive_summary_approve : req.body.executive_summary_approve,
                    executive_summary_edit : req.body.executive_summary_edit,
                    fhp_recommendations_comments : req.body.fhp_recommendations_comments,
                    fhp_recommendations_approve : req.body.fhp_recommendations_approve,
                    fhp_recommendations_edit : req.body.fhp_recommendations_edit,
                    samples_included_in_hra_comments : req.body.samples_included_in_hra_comments,
                    samples_included_in_hra_approve : req.body.samples_included_in_hra_approve,
                    samples_included_in_hra_edit : req.body.samples_included_in_hra_edit                    
                })
                .where('id', id);        }
        else 
        {
            id = await db('chem_enhanced_hra_qa_tool')
                .insert({
                    hra_id : req.body.hra_id,
                    hra_objective_comments : req.body.hra_objective_comments,
                    hra_objective_approve : req.body.hra_objective_approve,
                    hra_objective_edit : req.body.hra_objective_edit,
                    hazard_probability_comments : req.body.hazard_probability_comments,
                    hazard_probability_approve : req.body.hazard_probability_approve,
                    hazard_probability_edit : req.body.hazard_probability_edit,
                    executive_summary_comments : req.body.executive_summary_comments,
                    executive_summary_approve : req.body.executive_summary_approve,
                    executive_summary_edit : req.body.executive_summary_edit,
                    fhp_recommendations_comments : req.body.fhp_recommendations_comments,
                    fhp_recommendations_approve : req.body.fhp_recommendations_approve,
                    fhp_recommendations_edit : req.body.fhp_recommendations_edit,
                    samples_included_in_hra_comments : req.body.samples_included_in_hra_comments,
                    samples_included_in_hra_approve : req.body.samples_included_in_hra_approve,
                    samples_included_in_hra_edit : req.body.samples_included_in_hra_edit                    
                });
        }

        if ( req.body.hra_objective_approve == true && req.body.hazard_probability_approve == true && req.body.executive_summary_approve == true
            && req.body.fhp_recommendations_approve == true && req.body.samples_included_in_hra_approve == true)
            {
                await db('chem_enhanced_hra').update({
                    'approved_by' : user.username
                }).where('id',req.body.hra_id);
            }

        return res.json({
            status : "Success",
            id : id
        })

    }
    catch(err)
    {
        return res.status(500).send(err.message);
    }
});

module.exports = router