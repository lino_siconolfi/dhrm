const SmartabaseApi = require('src/smartabase/api-library');
const esmImport = require('esm')(module);
const express = require('express');
const router = express.Router();
const db = require('src/util/db');
const createError = require('http-errors');
const config = require('config');
const multer = require('multer');

var storage = multer.memoryStorage();
var upload = multer({
    limits : {
        fileSize: config.get('maxFileUploadSize')
    },
    storage : storage
});
var users = require('../../util/users');
const { 
    IFA_INCIDENT_SCENARIO, 
    IFA_INCIDENT_TYPE,
    IFA_INCIDENT_CAUSE,
    IFA_TRINARY,
    IFA_SUBMISSION_STATUS,
    IFA_QA_STATUS,
    IFA_STATUS,
    IFA_PREPARER_TYPE,
    IFA_ATTACHMENT_TYPE
} = require('../../shared/Constants');
const { Datetime } = esmImport('../../shared/datetime');
const { IfaPersonnel, IfaConcussiveDetails, IfaChemicalDetails, IfaReport, IfaPreparer } = esmImport("../../shared/IFA.js");

/**
 * Helper function to query the database for all preparers related to the 
 * incident and attach them to the report. 
 * 
 * @param {IfaReport} report 
 */
async function addPreparers(report)
{
    if(report.id)
    {
        let preparers = await db('ifa_preparer').where('ifa_incident_id', report.id);
        preparers.forEach(row=>report.addPreparer(new IfaPreparer(row)));
    }
    return report;
}

/**
 * Helper function to query the database for all incident details related to the 
 * incident and attach them to the report. 
 * 
 * This requires separa
 * 
 * @param {IfaReport} report 
 */
async function addIncidentDetails(report)
{
    if(report.id)
    {
        let chemicalDetails = await db('ifa_incident_chemical').where('ifa_incident_id', report.id);
        chemicalDetails.forEach(row=>report.addDetails(IfaChemicalDetails.normalize(row)));
        let concussiveDetails = await db('ifa_incident_concussive').where('ifa_incident_id', report.id);
        concussiveDetails.forEach(row=>report.addDetails(IfaConcussiveDetails.normalize(row)));
    }
    return report;
}

async function saveIfa(report, user)
{
    // list each column in the ifa_incident table. Commented out if not (yet) used.
    let ifa_incident = {
        incident_type : report.incident_type.join(','),
        report_start_datetime : report.report_start_datetime.date,
        report_completion_datetime : report.report_completion_datetime ? report.report_completion_datetime.date : null,
        // last_edited_by : , // Set below
        last_edited_on : db.fn.now(6),
        status : report.status,
        name : report.name,
        short_name : report.short_name,
        ccir_sigact_num : report.ccir_sigact_num,
        scenario : report.scenario,
        start_datetime : report.start_datetime ? report.start_datetime.date : null,
        end_datetime : report.end_datetime ? report.end_datetime.date : null,
        cause : report.cause,
        cause_descr : report.cause_descr,
        summary : report.summary,
        mgrs : report.location ? report.location.gridRef : null,
        latitude : report.location ? report.location.point[1] : null,
        longitude : report.location ? report.location.point[0] : null,
        units_involved : report.units_involved,
        // were_you_at_incident_location : report,
        qa_status : report.qa_status,
        submission_status : report.submission_status,
        mitigation_ppe : report.ppe_available,
        mitigation_personal_decontamination : report.ppe_decontamination,
        mitigation_area_decontamination : report.area_equip_decontamination,
        mitigation_medical_countermeasures : report.countermeasures_used,
        action_risk_communication : report.action_risk_comms,
        action_risk_communication_descr : report.risk_comms_descr,
        action_summary_of_exposures : report.action_gen_summary,
        action_summary_of_exposures_desc : report.gen_summary_descr,
        action_other : report.action_other,
        action_other_desc : report.other_descr
    };

    let ifa_preparer = {
        name : user.first_name + ' ' + user.last_name,
        email : user.email,
        phone_num : user.phone, 
        unit : user.unit
    };

    if(report.id)
    {
        // Add the current user to the list of preparers
        ifa_preparer.ifa_incident_id = report.id;
        ifa_preparer.type = IFA_PREPARER_TYPE.ADDITIONAL;
        let preparer_id = await db("ifa_preparer").insert(ifa_preparer);

        // Update report
        ifa_incident.last_edited_by = preparer_id;
        await db('ifa_incident')
            .update(ifa_incident)
            .where("id", report.id);
    }
    else 
    {
        // Save the report. We do not need to set the last)edited_by because 
        // it defaults to the initial preparer 
        report.id = await db('ifa_incident').insert(ifa_incident);

        // Save the curent user as the initial preparer. 
        ifa_preparer.ifa_incident_id = report.id;
        ifa_preparer.type = IFA_PREPARER_TYPE.INITIAL;
        await db('ifa_preparer').insert(ifa_preparer);
    }

    let getArr = (val) => Array.isArray(val) ? val[0] : val;
    let getInt = (val) => isNaN(parseInt(val,10)) ? null : parseInt(val,10);

    // Save details 
   if(report.chemical_details)
   {
        let detail = report.chemical_details;
        let record = {
            ifa_incident_id : report.id,
            exposure_routes : detail.exposure_routes.join(','),
            any_odors_reported : getArr(detail.any_odors_reported),
            any_odors_reported_descr : detail.any_odors_reported_descr,
            field_mon_detec_data_collected : getArr(detail.field_mon_detec_data_collected),
            field_mon_detec_data_collected_descr : detail.field_mon_detec_data_collected_descr,
            field_mon_detec_data_collected_by : detail.field_mon_detec_data_collected_by,
            equipment_selected : detail.equipment_selected.join(','),
            equipment_selected_descr : detail.equipment_selected_descr,
            samples_collected : getArr(detail.samples_collected),
            samples_collected_descr : detail.samples_collected_descr,
            samples_collected_by : detail.samples_collected_by,
            samples_collected_type : detail.samples_collected_type.join(','),
            samples_collected_type_descr : detail.samples_collected_type_descr,
            samples_collected_sent : detail.samples_collected_sent,
        }
        if(detail.id)
        {
            await db('ifa_incident_chemical').update(record).where("id", detail.id)
        }
        else
        {
            await db('ifa_incident_chemical').insert(record)
        }
   }

   if(report.concussive_details)
   {
        let detail = report.concussive_details;
        let record = {
            ifa_incident_id : report.id,
            cause : detail.cause.join(','),
            cause_descr : detail.cause_descr,
            cause_est_distance : getInt(detail.cause_est_distance),
            field_mon_detec_data_collected : getArr(detail.field_mon_detec_data_collected),
            field_mon_detec_data_collected_descr : detail.field_mon_detec_data_collected_descr,
            field_mon_detec_data_collected_by : detail.field_mon_detec_data_collected_by,
            
        }
        if(detail.id)
        {
            await db('ifa_incident_concussive').update(record).where("id", detail.id)
        }
        else
        {
            await db('ifa_incident_concussive').insert(record)
        }
   }

   return report.id;

}

/**
 * Helper that runs before all calls to paths that begin /hra/:hraId and 
 * validates that the HRA id being sent is valid. Attaches the hra object
 * to req.hra. 
 * 
 */
router.use('/report/:id', async function (req, res, next) {

    // Validate with the database
    try {
        let row = await db.raw(`
            select 
                ifa_incident.*, 
                (select count(*) 
                    from 
                        ifa_upload_attachment 
                    where 
                        ifa_upload_attachment.ifa_incident_id = ifa_incident.id
                        and attachment_type = '${IFA_ATTACHMENT_TYPE.PHOTOS}'
                ) as images_associated, 
                (select count(*) 
                    from 
                        ifa_associated_personnel 
                    where 
                        ifa_associated_personnel.ifa_incident_id = ifa_incident.id
                ) as personnel_associated, 
                (select count(*) 
                    from 
                        ifa_associated_personnel 
                    where 
                        ifa_associated_personnel.ifa_incident_id = ifa_incident.id
                    and 
                        ifa_associated_personnel.signs_symptoms = "YES"
                ) as symptoms_associated, 
                (select count(*) 
                    from 
                        ifa_incident_chemical 
                        join ifa_incident_concussive using(ifa_incident_id) 
                    where 
                        ifa_incident_chemical.ifa_incident_id = ifa_incident.id 
                        and (
                            ifa_incident_chemical.field_mon_detec_data_collected = "YES" 
                            OR ifa_incident_concussive.field_mon_detec_data_collected = "YES"
                        )
                ) as data_produced 
            from 
                ifa_incident
            where 
                ifa_incident.id = ?
        `,[req.params.id]);
        let report = row[0][0];
        if (!report) {
            throw new createError.NotFound("IFA Report Not found.");
        }
        report = await addPreparers(IfaReport.normalize(row[0][0])).then(addIncidentDetails);
        req.ifa = report;
        return next();
    }
    catch (error) {
        return next(error);
    }
});

// Sending data to Smartabase
router.post("/report/:id/send-to-smartabase", async (req, res, next) => 
{
    try 
    {   
        // Load personnel into the IFA object.
        req.ifa.personnel = await getPersonnel(req.ifa);

        // Call Smartabase API. This will throw if it fails, so the success
        // message will only be sent if this is successful. 
        let importIfa = await SmartabaseApi.eventImportIFA(req.ifa);

        return res.json({
            status : 'Successfully Imported to Smartabase'
        });
       
        
    }
    catch(err) 
    {
        return next(err);
    }
           
});

/**
 * 
 * @api {post} /api/ifa/uploadfile 
 * @apiDescription Upload files (*.pdf, *.jpg, *.xlsx, *.docx) to the database
 * @apiParam {ifa_incident_id} ifa Id URL Parameter. The JHRM ID of the IFA
 * @apiParam {attachment_type} ifa attachment_type
 * @apiGroup IFA
 * @apiSuccessExample Success
 *     HTTP/1.1 201 CREATED
 *     {
 *         status: 'Successfully Uploaded File'
 *     }
 * @apiErrorExample Bad Request
 *     HTTP/1.1 400 Assessment missing required field.
 */
router.post('/report/:id/uploadfile', upload.single('uploadfile'),async function(req, res, next) {
    try {

        if(!IFA_ATTACHMENT_TYPE.isValid(req.body.attachment_type))
        {
            throw new createError.BadRequest("Valid attachment type is required.");
        }

        //If an id and attachment type exist, then delete it
        await db('ifa_upload_attachment')
            .where('ifa_incident_id',req.ifa.id)
            .where('attachment_type', req.body.attachment_type)
            .del();

        //Insert a new record for the specified id
        let id = await db('ifa_upload_attachment').insert({
            'ifa_incident_id' : req.ifa.id,
            'file_name' : req.file.originalname,
            'file_type' : req.file.mimetype,
            'file' : req.file.buffer,
            'attachment_type' : req.body.attachment_type,
            'file_uploaded_date' : db.fn.now(6) 
        });
      
        return res.status(201).json({
            status: 'File Successfully Uploaded',
            file : {
                id : id,
                ifa_incident_id : req.ifa.id,
                file_name : req.file.originalname,
                file_type : req.file.mimetype,
                attachment_type : req.body.attachment_type,
            }

        });
    }catch(err) {
        return next(err);
    }
  });

  /**
 * 
 * @api {get} /api/ifa/downloadfile/:ifa_incident_id 
 * @apiDescription Get the file from the database and display it in the browser
 * @apiParam {ifa_incident_id} ifa Id URL Parameter. The JHRM ID of the IFA
 * @apiParam {file_name} ifa attachment_type
 * @apiGroup IFA
 * @apiSuccessExample Success
 *         Object Display in Browser
 * @apiErrorExample Bad Request
 *     HTTP/1.1 400 Bad Request.
 */
router.get('/report/:id/downloadfile/:file_id', async function(req, res, next) {

    try {      

        var fileobj = await db('ifa_upload_attachment')
        .where('id',req.params.file_id)

        if(!fileobj.length==1)
        {
            throw new createError.NotFound("File not found.");
        }

        fileobj = fileobj[0];

        res.setHeader('Content-Type', fileobj.file_type);
        res.setHeader('Content-Length', fileobj.file.buffer.byteLength);
        res.setHeader('Content-Disposition', `attachment; filename=${fileobj.file_name}`);
        
        return res.end(fileobj.file, 'binary');

    }
    catch(err) {
        return next(err);
    }
});

/**
 * 
 * @api {get} /api/ifa/ifafileattachments/:ifa_incident_id 
 * @apiDescription Get a list of files for the ifa incident from the database
 * @apiParam {:ifa_incident_id} ifa Id URL Parameter. The JHRM ID of the IFA
 * @apiGroup IFA
 * @apiSuccessExample {json} Success
 * 		HTTP/1.1 200 OK
 *		[{
            "ifa_incident_id": 1,
            "file_name": "name_of_file.jpg",
            "attachment_type": "MEDSITREP",
            "file_type": "image/jpeg"
 * 		}]
 * @apiErrorExample Bad Request
 *     HTTP/1.1 400 Assessment missing required field.
 */
router.get('/report/:id/fileattachments', async function(req, res, next) {

    try {

        var query = db('ifa_upload_attachment')
        .select('id')
        .select('file_name')
        .select('file_type')
        .select('attachment_type')
        .select('file_uploaded_date')
        .where('ifa_incident_id',req.ifa.id)

        if(req.query.attachment_type)
        {
            query.where('attachment_type', req.query.attachment_type);
        }

        let files = await query.select();

        return res.json(files);

    }catch(err) {
        return next(err);
    }
});


/**
 * @api {get} /api/ifa/reports List IFA Reports
 * @apiDescription List all IFA Reports 
 * @apiGroup IFA
 * @apiSuccess {Object[]} report[] Array of IFA Reports
 * @apiSuccess {Number} report.id Unique JHRM ID of IFA
 * @apiSuccess {IfaPreparer[]} report.preparers Array of IFA preparers
 * @apiSuccess {IFA_STATUS} report.status One of ()
 * @apiSuccess {IFA_SUBMISSION_STATUS} report.submission_status	Deprecated One of ()
 * @apiSuccess {IFA_QA_STATUS} report.qa_status	One of ()
 * @apiSuccess {Datetime} report.report_start_datetime
   @apiSuccess {Date} report.report_start_datetime.date 
 * @apiSuccess {Datetime} report.report_completion_datetime
   @apiSuccess {Date} report.report_completion_datetime.date:
 * @apiSuccess {String} report.name	
 * @apiSuccess {String} report.short_name	
 * @apiSuccess {Datetime} report.start_datetime	
 * @apiSuccess {Date} report.start_datetime.date	
 * @apiSuccess {Datetime} report.end_datetime
 * @apiSuccess {Date} report.end_datetime.date	
 * @apiSuccess {String} report.ccir_sigact_num	
 * @apiSuccess {IFA_INCIDENT_SCENARIO} report.scenario One of ()
 * @apiSuccess {IFA_INCIDENT_TYPE[]} report.incident_type One of ()
 * @apiSuccess {IFA_INCIDENT_CAUSE} report.cause One of ("UNKNOWN")
 * @apiSuccess {String} report.cause_descr	
 * @apiSuccess {String} report.summary	
 * @apiSuccess {Location} report.location
   @apiSuccess {Number} report.location.lon	
   @apiSuccess {Number} report.location.lat	
   @apiSuccess {String} report.location.mgrs
 * @apiSuccess {Boolean} report.map_image_available	false
 * @apiSuccess {IFA_TRINARY} report.pers_assoc_to_incident	One of "YES","NO,"UNKNOWN"
 * @apiSuccess {String} report.pers_assoc_to_incident_descr
 * @apiSuccess {IFA_TRINARY} report.signs_symptoms_reported One of "YES","NO,"UNKNOWN"
 * @apiSuccess {String} report.signs_symptoms_reported_descr
 * @apiSuccess {String} report.units_involved
 * @apiSuccess {Boolean} report.were_you_at_incident_location
 * @apiSuccessExample {json} Success
 * 		HTTP/1.1 200 OK
 *		[{
 *            "preparers": [
 *                {
 *                    "PortableType": "IfaPreparer",
 *                    "id": 1,
 *                    "ifa_incident_id": 1,
 *                    "type": "INITIAL",
 *                    "name": "Kyrstin Stinson",
 *                    "email": "kstinson0@princeton.edu",
 *                    "phone_num": "716-657-7582",
 *                    "unit": "in hac habitasse"
 *                }
 *            ],
 *            "status": "INCOMPLETE",
 *            "submission_status": "SUBMITTED",
 *            "qa_status": "READY_FOR_QA",
 *            "report_start_datetime": {
 *                "PortableType": "Datetime",
 *                "date": "2019-09-28T14:02:44.000Z"
 *            },
 *            "id": 1,
 *            "report_completion_datetime": {
 *                "PortableType": "Datetime",
 *                "date": "2020-09-02T09:29:36.400Z"
 *            },
 *            "name": "quam a odio in",
 *            "short_name": "nisi eu orci",
 *            "start_datetime": {
 *                "PortableType": "Datetime",
 *                "date": "2020-02-18T19:15:42.000Z"
 *            },
 *            "end_datetime": {
 *                "PortableType": "Datetime",
 *                "date": "2019-10-05T15:27:34.000Z"
 *            },
 *            "ccir_sigact_num": null,
 *            "scenario": "TRAINING",
 *            "incident_type": [
 *                "CONCUSSIVE"
 *            ],
 *            "cause": "UNKNOWN",
 *            "cause_descr": null,
 *            "summary": "Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.",
 *            "location": {
 *                "PortableType": "Location",
 *                "lon": 13.0299,
 *                "lat": 59.2669,
 *                "mgrs": "33VUF8770171432"
 *            },
 *            "map_image_available": false,
 *            "pers_assoc_to_incident": "UNKNOWN",
 *            "pers_assoc_to_incident_descr": null,
 *            "signs_symptoms_reported": "NO",
 *            "signs_symptoms_reported_descr": null,
 *            "units_involved": "metus",
 *            "were_you_at_incident_location": 0,
 *            "PortableType": "IfaReport"
 * 		}]
 * @apiErrorExample {none} Internal Error Error
 *    Undefined location cannot read property
 */
router.get('/reports', async function(req, res, next){
	
	try
	{	
        let reports = await db.raw(`
            select 
                ifa_incident.*, 
                (select count(*) 
                    from 
                        ifa_upload_attachment 
                    where 
                        ifa_upload_attachment.ifa_incident_id = ifa_incident.id
                        and attachment_type = '${IFA_ATTACHMENT_TYPE.PHOTOS}'
                ) as images_associated, 
                (select count(*) 
                    from 
                        ifa_associated_personnel 
                    where 
                        ifa_associated_personnel.ifa_incident_id = ifa_incident.id
                ) as personnel_associated, 
                (select count(*) 
                    from 
                        ifa_incident_chemical 
                        join ifa_incident_concussive using(ifa_incident_id) 
                    where 
                        ifa_incident_chemical.ifa_incident_id = ifa_incident.id 
                        and (
                        ifa_incident_chemical.field_mon_detec_data_collected = "YES" 
                        OR ifa_incident_concussive.field_mon_detec_data_collected = "YES"
                        )
                ) as data_produced 
            from 
                ifa_incident     
        `)

        // Return the response, even if it is empty
        res.json( await Promise.all(reports[0].map(report=>addPreparers(IfaReport.normalize(report)))));

    }
    catch (err) {
        return next(err);
    }
});

/**
 * @api {post} /api/ifa/report Create IFA
 * @apiDescription Save a new IFA report 
 * @apiGroup IFA
 * @apiSuccess {Object[]} report[] Array of IFA Reports
 * @apiSuccess {Number} report.id
 * @apiSuccessExample {json} Success
 * 		HTTP/1.1 200 OK
 * 		[{

 * 		}]
 * @apiErrorExample {none} Internal Error Error
 *    Undefined location cannot read property
 */
router.post('/report', async function(req, res, next){
	
	try
	{	
        let user = users.find(user=>user.username==req.cookies.user_name);
        if(!req.body.name || !req.body.summary)
        {
            return next(createError(403, "Missing required parameters."));
        }

        // Create a new report and save it. That way, we get all the default
        // field values. 
        let ifa = new IfaReport();
        ifa.short_name = req.body.name; 
        ifa.summary = req.body.summary;
        ifa.start_datetime = req.body.start_datetime;
        let id = await saveIfa(ifa, user);
        return res.status(201).json({
            status: 'Create Record Success',
            id: id
        });
    } 
    catch (err) 
    {
        return next(err);
    }
});

/**
 * @api {get} /api/ifa/reports Get IFA Report
 * @apiDescription Get a single IFA report by ID
 * @apiGroup IFA
 * @apiSuccess {Object} report[] IFA Report
 * @apiSuccess {Number} report.id Unique JHRM ID of IFA
 * @apiSuccess {IfaPreparer[]} report.preparers Array of IFA preparers
 * @apiSuccess {IFA_STATUS} report.status One of ()
 * @apiSuccess {IFA_SUBMISSION_STATUS} report.submission_status	Deprecated One of ()
 * @apiSuccess {IFA_QA_STATUS} report.qa_status	One of ()
 * @apiSuccess {Datetime} report.report_start_datetime
   @apiSuccess {Date} report.report_start_datetime.date 
 * @apiSuccess {Datetime} report.report_completion_datetime
   @apiSuccess {Date} report.report_completion_datetime.date:
 * @apiSuccess {String} report.name	
 * @apiSuccess {String} report.short_name	
 * @apiSuccess {Datetime} report.start_datetime	
 * @apiSuccess {Date} report.start_datetime.date	
 * @apiSuccess {Datetime} report.end_datetime
 * @apiSuccess {Date} report.end_datetime.date	
 * @apiSuccess {String} report.ccir_sigact_num	
 * @apiSuccess {IFA_INCIDENT_SCENARIO} report.scenario One of ()
 * @apiSuccess {IFA_INCIDENT_TYPE[]} report.incident_type One of ()
 * @apiSuccess {IFA_INCIDENT_CAUSE} report.cause One of ("UNKNOWN")
 * @apiSuccess {String} report.cause_descr	
 * @apiSuccess {String} report.summary	
 * @apiSuccess {Location} report.location
   @apiSuccess {Number} report.location.lon	
   @apiSuccess {Number} report.location.lat	
   @apiSuccess {String} report.location.mgrs
 * @apiSuccess {Boolean} report.map_image_available	false
 * @apiSuccess {IFA_TRINARY} report.pers_assoc_to_incident	One of "YES","NO,"UNKNOWN"
 * @apiSuccess {String} report.pers_assoc_to_incident_descr
 * @apiSuccess {IFA_TRINARY} report.signs_symptoms_reported One of "YES","NO,"UNKNOWN"
 * @apiSuccess {String} report.signs_symptoms_reported_descr
 * @apiSuccess {String} report.units_involved
 * @apiSuccess {Boolean} report.were_you_at_incident_location
 * @apiSuccessExample {json} Success
 * 		HTTP/1.1 200 OK
 *		{
 *            "preparers": [
 *                {
 *                    "PortableType": "IfaPreparer",
 *                    "id": 1,
 *                    "ifa_incident_id": 1,
 *                    "type": "INITIAL",
 *                    "name": "Kyrstin Stinson",
 *                    "email": "kstinson0@princeton.edu",
 *                    "phone_num": "716-657-7582",
 *                    "unit": "in hac habitasse"
 *                }
 *            ],
 *            "status": "INCOMPLETE",
 *            "submission_status": "SUBMITTED",
 *            "qa_status": "READY_FOR_QA",
 *            "report_start_datetime": {
 *                "PortableType": "Datetime",
 *                "date": "2019-09-28T14:02:44.000Z"
 *            },
 *            "id": 1,
 *            "report_completion_datetime": {
 *                "PortableType": "Datetime",
 *                "date": "2020-09-02T09:29:36.400Z"
 *            },
 *            "name": "quam a odio in",
 *            "short_name": "nisi eu orci",
 *            "start_datetime": {
 *                "PortableType": "Datetime",
 *                "date": "2020-02-18T19:15:42.000Z"
 *            },
 *            "end_datetime": {
 *                "PortableType": "Datetime",
 *                "date": "2019-10-05T15:27:34.000Z"
 *            },
 *            "ccir_sigact_num": null,
 *            "scenario": "TRAINING",
 *            "incident_type": [
 *                "CONCUSSIVE"
 *            ],
 *            "cause": "UNKNOWN",
 *            "cause_descr": null,
 *            "summary": "Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.",
 *            "location": {
 *                "PortableType": "Location",
 *                "lon": 13.0299,
 *                "lat": 59.2669,
 *                "mgrs": "33VUF8770171432"
 *            },
 *            "map_image_available": false,
 *            "pers_assoc_to_incident": "UNKNOWN",
 *            "pers_assoc_to_incident_descr": null,
 *            "signs_symptoms_reported": "NO",
 *            "signs_symptoms_reported_descr": null,
 *            "units_involved": "metus",
 *            "were_you_at_incident_location": 0,
 *            "PortableType": "IfaReport"
 * 		}
 * @apiErrorExample {none} Internal Error Error
 *    Undefined location cannot read property
 */
router.get('/report/:id', async function(req, res, next){
	
	try
	{	
        // In order to get here, we know that the IFA has already been loaded
        res.json(req.ifa);
    }
    catch (err) {
        return next(err);
        
    }
});



/**
 * @api {post} /api/ifa/report/:id Save IFA Report
 * @apiDescription Save an IFA that already exists in the system. 
 * @apiGroup IFA
 * @apiSuccess {Object[]} report[] IFA Report
 * @apiSuccess {Number} report.id
 * @apiSuccessExample {json} Success
 * 		HTTP/1.1 200 OK
 * 		[{

 * 		}]
 * @apiErrorExample {none} Internal Error Error
 *    Undefined location cannot read property
 */
router.post('/report/:id', async function(req, res, next){
	try
	{	
        let user = users.find(user=>user.username==req.cookies.user_name);
        await saveIfa(req.body, user);
        return res.status(201).json({
            status: 'Update Record Success',
            id: req.ifa.id
        });
    }
    catch (err) {
        return next(err);
    }
});

/**
 * @api {post} /api/ifa/reports Save IFA associated Personnel
 * @apiDescription Save associated personnel for an IFA report
 * @TODO Should this be included in the main report object? 
 * @apiGroup IFA
 * @apiSuccess {Object[]} report[] IFA Report
 * @apiSuccess {Number} report.id
 * @apiSuccessExample {json} Success
 * 		HTTP/1.1 200 OK
 * 		[{

 * 		}]
 * @apiErrorExample {none} Internal Error Error
 *    Undefined location cannot read property
 */
router.post('/report/:id/personnel', async function(req, res, next){
	try
	{	
        // Require an array argument, even if it is empty. But if it is not 
        // empty, all members have to be IfaPersonnel objects. 
        if(!Array.isArray(req.body) || req.body.find(item=>!(item instanceof IfaPersonnel)))
        {
            throw new createError.BadRequest("Invalid Arguments");
        }

        // Use a transaction
        await db.transaction(async trx => {

            // Delete removed associations 
            await trx('ifa_associated_personnel')
                .where('ifa_incident_id', req.ifa.id)
                .whereNotIn('id', req.body.filter(item=>item.id!==undefined).map(item=>item.id))
                .del();

            // For each personnel record, try to find a matching value in the 
            // personnel table and insert or update that record as required
            for(let i = 0 ; i < req.body.length ; i++)
            {
                let person = req.body[i];
                let person_record = {
                    name : person.name,
                    ssn : person.ssn,
                    dodid : person.dodid,
                    unit : person.unit,
                    isfn : person.isfn,
                };
                let association_record = {
                    id : person.id,
                    ifa_incident_id : req.ifa.id,
                    no_acute_symptoms : person.no_acute_symptoms,
                    signs_symptoms :  person.signs_symptoms,
                    ppe_worn :  person.ppe_worn,
                    detector_sensor_data :  person.detector_sensor_data
                };

                // See if an existing user with the same DODID exists, and if 
                // so, get the ID of that record. 
                let existing = await trx('ifa_personnel')
                    .where('dodid', person.dodid);

                if(existing.length == 1)
                {
                    person_record.id = existing[0].id;
                    await trx('ifa_personnel')
                        .where('id', person_record.id)
                        .update(person_record)
                }
                else 
                {
                    let id = await trx('ifa_personnel').insert(person_record);
                    person_record.id = id[0];
                }

                // Create or update the associations
                association_record.ifa_personnel_id = person_record.id;

                if(association_record.id!==undefined)
                {
                    await trx('ifa_associated_personnel')
                        .update(association_record)
                        .where('id', association_record.id);
                }
                else 
                {
                    delete association_record.id;
                    let id = await trx('ifa_associated_personnel')
                        .insert(association_record);
                    association_record.id = id[0];
                }

                // Save Signs/Symptoms
                if(person.concussive_signs_symptoms)
                {
                    let concussive_signs_symptoms_data_id = await db('ifa_concussive_ied_symptom_checklist')
                        .select('id')
                        .where('ifa_associated_personnel_id', association_record.id); //person_record.id);

                    //if there is a record already, we just update
                    if(concussive_signs_symptoms_data_id.length == 1 )
                    {
                        await trx('ifa_concussive_ied_symptom_checklist')
                        .update({
                            
                            //'ifa_associated_personnel_id' : concussive_signs_symptoms_data_id[0].id,
                            'physical_damage_to_body' : person.concussive_signs_symptoms.physical_damage_to_body,
                            'headaches_and_or_vomiting' : person.concussive_signs_symptoms.headaches_and_or_vomiting,
                            'ear_ringing' : person.concussive_signs_symptoms.ear_ringing,
                            'amnesia_loss_of_consiousness' : person.concussive_signs_symptoms.amnesia_loss_of_consiousness,
                            'double_vision_dizziness' : person.concussive_signs_symptoms.double_vision_dizziness,
                            'something_feels_wrong' : person.concussive_signs_symptoms.something_feels_wrong,
                            'within_50_meters' : person.concussive_signs_symptoms.within_50_meters,
                            'wearing_blast_gauges' : person.concussive_signs_symptoms.wearing_blast_gauges,
                            'max_peak_overpressure' : person.concussive_signs_symptoms.max_peak_overpressure

                        })
                        .where('id', concussive_signs_symptoms_data_id[0].id)
                    }
                    else  //No existing record, insert new
                    {
                        await trx('ifa_concussive_ied_symptom_checklist')
                        .insert({
                            
                            'ifa_associated_personnel_id' : association_record.id, //association_record.id[0],
                            'physical_damage_to_body' : person.concussive_signs_symptoms.physical_damage_to_body,
                            'headaches_and_or_vomiting' : person.concussive_signs_symptoms.headaches_and_or_vomiting,
                            'ear_ringing' : person.concussive_signs_symptoms.ear_ringing,
                            'amnesia_loss_of_consiousness' : person.concussive_signs_symptoms.amnesia_loss_of_consiousness,
                            'double_vision_dizziness' : person.concussive_signs_symptoms.double_vision_dizziness,
                            'something_feels_wrong' : person.concussive_signs_symptoms.something_feels_wrong,
                            'within_50_meters' : person.concussive_signs_symptoms.within_50_meters,
                            'wearing_blast_gauges' : person.concussive_signs_symptoms.wearing_blast_gauges,
                            'max_peak_overpressure' : person.concussive_signs_symptoms.max_peak_overpressure

                        })                                     
                    }
                } 
                else 
                {
                    await trx('ifa_concussive_ied_symptom_checklist')
                    .where('ifa_associated_personnel_id', association_record.id)
                    .del();
                }
                //save chemical signs and symptoms
                if(person.chemical_signs_symptoms)
                {
                    let chemical_signs_symptoms_id = await trx('ifa_chemical_signs_or_symptoms')
                        .select('id')
                        .where('ifa_associated_personnel_id', association_record.id);

                    //if there is a record already, we just update
                    if(chemical_signs_symptoms_id.length == 1 )
                    {
                        await trx('ifa_chemical_signs_or_symptoms')
                        .update({
                            'eyes_irritation_burning' : person.chemical_signs_symptoms.eyes_irritation_burning,
                            'eyes_pin_pointed_pupils' : person.chemical_signs_symptoms.eyes_pin_pointed_pupils,
                            'resp_irritation_burning' : person.chemical_signs_symptoms.resp_irritation_burning,
                            'resp_coughing' : person.chemical_signs_symptoms.resp_coughing,
                            'resp_trouble_breathing' : person.chemical_signs_symptoms.resp_trouble_breathing,
                            'gastro_nausea_vomiting' : person.chemical_signs_symptoms.gastro_nausea_vomiting,
                            'neuro_dizziness' : person.chemical_signs_symptoms.neuro_dizziness,
                            'neuro_seizures' : person.chemical_signs_symptoms.neuro_seizures,
                            'skin_irritation_burning' : person.chemical_signs_symptoms.skin_irritation_burning,
                            'skin_blistering' : person.chemical_signs_symptoms.skin_blistering
                        })
                        .where('id', chemical_signs_symptoms_id[0].id)
                    }
                    else //if there is no record, we insert a new one
                    {
                        await trx('ifa_chemical_signs_or_symptoms')
                        .insert({
                            'ifa_associated_personnel_id' : association_record.id,
                            'eyes_irritation_burning' : person.chemical_signs_symptoms.eyes_irritation_burning,
                            'eyes_pin_pointed_pupils' : person.chemical_signs_symptoms.eyes_pin_pointed_pupils,
                            'resp_irritation_burning' : person.chemical_signs_symptoms.resp_irritation_burning,
                            'resp_coughing' : person.chemical_signs_symptoms.resp_coughing,
                            'resp_trouble_breathing' : person.chemical_signs_symptoms.resp_trouble_breathing,
                            'gastro_nausea_vomiting' : person.chemical_signs_symptoms.gastro_nausea_vomiting,
                            'neuro_dizziness' : person.chemical_signs_symptoms.neuro_dizziness,
                            'neuro_seizures' : person.chemical_signs_symptoms.neuro_seizures,
                            'skin_irritation_burning' : person.chemical_signs_symptoms.skin_irritation_burning,
                            'skin_blistering' : person.chemical_signs_symptoms.skin_blistering
                            })
                    }
                }
                else
                {
                    await trx('ifa_chemical_signs_or_symptoms')
                    .where('ifa_associated_personnel_id', association_record.id)
                    .del();
                }

                // Save detector sensor data
                await trx('ifa_concussive_detector_sensor_data')
                    .where('ifa_associated_personnel_id', association_record.id)
                    .del();
                if(person.concussive_detector_sensor_data && person.concussive_detector_sensor_data.length)
                {
                    await trx('ifa_concussive_detector_sensor_data')
                    .insert(person.concussive_detector_sensor_data.map(item=>{
                        item.ifa_associated_personnel_id = association_record.id;
                        item.datetime = new Date(item.datetime);
                        return item;
                    }))
                }
                await trx('ifa_chemical_detector_sensor_data')
                    .where('ifa_associated_personnel_id', association_record.id)
                    .del();
                if(person.chemical_detector_sensor_data && person.chemical_detector_sensor_data.length)
                {
                    await trx('ifa_chemical_detector_sensor_data')
                    .insert(person.chemical_detector_sensor_data.map(item=>{
                        return {
                            ifa_associated_personnel_id : association_record.id,
                            chem_rapid_hra_id : item
                        }
                    }))
                }
                    
                // Save ppe worn data (both chemical and concussive)
                for(const type of ['concussive_ppe_worn', 'chemical_ppe_worn'])
                {
                    let table = `ifa_${type}`;
                    if(person[type])
                    {
                        let record_id = await trx(table)
                        .select('id')
                        .where('ifa_associated_personnel_id', association_record.id);

                        person[type].ifa_associated_personnel_id = association_record.id;

                        //if there is a record already, we just update
                        if(record_id.length == 1 )
                        {
                            await trx(table)
                            .update(person[type])
                            .where('id', record_id[0].id)
                        }
                        else  //if there is no record, we insert a new record
                        {
                            await trx(table)
                            .insert(person[type])                                     
                        }
                    }
                    else 
                    {
                        await trx(table)
                        .where('ifa_associated_personnel_id', association_record.id)
                        .del();
                    }
                }

                // Medical Information
                if(person.concussive_medical_information)
                {
                    let concussive_medical_information_data_id = await db('ifa_concussive_medical')
                        .select('id')
                        .where('ifa_associated_personnel_id', association_record.id);

                    //if there is a record already, we just update
                    if(concussive_medical_information_data_id.length == 1 )
                    {
                        await trx('ifa_concussive_medical')
                        .update({
                            'pers_decontamination_performed' : person.concussive_medical_information.pers_decontamination_performed,
                            'decontamination_descr' : person.concussive_medical_information.decontamination_descr,
                            'sought_medical_treatment' : person.concussive_medical_information.sought_medical_treatment,
                            'unitmedic_care_received' : person.concussive_medical_information.unitmedic_care_received,
                            'unitmedic_care_received_descr' : person.concussive_medical_information.unitmedic_care_received_descr,
                            'battalionaidestation_care_received' : person.concussive_medical_information.battalionaidestation_care_received,
                            'battalionaidestation_care_received_descr' : person.concussive_medical_information.battalionaidestation_care_received_descr,
                            'mtfcs_care_received' : person.concussive_medical_information.mtfcs_care_received,
                            'mtfcs_care_received_descr' : person.concussive_medical_information.mtfcs_care_received_descr,
                            'resulting_duty_status' : person.concussive_medical_information.resulting_duty_status,
                            'comments' : person.concussive_medical_information.comments
                        })
                        .where('id', concussive_medical_information_data_id[0].id)
                    }
                    else  //No existing record, insert new
                    {
                        await trx('ifa_concussive_medical')
                        .insert({
                            
                            'ifa_associated_personnel_id' : association_record.id, 
                            'pers_decontamination_performed' : person.concussive_medical_information.pers_decontamination_performed,
                            'decontamination_descr' : person.concussive_medical_information.decontamination_descr,
                            'sought_medical_treatment' : person.concussive_medical_information.sought_medical_treatment,
                            'unitmedic_care_received' : person.concussive_medical_information.unitmedic_care_received,
                            'unitmedic_care_received_descr' : person.concussive_medical_information.unitmedic_care_received_descr,
                            'battalionaidestation_care_received' : person.concussive_medical_information.battalionaidestation_care_received,
                            'battalionaidestation_care_received_descr' : person.concussive_medical_information.battalionaidestation_care_received_descr,
                            'mtfcs_care_received' : person.concussive_medical_information.mtfcs_care_received,
                            'mtfcs_care_received_descr' : person.concussive_medical_information.mtfcs_care_received_descr,
                            'resulting_duty_status' : person.concussive_medical_information.resulting_duty_status,
                            'comments' : person.concussive_medical_information.comments

                        })                                     
                    }
                } 
                else 
                {
                    await trx('ifa_concussive_medical')
                    .where('ifa_associated_personnel_id', association_record.id)
                    .del();
                }
            };

            return res.json({
                status : 'Success'
            });

        });
        
    }
    catch (err) {
        return next(err);
    }
});


/**
 * @api {get}  /report/:hraId/localdrihraslist Get the list of DRI HRAs IDs from the ifa_chemical_detector_sensor_data
 * @apiDescription List all samples
 * @apiParam {Number} :hraId URL Parameter. The JHRM ID of the HRA
 * @apiGroup IFA DRI HRA
 * @apiSuccess {Object} localreferencedrihras[] Array of samples
 * @apiSuccess {String} localreferencedrihras.ifa_associated_personnel_id
 * @apiSuccess {String} localreferencedrihras.chem_rapid_hra_id
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *      [{
 *           "ifa_associated_personnel_id": 1,
 *           "chem_rapid_hra_id": "R-69QC7LSH5I"
 *       },
 *       {
 *           "ifa_associated_personnel_id": 1,
 *           "chem_rapid_hra_id": "R-ZGNHWT5SJI"
 *      }]  
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 * @apiErrorExample  HRA not found
 *     HTTP/1.1 404 HRA not found
 */
router.get('/report/:hraId/localdrihraslist', async function(req, res, next){
	try
	{
        //
        let chemical = await db.distinct('chem_rapid_hra_id') 
        .from('ifa_chemical_detector_sensor_data') 
        .join('ifa_associated_personnel', 'ifa_chemical_detector_sensor_data.ifa_associated_personnel_id', 'ifa_associated_personnel.ifa_personnel_id')
 .where('ifa_incident_id', req.ifa.id);

        // Return local referenced list of DRI HRAs IDs 
        return res.json(chemical);

    }
    catch (err) {
        return next(err);
    }
});


/**
 * @api {get}  /report/:id/drihrasids Get the list of HRA DRIs from the location specified by the IFA incident information location
 * @apiDescription List all samples
 * @apiParam {Number} :ifaId URL Parameter. The JHRM ID of the IFA
 * @apiGroup IFA DRI HRA
 * @apiSuccess {Object} driHRAsIds[] Array of samples
 * @apiSuccess {String} driHRAsIds.chem_rapid_hra_id
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *      [{
 *          "chem_rapid_hra_id": "R-69QC7LSH5I"
 *      },
 *      {
 *          "chem_rapid_hra_id": "R-ZGNHWT5SJI"
 *      }]
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 * @apiErrorExample  HRA not found
 *     HTTP/1.1 404 HRA not found
 */
router.get('/report/:ifaId/drihrasids', async function(req, res, next){
	try
	{        
        //Get the list of HRA DRIs from the location specified by the IFA incident information location
        drihras = await db.raw(`
            SELECT DISTINCT chem_rapid_hra_id 
            From rhra_sample
            JOIN chem_rapid_hra ON rhra_sample.chem_rapid_hra_id = chem_rapid_hra.id
            JOIN ifa_incident ON SUBSTRING(ifa_incident.mgrs,1,8) = SUBSTRING(rhra_sample.mgrs,1,8) 
            WHERE ifa_incident.id = ? 
            AND chem_rapid_hra.status = 'COMPLETE'
        `,[req.ifa.id]);
        

        // Return the list of DRI HRAs
        return res.json(drihras[0]);

    }
    catch (err) {
        return next(err);
    }
});

/**
 * @api {get} /api/ifa/reports Fetch IFA associated Personnel
 * @apiDescription Get associated personnel for an IFA report 
 * @TODO Should this be included in the main report object? 
 * @apiGroup IFA
 * @apiSuccess {Object[]} report[] IFA Report
 * @apiSuccess {Number} report.id
 * @apiSuccessExample {json} Success
 * 		HTTP/1.1 200 OK
 * 		[{

 * 		}]
 * @apiErrorExample {none} Internal Error Error
 *    Undefined location cannot read property
 */
router.get('/report/:id/personnel', async function(req, res, next){
	try
	{	
        return res.json(await getPersonnel(req.ifa));
    }
    catch (err) {
        return next(err);
    }
});

async function getPersonnel(ifa)
{
    let personnel = await db('ifa_associated_personnel')
    .join('ifa_personnel', 'ifa_associated_personnel.ifa_personnel_id', 'ifa_personnel.id')
    .select({
        id : 'ifa_associated_personnel.id',
        ifa_incident_id : 'ifa_associated_personnel.ifa_incident_id',
        ifa_personnel_id : 'ifa_associated_personnel.ifa_personnel_id',
        no_acute_symptoms : 'ifa_associated_personnel.no_acute_symptoms',
        signs_symptoms : 'ifa_associated_personnel.signs_symptoms',
        ppe_worn : 'ifa_associated_personnel.ppe_worn',
        detector_sensor_data : 'ifa_associated_personnel.detector_sensor_data',
        name : 'ifa_personnel.name',
        ssn : 'ifa_personnel.ssn',
        dodid : 'ifa_personnel.dodid',
        unit : 'ifa_personnel.unit',
        isfn : 'ifa_personnel.isfn'
    })
    .where('ifa_incident_id', ifa.id);
    personnel = personnel.map(item=>{ 
        return IfaPersonnel.normalize(item)
    });

    // Loop through each personnel record. 
    for(let i = 0; i < personnel.length; i++)
    {
        let current = personnel[i];

        // If the record indicates that there is attached detector sensor
        // data, then make a separate query to fetch it. This should be 
        // repeated for each type of data that needs to be attached, such 
        // as PPE worn
        if(current.signs_symptoms == IFA_TRINARY.YES)
        {
            let concussive = await db('ifa_concussive_ied_symptom_checklist')
                .where('ifa_associated_personnel_id', personnel[i].id);
            current.concussive_signs_symptoms = concussive[0];

            let chemical = await db('ifa_chemical_signs_or_symptoms')
                .where('ifa_associated_personnel_id', personnel[i].id);
            current.chemical_signs_symptoms = chemical[0];
        } else
		{   switch(current.signs_symptoms)
            {
                case IFA_TRINARY.NO:
                    current.chemical_signs_symptoms = "No";
			    case IFA_TRINARY.UNKNOWN:
                    current.chemical_signs_symptoms = "Unknown";
            }
			
		}
        if(current.detector_sensor_data == IFA_TRINARY.YES)
        {
            let concussive = await db('ifa_concussive_detector_sensor_data')
                .where('ifa_associated_personnel_id', personnel[i].id);
            current.concussive_detector_sensor_data = concussive.map(item=>{
                item.datetime = new Datetime(item.datetime);
                return item;
            });
            let chemical = await db('ifa_chemical_detector_sensor_data')
            .where('ifa_associated_personnel_id', personnel[i].id);
            current.chemical_detector_sensor_data = chemical.map(item=>{
                return item.chem_rapid_hra_id;
            });
        }
        if(current.ppe_worn == IFA_TRINARY.YES)
        {
            let concussive = await db('ifa_concussive_ppe_worn')
                .where('ifa_associated_personnel_id', personnel[i].id);
            current.concussive_ppe_worn = concussive[0];
            let chemical = await db('ifa_chemical_ppe_worn')
                .where('ifa_associated_personnel_id', personnel[i].id);
            current.chemical_ppe_worn = chemical[0];
        }

        let data = await db('ifa_concussive_medical')
            .where('ifa_associated_personnel_id', personnel[i].id);
        current.concussive_medical_information = data[0];
    }

    return personnel;
}


router.post('/upload/', async function(req, res, next){
	
	try
	{
        //var FileName = file;
        // In order to get here, we know that the IFA has already been loaded
        //res.json(req.ifa);
        return res.status(201).json({
            status: 'File Uploaded Success'
        });

    }
    catch (err) {
        return next(err);
        
    }
});


/**
 * Helper that standardizes error output to be an JSON object, and sets the
 * correct headers. Example body content: 
 *  
 * {
 *   "status": "Error",
 *   "statusCode": 404,
 *   "message": "HRA Not found."
 * }
 * 
 * Error output is santitized to avoid exposing implementation details unless 
 * running in debug mode. 
 * 
 * In order to make certain that this handler is called, especially if your 
 * method is asynchronous, make sure to call the next() function provided 
 * by express with the error as the first argument. 
 * 
 */
router.use(function (err, req, res, next) {
    if (!err.statusCode) {
        err = createError(500, err);
    }
    res.status(err.statusCode).json({
        status: 'Error',
        statusCode: err.statusCode,
        message: err.expose || config.get('debug') ? err.message : "Internal Server Error."
    });
});

module.exports = router