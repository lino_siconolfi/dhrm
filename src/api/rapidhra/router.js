const esmImport = require('esm')(module);
const express = require('express');
const router = express.Router();
const db = require('src/util/db');
const uniqueid = require('src/util/uniqueid');
const hrastatus = require('src/util/hrastatus.js');
const pdfmake = require('pdfmake');
const createError = require('http-errors');
const config = require('config');
const { DimensionedValue } = esmImport("../../shared/DimensionedValue.js");
const calculatePepc = esmImport("../../shared/calculatePepc.js");
const { normalizeMegs } = esmImport("../../shared/normalizeMegs.js");
const { Unit } = esmImport("../../shared/Unit.js");
const { DriHra } = esmImport("../../shared/DriHra.js");
const { Location } = esmImport("../../shared/location.js");
const { Datetime } = esmImport("../../shared/datetime.js");
const multer = require('multer');
var storage = multer.memoryStorage();
var upload = multer({
    limits : {
        fileSize: config.get('maxFileUploadSize')
    },
    storage : storage
});

/**
 * Helper that runs before all calls to paths that begin /hra/:hraId and
 * validates that the HRA id being sent is valid. Attaches the hra object
 * to req.hra for use by the request handler.
 *
 *  @param {object} req Express Request object
 *  @param {object} res Express Response object
 *  @param {function} next Callback function
 */
router.use('/hra/:hraId', async function (req, res, next) {

    // Validate the HRA ID with the database
    try {
        let hra = await db('chem_rapid_hra')
            .where("id", req.params.hraId);
        if (!hra.length) {
            throw new createError.NotFound("HRA Not found.");
        }

        req.hra = hra[0];
        return next();
    }
    catch (error) {
        return next(error);
    }
});

/**
 * Helper function to update an existing HRA and make sure that the last
 * modified date and user are correctly set.
 *
 * This should be called when any modification is done to an HRA or any of the
 * supporting HRA data tables.
 *
 * @param {string} id HRA ID
 * @param {object} additional Hash of additional fields to update
 */
async function updateHra(id, req, additional)
{
    let fields = additional || {};
    fields.last_edited_by = req.cookies.user_name;
    fields.last_edited_on = db.fn.now(6);
    return db('chem_rapid_hra').update(fields).where('id', id);
}

/**
 * Helper function to query for new readings.
 * @param {string} sensorType
 * @param {string} hraId
 */
async function querySamples(hraId)
{
    var samples = await db.raw(`
        SELECT
        max(r.sensor_sn) as sensor_sn,
        max(r.sensor_id) as sensor_id,
        max(r.latitude) as latitude,
        max(r.longitude) as longitude,
        max(r.mgrs) as mgrs,
        min(r.begin) as begin,
        max(r.end) as end,
        max(r.cas_number) as cas_number,
        max(r.chemical_name) as chemical_name,
        ROUND(max(r.value), 3 - 1 - FLOOR(LOG10(ABS(max(r.value))))) as peak,
        ROUND(avg(r.value), 3 - 1 - FLOOR(LOG10(ABS(avg(r.value))))) as average,
        max(r.units) as units,
        SUBSTRING(md5(CONCAT(date(max(r.begin)), max(r.unique_by), max(r.cas_number), max(r.sensor_type))),1,6) as sample_id,
        max(m.sensor_location_description) as sensor_location_description,
        max(m.sensor_metadata_id) as sensor_metadata_id,
        max(m.sensor_mgrs_location) as sensor_mgrs_location,
        max(m.sensor_employment_type) as sensor_employment_type,
        max(s.selected) as selected,
        max(r.sensor_type) as sensor_type
        FROM
            (
                (
                    # MultiRAE Query
                    SELECT
                        ms.sensor_sn,
                        ms.id as sensor_id,
                        md.sensing_component_id as unique_by,
                        md.latitude,
                        md.longitude,
                        md.mgrs,
                        md.start_time as begin,
                        md.end_time as end,
                        md.cas_number,
                        md.chemical_name,
                        md.chem_measure_value as value,
                        md.chem_measure_units as units,
                        'multirae' as sensor_type
                    FROM multirae_sensing_data md
                    LEFT JOIN multirae_sensing_component mc
                        ON mc.id = md.sensing_component_id
                    LEFT JOIN multirae_sensors ms
                        ON ms.id = mc.sensor_id
                )
                UNION
                (
                    # JCAD Query
                    SELECT
                        js.serial_number as sensor_sn,
                        jcr.jcad_sensor_id as sensor_id,
                        jcr.jcad_sensor_id as unique_by,
                        jcr.latitude,
                        jcr.longitude,
                        jcr.mgrs,
                        jcr.time_received as begin,
                        jcr.time_received as end,
                        jcr.cas_number,
                        jcr.material_class as chemical_name,
                        jcr.concentration as value,
                        jcr.units,
                        'jcad' as sensor_type
                    FROM jcad_chemical_readings jcr
                    LEFT JOIN jcad_sensor_status jst
                        ON jst.id = jcr.jcad_sensor_id
                    LEFT JOIN jcad_sensors js
                        ON js.id = jst.jcad_sensor_id
                )
                UNION
                (
                    # HapSite Query
                    SELECT
                        hs.serial_number as sensor_sn,
                        hss.hapsite_sensor_id as sensor_id,
                        hr.hapsite_sample_id as unique_by,
                        hss.latitude,
                        hss.longitude,
                        hss.mgrs,
                        hr.start_time as begin,
                        hr.end_time as end,
                        hr.cas_number,
                        hr.parameter as chemical_name,
                        hr.concentration as value,
                        hr.units,
                        'hapsite' as sensor_type
                    FROM hapsite_result hr
                    LEFT JOIN hapsite_sample hss
                        ON hss.id = hr.hapsite_sample_id
                    LEFT JOIN hapsite_sensors hs
                        ON hs.id = hss.hapsite_sensor_id
                )
            ) r

            LEFT JOIN chem_rapid_hra cra
                ON cra.id = ?

            LEFT JOIN rhra_pathway rp
                ON rp.chem_rapid_hra_id = cra.id

            LEFT JOIN meta_sensor_profile m
                ON m.serial_number = r.sensor_sn

            LEFT JOIN rhra_sample s
                ON s.unique_sample_id = SUBSTRING(md5(CONCAT(date(r.begin), r.unique_by, r.cas_number, r.sensor_type)),1,6)
                AND s.chem_rapid_hra_id  = cra.id

            #If a DRI is created with an existing exposure pathway, change the value of 0 to 1.
            #Note, this will only work outside when running this script outside the application.
            WHERE
                IF(cra.selected_pathway = 1 AND rp.sensor_array_id IS NOT NULL, m.sensor_metadata_id = rp.sensor_array_id,1)
                AND r.cas_number != 'none'

            GROUP BY
                date(r.begin),
                r.unique_by,
                r.cas_number,
                r.sensor_type
        `, [hraId]);

    return samples[0];
}


/**
 * Helper function to get the Sensor location from either the Sensor or Meta Table.
 *
 * This function should only be called to determine whether to use MGRS or Lat/Long from the sensor table
 * oe MGRS from the meta table when more than one point of the same location exists
 *
 * @param {string} latSensor Sensor Table Latitude
 * @param {string} longSensor Sensor Table Longitude
 * @param {string} mgrsSensor Sensor Table MGRS
 * @param {string} mgrsMeta Meta Table MGRS
 * @param {string} mgrsMeta Generic MGRS
 * @apiSuccess {String} response.Location
 */
function getLocation(latSensor,longSensor,mgrsSensor,mgrsMeta)
{
    var latitude = parseFloat(latSensor);
    var longitude = parseFloat(longSensor);

    if (mgrsSensor)
    {
        try
        {
            return new Location(mgrsSensor);
        }
        catch
        {

        }
    }

    if (latSensor || longSensor)
    {
        try
        {
            return new Location(latitude,longitude);
        }
        catch
        {

        }
    }

    if (mgrsMeta)
    {
        try
        {
            return new Location(mgrsMeta);
        }
        catch
        {

        }
    }

    return new Location('36WWB7689284956');

}

/**
 * @api {get} /api/rapidhra/hra List HRAs
 * @apiDescription List all DRI HRAs
 * @apiGroup DRI HRA
 * @apiSuccess {Object[]} hra[] Array of DRI HRAs
 * @apiSuccess {String} hra.id
 * @apiSuccess {String} hra.hra_objective
 * @apiSuccess {String} hra.location
 * @apiSuccess {String} hra.sap_file_name
 * @apiSuccess {String} hra.created_by
 * @apiSuccess {Date} hra.created_on
 * @apiSuccess {String} hra.approved_by
 * @apiSuccess {String} hra.last_edited_by
 * @apiSuccess {String} hra.last_edited_on
 * @apiSuccess {Number} hra.uploaded_to_doehrs
 * @apiSuccess {Number} hra.qa_approved
 * @apiSuccess {String} hra.status
 * @apiSuccess {String} hra.sensor_type
 * @apiSuccess {String} hra.media_type
 * @apiSuccess {String} hra.population_at_risk
 * @apiSuccess {Number} hra.individuals_at_risk
 * @apiSuccess {String} hra.chemical_name
 * @apiSuccess {String} hra.cas_number
 * @apiSuccess {Number} hra.acute_peak_pepc
 * @apiSuccess {String} hra.acute_peak_pepc_units
 * @apiSuccess {Number} hra.acute_average_pepc
 * @apiSuccess {String} hra.acute_average_pepc_units
 * @apiSuccess {Number} hra.acute_peak_meg
 * @apiSuccess {String} hra.acute_peak_meg_name
 * @apiSuccess {String} hra.acute_peak_meg_units
 * @apiSuccess {String} hra.acute_peak_risk
 * @apiSuccess {String} hra.acute_peak_confidence
 * @apiSuccess {Number} hra.acute_average_meg
 * @apiSuccess {String} hra.acute_average_meg_name
 * @apiSuccess {String} hra.acute_average_meg_units
 * @apiSuccess {String} hra.acute_average_risk
 * @apiSuccess {String} hra.acute_average_confidence
 * @apiSuccess {String} hra.pathway_id
 * @apiSuccess {String} hra.pathway_name
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     [{
 *         "id": "R-6JHHO9BP8R",
 *         "hra_objective": "Complete an HRA according to the exposure pathway",
 *         "location": "Base Camp AL AWAD",
 *         "sap_file_name": "",
 *         "created_by": "John Doe",
 *         "created_on": "2020-07-10T13:18:45.000Z",
 *         "approved_by": null,
 *         "last_edited_by": null,
 *         "last_edited_on": null,
 *         "uploaded_to_doehrs": 0,
 *         "qa_approved": 0,
 *         "status": "COMPLETE",
 *         "sensor_type": null,
 *         "media_type": null,
 *         "population_at_risk": "Entire Base",
 *         "individuals_at_risk": 120,
 *         "chemical_name": "Carbon Monoxide",
 *         "cas_number": "630-08-0",
 *         "acute_peak_pepc": 20,
 *         "acute_peak_pepc_units": "mg/m3",
 *         "acute_average_pepc": 0,
 *         "acute_average_pepc_units": "mg/m3",
 *         "acute_peak_meg": 95,
 *         "acute_peak_meg_name": "10minNEG",
 *         "acute_peak_meg_units": "mg/m3",
 *         "acute_peak_risk": "Low",
 *         "acute_peak_confidence": "low",
 *         "acute_average_meg": 95,
 *         "acute_average_meg_name": "1hourNEG",
 *         "acute_average_meg_units": "mg/m3",
 *         "acute_average_risk": "Low",
 *         "acute_average_confidence": "low",
 *         "pathway_id": null,
 *         "pathway_name": null
 *     }]
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 */
router.get('/hra', async function(req, res, next){

    try
    {
        // This query will return one row for each chemical in an HRA, not per HRA.
        // This is by design, to support multiple chemicals.
        var response = await db.raw(`
            select
                hra.*,
                pre.chemical_name,
                pre.cas_number,
                pre.peak_pepc as acute_peak_pepc,
                pre.peak_pepc_units as acute_peak_pepc_units,
                pre.avg_pepc as acute_average_pepc,
                pre.avg_pepc_units as acute_average_pepc_units,
                peak.meg as acute_peak_meg,
                peak.meg_name as acute_peak_meg_name,
                peak.meg_units as acute_peak_meg_units,
                peak.risk_level as acute_peak_risk,
                peak.confidence as acute_peak_confidence,
                avg.meg as acute_average_meg,
                avg.meg_name as acute_average_meg_name,
                avg.meg_units as acute_average_meg_units,
                avg.risk_level as acute_average_risk,
                avg.confidence as acute_average_confidence,
                pathway.id as pathway_id,
                pathway.name as pathway_name
            from chem_rapid_hra hra
            left join rhra_pathway pathway
                on pathway.chem_rapid_hra_id = hra.id
            left join rhra_prescreen pre
                on pre.chem_rapid_hra_id = hra.id
            left join rhra_assessment peak
                on peak.type = "ACUTE_PEAK" and peak.chem_rapid_hra_id = hra.id
                and peak.cas_number = pre.cas_number
            left join rhra_assessment avg
                on avg.type = "ACUTE_AVERAGE" and avg.chem_rapid_hra_id = hra.id
                and avg.cas_number = pre.cas_number
        `)

        // Return the response, even if it is empty
        res.json(response[0].map(hra=>DriHra.normalize(hra)));

    }
    catch (err) {
        console.log(err);
        return next(err);

    }
});

/**
 * @api {get} /api/rapidhra/hra/:hraId Get single DRI HRA
 * @apiDescription Look up a DRI HRA by ID
 * @apiGroup DRI HRA
 * @apiParam {Number} hraId URL Parameter. The JHRM ID of the HRA
 * @apiSuccess {Object} hra DRI HRA
 * @apiSuccess {String} hra.id
 * @apiSuccess {String} hra.hra_objective
 * @apiSuccess {String} hra.location
 * @apiSuccess {String} hra.sap_file_name
 * @apiSuccess {String} hra.created_by
 * @apiSuccess {Date} hra.created_on
 * @apiSuccess {String} hra.approved_by
 * @apiSuccess {String} hra.last_edited_by
 * @apiSuccess {String} hra.last_edited_on
 * @apiSuccess {Number} hra.uploaded_to_doehrs
 * @apiSuccess {Number} hra.qa_approved
 * @apiSuccess {String} hra.status
 * @apiSuccess {String} hra.sensor_type
 * @apiSuccess {String} hra.media_type
 * @apiSuccess {String} hra.population_at_risk
 * @apiSuccess {Number} hra.individuals_at_risk
 * @apiSuccess {String} hra.chemical_name
 * @apiSuccess {String} hra.cas_number
 * @apiSuccess {Number} hra.acute_peak_pepc
 * @apiSuccess {String} hra.acute_peak_pepc_units
 * @apiSuccess {Number} hra.acute_average_pepc
 * @apiSuccess {String} hra.acute_average_pepc_units
 * @apiSuccess {Number} hra.acute_peak_meg
 * @apiSuccess {String} hra.acute_peak_meg_name
 * @apiSuccess {String} hra.acute_peak_meg_units
 * @apiSuccess {String} hra.acute_peak_risk
 * @apiSuccess {String} hra.acute_peak_confidence
 * @apiSuccess {Number} hra.acute_average_meg
 * @apiSuccess {String} hra.acute_average_meg_name
 * @apiSuccess {String} hra.acute_average_meg_units
 * @apiSuccess {String} hra.acute_average_risk
 * @apiSuccess {String} hra.acute_average_confidence
 * @apiSuccess {String} hra.pathway_id
 * @apiSuccess {String} hra.pathway_name
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *         "id": "R-6JHHO9BP8R",
 *         "hra_objective": "Complete an HRA according to the exposure pathway",
 *         "location": "Base Camp AL AWAD",
 *         "sap_file_name": "",
 *         "created_by": "John Doe",
 *         "created_on": "2020-07-10T13:18:45.000Z",
 *         "approved_by": null,
 *         "last_edited_by": null,
 *         "last_edited_on": null,
 *         "uploaded_to_doehrs": 0,
 *         "qa_approved": 0,
 *         "status": "COMPLETE",
 *         "sensor_type": null,
 *         "media_type": null,
 *         "population_at_risk": "Entire Base",
 *         "individuals_at_risk": 120,
 *         "chemical_name": "Carbon Monoxide",
 *         "cas_number": "630-08-0",
 *         "acute_peak_pepc": 20,
 *         "acute_peak_pepc_units": "mg/m3",
 *         "acute_average_pepc": 0,
 *         "acute_average_pepc_units": "mg/m3",
 *         "acute_peak_meg": 95,
 *         "acute_peak_meg_name": "10minNEG",
 *         "acute_peak_meg_units": "mg/m3",
 *         "acute_peak_risk": "Low",
 *         "acute_peak_confidence": "low",
 *         "acute_average_meg": 95,
 *         "acute_average_meg_name": "1hourNEG",
 *         "acute_average_meg_units": "mg/m3",
 *         "acute_average_risk": "Low",
 *         "acute_average_confidence": "low",
 *         "pathway_id": null,
 *         "pathway_name": null
 *     }
 * @apiErrorExample HRA Not Found
 *     HTTP/1.1 404 HRA Not Found
 * @apiErrorExample Access Denied
 *     HTTP/1.1 403 Access Denied
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 */
router.get('/hra/:hraId', async function(req, res, next){

    try
    {
        var hras = await db.raw(`
        select
            hra.*,
            pre.chemical_name,
            pre.cas_number,
            pre.peak_pepc as acute_peak_pepc,
            pre.peak_pepc_units as acute_peak_pepc_units,
            pre.avg_pepc as acute_average_pepc,
            pre.avg_pepc_units as acute_average_pepc_units,
            peak.meg as acute_peak_meg,
            peak.meg_name as acute_peak_meg_name,
            peak.meg_units as acute_peak_meg_units,
            peak.risk_level as acute_peak_risk,
            peak.confidence as acute_peak_confidence,
            avg.meg as acute_average_meg,
            avg.meg_name as acute_average_meg_name,
            avg.meg_units as acute_average_meg_units,
            avg.risk_level as acute_average_risk,
            avg.confidence as acute_average_confidence,
            pathway.id as pathway_id,
            pathway.name as pathway_name
        from chem_rapid_hra hra
        left join rhra_pathway pathway
            on pathway.chem_rapid_hra_id = hra.id
        left join rhra_prescreen pre
            on pre.chem_rapid_hra_id = hra.id
        left join rhra_assessment peak
            on peak.type = "ACUTE_PEAK" and peak.chem_rapid_hra_id = hra.id
            and peak.cas_number = pre.cas_number
        left join rhra_assessment avg
            on avg.type = "ACUTE_AVERAGE" and avg.chem_rapid_hra_id = hra.id
            and avg.cas_number = pre.cas_number
        where hra.id = ?
        `, req.params.hraId)

        // Return 404 if we do not find an HRA with the specified ID.
        if (hras[0].length === 0) {
            return next(createError(404, "HRA ID " + req.params.hraId + " Not Found"));
        }
        else {
            return res.json(DriHra.normalize(hras[0][0]));
        }
    }
    catch (err) {
        return next(err);
    }
});

/**
 * @api {post} /api/rapidhra/hra Save DRI HRA
 * @apiDescription Create or save a DRI HRA
 * @apiParam {Number} hraId URL Parameter. The JHRM ID of the HRA
 * @apiParam {Object} hra HRA Properties object
 * @apiParam {String} hra.id
 * @apiParam {String} hra.hra_objective
 * @apiParam {String} hra.location
 * @apiParam {String} hra.population_at_risk
 * @apiParam {Number} hra.individuals_at_risk
 * @apiParam {String} hra.sap_file_name
 * @apiGroup DRI HRA
 * @apiSuccess {Object} response Response JSON Object
 * @apiSuccess {String} response.Status
 * @apiSuccess {String} response.id
 * @apiSuccessExample Success
 *     HTTP/1.1 201 CREATED
 *     {
 *         Status: "Create Record Success"
 *         id: "E-FFGTUSD1ZS"
 *     }
 * @apiErrorExample HRA Not Found
 *     HTTP/1.1 404 HRA Not Found
 * @apiErrorExample Access Denied
 *     HTTP/1.1 403 Not allowed to edit complete HRA.
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 */
router.post('/hra', async function (req, res, next) {

    try {

        if (req.body.id)
        {
            var hra = await db('chem_rapid_hra')
                .where("id", req.body.id);

            if (!hra.length)
            {
                return next(createError(404, "HRA Not Found"));
            }

            hra = hra[0];
            if (hra.status != hrastatus.INCOMPLETE) {
                // If the HRA is complete, this is not allowed
                return next(createError(403, "Not allowed to edit complete HRA."));
            }

            // If no selected pathway, delete any pathways.
            if(!req.body.selected_pathway)
            {
                await db('rhra_pathway').where('chem_rapid_hra_id', req.body.id).del();
            }

            id = req.body.id;
            await updateHra(id, req, {
                'hra_objective': req.body.hra_objective,
                'selected_pathway' : req.body.selected_pathway,
                'location': req.body.location,
                'population_at_risk': req.body.population_at_risk,
                'individuals_at_risk': req.body.individuals_at_risk,
                'sap_file_name': req.body.sap_file_name
            });
        }
        else
        {
            id = uniqueid('R-');
            var responsehra = await db('chem_rapid_hra').insert({
                'id': id,
                'hra_objective': req.body.hra_objective,
                'selected_pathway' : req.body.selected_pathway,
                'location': req.body.location,
                'sap_file_name': req.body.sap_file_name,
                'created_by': req.cookies.user_name,
                'created_on': db.fn.now(6),
                'population_at_risk': req.body.population_at_risk,
                'individuals_at_risk': req.body.individuals_at_risk,
                'status': hrastatus.INCOMPLETE
            });
        }

        return res.status(201).json({
            Status: 'Create Record Success',
            id: id
        });
    }
    catch (err) {
        return next(err);
    }

});

/**
 * @api {get} /api/rapidhra/hra/:hraId/megs Get DRI HRA Megs
 * @apiDescription Get all related megs for a DRI HRA. If no chemical is selected
 *     yet for the HRA, this will return an error.
 * @apiParam {Number} hraId URL Parameter. The JHRM ID of the HRA
 * @apiGroup DRI HRA
 * @apiSuccess {Object[]} meg[] Array of MEGs
 * @apiSuccess {String} meg.chemical_name
 * @apiSuccess {String} meg.cas_number
 * @apiSuccess {DimensionedValue} meg.value
 * @apiSuccess {String} meg.basis
 * @apiSuccess {String} meg.severity
 * @apiSuccess {String} meg.timeframe
 * @apiSuccess {Number} meg.minutes
 * @apiSuccess {String} meg.parm
 * @apiSuccess {Bool} meg.is_cwa
 * @apiSuccess {Number} meg.molecular_weight
 * @apiSuccess {String} meg.media
 * @apiSuccess {String} meg.version
 * @apiSuccessExample Success
 *     HTTP/1.1 200 OK
 *     [{
 *         chemical_name : "Nitric oxide",
 *         cas_number : "10102-43-9",
 *         value :
 *         {
 *             value : 3.68,
 *             isValid : true,
 *             originalValue : 3.68,
 *             units : "mg/m3",
 *             precision : 3,
 *             history : ["Current value: 3.68 mg/m3"]
 *         },
 *         basis : "CEGL*",
 *         severity : "Negligible",
 *         timeframe : "8hour",
 *         minutes : 480,
 *         parm : "8hourNEG",
 *         is_cwa : false,
 *         molecular_weight : 30.0061,
 *         media : "Air",
 *         version : "2013 Revision"
 *     }]
 * @apiErrorExample No associated megs found.
 *     HTTP/1.1 404 No associated megs found.
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 */
router.get('/hra/:hraId/megs', async function(req, res, next)
{
    try
    {
        var samples = await querySamples(req.params.hraId);

        // If no samples, 404
        if(!samples.length)
        {
            return next(createError.NotFound("No associated megs found."));
        }

        var megs = normalizeMegs(await db('tg230_master_meg')
            .leftJoin('chemical_info', 'chemical_info.cas_number','tg230_master_meg.CASRN')
            .where('MEDIA','Air')
            .whereIn('CASRN', [...new Set(samples.filter(sample=>sample.selected).map(sample=>sample.cas_number))])
        );

        // Return the array even if it is empty
        return res.json(megs);
    }
    catch(err)
    {
        return next(err);
    }
});

/**
 * @api {get} /api/rapidhra/hra/:hraId/assessment Get DRI HRA Assessments
 * @apiDescription Get all assessments for the given HRA.
 * @apiParam {Number} hraId URL Parameter. The JHRM ID of the HRA
 * @apiGroup DRI HRA
 * @apiSuccess {Object[]} assessment[] Array of assessments
 * @apiSuccess {Number} assessment.id
 * @apiSuccess {String} assessment.chem_rapid_hra_id
 * @apiSuccess {String} assessment.type
 * @apiSuccess {String} assessment.chemical_name
 * @apiSuccess {String} assessment.cas_number
 * @apiSuccess {Number} assessment.meg
 * @apiSuccess {String} assessment.meg_name
 * @apiSuccess {String} assessment.meg_units
 * @apiSuccess {String} assessment.meg_version
 * @apiSuccess {String} assessment.severity
 * @apiSuccess {String} assessment.prob_degree_of_exp
 * @apiSuccess {Number} assessment.prob_degree_of_exp_score
 * @apiSuccess {String} assessment.prob_rep_of_data
 * @apiSuccess {Number} assessment.prob_rep_of_data_score
 * @apiSuccess {String} assessment.prob_duration_of_exp
 * @apiSuccess {Number} assessment.prob_duration_of_exp_score
 * @apiSuccess {String} assessment.prob_rate_of_exp
 * @apiSuccess {Number} assessment.prob_rate_of_exp_score
 * @apiSuccess {String} assessment.probability
 * @apiSuccess {Number} assessment.probability_score
 * @apiSuccess {String} assessment.risk_level
 * @apiSuccess {String} assessment.confidence
 * @apiSuccess {Number} assessment.custom_exposure_duration
 * @apiSuccess {Number} assessment.custom_meg
 * @apiSuccessExample Success
 *     HTTP/1.1 200 OK
 *     [{
 *         id : 22
 *         chem_rapid_hra_id : "R-P9QFS798RK"
 *         type : "ACUTE_PEAK"
 *         chemical_name : "Nitric oxide"
 *         cas_number : "10102-43-9"
 *         meg : 3.68
 *         meg_name : "8hourNEG"
 *         meg_units : "mg/m3"
 *         meg_version : "2013 Revision"
 *         severity : "Negligible"
 *         prob_degree_of_exp : ""
 *         prob_degree_of_exp_score : 0
 *         prob_rep_of_data : "Adequate"
 *         prob_rep_of_data_score : 2
 *         prob_duration_of_exp : ""
 *         prob_duration_of_exp_score : 0
 *         prob_rate_of_exp : "Moderate"
 *         prob_rate_of_exp_score : 2
 *         probability : "Unlikely"
 *         probability_score : 4
 *         risk_level : "Low"
 *         confidence : "low"
 *         custom_exposure_duration : 10
 *         custom_meg : 0
 *     }]
 * @apiErrorExample No associated megs found.
 *     HTTP/1.1 404 No associated megs found.
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 */
router.get('/hra/:hraId/assessment', async function (req, res, next)
{
    try
    {
        // Assessments are always deleted when selected samples are changed,
        // so we can be sure that any assessment in the database is up to date
        // with the selected samples.
        var assessments = await db('rhra_assessment')
            .where('chem_rapid_hra_id', req.params.hraId);

        return res.json(assessments);
    }
    catch(err)
    {
        return next(err);
    }
});

/**
 * @api {post} /api/rapidhra/hra/:hraId/assessment
 * @apiDescription Post one or more assessments, unique on chemical and type
 * @apiParam {Number} hraId URL Parameter. The JHRM ID of the HRA
 * @apiParam {Object[]} assessment[] Array of assessments
 * @apiParam {String} assessment.chem_rapid_hra_id
 * @apiParam {String} assessment.type
 * @apiParam {String} assessment.chemical_name
 * @apiParam {String} assessment.cas_number
 * @apiParam {Number} assessment.meg
 * @apiParam {String} assessment.meg_name
 * @apiParam {String} assessment.meg_units
 * @apiParam {String} assessment.meg_version
 * @apiParam {String} assessment.severity
 * @apiParam {String} assessment.prob_degree_of_exp
 * @apiParam {Number} assessment.prob_degree_of_exp_score
 * @apiParam {String} assessment.prob_rep_of_data
 * @apiParam {Number} assessment.prob_rep_of_data_score
 * @apiParam {String} assessment.prob_duration_of_exp
 * @apiParam {Number} assessment.prob_duration_of_exp_score
 * @apiParam {String} assessment.prob_rate_of_exp
 * @apiParam {Number} assessment.prob_rate_of_exp_score
 * @apiParam {String} assessment.probability
 * @apiParam {Number} assessment.probability_score
 * @apiParam {String} assessment.risk_level
 * @apiParam {String} assessment.confidence
 * @apiParam {Number} assessment.custom_exposure_duration
 * @apiParam {Number} assessment.custom_meg
 * @apiGroup DRI HRA
 * @apiSuccess {Object} response Response JSON Object
 * @apiSuccess {String} response.Status
 * @apiSuccessExample Success
 *     HTTP/1.1 201 CREATED
 *     {
 *         Status: "Create Record Success"
 *     }
 * @apiErrorExample HRA Not Found
 *     HTTP/1.1 404 HRA Not Found
 * @apiErrorExample Access Denied
 *     HTTP/1.1 403 Not allowed to edit complete HRA.
 * @apiErrorExample Bad Request
 *     HTTP/1.1 400 Assessment missing required field.
 * @apiErrorExample Bad Request
 *     HTTP/1.1 400 At least one assessment is required.
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 */
router.post('/hra/:hraId/assessment', async function (req, res, next)
{
    try
    {
        if (req.hra.status != hrastatus.INCOMPLETE)
        {
            // If the HRA is complete, this is not allowed
            return next(createError(403, "Not allowed to edit complete HRA."));
        }

        var assessments = req.body.assessments || [];
        if(assessments.length == 0)
        {
            return next(createError.BadRequest("At least one assessment is required."))
        }

        // Check all required fields in all provided assessments.
        var required = ['type', 'cas_number'];
        for(let i = 0; i < assessments.length; i++)
        {
            for(let j = 0; j < required.length; j++)
            {
                if(!assessments[i].hasOwnProperty(required[j]))
                {
                    return next(createError.BadRequest(`Assessment missing required field ${required[j]}`));
                }
            }
        }

        // Delete existing assessments for the provided type(s) and chemical(s)
        await db('rhra_assessment')
            .where('chem_rapid_hra_id', req.params.hraId)
            .whereIn('cas_number', [...new Set(assessments.map(assessment=>assessment.cas_number))])
            .whereIn('type', [...new Set(assessments.map(assessment=>assessment.type))])
            .del();

        // Insert new assessments
        await db('rhra_assessment').insert(assessments.map(assessment=>{
            assessment.chem_rapid_hra_id = req.params.hraId;
            return assessment;
        }));

        await updateHra(req.hra.id, req);

        return res.status(201).json({
            Status: 'Create Record Success'
        });
    }
    catch(err)
    {
        return next(err);
    }
});


/**
 * @api {get} /api/rapidhra/hra/:hraId/outcomes Get potential health outcomes
 * @apiDescription Get potential health outcomes from the RD230 reference. Search by CAS number and MEG.
 * @apiGroup DRI HRA
 * @apiParam {Number} hraId URL Parameter. The JHRM ID of the HRA
 * @apiSuccess {object} response
 * @apiSuccess {string} response.potential_health_outcomes
 * @apiSuccessExample Success
 *     HTTP/1.1 200 OK
 *     {
 *         "potential_health_outcomes" : null
 *     }
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 * @apiErrorExample  HRA not found
 *     HTTP/1.1 404 HRA not found
 */
router.get('/hra/:hraId/outcomes', async function(req, res, next){
    try {
        // Get CAS
        var assessment = await db('rhra_assessment')
            .where('type','ACUTE_PEAK')
            .where('chem_rapid_hra_id', req.hra.id);
        if(!assessment.length)
        {
            return next(createError.NotFound());
        }

        // Get Effects
        var effects = await db('meg_critical_effect')
            .where('casrn', assessment[0].cas_number)
            .whereIn('meg_parm', assessment.map(item=>item.meg_name));

        console.log(effects);

        res.json({
            potential_health_outcomes : effects.length > 0 ? effects.map(item=>item.health_effects_basis).join(', ') : null
        });
    }
    catch(err)
    {
        return next(err);
    }
});

/**
 * @api {get}  /api/rapidhra/hra/:hraId/samples Get a list of MultiRae samples for Rapid HRA
 * @apiDescription List all samples
 * @apiParam {Number} hraId URL Parameter. The JHRM ID of the HRA
 * @apiParam {Boolean} selected URL Parameter. Limit results to selected samples
 * @apiGroup DRI HRA
 * @apiSuccess {Object} sample[] Array of samples
 * @apiSuccess {String} sample.sample_id
 * @apiSuccess {String} sample.sampling_point
 * @apiSuccess {String} sample.chemical_name
 * @apiSuccess {String} sample.cas_number
 * @apiSuccess {String} sample.sensor_type
 * @apiSuccess {Date} sample.start_date_time
 * @apiSuccess {Date} sample.end_date_time
 * @apiSuccess {Number} sample.duration
 * @apiSuccess {String} sample.exceeds_meg
 * @apiSuccess {Number} sample.latitude
 * @apiSuccess {Number} sample.longitude
 * @apiSuccess {String} sample.media
 * @apiSuccess {Number} sample.selected
 * @apiSuccess {DimensionedValue} sample.peak_concentration
 * @apiSuccess {DimensionedValue} sample.average_concentration
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    [{
 *        id : "123456",
 *        sampling_point : "Office Area",
 *        chemical_name : "NO",
 *        cas_number : "10102-43-9",
 *        sensor_type : "MULTIRAE",
 *        start_date_time : "2017-08-01T10:53:00.000Z",
 *        end_date_time : "2017-08-01T11:34:00.000Z",
 *        duration : 41,
 *        exceeds_meg : "Yes",
 *        latitude : 33.5396754,
 *        longitude : 44.2019173,
 *        media : "AIR",
 *        selected : 1,
 *        peak_concentration :
 *        {
 *            value : 3.68174847,
 *            isValid : true,
 *            originalValue : 3.68174847,
 *            units : "mg/m3",
 *            precision : 1,
 *            history : [ "Current value: 3 ppm", "Converted from ppm to mg/m3 with equation: 0.0409 x ppm (3) x 30.0061", "Current value: 4 mg/m3" ]
 *        }
 *        average_concentration :
 *        {
 *            value : 2.45449898,
 *            isValid : true,
 *            originalValue : 2.45449898,
 *            units : "mg/m3",
 *            precision : 1,
 *            history : [ "Current value: 2 ppm", "Converted from ppm to mg/m3 with equation: 0.0409 x ppm (2) x 30.0061", "Current value: 2 mg/m3" ]
 *         }
 *     }]
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 * @apiErrorExample  HRA not found
 *     HTTP/1.1 404 HRA not found
 */
router.get('/hra/:hraId/samples', async function(req, res, next){

    try
    {
        var samples = await querySamples(req.params.hraId);

        // Add selected filter
        if(req.query.selected)
        {
            samples = samples.filter(sample=>sample.selected);
        }

        //Remove samples where avarage or peak are null
        samples = samples.filter(sample=> sample.average != null && sample.peak != null);

        // Return 404 if we do not find any samples
        if (!samples.length)
        {
            return next(createError(404, "Sensor Type Not Found"));
        }
        else
        {
            // Get megs
            var megs = normalizeMegs(await db('tg230_master_meg')
                .leftJoin('chemical_info', 'chemical_info.cas_number', 'tg230_master_meg.CASRN')
                .where('MEDIA', 'Air')
                .whereIn('CASRN', samples.map(result => result.cas_number))
            );

            // Return data, even if it is an empty array.
            return res.json(samples.filter(sample=>megs.find(meg=>meg.cas_number===sample.cas_number)).map(sample => {

                let myMegs = megs.filter(meg => meg.cas_number === sample.cas_number);

                var retLocation =  getLocation(sample.latitude, sample.longitude, sample.mgrs, sample.sensor_mgrs_location);

                return {
                    id : sample.sample_id,
                    sampling_point : sample.sensor_location_description,
                    chemical_name : sample.chemical_name,
                    cas_number : sample.cas_number,
                    units : sample.units,
                    sensor_type : sample.sensor_type.toUpperCase(),
                    start_date_time: new Datetime(sample.begin),
                    end_date_time : new Datetime(sample.end),
                    duration : Math.round((sample.end - sample.begin) / 60000),
                    exceeds_meg : calculatePepc.acutePrescreen(new DimensionedValue(sample.peak, sample.units), myMegs).exceeded === true ? 'Yes' : 'No',
                    location : retLocation,
                    media : myMegs[0].media.toUpperCase(),
                    selected : sample.selected,
                    peak_concentration : Unit.normalize(new DimensionedValue(sample.peak, sample.units), myMegs[0]),
                    average_concentration : Unit.normalize(new DimensionedValue(sample.average, sample.units, true, sample.peak.toExponential().replace(/e[\+\-0-9]*$/, '').replace( /(^0\.?0*)|\./, '').length), myMegs[0]),
                    operation_mode : sample.sensor_employment_type
                };

            }));
        }
    }
    catch (err) {
        console.log(err);
        return next(err);
    }
});

/**
 * @api {get} /api/rapidhra/hra/:hraId/results Get Data Log
 * @apiDescription Get the raw datalog values that comprise the sample(s)
 * @apiParam {Number} hraId URL Parameter. The JHRM ID of the HRA
 * @apiGroup DRI HRA
 * @apiSuccess {Object} result[] Array of results
 * @apiSuccess {String} result.id
 * @apiSuccess {String} result.chem_rapid_hra_id
 * @apiSuccess {String} result.unique_sample_id
 * @apiSuccess {String} result.unique_result_id
 * @apiSuccess {String} result.chemical_name
 * @apiSuccess {String} result.cas_number
 * @apiSuccess {String} result.concentration
 * @apiSuccess {String} result.units
 * @apiSuccess {String} result.analytical_method
 * @apiSuccess {String} result.concentration_below_threshold
 * @apiSuccess {String} result.selected
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    [{
 *        id : 54,
 *        chem_rapid_hra_id : "R-P9QFS798RK",
 *        unique_sample_id : "123456",
 *        unique_result_id : "CEW7ADOL6K",
 *        chemical_name : "NO",
 *        cas_number : "10102-43-9",
 *        concentration : 0.304025,
 *        units : "mg/m3",
 *        analytical_method : "",
 *        concentration_below_threshold : 0,
 *        selected : 1,
 *     }]
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 * @apiErrorExample  HRA not found
 *     HTTP/1.1 404 HRA not found
 */
router.get('/hra/:hraId/results', async function (req, res, next) {
    // @LINO: Adjust this as needed for datalog support
    try {
        let results = await db('rhra_sample_results')
        .where('chem_rapid_hra_id', req.hra.id);
        res.json(results.map(result=>{
            delete result.concentration_below_threshold;
            result.concentration = new DimensionedValue(result.concentration, result.units);
            delete result.units;
            return result;
        }));
    }
    catch (err) {
        return next(err);
    }
});

/**
 * @api {post} /api/hra/:hraId/samples Associate Samples
 * @apiDescription Associate samples with this DRI HRA
 * @apiParam {Number} hraId URL Parameter. The JHRM ID of the HRA that is being created
 * @apiParam {Number[]} Array of sample IDs to associate.
 * @apiGroup DRI HRA
 * @apiSuccess {object} response
 * @apiSuccess {string} response.status
 * @apiSuccessExample Success
 *    HTTP/1.1 200 OK
 *    {
 *        "status":"success"
 *    }
 * @apiErrorExample Access Denied
 *     HTTP/1.1 403 Not allowed to edit complete HRA.
 * @apiErrorExample Internal Server Error
 *    HTTP/1.1 500 Internal Server Error
 * @apiErrorExample Internal Server Error
 *    HTTP/1.1 500 No samples found
 * @apiErrorExample Internal Server Error
 *    HTTP/1.1 500 Unable to reconcile sample IDs
 * @apiErrorExample  HRA not found
 *    HTTP/1.1 404 HRA not found
 * @apiErrorExample  Bad Request
 *    HTTP/1.1 401 At least one sample must be selected
 */
router.post('/hra/:hraId/samples', async function(req, res, next){
    try
    {
        if(req.hra.status != hrastatus.INCOMPLETE)
        {
            return next(createError.Forbidden("Cannot edit completed HRA."));
        }

        if(!req.body.sampleIds.length)
        {
            return next(createError.BadRequest("At least one sample must be selected."));
        }

        var samples = await querySamples(req.params.hraId);
        if(!samples.length)
        {
            return next(createError.InternalServerError("No samples found. "));
        }

        var previouslySelected = samples.filter(sample=>sample.selected);

        var selected = samples.filter(sample=>req.body.sampleIds.includes(sample.sample_id));
        if(selected.length != req.body.sampleIds.length)
        {
            return next(createError.InternalServerError("Unable to reconcile sample IDs."))
        }

        // If all selected samples are already marked as selected, and the number
        // of previously selected samples matches the number of currently selected
        // Then the database does not need to be updated, as the sample selection
        // has not changed.
        if(selected.length != previouslySelected.length || !selected.reduce((prev,cur)=>{return prev && cur.selected}, true))
        {
            var megs = normalizeMegs(await db('tg230_master_meg')
                .leftJoin('chemical_info', 'chemical_info.cas_number','tg230_master_meg.CASRN')
                .where('MEDIA','Air')
                .whereIn('CASRN', [...new Set(samples.map(sample=>sample.cas_number))])
            );

            // Use a transaction for the updates so we cannot end up in an
            // undefined state.
            await db.transaction(async trx => {

                //Delete all samples currently associated with hraId
                await trx('rhra_sample')
                    .where('chem_rapid_hra_id', req.params.hraId)
                    .del();

                // Delete Datalog
                // @LINO: Adjust this as needed for datalog support
                await trx('rhra_sample_results')
                    .where('chem_rapid_hra_id', req.params.hraId)
                    .del();

                // Also Delete all prescreens, because they are based on the samples.
                await trx('rhra_prescreen')
                    .where('chem_rapid_hra_id', req.params.hraId)
                    .del();

                // Also Delete all assessments, because they are based on the samples.
                await trx('rhra_assessment')
                    .where('chem_rapid_hra_id', req.params.hraId)
                    .del();

                //Populate the database tables with the selected samples
                await trx('rhra_sample').insert(selected.map(sample=>{
                    return {
                        chem_rapid_hra_id : req.params.hraId,
                        unique_sensor_id : sample.sensor_id,
                        unique_sample_id : sample.sample_id,
                        sensor_type : sample.sensor_type,
                        sampling_point : sample.sensor_location_description || '',
                        media : 'Air',
                        start_date_time : sample.begin,
                        end_date_time : sample.end,
                        sample_time : (sample.end - sample.begin) / 60000,
                        selected : 1,
                        mgrs : getLocation(sample.latitude, sample.longitude, sample.mgrs, sample.sensor_mgrs_location).gridRef
                    };
                }));

                // Populate the datalog
                // @LINO: Adjust this as needed for datalog support
                let myMegs = megs.filter(meg=>meg.cas_number == selected[0].cas_number);
                let datalog = calculatePepc.fakeDataLog({
                    id : selected[0].sample_id,
                    sampling_point : selected[0].sampling_point,
                    chemical_name : selected[0].chemical_name,
                    cas_number : selected[0].cas_number,
                    units : selected[0].units,
                    sensor_type : selected[0].sensor_type.toUpperCase(),
                    start_date_time: selected[0].begin,
                    end_date_time : selected[0].end,
                    duration : (selected[0].end - selected[0].begin) / 60000,
                    exceeds_meg : calculatePepc.acutePrescreen(new DimensionedValue(selected[0].peak, selected[0].units), myMegs).exceeded === true ? 'Yes' : 'No',
                    sensor_mgrs_location : selected[0].sensor_mgrs_location,
                    media : myMegs[0].media.toUpperCase(),
                    selected : selected[0].selected,
                    peak_concentration : Unit.normalize(new DimensionedValue(selected[0].peak, selected[0].units), myMegs[0]),
                    average_concentration : Unit.normalize(new DimensionedValue(selected[0].average, selected[0].units), myMegs[0])
                });
                await trx('rhra_sample_results').insert(datalog.map(item=>{
                    return {
                        chem_rapid_hra_id : req.params.hraId,
                        unique_sample_id : selected[0].sample_id,
                        unique_result_id : uniqueid(),
                        chemical_name : selected[0].chemical_name,
                        cas_number : item.cas_number,
                        concentration : item.concentration.value,
                        units :item.concentration.units,
                        analytical_method : '',
                        concentration_below_threshold : 0,
                        selected : 1
                    };
                }));

                // Populate the prescreen tables
                await trx('rhra_prescreen').insert(
                    [... new Set(selected.map(sample=>sample.cas_number))]
                        .map(cas_number=>{
                            let myMegs = megs.filter(meg=>meg.cas_number == cas_number);
                            let samples = selected
                                .filter(sample=>sample.cas_number == cas_number);

                            let peak  = samples
									.map(sample => Unit.normalize(new DimensionedValue(sample.peak, sample.units), myMegs[0]))
									.reduce((prev, cur) => (!prev || cur.value > prev.value) ? cur : prev);
                            let avg = samples
                                .map(sample => Unit.normalize(new DimensionedValue(sample.average, sample.units), myMegs[0]))
                                .reduce((prev, cur, idx) => {
                                    if(idx==0)
                                    {
                                        return cur;
                                    }
                                    prev.value = ((prev.value * idx) + cur.value) / (idx+1);
                                    return prev;
                                });
                            let prescreen = calculatePepc.acutePrescreen(peak, myMegs);
                            return {
                                chem_rapid_hra_id : req.hra.id,
                                chemical_name : myMegs[0].chemical_name,
                                cas_number : cas_number,
                                valid_samples : 1,
                                peak_pepc : peak.print(true),
                                peak_pepc_units : peak.units,
                                avg_pepc : avg.print(true),
                                avg_pepc_units : avg.units,
                                ar_meg : prescreen.meg.value.value,
                                ar_meg_name : prescreen.meg.parm,
                                ar_meg_units : prescreen.meg.value.units,
                                ar_meg_exceeded : prescreen.exceeded ? 1 : 0,
                                ar_meg_version : prescreen.meg.version
                            };
                        })
                );
            });
        }

        await updateHra(req.hra.id, req);

        return res.json({
            status : 'Success'
        });
    }
    catch(err)
    {
        return next(err);
    }
});

/**
 * @api {post} /api/rapidhra/hra/:hraId/summary.  Set Summary Report Input
 * @apiDescription Associate samples with this DRI HRA
 * @apiParam {Number} hraId URL Parameter. The JHRM ID of the HRA that is being created
 * @apiParam {Object} report
 * @apiParam {String} report.potential_health_outcomes
 * @apiParam {String} report.additional_information
 * @apiParam {String} report.recommended_mitigation
 * @apiParam {String} report.communicated_to_name
 * @apiParam {String} report.communicated_to_position
 * @apiParam {String} report.communicated_to_contact
 * @apiParam {String} report.communicated_date
 * @apiParam {String} report.communicated_by
 * @apiParam {String} report.communicated_how
 * @apiGroup DRI HRA
 * @apiSuccess {Object} response Response JSON Object
 * @apiSuccess {String} response.Status
 * @apiSuccessExample Success
 *     HTTP/1.1 201 CREATED
 *     {
 *         Status: "Create Record Success"
 *     }
 * @apiErrorExample HRA Not Found
 *     HTTP/1.1 404 HRA Not Found
 * @apiErrorExample Access Denied
 *     HTTP/1.1 403 Not allowed to edit complete HRA.
 * @apiErrorExample Bad Request
 *     HTTP/1.1 400 Assessment missing required field.
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 */
router.post('/hra/:hraId/summary', async function (req, res, next) {

    if(req.hra.status != hrastatus.INCOMPLETE)
    {
        return next(createError.Forbidden("Cannot edit completed HRA."));
    }

    // Make sure required fields are set.
    let required = [
        "recommended_mitigation",
        "communicated_to_name",
        "communicated_to_position",
        "communicated_to_contact",
        "communicated_date",
        "communicated_by",
        "communicated_how",
    ];
    for (let i = 0; i < required.length; i++) {
        if (!req.body[required[i]]) {
            return next(
                createError(401, `The parameter "${required[i]}" is required.`)
            );
        }
    }
    try {
        // Delete any existing records that match the hraId and then add new record.
        await db('rhra_report').where('chem_rapid_hra_id', req.params.hraId).del();

        await db('rhra_report').insert({
            chem_rapid_hra_id: req.params.hraId,
            potential_health_outcomes: req.body.potential_health_outcomes,
            additional_information: req.body.additional_information,
            recommended_mitigation: req.body.recommended_mitigation,
            communicated_to_name: req.body.communicated_to_name,
            communicated_to_position: req.body.communicated_to_position,
            communicated_to_contact: req.body.communicated_to_contact,
            communicated_date: req.body.communicated_date,
            communicated_by: req.body.communicated_by,
            communicated_how: req.body.communicated_how,
        });

        await db('chem_rapid_hra').update({
            status : hrastatus.COMPLETE
        }).where('id', req.hra.id);

        await updateHra(req.hra.id, req);

        res.status(201).json({
            Status: 'Create Record Success'
        });
    } catch (err) {
        return next(err);
    }
});

/**
 * @api {post} /api/rapidhra/hra/:hraId/summary.  Set Summary Report Input
 * @apiDescription Associate samples with this DRI HRA
 * @apiParam {Number} hraId URL Parameter. The JHRM ID of the HRA that is being created
 * @apiGroup DRI HRA
 * @apiSuccess {Object} report
 * @apiSuccess {String} report.chem_rapid_hra_id
 * @apiSuccess {String} report.potential_health_outcomes
 * @apiSuccess {String} report.additional_information
 * @apiSuccess {String} report.recommended_mitigation
 * @apiSuccess {String} report.communicated_to_name
 * @apiSuccess {String} report.communicated_to_position
 * @apiSuccess {String} report.communicated_to_contact
 * @apiSuccess {String} report.communicated_date
 * @apiSuccess {String} report.communicated_by
 * @apiSuccess {String} report.communicated_how
 * @apiSuccessExample Success
 *     HTTP/1.1 200 OK
 *     {
 *         chem_rapid_hra_id : "R-2018BE4"
 *         potential_health_outcomes : "value"
 *         additional_information : "value"
 *         recommended_mitigation : "value"
 *         communicated_to_name : "value"
 *         communicated_to_position : "value"
 *         communicated_to_contact : "value"
 *         communicated_date : "value"
 *         communicated_by : "value"
 *         communicated_how : "value"
 *     }
 * @apiErrorExample HRA Not Found
 *     HTTP/1.1 404 HRA Not Found
 * @apiErrorExample Access Denied
 *     HTTP/1.1 403 Not allowed to edit complete HRA.
 * @apiErrorExample Bad Request
 *     HTTP/1.1 400 Assessment missing required field.
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 */
router.get('/hra/:hraId/summary', async function (req, res, next) {

    try {

        let report = await db('rhra_report').where('chem_rapid_hra_id', req.params.hraId);
        if (!report.length)
        {
            return next(createError(404, "No report found."));
        }
        report = report[0];
        report.communicated_date = new Datetime(report.communicated_date, Datetime.FORMATS.DATE);
        res.json(report);
    } catch (err) {
        return next(err);
    }
});

/**
 * @api {post} /api/rapidhra/hra/:hraId/pathway Create exposure pathway
 * @apiDescription Create a new exposure pathway and associate with the
 *     current HRA.
 * @apiParam {Number} hraId URL Parameter. The JHRM ID of the HRA
 * @apiParam {Object} pathway Exposure Pathway
 * @apiParam {String} pathway.location
 * @apiParam {String} pathway.name
 * @apiParam {String} pathway.threat_source
 * @apiParam {String} pathway.health_hazard
 * @apiParam {String} pathway.exposure_point_area
 * @apiParam {String} pathway.exposure_medium
 * @apiParam {String} pathway.exposure_route
 * @apiParam {String} pathway.exposure_duration_of_concern
 * @apiParam {String} pathway.exposed_population
 * @apiGroup DRI HRA
 * @apiSuccess {Object} response Response JSON Object
 * @apiSuccess {String} response.Status
 * @apiSuccess {String} response.id
 * @apiSuccessExample Success
 *     HTTP/1.1 201 CREATED
 *     {
 *         Status: "Create Record Success"
 *         id: "P-FFGTUSD1ZS"
 *     }
 * @apiErrorExample HRA Not Found
 *     HTTP/1.1 404 HRA Not Found
 * @apiErrorExample Access Denied
 *     HTTP/1.1 403 Not allowed to edit complete HRA.
 * @apiErrorExample Invalid Request
 *     HTTP/1.1 401 The parameter "name" is required.
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 */
router.post('/hra/:hraId/pathway', async function (req, res, next) {

    if (req.hra.status != hrastatus.INCOMPLETE)
    {
        // If the HRA is complete, this is not allowed
        return next(createError(403, "Not allowed to edit complete HRA."));
    }

    // Make sure required fields are set. Might need fedback to expand this list
    let required = ["location", "name"];
    for (let i = 0; i < required.length; i++) {
        if (!req.body[required[i]]) {
            return next(createError(401, `The parameter "${required[i]}" is required.`));
        }
    }
    try{
        await db('rhra_pathway').where('chem_rapid_hra_id', req.hra.id).del();
        id = uniqueid('P-');
        await db('rhra_pathway').insert({
            id : id,
            chem_rapid_hra_id : req.hra.id,
            sensor_array_id : req.body.sensor_array_id,
            location : req.body.location,
            name : req.body.name,
            threat_source : req.body.threat_source,
            health_hazard : req.body.health_hazard,
            exposure_point_area : req.body.exposure_point_area,
            exposure_medium : req.body.exposure_medium,
            exposure_route : req.body.exposure_route,
            exposure_duration_of_concern : req.body.exposure_duration_of_concern,
            exposed_population : req.body.exposed_population
        });
        await updateHra(req.hra.id, req);

        return res.status(201).json({
            Status: 'Create Record Success',
            id: id
        });
    }
    catch (err) {
        return next(err);
    }
});

// Force DATETIME strings to MySQL to be interpereted as UTC, regardless of
// the timezone where Node is running. This ensures that all dates will be saved
// to the database correctly and not adjusted from whatever timezone the developer is in.
async function forceTimezone(field) {
        let t = field.toString().split(/[/ :]/);
        return new Date(Date.UTC(t[2], t[1]-1, t[0], t[3]||0, t[4]||0, t[5]||0));
  }

router.post('/dataingestion', async function (req, res, next)
{
    try
    {
        let val = req.body;
        var multirae_sensing_data;
        sample_period_length = val.Summary[13][1];
        number_of_records = val.Summary[14][1];
        begin_time = val.Summary[11][1];
        let regchknum = /^(0|[1-9]\d*)(\.\d+)?$/;
        let colWidth = 0;

        var multirae_sensor = await db('multirae_sensors')
        .where('sensor_sn',val.Summary[3][1])

        if(multirae_sensor.length > 0)
        {
            await db('multirae_sensors').update({
                sender : 'Manual',
                message_id : 'N/A',
                unit_firmware_version : val.Summary[4][1],
                running_mode : val.Summary[5][1],
                datalog_mode : val.Summary[6][1],
                diagnostic_mode : val.Summary[7][1],
                stop_reason : val.Summary[8][1],
                site_id : val.Summary[9][1],
                user_id : val.Summary[10][1],
                sample_period_length : sample_period_length,
                number_of_records : number_of_records
            })
            .where('sensor_sn',val.Summary[3][1]);
        }
        else
        {
            await db('multirae_sensors').insert({
                uci : "JHRM-MultiRAE " + val.Summary[3][1],
                sender : 'Manual',
                message_id : 'N/A',
                sensor_sn : val.Summary[3][1],
                model_number : val.Summary[2][1],
                sensor_type : 'MULTIRAE',
                unit_firmware_version : val.Summary[4][1],
                running_mode : val.Summary[5][1],
                datalog_mode : val.Summary[6][1],
                diagnostic_mode : val.Summary[7][1],
                stop_reason : val.Summary[8][1],
                site_id : val.Summary[9][1],
                user_id : val.Summary[10][1],
                sample_period_length : sample_period_length,
                number_of_records : number_of_records
            });
        }

        var multirae_sensor_id = await db('multirae_sensors')
        .where('sensor_sn',val.Summary[3][1])

        for (c = 1; c<val.Summary[16].length; c++)
        {
            sensor_chem_name = val.Summary[15][c].substr(0, val.Summary[15][c].indexOf('('));
            sensor_measure_units = val.Summary[15][c].match(/\((.{0,10})\)/).pop();

            var multirae_sensing_component = await db('multirae_sensing_component')
                .where('sensing_component_sn',val.Summary[16][c])

            if (multirae_sensing_component.length > 0)
            {
                await db('multirae_sensing_component').update({
                    span : val.Summary[18][c],
                    low_alarm : val.Summary[20][c],
                    high_alarm : val.Summary[21][c],
                    over_alarm : val.Summary[22][c],
                    stel_alarm : val.Summary[23][c],
                    twa_alarm : val.Summary[24][c],
                    calibration_time : await forceTimezone(val.Summary[26][c]),
                    peak : val.Summary[27][c],
                    min : val.Summary[28][c],
                    average : val.Summary[29][c]
                })
                .where('sensor_id',multirae_sensor_id[0].id)
                .where('sensing_component_sn',val.Summary[16][c]);
            }
            else{
                await db('multirae_sensing_component').insert({
                    sensing_component_sn : val.Summary[16][c],
                    sensor_id : multirae_sensor_id[0].id,
                    sensor_chemical : sensor_chem_name,
                    sensor_type : 'MULTIRAE',
                    measure_units : sensor_measure_units,
                    measure_type : val.Summary[17][c],
                    span : val.Summary[18][c],
                    low_alarm : val.Summary[20][c],
                    high_alarm : val.Summary[21][c],
                    over_alarm : val.Summary[22][c],
                    stel_alarm : val.Summary[23][c],
                    twa_alarm : val.Summary[24][c],
                    calibration_time : await forceTimezone(val.Summary[26][c]),
                    peak : val.Summary[27][c],
                    min : val.Summary[28][c],
                    average : val.Summary[29][c]
                });
            }

            var multirae_sensing_component = await db('multirae_sensing_component')
                .where('sensor_id',multirae_sensor_id[0].id)

            //down (i) and right (y) for only "real" values, the first index, last index,
            //and concentrations greater than 0
            prev_time = begin_time;
            for (i=3; i<val.Datalog.length-3; i++)
            {
                for (y=3; y<val.Datalog[1].length; y++)
                {
                    comp_chem_name = val.Datalog[1][y].substr(0, val.Datalog[1][y].indexOf('('));
                    comp_measure_units = val.Datalog[1][y].match(/\((.{0,10})\)/).pop();
                    chem_info = await db('chemical_info')
                        .where('formula', comp_chem_name)

                    if (val.Datalog[2][y] === '(Real)' && sensor_chem_name == comp_chem_name &&
                        (val.Datalog[i][y] > 0 || val.Datalog[i][0] == 1 || val.Datalog[i][0] == (val.Datalog.length-6))){

                        multirae_sensing_data = await db('multirae_sensing_data')
                            .where('sensing_component_id',multirae_sensing_component[c-1].id)
                            .where('end_time',await forceTimezone(val.Datalog[i][1]))
                            .where('chemical_name', comp_chem_name);

                        if(multirae_sensing_data.length > 0)
                        {
                            await db('multirae_sensing_data').insert({
                                sensing_component_id : multirae_sensing_component[c-1].id,
                                sample_id : val.Datalog[i][0],
                                start_time : await forceTimezone(prev_time),
                                end_time : await forceTimezone(val.Datalog[i][1]),
                                chemical_name : comp_chem_name,
                                cas_number : chem_info.length > 0 ? chem_info[0].cas_number : null,
                                chem_measure_value : val.Datalog[i][y] == '---' ? 0 : val.Datalog[i][y],
                                chem_measure_units : comp_measure_units
                            })
                            .where('sensing_component_id', multirae_sensing_data[0].id)
                            .whereNot('start_time', await forceTimezone(prev_time))
                            .whereNot('chemical_name', comp_chem_name)
                        }
                        else
                        {
                            await db('multirae_sensing_data').insert({
                                sensing_component_id : multirae_sensing_component[c-1].id,
                                sample_id : val.Datalog[i][0],
                                start_time : await forceTimezone(prev_time),
                                end_time : await forceTimezone(val.Datalog[i][1]),
                                chemical_name : comp_chem_name,
                                cas_number : chem_info.length > 0 ? chem_info[0].cas_number : null,
                                chem_measure_value : val.Datalog[i][y] == '---' ? 0 : val.Datalog[i][y],
                                chem_measure_units : comp_measure_units
                            });
                        }
                    }
                }
                prev_time = val.Datalog[i][1];
            }

            removeFromIndex = [0,1];
            for (var i = removeFromIndex.length -1; i >= 0; i--)
                val.TWASTEL[1].splice(removeFromIndex[i],1);

            for (var i = removeFromIndex.length -1; i >= 0; i--)
                val.TWASTEL[2].splice(removeFromIndex[i],1);



            for (i=3; i<val.TWASTEL.length; i++)
            {
                for (y=0; y<1; y++)
                {
                    if (val.TWASTEL[i][colWidth+2] > 0 || val.TWASTEL[i][colWidth+3] > 0 || (val.TWASTEL[i][0] == 1 || val.TWASTEL[i][0] == (val.TWASTEL.length-3)))
                    {
                        twa_stel_data = await db('twa_stel_data')
                        .where('sensing_component_id',multirae_sensing_component[c-1].id)
                        .where('date_time',val.TWASTEL[i][1] == '---' ? await forceTimezone(val.Datalog[i][1]) : await forceTimezone(val.TWASTEL[i][1]))
                        .where('sample_id', val.TWASTEL[i][0]);


                        if(twa_stel_data.length == 0)
                        {
                            //insert if new record
                            await db('twa_stel_data').insert({
                                sensing_component_id : multirae_sensing_component[c-1].id,
                                sample_id : val.TWASTEL[i][0],
                                date_time : val.TWASTEL[i][1] == '---' ? await forceTimezone(val.Datalog[i][1]) : await forceTimezone(val.TWASTEL[i][1]),
                                twa : await val.TWASTEL[i][colWidth+2].match(regchknum) == null  ? 0 : val.TWASTEL[i][colWidth+2],
                                stel : await val.TWASTEL[i][colWidth+3].match(regchknum) == null ? 0 : val.TWASTEL[i][colWidth+3]
                            })
                        }
                    }

                }

            }
            colWidth = colWidth + 2;
        }

        return res.status(201).json({
            Status: 'Success',
            Sensor_Id: multirae_sensor_id[0].id
        });
    }
    catch (err) {
        return next(err);
    }
});


/**
 *
 * @api {post} /api/rapidhra/uploadfile
 * @apiDescription Upload files (*.txt) to the database
 * @apiParam {multirae_sensor_id} miltirae sensor id
 * @apiParam {file_name} upload file name
 * @apiParam {file} file buffer
 * @apiGroup RAPIDHRA
 * @apiSuccessExample Success
 *     HTTP/1.1 201 CREATED
 *     {
 *         status: 'Successfully Uploaded File'
 *     }
 * @apiErrorExample Bad Request
 *     HTTP/1.1 400 Assessment missing required field.
 */
router.post('/uploadfile', upload.single('uploadfile'),async function(req, res, next) {
    try {

        var fileobj = await db('multirae_ingestion_files')
        .where('file_name',req.file.originalname)

        if (fileobj.length > 0)
        {
            await db('multirae_ingestion_files').update({
                'multirae_sensor_id' : req.body.sensor_id,
                'file_name' : req.file.originalname,
                'file' : req.file.buffer,
                'created' : new Datetime().date
            })
            .where('id', fileobj[0].id);
        }
        else
        {
            //Insert a new record for the specified id
            await db('multirae_ingestion_files').insert({
                'multirae_sensor_id' : req.body.sensor_id,
                'file_name' : req.file.originalname,
                'file' : req.file.buffer,
                'created' : new Datetime().date
            });
        }

        return res.status(201).json({
            Status: 'Success',
        });
    }catch(err) {
        return next(err);
    }
});

/**
 * @api {get} /api/rapidhra/ingestionfiles file list
 * @apiDescription Return file list previously uploaded
 * @apiGroup RAPIDHRA
 * @apiSuccessExample Success
    HTTP/1.1 200 OK
    {
        "created" : "2021-04-06T20:20:18.000Z"
        "file" : {type: "Buffer", data: Array(18996)}
        "file_name" : NameOfFile.txt"
        "id" : 1
        "multirae_sensor_id" : 5
    }
 * @apiErrorExample Internal Server Error
    HTTP/1.1 500 Internal Server Error
 * @apiErrorExample  HRA not found
    HTTP/1.1 404 HRA not found
 */
router.get('/ingestionfiles', async function (req, res, next) {

    try {

        let response = await db('multirae_ingestion_files');

        res.json(response.map(item=>{
            item.created = new Datetime(item.created);
            return item;
        }));
    } catch (err) {
        return next(err);
    }
});

/**
 *
 * @api {get} /api/rapidhra/downloadfile/:id/:file_name
 * @apiDescription Get the file from the database and download to local computer
 * @apiParam {id} multirae sensor id
 * @apiParam {file_name} name of file to download
 * @apiGroup RAPIDHRA
 * @apiSuccessExample Success
 *         Object Display in Browser
 * @apiErrorExample Bad Request
 *     HTTP/1.1 400 Bad Request.
 */
router.get('/downloadfile/:id', async function(req, res, next) {

    try {

        var fileobj = await db('multirae_ingestion_files')
        .where('id',req.params.id)


        if(!fileobj.length==1)
        {
            throw new createError.NotFound("File not found.");
        }

        fileobj = fileobj[0];

        res.setHeader('Content-Type', 'application/txt');
        res.setHeader('Content-Length', fileobj.file.buffer.byteLength);
        res.setHeader('Content-Disposition', `attachment; filename=${fileobj.file_name}`);

        return res.end(fileobj.file, 'binary');

    }
    catch(err) {
        return next(err);
    }
});


/**
 * @api {get} /api/rapidhra/pathway List MetaData Pathways
 * @apiDescription List exposure pathways optionally filtered by location and environmental media.
 * @apiParam {String} location Location (Optional)
 * @apiParam {String} env_media Environmental Media (Optional)
 * @apiGroup  EHRA DOEHRS Data
 * @apiSuccess {Object[]} pathway[] Array of exposure pathways
 * @apiSuccess {Number} pathway.id
 * @apiSuccess {String} pathway.population_at_risk_descr
 * @apiSuccess {String} pathway.name
 * @apiSuccess {String} pathway.source
 * @apiSuccess {String} pathway.environmental_media
 * @apiSuccess {String} pathway.health_threat
 * @apiSuccess {String} pathway.exposure_route
 * @apiSuccess {String} pathway.affected_personnel
 * @apiSuccess {String} pathway.priority
 * @apiSuccessExample {json} Success
    HTTP/1.1 200 OK
    [{
        "id":14681,
        "name":"PM2.5 AMBIENT AIR",
        "source":"Ambient Air",
        "environmental_media":"Air",
        "health_threat":"Airborne Contaminants/Particulate Matter",
        "exposure_route":"Inhalation",
        "affected_personnel":1,
        "population_at_risk_descr":"Entire Camp",
        "priority":"Low"
    }]
 * @apiErrorExample Invalid parameter
    HTTP/1.1 422 Invalid parameter
 * @apiErrorExample Internal Server Error
    HTTP/1.1 500 Internal Server Error
 */
router.get('/meta/pathway', async function(req,res){

    try
    {
        var query = await db('meta_array_pathway')
            .select('id')
            .select('sensor_array_id')
            .select('location')
            .select('name')
            .select('threat_source')
            .select('health_hazard')
            .select('exposure_point_area')
            .select('exposure_medium')
            .select('exposure_route')
            .select('exposure_duration_of_concern')
            .select('exposed_population');

        res.json(query);

    }
    catch(err)
    {
        res.status(500).send(err.message);
    }
});

/**
 * @api {get} /api/rapidhra/meta/:arrayId/pathway MetaData Pathways
 * @apiDescription Return exposure pathways by arrayId.
 * @apiParam {String} Meta Array Id Pathway
 * @apiGroup  Meta Array Pathway Data
 * @apiSuccess {Object} pathway JSON Meta Array Pathway
 * @apiSuccess {String} pathway.id
 * @apiSuccess {String} pathway.sensor_array_id
 * @apiSuccess {String} pathway.location
 * @apiSuccess {String} pathway.name
 * @apiSuccess {String} pathway.threa_source
 * @apiSuccess {String} pathway.health_hazard
 * @apiSuccess {String} pathway.exposure_point_area
 * @apiSuccess {String} pathway.exposure_medium
 * @apiSuccess {String} pathway.exposure_route
 * @apiSuccess {String} pathway.exposure_duration_of_concern
 * @apiSuccess {String} pathway.exposed_population
 * @apiSuccessExample {json} Success
    HTTP/1.1 200 OK
    {
        "id":SEP-5OH096JR7M,
        "sensor_array_id : M-111112"
        "location : Base Camp TAJI"
        "name":"PM2.5 AMBIENT AIR",
        "threat_source":"Burn Pit",
        "health_hazard":"Polycyclic Aromatic Hydrocarbons (PAHs)",
        "exposure_point_area":"",
        "exposure_medium":"",
        "exposure_route":"Inhalation",
        "exposure_duration_of_concern":"24 Hours",
        "exposed_population":"99"
    }
 * @apiErrorExample Invalid parameter
    HTTP/1.1 422 Invalid parameter
 * @apiErrorExample Internal Server Error
    HTTP/1.1 500 Internal Server Error
 */
router.get('/meta/:arrayId/pathway', async function(req,res){

    try
    {
        var result = await db('meta_array_pathway')
            .where("id", req.params.arrayId);

        res.json(result[0]);

    }
    catch(err)
    {
        res.status(500).send(err.message);
    }
});

/**
 * @api {get} /api/rapidhra/hra/:hraId/pathway Get exposure pathway
 * @apiDescription Get the exposure pathway associated with the current HRA.
 * @apiParam {Number} hraId URL Parameter. The JHRM ID of the HRA
 * @apiGroup DRI HRA
 * @apiSuccess {Object} pathway Exposure Pathway
 * @apiSuccess {String} pathway.id
 * @apiSuccess {String} pathway.location
 * @apiSuccess {String} pathway.name
 * @apiSuccess {String} pathway.threat_source
 * @apiSuccess {String} pathway.health_hazard
 * @apiSuccess {String} pathway.exposure_point_area
 * @apiSuccess {String} pathway.exposure_medium
 * @apiSuccess {String} pathway.exposure_route
 * @apiSuccess {String} pathway.exposure_duration_of_concern
 * @apiSuccess {String} pathway.exposed_population
 * @apiSuccessExample Success
 *     HTTP/1.1 200 OK
 *     [{
 *         id : "value",
 *         location : "value",
 *         name : "value",
 *         threat_source : "value",
 *         health_hazard : "value",
 *         exposure_point_area : "value",
 *         exposure_medium : "value",
 *         exposure_route : "value",
 *         exposure_duration_of_concern : "value",
 *         exposed_population : "value"
 *     }]
 * @apiErrorExample HRA Not Found
 *     HTTP/1.1 404 HRA Not Found
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 */
router.get('/hra/:hraId/pathway', async function (req, res, next) {

    try {
        res.json(await db('rhra_pathway')
            .where('chem_rapid_hra_id', req.hra.id));
    }
    catch (err) {
        return next(err);
    }
});

/**
 * Helper that standardizes error output to be an JSON object, and sets the
 * correct headers. Example body content:
 *
 * {
 *   "status": "Error",
 *   "statusCode": 404,
 *   "message": "HRA Not found."
 * }
 *
 * Error output is santitized to avoid exposing implementation details unless
 * running in debug mode.
 *
 * In order to make certain that this handler is called, especially if your
 * method is asynchronous, make sure to call the next() function provided
 * by express with the error as the first argument.
 *
 */
router.use(function (err, req, res, next) {
    if (!err.statusCode) {
        err = createError(500, err);
    }
    res.status(err.statusCode).json({
        status: 'Error',
        statusCode: err.statusCode,
        message: err.expose || config.get('debug') ? err.message : "Internal Server Error."
    });
});



module.exports = router
