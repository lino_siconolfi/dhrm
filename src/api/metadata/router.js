var express = require('express');
var router = express.Router();
var moment = require('moment');
const db = require('src/util/db');
const uniqueid = require('src/util/uniqueid');
const createError = require('http-errors');
const { Datetime } = require('../../shared/datetime');

/**
 * @api {get} /api/metadata/ get user
 * @apiGroup metadata
 * 
 * @apiSuccess {Object} response Response JSON Object
 * @apiSuccess {String} response.Status
 * @apiSuccess {String} response.id
 * @apiSuccessExample Success
    HTTP/1.1 200 OK
    {
        "id": 1,
        "full_name": "John Smith",
        "email": "jsmith@domenix.com",
        "commercial_phone_number": "555-555-1234",
        "dsn_phone_number": "555-323-2461",
        "unit": "Team Bravo",
        "last_updated": "7/22/2020"
    }
 * @apiErrorExample Internal Server Error
    HTTP/1.1 500 Internal Server Error
 **/
router.get('/user', async function(req, res, next){

    // Validate the user ID with the database
    try 
    {
        var userProfile = await db('meta_user_info').where('user_name', req.cookies.user_name);

        if(!userProfile.length)
        {
            return res.json(userProfile.length);
        }

        // Return the response
        res.json(userProfile[0]);
    
    }
    catch(error)
    {
        res.status(500).send(error.message);
    }
});


/**
 * @api {get} /api/metadata/ post user
 * @apiGroup metadata
 * 
 * @apiSuccess {Object} response Response JSON Object
 * @apiSuccess {String} response.Status
 * @apiSuccess {String} response.id
 * @apiSuccessExample Success
    HTTP/1.1 200 OK
    {
        "id": 1,
        "full_name": "Joe Smith",
        "email": "jsmith@domenix.com",
        "commercial_phone_number": "555-555-1234",
        "dsn_phone_number": "555-323-2461",
        "unit": "Team Bravo"
    }
 * @apiErrorExample Internal Server Error
    HTTP/1.1 500 Internal Server Error
 **/
router.post('/user', async function(req, res, next){

    try 
    {
        var userProfile = await db('meta_user_info').where('user_name', req.cookies.user_name);
      
        if(userProfile.length)
        {
            await db('meta_user_info').update({
                'full_name' : req.body.full_name,
                'email' : req.body.email,
                'commercial_phone_number' : req.body.commercial_phone_number,
                'dsn_phone_number' : req.body.dsn_phone_number,
                'unit' : req.body.unit,
            }).where('user_name', req.cookies.user_name);
        }
        else
        {
            await db('meta_user_info').insert({
                'full_name' : req.body.full_name,
                'email' : req.body.email,
                'commercial_phone_number' : req.body.commercial_phone_number,
                'dsn_phone_number' : req.body.dsn_phone_number,
                'unit' : req.body.unit,
                'user_name' : req.cookies.user_name
            });
        }
                
        var userProfile = await db('meta_user_info').where('user_name', req.cookies.user_name);

        if(!userProfile.length)
        {
            return res.sendStatus(404);
        }

        // Return the response, even if it is empty
        res.json(userProfile);
    
    }
    catch(error)
    {
        res.status(500).send(error.message);
    }
});

/**
 * @api {get} /api/metadata/ sensor array profile
 * @apiDescription List all Sensor Array Profiles
 * @apiGroup Array Profiles
 * @apiSuccess {Object} Response JSON Array Object
 * @apiSuccess {String} response.sensor_array_id
 * @apiSuccess {String} response.fixed_mobile
 * @apiSuccess {String} response.site_mgrs
 * @apiSuccess {String} response.doehrs_location
 * @apiSuccess {String} response.mgrs_location
 * @apiSuccess {String} response.sampling_points_notes
 * @apiSuccess {String} response.sensorType
 * @apiSuccessExample Success
    HTTP/1.1 200 OK
    {
        "sensor_array_id": "M-8RY8OF76KG",
        "fixed_mobile": "Fixed",
        "site_mgrs": "38SMC1661906536",
        "doehrs_location": "Base Camp Taji",
        "location_description": "All within 30 meters of the back gate",
        "mgrs_location": null,
        "sampling_points_notes": "Incident"
        "sensor type: JCAD"
    }
 * @apiErrorExample Internal Server Error
    HTTP/1.1 500 Internal Server Error
 **/
router.get('/sensorarrayprofile', async function(req, res, next){

    // Get full list of sensor array profile with the database
    try 
    {
       // var response = await db('meta_array_profile');
        
        var response = await db.raw(`
        SELECT distinct 
            metaarray.sensor_array_id, 
            metaarray.date_created, 
            metaarray.array_type, 
            sensor.sensor_type,
            count(sensor.sensor_metadata_id) as number_of_sensors_in_array,
            metaarray.array_mgrs,
            metaarray.doehrs_location,
            metaarray.array_location_description,
            metaarray.sampling_reason,
            metaarray.sampling_data_received
        FROM jhrmecddb.meta_array_profile metaarray
        left join meta_sensor_profile sensor
            on metaarray.sensor_array_id = sensor.sensor_metadata_id
        group by metaarray.sensor_array_id;
        `)

        // Return the response, even if it is empty
        res.json(response[0].map(row=>{
            row.date_created = new Datetime(row.date_created);
            return row;
        }));
    
    }
    catch(error)
    {
        res.status(500).send(error.message);
    }
});

router.get('/sensorarrayprofile/:arrayId', async function(req, res, next){
	
	try
	{	

        var arrayprofile = await db('meta_array_profile')
            .where('sensor_array_id', req.params.arrayId);

        // Return 404 if we do not find an Sensor Array Profile with the specified ID.
        if (arrayprofile.length === 0) {
            return next(createError(404, "Sensor Array Profile " + req.params.arrayId + " Not Found"));
        }
        else 
        {
            let response = arrayprofile[0];
            response.site_occupation_date = new Datetime(response.site_occupation_date, Datetime.FORMATS.DATE);
            return res.json(arrayprofile[0]);
        }
    }
    catch (err) {
        return next(err);
    }
});

router.post('/sensorarrayprofile', async function (req, res, next) {

    try {

        if (req.body.id) 
        {
            var sensor_array = await db('meta_array_profile')
                .where("sensor_array_id", req.body.id);

            if (!sensor_array.length) 
            {
                return next(createError(404, "Array Profile Not Found"));
            }

            id = req.body.id;
            await db('meta_array_profile').update({
                'sensor_array_id' : id,
                'date_created' : await getDate(),
                'array_type' : req.body.array_type,
                'array_mgrs' : req.body.array_mgrs,
                'doehrs_location' : req.body.doehrs_location || '',
                'array_location_description' : req.body.array_location_description,
                'site_occupation_date' : req.body.site_occupation_date,
                'sampling_reason' : req.body.sampling_reason,
                'sampling_reason_narrative' : req.body.sampling_reason_narrative
            }).where('sensor_array_id', req.body.id);
        }
        else 
        {
            id = uniqueid('M-');
            var response = await db('meta_array_profile').insert({
                'sensor_array_id' : id,
                'date_created' : await getDate(),
                'array_type' : req.body.array_type,
                'array_mgrs' : req.body.array_mgrs,
                'doehrs_location' : req.body.doehrs_location || '',
                'array_location_description' : req.body.array_location_description,
                'site_occupation_date' : req.body.site_occupation_date,
                'sampling_reason' : req.body.sampling_reason,
                'sampling_reason_narrative' : req.body.sampling_reason_narrative
            });
        }

        return res.status(201).json({
            Status: 'Create Record Success',
            id: id
        });
    }
    catch (err) {
        return next(err);
    }

});

async function getDate()
{
    var currentdate = new Date(); 
    var datetime = currentdate.getFullYear() + "/"
                    + (currentdate.getMonth()+1)  + "/" 
                    + currentdate.getDate() + " "  
                    + currentdate.getHours() + ":"  
                    + currentdate.getMinutes() + ":" 
                    + currentdate.getSeconds();

    return datetime;
}

/**
 * @api {get} /api/metadata/sensorarray/:arrayId/pathway Get exposure pathway
 * @apiDescription Get the exposure pathway associated with the current sensor array.
 * @apiParam {AlphaNumeric} Sensor Array ID URL Parameter. The Sensor Array ID of the Array Profile
 * @apiGroup MetaData Sensor Array Profile
 * @apiSuccess {Object} pathway Exposure Pathway
 * @apiSuccess {String} pathway.id
 * @apiSuccess {String} pathway.location
 * @apiSuccess {String} pathway.name
 * @apiSuccess {String} pathway.threat_source
 * @apiSuccess {String} pathway.health_hazard
 * @apiSuccess {String} pathway.exposure_point_area
 * @apiSuccess {String} pathway.exposure_medium
 * @apiSuccess {String} pathway.exposure_route
 * @apiSuccess {String} pathway.exposure_duration_of_concern
 * @apiSuccess {String} pathway.exposed_population
 * @apiSuccessExample Success
 *     HTTP/1.1 200 OK
 *     [{
 *         id : "value",
 *         location : "value",
 *         name : "value",
 *         threat_source : "value",
 *         health_hazard : "value",
 *         exposure_point_area : "value",
 *         exposure_medium : "value",
 *         exposure_route : "value",
 *         exposure_duration_of_concern : "value",
 *         exposed_population : "value"
 *     }]
 * @apiErrorExample Sensor Array Not Found
 *     HTTP/1.1 404 Sensor Array Not Found
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 */
router.get('/sensorarray/:arrayId/pathway', async function (req, res, next) {

    try {
        res.json(await db('meta_array_pathway')
            .where('sensor_array_id', req.params.arrayId));
    }
    catch (err) {
        return next(err);
    }
});

/**
 * @api {post} /api/metadata/sensorarray/:arrayId/pathway Create exposure pathway for sensor array profile
 * @apiDescription Create a new exposure pathway and associate with the 
 *     current Sensor Array Profile. 
 * @apiParam {Number} sensor_array_id URL Parameter. The sensor array ID of the sensor array profile
 * @apiParam {Object} pathway Exposure Pathway
 * @apiParam {String} pathway.location
 * @apiParam {String} pathway.name
 * @apiParam {String} pathway.threat_source
 * @apiParam {String} pathway.health_hazard
 * @apiParam {String} pathway.exposure_point_area
 * @apiParam {String} pathway.exposure_medium
 * @apiParam {String} pathway.exposure_route
 * @apiParam {String} pathway.exposure_duration_of_concern
 * @apiParam {String} pathway.exposed_population
 * @apiGroup MetaData Sensor Array Profile
 * @apiSuccess {Object} response Response JSON Object
 * @apiSuccess {String} response.Status
 * @apiSuccess {String} response.id
 * @apiSuccessExample Success
 *     HTTP/1.1 201 CREATED
 *     {
 *         Status: "Create Record Success"
 *         id: "SAP-FFGTUSD1ZS"
 *     }
 * @apiErrorExample Sensor Array Not Found
 *     HTTP/1.1 404 Sensor Array Not Found
 * @apiErrorExample Access Denied
 *     HTTP/1.1 403 Not allowed to edit complete process.
 * @apiErrorExample Invalid Request
 *     HTTP/1.1 401 The parameter "name" is required.
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 */
router.post('/sensorarray/:arrayId/pathway', async function (req, res, next) {

    // Make sure required fields are set. Might need fedback to expand this list 
    let required = ["location", "name"];
    for (let i = 0; i < required.length; i++) {
        if (!req.body[required[i]]) {
            return next(createError(401, `The parameter "${required[i]}" is required.`));
        }
    }
    try{
        await db('meta_array_pathway').where('sensor_array_id', req.params.arrayId).del();
        id = uniqueid('SEP-');
        await db('meta_array_pathway').insert({
            id : id,
            sensor_array_id : req.params.arrayId,
            location : req.body.location,
            name : req.body.name,
            threat_source : req.body.threat_source,
            health_hazard : req.body.health_hazard,
            exposure_point_area : req.body.exposure_point_area,
            exposure_medium : req.body.exposure_medium,
            exposure_route : req.body.exposure_route,
            exposure_duration_of_concern : req.body.exposure_duration_of_concern,
            exposed_population : req.body.exposed_population
        });
        
        return res.status(201).json({
            Status: 'Create Record Success',
            id: id
        });
    }
    catch (err) {
        return next(err);
    }
});

/**
 * @api {get} /api/metadata/sensorarray/:arrayID/sensordetails sensor details
 * @apiDescription List all Sensors in an Array
 * @apiGroup Sensor Details
 * @apiSuccess {Object} Response JSON Array Object
 * @apiSuccess {String} response.sensor_metadata_id
 * @apiSuccess {String} response.sensor_id
 * @apiSuccess {String} response.sensor_type
 * @apiSuccess {String} response.serial_number
 * @apiSuccess {String} response.software_version
 * @apiSuccess {String} response.hardware_version
 * @apiSuccess {String} response.mfr_date
 * @apiSuccess {String} response.sensor_mgrs_location
 * @apiSuccess {String} response.sensor_location_description
 * @apiSuccess {String} response.sensor_employment_type
 * @apiSuccessExample Success
    HTTP/1.1 200 OK
    {
        "sensor_metadata_id": "M-8RY8OF76KG",
        "Sensor_id" : "JCAD13",
        "Sensor_type" : "JCAD",
        "Serial_number" : "JCAD1o1133",
        "Software_version" : "12.2",
        "Hardware_version" : "3",
        "MFR_Date" : "1/1/2020",
        "Sensor_mgrs_location" : "38SMC1661906536",
        "Sensor_location_description" : "All by the back entrance",
        "Sensor_employment_type" : "null"
    }
 /* @apiErrorExample Internal Server Error
    HTTP/1.1 500 Internal Server Error
 **/
router.get('/sensorarray/:arrayId/sensordetails', async function(req, res, next){

    // Get full list of sensors in a specific array
    try 
    {
       var response = await db('meta_sensor_profile').where('sensor_metadata_id', req.params.arrayId)
      res.json(response.map(row=>{
          row.mfr_date = new Datetime(row.mfr_date, Datetime.FORMATS.DATE);
          return row;
      }));
    
    }
    catch(error)
    {
        res.status(500).send(error.message);
    }
});

/**
 * @api {post} /api/metadata/sensorarray/:arrayId/sensordetails Add sensors to an Array profile
 * @apiDescription Add a new sensor to the current Sensor Array Profile. 
 * @apiParam {Number} sensor_array_id URL Parameter. The sensor array ID of the sensor array profile
 * @apiGroup MetaData Sensor Details
 * @apiSuccess {Object} Response JSON Array Object
 * @apiSuccess {String} response.sensor_metadata_id
 * @apiSuccess {String} response.sensor_id
 * @apiSuccess {String} response.sensor_type
 * @apiSuccess {String} response.serial_number
 * @apiSuccess {String} response.software_version
 * @apiSuccess {String} response.hardware_version
 * @apiSuccess {String} response.mfr_date
 * @apiSuccess {String} response.sensor_mgrs_location
 * @apiSuccess {String} response.sensor_location_description
 * @apiSuccess {String} response.sensor_employment_type
 * @apiSuccessExample Success
 *     HTTP/1.1 201 CREATED
 *     {
 *         Status: "Successfully added to the sensor array!"
 *         id: "SAP-FFGTUSD1ZS"
 *     }
 * @apiErrorExample Sensor Not Found
 *     HTTP/1.1 404 Sensor Not Found
 * @apiErrorExample Access Denied
 *     HTTP/1.1 403 Not allowed to edit complete process.
 * @apiErrorExample Invalid Request
 *     HTTP/1.1 401 The parameter "name" is required.
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 */
router.post('/sensorarray/:arrayId/:sensorId/sensordetails', async function (req, res, next) {

    try
    {
        let sensor_id = undefined;

        let records = await db('meta_sensor_profile')
                        .where('sensor_metadata_id', req.params.arrayId)
                        .where('serial_number', req.params.sensorId);
        
        if (!records.length) 
        {
            sensor_id = uniqueid('SENSOR-');
            await db('meta_sensor_profile').insert({
                sensor_metadata_id : req.params.arrayId,
                sensor_id : sensor_id,
                sensor_type : req.body.sensor_type,
                serial_number : req.body.serial_number,
                software_version : req.body.software_version,
                hardware_version : req.body.hardware_version,
                mfr_date : req.body.mfr_date,
                sensor_mgrs_location : req.body.sensor_mgrs_location,
                sensor_location_description : req.body.sensor_location_description,
                sensor_employment_type : req.body.sensor_employment_type
            });
        }
        else
        {
            sensor_id = records[0].sensor_id;

            await db('meta_sensor_profile')
                .where('sensor_metadata_id', req.params.arrayId)
                .where('serial_number', req.params.sensorId)
                .update({
                    sensor_metadata_id : req.params.arrayId,
                    sensor_id : sensor_id,
                    sensor_type : req.body.sensor_type,
                    serial_number : req.body.serial_number,
                    software_version : req.body.software_version,
                    hardware_version : req.body.hardware_version,
                    mfr_date : req.body.mfr_date,
                    sensor_mgrs_location : req.body.sensor_mgrs_location,
                    sensor_location_description : req.body.sensor_location_description,
                    sensor_employment_type : req.body.sensor_employment_type
                });
        }    
    
        return res.status(201).json({
            Status: 'Create Record Success',
            id: sensor_id
        });
    }
    catch (err) {
        return next(err);
    }
});


/**
 * @api {post} /api/metadata/sensorarray/:arrayId/:sensorSn/deletesensor Add sensors to an Array profile
 * @apiDescription Delete a new sensor from the Sensor Array Profile. 
 * @apiParam {alpha-number} sensor_metadata_id URL Parameter. The sensor array ID of the sensor array profile
 * @apiParam {Number} serial_number URL Parameter. The serial number of the sensor array
 * @apiGroup Delete MetaData Sensor Row
 * @apiSuccess {Object} Response JSON Array Object
 * @apiSuccess {String} response.sensor_metadata_id
 * @apiSuccess {String} response.serial_number
 * @apiSuccessExample Success
 *     HTTP/1.1 201 CREATED
 *     {
 *         Status: "Delete Record Success"
 *         id: "M-123456"
 *     }
 * @apiErrorExample Sensor Not Found
 *     HTTP/1.1 404 Sensor Not Found
 */
router.post('/sensorarray/:arrayId/:sensorSn/deletesensor', async function (req, res, next) {

    try
    {

        let records = await db('meta_sensor_profile')
                        .where('sensor_metadata_id', req.params.arrayId)
                        .where('serial_number', req.params.sensorSn)
                        .del();
        
        if (!records) 
        {
            return next(createError(404, "Sensor Array Not Found"));
        }
        
        return res.status(201).json({
            Status: 'Delete Record Success',
            ArrayId: req.params.arrayId
        });
        
    }
    catch (err) 
    {
        return next(err);
    }
});


/**
 * @api {get} /api/metadata/sensorarray/:arrayId/:sensorSn/deletesensor Get sensors from an Array profile
 * @apiDescription Get a new sensor from the Sensor Array Profile. 
 * @apiParam {alpha-number} sensor_metadata_id URL Parameter. The sensor array ID of the sensor array profile
 * @apiParam {Number} serial_number URL Parameter. The serial number of the sensor array
 * @apiGroup Get Selected MetaData Sensor Row
 * @apiSuccess {Object} Response JSON Array Object
 * @apiSuccess {String} response.sensor_metadata_id
 * @apiSuccess {String} response.sensor_id
 * @apiSuccess {String} response.sensor_type
 * @apiSuccess {String} response.serial_number
 * @apiSuccess {String} response.software_version
 * @apiSuccess {String} response.hardware_version
 * @apiSuccess {String} response.mfr_date
 * @apiSuccess {String} response.sensor_mgrs_location
 * @apiSuccess {String} response.sensor_location_description
 * @apiSuccess {String} response.sensor_employment_type
 * @apiSuccessExample Success
 *     HTTP/1.1 200 OK
    {
        "sensor_metadata_id": "M-8RY8OF76KG",
        "Sensor_id" : "JCAD13",
        "Sensor_type" : "JCAD",
        "Serial_number" : "JCAD1o1133",
        "Software_version" : "12.2",
        "Hardware_version" : "3",
        "MFR_Date" : "1/1/2020",
        "Sensor_mgrs_location" : "38SMC1661906536",
        "Sensor_location_description" : "All by the back entrance",
        "Sensor_employment_type" : "Datalog"
    }
 * @apiErrorExample Sensor Not Found
 *     HTTP/1.1 404 Sensor Not Found
 */
router.get('/sensorarray/:arrayId/:sensorSn/getrowsensor', async function (req, res, next) {

    try
    {

        let records = await db('meta_sensor_profile')
                        .where('sensor_metadata_id', req.params.arrayId)
                        .where('serial_number', req.params.sensorSn);
        
        if (!records) 
        {
            return next(createError(404, "Sensor Array Not Found"));
        }

        let response = records[0];
        response.mfr_date = new Datetime(response.mfr_date, Datetime.FORMATS.DATE);
        
        res.json(records[0]);
        
    }
    catch (err) 
    {
        return next(err);
    }
});

 module.exports = router