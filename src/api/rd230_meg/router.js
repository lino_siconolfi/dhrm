var express = require('express');
var router = express.Router();
var config = require('config');
const db = require('src/util/db');

/**
 * @api {get} /api/rd230/ Get All rd230
 * @apiDescription Get all rd230 records in the system
 * @apiGroup rd230
 */
router.get('/list', function (req, res) {
	db('meg_critical_effect')
	    .select('meg_critical_effect.id as id')
	    .select('meg_critical_effect.chemical_name as chemical_name')
	    .select('meg_critical_effect.casrn as casrn')
	    .select('meg_critical_effect.meg_value as meg_value')
	    .select('meg_critical_effect.units as units')
	    .select('meg_critical_effect.basis as basis')
	    .select('meg_critical_effect.media as media')
	    .select('meg_critical_effect.severity as severity')
	    .select('meg_critical_effect.timeframe as timeframe')
	    .select('meg_critical_effect.intake_rate as intake_rate')
        .select('meg_critical_effect.basic_lookup as basic_lookup')
        .select('meg_critical_effect.health_effects_basis as health_effects_basis')
	    .then(function(result){
	    	res.json(result);
	    })
})


module.exports = router

