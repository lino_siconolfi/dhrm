const express = require('express');
const router = express.Router();
const db = require('src/util/db');
const MhceApi = require('src/mhce/api-library');
const createError = require('http-errors');
const config = require('config');



/**
 * Given an array of WFL records, update the JHRM dataabse
 * 
 * @param {Object[]} wfls 
 */
async function updateWfl(wfls) 
{
    await db.transaction(async transaction => 
        {
            for(const wfl of wfls)
            {
                await transaction('wfl_weapon_firing_log').update({
                    dod_id: wfl.DOD,
                    blast_wfl_id: wfl.Blast_WFL_Id,
                    document_id: wfl.Document_Id,
                    app_nucleus_id: wfl.AppNucleus_Id,
                    blast_record_id: wfl.BlastRecord_Id,
                    blast_web_user_id: wfl.BlastWebUser_Id,
                    user_name: wfl.Username,
                    approval_status: wfl.ApprovalStatus,
                    capture_method: wfl.CaptureMethod,
                    num_of_weapons: wfl.NumberOfWeapons,
                    other_users: wfl.OtherUsers,
                    from_date_time: wfl.FromDateTime,
                    to_date_time: wfl.ToDateTime,
                    time_zone: wfl.Timezone,
                    firing_range: wfl.Range,
                    location: wfl.Location,
                    event_title: wfl.EventTitle,
                    unit_name: wfl.UnitName,
                    bop_sensor: wfl.BOPSensor,
                    in_ear_protection: wfl.InEarProtection,
                    over_ear_protection: wfl.OverEarProtection
                })
                .where('blast_wfl_id', wfl.Blast_WFL_Id);

                for(const weaponFired of wfl.WeaponsFired)
                {
                    await transaction('wfl_weapons_fired').update({
                        blast_wfl_id: wfl.Blast_WFL_Id,
                        wfl_weapons_fired_id: weaponFired.WeaponsFired_Id,
                        weapon_system_type: weaponFired.WeaponSystemType,
                        weapon_system: weaponFired.WeaponSystem,
                        rounds_fired: weaponFired.RoundsFired,
                        body_position: weaponFired.BodyPosition,
                        crew_position: weaponFired.CrewPosition,
                        enclosed_space: weaponFired.EnclosedSpace,
                        weapons_mod: weaponFired.WeaponsMod,
                        breaching_door: weaponFired.BreachingDoor,
                        breaching_wall: weaponFired.BreachingWall,
                        munition_type: weaponFired.MunitionType,
                        munition_type_other: weaponFired.MunitionTypeOther,
                        other_details: weaponFired.OtherDetails,
                        proximity_to_source: weaponFired.ProximityToSource,
                        stacking_position: weaponFired.StackingPosition,
                        breaching_umbc: weaponFired.BreachingUmbc,
                        breaching_sof: weaponFired.BreachingSOF,
                        under_water: weaponFired.UnderWater,
                        breaching_cust_conf_desc: weaponFired.BreachingCustConfDesc,
                        breaching_cust_weight: weaponFired.BreachingCustWeight,
                        breaching_wall_type: weaponFired.BreachingWallType,
                        breaching_door_type: weaponFired.BreachingDoorType,
                        breaching_target_door: weaponFired.BreachingTargetDoor,
                        breaching_target_wall: weaponFired.BreachingTargetWall,
                        breaching_target_other: weaponFired.BreachingTargetOther,
                        target_other_details: weaponFired.TargetOtherDetails
                    })
                    .where('blast_wfl_id', wfl.Blast_WFL_Id)
                    .where('wfl_weapons_fired_id', weaponFired.WeaponsFired_Id);
                }
            }
            await transaction.commit();
        });
}

/**
 * Given an array of WFL records, insert into the JHRM dataabse
 * 
 * @param {Object[]} wfls 
 */
async function insertWfl(wfls) 
{
    await db.transaction(async transaction => 
    {
        for(const wfl of wfls)
        {
            await transaction('wfl_weapon_firing_log').insert({
                dod_id: wfl.DOD,
                blast_wfl_id: wfl.Blast_WFL_Id,
                document_id: wfl.Document_Id,
                app_nucleus_id: wfl.AppNucleus_Id,
                blast_record_id: wfl.BlastRecord_Id,
                blast_web_user_id: wfl.BlastWebUser_Id,
                user_name: wfl.Username,
                approval_status: wfl.ApprovalStatus,
                capture_method: wfl.CaptureMethod,
                num_of_weapons: wfl.NumberOfWeapons,
                other_users: wfl.OtherUsers,
                from_date_time: wfl.FromDateTime,
                to_date_time: wfl.ToDateTime,
                time_zone: wfl.Timezone,
                firing_range: wfl.Range,
                location: wfl.Location,
                event_title: wfl.EventTitle,
                unit_name: wfl.UnitName,
                bop_sensor: wfl.BOPSensor,
                in_ear_protection: wfl.InEarProtection,
                over_ear_protection: wfl.OverEarProtection
            });

            for(const weaponFired of wfl.WeaponsFired)
            {
                await transaction('wfl_weapons_fired').insert({
                    blast_wfl_id: wfl.Blast_WFL_Id,
                    wfl_weapons_fired_id: weaponFired.WeaponsFired_Id,
                    weapon_system_type: weaponFired.WeaponSystemType,
                    weapon_system: weaponFired.WeaponSystem,
                    rounds_fired: weaponFired.RoundsFired,
                    body_position: weaponFired.BodyPosition,
                    crew_position: weaponFired.CrewPosition,
                    enclosed_space: weaponFired.EnclosedSpace,
                    weapons_mod: weaponFired.WeaponsMod,
                    breaching_door: weaponFired.BreachingDoor,
                    breaching_wall: weaponFired.BreachingWall,
                    munition_type: weaponFired.MunitionType,
                    munition_type_other: weaponFired.MunitionTypeOther,
                    other_details: weaponFired.OtherDetails,
                    proximity_to_source: weaponFired.ProximityToSource,
                    stacking_position: weaponFired.StackingPosition,
                    breaching_umbc: weaponFired.BreachingUmbc,
                    breaching_sof: weaponFired.BreachingSOF,
                    under_water: weaponFired.UnderWater,
                    breaching_cust_conf_desc: weaponFired.BreachingCustConfDesc,
                    breaching_cust_weight: weaponFired.BreachingCustWeight,
                    breaching_wall_type: weaponFired.BreachingWallType,
                    breaching_door_type: weaponFired.BreachingDoorType,
                    breaching_target_door: weaponFired.BreachingTargetDoor,
                    breaching_target_wall: weaponFired.BreachingTargetWall,
                    breaching_target_other: weaponFired.BreachingTargetOther,
                    target_other_details: weaponFired.TargetOtherDetails
                });
            }
        }
        await transaction.commit();
    });
}

// This route pulls a single WFL entry as well as its associated entries (by blast_wfl_id) in weapons the fired table (used for testing)
router.get('/wfllog', async function (req, res, next) 
{
    try 
    {
        let firingLog = await db('wfl_weapon_firing_log')
            .where('blast_wfl_id', req.query.iD);
        let weaponsFired = await db('wfl_weapons_fired')
            .where('blast_wfl_id', req.query.iD);

        // Put data from database into array. First element will always be weapons firing log followed by all weapons fired tables.   
        let fullLog = [];

        fullLog.push(firingLog[0]);
        weaponsFired.forEach(element => fullLog.push(element));

        // Now we can call the Smartabase API to firingLog send to Smartabase
        let importWfl = await SmartabaseApi.eventImportWFL(fullLog);

        res.json(importWfl);
    } catch (err) 
    {
        return next(err);
    }
});

// This route is for taking a single WFL form and posting to Smartabse
router.get('/import', async function (req, res, next) 
{
    try 
    {
        let wfls = await MhceApi.getWfls()

        await insertWfl(wfls.data)
        await updateWfl(wfls.updates)

        res.json("Success");

    } catch (err) 
    {
        return next(err);
    }
});


/**
 * Helper that standardizes error output to be an JSON object, and sets the
 * correct headers. Example body content: 
 *  
 * {
 *   "status": "Error",
 *   "statusCode": 404,
 *   "message": "HRA Not found."
 * }
 * 
 * Error output is santitized to avoid exposing implementation details unless 
 * running in debug mode. 
 * 
 * In order to make certain that this handler is called, especially if your 
 * method is asynchronous, make sure to call the next() function provided 
 * by express with the error as the first argument. 
 * 
 */
 router.use(function (err, req, res, next) {
    if (!err.statusCode) {
        err = createError(500, err);
    }
    res.status(err.statusCode).json({
        status: 'Error',
        statusCode: err.statusCode,
        message: err.expose || config.get('debug') ? err.message : "Internal Server Error."
    });
});

module.exports = router