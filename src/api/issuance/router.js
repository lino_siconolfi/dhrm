const esmImport = require('esm')(module);
const express = require('express');
const router = express.Router();
const db = require('src/util/db');
const createError = require('http-errors');
const config = require('config');
const { validDodid } = esmImport('../../shared/validDodid');
var users = require('../../util/users');
const { ISS_DEVICE_STATUS, ISS_EVENT_TYPE, ISS_ASSOCIATION, ISS_DEVICE_TYPE_ID} = esmImport('../../shared/Constants');
const { Datetime } = esmImport('../../shared/datetime');


/**
 * Helper function to add/update an issued personnel to/in the database.
 *
 * @param {Object} query builder
 * @param {Object} person object containing Issuance Personnel attributes
 * @return {Int} record ID
 */
async function addPersonnel(db, person) {
    let person_record = {
        first_name : person.first_name,
        last_name : person.last_name,
        dodid : person.dodid,
        unit : person.unit,
        grade: person.grade,
        mos_code: person.mos_code
    }

    // Attempt to update personnel if id is supplied.
    if (person.id)
    {
        let update = await db('iss_personnel')
            .update(person_record)
            .where('id', person.id);

        if (update == 1)
        {
            return person.id;
        }
        else
        {
            return -2;
        }
    }

    // See if an existing user with the same DODID exists, and if
    // so, get the ID of that record.
    let existing = await db('iss_personnel').where('dodid', person_record.dodid);

    if (existing.length > 0)
    {
        return -1;
    }
    else
    {
        let id = await db('iss_personnel').insert(person_record);
        return id[0];
    }
}

/**
 * @api {get}  /api/issuance/inventory Get the full Inventory of Sensors
 * @apiDescription List of all issued device types
 * @apiGroup JHRM Issuance Sensor
 * @apiSuccess {Object[]} issuancedevices[] Array of Issued Sensors
 * @apiSuccess {String} issuancedevices.id
 * @apiSuccess {String} issuancedevices.serial_number
 * @apiSuccess {String} issuancedevices.manufacturer
 * @apiSuccess {String} issuancedevices.model_number
 * @apiSuccess {String} issuancedevices.lot_number
 * @apiSuccess {String} issuancedevices.status
 * @apiSuccess {String} issuancedevices.device_type
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *      [{
 *       "id": 1,
 *       "serial_number": "2af1245",
 *       "manufacturer": "Honeywell",
 *       "model_number": "1.0",
 *       "lot_number": "N/A",
 *       "status": "ISSUED",
 *       "device_type": "HEAD"
 *      },
 *      {
 *       "id": 2,
 *       "serial_number": "4fed342",
 *       "manufacturer": "Honeywell",
 *       "model_number": "1.0",
 *       "lot_number": "N/A",
 *       "status": "ISSUED",
 *       "device_type": "SHOULDER"
 *      }]
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 * @apiErrorExample  HRA not found
 *     HTTP/1.1 404 HRA not found
 */
router.get('/inventory', async function(req, res, next){
	try
	{      
        //Get the full list of sensor inventory for now.
        let query = db('iss_device')
        .join('iss_device_blast', 'iss_device.id', 'iss_device_blast.iss_device_id')
        .select({
            id : 'iss_device.id',
            iss_device_type_id : 'iss_device.iss_device_type_id',
            serial_number : 'iss_device.serial_number',
            manufacturer : 'iss_device.manufacturer',
            model_number : 'iss_device.model_number',
            lot_number : 'iss_device.lot_number',
            status : 'iss_device.status',
            device_type : 'iss_device_blast.device_type'
        })
        .whereNot('status', "DELETED");

        // Filter by serial if requested
        if(req.query.serial_number && req.query.device_type_id)
        {
            query.where('iss_device.serial_number', req.query.serial_number);
            query.where('iss_device.iss_device_type_id', req.query.device_type_id);
        }  

        let result = await query.select();
        
        // Return the list of sensors
        return res.json(result);

    }
    catch (err) {
        return next(err);
    }
});

/**
 * @api {post}  /api/issuance/inventory/blastgauge
 * @apiDescription Save or Update Blast Gauge Sensor Data
 * @apiGroup JHRM Issuance Sensor
 * @apiParam {Number} issuanceblastgauge.device_type_id
 * @apiParam {String} issuanceblastgauge.serial_number
 * @apiParam {String} issuanceblastgauge.manufacturer
 * @apiParam {String} issuanceblastgauge.model_number
 * @apiParam {String} issuanceblastgauge.lot_number
 * @apiParam {String} issuancedevices.sensor_type
 * @apiSuccess {string} response.status
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    {
 *        "Status": 'Blast Gauge Data Saved'
 *            "id": [10]
 *    }
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 * @apiErrorExample  HRA not found
 *     HTTP/1.1 404 HRA not found
 */
router.post('/inventory/blastgauge', async function (req, res, next)
{
    try
    {
        let exist = false;
        let id = req.body.id;

        let result = await db('iss_device')
            .where('serial_number', req.body.serial_number)
            .where('iss_device_type_id', req.body.iss_device_type_id);

        if(result.length > 0)
        {
            throw new createError.BadRequest("Serial Number and Sensor Type already exist.");
        }

        if (id)
        {
            let existing = await db('iss_device').where('id',id);

            if(existing.length===0)
            {
                throw new createError.BadRequest("Sensor Does Not Exist");
            }

            // Update Blast Gauge Sensor
            await db('iss_device')
                .where('id', id)
                .update({
                    iss_device_type_id : req.body.device_type_id,
                    serial_number : req.body.serial_number,
                    manufacturer : req.body.manufacturer,
                    model_number : req.body.model_number,
                    initialization_date : new Date(),
                    lot_number : 'N/A'
                });
            
            //Update New Blast Gauge Sensor Device Type
            await db('iss_device_blast')
                .where('iss_device_id',  id)
                .update({
                    device_type : req.body.device_type
                });
        }
        else
        { 

            // Insert Blast Gauge Sensor
            id = await db('iss_device').insert({
                iss_device_type_id : req.body.iss_device_type_id,
                serial_number : req.body.serial_number,
                manufacturer : req.body.manufacturer,
                model_number : req.body.model_number,
                initialization_date : new Date(),
                lot_number : 'N/A'
            });
            
            //Insert New Blast Gauge Sensor Device Type
            await db('iss_device_blast').insert({
                iss_device_id : id[0],
                device_type : req.body.device_type
            });
            
            id = id[0];
        }

        return res.status(201).json({
            Status: 'Blast Gauge Record Data Saved',
            id : id
        });

    }

    catch (err) {
        return next(err);
        }
    });

/**
 * @api {post}  /api/issuance/inventory/editsensor
 * @apiDescription Update Blast Gauge Sensor Data
 * @apiGroup JHRM Issuance Sensor
 * @apiParam {String} req.body.device_type
 * @apiParam {String} req.body.serial_number
 * @apiParam {String} req.body.manufacturer
 * @apiParam {String} req.body.model_number
 * @apiParam {String} req.body.id
 * @apiParam {String} req.body.iss_device_type_id
 * @apiSuccess {string} response.status
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    {
 *        "Status": 'Blast Gauge Record Updated'
 *            "id": [10]
 *    }
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 * @apiErrorExample  Sensor Already Exist
 *     HTTP/1.1 404 Sensor Already Exist
 */
router.post('/inventory/editsensor', async function (req, res, next)
{
    try
    {
        let id = req.body.id;

        let result = await db('iss_device')
            .where('id', id)

        if(result.length == 0)
        {
            throw new createError.BadRequest("Sensor does not exist.");
        }


        // Update Blast Gauge Sensor
        await db('iss_device')
            .where('id', id)
            .update({
                manufacturer : req.body.manufacturer,
                model_number : req.body.model_number,
                initialization_date : new Date(),
                lot_number : 'N/A'
            });

        //Update Blast Gauge Sensor Device Type
        await db('iss_device_blast')
            .where('iss_device_id',  id)
            .update({
                device_type : req.body.device_type
            });

        return res.status(201).json({
            Status: 'Blast Gauge Record Updated',
            id : result[0].id
        });

    }
    catch (err) {
        return next(err);
        }
});
 
/**
 * @api {get} /api/issuance/personnel Get the full list of Issued Personnel
 * @apiDescription List of all Issued Personnel
 * @apiGroup JHRM Issuance Personnel
 * @apiSuccess {Object[]} issuancepersonnel[] Array of Issued Personnel
 * @apiSuccess {Number} issuancepersonnel.id
 * @apiSuccess {String} issuancepersonnel.first_name
 * @apiSuccess {String} issuancepersonnel.last_name
 * @apiSuccess {Number} issuancepersonnel.dodid
 * @apiSuccess {String} issuancepersonnel.unit
 * @apiSuccess {String} issuancepersonnel.grade
 * @apiSuccess {String} issuancepersonnel.mos_code
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     [
 *         {
 *             "id": 1,
 *             "first_name": "John",
 *             "last_name": "Smith",
 *             "dodid": 1050107503,
 *             "unit": "3rd Battalion, Alpha Company",
 *             "grade": "SFC",
 *             "mos_code": "18B"
 *         },
 *         {
 *             "id": 2,
 *             "first_name": "Mike",
 *             "last_name": "Walker",
 *             "dodid": 1046856826,
 *             "unit": "3rd Battalion, Alpha Company",
 *             "grade": "SSG",
 *             "mos_code": "18B"
 *        }
 *     ]
 * @apiErrorExample {none} Internal Error Error
 *    Undefined location cannot read property
 */
router.get('/personnel', async function(req, res, next){
    try
    {
        let personnels
        // Get list of all Issued Personnel
        let query = db('iss_personnel');

        // Filter by DODID if requested
        if(req.query.dodid)
        {
            query.where('dodid', req.query.dodid);
             // Run query
             personnels = await query.select();
        }
        else
        {
            personnels = await db.raw(`
            select 
                distinct iss_personnel.*,
                max(if(iss_event.iss_personnel_id=iss_personnel.id AND iss_device.status="ISSUED", 1, 0)) as has_sensor
                from iss_personnel
                join iss_device
                join iss_event on iss_event.id = iss_device.last_issued_event_id
                group by dodid
                `
            )

            personnels = personnels[0];
        }
        
        // Return the list of all Issued Personnel
        return res.json(personnels);
    }
    catch (err)
    {
        return next(err);
    }
});

/**
 * @api {get} /api/issuance/inventory/blastgauge/serviceable Get the full list of Servicable Sensor By Type
 * @apiDescription List of all Sensors By Type
 * @apiGroup JHRM Issuance Personnel
 * @apiSuccess {Object[]} sensorbytype[] Array of Servicable Sensors By Type
 * @apiSuccess {String} issuancepersonnel.serial_number
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     [
 *         {
 *             "serial_number": "a22345-W"
 *         },
 *         {
 *             "serial_number": "b22345-S"
 *        }
 *     ]
 * @apiErrorExample {none} Internal Error Error
 *    Undefined location cannot read property
 */
router.get('/inventory/blastgauge/serviceable', async function(req, res, next){
    try
    {
        // Get list of all Sensor by Type
        let iss_devices = await db('iss_device')
            .select('serial_number')
            .where('status', "SERVICEABLE")
            

        // Return the list of all Sensor by Type
        return res.json(iss_devices);
    }
    catch (err)
    {
        return next(err);
    }
});


/**
 * @api {post}  /api/issuance/issue/sensor
 * @apiDescription Save or Update Blast Gauge Sensor Data
 * @apiGroup JHRM Issuance Sensor
 * @apiParam {Object} Issued Sensor object
 * @apiParam {Int} req.body.id
 * @apiParam {Int} req.body.iss_device_id
 * @apiParam {String} req.body.serial_number
 * @apiParam {String} req.body.wear_location
 * @apiParam {String} req.body.wear_location_description
 * @apiParam {String} req.body.initial_value
 * @apiParam {String} req.body.type
 * @apiParam {String} req.body.device_status
 * @apiParam {String} req.body.personnel_association
 * @apiParam {String} req.body.sensor_association
 * @apiParam {String} req.body.location_association
 * @apiParam {String} req.body.date_association 
 * @apiSuccess {string} response.status
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    {
 *        "Status": 'Sensor Type Data Saved'
 *    }
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 * @apiErrorExample  HRA not found
 *     HTTP/1.1 404 HRA not found
 */
router.post('/issue/sensor', async function (req, res, next)
{
    try
    {
        let user = users.find(user=>user.username==req.cookies.user_name);

        if (req.body.length == 0 || !user.username)
        {
            throw new createError.BadRequest("Invalid Arguments");
        }

        // Verfiy that all devices exist in the database. 
        let sensors = await db('iss_device').whereIn('id', req.body.map(item=>item.id));
        if(sensors.length != req.body.length)
        {
            throw new createError.BadRequest("Invalid sensor ID");
        }

        // Verify all sensors being issued are in the appropriate state
        if(sensors.find(sensor=>sensor.status!=ISS_DEVICE_STATUS.SERVICEABLE))
        {
            throw new createError.BadRequest("Sensor not issueable.");
        }

        // If any blast gauges are being issued, create a set ID. 
        if(sensors.find(sensor=>sensor.iss_device_type_id==ISS_DEVICE_TYPE_ID.BLAST_GAUGE))
        {
            // Seems strange, but we are inserting only default values: auto 
            // incrementing ID and current timestamp. 
            var set_id = await db('iss_set').insert({});
        }

        for (var issuance_event of req.body) 
        {
            let sensor = sensors.find(item=>item.id==issuance_event.id);

            let response = await db('iss_event').insert({
                'created_by' : user.username,
                'iss_device_id' : issuance_event.id,
                'iss_personnel_id' : issuance_event.iss_personnel_id,
                'date' : issuance_event.date ? issuance_event.date : new Date(),
                'location' : issuance_event.location,
                'wear_location' : issuance_event.wear_location,
                'wear_location_description' : issuance_event.wear_location_description,
                'initial_value' : issuance_event.initial_value,
                'type' : ISS_EVENT_TYPE.ISSUED,
                'device_status' : ISS_DEVICE_STATUS.ISSUED,
                'personnel_association' : issuance_event.personnel_association ? 
                    issuance_event.personnel_association : ISS_ASSOCIATION.MANUAL,
                'sensor_association' : issuance_event.sensor_association ? 
                    issuance_event.sensor_association : ISS_ASSOCIATION.MANUAL,
                'date_association' : issuance_event.date ? 
                    ISS_ASSOCIATION.MANUAL : ISS_ASSOCIATION.SYSTEM
                })

                await db('iss_device')
                    .update({
                        'status' : ISS_DEVICE_STATUS.ISSUED,
                        'last_issued_event_id' : response[0],
                    })
                    .where('id', issuance_event.id)
                    .where('serial_number', issuance_event.serial_number);

                // If this is a blast gauge, remove from any previous sets and
                // add to the current set. 
                if(sensor.iss_device_type_id==ISS_DEVICE_TYPE_ID.BLAST_GAUGE)
                {
                    await db('iss_device_set')
                        .update({
                            'date_removed' : new Date()
                        })
                        .where('iss_device_id', issuance_event.id)
                        .whereNull('date_removed');

                    await db('iss_device_set').insert({
                        'iss_device_id' : issuance_event.id,
                        'iss_set_id' : set_id
                    });
                }
                    
            }

        return res.status(201).json({
            Status: 'Sensor Type Data Saved'
        });

    }
    catch (err) {
        return next(err);
        }
    });

 
/**
 * @api {get} /api/issuance/inventory/issued/sensor/:dodid Get list of issued sensors by user dodid
 * @apiDescription List of issued sensors by specified dodid
 * @apiParam {Number} :dodid URL Parameter. The DODID of specified user
 * @apiGroup JHRM Issuance Personnel
 * @apiSuccess {Object[]} sensorbytype[] Array of Isued Sensors By dodid
 * @apiSuccess {String} issuancepersonnel.serial_number
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     [
 *         {
 *              "created_on": "2021-02-13T18:48:03.000Z",
 *              "date": "2021-02-08T16:49:14.000Z",
 *              "wear_location": "SHOULDER",
 *              "notes": null,
 *              "device_status": "SERVICEABLE",
 *              "serial_number": "e22345-S"
 *          },
 *          {
 *              "created_on": "2021-02-13T18:48:03.000Z",
 *              "date": "2021-02-08T16:49:14.000Z",
 *              "wear_location": "CHEST",
 *              "notes": null,
 *              "device_status": "SERVICEABLE",
 *              "serial_number": "j89101-C"
 *          },
 *          {
 *              "created_on": "2021-02-13T18:48:03.000Z",
 *              "date": "2021-02-08T16:49:14.000Z",
 *              "wear_location": "HEAD",
 *              "notes": null,
 *              "device_status": "SERVICEABLE",
 *              "serial_number": "d22345-H"
 *          }
 *     ]
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 */
router.get('/inventory/issued/sensor/:dodid', async function(req, res, next){
    try
    {
        // Get list issued sensors for individual by dodid and status
        let response = await db.raw(`
            select
                iss_device.id,
                iss_event.iss_personnel_id,
                created_on, 
                date, 
                iss_event.wear_location, 
                notes, 
                device_status, 
                serial_number,
                iss_device_type_id
                from iss_device
                
                join iss_event on iss_event.id = iss_device.last_issued_event_id
                join iss_personnel on iss_personnel.id = iss_event.iss_personnel_id
                
                where iss_device.status = ?
                and iss_personnel.dodid = ?
        `,[ISS_DEVICE_STATUS.ISSUED, req.params.dodid]);
        return res.json(response[0]);
    }
    catch (err)
    {
        return next(err);
    }
});

/**
 * @api {post}  /api/issuance/issue/returnsensor
 * @apiDescription Insert new entry for returned sensor
 * @apiGroup JHRM Issuance Sensor
 * @apiParam {Object} Issued Sensor object
 * @apiParam {String} req.body.username
 * @apiParam {Date} req.body.created_on
 * @apiParam {Int} req.body.iss_device_id
 * @apiParam {Int} req.body.iss_personnel_id
 * @apiParam {Date} req.body.date
 * @apiParam {String} req.body.serial_number
 * @apiParam {String} req.body.location
 * @apiParam {String} req.body.wear_location
 * @apiParam {String} req.body.wear_location_description
 * @apiParam {String} req.body.type
 * @apiParam {String} req.body.initial_value
 * @apiParam {String} req.body.device_status
 * @apiParam {String} req.body.personnel_association
 * @apiParam {String} req.body.sensor_association
 * @apiParam {String} req.body.location_association
 * @apiParam {String} req.body.date_association 
 * @apiSuccess {string} response.status
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    {
 *        "Status": 'Sensor Type Data Saved'
 *    }
 * @apiErrorExample Internal Server Error
 *     HTTP/1.1 500 Internal Server Error
 * @apiErrorExample  HRA not found
 *     HTTP/1.1 404 HRA not found
 */
router.post('/issue/returnsensor', async function (req, res, next)
{
    try
    {
        let user = users.find(user=>user.username==req.cookies.user_name);

        if (req.body.length == 0 || !user.username)
        {
            throw new createError.BadRequest("Invalid Arguments");
        }

        for (var return_event of req.body) 
        {
            let response;

            // Create an explanatory event for every status except for SERVICEABLE
            if(return_event.device_status != ISS_DEVICE_STATUS.SERVICEABLE)
            {
                response = await db('iss_event').insert({
                    'created_by' : user.username,
                    'created_on' : return_event.date_reported,
                    'iss_device_id' : return_event.device_id,
                    'iss_personnel_id' : return_event.personnel_id,
                    'date' : return_event.date_discovered,
                    'type' : return_event.device_status == "NONSERVICEABLE" ? ISS_EVENT_TYPE.DAMAGED : 
                        return_event.device_status == "DATAMISMATCH" ? ISS_EVENT_TYPE.OTHER : 
                        ISS_EVENT_TYPE.RETURNED,
                    'device_status' : ISS_DEVICE_STATUS.NONSERVICEABLE,
                    'notes' : return_event.notes,
                    'personnel_association' : return_event.personnel_association ? 
                        return_event.personnel_association : ISS_ASSOCIATION.MANUAL,
                    'sensor_association' : return_event.sensor_association ? 
                        return_event.sensor_association : ISS_ASSOCIATION.MANUAL,
                    'date_association' : ISS_ASSOCIATION.MANUAL
                    })
            }

            // Create a returned event for every status except for LOST, which 
            // is by definition not being returned. 
            if(return_event.device_status !== ISS_DEVICE_STATUS.LOST)
            {
                response = await db('iss_event').insert({
                    'created_by' : user.username,
                    'iss_device_id' : return_event.device_id,
                    'iss_personnel_id' : return_event.personnel_id,
                    'date' : return_event.date ? return_event.date : new Date(),
                    'location' : return_event.location,
                    'type' : ISS_EVENT_TYPE.RETURNED,
                    'device_status' : return_event.device_status == ISS_DEVICE_STATUS.SERVICEABLE ? 
                        ISS_DEVICE_STATUS.SERVICEABLE : ISS_DEVICE_STATUS.NONSERVICEABLE,
                    'notes' : return_event.notes,
                    'personnel_association' : return_event.personnel_association ? 
                        return_event.personnel_association : ISS_ASSOCIATION.MANUAL,
                    'sensor_association' : return_event.sensor_association ? 
                        return_event.sensor_association : ISS_ASSOCIATION.MANUAL,
                    'date_association' : return_event.date ? 
                        ISS_ASSOCIATION.MANUAL : ISS_ASSOCIATION.SYSTEM
                });
            }

            await db('iss_device')
                .update({
                    'status' : return_event.device_status != ISS_DEVICE_STATUS.SERVICEABLE ? 
                        ISS_DEVICE_STATUS.NONSERVICEABLE : 
                        ISS_DEVICE_STATUS.SERVICEABLE,
                    'last_status_change_event_id' : response[0],
                })
                .where('id', return_event.device_id)
                    
            }

        return res.status(201).json({
            Status: 'Sensor Type Data Saved'
        });

    }
    catch (err) 
    {
        return next(err);
    }
});



/**
 * @api {post} /api/issuance/personnel Save array of Issued Personnel
 * @apiDescription Save array of Issued Personnel
 * @apiParam {Object[]} personnel[] Array of Issued Personnel
 * @apiParam {Number} issuancepersonnel.id
 * @apiParam {String} issuancepersonnel.first_name
 * @apiParam {String} issuancepersonnel.last_name
 * @apiParam {Number} issuancepersonnel.dodid
 * @apiParam {String} issuancepersonnel.unit
 * @apiParam {String} issuancepersonnel.grade
 * @apiParam {String} issuancepersonnel.mos_code
 * @apiGroup JHRM Issuance Personnel
 * @apiSuccess {Object} response Response JSON Object
 * @apiSuccess {String} response.Status
 * @apiSuccess {String} response.id
 * @apiSuccessExample Success
 *     HTTP/1.1 200 OK
 *     {
 *         "status": "Success"
 *     }
 * @apiErrorExample Bad Request
 *     HTTP/1.1 400 Invalid Arguments
 * @apiErrorExample {none} Internal Error Error
 *     Undefined location cannot read property
 */
router.post('/personnel', async function(req, res, next) {
    try
    {
        // Require an array argument, even if it is empty. But if it is not 
        // empty, all members have to contain a numeric DODID.
        if (!Array.isArray(req.body) || req.body.find(item=>!validDodid(item.dodid)))
        {
            throw new createError.BadRequest("Invalid Arguments");
        }

        await db.transaction(async trx => {
            // Delete removed persons
            await trx('iss_personnel')
                .whereNotIn('dodid', req.body.filter(item=>item.dodid!==undefined).map(item=>item.dodid))
                .del();

            for (let i = 0; i < req.body.length; i++)
            {
                let person = req.body[i];
                await addPersonnel(trx, person);
            }
        })

        return res.json({
            status : 'Success'
        });
    }
    catch(err)
    {
        return next(err);
    }
});

/**
 * @api {post} /api/issuance/addpersonnel Add/Update an Issued Personnel
 * @apiDescription Add a new or update an existing Issued Personnel
 * @apiParam {Object} issuancepersonnel Issuance Personnel
 * @apiParam {String} issuancepersonnel.first_name
 * @apiParam {String} issuancepersonnel.last_name
 * @apiParam {Number} issuancepersonnel.dodid
 * @apiParam {String} issuancepersonnel.unit
 * @apiParam {String} issuancepersonnel.grade
 * @apiParam {String} issuancepersonnel.mos_code
 * @apiGroup JHRM Issuance Personnel
 * @apiSuccess {object} response
 * @apiSuccess {string} response.status
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *         "status": "Success"
 *         "id": 1
 *     }
 * @apiErrorExample Invalid Request
 *     HTTP/1.1. 401 DOD ID is not a 10 digit number
 * @apiErrorExample Conflict Request
 *     HTTP/1.1. 409 Person with DOD ID already exists
 * @apiErrorExample Conflict Request
 *     HTTP/1.1. 409 Person with row ID does not exist
 * @apiErrorExample {none} Internal Error Error
 *     Undefined location cannot read property
 */
router.post('/addpersonnel', async function(req, res, next) {
    try
    {
        // Make sure dodid is valid.
        if (!validDodid(req.body.dodid))
        {
            return next(createError(401, 'DOD ID is not a 10 digit number.'));
        }

        let id = await addPersonnel(db, req.body);
        if (id == -1)
        {
            return next(createError(409, 'Person with DOD ID already exists.'));
        }
        else if (id == -2)
        {
            return next(createError(409, 'Person no longer exists.'));
        }
        else
        {
            return res.json({
                status: 'Success',
                id: id
            });
        }
    }
    catch (err)
    {
        return next(err);
    }
});


router.post('/importpersonnel', async function(req, res, next) {
    try
    {
        for (let i=0; i < req.body.length; i++)
		{
            
            let person_record = {
                first_name : req.body[i].first_name,
                last_name : req.body[i].last_name,
                dodid : req.body[i].dodid,
                unit : req.body[i].unit,
                grade: req.body[i].grade,
            }

            let existing = await db('iss_personnel').where('dodid', person_record.dodid);
        
            if (existing.length > 0)
            {
               await db('iss_personnel')
                    .update(person_record)
                    .where('dodid', person_record.dodid);
            }
            else
            {
                await db('iss_personnel').insert(person_record);
            }
        }

        return res.json({
            status: 'Success'
        });
    }
    catch (err)
    {
        return next(err);
    }
});


/**
 * @api {post} /api/issuance/deletepersonnel Delete an Issued Personnel
 * @apiDescription Delete an Issued Personnel
 * @apiParam {Object} Response JS Issuance Personnel
 * @apiParam {String} response.dodid
 * @apiGroup JHRM Issuance Personnel
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 201 CREATED
 *     {
 *         "status": "Delete Record Success"
 *     }
 * @apiErrorExample Issued Personnel Not Found
 *     HTTP/1.1 404 Issued Personnel Not Found
 * @apiErrorExample {none} Internal Error Error
 *     Undefined location cannot read property
 */
router.post('/deletepersonnel', async function(req, res, next) {
    try
    {
        let records = await db('iss_personnel')
            .where('dodid', req.body.dodid)
            .del();

        if (!records)
        {
            return next(createError(404, 'Issued Personnel Not Found'));
        }
        else 
        {
            return res.status(201).json({
                status: 'Delete Record Success'
            });
        }
    }
    catch (err)
    {
        return next(err);
    }
});

router.get('/personnel/distinctunit', async function(req, res, next)
{
    //return all distinct units that are associated to any personnel to display
    try {

        var response = await db.raw(`
            SELECT DISTINCT unit 
            From iss_personnel `);
    
        return res.json(response[0]);

    } catch (error) {
        res.status(500).send(error.message);
    }
});

/**
 * @api {post} /api/issuance/inventory/delete Hide a Sensor from inventory
 * @apiDescription Hide an Sensor From inventory
 * @apiParam {Object} Sensor
 * @apiParam {String} Sensor.id
 * @apiGroup JHRM Issuance Sensor Inventory
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 201 
 *     {
 *         "status": "Update Record Success"
 *     }
 * @apiErrorExample Sensor Not Found
 *     HTTP/1.1 404 Sensor Not Found
 * @apiErrorExample {none} Bad Request
 *     Issued sensors cannot be deleted.
 */
router.post('/inventory/delete', async function(req, res, next) {

    try
    {
       let existing = await db('iss_device')
       .where('id', req.body.id)

        if(existing.length===0)
        {
            throw new createError.NotFound("Sensor Does Not Exist");
        }
        
        if (existing[0].status != ISS_DEVICE_STATUS.ISSUED)
        {
            await db('iss_device').update({
                'status' : ISS_DEVICE_STATUS.DELETED })
                .where('id', req.body.id)
        
            return res.status(201).json({
                status: 'Update Record Success' 
            });
        }
        else
        {
            throw new createError.BadRequest("Issued sensors cannot be deleted.");
        }
    }
    catch (err)
    {
        return next(err);
    }
});

/**
 * @api {get} /api/issuance/report/blastgauges Get Blast Gauge Issuance Report
 * @apiDescription List of currently issued blast gauges
 * @apiParam {String} unit Unit (Optional)
 * @apiGroup JHRM Issuance Report
 * @apiSuccess {Object[]} report[] Array of issued blast gauges
 * @apiSuccess {Number} report.dodid
 * @apiSuccess {String} report.first_name
 * @apiSuccess {String} report.last_name
 * @apiSuccess {String} report.grade
 * @apiSuccess {String} report.serial_number
 * @apiSuccess {Datetime} report.issued_date
 * @apiSuccess {String} report.wear_location
 * @apiSuccess {Datetime} report.returned_date
 * @apiSuccess {String} report.returned_sensor_status
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     [
 *         {
 *             "dodid": "1050107503",
 *             "first_name": "Joe",
 *             "last_name": "Smith",
 *             "grade": "E-7",
 *             "device_type": "HEAD",
 *             "serial_number": "a22345-H",
 *             "issued_date": "2020-06-27T08:30:00.000Z",
 *             "wear_location": "On front of helmet",
 *             "returned_date": "2020-06-30T15:01:00.000Z",
 *             "returned_sensor_status": "SERVICEABLE"
 *         },
 *         {
 *             "dodid": "1050107503",
 *             "first_name": "Joe",
 *             "last_name": "Smith",
 *             "grade": "E-7",
 *             "device_type": "SHOULDER",
 *             "serial_number": "b22345-S",
 *             "issued_date": "2020-06-27T08:30:00.000Z",
 *             "wear_location": "Right Shoulder",
 *             "returned_date": "2020-06-30T15:02:00.000Z",
 *             "returned_sensor_status": "SERVICEABLE"
 *         },
 *         {
 *             "dodid": "1050107503",
 *             "first_name": "Joe",
 *             "last_name": "Smith",
 *             "grade": "E-7",
 *             "device_type": "CHEST",
 *             "serial_number": "c22345-C",
 *             "issued_date": "2020-06-27T08:30:00.000Z",
 *             "wear_location": "Center of chest on body armor",
 *             "returned_date": "2020-06-30T15:03:00.000Z",
 *             "returned_sensor_status": "SERVICEABLE"
 *         }
 *     ]
 * @apiErrorExample {none} Internal Error Error
 *    Undefined location cannot read property
 */

router.get('/report/blastgauges', async function (req, res, next)
{
    try
    {
        // .onVal() is not listed in the Knex.js documentation
        // but it is used to join on a value instead of a column
        let query = db('iss_device_type AS device_type')
                .where('name', 'Blast Gauge')
            .join('iss_device AS device', 'device_type.id', 'device.iss_device_type_id')
            .join('iss_event AS issued', 'device.last_issued_event_id', 'issued.id')
            .join('iss_personnel AS personnel', 'issued.iss_personnel_id', 'personnel.id')
            .join('iss_device_blast AS device_blast', 'device.id', 'device_blast.iss_device_id',)
            .leftJoin('iss_event AS returned', function() {
                this.onVal(
                    'returned.date',
                    db('iss_event')
                        .min('date')
                        .where('date', '>', db.ref('issued.date'))
                        .andWhere('type', 'RETURNED')
                        .andWhere('iss_device_id', db.ref('issued.iss_device_id'))
                )
                .onVal('returned.type', 'RETURNED')
                .on('returned.iss_device_id', 'issued.iss_device_id')
            })
            .select(
                'dodid',
                'first_name',
                'last_name',
                'grade',
                'device_type',
                'serial_number',
                'issued.date AS issued_date',
                'issued.wear_location_description AS wear_location',
                'returned.date AS returned_date',
                'returned.device_status AS returned_sensor_status'
            );

        // Filter by unit if requested
        if(req.query.unit)
        {
            query.where('unit', req.query.unit);
        }

        let response = await query

        return res.json(response);
    }
    catch (err)
    {
        return next(err);
    }
});


/**
 * Helper that standardizes error output to be an JSON object, and sets the
 * correct headers. Example body content: 
 *  
 * {
 *   "status": "Error",
 *   "statusCode": 404,
 *   "message": "ISS Not found."
 * }
 * 
 * Error output is santitized to avoid exposing implementation details unless 
 * running in debug mode. 
 * 
 * In order to make certain that this handler is called, especially if your 
 * method is asynchronous, make sure to call the next() function provided 
 * by express with the error as the first argument. 
 * 
 */
router.use(function (err, req, res, next) {
    if (!err.statusCode) {
        err = createError(500, err);
    }
    res.status(err.statusCode).json({
        status: 'Error',
        statusCode: err.statusCode,
        message: err.expose || config.get('debug') ? err.message : "Internal Server Error."
    });
});

module.exports = router