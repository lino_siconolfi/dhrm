// This allows project-relative includes. Just a bit of convenience. 
process.env.NODE_PATH = __dirname;
require('module').Module._initPaths();

// Lib Requirements
var http = require('http');
var config = require('config');
var express = require('express'); 
var cookieParser = require('cookie-parser');
const session = require("express-session");
const KnexSessionStore = require("connect-session-knex")(session);
var okta = require('./src/util/okta');
const fs = require('fs')
const path = require('path');
const parseurl = require('parseurl')

// Route Handlers
var healthCheck = require('./src/handlers/health-check');
var apiRouter = require('./src/api/router');

// Hack to allow case insensitive filesystems to act case sensitive. Included
// here directly to make it more obvious than an import
function caseSensitiveStatic(root, options)
{
	var wrapped = express.static(root, options);
	return function(req, res, next)
	{
		var filepath = path.join(root, parseurl(req).pathname);
		var dir = path.dirname(filepath);
		var name = path.basename(filepath);
		fs.readdir(dir, function (err, files) 
		{
			if (err) 
			{
				return next(err);
			}
			if (files.indexOf(name) >= 0) 
			{
				wrapped(req, res, next);
			}
			else 
			{
				res.status(404).end();
			}
		});
	}
}

// Express Config
var app = express();

// Cookie Parser
app.use(cookieParser());

// Session Handing
const db = require('src/util/db');
const store = new KnexSessionStore({
  knex: db,
  tablename: "sessions"
});

// Cookie Parser
app.use(cookieParser());

app.use(
  session({
	secret: config.get('session.secret'),
	saveUninitialized : false,
	resave : false,
    cookie: {
      maxAge: config.get('session.cookieMaxAge')
    },
    store: store
  })
);

// Serve JS utilities to the front end
app.use('/vendor/papaparse',
	caseSensitiveStatic(
		'node_modules/papaparse',
		{
			dotfiles: 'ignore',
			etag: true,
			maxAge: config.get('staticMaxAge'),
			redirect: false
		}
	)
);

// Serve JS utilities to the front end
app.use('/vendor/read-excel-file',
	caseSensitiveStatic(
		'node_modules/read-excel-file/bundle',
		{
			dotfiles: 'ignore',
			etag: true,
			maxAge: config.get('staticMaxAge'),
			redirect: false
		}
	)
);

// Serve JS utilities to the front end
app.use('/vendor/d3',
	caseSensitiveStatic(
		'node_modules/d3/build',
		{
			dotfiles: 'ignore',
			etag: true,
			maxAge: config.get('staticMaxAge'),
			redirect: false
		}
	)
);

// Serve JS utilities to the front end
app.use('/vendor/metrics-graphics',
	caseSensitiveStatic(
		'node_modules/metrics-graphics/dist',
		{
			dotfiles: 'ignore',
			etag: true,
			maxAge: config.get('staticMaxAge'),
			redirect: false
		}
	)
);

// Serve JS utilities to the front end
app.use('/js/util',
	caseSensitiveStatic(
		'src/util',
		{
			dotfiles: 'ignore',
			etag: true,
			maxAge: config.get('staticMaxAge'),
			redirect: false
		}
	)
);

// Serve JS utilities to the front end
app.use('/js/shared',
	caseSensitiveStatic(
		'src/shared',
		{
			dotfiles: 'ignore',
			etag: true,
			maxAge: config.get('staticMaxAge'),
			redirect: false
		}
	)
);

// Serve vendor JS
app.use('/vendor/leaflet',
	caseSensitiveStatic(
		'node_modules/leaflet/dist',
		{
			dotfiles: 'ignore',
			etag: true,
			maxAge: config.get('staticMaxAge'),
			redirect: false
		}
	)
);
app.use('/vendor/leaflet.markercluster',
	caseSensitiveStatic(
		'node_modules/leaflet.markercluster/dist',
		{
			dotfiles: 'ignore',
			etag: true,
			maxAge: config.get('staticMaxAge'),
			redirect: false
		}
	)
);
app.use('/vendor/leaflet.gridlayer.googlemutant',
	caseSensitiveStatic(
		'node_modules/leaflet.gridlayer.googlemutant/dist',
		{
			dotfiles: 'ignore',
			etag: true,
			maxAge: config.get('staticMaxAge'),
			redirect: false
		}
	)
);

// Serve API (internal)
app.use('/api', apiRouter);


// Serve API (external) with okta auth (if enabled)
let useOkta = config.get('security.okta.enabled');
if(useOkta)
{
	app.use('/external-api', okta);
}
app.use('/external-api', apiRouter);


// Serve Health Checks
app.get('/health-simple', healthCheck.simple);
app.get('/health-full', healthCheck.full);

// Serve static files
app.use(
	caseSensitiveStatic(
		'public',
		{
			dotfiles: 'ignore',
			etag: true,
			index: "index.html",
			maxAge: config.get('staticMaxAge'),
			redirect: false
		}
	)
);

// Start Server
var server = http.createServer(app);
server.listen(
	process.env.PORT || config.get('port'),
	function ()
	{
		var host = server.address().address;
		var port = server.address().port;
		console.log('Listening at https://%s:%s', host, port);
	}
);